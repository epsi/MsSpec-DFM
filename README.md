Building MsSpec_DFM
===================

To build MsSpec_DFM, you need a fortran compiler (gfortran), python > 3.6 and pip

Getting the source code
-----------------------

To download the source code, you can clone the git repo of the project

```
$ git clone https://git.ipr.univ-rennes1.fr/epsi/MsSpec-DFM.git
```

If git is not avilable on your system, you can download a compressed archive
on the [repo website](https://git.ipr.univ-rennes1.fr/epsi/MsSpec-DFM).
Once downloaded, this archive should be uncompressed.


Building and installing
-----------------------

```
# Change to the src folder
$ cd MsSpec-DFM/src

# Type in 'make' or 'make all'
$ make

# This will install the commandline utility and python bindings
$ make install
```

Every new Python packages and dependencies will be installed in the
Python user site packages (under *$HOME/.local/* tree).

You can also use a Python virtual environment


Uninstalling
------------

```
$ pip uninstall msspec_dfm
```



Running the code
================

The program can be run either from the command line or from a Python script.

Command-line
------------

The executable is called 'eps'. Type in 'man eps' to get more help. It has 3 main sub-commands:

- generate
- compute
- plot

Begin by generating an input file with default values:

```
$ eps generate
```

It will create a default input file (Data/epsilon.dat).
Then, edit this input file according to your needs. You can find some details
about the allowed values of input parameters [here](./DESC) and launch a calculation.

```
$ eps compute Data/epsilon.dat
```

Finally you can plot the results

```
$ eps plot diel_func
```

Python bindings
---------------

Have a look to [this example](./examples/example00.py)
