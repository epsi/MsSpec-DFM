#!/usr/bin/env python
#
# Copyright © 2022 - Rennes Physics Institute
#
# This file is part of MsSpec-DFM.
#
# MsSpec-DFM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# MsSpec-DFM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with MsSpec-DFM.  If not, see <http://www.gnu.org/licenses/>.
#
# Source file  : src/python/setup.py
# Last modified: Fri, 25 Feb 2022 17:27:32 +0100
# Committed by : Sylvain Tricot <sylvain.tricot@univ-rennes1.fr> 1645806435 +0100

import glob
import sys
from setuptools import setup, find_packages
from msspec_dfm.version import __version__

with open('pip.freeze', 'r') as fd:
    REQUIREMENTS = fd.read().strip().split('\n')

if __name__ == "__main__":
    setup(name='msspec_dfm',
          version=__version__,
          include_package_data=True,
          packages=find_packages(include='msspec_dfm.*'),
          install_requires=REQUIREMENTS,
          data_files = [('share/man/man1', glob.glob('man/pages/*.gz'))],

          author='Didier Sébilleau, Aditi Mandhal, Sylvain Tricot',
          author_email='sylvain.tricot@univ-rennes1.fr',
          maintainer='Sylvain Tricot',
          maintainer_email='sylvain.tricot@univ-rennes1.fr',
          url='https://git.ipr.univ-rennes1.fr/epsi/MsSpec-DFM',
          description='',
          long_description="",
          #download_url='',
          # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
          classifiers=[
            'Development Status :: 3 - Alpha',
            'Environment :: Console',
            'Intended Audience :: Science/Research',
            'License :: OSI Approved :: GNU General Public License (GPL)',
            'Natural Language :: English',
            'Operating System :: Microsoft :: Windows :: Windows 10',
            'Operating System :: POSIX :: Linux',
            'Operating System :: MacOS :: MacOS X',
            'Programming Language :: Fortran',
            'Programming Language :: Python :: 3 :: Only',
            'Topic :: Scientific/Engineering :: Physics',
          ],
          keywords='',
          license='GPL',
          entry_points={'console_scripts': [
                              'eps=msspec_dfm.cli:main',],
                       },
          )
