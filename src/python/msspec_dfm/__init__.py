#!/usr/bin/env python
# coding: utf-8
#
# Copyright © 2022 - Rennes Physics Institute
#
# This file is part of MsSpec-DFM.
#
# MsSpec-DFM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# MsSpec-DFM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with MsSpec-DFM.  If not, see <http://www.gnu.org/licenses/>.
#
# Source file  : src/python/msspec_dfm/__init__.py
# Last modified: Fri, 25 Feb 2022 17:27:31 +0100
# Committed by : Sylvain Tricot <sylvain.tricot@univ-rennes1.fr> 1645806435 +0100

from msspec_dfm.version import __version__
try:
    from msspec_dfm.compute import compute
except ModuleNotFoundError as err:
    pass
