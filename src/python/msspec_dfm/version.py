#!/usr/bin/env python
# coding: utf-8
#
# Copyright © 2022 - Rennes Physics Institute
#
# This file is part of MsSpec-DFM.
#
# MsSpec-DFM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# MsSpec-DFM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with MsSpec-DFM.  If not, see <http://www.gnu.org/licenses/>.
#
# Source file  : src/python/msspec_dfm/version.py
# Last modified: Fri, 25 Feb 2022 17:27:32 +0100
# Committed by : Sylvain Tricot <sylvain.tricot@univ-rennes1.fr> 1645806435 +0100

import os

from pkg_resources import DistributionNotFound
from pkg_resources import get_distribution
from pkg_resources import parse_version

# find the version number
# 1- Try to read it from the git info
# 2- If it fails, try to read it from the distribution file
# 3- If it fails, try to read it from the VERSION file

try:
    from setuptools_scm import get_version
    v = get_version(root='../../../', relative_to=__file__, version_scheme="post-release")
    v = parse_version(v)
    __version__ = v.base_version
    # if v._version.post[-1] == 0:
        # __version__ = v.base_version
    # else:
        # __version__ = v.public
except Exception as err:
    try:
        __version__ = get_distribution(__name__.strip('.version')).version
    except Exception as err:
        try:
            thisfile_path = os.path.abspath(__file__)
            thisfile_dir = os.path.dirname(thisfile_path)
            versionfile = os.path.join(thisfile_dir, "../VERSION")
            with open(versionfile, "r") as fd:
                __version__ = fd.readline()
        except Exception as err:
            #print("Unable to get the version number!")
            __version__ = "0.0.0"
