#!/usr/bin/env python
# coding: utf-8
#
# Copyright © 2022 - Rennes Physics Institute
#
# This file is part of MsSpec-DFM.
#
# MsSpec-DFM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# MsSpec-DFM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with MsSpec-DFM.  If not, see <http://www.gnu.org/licenses/>.
#
# Source file  : src/python/msspec_dfm/version.py
# Last modified: Fri, 25 Feb 2022 17:27:32 +0100
# Committed by : Sylvain Tricot <sylvain.tricot@univ-rennes1.fr> 1645806435 +0100

import os

class InputFile:
    defaults = {
            # GENERAL PARAMETERS
            # (q, omega, r)
            'Q_MIN'    : (0.010, '13.3f'),
            'Q_MAX'    : (4.000, '13.3f'),
            'N_Q'      : (1000, '10d'),
            'E_MIN'    : (0.010, '13.3f'),
            'E_MAX'    : (4.000, '13.3f'),
            'N_E'      : (2000, '10d'),
            'R_MIN'    : (0.010, '13.3f'),
            'R_MAX'    : (4.000, '13.3f'),
            'N_R'      : (2000, '>10d'),
            # Material's properties
            'RS'       : (2.079, '13.3f'),
            'MSOM'     : (1.000, '13.3f'),
            'MAT_TYP'  : ('SCHRO', '>10s'),
            'EPS_B'    : (1.000, '13.3f'),
            # External fields
            'T'        : (1.00, '12.2f'),
            'E'        : (0.000, '13.3f'),
            'H'        : (0.000, '13.3f'),
            'FLD'      : ('NO', '>10s'),
            # System's dimension
            'DIM'      : ('3D', '>10s'),
            # Confinement
            'R0'       : (0.000, '13.3f'),
            'L'        : (0.000, '13.3f'),
            'OM0'      : (0.00, '12.2f'),
            'CONFIN'   : ('NO-CONF', '>10s'),
            # Multilayer structure
            'DL'       : (0.000, '13.3f'),
            'D1'       : (0.000, '13.3f'),
            'N_DEP'    : (0.00, '12.2f'),
            'N_INV'    : (0.00, '12.2f'),
            'H_TYPE'   : ('NONE', '>10s'),
            'EPS_1'    : (12.000, '13.3f'),
            'EPS_2'    : (12.000, '13.3f'),
            # Units
            'UNIT'     : ('SIU', '>10s'),
            'UNIK'     : ('SI', '>10s'),
            # Screening
            'SC_TYPE'  : ('NO', '>10s'),
            # Plasma type
            'PL_TYPE'  : ('OCP', '>10s'),
            'ZION'     : (1.000, '13.3f'),
            'ZION2'    : (0.000, '13.3f'),
            # Calculation type
            'CAL_TYPE' : ('QUANTUM', '>10s'),
            # DIELECTRIC FUNCTION
            'ESTDY'    : ('DYNAMIC', '>10s'),
            'EPS_T'    : ('LONG', '>10s'),
            'D_FUNC'   : ('NEV3', '>10s'),
            'I_T'      : (0, '>10d'),
            'NEV_TYPE' : ('STA2', '>10s'),
            'MEM_TYPE' : ('COCO', '>10s'),
            'ALPHA'    : (0.500, '13.3f'),
            'BETA'     : (0.600, '13.3f'),
            # Analytical plasmon dispersion
            'PL_DISP'  : ('RP2_MOD', '>10s'),
            # Local-field corrections
            'GSTDY'    : ('STATIC', '>10s'),
            'GQ_TYPE'  : ('ICUT', '>10s'),
            'IQ_TYPE'  : ('IKP', '>10s'),
            'LANDAU'   : ('NONE', '>10s'),
            'GQO_TYPE' : ('NONE', '>10s'),
            'G0_TYPE'  : ('EC', '>10s'),
            'GI_TYPE'  : ('EC', '>10s'),
            # Damping
            'DAMPING'  : ('RELA', '>10s'),    
            'LT_TYPE'  : ('NONE', '>10s'),
            'RT_TYPE'  : ('EX1', '>10s'),
            'DR_TYPE'  : ('NONE', '>10s'),
            'DC_TYPE'  : ('EXTE', '>10s'),
            'VI_TYPE'  : ('NONE', '>10s'),
            'EE_TYPE'  : ('NONE', '>10s'),
            'EP_TYPE'  : ('NONE', '>10s'),
            'EI_TYPE'  : ('NONE', '>10s'),
            'IP_TYPE'  : ('NONE', '>10s'),
            'PD_TYPE'  : ('NONE', '>10s'),
            'QD_TYPE'  : ('LORE', '>10s'),
            'ZETA'     : (1.250, '13.3f'),
            'D_VALUE_1': (0.500, '13.3f'),
            'POWER_1'  : ('FEMTO', '>10s'),
            'EK'       : (50.00, '12.2f'),
            'D_VALUE_2': (5.000, '13.3f'),
            'POWER_2'  : ('FEMTO', '>10s'),
            'PCT'      : (0.80, '12.2f'),
            # Electron-electron interaction
            'INT_POT'  : ('COULO', '>10s'),
            'S'        : (2.590, '13.3f'),
            'EPS'      : (470.000, '13.3f'),
            'DELTA'    : (1.500, '13.3f'),
            'RC'       : (1.500, '13.3f'),
            'ALF'      : (5.000, '13.3f'),
            'M'        : (7, '10d'),
            'N'        : (28, '10d'),         
            'A1'       : (1.000, '13.3f'),
            'A2'       : (1.000, '13.3f'),
            'A3'       : (1.000, '13.3f'),
            'A4'       : (1.000, '13.3f'),
            # Electron-phonon interaction
            'EP_C'     :  (1500.000, '13.3f'),
            'DEBYE_T'  :  (1500.000, '13.3f'), 
            'NA'       :  (12.000, '13.3f'),
            'MA'       :  (0.000, '13.3f'),
            'RA'       :  (0.000, '13.3f'),
            # Electron-impurity interaction
            'NI'       : (0.000, '13.3f'),
            'EI_C'     : (0.000, '13.3f'),
            # Classical fluid parameters
            'CF_TYPE'  : ('SHS', '>10s'),
            'PF_TYPE'  : ('HSM', '>10s'),
            'SL_TYPE'  : ('HSP', '>10s'), 
            # STRUCTURE FACTOR
            'SSTDY'    : ('DYNAMIC', '>10s'),
            'SQ_TYPE'  : ('PKA', '>10s'),
            'SQO_TYPE' : ('EPS', '>10s'),
            # PAIR CORRELATION FUNCTION
            'GR_TYPE'  : ('SHA', '>10s'),
            'GR0_MODE' : ('KIMB', '>10s'),
            # PAIR DISTRIBUTION FUNCTION
            'RH_TYPE'  : ('CEG', '>10s'),
            # SPECTRAL FUNCTION
            'SPF_TYPE' : ('NAIC', '>10s'),
            # ENERGY CALCULATIONS
            'EC_TYPE'  : ('GGSB_G', '>10s'),
            'FXC_TYPE' : ('NO', '>10s'),
            'EXC_TYPE' : ('NO', '>10s'),
            'EX_TYPE'  : ('HEG', '>10s'),
            'EK_TYPE'  : ('HEG', '>10s'),         
            # SPIN POLARIZATION
            'IMODE'    : (1, '10d'),
            'XI'       : (0.000, '13.3f'),
            # THERMODYNAMIC PROPERTIES
            'TH_PROP'  : ('QUAN', '>10s'),
            'GP_TYPE'  : ('IK0', '>10s'),
            # ELECTRON MEAN FREE PATH
            'EK_INI'   : (150.00, '12.2f'),
            'EK_FIN'   : (200.00, '12.2f'),
            # CALCULATIONS OF MOMENTS
            'N_M'      : (1, '10d'),
            'M_TYPE'   : ('SQO', '>10s'),
            # INCOMING ION BEAM
            'Z_BEAM'   : (1.00, '12.2f'),
            'EK_BEAM'  : (15000.00, '12.2f'),
            # OUTPUT CALCULATIONS/PRINTING
            'I_DF'     : (1, '10d'),
            'I_PZ'     : (0, '10d'),
            'I_SU'     : (0, '10d'),
            'I_CD'     : (0, '10d'),
            'I_PD'     : (1, '10d'),
            'I_EH'     : (1, '10d'),
            'I_E2'     : (0, '10d'),
            'I_CK'     : (0, '10d'),
            'I_CR'     : (0, '10d'),
            'I_PK'     : (0, '10d'),
            'I_LF'     : (0, '10d'),
            'I_IQ'     : (0, '10d'),
            'I_SF'     : (0, '10d'),
            'I_PC'     : (0, '10d'),
            'I_P2'     : (0, '10d'),
            'I_VX'     : (0, '10d'),
            'I_DC'     : (0, '10d'),
            'I_MD'     : (0, '10d'),
            'I_LD'     : (0, '10d'),
            'I_DP'     : (0, '10d'),
            'I_LT'     : (0, '10d'),
            'I_BR'     : (0, '10d'),
            'I_PE'     : (0, '10d'),
            'I_QC'     : (0, '10d'),
            'I_RL'     : (0, '10d'),
            'I_KS'     : (0, '10d'),
            'I_OQ'     : (0, '10d'),
            'I_ME'     : (0, '10d'),
            'I_MS'     : (0, '10d'),
            'I_ML'     : (0, '10d'),
            'I_MC'     : (0, '10d'),
            'I_DE'     : (0, '10d'),
            'I_ZE'     : (0, '10d'),
            'I_SR'     : (0, '10d'),
            'I_CW'     : (0, '10d'),
            'I_CF'     : (0, '10d'),
            'I_EM'     : (0, '10d'),
            'I_MF'     : (0, '10d'),
            'I_SP'     : (0, '10d'),
            'I_SE'     : (0, '10d'),
            'I_SB'     : (0, '10d'),
            'I_ES'     : (0, '10d'),
            'I_GR'     : (0, '10d'),
            'I_FD'     : (0, '10d'),
            'I_BE'     : (0, '10d'),
            'I_MX'     : (0, '10d'),
            'I_SC'     : (0, '10d'),
            'I_DS'     : (0, '10d'),
            'I_NV'     : (0, '10d'),
            'I_MT'     : (0, '10d'),
            'I_GP'     : (0, '10d'),
            'I_PR'     : (0, '10d'),
            'I_CO'     : (0, '10d'),
            'I_CP'     : (0, '10d'),
            'I_BM'     : (0, '10d'),
            'I_SH'     : (0, '10d'),
            'I_S0'     : (0, '10d'),
            'I_S1'     : (0, '10d'),
            'I_DT'     : (0, '10d'),
            'I_PS'     : (0, '10d'),
            'I_IE'     : (0, '10d'),
            'I_EI'     : (0, '10d'),
            'I_FH'     : (0, '10d'),
            'I_EY'     : (0, '10d'),
            'I_EF'     : (1, '10d'),
            'I_KF'     : (1, '10d'),
            'I_VF'     : (1, '10d'),
            'I_TE'     : (1, '10d'),
            'I_DL'     : (1, '10d'),
            'I_TW'     : (0, '10d'),
            'I_VT'     : (0, '10d'),
            'I_TC'     : (0, '10d'),
            'I_EG'     : (0, '10d'),
            'I_EX'     : (0, '10d'),
            'I_XC'     : (0, '10d'),
            'I_EC'     : (0, '10d'),
            'I_HF'     : (0, '10d'),
            'I_EK'     : (0, '10d'),
            'I_EP'     : (0, '10d'),
            'I_VI'     : (0, '10d'),
            'I_DI'     : (0, '10d'),
            'I_FP'     : (0, '10d'),
            'I_EL'     : (0, '10d'),
            'I_PO'     : (0, '10d'),
            'I_RF'     : (0, '10d'),
            'I_VC'     : (0, '10d'),
            'I_FN'     : (0, '10d'),
            'I_WR'     : (2, '10d'),
            'I_TI'     : (0, '10d'),
            }

    def __init__(self, filename, **parameters):
        self.filename = filename
        self.parameters = self.defaults
        for key, value in parameters.items():
            fmt = self.parameters[key][1]
            self.parameters.update({key:(value, fmt)})


    def _stack(self, *keys, comments=""):
        params = []
        for key in keys:
            value, fmt = self.parameters[key]
            params.append((key, value, fmt))
        line = [' ',] * 49
        labels = []
        for i, (label, value, fmt) in enumerate(params):
            labels.append(label)
            fmts = ' ' * (10 * i) + '{{:{}}}'.format(fmt)
            line_ = fmts.format(value)
            for ichar, char in enumerate(line_):
                if char != ' ':
                    line[ichar] = char

        # add labels
        labels = list(','.join(labels))
        line += labels

        # Pad with spaces if needed
        line += ' ' * max(78 - len(line), 0)

        # Add stars
        line[1] = '*'
        line += '*'

        # Add comments
        line += comments

        # Add CR
        line += '\n'
        
        return ''.join(line)
    

    def write(self):
        content  = ""
        content += " ******************************************************************************\n"
        content += " *                     MsSpec DIELECTRIC FUNCTION MODULE                      *\n"
        content += " ******************************************************************************\n"
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += " *                           GENERAL PARAMETERS :                             *\n"
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += " *                        (q,omega,r)  :                                      *\n"
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('Q_MIN', 'Q_MAX', 'N_Q', comments=" in units of k_F")
        content += self._stack('E_MIN', 'E_MAX', 'N_E', comments=" in units of E_F")
        content += self._stack('R_MIN', 'R_MAX', 'N_R', comments=" in units of 1/k_F")
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += " *                        Material's properties :                             *\n"
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('RS', 'MSOM', 'MAT_TYP', 'EPS_B')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += " *                        External fields :                                   *\n"
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('T', 'E', 'H', 'FLD')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += " *                        System's dimension :                                *\n"
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('DIM')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += " *                        Confinement :                                       *\n"
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('R0', 'L', 'OM0', 'CONFIN')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += " *                        Multilayer structure :                              *\n"
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('DL', 'D1', 'N_DEP', 'N_INV', comments="   --- EPS_1 ---")
        content += self._stack('H_TYPE', 'EPS_1', 'EPS_2',   comments="       EPS_2")
        content += " *-------+---------+---------+---------+---------+----------------------------*   --- EPS_1 ---\n"
        content += " *                        Units :                                             *\n"
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('UNIT', 'UNIK')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += " *                        Screening :                                         *\n"
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('SC_TYPE')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += " *                        Plasma type :                                       *\n"
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('PL_TYPE', 'ZION', 'ZION2')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += " *                        Calculation type :                                  *\n"
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('CAL_TYPE')
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += " *                        DIELECTRIC FUNCTION :                               *\n"
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += self._stack('ESTDY', 'EPS_T', 'D_FUNC', 'I_T')       
        content += self._stack('NEV_TYPE', 'MEM_TYPE', 'ALPHA', 'BETA')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += " *                        Analytical plasmon dispersion :                     *\n"
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('PL_DISP')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += " *                        Local-field corrections                             *\n"
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('GSTDY', 'GQ_TYPE', 'IQ_TYPE')
        content += self._stack('LANDAU', 'GQO_TYPE', 'G0_TYPE', 'GI_TYPE')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += " *                        Damping :                                           *\n"
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('DAMPING', 'LT_TYPE', 'RT_TYPE')
        content += self._stack('DR_TYPE', 'DC_TYPE', 'VI_TYPE')
        content += self._stack('EE_TYPE', 'EP_TYPE', 'EI_TYPE')
        content += self._stack('IP_TYPE', 'PD_TYPE', 'QD_TYPE', 'ZETA')
        content += self._stack('D_VALUE_1', 'POWER_1', 'EK')
        content += self._stack('D_VALUE_2', 'POWER_2', 'PCT')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += " *                        Electron-electron interaction :                     *\n"
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('INT_POT','S','EPS','DELTA')
        content += self._stack('RC','ALF','M','N')
        content += self._stack('A1','A2','A3','A4')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += " *                        Electron-phonon interaction :                       *\n"
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('EP_C','DEBYE_T')
        content += self._stack('NA','MA','RA')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += " *                        Electron-impurity interaction :                     *\n"
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('NI','EI_C')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += " *                        Classical fluid parameters :                        *\n"
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('CF_TYPE','PF_TYPE','SL_TYPE')
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += " *                        STRUCTURE FACTOR :                                  *\n"
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += self._stack('SSTDY','SQ_TYPE','SQO_TYPE')
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += " *                        PAIR CORRELATION FUNCTION :                         *\n"
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += self._stack('GR_TYPE','GR0_MODE')
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += " *                        PAIR DISTRIBUTION FUNCTION :                        *\n"
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += self._stack('RH_TYPE')
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += " *                        SPECTRAL FUNCTION :                                 *\n"
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += self._stack('SPF_TYPE')
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += " *                        ENERGY CALCULATIONS :                               *\n"
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += self._stack('EC_TYPE','FXC_TYPE','EXC_TYPE')
        content += self._stack('EX_TYPE','EK_TYPE')
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += " *                        SPIN POLARIZATION :                                 *\n"
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += self._stack('IMODE','XI')
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += " *                        THERMODYNAMIC PROPERTIES :                          *\n"
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += self._stack('TH_PROP','GP_TYPE')
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += " *                        ELECTRON MEAN FREE PATH :                           *\n"
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += self._stack('EK_INI','EK_FIN')
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += " *                        CALCULATION OF MOMENTS :                            *\n"
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += self._stack('N_M','M_TYPE')
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += " *                        INCOMING ION BEAM :                                 *\n"
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += self._stack('Z_BEAM', 'EK_BEAM')
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += " *                        OUTPUT CALCULATIONS/PRINTING :                      *\n"
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += self._stack('I_DF','I_PZ','I_SU','I_CD')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('I_PD','I_EH','I_E2','I_CK')
        content += self._stack('I_CR','I_PK')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('I_LF','I_IQ','I_SF','I_PC')
        content += self._stack('I_P2','I_VX','I_DC','I_MD')
        content += self._stack('I_LD','I_DP','I_LT','I_BR')
        content += self._stack('I_PE','I_QC','I_RL','I_KS')
        content += self._stack('I_OQ','I_ME','I_MS','I_ML')
        content += self._stack('I_MC','I_DE','I_ZE','I_SR')
        content += self._stack('I_CW','I_CF','I_EM','I_MF')
        content += self._stack('I_SP','I_SE','I_SB','I_ES')
        content += self._stack('I_GR','I_FD','I_BE','I_MX')
        content += self._stack('I_SC','I_DS','I_NV','I_MT')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('I_GP','I_PR','I_CO','I_CP')
        content += self._stack('I_BM','I_SH','I_S0','I_S1')
        content += self._stack('I_DT','I_PS','I_IE','I_EI')
        content += self._stack('I_FH','I_EY')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('I_EF','I_KF','I_VF','I_TE')
        content += self._stack('I_DL')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('I_TW','I_VT','I_TC')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('I_EG','I_EX','I_XC','I_EC')
        content += self._stack('I_HF','I_EK','I_EP')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('I_VI','I_DI')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('I_FP','I_EL','I_PO','I_RF')
        content += self._stack('I_VC')
        content += " *-------+---------+---------+---------+---------+----------------------------*\n"
        content += self._stack('I_FN','I_WR','I_TI')
        content += " *=======+=========+=========+=========+=========+============================*\n"
        content += " *                                 INPUT FILES :                              *\n"
        content += " *----------------------------------------------------------------------------*\n"
        content += " *           NAME                    UNIT                TYPE                 *\n"
        content += " *=======+======================+======+=========+============================*\n"
        content += " *       epsilon.dat                   5         INPUT DATA FILE              *\n"
        content += " *=======+======================+======+=========+============================*\n"
        content += " *                                OUTPUT FILES :                              *\n"
        content += " *----------------------------------------------------------------------------*\n"
        content += " *           NAME                    UNIT                TYPE                 *\n"
        content += " *=======+======================+======+=========+============================*\n"
        content += " *       epsilon.lis                   6         CHECK FILE                   *\n"
        content += " *=======+======================+======+=========+============================*\n"
        content += " *                             END OF THE DATA FILE                           *\n"
        content += " *============================================================================*\n"
        content += " ******************************************************************************\n"

        abs_path = os.path.abspath(self.filename)
        dir_path = os.path.dirname(abs_path)
        os.makedirs(dir_path, exist_ok=True)
        with open(self.filename, 'w') as fd:
            fd.write(content)
