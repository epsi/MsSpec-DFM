.SH INPUT FILE FORMAT

The input file is formatted as follows:

 ******************************************************************************
 *                     MsSpec DIELECTRIC FUNCTION MODULE                      *
 ******************************************************************************
 *=======+=========+=========+=========+=========+============================*
 *                           GENERAL PARAMETERS :                             *
 *=======+=========+=========+=========+=========+============================*
 *                        (q,omega,r)  :                                      *
 *-------+---------+---------+---------+---------+----------------------------*
 *      0.010     4.000   1000                   Q_MIN,Q_MAX,N_Q              * in units of k_F
 *      0.010     4.000   2000                   E_MIN,E_MAX,N_E              * in units of E_F
 *      0.010     4.000   2000                   R_MIN,R_MAX,N_R              * in units of 1/k_F
 *-------+---------+---------+---------+---------+----------------------------*
 *                        Material's properties :                             *
 *-------+---------+---------+---------+---------+----------------------------*
 *      2.079     1.000  SCHRO        1.000      RS,MSOM,MAT_TYP,EPS_B        *
 *-------+---------+---------+---------+---------+----------------------------*
 *                        External fields :                                   *
 *-------+---------+---------+---------+---------+----------------------------*
 *      1.00      0.000     0.000     NO         T,E,H,FLD                    *
 *-------+---------+---------+---------+---------+----------------------------*
 *                        System's dimension :                                *
 *-------+---------+---------+---------+---------+----------------------------*
 *      3D                                       DIM                          *
 *-------+---------+---------+---------+---------+----------------------------*
 *                        Confinement :                                       *
 *-------+---------+---------+---------+---------+----------------------------*
 *      0.000     0.000     0.00 NO-CONF         R0,L,OM0,CONFIN              *
 *-------+---------+---------+---------+---------+----------------------------*
 *                        Multilayer structure :                              *
 *-------+---------+---------+---------+---------+----------------------------*
 *      0.000     0.000     0.00      0.00       DL,D1,N_DEP,N_INV            *   --- EPS_1 ---
 *    NONE       12.000    12.000                H_TYPE,EPS_1,EPS_2           *       EPS_2
 *-------+---------+---------+---------+---------+----------------------------*   --- EPS_1 ---
 *                        Units :                                             *
 *-------+---------+---------+---------+---------+----------------------------*
 *     SIU        SI                             UNIT,UNIK                    *
 *-------+---------+---------+---------+---------+----------------------------*
 *                        Screening :                                         *
 *-------+---------+---------+---------+---------+----------------------------*
 *      NO                                       SC_TYPE                      *
 *-------+---------+---------+---------+---------+----------------------------*
 *                        Plasma type :                                       *
 *-------+---------+---------+---------+---------+----------------------------*
 *     OCP        1.000     0.000                PL_TYPE,ZION,ZION2           *
 *-------+---------+---------+---------+---------+----------------------------*
 *                        Calculation type :                                  *
 *-------+---------+---------+---------+---------+----------------------------*
 * QUANTUM                                       CAL_TYPE                     *
 *=======+=========+=========+=========+=========+============================*
 *                        DIELECTRIC FUNCTION :                               *
 *=======+=========+=========+=========+=========+============================*
 * DYNAMIC      LONG      NEV3         0         ESTDY,EPS_T,D_FUNC,I_T       *
 *    STA2      COCO        0.500     0.600      NEV_TYPE,MEM_TYPE,ALPHA,BETA *
 *-------+---------+---------+---------+---------+----------------------------*
 *                        Analytical plasmon dispersion :                     *
 *-------+---------+---------+---------+---------+----------------------------*
 * RP2_MOD                                       PL_DISP                      *
 *-------+---------+---------+---------+---------+----------------------------*
 *                        Local-field corrections                             *
 *-------+---------+---------+---------+---------+----------------------------*
 *  STATIC      ICUT       IKP                   GSTDY,GQ_TYPE,IQ_TYPE        *
 *    NONE      NONE        EC        EC         LANDAU,GQO_TYPE,G0_TYPE,GI_TYPE*
 *-------+---------+---------+---------+---------+----------------------------*
 *                        Damping :                                           *
 *-------+---------+---------+---------+---------+----------------------------*
 *    RELA      NONE       EX1                   DAMPING,LT_TYPE,RT_TYPE      *
 *    NONE      EXTE      NONE                   DR_TYPE,DC_TYPE,VI_TYPE      *
 *    NONE      NONE      NONE                   EE_TYPE,EP_TYPE,EI_TYPE      *
 *    NONE      NONE      LORE        1.250      IP_TYPE,PD_TYPE,QD_TYPE,ZETA *
 *      0.500  FEMTO       50.00                 D_VALUE_1,POWER_1,EK         *
 *      5.000  FEMTO        0.80                 D_VALUE_2,POWER_2,PCT        *
 *-------+---------+---------+---------+---------+----------------------------*
 *                        Electron-electron interaction :                     *
 *-------+---------+---------+---------+---------+----------------------------*
 *   COULO        2.590   470.000     1.500      INT_POT,S,EPS,DELTA          *
 *      1.500     5.000      7        28         RC,ALF,M,N                   *
 *      1.000     1.000     1.000     1.000      A1,A2,A3,A4                  *
 *-------+---------+---------+---------+---------+----------------------------*
 *                        Electron-phonon interaction :                       *
 *-------+---------+---------+---------+---------+----------------------------*
 *   1500.000  1500.000                          EP_C,DEBYE_T                 *
 *     12.000     0.000     0.000                NA,MA,RA                     *
 *-------+---------+---------+---------+---------+----------------------------*
 *                        Electron-impurity interaction :                     *
 *-------+---------+---------+---------+---------+----------------------------*
 *      0.000     0.000                          NI,EI_C                      *
 *-------+---------+---------+---------+---------+----------------------------*
 *                        Classical fluid parameters :                        *
 *-------+---------+---------+---------+---------+----------------------------*
 *     SHS       HSM       HSP                   CF_TYPE,PF_TYPE,SL_TYPE      *
 *=======+=========+=========+=========+=========+============================*
 *                        STRUCTURE FACTOR :                                  *
 *=======+=========+=========+=========+=========+============================*
 * DYNAMIC       PKA       EPS                   SSTDY,SQ_TYPE,SQO_TYPE       *
 *=======+=========+=========+=========+=========+============================*
 *                        PAIR CORRELATION FUNCTION :                         *
 *=======+=========+=========+=========+=========+============================*
 *     SHA      KIMB                             GR_TYPE,GR0_MODE             *
 *=======+=========+=========+=========+=========+============================*
 *                        PAIR DISTRIBUTION FUNCTION :                        *
 *=======+=========+=========+=========+=========+============================*
 *     CEG                                       RH_TYPE                      *
 *=======+=========+=========+=========+=========+============================*
 *                        SPECTRAL FUNCTION :                                 *
 *=======+=========+=========+=========+=========+============================*
 *    NAIC                                       SPF_TYPE                     *
 *=======+=========+=========+=========+=========+============================*
 *                        ENERGY CALCULATIONS :                               *
 *=======+=========+=========+=========+=========+============================*
 *  GGSB_G        NO        NO                   EC_TYPE,FXC_TYPE,EXC_TYPE    *
 *     HEG       HEG                             EX_TYPE,EK_TYPE              *
 *=======+=========+=========+=========+=========+============================*
 *                        SPIN POLARIZATION :                                 *
 *=======+=========+=========+=========+=========+============================*
 *       1        0.000                          IMODE,XI                     *
 *=======+=========+=========+=========+=========+============================*
 *                        THERMODYNAMIC PROPERTIES :                          *
 *=======+=========+=========+=========+=========+============================*
 *    QUAN       IK0                             TH_PROP,GP_TYPE              *
 *=======+=========+=========+=========+=========+============================*
 *                        ELECTRON MEAN FREE PATH :                           *
 *=======+=========+=========+=========+=========+============================*
 *    150.00    200.00                           EK_INI,EK_FIN                *
 *=======+=========+=========+=========+=========+============================*
 *                        CALCULATION OF MOMENTS :                            *
 *=======+=========+=========+=========+=========+============================*
 *       1       SQO                             N_M,M_TYPE                   *
 *=======+=========+=========+=========+=========+============================*
 *                        INCOMING ION BEAM :                                 *
 *=======+=========+=========+=========+=========+============================*
 *      1.00  15000.00                            Z_BEAM,EK_BEAM              *
 *=======+=========+=========+=========+=========+============================*
 *                        OUTPUT CALCULATIONS/PRINTING :                      *
 *=======+=========+=========+=========+=========+============================*
 *       1         0         0         0         I_DF,I_PZ,I_SU,I_CD          *
 *-------+---------+---------+---------+---------+----------------------------*
 *       1         1         0         0         I_PD,I_EH,I_E2,I_CK          *
 *       0         0                             I_CR,I_PK                    *
 *-------+---------+---------+---------+---------+----------------------------*
 *       0         0         0         0         I_LF,I_IQ,I_SF,I_PC          *
 *       0         0         0         0         I_P2,I_VX,I_DC,I_MD          *
 *       0         0         0         0         I_LD,I_DP,I_LT,I_BR          *
 *       0         0         0         0         I_PE,I_QC,I_RL,I_KS          *
 *       0         0         0         0         I_OQ,I_ME,I_MS,I_ML          *
 *       0         0         0         0         I_MC,I_DE,I_ZE,I_SR          *
 *       0         0         0         0         I_CW,I_CF,I_EM,I_MF          *
 *       0         0         0         0         I_SP,I_SE,I_SB,I_ES          *
 *       0         0         0         0         I_GR,I_FD,I_BE,I_MX          *
 *       0         0         0         0         I_SC,I_DS,I_NV,I_MT          *
 *-------+---------+---------+---------+---------+----------------------------*
 *       0         0         0         0         I_GP,I_PR,I_CO,I_CP          *
 *       0         0         0         0         I_BM,I_SH,I_S0,I_S1          *
 *       0         0         0         0         I_DT,I_PS,I_IE,I_EI          *
 *       0         0                             I_FH,I_EY                    *
 *-------+---------+---------+---------+---------+----------------------------*
 *       1         1         1         1         I_EF,I_KF,I_VF,I_TE          *
 *       1                                       I_DL                         *
 *-------+---------+---------+---------+---------+----------------------------*
 *       0         0         0                   I_TW,I_VT,I_TC               *
 *-------+---------+---------+---------+---------+----------------------------*
 *       0         0         0         0         I_EG,I_EX,I_XC,I_EC          *
 *       0         0         0                   I_HF,I_EK,I_EP               *
 *-------+---------+---------+---------+---------+----------------------------*
 *       0         0                             I_VI,I_DI                    *
 *-------+---------+---------+---------+---------+----------------------------*
 *       0         0         0         0         I_FP,I_EL,I_PO,I_RF          *
 *       0                                       I_VC                         *
 *-------+---------+---------+---------+---------+----------------------------*
 *       0         2         0                   I_FN,I_WR,I_TI               *
 *=======+=========+=========+=========+=========+============================*
 *                                 INPUT FILES :                              *
 *----------------------------------------------------------------------------*
 *           NAME                    UNIT                TYPE                 *
 *=======+======================+======+=========+============================*
 *       epsilon.dat                   5         INPUT DATA FILE              *
 *=======+======================+======+=========+============================*
 *                                OUTPUT FILES :                              *
 *----------------------------------------------------------------------------*
 *           NAME                    UNIT                TYPE                 *
 *=======+======================+======+=========+============================*
 *       epsilon.lis                   6         CHECK FILE                   *
 *=======+======================+======+=========+============================*
 *                             END OF THE DATA FILE                           *
 *============================================================================*
 ******************************************************************************


.SH DESCRIPTION OF THE CALCULATION PARAMETERS

 *=======+=========+=========+=========+=========+===========================*
 *                           GENERAL PARAMETERS :                            *
 *=======+=========+=========+=========+=========+===========================*

       * Q_MIN    : minimum value of q (in units of k_F)
       * Q_MAX    : maximum value of q (in units of k_F)
       * N_Q      : number of q-points

       * E_MIN    : minimum value of energy (in units of E_F)
       * E_MAX    : maximum value of energy (in units of E_F)
       * N_E      : number of E-points

       * R_MIN    : minimum value of distance r (in units of 1/k_F)
       * R_MAX    : maximum value of distance r (in units of 1/k_F))
       * N_R      : number of r-points

 *-------+---------+---------+---------+---------+---------------------------*

       * RS       : average distance between 2 electrons (in units of a0)
                                            ~
       * MSOM     : m*/m (for semiconductors)
                                            ~
       * MAT_TYP  : type of material
                       MAT_TYPE  = 'SCHRO' standard solid
                       MAT_TYPE  = 'DIRAC' massless Fermions
                       MAT_TYPE  = 'NEUTR' neutral classical liquid
                       MAT_TYPE  = 'POLAR' polar classical liquid
                                            ~
       * EPS_B    : material's dielectric constant

 *-------+---------+---------+---------+---------+---------------------------*

       * T        : temperature (in SI)
                                            ~
       * E        : external electric field (in SI)
                                            ~
       * H        : external magnetic field (in SI)
                                            ~
       * FLD      : strength of the magnetic field
                       FLD = 'NO' no field
                       FLD = 'WF' weak field
                       FLD = 'IF' intermediate field
                       FLD = 'LF' large field

 *-------+---------+---------+---------+---------+---------------------------*

       * DIM      : dimension of the system
                       DIM  = '3D'
                       DIM  = '2D'
                       DIM  = '1D'
                       DIM  = 'Q0'  quasi-0D
                       DIM  = 'Q1'  quasi-1D
                       DIM  = 'Q2'  quasi-2D
                       DIM  = 'BL'  bilayer
                       DIM  = 'ML'  multilayer
                       DIM  = 'ML'  multilayer

                                            ~

       * R0       : wire radius
                                            ~
       * L        : length of quantum well
                                            ~
       * OM0      : frequency of the confinement potential (SI)
                                            ~
       * CONFIN   : type of confinement
                       CONFIN = 'NO-CONF' no confinement
                       CONFIN = 'DSEPLAY' layer within a stacking of layers
                       CONFIN = 'CC-1111' cylindrical within subband 1
                       CONFIN = 'CC-1122' cylindrical between subbands 1 and 2
                       CONFIN = 'CC-1221' cylindrical between subbands 1 and 2
                       CONFIN = 'CC-2222' cylindrical within subband 2
                       CONFIN = 'HC-1111' harmonic within subband 1
                       CONFIN = 'HC-1122' harmonic between subbands 1 and 2
                       CONFIN = 'HC-1221' harmonic between subbands 1 and 2
                       CONFIN = 'HC-2222' harmonic within subband 2
                       CONFIN = 'INVLAYE' inversion layer in semiconductor
                       CONFIN = 'IQWE_LB' square well with an infinite barrier
                       CONFIN = 'PC1_QWI' parabolic
                       CONFIN = 'PC2_QWI' parabolic
                       CONFIN = 'SOFTCOR' soft-core potential
                       CONFIN = 'SWC_QWI' square well with an infinite barrier

                                            ~

       * DL       : interlayer distance
                                            ~
       * D1       : distance between the two layers in the unit cell
                                            ~
       * N_DEP    : electron concentration in depletion layer (SI)
                                            ~
       * N_INV    : electron concentration in inversion layer (SI)

                                            ~

       * H_TYPE   : heterostructure type
                       H_TYPE  =  'SSL1' semiconductor superlattice of type I
                       H_TYPE  =  'SSL2' semiconductor superlattice of type II
                       H_TYPE  =  'BILA' bilayer
                       H_TYPE  =  'MLA1' multilayer with with one layer  / unit cell
                       H_TYPE  =  'MLA2' multilayer with with two layers / unit cell
                                            ~
       * EPS_1    : background/layer dielectric constant
                                            ~
       * EPS_2    : interlayer dielectric constant

 *-------+---------+---------+---------+---------+---------------------------*

       * UNIT     : system unit
                       UNIT  = 'SIU' international system
                       UNIT  = 'CGS' CGS system
                       UNIT  = 'ATU' atomic units

       * UNIK     : K unit
                       UNIK  = 'SI' international system
                       UNIK  = 'AU' atomic units

 *-------+---------+---------+---------+---------+---------------------------*

       * SC_TYPE  : type of screeening
                       SC_TYPE  = 'NO' no screening
                       SC_TYPE  = 'DH' Debye-Hückel
                       SC_TYPE  = 'IS' Tago-Utsumi-Ichimaru
                       SC_TYPE  = 'KL' Kleinman
                       SC_TYPE  = 'OC' one-component plasma
                       SC_TYPE  = 'RP' RPA
                       SC_TYPE  = 'ST' Streitenberger
                       SC_TYPE  = 'TF' Thomas-Fermi
                       SC_TYPE  = 'UI' Utsumi-Ichimaru
                       SC_TYPE  = 'YT' Yasuhara-Takada

 *-------+---------+---------+---------+---------+---------------------------*

       * PL_TYPE  : type of plasma considered
                      PL_TYPE  = 'OCP' --> one-component plasma (~ electron gas)
                      PL_TYPE  = 'DCP' --> two-component plasma

       * ZION     : atomic number of (first) ion

       * ZION2    : atomic number of second ion

 *-------+---------+---------+---------+---------+---------------------------*

       * CAL_TYPE : type of calculation
                      CAL_TYPE  = 'QUANTUM' --> quantum fluid
                      CAL_TYPE  = 'CLASSIC' --> classical fluid

 *=======+=========+=========+=========+=========+===========================*
 *                        DIELECTRIC FUNCTION :                              *
 *=======+=========+=========+=========+=========+===========================*


       * ESTDY    : static vs dynamic dielectric function
                      STDY = ' STATIC'
                      STDY = 'DYNAMIC'

       * EPS_T    : type of dielcectric function
                       EPS_T  = 'LONG' longitudinal --> D_FUNCL
                       EPS_T  = 'TRAN' transverse   --> D_FUNCT

       * D_FUNC   : model of dielectric function :

                    1) Static:

                       ---> longitudinal:

                      D_FUNCL = 'LRPA' random phase approximation          (3D,2D,1D)
                      D_FUNCL = 'THFE' Thomas-Fermi approximation          (3D,2D,1D)

                    2) Dynamic:

                       ---> transverse:

                      D_FUNCT = 'RPA1' random phase approximation          (3D,2D)
                      D_FUNCT = 'RPA2' random phase approximation          (3D)
                      D_FUNCT = 'LVLA' linearized Vlasov                   (3D)
                      D_FUNCT = 'MER1' Mermin                              (3D)
                      D_FUNCT = 'BLTZ' Boltzmann                           (3D)

                       ---> longitudinal:

                      D_FUNCL = 'ARBR' Arista-Brandt 1                     <-- T-dependent
                      D_FUNCL = 'ATAS' Atwal-Ashcroft                      <-- T-dependent
                      D_FUNCL = 'BLZ1' Boltzmann
                      D_FUNCL = 'BLZ2' damped Boltzmann
                      D_FUNCL = 'DACA' Arista-Brandt 2                     <-- T-dependent
                      D_FUNCL = 'GOTZ' Götze memory function
                      D_FUNCL = 'HEAP' Hertel-Appel
                      D_FUNCL = 'HAFO' Hartree-Fock
                      D_FUNCL = 'HUCO' Hu-O'Connell                        <-- damping (3D,2D,Q1)
                      D_FUNCL = 'HYDR' hydrodynamic                        <-- damping
                      D_FUNCL = 'KLEI' Kleinman                            <-- T-dependent
                      D_FUNCL = 'KLKD' Klimontovich-Kraeft                 <-- T-dependent
                      D_FUNCL = 'KLKN' Klimontovich-Kraeft                 <-- T-dependent
                      D_FUNCL = 'LAND' Landau parameters-based
                      D_FUNCL = 'LVL1' linearized Vlasov (weak coupling)   <-- T-dependent
                      D_FUNCL = 'LVL2' linearized Vlasov (strong coupling) <-- T-dependent
                      D_FUNCL = 'MEM2' Two-moment memory function          <-- T-dependent
                      D_FUNCL = 'MEM3' Three-moment memory function        <-- T-dependent
                      D_FUNCL = 'MEM4' Four-moment memory function         <-- T-dependent
                      D_FUNCL = 'MER1' Mermin 1                            <-- damping (3D,2D)
                      D_FUNCL = 'MER2' Lindhard-Mermin                     <-- T-dependent
                      D_FUNCL = 'MER+' Mermin with Local Field Corrections <-- damping
                      D_FUNCL = 'MSAP' mean spherical approximation
                      D_FUNCL = 'NEV2' Two-moment Nevanlinna               <-- T-dependent
                      D_FUNCL = 'NEV3' Three-moment Nevanlinna             <-- T-dependent
                      D_FUNCL = 'NEV4' Four-moment Nevanlinna              <-- T-dependent
                      D_FUNCL = 'PLPO' plasmon pole
                      D_FUNCL = 'RDF1' Altshuler et al                     <-- damping
                      D_FUNCL = 'RDF2' Altshuler et al                     <-- damping
                      D_FUNCL = 'RPA1' RPA
                      D_FUNCL = 'RPA2' RPA                                 <-- T-dependent
                      D_FUNCL = 'RPA+' RPA + static local field corrections
                      D_FUNCL = 'RPA3' random phase approximation          <-- magnetic field (2D)
                      D_FUNCL = 'SO2E' computed from S(q,omega)
                      D_FUNCL = 'UTIC' Utsumi-Ichimaru                     <-- T-dependent
                      D_FUNCL = 'VLFP' Vlasov-Fokker-Planck                <-- damping

       * I_T      :  way to incorporate temperature dependence
                      I_T  = 0  --> no temperature (T = 0)
                      I_T  = 1  --> analytical models used
                      I_T  = 2  --> temperature convolution

                                            ~

       * NEV_TYPE : type of Nevalinna function used (only for D_FUNCL = 'NEVn')
                      NEV_TYPE  = 'NONE' --> no function
                      NEV_TYPE  = 'RELA' --> static value h(q) = i / tau
                      NEV_TYPE  = 'STA1' --> static value h(q)
                      NEV_TYPE  = 'STA2' --> static value h(q)
                      NEV_TYPE  = 'STA3' --> static value h(q)
                      NEV_TYPE  = 'STA4' --> static value h(q)
                      NEV_TYPE  = 'PEEL' --> Perel'-Eliashberg function
                      NEV_TYPE  = 'PE76' --> Perel'-Eliashberg by Arkhipov et al
                      NEV_TYPE  = 'CCP1' -->
                      NEV_TYPE  = 'CCP2' -->
                      NEV_TYPE  = 'CCP3' -->
                      NEV_TYPE  = 'CCP4' -->
                      NEV_TYPE  = 'PST1' -->

       * MEM_TYPE : type of memory function used (only for D_FUNCL = 'MEMn')
                      MEM_TYPE  = 'NONE' --> no function
                      MEM_TYPE  = 'DELT' --> delta function
                      MEM_TYPE  = 'DGAU' --> double Gaussian functions
                      MEM_TYPE  = 'EXPO' --> exponential function
                      MEM_TYPE  = 'GAUS' --> Gaussian function
                      MEM_TYPE  = 'LORE' --> Lorentzian function
                      MEM_TYPE  = 'SINC' --> sinc function
                      MEM_TYPE  = 'BES0' --> J_0(t) function
                      MEM_TYPE  = 'BES1' --> J_1(t)/t function
                      MEM_TYPE  = 'SEC2' --> sech^2(t) function
                      MEM_TYPE  = 'COCO' --> Cole-Cole function
                      MEM_TYPE  = 'CODA' --> Cole-Davidson function
                      MEM_TYPE  = 'HANE' --> Habriliak-Negami function
                      MEM_TYPE  = 'RAYI' --> Raganathan-Yip function
                      MEM_TYPE  = 'LIHY' --> linearized hydrodynamic function

       * ALPHA    : value of the Habriliak-Negami first parameter  (in ]0,1])

       * BETA     : value of the Habriliak-Negami second parameter (in ]0,1])

 *-------+---------+---------+---------+---------+---------------------------*

       * PL_DISP   : method used to compute the plasmon dispersion (3D real case)
                       PL_DISP = 'HYDRODY' hydrodynamic model
                       PL_DISP = 'RP1_MOD' RPA model up to q^2
                       PL_DISP = 'RP2_MOD' RPA model up to q^4
                       PL_DISP = 'GOA_MOD' Gorobchenko model
                       PL_DISP = 'HER_APP' Hertel-Appel model     <-- T-dependent
                       PL_DISP = 'HUBBARD' Hubbard model
                       PL_DISP = 'ELASTIC' elastic model
                       PL_DISP = '  EXACT' computed from the dielectric function
                       PL_DISP = 'SGBBN_M' SGBBN model
                       PL_DISP = 'AL0_MOD' gamma_0   limit
                       PL_DISP = 'ALI_MOD' gamma_inf limit
                       PL_DISP = 'NOP_MOD' Nozières-Pines model
                       PL_DISP = 'UTI_MOD' Utsumi-Ichimaru model
                       PL_DISP = 'TWA_MOD' Toigo-Woodruff model
                       PL_DISP = 'SUM_RU2' f-sum_rule
                       PL_DISP = 'SUM_RU3' 3rd-frequency sum_rule
       * PL_DISP   : method used to compute the plasmon dispersion (2D real case)
                       PL_DISP = 'HYDRODY' hydrodynamic model
                       PL_DISP = 'RPA_MOD' RPA model
                       PL_DISP = 'RAJAGOP' Rajagopal formula
       * PL_DISP   : method used to compute the plasmon dispersion (1D real case)
                       PL_DISP = 'HYDRODY' hydrodynamic model
                       PL_DISP = 'RPA_MOD' RPA model

 *-------+---------+---------+---------+---------+---------------------------*

       * GSTDY    : static vs dynamic local-field corrections
                      GSTDY = ' STATIC'  --> GQ_TYPE
                      GSTDY = 'DYNAMIC'  --> GQO_TYPE

       * GQ_TYPE  : local-field correction type (3D) static
                       GQ_TYPE = 'NONE' no local field correction
                       GQ_TYPE = 'ALDA' adiabatic local density
                       GQ_TYPE = 'ALFL' Alvarellos-Flores
                       GQ_TYPE = 'BEBR' Bedell-Brown
                       GQ_TYPE = 'CDOP' TDDFT Corradini et al correction
                       GQ_TYPE = 'GEV2' Geldart-Vosko 2
                       GQ_TYPE = 'GEVO' Geldart-Vosko correction
                       GQ_TYPE = 'GOCA' Gold-Calmels
  temperature-dep. --> GQ_TYPE = 'HNCA' hypernetted chain
                       GQ_TYPE = 'HORA' Holas-Rahman
                       GQ_TYPE = 'HUBB' Hubbard correction (only exchange)
                       GQ_TYPE = 'ICUT' Ichimaru-Utsumi correction
                       GQ_TYPE = 'IKPA' Iwamoto-Krotscheck-Pines
                       GQ_TYPE = 'IWA1' Iwamoto G_{-1}
                       GQ_TYPE = 'IWA2' Iwamoto G_{3} approx.
  temperature-dep. --> GQ_TYPE = 'IWA3' Iwamoto G_{-1}
                       GQ_TYPE = 'IWA4' Iwamoto G_{3} exact
                       GQ_TYPE = 'JGDG' Jung-Garcia-Gonzalez-Dobson-Godby
                       GQ_TYPE = 'KLLA' Kleinman-Langreth correction
                       GQ_TYPE = 'LDAC' LDA correction
                       GQ_TYPE = 'MCSC' Moroni-Ceperley-Senatore correction
                       GQ_TYPE = 'NAGY' Nagy correction
                       GQ_TYPE = 'NEV1' Nevalinna two-moment approximation
                       GQ_TYPE = 'PGGA' Petersilka-Gossmann-Gross
                       GQ_TYPE = 'PVHF' Pavas-Vashishta Hartree-Fock correction
                       GQ_TYPE = 'RICE' Rice correction
                       GQ_TYPE = 'SHAW' Shaw correction
                       GQ_TYPE = 'SLAT' Slater correction
                       GQ_TYPE = 'STLS' Singwi et al correction
  temperature-dep. --> GQ_TYPE = 'TKAC' Tkachenko correction
                       GQ_TYPE = 'TOUL' Toulouse parametrization of CDOP
                       GQ_TYPE = 'TRMA' Tripathy-Mandal
                       GQ_TYPE = 'VASI' Vashishta-Singwi correction
                       GQ_TYPE = 'UTI1' Utsumi-Ichimaru correction (only exchange)

       * GQ_TYPE  : local-field correction type (2D) static
                       GQ_TYPE = 'NONE' no local field correction
                       GQ_TYPE = 'BUTO' Bulutay-Tomak
                       GQ_TYPE = 'DPGT' Davoudi-Giuliani-Giuliani-Tosi
                       GQ_TYPE = 'GOCA' Gold-Calmels
                       GQ_TYPE = 'HUBB' Hubbard correction (only exchange)
                       GQ_TYPE = 'IWA1' Iwamoto G_{-1}
                       GQ_TYPE = 'IWA2' Iwamoto G_{3}
                       GQ_TYPE = 'SAIC' Sato-Ichimaru correction

       * GQ_TYPE  : local-field correction type (1D) static
                       GQ_TYPE = 'NONE' no local field correction
                       GQ_TYPE = 'GOCA' Gold-Calmels
                       GQ_TYPE = 'HUBB' Hubbard correction (only exchange)

       * IQ_TYPE  : type of approximation for I(q)
                       IQ_TYPE  = 'NON' I(q) = 0
                       IQ_TYPE  = 'GKM' Gorobchenko-Kohn-Maksimov
                       IQ_TYPE  = 'HKA' Hong-Kim
                       IQ_TYPE  = 'IKP' Iwamoto-Krotscheck-Pines parametrization
                       IQ_TYPE  = 'KU1' Kugler 1
                       IQ_TYPE  = 'KU2' Kugler 1

                                            ~

       * LANDAU  : model chosen for the calculation of the Landau parameters (3D)
                       LANDAU = 'NONE' Landau's theory not used
                       LANDAU = 'CHEN' Chen's approach
                       LANDAU = 'RASC' Rayleigh-Schrödinger expansion
                       LANDAU = 'ANBR' Anderson-Brinkman model
                       LANDAU = 'GUTZ' Gutzwiller model
                       LANDAU = 'IWPI' Iwamoto-Pines model (hard-sphere)
                       LANDAU = 'GCYO' Giuliani-Vignale parametrization of
                                        Yasuhara-Ousaka approach
                       LANDAU = 'SBOH' slave-boson one-band Hubbard model

       * LANDAU  : model chosen for the calculation of the Landau parameters (2D)
                       LANDAU = 'NONE' Landau's theory not used
                       LANDAU = 'ERZA' Engelbrecht-Randeria-Zhang approach
                       LANDAU = 'GVYO' Giuliani-Vignale parametrization of
                                        Yasuhara-Ousaka approach
                       LANDAU = 'KCMP' Kwoon-Ceperley-Martin parametrization
       * GQO_TYPE : local-field correction type (3D)
                       GQO_TYPE = 'NONE' no local field correction
                       GQO_TYPE = 'ALFL' Alvarellos-Flores correction
                       GQO_TYPE = 'BACA' Barriga-Carrasco correction
                       GQO_TYPE = 'BBSA' Bachlechner-Böhm-Schinner
                       GQO_TYPE = 'COPI' Constantin-Pitarke
                       GQO_TYPE = 'DABR' Dabrowski
                       GQO_TYPE = 'FWRA' Forstmann-Wierling-Röpke
                       GQO_TYPE = 'HOK1' Hong-Kim correction
                       GQO_TYPE = 'HOK2' Hong-Kim correction
                       GQO_TYPE = 'JEWS' Jewsbury approximation
                       GQO_TYPE = 'KUG1' Kugler q --> 0 approximation
                       GQO_TYPE = 'KUG2' Kugler approximation
                       GQO_TYPE = 'MDGA' Mithen-Daligault-Gregori
                       GQO_TYPE = 'NEV2' Nevalinna three-moment approximation
                       GQO_TYPE = 'NLGA' Nagy-Laszlo-Giber approximation
                       GQO_TYPE = 'RIA1' Richardson-Ashcroft G_s
                       GQO_TYPE = 'RIA2' Richardson-Ashcroft G_n
                       GQO_TYPE = 'RIA3' Richardson-Ashcroft G_a
                       GQO_TYPE = 'SHMU' Shah-Mukhopadhyay
                       GQO_TYPE = 'STGU' Sturm-Gusarov
                       GQO_TYPE = 'TOWO' Toigo-Woodruff
                       GQO_TYPE = 'UTI2' Utsumi-Ichimaru approximation
                       GQO_TYPE = 'VISC' viscosity approximation

       * G0_TYPE : type of calculation of gamma_0
                       G0_TYPE = 'SQ' from the static structure factor S(q)
                       G0_TYPE = 'EC' from the correlation energy E_c

       * GI_TYPE : type of calculation of gamma_inf
                       GI_TYPE = 'SQ' from the static strcuture factor S(q)
                       GI_TYPE = 'EC' from the correlation energy E_c


 *-------+---------+---------+---------+---------+---------------------------*

       * DAMPING : type of damping used
                       DAMPING = 'NONE' no damping
                       DAMPING = 'LFTM' lifetime
                       DAMPING = 'RELA' relaxation time
                       DAMPING = 'DECA' decay rate
                       DAMPING = 'DIFF' diffusion coefficient
                       DAMPING = 'VISC' viscosity

       * LT_TYPE    : approximation used for lifetime (3D)
                       LT_TYPE = 'EXTE' given externally (D_VALUE,POWER)
                       LT_TYPE = 'DAVI' Davies formula
                       LT_TYPE = 'GALI' Galitskii formula
                       LT_TYPE = 'GIQU' Giuliani-Quinn formula
                       LT_TYPE = 'GIVI' Giuliani-Vignale formula
                       LT_TYPE = 'INPE' Inogamov-Petrov formula
                       LT_TYPE = 'LUBR' Lugovskoy-Bray formula
                       LT_TYPE = 'NAEC' Nagy-Echenique formula
                       LT_TYPE = 'QIVI' Qian-Vignale formula
                       LT_TYPE = 'EXTE' given externally (D_VALUE,POWER)
                       LT_TYPE = 'QUFE' Quinn-Ferrell formula

       * LT_TYPE    : approximation used for lifetime (2D)
                       LT_TYPE = 'GIQ1' Giuliani-Quinn formula for e-h loss
                       LT_TYPE = 'GIQ2' Giuliani-Quinn formula for plasmon loss
                       LT_TYPE = 'GIVI' Giuliani-Vignale formula
                       LT_TYPE = 'HAWR' Hawrylak formula
                       LT_TYPE = 'MELA' Menashe-Laikhtman formula
                       LT_TYPE = 'QIVI' Qian-Vignale formula

       * RT_TYPE  : relaxation time
                       RT_TYPE = ' NO'   --> no relaxation time
                       RT_TYPE = 'EX1'   --> given externally (D_VALUE_1,POWER_1)
                       RT_TYPE = 'EX2'   --> given externally (D_VALUE_1,POWER_1,D_VALUE_2,POWER_2)
                       RT_TYPE = 'E-E'   --> electron-electron interaction
                       RT_TYPE = 'E-P'   --> electron-phonon interaction
                       RT_TYPE = 'E-I'   --> electron-phonon impurity
                       RT_TYPE = 'ALL'   --> all three taken into account

                                            ~

       * DR_TYPE  : decay rate in 3D
                       DR_TYPE = 'EXTE' given externally (D_VALUE,POWER)
                       DR_TYPE = 'UTIC'   --> Utsumi-Ichimaru approximation
                       DR_TYPE = 'VLAS'   --> Vlasov approximation

       * DC_TYPE  : diffusion coefficient in 3D
                       DC_TYPE = 'EXTE' given externally (D_VALUE,POWER)
                       DC_TYPE = 'ASHO'   --> Ashurst-Hoover

       * VI_TYPE  : viscosity in 3D
                       VI_TYPE = 'EXTE' given externally (D_VALUE,POWER)
                       VI_TYPE = 'AMPP' Angilella et al hard-sphere fluid     --> T-dependent
                       VI_TYPE = 'DRBA' Daligault-Rasmussen-Baalrud (plasmas) --> T-dependent
                       VI_TYPE = 'KHRA' Khrapak for Yukawa fluid  --> T-dependent
                       VI_TYPE = 'LHPO' Longuet-Higgins-Pope      --> T-dependent
                       VI_TYPE = 'LLPA' Landau-Lifshitz-Pitaevskii--> T-dependent
                       VI_TYPE = 'SCHA' Schäfer                   --> T-dependent
                       VI_TYPE = 'SCHD' Schäfer (dynamic)         --> T-dependent
                       VI_TYPE = 'SHTE' Shternin                  --> T-dependent
                       VI_TYPE = 'STEI' Steinberg low-temperature --> T-dependent

       * VI_TYPE  : viscosity in 2D
                       VI_TYPE = 'EXTE' given externally (D_VALUE,POWER)
                       VI_TYPE = 'SCHA' Schäfer                     --> T-dependent
         graphene <--  VI_TYPE = 'KISC' Kiselev-Schmalian (dynamic) --> T-dependent
         graphene <--  VI_TYPE = 'MSFA' Müller-Schmalian-Fritz      --> T-dependent

                                            ~

       * EE_TYPE  : e-e relaxation time in 3D
                       EE_TYPE = 'ALAR'   --> Al'tshuler-Aronov (e-e + impurities)
                       EE_TYPE = 'ALA2'   --> Al'tshuler-Aronov (e-e + impurities)
                       EE_TYPE = 'BACA'   --> Barriga-Carrasco approximation (e-e)
                       EE_TYPE = 'FSTB'   --> Fann et al approximation       (e-e)
                       EE_TYPE = 'PIN1'   --> Pines-Nozières 1st approx.     (e-e)
                       EE_TYPE = 'PIN2'   --> Pines-Nozières 2nd approx.     (e-e)
                       EE_TYPE = 'QIV2'   --> Qian-Vignale high-density limit(e-e)
                       EE_TYPE = 'QIVI'   --> Qian-Vignale                   (e-e)
                       EE_TYPE = 'RASM'   --> Rammer-Smith                   (e-e)
                       EE_TYPE = 'TAI0'   --> Tanaka-Ichimaru approximation  (e-e) --> q = 0
                       EE_TYPE = 'TAIQ'   --> Tanaka-Ichimaru approximation  (e-e) --> q-dependent
                       EE_TYPE = 'UTIC'   --> Utsumi-Ichimaru approximation  (e-e)


       * EE_TYPE  : relaxation time in 2D
                       EE_TYPE = 'ALA2'   --> Al'tshuler-Aronov (e-e + impurities)
                       EE_TYPE = 'FUAB'   --> Fukuyama-Abrahams (disordered metals)
          graphene <-- EE_TYPE = 'LUFO'   --> Lucas-Fong      (e-e)
                       EE_TYPE = 'QIVI'   --> Qian-Vignale    (e-e)
                       EE_TYPE = 'RASM'   --> Rammer-Smith    (e-e)
  heterostructures <-- EE_TYPE = 'REWI'   --> Reizer-Wilkins  (e-e)
                       EI_TYPE = 'SHAS'   --> Sharma-Ashraf   (e-e + impurities)
                       EE_TYPE = 'ZHDA'   --> Zhang-Das Sarma (e-e)

       * EE_TYPE  : relaxation time in 1D
                       EE_TYPE = 'ALA2'   --> Al'tshuler-Aronov (e-e + impurities)
                       EE_TYPE = 'SHAS'   --> Sharma-Ashraf   (e-e + impurities)

       * EP_TYPE  : e-phonon relaxation time in 3D
                       EP_TYPE = 'STEL'   --> Steinberg low-temperature
                       EP_TYPE = 'STEH'   --> Steinberg High-temperature

       * EI_TYPE  : e-impurit relaxation time in 3D
                       EI_TYPE = 'HEAP'   --> Hertel-Appel approximation

                                            ~

       * IP_TYPE  : ion plasma relaxation time in 3D
                       IP_TYPE = 'SEMO'   --> Selchow-Morawetz approximation
                       IP_TYPE = 'SPIT'   --> Spitzer approximation

       * PD_TYPE  : method used to compute the plasmon damping (3D)
                       PD_TYPE = 'NONE'   --> no plasmon damping
                       PD_TYPE = 'CALL'   --> Callen approximation
                       PD_TYPE = 'DGKA'   --> DuBois-Gilinsky-Kivelson approximation
                       PD_TYPE = 'FEWA'   --> Fetter and Walecka approximation
                       PD_TYPE = 'JEWS'   --> Jewsbury approximation
                       PD_TYPE = 'LITI'   --> Giuliani-Quinn lifetime approximation
                       PD_TYPE = 'MOPE'   --> Molinari-Peerani approximation
                       PD_TYPE = 'NPSA'   --> Ninham-Powel-Swanson approximation
                       PD_TYPE = 'SGAA'   --> Segui-Gervasoni-Arista approximation

       * QD_TYPE  : method used to compute q-dependent relaxation time
                       QD_TYPE = 'NONE'   --> no q-dependence
                       QD_TYPE = 'GAUS'   --> Gaussian
                       QD_TYPE = 'LORE'   --> Lorentzian

       * ZETA     : Value of Tanaka-Ichimaru parameter

                                            ~

       * D_VALUE_1: Value of the 1st damping coefficient (between 0 and 999.999)

        * POWER_1 : power of ten to multiply D_VALUE_1 by
                       POWER = ' KILO'
                       POWER = ' MEGA'
                       POWER = ' GIGA'
                       POWER = ' TERA'
                       POWER = ' PETA'
                       POWER = '  EXA'
                       POWER = 'ZETTA'
                       POWER = 'MILLI'
                       POWER = 'MICRO'
                       POWER = ' NANO'
                       POWER = ' PICO'
                       POWER = 'FEMTO'
                       POWER = ' ATTO'
                       POWER = 'ZEPTO'
                       POWER = 'YOCTO'

        * EK      : kinetic energy of electron considered (in eV)
                      with respect to vacuum level

                                            ~

        * D_VALUE_2: Value of the 2nd damping coefficient (between 0 and 999.999)

        * POWER_2 : power of ten to multiply D_VALUE_2 by
                       POWER = ' KILO'
                       POWER = ' MEGA'
                       POWER = ' GIGA'
                       POWER = ' TERA'
                       POWER = ' PETA'
                       POWER = '  EXA'
                       POWER = 'ZETTA'
                       POWER = 'MILLI'
                       POWER = 'MICRO'
                       POWER = ' NANO'
                       POWER = ' PICO'
                       POWER = 'FEMTO'
                       POWER = ' ATTO'
                       POWER = 'ZEPTO'
                       POWER = 'YOCTO'

        * PCT     : weight of first memory function (relaxation time D_VALUE_1)
                       0.00 <= PCT <= 1.00

 *-------+---------+---------+---------+---------+---------------------------*

       * INT_POT  : type of interaction potential (3D)
                       INT_POT = 'COULO' Coulomb interaction
                       INT_POT=  'YUKAW' Yukawa interaction
                       INT_POT=  'SOFTS' soft sphere
                       INT_POT=  'LNJNS' Lennard-Jones
                       INT_POT=  'HCLNJ' hard-core Lennard-Jones
                       INT_POT=  'KIHAR' Kihara
                       INT_POT=  'MIE_P' Mie
                       INT_POT=  'VANDW' Van der Waals
                       INT_POT=  'MORSE' Morse
                       INT_POT=  'G_EXP' generalised exponential
                       INT_POT=  'EXP_6' exp-6
                       INT_POT=  'MBUCK' modified Buckingham
                       INT_POT=  'N_COU' neutralised Coulomb
                       INT_POT=  'H_COR' hard-core
                       INT_POT=  'P_SPH' penetrable sphere
                       INT_POT=  'ST-JO' Starkloff-Joannopoulos soft-core
                       INT_POT=  'LR_OS' long-range oscillatory
                       INT_POT=  'STOCK' Stockmayer
                       INT_POT=  'RPAPO' RPA interaction
                       INT_POT = 'OVER1' Overhauser interaction
                       INT_POT = 'OVER2' modified Overhauser interaction
                       INT_POT = 'DEUTS' Deutsch interaction
                       INT_POT = 'PHOLE' particle-hole interaction
                       INT_POT = 'KELBG' Kelbg interaction

        * S       :  length scale parameter of the potential (in Angström)

        * EPS     :  depth of the potential (in units of k_B, i.e. in Kelvin)

        * DELTA   :  polarisation of the fluid (dimensionless)

                                            ~

       * RC       :  potential core radius (in Angström)
       * ALF      :  potential stiffness
       * M        :  \   exponents for Kihara
       * N        :  /   and Mie potentials

                                            ~

       * A1       :  \
       * A2       :   \  parameters for
       * A3       :   /  long-range oscillatory potentials
       * A4       :  /

 *-------+---------+---------+---------+---------+---------------------------*

       * EP_C     :  electron-phonon coupling
       * DEBYE_T  :  material Debye temperature

                                            ~

       * NA       :  number of atoms per unit volume
       * MA       :  mass of the atoms
       * RA       :  radius of the atoms

 *-------+---------+---------+---------+---------+---------------------------*

        * NI      :  impurity concentration
        * EI_C    :  strength of impurity scattering

 *-------+---------+---------+---------+---------+---------------------------*


        * CF_TYPE  : type of classical fluid calculation         --> 3D
                       CF_TYPE = 'SHS' smooth hard spheres
                       CF_TYPE = 'RH1' rough hard spheres (Pidduck)
                       CF_TYPE = 'RH2' rough hard spheres (Condiff-Lu-Dahler)
                       CF_TYPE = 'RH3' rough hard spheres (McCoy-Sandler-Dahler)
                       CF_TYPE = 'DCE' dilute Chapman-Enskog
                       CF_TYPE = 'HCE' heavy (i.e. dense) Chapman-Enskog
                       CF_TYPE = 'LJF' Lennard-Jones fluid

                       CF_TYPE = 'DHD' dense hard disks          --> 2D

        * PF_TYPE  : type of packing fraction                    --> 2D
                       PF_TYPE = 'HDM'   --> hard disk model

                                                                 --> 3D
                       PF_TYPE = 'HSM'   --> hard sphere model
                       PF_TYPE = 'RCP'   --> random closed-packed
                       PF_TYPE = 'FCC'   --> FCC closed-packed
                       PF_TYPE = 'FRE'   --> freezing
                       PF_TYPE = 'MEL'   --> melting

        * SL_TYPE  : type of scattering length calculation
                       SL_TYPE = 'HSP'   --> hard sphere potential
                       SL_TYPE = 'ASW'   --> attractive square well (without bound state)
                       SL_TYPE = 'RSW'   --> repulsive square well
                       SL_TYPE = 'DSP'   --> delta-shell potential
                       SL_TYPE = 'AYP'   --> attractive Yukawa potential
                       SL_TYPE = 'CCO'   --> Coulomb cut-off potential
                       SL_TYPE = 'HUL'   --> Hulthén potential

 *=======+=========+=========+=========+=========+===========================*
 *                        STRUCTURE FACTOR :                                 *
 *=======+=========+=========+=========+=========+===========================*


       * SSTDY    : static vs dynamic local-field corrections
                      SSTDY = ' STATIC'  --> SQ_TYPE
                      SSTDY = 'DYNAMIC'  --> SQO_TYPE

       * SQ_TYPE   : structure factor approximation (3D) --> static
                       SQ_TYPE  = 'DEH' Debye-Hückel approximation
                       SQ_TYPE  = 'GEA' generalized approximation
                       SQ_TYPE  = 'GOR' Gorobchenko approximation
                       SQ_TYPE  = 'GR2' computed from g(r) (GR_TO_SQ.f90 code)
                       SQ_TYPE  = 'GSB' Gori-Giorgi-Sacchetti-Bachelet approximation
                       SQ_TYPE  = 'HFA' Hartree-Fock approximation (only exchange)
                       SQ_TYPE  = 'HUB' Hubbard approximation
                       SQ_TYPE  = 'ICH' Ichimaru approximation
                       SQ_TYPE  = 'LEE' Lee ideal Fermi gas
                       SQ_TYPE  = 'MSA' mean spherical approximation
                       SQ_TYPE  = 'PKA' Pietiläinen-Kallio
                       SQ_TYPE  = 'RPA' RPA approximation
                       SQ_TYPE  = 'SHA' Shaw approximation
                       SQ_TYPE  = 'SIN' Singh
                       SQ_TYPE  = 'SPA' Singh-Pathak
                       SQ_TYPE  = 'TWA' Toigo-Woodruff approximation
!
       * SQO_TYPE   : structure factor approximation (3D) --> dynamic
                       SQO_TYPE  = 'ABA' Arista-Brandt approximation
                       SQO_TYPE  = 'EPS' computed from dielectric function
                       SQO_TYPE  = 'HFA' Hartree-Fock approximation
                       SQO_TYPE  = 'HYD' hydrodynamic approximation
                       SQO_TYPE  = 'IGA' ideal gas approximation
                       SQO_TYPE  = 'ITA' Ichimaru-Tanaka approximation
                       SQO_TYPE  = 'MFA' Hansen-McDonald-Pollock approximation
                       SQO_TYPE  = 'MFD' memory function model
                       SQO_TYPE  = 'NIC' Nakano-Ichimaru approximation
                       SQO_TYPE  = 'UTI' Utsumi-Ichimaru approximation (3D)
                       SQO_TYPE  = 'VLA' linearized Vlasov approximation

 *=======+=========+=========+=========+=========+===========================*
 *                        PAIR CORRELATION FUNCTION :                        *
 *=======+=========+=========+=========+=========+===========================*

       * GR_TYPE     : pair correlation function approximation g(r) (3D)
                       GR_TYPE  = 'CDF' from chain diagram formula of PDF (long distance)
                       GR_TYPE  = 'DHA' Debye-Hückel approximation
                       GR_TYPE  = 'DWA' DeWitt approximation
                       GR_TYPE  = 'FBA' Frieman-Book approximation
                       GR_TYPE  = 'HFA' Hartree-Fock approximation (only exchange)
                       GR_TYPE  = 'HUB' Hubbard approximation
                       GR_TYPE  = 'LLA' Lee-Long approximation
                       GR_TYPE  = 'ORB' Ortiz-Ballone approximation
                       GR_TYPE  = 'PDF' from pair distribution function
                       GR_TYPE  = 'SHA' Shaw approximation
                       GR_TYPE  = 'SQ2' computed from S(q) (SQ_TO_GR.f90 code)
                       GR_TYPE  = 'WIG' Wigner approximation

       * GR0_MODE : g(0) (3D)
                       GR0_MODE  = 'CAGO' --> Calmels-Gold
                       GR0_MODE  = 'DPGT' --> Davoudi-Polini-Giuliani-Tosi
                       GR0_MODE  = 'HASA' --> Holas-Aravind-Singwi (small r_s)
                       GR0_MODE  = 'ICHI' --> Ichimaru
                       GR0_MODE  = 'KIMB' --> Kimball
                       GR0_MODE  = 'OVE1' --> Overhauser 1
                       GR0_MODE  = 'OVE2' --> Overhauser 2
                       GR0_MODE  = 'QIAN' --> Qian
       * GR0_MODE : g(0) (2D)
                       GR0_MODE  = 'CAGO' --> Calmels-Gold
                       GR0_MODE  = 'HAFO' --> Hartree-Fock
                       GR0_MODE  = 'MOMA' --> Moreno-Marinescu
                       GR0_MODE  = 'NSOA' --> Nagano-Singwi-Ohnishi
                       GR0_MODE  = 'QIAN' --> Qian

 *=======+=========+=========+=========+=========+===========================*
 *                        PAIR DISTRIBUTION FUNCTION :                       *
 *=======+=========+=========+=========+=========+===========================*

       * RH_TYPE     : pair distribution function approximation (3D)
                       RH_TYPE  = 'CDI' chain diagram improved
                       RH_TYPE  = 'CEG' classical electron gas
                       RH_TYPE  = 'DEB' Debye electron gas
                       RH_TYPE  = 'FUA' correct to order 2 in epsilon
                       RH_TYPE  = 'SDC' short-distance correlations
                       RH_TYPE  = 'WDA' watermelon diagrams summed

 *=======+=========+=========+=========+=========+===========================*
 *                        SPECTRAL FUNCTION :                                *
 *=======+=========+=========+=========+=========+===========================*

                       SPF_TYPE  = 'NAIC' Nakano-Ichimaru approximation

 *=======+=========+=========+=========+=========+===========================*
 *                         ENERGY CALCULATIONS :                             *
 *=======+=========+=========+=========+=========+===========================*

       * EC_TYPE  : type of correlation energy functional (3D)
                       EC_TYPE = 'GEBR_W'   --> Gell-Mann and Brueckner
                       EC_TYPE = 'CAMA_W'   --> Carr and Maradudin
                       EC_TYPE = 'EHTY_G'   --> Endo-Horiuchi-Takada-Yasuhara
                       EC_TYPE = 'HELU_G'   --> Hedin and Lundqvist
                       EC_TYPE = 'VBHE_G'   --> von Barth and Hedin
                       EC_TYPE = 'PEZU_G'   --> Perdew and Zunger
                       EC_TYPE = 'WIGN_G'   --> Wigner
                       EC_TYPE = 'NOPI_G'   --> Nozières and Pines
                       EC_TYPE = 'LIRO_G'   --> Lindgren and Rosen
                       EC_TYPE = 'PEZU_G'   --> Perdew and Zunger
                       EC_TYPE = 'REHI_G'   --> Rebei and Hitchon
                       EC_TYPE = 'GGSB_G'   --> Gori-Giorgi-Sacchetti-Bachelet
                       EC_TYPE = 'PRKO_G'   --> Proynov and Kong
                       EC_TYPE = 'GGSB_G'   --> Vosko, Wilk and Nusair
                       EC_TYPE = 'VWNU_G'   --> Perdew and Wang
                       EC_TYPE = 'HUBB_G'   --> Hubbard
                       EC_TYPE = 'CHAC_G'   --> Chachiyo
                       EC_TYPE = 'ISKO_T'   --> Isihara and Kojima
       * EC_TYPE  : type of correlation energy functional (2D)
                       EC_TYPE = 'TACE_G'   --> Tanatar-Ceperley
                       EC_TYPE = 'CPPA_G'   --> Seidl-Perdew_Levy
                       EC_TYPE = 'AMGB_G'   --> Attaccalite-Moroni-Gori-Giorgi-Bachelet
                       EC_TYPE = 'SEID_G'   --> Seidl
                       EC_TYPE = 'LOOS_W'   --> Loos
                       EC_TYPE = 'WIGN_G'   --> Wigner
                       EC_TYPE = 'ISTO_T'   --> Isihara-Toyoda
       * EC_TYPE  : type of correlation energy functional (1D)
                       EC_TYPE = 'LOOS_W'   --> Loos
                       EC_TYPE = 'WIGN_G'   --> Wigner


       * FXC_TYPE : type of XC free energy functional           --> 3D
                       FXC_TYPE = 'NO'   --> None
                       FXC_TYPE = 'EB'   --> Ebeling et al
                       FXC_TYPE = 'IC'   --> Ichimaru et al
                       FXC_TYPE = 'KS'   --> Karasiev et al
                       FXC_TYPE = 'VS'   --> Vashishta and Singwi
                       FXC_TYPE = 'PD'   --> Perrot and Dharma-Wardana
                       FXC_TYPE = 'EK'   --> Ebeling-Kraeft-Kremp-Röpke

       * EXC_TYPE : type of exchange-correlation energy functional   --> 3D
                       EXC_TYPE = 'NO'   --> None
                       EXC_TYPE = 'GT'   --> Goedeker-Tetter-Hutter
                       EXC_TYPE = 'ST'   -->
                       EXC_TYPE = 'BD'   --> Brown-DuBois-Holzmann-Ceperley

                                            ~


 *=======+=========+=========+=========+=========+===========================*
 *                         SPIN POLARIZATION :                               *
 *=======+=========+=========+=========+=========+===========================*

       * IMODE    : choice of spin parameters
                       IMODE = 1 : no spin polarization
                       IMODE = 2 : fully spin-polarized

       * XI       : spin polarization : (n+ - n-) / n

 *=======+=========+=========+=========+=========+===========================*
 *                        THERMODYNAMIC PROPERTIES :                         *
 *=======+=========+=========+=========+=========+===========================*

       * TH_PROP   : type of calculation  --> thermodynamic properties
                      TH_PROP  = 'CLAS' : classical approximation
                      TH_PROP  = 'QUAN' : quantum approximation

       * GP_TYPE  : grand partition function type (3D)
                       GP_TYPE = 'IK0' Isihara-Kojima formulation
                       GP_TYPE = 'RH0' Rebei-Hitchon formulation
                       GP_TYPE = 'IKM' Isihara-Kojima with magnetic field

       * GP_TYPE  : grand partition function type (2D)
                       GP_TYPE = 'I20' Isihara-Kojima formulation
                       GP_TYPE = 'I2M' Isihara-Kojima with magnetic field

 *=======+=========+=========+=========+=========+===========================*
 *                        ELECTRON MEAN FREE PATH :                          *
 *=======+=========+=========+=========+=========+===========================*

       * EK_INI   :  starting kinetic energy of electron in eV
       * EK_FIN   :  final    kinetic energy of electron in eV

 *=======+=========+=========+=========+=========+===========================*
 *                        CALCULATION OF MOMENTS :                           *
 *=======+=========+=========+=========+=========+===========================*

       * N        :  order of moment
       * M_TYPE   : way S(q,omega) is computed
                       M_TYPE  = 'EPS' from epsilon(q,omega)
                       M_TYPE  = 'SQO' from S(q,omega)

 *=======+=========+=========+=========+=========+===========================*
 *                        INCOMING ION BEAM :                                *
 *=======+=========+=========+=========+=========+===========================*

       * Z_BEAM    :  charge of ions in incoming beam
       * EK_BEAM   :  kinetic energy of incoming beam ions (eV)

 *=======+=========+=========+=========+=========+===========================*
 *                        OUTPUT CALCULATIONS :                              *
 *=======+=========+=========+=========+=========+===========================*

       * I_DF      : switch for dielectric function printing
                      I_DF  = 0 : dielectric function not printed
                      I_DF  = 1 : dielectric function printed in file 'diel_func.dat'

       * I_PZ      : switch for polarization function printing
                      I_PZ  = 0 : polarization function not printed
                      I_PZ  = 1 : polarization function printed in file 'pola_func.dat'

       * I_SU      : switch for susceptibility function printing
                      I_SU  = 0 : susceptibility function not printed
                      I_SU  = 1 : susceptibility function printed in file 'susc_func.dat'

       * I_CD      : switch for electrical conductivity printing
                      I_CD  = 0 : electrical conductivity not printed
                      I_CD  = 1 : electrical conductivity printed in file 'cond_func.dat'

 *-------+---------+---------+---------+---------+---------------------------*

       * I_PD      : switch for plasmon dispersion printing
                      I_PD  = 0 :  plasmon dispersion not calculated
                      I_PD  = 1 :  plasmon dispersion printed in file 'plas_disp.dat'

       * I_EH      : switch for electron-hole dispersion printing
                      I_EH  = 0 :  electron-hole dispersion not calculated
                      I_EH  = 1 :  electron-hole dispersion printed in file 'elec_hole.dat'

       * I_E2      : switch for two-electron-hole dispersion printing
                      I_E2  = 0 :  two-electron-hole dispersion not calculated
                      I_E2  = 1 :  two-electron-hole dispersion printed in file 'elec_hol2.dat'

       * I_CK      : switch for k-space e-e interaction potential printing
                      I_CK  = 0 :  potential not calculated
                      I_CK  = 1 :  potential printed in file 'int_pot_k.dat'

       * I_CR      : switch for real-space e-e interaction potential printing
                      I_CR  = 0 :  potential not calculated
                      I_CR  = 1 :  potential printed in file 'int_pot_r.dat'

       * I_PK      : switch for plasmon kinetic energy printing
                      I_PK  = 0 :  plasmon kinetic energy not calculated
                      I_PK  = 1 :  plasmon kinetic energy printed in file 'plas_kine.dat'

 *-------+---------+---------+---------+---------+---------------------------*

       * I_LF      : switch for local-field corrections G(q,omega) printing
                      I_LF  = 0 : local-field corrections not calculated
                      I_LF  = 1 : local-field corrections printed in file 'loca_fiel.dat'

       * I_IQ      : switch for G(q,inf) printing
                      I_IQ  = 0 : G(q,inf) not calculated
                      I_IQ  = 1 : G(q,inf) printed in file 'ginf_fiel.dat'

       * I_SF      : switch for structure factor S(q,omega) printing
                      I_SF  = 0 : structure factor not calculated
                      I_SF  = 1 : structure factor printed in file 'stru_fact.dat'

       * I_PC      : switch for pair correlation function g(r) printing
                      I_PC  = 0 :  pair correlation function not calculated
                      I_PC  = 1 :  pair correlation function printed in file 'pair_corr.dat'

                                            ~

       * I_P2      : switch for pair distribution rho2(r) printing
                      I_P2  = 0 :  pair distribution function not calculated
                      I_P2  = 1 :  pair distribution function printed in file 'pair_dist.dat'

       * I_VX      : switch for  vertex function Gamma(q,omega) printing
                      I_VX  = 0 :  vertex function not calculated
                      I_VX  = 1 :  vertex function printed in file 'vertex_fu.dat'

       * I_DC      : switch for plasmon damping coefficient Im[eps]/q^2 printing
                      I_DC  = 0 :  plasmon damping not calculated
                      I_DC  = 1 :  plasmon damping printed in file 'plas_damp.dat'

       * I_MD      : switch for momentum distribution printing
                      I_MD  = 0 :  momentum distribution not calculated
                      I_MD  = 1 :  momentum distribution printed in file 'mome_dist.dat'

                                            ~

       * I_LD      : switch for Landau parameters printing
                      I_LD  = 0 : Landau parameters not calculated
                      I_LD  = 1 : Landau parameters printed in file 'landau_pa.dat'

       * I_DP      : switch for damping printing
                      I_DP  = 0 : damping not calculated
                      I_DP  = 1 : damping printed in file 'damp_file.dat'

       * I_LT      : switch for plasmon lifetime printing
                      I_LT  = 0 : plasmon lifetime not calculated
                      I_LT  = 1 : plasmon lifetime printed in file 'life_time.dat'

       * I_BR      : switch for plasmon broadening printing
                      I_BR  = 0 : plasmon broadening not calculated
                      I_BR  = 1 : plasmon broadening printed in file 'broadenin.dat'

                                            ~

       * I_PE      : switch for plasmon energy printing
                      I_PE  = 0 : plasmon energy not calculated
                      I_PE  = 1 : plasmon energy printed in file 'plas_ener.dat'

       * I_QC      : switch for plasmon q-bounds printing
                      I_QC  = 0 : plasmon q-bounds not calculated
                      I_QC  = 1 : plasmon q-bounds printed in file 'qc_bounds.dat'

       * I_RL      : switch for relaxation time printing
                      I_RL  = 0 : relaxation time not calculated
                      I_RL  = 1 : relaxation time printed in file 'rela_time.dat'

       * I_KS      : switch for screening wave vector printing
                      I_KS  = 0 : screening wave vector not calculated
                      I_KS  = 1 : screening wave vector printed in file 'screen_wv.dat'

                                            ~

       * I_OQ      : switch for omega = q * v_F printing
                      I_DY  = 0 :  omega = q * v_F not calculated
                      I_DY  = 1 :  omega = q * v_F printed in file 'omega_qvf.dat'

       * I_ME      : switch for moments of epsilon(q,omega) printing
                      I_ME  = 0 : moments of epsilon not calculated
                      I_ME  = 1 : moments of epsilon printed in file 'moments_e.dat'

       * I_MS      : switch for moments of S(q,omega) printing
                      I_MS  = 0 : moments of structure factor not calculated
                      I_MS  = 1 : moments of structure factor printed in file 'moments_s.dat'

       * I_ML      : switch for moments of loss function printing
                      I_ML  = 0 : moments of loss function not calculated
                      I_ML  = 1 : moments of loss function printed in file 'moments_l.dat'

                                            ~

       * I_MC      : switch for moments of conductivity printing
                      I_MC  = 0 : moments of conductivity not calculated
                      I_MC  = 1 : moments of conductivity printed in file 'moments_c.dat'

       * I_DE      : switch for derivative of Re[ dielectric function ] printing
                      I_DE  = 0 : derivative not calculated
                      I_DE  = 1 : derivative printed in file 'deri_epsi.dat'

       * I_ZE      : switch for Re[ dielectric function ] = 0 printing
                      I_ZE  = 0 : function not calculated
                      I_ZE  = 1 : function printed in file 'ree0_file.dat'

       * I_SR      : switch for sum rules for epsilon printing
                      I_SR  = 0 : sum rules not calculated
                      I_SR  = 1 : sum rules printed in file 'sum_rules.dat'

                                            ~

       * I_CW      : switch for confinement wave function printing
                      I_CW  = 0 : confinement wave function not calculated
                      I_CW  = 1 : confinement wave function printed in file 'confin_wf.dat'

       * I_CF      : switch for confinement potential printing
                      I_CF  = 0 : confinement potential not calculated
                      I_CF  = 1 : confinement potential printed in file 'confin_pt.dat'

       * I_EM      : switch for effective mass printing
                      I_EM  = 0 : effective mass not calculated
                      I_EM  = 1 : effective mass printed in file 'effe_mass.dat'

       * I_MF      : switch for mean free path printing
                      I_MF  = 0 : mean free path not calculated
                      I_MF  = 1 : mean free path printed in file 'mean_path.dat'

                                            ~

       * I_SP      : switch for spectral function printing
                      I_SP  = 0 : spectral function not calculated
                      I_SP  = 1 : spectral function printed in file 'spec_func.dat'

       * I_SE      : switch for self-energy printing
                      I_SE  = 0 : self-energy not calculated
                      I_SE  = 1 : self-energy printed in file 'self_ener.dat'

       * I_SB      : switch for  subband energies printing
                      I_SB  = 0 : subband energies not calculated
                      I_SB  = 1 : subband energies printed in file 'subb_ener.dat'

       * I_ES      : switch for Eliashberg function printing
                      I_ES  = 0 : Eliashberg function not calculated
                      I_ES  = 1 : Eliashberg function printed in file 'elia_func.dat'

                                            ~

       * I_GR      : switch for Grüneisen parameter printing
                      I_GR  = 0 : Grüneisen parameter not calculated
                      I_GR  = 1 : Grüneisen parameter printed in file 'grune_par.dat'

       * I_FD      : switch for Fermi-Dirac distribution printing
                      I_FD  = 0 : Fermi-Dirac distribution not calculated
                      I_FD  = 1 : Fermi-Dirac distribution printed in file 'fermi_dir.dat'

       * I_BE      : switch for Bose-Einstein distribution printing
                      I_BE  = 0 : Bose-Einstein distribution not calculated
                      I_BE  = 1 : Bose-Einstein distribution printed in file 'bose_eins.dat'

       * I_MX      : switch for Maxwell distribution printing
                      I_MX  = 0 : Maxwell distribution not calculated
                      I_MX  = 1 : Maxwell distribution printed in file 'maxwell_d.dat'

                                            ~

       * I_SC      : switch for scale parameters  printing
                      I_SC  = 0 : scale parameters not calculated
                      I_SC  = 1 : scale parameters printed in file 'scale_par.dat'

       * I_DS      : switch for density of states printing
                      I_DS  = 0 : density of states not calculated
                      I_DS  = 1 : density of states printed in file 'dens_stat.dat'

       * I_NV      : switch for Fourier domain Nevanlinaa/memory function printing
                      I_NV  = 0 : Nevanlinaa function not calculated
                      I_NV  = 1 : Nevanlinaa function printed in file 'neva_four.dat'

       * I_MT      : switch for  time domain memory function printing
                      I_MT  = 0 : memory function not calculated
                      I_MT  = 1 : memory function printed in file 'memo_time.dat'

 *-------+---------+---------+---------+---------+---------------------------*

       * I_GP      : switch for grand partition function printing
                      I_GP  = 0 : grand partition function not calculated
                      I_GP  = 1 : grand partition function printed in file 'gran_part.dat'

       * I_PR      : switch for electronic pressure printing
                      I_PR  = 0 : electronic pressure not calculated
                      I_PR  = 1 : electronic pressure printed in file 'epressure.dat'

       * I_CO      : switch for compressibility printing
                      I_CO  = 0 : compressibility not calculated
                      I_CO  = 1 : compressibility printed in file 'comp_file.dat'

       * I_CP      : switch for chemical potential printing
                      I_CP  = 0 : chemical potential not calculated
                      I_CP  = 1 : chemical potential printed in file 'chem_pote.dat'

                                            ~

       * I_BM      : switch for bulk modulus printing
                      I_BM  = 0 : bulk modulus not calculated
                      I_BM  = 1 : bulk modulus printed in file 'bulk_modu.dat'

       * I_SH      : switch for shear modulus printing
                      I_SH  = 0 : shear modulus not calculated
                      I_SH  = 1 : shear modulus printed in file 'shear_mod.dat'

       * I_S0      : switch for zero sound velocity printing
                      I_S0  = 0 : zero sound velocity not calculated
                      I_S0  = 1 : zero sound velocity printed in file 'zero_soun.dat'

       * I_S1      : switch for first sound velocity printing
                      I_S1  = 0 : first sound velocity not calculated
                      I_S1  = 1 : first sound velocity printed in file 'firs_soun.dat'

                                            ~

       * I_DT      : switch for Debye temperature printing
                      I_DT  = 0 : Debye temperature not calculated
                      I_DT  = 1 : Debye temperature printed in file 'Debye_tmp.dat'

       * I_PS      : switch for Pauli paramagnetic susceptibility printing
                      I_PS  = 0 : Pauli paramagnetic susceptibility not calculated
                      I_PS  = 1 : Pauli paramagnetic susceptibility printed in file 'para_susc.dat'

       * I_IE      : switch for internal energy printing
                      I_IE  = 0 : internal energy not calculated
                      I_IE  = 1 : internal energy printed in file 'inter_ene.dat'

       * I_EI      : switch for excess internal energy printing
                      I_EI  = 0 : excess internal energy not calculated
                      I_EI  = 1 : excess internal energy printed in file 'exces_ene.dat'

                                            ~

       * I_FH      : switch for Helmholtz free energy printing
                      I_FH  = 0 : Helmholtz free energy not calculated
                      I_FH  = 1 : Helmholtz free energy printed in file 'helm_free.dat'

       * I_EY      : switch for entropy printing
                      I_EY  = 0 : entropy not calculated
                      I_EY  = 1 : entropy printed in file 'entropy_f.dat'

 *-------+---------+---------+---------+---------+---------------------------*

       * I_EF      : switch for Fermi energy printing
                      I_EF  = 0 : Fermi energy not calculated
                      I_EF  = 1 : Fermi energy printed in file 'fermi_ene.dat'

       * I_KF      : switch for Fermi momentum printing
                      I_KF  = 0 : Fermi momentum not calculated
                      I_KF  = 1 : Fermi momentum printed in file 'fermi_vec.dat'

       * I_VF      : switch for Fermi velocity printing
                      I_VF  = 0 : Fermi velocity not calculated
                      I_VF  = 1 : Fermi velocity printed in file 'fermi_vel.dat'

       * I_TE      : switch for Fermi temperature printing
                      I_TE  = 0 : Fermi temperature not calculated
                      I_TE  = 1 : Fermi temperature printed in file 'fermi_tmp.dat'

                                            ~

       * I_DL      : switch for density of states at Fermi level printing
                      I_DL  = 0 : density of states at Fermi level not calculated
                      I_DL  = 1 : density of states at Fermi level printed in file 'fermi_dos.dat'

 *-------+---------+---------+---------+---------+---------------------------*

       * I_TW      : switch for thermal De Broglie wavelength printing
                      I_TW  = 0 : thermal De Broglie wavelength not calculated
                      I_TW  = 1 : thermal De Broglie wavelength printed in file 'thermal_w.dat'

       * I_VT      : switch for thermal velocity printing
                      I_VT  = 0 : thermal velocity not calculated
                      I_VT  = 1 : thermal velocity printed in file 'thermal_v.dat'

       * I_TC      : switch for thermal conductivity printing
                      I_TC  = 0 : thermal conductivity not calculated
                      I_TC  = 1 : thermal conductivity printed in file 'thermal_c.dat'

 *-------+---------+---------+---------+---------+---------------------------*

       * I_EG      : switch for ground state energy printing
                      I_EG  = 0 : ground state energy not calculated
                      I_EG  = 1 : ground state energy printed in file 'ground_st.dat'

       * I_EX      : switch for exchange energy printing
                      I_EX  = 0 : exchange energy not calculated
                      I_EX  = 1 : exchange energy printed in file 'ex_energy.dat'

       * I_XC      : switch for exchange correlation energy printing
                      I_XC  = 0 : exchange correlation energy not calculated
                      I_XC  = 1 : exchange correlation energy printed in file 'xc_energy.dat'

       * I_EC      : switch for correlation energy printing
                      I_EC  = 0 : correlation energy not calculated
                      I_EC  = 1 : correlation energy printed in file 'corr_ener.dat'

                                            ~

       * I_HF      : switch for Hartree-Fock energy printing
                      I_HF  = 0 : Hartree-Fock energy not calculated
                      I_HF  = 1 : Hartree-Fock energy printed in file 'hf_energy.dat'

       * I_EK      : switch for kinetic energy printing
                      I_EK  = 0 : kinetic energy not calculated
                      I_EK  = 1 : kinetic energy printed in file 'kine_ener.dat'

       * I_EP      : switch for potential energy printing
                      I_EP  = 0 : potential energy not calculated
                      I_EP  = 1 : potential energy printed in file 'pote_ener.dat

 *-------+---------+---------+---------+---------+---------------------------*

       * I_VI      : switch for shear viscosity printing
                      I_VI  = 0 : shear viscosity not calculated
                      I_VI  = 1 : shear viscosity printed in file 'visc_coef.dat'

       * I_DI      : switch for diffusion coefficient printing
                      I_DI  = 0 : diffusion coefficient not calculated
                      I_DI  = 1 : diffusion coefficient printed in file 'diff_coef.dat'

 *-------+---------+---------+---------+---------+---------------------------*

       * I_FP      : switch for fluctuation potential calculation
                      I_FP  = 0 :  fluctuation potential not calculated

                      I_FP  = 1 :  fluctuation potential printed in file 'fluct_pot.dat' (function of q)       \ exact
                      I_FP  = 2 :  fluctuation potential printed in file 'fluct_pot.dat' (function of q and r) / dispersion

                      I_FP  = 3 :  fluctuation potential printed in file 'fluct_pot.dat' (function of q)       \ PL_DISP
                      I_FP  = 4 :  fluctuation potential printed in file 'fluct_pot.dat' (function of q and r) / dispersion

       * I_EL      : switch for loss function calculation
                      I_EL  = 0 :  loss function not calculated
                      I_EL  = 1 :  loss function printed in file 'ener_loss.dat'

       * I_PO      : switch for stopping power calculation
                      I_PO  = 0 :  stopping power not calculated
                      I_PO  = 1 :  stopping power printed in file 'stop_powe.dat'

       * I_RF      : switch for refractive index calculation
                      I_RF  = 0 :  refractive index not calculated
                      I_RF  = 1 :  refractive index printed in file 'refrac_in.dat'

                                            ~

       * I_VC      : switch for dynamic screened Coulomb potential V(q,omega) calculation
                      I_VC  = 0 : dynamic screened Coulomb potential not calculated
                      I_VC  = 1 : dynamic screened Coulomb potential printed in file 'dyna_coul.dat'

 *-------+---------+---------+---------+---------+---------------------------*

       * I_FN      : switch for appending the calculation type string
                        to the output filename
                      I_FN  = 0 :  standard output filename
                      I_FN  = 1 :  parameter added to filename

       * I_WR      : switch for writing physical properties into the log file
                      I_WR  = 0 :  does not write
                      I_WR  = 1 :  writes
                      I_WR  = 2 :  writes only for first q-index

       * I_TI      : switch for writing integration tests into the log file
                      I_TI  = -1 :  writes all the tests
                      I_TI  =  0 :  does not write
                      I_TI  =  1 :  writes test for MODULE SPECIFIC_INT_1
                      I_TI  =  2 :  writes test for MODULE SPECIFIC_INT_2
                      I_TI  =  3 :  writes test for MODULE SPECIFIC_INT_3
                      I_TI  =  4 :  writes test for MODULE SPECIFIC_INT_4
                      I_TI  =  5 :  writes test for MODULE SPECIFIC_INT_5
                      I_TI  =  6 :  writes test for MODULE SPECIFIC_INT_6
                      I_TI  =  7 :  writes test for MODULE SPECIFIC_INT_7
                      I_TI  =  8 :  writes test for MODULE SPECIFIC_INT_8
                      I_TI  =  9 :  writes test for MODULE SPECIFIC_INT_9
                      I_TI  = 10 :  writes test for MODULE TEST_INT_HUBBARD



     Description of the calculation parameters:

 *=======+=========+=========+=========+=========+===========================*
 *                           GENERAL PARAMETERS :                            *
 *=======+=========+=========+=========+=========+===========================*

       * Q_MIN    : minimum value of q (in units of k_F)
       * Q_MAX    : maximum value of q (in units of k_F)
       * N_Q      : number of q-points

       * E_MIN    : minimum value of energy (in units of E_F)
       * E_MAX    : maximum value of energy (in units of E_F)
       * N_E      : number of E-points

       * R_MIN    : minimum value of distance r (in units of 1/k_F)
       * R_MAX    : maximum value of distance r (in units of 1/k_F))
       * N_R      : number of r-points

 *-------+---------+---------+---------+---------+---------------------------*

       * RS       : average distance between 2 electrons (in units of a0)
                                            ~
       * MSOM     : m*/m (for semiconductors)
                                            ~
       * MAT_TYP  : type of material
                       MAT_TYPE  = 'SCHRO' standard solid
                       MAT_TYPE  = 'DIRAC' massless Fermions 
                       MAT_TYPE  = 'WATER' liquid water
                                            ~
       * EPS_B    : material's dielectric constant

 *-------+---------+---------+---------+---------+---------------------------*

       * T        : temperature (in SI)
                                            ~
       * E        : external electric field (in SI)
                                            ~
       * H        : external magnetic field (in SI)
                                            ~
       * FLD      : strength of the magnetic field
                       FLD = 'NO' no field        
                       FLD = 'WF' weak field        
                       FLD = 'IF' intermediate field
                       FLD = 'LF' large field

 *-------+---------+---------+---------+---------+---------------------------*

       * DIM      : dimension of the system
                       DIM  = '3D' 
                       DIM  = '2D' 
                       DIM  = '1D' 
                       DIM  = 'Q0'  quasi-0D
                       DIM  = 'Q1'  quasi-1D
                       DIM  = 'Q2'  quasi-2D
                       DIM  = 'BL'  bilayer
                       DIM  = 'ML'  multilayer
                       DIM  = 'ML'  multilayer

                                            ~

       * R0       : wire radius
                                            ~
       * L        : length of quantum well
                                            ~
       * OM0      : frequency of the confinement potential (SI)
                                            ~
       * CONFIN   : type of confinement
                       CONFIN = 'NO-CONF' no confinement 
                       CONFIN = 'DSEPLAY' layer within a stacking of layers
                       CONFIN = 'CC-1111' cylindrical within subband 1
                       CONFIN = 'CC-1122' cylindrical between subbands 1 and 2
                       CONFIN = 'CC-1221' cylindrical between subbands 1 and 2
                       CONFIN = 'CC-2222' cylindrical within subband 2
                       CONFIN = 'HC-1111' harmonic within subband 1
                       CONFIN = 'HC-1122' harmonic between subbands 1 and 2
                       CONFIN = 'HC-1221' harmonic between subbands 1 and 2
                       CONFIN = 'HC-2222' harmonic within subband 2
                       CONFIN = 'INVLAYE' inversion layer in semiconductor
                       CONFIN = 'IQWE_LB' square well with an infinite barrier
                       CONFIN = 'PC1_QWI' parabolic 
                       CONFIN = 'PC2_QWI' parabolic
                       CONFIN = 'SOFTCOR' soft-core potential
                       CONFIN = 'SWC_QWI' square well with an infinite barrier
 
                                            ~

       * DL       : interlayer distance 
                                            ~
       * D1       : distance between the two layers in the unit cell
                                            ~
       * N_DEP    : electron concentration in depletion layer (SI)
                                            ~
       * N_INV    : electron concentration in inversion layer (SI)
 
                                            ~

       * H_TYPE   : heterostructure type
                       H_TYPE  =  'SSL1' semiconductor superlattice of type I
                       H_TYPE  =  'SSL2' semiconductor superlattice of type II
                       H_TYPE  =  'BILA' bilayer
                       H_TYPE  =  'MLA1' multilayer with with one layer  / unit cell
                       H_TYPE  =  'MLA2' multilayer with with two layers / unit cell
                                            ~
       * EPS_1    : background/layer dielectric constant
                                            ~
       * EPS_2    : interlayer dielectric constant

 *-------+---------+---------+---------+---------+---------------------------*
 
       * UNIT     : system unit  
                       UNIT  = 'SIU' international system
                       UNIT  = 'CGS' CGS system
                       UNIT  = 'ATU' atomic units
 
       * UNIK     : K unit  
                       UNIK  = 'SI' international system
                       UNIK  = 'AU' atomic units

 *-------+---------+---------+---------+---------+---------------------------*
 
       * SC_TYPE  : type of screeening
                       SC_TYPE  = 'NO' no screening
                       SC_TYPE  = 'DH' Debye-Hückel
                       SC_TYPE  = 'KL' Kleinman
                       SC_TYPE  = 'ST' Streitenberger
                       SC_TYPE  = 'TF' Thomas-Fermi
                       SC_TYPE  = 'UI' Utsumi-Ichimaru
                       SC_TYPE  = 'YT' Yasuhara-Takada

 *-------+---------+---------+---------+---------+---------------------------*
 
       * PL_TYPE  : type of plasma considered
                      PL_TYPE  = 'OCP' --> one-component plasma (~ electron gas)
                      PL_TYPE  = 'DCP' --> two-component plasma
 
       * ZION     : atomic number of (first) ion
 
       * ZION2    : atomic number of second ion

 *=======+=========+=========+=========+=========+===========================*
 *                        DIELECTRIC FUNCTION :                              *
 *=======+=========+=========+=========+=========+===========================*


       * ESTDY    : static vs dynamic dielectric function
                      STDY = ' STATIC'
                      STDY = 'DYNAMIC'

       * EPS_T    : type of dielcectric function
                       EPS_T  = 'LONG' longitudinal --> D_FUNCL
                       EPS_T  = 'TRAN' transverse   --> D_FUNCT

       * D_FUNC   : model of dielectric function :

                    1) Static:

                       ---> longitudinal:

                      D_FUNCL = 'LRPA' random phase approximation          (3D,2D,1D)
                      D_FUNCL = 'THFE' Thomas-Fermi approximation          (3D,2D,1D)

                    2) Dynamic:

                       ---> transverse:

                      D_FUNCT = 'RPA1' random phase approximation          (3D,2D)
                      D_FUNCT = 'RPA2' random phase approximation          (3D)
                      D_FUNCT = 'LVLA' linearized Vlasov                   (3D)
                      D_FUNCT = 'MER1' Mermin                              (3D)
                      D_FUNCT = 'BLTZ' Boltzmann                           (3D)

                       ---> longitudinal:

                      D_FUNCL = 'ATAS' Atwal-Ashcroft                      <-- T-dependent
                      D_FUNCL = 'BLZ1' Boltzmann
                      D_FUNCL = 'BLZ2' damped Boltzmann
                      D_FUNCL = 'DACA' Arista-Brandt                       <-- T-dependent
                      D_FUNCL = 'HEAP' Hertel-Appel
                      D_FUNCL = 'HAFO' Hartree-Fock        
                      D_FUNCL = 'HUCO' Hu-O'Connell                        <-- damping (3D,2D,Q1)
                      D_FUNCL = 'HYDR' hydrodynamic                        <-- damping
                      D_FUNCL = 'KLEI' Kleinman                            <-- T-dependent
                      D_FUNCL = 'KLKD' Klimontovich-Kraeft                 <-- T-dependent
                      D_FUNCL = 'KLKN' Klimontovich-Kraeft                 <-- T-dependent
                      D_FUNCL = 'LAND' Landau parameters-based
                      D_FUNCL = 'LVL1' linearized Vlasov (weak coupling)   <-- T-dependent
                      D_FUNCL = 'LVL2' linearized Vlasov (strong coupling) <-- T-dependent
                      D_FUNCL = 'MER1' Mermin 1                            <-- damping
                      D_FUNCL = 'MER2' Mermin 2                            <-- T-dependent
                      D_FUNCL = 'MSAP' mean spherical approximation
                      D_FUNCL = 'NEVA' Nevanlinna                          <-- T-dependent
                      D_FUNCL = 'PLPO' plasmon pole
                      D_FUNCL = 'RDF1' Altshuler et al                     <-- damping
                      D_FUNCL = 'RDF2' Altshuler et al                     <-- damping
                      D_FUNCL = 'RPA1' RPA
                      D_FUNCL = 'RPA2' RPA                                 <-- T-dependent
                      D_FUNCL = 'RPA3' random phase approximation          <-- magnetic field (2D)
                      D_FUNCL = 'UTIC' Utsumi-Ichimaru                     <-- T-dependent
                      D_FUNCL = 'VLFP' Vlasov-Fokker-Planck                <-- damping

       * NEV_TYPE : type of Nevalinna function used (only for D_FUNCL = 'NEVA')
                      NEV_TYPE  = 'NONE' --> no function
                      NEV_TYPE  = 'STA1' --> static value h(q)
                      NEV_TYPE  = 'STA2' --> static value h(q)
                      NEV_TYPE  = 'CLCO' --> Classical Coulomb OCP
                      NEV_TYPE  = 'AMTA' --> Adamjan-Meyer-Tkachenko
                      NEV_TYPE  = 'PEEL' --> Perel'-Eliashberg function
                      NEV_TYPE  = 'PE76' --> Perel'-Eliashberg by Arkhipov et al

                                            ~

       * I_T      :  way to incorporate temperature dependence
                      I_T  = 0  --> no temperature (T = 0)
                      I_T  = 1  --> analytical models used
                      I_T  = 2  --> temperature convolution

 *-------+---------+---------+---------+---------+---------------------------*

       * PL_DISP   : method used to compute the plasmon dispersion (3D real case)
                       PL_DISP = 'HYDRODY' hydrodynamic model
                       PL_DISP = 'RPA_MOD' RPA model
                       PL_DISP = 'TWA_MOD' Toigo-Woodruff model
                       PL_DISP = 'GOA_MOD' Gorobchenko model
                       PL_DISP = 'HUBBARD' Hubbard model
                       PL_DISP = 'ELASTIC' elastic model
                       PL_DISP = 'SGBBN_M' SGBBN model            
                       PL_DISP = 'UTI_MOD' Utsumi-Ichimaru model
       * PL_DISP   : method used to compute the plasmon dispersion (2D real case)
                       PL_DISP = 'HYDRODY' hydrodynamic model
                       PL_DISP = 'RPA_MOD' RPA model
       * PL_DISP   : method used to compute the plasmon dispersion (1D real case)
                       PL_DISP = 'HYDRODY' hydrodynamic model
                       PL_DISP = 'RPA_MOD' RPA model

 *-------+---------+---------+---------+---------+---------------------------*

       * GSTDY    : static vs dynamic local-field corrections
                      GSTDY = ' STATIC'  --> GQ_TYPE
                      GSTDY = 'DYNAMIC'  --> GQO_TYPE 

       * GQ_TYPE  : local-field correction type (3D) static
                       GQ_TYPE = 'NONE' no local field correction
                       GQ_TYPE = 'ALDA' adiabatic local density
                       GQ_TYPE = 'ALFL' Alvarellos-Flores
                       GQ_TYPE = 'BEBR' Bedell-Brown
                       GQ_TYPE = 'CDOP' TDDFT Corradini et al correction
                       GQ_TYPE = 'GEV2' Geldart-Vosko 2
                       GQ_TYPE = 'GEVO' Geldart-Vosko correction
                       GQ_TYPE = 'GOCA' Gold-Calmels
  temperature-dep. --> GQ_TYPE = 'HNCA' hypernetted chain
                       GQ_TYPE = 'HORA' Holas-Rahman
                       GQ_TYPE = 'HUBB' Hubbard correction (only exchange)
                       GQ_TYPE = 'ICUT' Ichimaru-Utsumi correction
                       GQ_TYPE = 'IKPA' Iwamoto-Krotscheck-Pines
                       GQ_TYPE = 'IWA1' Iwamoto G_{-1}
                       GQ_TYPE = 'IWA2' Iwamoto G_{3} approx.
  temperature-dep. --> GQ_TYPE = 'IWA3' Iwamoto G_{-1}  
                       GQ_TYPE = 'IWA4' Iwamoto G_{3} exact
                       GQ_TYPE = 'JGDG' Jung-Garcia-Gonzalez-Dobson-Godby
                       GQ_TYPE = 'KLLA' Kleinman-Langreth correction
                       GQ_TYPE = 'LDAC' LDA correction
                       GQ_TYPE = 'MCSC' Moroni-Ceperley-Senatore correction
                       GQ_TYPE = 'NAGY' Nagy correction
                       GQ_TYPE = 'PAVA' Pavas-Vashishta correction
                       GQ_TYPE = 'PGGA' Petersilka-Gossmann-Gross
                       GQ_TYPE = 'RICE' Rice correction
                       GQ_TYPE = 'SHAW' Shaw correction
                       GQ_TYPE = 'SLAT' Slater correction
                       GQ_TYPE = 'STLS' Singwi et al correction
  temperature-dep. --> GQ_TYPE = 'TKAC' Tkachenko correction
                       GQ_TYPE = 'TRMA' Tripathy-Mandal
                       GQ_TYPE = 'VASI' Vashishta-Singwi correction
                       GQ_TYPE = 'UTI1' Utsumi-Ichimaru correction (only exchange)

       * GQ_TYPE  : local-field correction type (2D) static
                       GQ_TYPE = 'NONE' no local field correction
                       GQ_TYPE = 'BUTO' Bulutay-Tomak
                       GQ_TYPE = 'DPGT' Davoudi-Giuliani-Giuliani-Tosi
                       GQ_TYPE = 'GOCA' Gold-Calmels
                       GQ_TYPE = 'HUBB' Hubbard correction (only exchange)
                       GQ_TYPE = 'IWA1' Iwamoto G_{-1}
                       GQ_TYPE = 'IWA2' Iwamoto G_{3}
                       GQ_TYPE = 'SAIC' Sato-Ichimaru correction

       * GQ_TYPE  : local-field correction type (1D) static
                       GQ_TYPE = 'NONE' no local field correction
                       GQ_TYPE = 'GOCA' Gold-Calmels
                       GQ_TYPE = 'HUBB' Hubbard correction (only exchange)
 
       * IQ_TYPE  : type of approximation for I(q)
                       IQ_TYPE  = 'GKM' Gorobchenko-Kohn-Maksimov
                       IQ_TYPE  = 'HKA' Hong-Kim
                       IQ_TYPE  = 'IKP' Iwamoto-Krotscheck-Pines parametrization
                       IQ_TYPE  = 'KU1' Kugler 1
                       IQ_TYPE  = 'KU2' Kugler 1

                                            ~

       * LANDAU  : model chosen for the calculation of the Landau parameters (3D)
                       LANDAU = 'NONE' Landau's theory not used
                       LANDAU = 'CHEN' Chen's approach
                       LANDAU = 'RASC' Rayleigh-Schrödinger expansion
                       LANDAU = 'ANBR' Anderson-Brinkman model
                       LANDAU = 'GUTZ' Gutzwiller model
                       LANDAU = 'IWPI' Iwamoto-Pines model (hard-sphere)
                       LANDAU = 'GCYO' Giuliani-Vignale parametrization of 
                                        Yasuhara-Ousaka approach
                       LANDAU = 'SBOH' slave-boson one-band Hubbard model

       * LANDAU  : model chosen for the calculation of the Landau parameters (2D)
                       LANDAU = 'NONE' Landau's theory not used
                       LANDAU = 'ERZA' Engelbrecht-Randeria-Zhang approach
                       LANDAU = 'GVYO' Giuliani-Vignale parametrization of 
                                        Yasuhara-Ousaka approach
                       LANDAU = 'KCMP' Kwoon-Ceperley-Martin parametrization
       * GQO_TYPE : local-field correction type (3D)
                       GQO_TYPE = 'NONE' no local field correction
                       GQO_TYPE = 'ALFL' Alvarellos-Flores correction
                       GQO_TYPE = 'BACA' Barriga-Carrasco correction
                       GQO_TYPE = 'BBSA' Bachlechner-Böhm-Schinner
                       GQO_TYPE = 'COPI' Constantin-Pitarke
                       GQO_TYPE = 'DABR' Dabrowski
                       GQO_TYPE = 'FWRA' Forstmann-Wierling-Röpke
                       GQO_TYPE = 'HOK1' Hong-Kim correction
                       GQO_TYPE = 'HOK2' Hong-Kim correction
                       GQO_TYPE = 'JEWS' Jewsbury approximation
                       GQO_TYPE = 'KUG1' Kugler q --> 0 approximation
                       GQO_TYPE = 'KUG2' Kugler approximation
                       GQO_TYPE = 'MDGA' Mithen-Daligault-Gregori
                       GQO_TYPE = 'NLGA' Nagy-Laszlo-Giber approximation
                       GQO_TYPE = 'RIA1' Richardson-Ashcroft G_s
                       GQO_TYPE = 'RIA2' Richardson-Ashcroft G_n
                       GQO_TYPE = 'RIA3' Richardson-Ashcroft G_a
                       GQO_TYPE = 'SHMU' Shah-Mukhopadhyay 
                       GQO_TYPE = 'TOWO' Toigo-Woodruff
                       GQO_TYPE = 'UTI2' Utsumi-Ichimaru approximation
                       GQO_TYPE = 'VISC' viscosity approximation
 
 *-------+---------+---------+---------+---------+---------------------------*

       * DAMPING : type of damping used
                       DAMPING = 'NONE' no damping
                       DAMPING = 'LFTM' lifetime
                       DAMPING = 'RELA' relaxation time
                       DAMPING = 'DECA' decay rate
                       DAMPING = 'DIFF' diffusion coefficient
                       DAMPING = 'VISC' viscosity

       * LT_TYPE    : approximation used for lifetime (3D)
                       LT_TYPE = 'DAVI' Davies formula
                       LT_TYPE = 'GALI' Galitskii formula
                       LT_TYPE = 'GIQU' Giuliani-Quinn formula
                       LT_TYPE = 'GIVI' Giuliani-Vignale formula 
                       LT_TYPE = 'INPE' Inogamov-Petrov formula
                       LT_TYPE = 'LUBR' Lugovskoy-Bray formula
                       LT_TYPE = 'NAEC' Nagy-Echenique formula
                       LT_TYPE = 'QIVI' Qian-Vignale formula
                       LT_TYPE = 'QUFE' Quinn-Ferrell formula

       * LT_TYPE    : approximation used for lifetime (2D)
                       LT_TYPE = 'GIQ1' Giuliani-Quinn formula for e-h loss
                       LT_TYPE = 'GIQ2' Giuliani-Quinn formula for plasmon loss
                       LT_TYPE = 'GIVI' Giuliani-Vignale formula
                       LT_TYPE = 'HAWR' Hawrylak formula
                       LT_TYPE = 'MELA' Menashe-Laikhtman formula
                       LT_TYPE = 'QIVI' Qian-Vignale formula

       * RT_TYPE  : relaxation time
                       RT_TYPE = ' NO'   --> no relaxation time
                       RT_TYPE = 'E-E'   --> electron-electron interaction
                       RT_TYPE = 'E-P'   --> electron-phonon interaction
                       RT_TYPE = 'E-I'   --> electron-phonon impurity
                       RT_TYPE = 'ALL'   --> all three taken into account

                                            ~
 
       * DR_TYPE  : decay rate in 3D
                       DR_TYPE = 'UTIC'   --> Utsumi-Ichimaru approximation
                       DR_TYPE = 'VLAS'   --> Vlasov approximation

       * DC_TYPE  : diffusion coefficient in 3D
                       DC_TYPE = 'ASHO'   --> Ashurst-Hoover

       * VI_TYPE  : viscosity in 3D
                       VI_TYPE = 'AMPP' Angilella et al hard-sphere fluid     --> T-dependent
                       VI_TYPE = 'DRBA' Daligault-Rasmussen-Baalrud (plasmas) --> T-dependent
                       VI_TYPE = 'KHRA' Khrapak for Yukawa fluid  --> T-dependent
                       VI_TYPE = 'LHPO' Longuet-Higgins-Pope      --> T-dependent
                       VI_TYPE = 'SCHA' Schäfer                   --> T-dependent
                       VI_TYPE = 'SCHD' Schäfer (dynamic)         --> T-dependent
                       VI_TYPE = 'SHTE' Shternin                  --> T-dependent
                       VI_TYPE = 'STEI' Steinberg low-temperature --> T-dependent

       * VI_TYPE  : viscosity in 2D
                       VI_TYPE = 'SCHA' Schäfer                     --> T-dependent
         graphene <--  VI_TYPE = 'KISC' Kiselev-Schmalian (dynamic) --> T-dependent
         graphene <--  VI_TYPE = 'MSFA' Müller-Schmalian-Fritz      --> T-dependent  

                                            ~

       * EE_TYPE  : e-e relaxation time in 3D
                       RT_TYPE = 'ALAR'   --> Al'tshuler-Aronov (e-e + impurities)
                       RT_TYPE = 'BACA'   --> Barriga-Carrasco approximation (e-e)
                       RT_TYPE = 'FSTB'   --> Fann et al approximation       (e-e)
                       RT_TYPE = 'QIVI'   --> Qian-Vignale                   (e-e)
                       RT_TYPE = 'RASM'   --> Rammer-Smith                   (e-e)
                       RT_TYPE = 'UTIC'   --> Utsumi-Ichimaru approximation  (e-e)
                       RT_TYPE = 'TAIC'   --> Tanaka-Ichimaru approximation  (e-e)


       * EE_TYPE  : relaxation time in 2D
                       EE_TYPE = 'FUAB'   --> Fukuyama-Abrahams (disordered metals)
          graphene <-- EE_TYPE = 'LUFO'   --> Lucas-Fong      (e-e)
                       EE_TYPE = 'QIVI'   --> Qian-Vignale    (e-e)
                       EE_TYPE = 'RASM'   --> Rammer-Smith    (e-e)
  heterostructures <-- EE_TYPE = 'REWI'   --> Reizer-Wilkins  (e-e)
                       EI_TYPE = 'SHAS'   --> Sharma-Ashraf   (e-e + impurities)
                       EE_TYPE = 'ZHDA'   --> Zhang-Das Sarma (e-e)

       * EE_TYPE  : relaxation time in 1D
                       EI_TYPE = 'SHAS'   --> Sharma-Ashraf   (e-e + impurities)

       * EP_TYPE  : e-phonon relaxation time in 3D
                       EP_TYPE = 'STEL'   --> Steinberg low-temperature  
                       EP_TYPE = 'STEH'   --> Steinberg High-temperature

       * EI_TYPE  : e-impurit relaxation time in 3D
                       EI_TYPE = 'HEAP'   --> Hertel-Appel approximation

                                            ~

       * IP_TYPE  : ion plasma relaxation time in 3D
                       IP_TYPE = 'SEMO'   --> Selchow-Morawetz approximation
                       IP_TYPE = 'SPIT'   --> Spitzer approximation 

       * PD_TYPE  : method used to compute the plasmon damping (3D)
                       PD_TYPE = 'NONE'   --> no plasmon damping
                       PD_TYPE = 'CALL'   --> Callen approximation
                       PD_TYPE = 'DGKA'   --> DuBois-Gilinsky-Kivelson approximation
                       PD_TYPE = 'FEWA'   --> Fetter and Walecka approximation
                       PD_TYPE = 'JEWS'   --> Jewsbury approximation
                       PD_TYPE = 'LITI'   --> Giuliani-Quinn lifetime approximation
                       PD_TYPE = 'MOPE'   --> Molinari-Peerani approximation
                       PD_TYPE = 'NPSA'   --> Ninham-Powel-Swanson approximation
                       PD_TYPE = 'SGAA'   --> Segui-Gervasoni-Arista approximation

                                            ~

       * QD_TYPE  : method used to compute q-dependent relaxation time
                       QD_TYPE = 'NONE'   --> no q-dependence
                       QD_TYPE = 'GAUS'   --> Gaussian
                       QD_TYPE = 'LORE'   --> Lorentzian

       * ZETA     : Value of Tanaka-Ichimaru parameter

 *-------+---------+---------+---------+---------+---------------------------*

       * INT_POT  : type of interaction potential (3D)
                       INT_POT = 'COULO' Coulomb interaction
                       INT_POT=  'YUKAW' Yukawa interaction
                       INT_POT=  'RPAPO' RPA interaction
                       INT_POT = 'OVER1' Overhauser interaction
                       INT_POT = 'OVER2' modified Overhauser interaction
                       INT_POT = 'DEUTS' Deutsch interaction
                       INT_POT = 'PHOLE' particle-hole interaction
                       INT_POT = 'KELBG' Kelbg interaction

        * S       :  

        * EPS     :  

 *-------+---------+---------+---------+---------+---------------------------*

       * EK       :  electron kinetic energy (eV)
       * EP_C     :  electron-phonon coupling
       * DEBYE_T  :  material Debye temperature

                                            ~

       * NA       :  number of atoms per unit volume
       * MA       :  mass of the atoms
       * RA       :  radius of the atoms

 *-------+---------+---------+---------+---------+---------------------------*

        * NI      :  impurity concentration
        * EI_C    :  strength of impurity scattering

 *-------+---------+---------+---------+---------+---------------------------*


        * CF_TYPE  : type of classical fluid calculation         --> 3D
                       CF_TYPE = 'SHS' smooth hard spheres
                       CF_TYPE = 'RH1' rough hard spheres (Pidduck)
                       CF_TYPE = 'RH2' rough hard spheres (Condiff-Lu-Dahler)
                       CF_TYPE = 'RH3' rough hard spheres (McCoy-Sandler-Dahler)
                       CF_TYPE = 'DCE' dilute Chapman-Enskog
                       CF_TYPE = 'HCE' heavy (i.e. dense) Chapman-Enskog
                       CF_TYPE = 'LJF' Lennard-Jones fluid

                       CF_TYPE = 'DHD' dense hard disks          --> 2D 

        * PF_TYPE  : type of packing fraction                    --> 2D    
                       PF_TYPE = 'HDM'   --> hard disk model

                                                                 --> 3D
                       PF_TYPE = 'HSM'   --> hard sphere model
                       PF_TYPE = 'RCP'   --> random closed-packed
                       PF_TYPE = 'FCC'   --> FCC closed-packed
                       PF_TYPE = 'FRE'   --> freezing
                       PF_TYPE = 'MEL'   --> melting

        * SL_TYPE  : type of scattering length calculation
                       SL_TYPE = 'HSP'   --> hard sphere potential
                       SL_TYPE = 'ASW'   --> attractive square well (without bound state)
                       SL_TYPE = 'RSW'   --> repulsive square well
                       SL_TYPE = 'DSP'   --> delta-shell potential
                       SL_TYPE = 'AYP'   --> attractive Yukawa potential
                       SL_TYPE = 'CCO'   --> Coulomb cut-off potential
                       SL_TYPE = 'HUL'   --> Hulthén potential

 *=======+=========+=========+=========+=========+===========================*
 *                        STRUCTURE FACTOR :                                 *
 *=======+=========+=========+=========+=========+===========================*


       * SSTDY    : static vs dynamic local-field corrections
                      SSTDY = ' STATIC'  --> SQ_TYPE
                      SSTDY = 'DYNAMIC'  --> SQO_TYPE 

       * SQ_TYPE   : structure factor approximation (3D) --> static
                       SQ_TYPE  = 'DEH' Debye-Hückel approximation
                       SQ_TYPE  = 'GEA' generalized approximation
                       SQ_TYPE  = 'GOR' Gorobchenko approximation
                       SQ_TYPE  = 'GR2' computed from g(r) (GR_TO_SQ.f code)
                       SQ_TYPE  = 'GSB' Gori-Giorgi-Sacchetti-Bachelet approximation
                       SQ_TYPE  = 'HFA' Hartree-Fock approximation (only exchange)
                       SQ_TYPE  = 'HUB' Hubbard approximation
                       SQ_TYPE  = 'ICH' Ichimaru approximation
                       SQ_TYPE  = 'MSA' mean spherical approximation
                       SQ_TYPE  = 'PKA' Pietiläinen-Kallio
                       SQ_TYPE  = 'RPA' RPA approximation
                       SQ_TYPE  = 'SHA' Shaw approximation
                       SQ_TYPE  = 'SIN' Singh
                       SQ_TYPE  = 'SPA' Singh-Pathak
                       SQ_TYPE  = 'TWA' Toigo-Woodruff approximation
! 
       * SQO_TYPE   : structure factor approximation (3D) --> dynamic
                       SQO_TYPE  = 'ABA' Arista-Brandt approximation
                       SQO_TYPE  = 'HFA' Hartree-Fock approximation
                       SQO_TYPE  = 'HYD' hyrodynamic approximation
                       SQO_TYPE  = 'IGA' ideal gas approximation
                       SQO_TYPE  = 'ITA' Ichimaru-Tanaka approximation
                       SQO_TYPE  = 'MFA' Hansen-McDonald-Pollock approximation
                       SQO_TYPE  = 'NIC' Nakano-Ichimaru approximation
                       SQO_TYPE  = 'UTI' Utsumi-Ichimaru approximation (3D)
                       SQO_TYPE  = 'VLA' linearized Vlasov approximation
 
 *=======+=========+=========+=========+=========+===========================*
 *                        PAIR CORRELATION FUNCTION :                        *
 *=======+=========+=========+=========+=========+===========================*

       * GR_TYPE     : pair correlation function approximation g(r) (3D)
                       GR_TYPE  = 'CDF' from chain diagram formula of PDF (long distance)
                       GR_TYPE  = 'DHA' Debye-Hückel approximation 
                       GR_TYPE  = 'DWA' DeWitt approximation 
                       GR_TYPE  = 'FBA' Frieman-Book approximation 
                       GR_TYPE  = 'HFA' Hartree-Fock approximation (only exchange)
                       GR_TYPE  = 'HUB' Hubbard approximation
                       GR_TYPE  = 'LLA' Lee-Long approximation
                       GR_TYPE  = 'ORB' Ortiz-Ballone approximation
                       GR_TYPE  = 'PDF' from pair distribution function 
                       GR_TYPE  = 'SHA' Shaw approximation
                       GR_TYPE  = 'SQ2' computed from S(q) (SQ_TO_GR.f code)
                       GR_TYPE  = 'WIG' Wigner approximation
 
       * GR0_MODE : g(0) (3D)
                       GR0_MODE  = 'CAGO' --> Calmels-Gold
                       GR0_MODE  = 'DPGT' --> Davoudi-Polini-Giuliani-Tosi
                       GR0_MODE  = 'HASA' --> Holas-Aravind-Singwi (small r_s)
                       GR0_MODE  = 'ICHI' --> Ichimaru
                       GR0_MODE  = 'KIMB' --> Kimball
                       GR0_MODE  = 'OVE1' --> Overhauser 1
                       GR0_MODE  = 'OVE2' --> Overhauser 2
                       GR0_MODE  = 'QIAN' --> Qian
       * GR0_MODE : g(0) (2D)
                       GR0_MODE  = 'CAGO' --> Calmels-Gold
                       GR0_MODE  = 'HAFO' --> Hartree-Fock 
                       GR0_MODE  = 'MOMA' --> Moreno-Marinescu 
                       GR0_MODE  = 'NSOA' --> Nagano-Singwi-Ohnishi
                       GR0_MODE  = 'QIAN' --> Qian
 
 *=======+=========+=========+=========+=========+===========================*
 *                        PAIR DISTRIBUTION FUNCTION :                       *
 *=======+=========+=========+=========+=========+===========================*
 
       * RH_TYPE     : pair distribution function approximation (3D)
                       RH_TYPE  = 'CDI' chain diagram improved
                       RH_TYPE  = 'CEG' classical electron gas
                       RH_TYPE  = 'DEB' Debye electron gas 
                       RH_TYPE  = 'FUA' correct to order 2 in epsilon
                       RH_TYPE  = 'SDC' short-distance correlations
                       RH_TYPE  = 'WDA' watermelon diagrams summed
 
 *=======+=========+=========+=========+=========+===========================*
 *                         ENERGY CALCULATIONS :                             *
 *=======+=========+=========+=========+=========+===========================*

       * EC_TYPE  : type of correlation energy functional (3D)
                       EC_TYPE = 'GEBR_W'   --> Gell-Mann and Brueckner
                       EC_TYPE = 'CAMA_W'   --> Carr and Maradudin
                       EC_TYPE = 'EHTY_S'   --> Endo-Horiuchi-Takada-Yasuhara
                       EC_TYPE = 'HELU_W'   --> Hedin and Lundqvist
                       EC_TYPE = 'VBHE_W'   --> von Barth and Hedin
                       EC_TYPE = 'PEZU_W'   --> Perdew and Zunger
                       EC_TYPE = 'WIGN_S'   --> Wigner
                       EC_TYPE = 'NOPI_S'   --> Nozières and Pines
                       EC_TYPE = 'LIRO_S'   --> Lindgren and Rosen
                       EC_TYPE = 'PEZU_S'   --> Perdew and Zunger
                       EC_TYPE = 'REHI_S'   --> Rebei and Hitchon
                       EC_TYPE = 'GGSB_G'   --> Gori-Giorgi-Sacchetti-Bachelet
                       EC_TYPE = 'PRKO_G'   --> Proynov and Kong
                       EC_TYPE = 'VWNU_G'   --> Vosko, Wilk and Nusair
                       EC_TYPE = 'PEWA_G'   --> Perdew and Wang
                       EC_TYPE = 'HUBB_G'   --> Hubbard
                       EC_TYPE = 'CHAC_G'   --> Chachiyo
                       EC_TYPE = 'ISKO_T'   --> Isihara and Kojima
       * EC_TYPE  : type of correlation energy functional (2D)
                       EC_TYPE = 'TACE_G'   --> Tanatar-Ceperley
                       EC_TYPE = 'CPPA_G'   --> Seidl-Perdew_Levy
                       EC_TYPE = 'AMGB_G'   --> Attaccalite-Moroni-Gori-Giorgi-Bachelet
                       EC_TYPE = 'SEID_G'   --> Seidl
                       EC_TYPE = 'LOOS_W'   --> Loos
                       EC_TYPE = 'WIGN_S'   --> Wigner
                       EC_TYPE = 'ISTO_T'   --> Isihara-Toyoda
       * EC_TYPE  : type of correlation energy functional (1D)
                       EC_TYPE = 'LOOS_W'   --> Loos
                       EC_TYPE = 'WIGN_S'   --> Wigner
 
 
       * FXC_TYPE : type of XC free energy functional           --> 3D
                       FXC_TYPE = 'NO'   --> None
                       FXC_TYPE = 'EB'   --> Ebeling et al
                       FXC_TYPE = 'IC'   --> Ichimaru et al
                       FXC_TYPE = 'KS'   --> Karasiev et al
                       FXC_TYPE = 'VS'   --> Vashishta and Singwi
                       FXC_TYPE = 'PD'   --> Perrot and Dharma-Wardana
                       FXC_TYPE = 'EK'   --> Ebeling-Kraeft-Kremp-Röpke

       * EXC_TYPE : type of exchange-correlation energy functional   --> 3D
                       EXC_TYPE = 'NO'   --> None
                       EXC_TYPE = 'GT'   --> Goedeker-Tetter-Hutter
                       EXC_TYPE = 'ST'   --> 
                       EXC_TYPE = 'BD'   --> Brown-DuBois-Holzmann-Ceperley

                                            ~

 
 *=======+=========+=========+=========+=========+===========================*
 *                         SPIN POLARIZATION :                               *
 *=======+=========+=========+=========+=========+===========================*
 
       * IMODE    : choice of spin parameters
                       IMODE = 1 : no spin polarization 
                       IMODE = 2 : fully spin-polarized
             
       * XI       : spin polarization : (n+ - n-) / n

 *=======+=========+=========+=========+=========+===========================*
 *                        THERMODYNAMIC PROPERTIES :                         *
 *=======+=========+=========+=========+=========+===========================*
 
       * TH_PROP   : type of calculation  --> thermodynamic properties
                      TH_PROP  = 'CLAS' : classical approximation 
                      TH_PROP  = 'QUAN' : quantum approximation 

       * GP_TYPE  : grand partition function type (3D)
                       GP_TYPE = 'IK0' Isihara-Kojima formulation
                       GP_TYPE = 'RH0' Rebei-Hitchon formulation
                       GP_TYPE = 'IKM' Isihara-Kojima with magnetic field

       * GP_TYPE  : grand partition function type (2D)
                       GP_TYPE = 'I20' Isihara-Kojima formulation
                       GP_TYPE = 'I2M' Isihara-Kojima with magnetic field
 
 *=======+=========+=========+=========+=========+===========================*
 *                        INCOMING ION BEAM :                                *
 *=======+=========+=========+=========+=========+===========================*

       * Z_BEAM    :  charge of ions in incoming beam
       * EK_BEAM   :  kinetic energy of incoming beam ions (eV)

 *=======+=========+=========+=========+=========+===========================*
 *                        OUTPUT CALCULATIONS :                              *
 *=======+=========+=========+=========+=========+===========================*
 
       * I_DF      : switch for dielectric function printing
                      I_DF  = 0 : dielectric function not printed
                      I_DF  = 1 : dielectric function printed in file 'diel_func.dat'
 
       * I_PZ      : switch for polarization function printing
                      I_PZ  = 0 : polarization function not printed
                      I_PZ  = 1 : polarization function printed in file 'pola_func.dat'
 
       * I_SU      : switch for susceptibility function printing
                      I_SU  = 0 : susceptibility function not printed
                      I_SU  = 1 : susceptibility function printed in file 'susc_func.dat'
 
       * I_CD      : switch for electrical conductivity printing
                      I_CD  = 0 : electrical conductivity not printed
                      I_CD  = 1 : electrical conductivity printed in file 'cond_func.dat'
 
 *-------+---------+---------+---------+---------+---------------------------*
 
       * I_PD      : switch for plasmon dispersion printing
                      I_PD  = 0 :  plasmon dispersion not calculated
                      I_PD  = 1 :  plasmon dispersion printed in file 'plas_disp.dat'
 
       * I_EH      : switch for electron-hole dispersion printing
                      I_EH  = 0 :  electron-hole dispersion not calculated
                      I_EH  = 1 :  electron-hole dispersion printed in file 'elec_hole.dat'
 
       * I_E2      : switch for two-electron-hole dispersion printing
                      I_E2  = 0 :  two-electron-hole dispersion not calculated
                      I_E2  = 1 :  two-electron-hole dispersion printed in file 'elec_hol2.dat'
 
       * I_CK      : switch for k-space e-e interaction potential printing
                      I_CK  = 0 :  potential not calculated
                      I_CK  = 1 :  potential printed in file 'int_pot_k.dat'
 
       * I_CR      : switch for real-space e-e interaction potential printing
                      I_CR  = 0 :  potential not calculated
                      I_CR  = 1 :  potential printed in file 'int_pot_r.dat'
 
       * I_SA      : switch for scattering amplitude printing
                      I_SA  = 0 :  scattering amplitude not calculated
                      I_SA  = 1 :  scattering amplitude printed in file 'scat_ampl.dat'
 
 *-------+---------+---------+---------+---------+---------------------------*
 
       * I_LF      : switch for local-field corrections G(q,omega) printing
                      I_LF  = 0 : local-field corrections not calculated
                      I_LF  = 1 : local-field corrections printed in file 'loca_fiel.dat'
 
       * I_IQ      : switch for G(q,inf) printing
                      I_IQ  = 0 : G(q,inf) not calculated
                      I_IQ  = 1 : G(q,inf) printed in file 'ginf_fiel.dat'
 
       * I_SF      : switch for structure factor S(q,omega) printing
                      I_SF  = 0 : structure factor not calculated
                      I_SF  = 1 : structure factor printed in file 'stru_fact.dat'
 
       * I_PC      : switch for pair correlation function g(r) printing
                      I_PC  = 0 :  pair correlation function not calculated
                      I_PC  = 1 :  pair correlation function printed in file 'pair_corr.dat'
 
                                            ~
 
       * I_P2      : switch for pair distribution rho2(r) printing
                      I_P2  = 0 :  pair distribution function not calculated
                      I_P2  = 1 :  pair distribution function printed in file 'pair_dist.dat'
 
       * I_VX      : switch for  vertex function Gamma(q,omega) printing
                      I_VX  = 0 :  vertex function not calculated
                      I_VX  = 1 :  vertex function printed in file 'vertex_fu.dat'
 
       * I_DC      : switch for plasmon damping coefficient Im[eps]/q^2 printing
                      I_DC  = 0 :  plasmon damping not calculated
                      I_DC  = 1 :  plasmon damping printed in file 'plas_damp.dat'
 
       * I_MD      : switch for momentum distribution printing
                      I_MD  = 0 :  momentum distribution not calculated
                      I_MD  = 1 :  momentum distribution printed in file 'mome_dist.dat'
 
                                            ~
 
       * I_LD      : switch for Landau parameters printing
                      I_LD  = 0 : Landau parameters not calculated
                      I_LD  = 1 : Landau parameters printed in file 'landau_pa.dat'
 
       * I_DP      : switch for damping printing
                      I_DP  = 0 : damping not calculated
                      I_DP  = 1 : damping printed in file 'damp_file.dat'
 
       * I_LT      : switch for plasmon lifetime printing
                      I_LT  = 0 : plasmon lifetime not calculated
                      I_LT  = 1 : plasmon lifetime printed in file 'life_time.dat'
 
       * I_BR      : switch for plasmon broadening printing
                      I_BR  = 0 : plasmon broadening not calculated
                      I_BR  = 1 : plasmon broadening printed in file 'broadenin.dat'
 
                                            ~
 
       * I_PE      : switch for plasmon energy printing
                      I_PE  = 0 : plasmon energy not calculated
                      I_PE  = 1 : plasmon energy printed in file 'plas_ener.dat'
 
       * I_QC      : switch for plasmon q-bounds printing
                      I_QC  = 0 : plasmon q-bounds not calculated
                      I_QC  = 1 : plasmon q-bounds printed in file 'qc_bounds.dat'
 
       * I_RL      : switch for relaxation time printing
                      I_RL  = 0 : relaxation time not calculated
                      I_RL  = 1 : relaxation time printed in file 'rela_time.dat'
 
       * I_TF      : switch for Thomas-Fermi wave vector printing
                      I_TF  = 0 : Thomas-Fermi wave vector not calculated
                      I_TF  = 1 : Thomas-Fermi wave vector printed in file 'thomas-fe.dat'
 
                                            ~
 
       * I_DY      : switch for Debye wave vector printing
                      I_DY  = 0 : Debye wave vector not calculated
                      I_DY  = 1 : Debye wave vector printed in file 'debye_wav.dat'
 
       * I_ME      : switch for moments of epsilon(q,omega) printing
                      I_ME  = 0 : moments of epsilon not calculated
                      I_ME  = 1 : moments of epsilon printed in file 'moments_e.dat'
 
       * I_MS      : switch for moments of S(q,omega) printing
                      I_MS  = 0 : moments of structure factor not calculated
                      I_MS  = 1 : moments of structure factor printed in file 'moments_s.dat'
 
       * I_ML      : switch for moments of loss function printing
                      I_ML  = 0 : moments of loss function not calculated
                      I_ML  = 1 : moments of loss function printed in file 'moments_l.dat'
 
                                            ~
 
       * I_MC      : switch for moments of conductivity printing
                      I_MC  = 0 : moments of conductivity not calculated
                      I_MC  = 1 : moments of conductivity printed in file 'moments_c.dat'
 
       * I_DE      : switch for derivative of Re[ dielectric function ] printing
                      I_DE  = 0 : derivative not calculated
                      I_DE  = 1 : derivative printed in file 'deri_epsi.dat'
 
       * I_ZE      : switch for Re[ dielectric function ] = 0 printing
                      I_ZE  = 0 : function not calculated
                      I_ZE  = 1 : function printed in file 'ree0_file.dat'
 
       * I_SR      : switch for sum rules for epsilon printing
                      I_SR  = 0 : sum rules not calculated
                      I_ST  = 1 : sum rules printed in file 'sum_rules.dat'
 
                                            ~
 
       * I_CW      : switch for confinement wave function printing
                      I_CW  = 0 : confinement wave function not calculated
                      I_CW  = 1 : confinement wave function printed in file 'confin_wf.dat'
 
       * I_CF      : switch for confinement potential printing
                      I_CF  = 0 : confinement potential not calculated
                      I_CF  = 1 : confinement potential printed in file 'confin_pt.dat'
 
       * I_EM      : switch for effective mass printing
                      I_EM  = 0 : effective mass not calculated
                      I_EM  = 1 : effective mass printed in file 'effe_mass.dat'
 
       * I_MF      : switch for mean free path printing
                      I_MF  = 0 : mean free path not calculated
                      I_MF  = 1 : mean free path printed in file 'mean_path.dat'
 
                                            ~
 
       * I_SP      : switch for spectral function printing
                      I_SP  = 0 : spectral function not calculated
                      I_SP  = 1 : spectral function printed in file 'spec_func.dat'
 
       * I_SE      : switch for self-energy printing
                      I_SE  = 0 : self-energy not calculated
                      I_SE  = 1 : self-energy printed in file 'self_ener.dat'
 
       * I_NV      : switch for Nevanlinaa function printing
                      I_NV  = 0 : Nevanlinaa function not calculated
                      I_NV  = 1 : Nevanlinaa function printed in file 'nevanlina.dat'
 
       * I_ES      : switch for Eliashberg function printing
                      I_ES  = 0 : Eliashberg function not calculated
                      I_ES  = 1 : Eliashberg function printed in file 'elia_func.dat'
 
                                            ~
 
       * I_GR      : switch for Grüneisen parameter printing
                      I_GR  = 0 : Grüneisen parameter not calculated
                      I_GR  = 1 : Grüneisen parameter printed in file 'grune_par.dat'
 
       * I_FD      : switch for Fermi-Dirac distribution printing
                      I_FD  = 0 : Fermi-Dirac distribution not calculated
                      I_FD  = 1 : Fermi-Dirac distribution printed in file 'fermi_dir.dat'
 
       * I_BE      : switch for Bose-Einstein distribution printing
                      I_BE  = 0 : Bose-Einstein distribution not calculated
                      I_BE  = 1 : Bose-Einstein distribution printed in file 'bose_eins.dat'
 
       * I_MX      : switch for Maxwell distribution printing
                      I_MX  = 0 : Maxwell distribution not calculated
                      I_MX  = 1 : Maxwell distribution printed in file 'maxwell_d.dat' 
 
                                            ~
 
       * I_SC      : switch for scale parameters  printing
                      I_SC  = 0 : scale parameters not calculated
                      I_SC  = 1 : scale parameters printed in file 'scale_par.dat'
 
       * I_DS      : switch for density of states printing
                      I_DS  = 0 : density of states not calculated
                      I_DS  = 1 : density of states printed in file 'dens_stat.dat'
 
       * I_SB      : switch for  subband energies printing
                      I_SB  = 0 : subband energies not calculated
                      I_SB  = 1 : subband energies printed in file 'subb_ener.dat'
 
 *-------+---------+---------+---------+---------+---------------------------*
 
       * I_GP      : switch for grand partition function printing
                      I_GP  = 0 : grand partition function not calculated
                      I_GP  = 1 : grand partition function printed in file 'gran_part.dat'
 
       * I_PR      : switch for electronic pressure printing
                      I_PR  = 0 : electronic pressure not calculated
                      I_PR  = 1 : electronic pressure printed in file 'epressure.dat'
 
       * I_CO      : switch for compressibility printing
                      I_CO  = 0 : compressibility not calculated
                      I_CO  = 1 : compressibility printed in file 'comp_file.dat'
 
       * I_CP      : switch for chemical potential printing
                      I_CP  = 0 : chemical potential not calculated
                      I_CP  = 1 : chemical potential printed in file 'chem_pote.dat' 
 
                                            ~
 
       * I_BM      : switch for bulk modulus printing
                      I_BM  = 0 : bulk modulus not calculated
                      I_BM  = 1 : bulk modulus printed in file 'bulk_modu.dat'
 
       * I_SH      : switch for shear modulus printing
                      I_SH  = 0 : shear modulus not calculated
                      I_SH  = 1 : shear modulus printed in file 'shear_mod.dat'
 
       * I_S0      : switch for zero sound velocity printing
                      I_S0  = 0 : zero sound velocity not calculated
                      I_S0  = 1 : zero sound velocity printed in file 'zero_soun.dat' 
 
       * I_S1      : switch for first sound velocity printing
                      I_S1  = 0 : first sound velocity not calculated
                      I_S1  = 1 : first sound velocity printed in file 'firs_soun.dat' 
 
                                            ~
 
       * I_DT      : switch for Debye temperature printing
                      I_DT  = 0 : Debye temperature not calculated
                      I_DT  = 1 : Debye temperature printed in file 'Debye_tmp.dat'
 
       * I_PS      : switch for Pauli paramagnetic susceptibility printing
                      I_PS  = 0 : Pauli paramagnetic susceptibility not calculated
                      I_PS  = 1 : Pauli paramagnetic susceptibility printed in file 'para_susc.dat'
 
       * I_IE      : switch for internal energy printing
                      I_IE  = 0 : internal energy not calculated
                      I_IE  = 1 : internal energy printed in file 'inter_ene.dat'
 
       * I_EI      : switch for excess internal energy printing
                      I_EI  = 0 : excess internal energy not calculated
                      I_EI  = 1 : excess internal energy printed in file 'exces_ene.dat'
 
                                            ~
 
       * I_FH      : switch for Helmholtz free energy printing
                      I_FH  = 0 : Helmholtz free energy not calculated
                      I_FH  = 1 : Helmholtz free energy printed in file 'helm_free.dat'
 
       * I_EY      : switch for entropy printing
                      I_EY  = 0 : entropy not calculated
                      I_EY  = 1 : entropy printed in file 'entropy_f.dat'
 
 *-------+---------+---------+---------+---------+---------------------------*
 
       * I_EF      : switch for Fermi energy printing
                      I_EF  = 0 : Fermi energy not calculated
                      I_EF  = 1 : Fermi energy printed in file 'fermi_ene.dat'
 
       * I_KF      : switch for Fermi momentum printing
                      I_KF  = 0 : Fermi momentum not calculated
                      I_KF  = 1 : Fermi momentum printed in file 'fermi_vec.dat'
 
       * I_VF      : switch for Fermi velocity printing
                      I_VF  = 0 : Fermi velocity not calculated
                      I_VF  = 1 : Fermi velocity printed in file 'fermi_vel.dat'
 
       * I_TE      : switch for Fermi temperature printing
                      I_TE  = 0 : Fermi temperature not calculated
                      I_TE  = 1 : Fermi temperature printed in file 'fermi_tmp.dat'
 
                                            ~
 
       * I_DL      : switch for density of states at Fermi level printing
                      I_DL  = 0 subband energies: density of states at Fermi level not calculated
                      I_DL  = 1 : density of states at Fermi level printed in file 'fermi_dos.dat'
 
 *-------+---------+---------+---------+---------+---------------------------*
 
       * I_TW      : switch for thermal De Broglie wavelength printing
                      I_TW  = 0 : thermal De Broglie wavelength not calculated
                      I_TW  = 1 : thermal De Broglie wavelength printed in file 'thermal_w.dat'
 
       * I_VT      : switch for thermal velocity printing
                      I_VT  = 0 : thermal velocity not calculated
                      I_VT  = 1 : thermal velocity printed in file 'thermal_v.dat'
 
       * I_TC      : switch for thermal conductivity printing
                      I_TC  = 0 : thermal conductivity not calculated
                      I_TC  = 1 : thermal conductivity printed in file 'thermal_c.dat'
 
 *-------+---------+---------+---------+---------+---------------------------*
 
       * I_EG      : switch for ground state energy printing
                      I_EG  = 0 : ground state energy not calculated
                      I_EG  = 1 : ground state energy printed in file 'ground_st.dat'
 
       * I_EX      : switch for exchange energy printing
                      I_EX  = 0 : exchange energy not calculated
                      I_EX  = 1 : exchange energy printed in file 'ex_energy.dat'
 
       * I_XC      : switch for exchange correlation energy printing
                      I_XC  = 0 : exchange correlation energy not calculated
                      I_XC  = 1 : exchange correlation energy printed in file 'xc_energy.dat'
 
       * I_EC      : switch for correlation energy printing
                      I_EC  = 0 : correlation energy not calculated
                      I_EC  = 1 : correlation energy printed in file 'corr_ener.dat'
 
                                            ~
 
       * I_HF      : switch for Hartree-Fock energy printing
                      I_HF  = 0 : Hartree-Fock energy not calculated
                      I_HF  = 1 : Hartree-Fock energy printed in file 'hf_energy.dat'
 
       * I_EK      : switch for kinetic energy printing
                      I_EK  = 0 : kinetic energy not calculated
                      I_EK  = 1 : kinetic energy printed in file 'kine_ener.dat'
 
       * I_EP      : switch for potential energy printing
                      I_EP  = 0 : potential energy not calculated
                      I_EP  = 1 : potential energy printed in file 'pote_ener.dat
 
 *-------+---------+---------+---------+---------+---------------------------*
 
       * I_VI      : switch for shear viscosity printing
                      I_VI  = 0 : shear viscosity not calculated
                      I_VI  = 1 : shear viscosity printed in file 'visc_coef.dat'
 
       * I_DI      : switch for diffusion coefficient printing
                      I_DI  = 0 : diffusion coefficient not calculated
                      I_DI  = 1 : diffusion coefficient printed in file 'diff_coef.dat'
 
 *-------+---------+---------+---------+---------+---------------------------*
 
       * I_FP      : switch for fluctuation potential calculation
                      I_FP  = 0 :  fluctuation potential not calculated
                      I_FP  = 1 :  fluctuation potential printed in file 'fluct_pot.dat'
 
       * I_EL      : switch for loss function calculation
                      I_EL  = 0 :  loss function not calculated
                      I_EL  = 1 :  loss function printed in file 'ener_loss.dat'
 
       * I_PO      : switch for stopping power calculation
                      I_PO  = 0 :  stopping power not calculated
                      I_PO  = 1 :  stopping power printed in file 'stop_powe.dat'
 
       * I_RF      : switch for refractive index calculation
                      I_RF  = 0 :  refractive index not calculated
                      I_RF  = 1 :  refractive index printed in file 'refrac_in.dat' 
 
                                            ~
 
       * I_VC      : switch for dynamic screened Coulomb potential V(q,omega) calculation
                      I_RF  = 0 : dynamic screened Coulomb potential not calculated
                      I_RF  = 1 : dynamic screened Coulomb potential printed in file 'dyna_coul.dat'
 
 *-------+---------+---------+---------+---------+---------------------------*
 
       * I_FN      : switch for appending the calculation type string 
                        to the output filename
                      I_FN  = 0 :  standard output filename
                      I_FN  = 1 :  parameter added to filename
 
       * I_WR      : switch for writing physical properties into the log file 
                      I_WR  = 0 :  does not write
                      I_WR  = 1 :  writes
