#!/bin/bash
#
#  Replaces STRING1 by STRING2 in all the files
#    named FILENAME*.dat
#
#
#  Usage: replace STRING1 STRING2 FILENAME
#
#
name=$3*.dat
#
sed -i "s/`echo $1`/`echo $2`/g" `echo $name`
#
exit
 
