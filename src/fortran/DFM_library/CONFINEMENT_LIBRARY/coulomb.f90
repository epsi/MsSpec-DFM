!
!=======================================================================
!
MODULE COULOMB_K 
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE COULOMB_FF(DMN,UNIT,Q,KS,V_C)
!
!  This subroutine computes Coulomb potentials in the k-space 
!    in all dimensions, including the form factors 
!    to account for various confinements
!
!
!  Input parameters:
!
!       * DMN      : dimension of the system
!       * UNIT     : system unit
!       * Q        : wave vector                               in UNIT
!       * KS       : screening wave vector                     in UNIT
!
!
!  Output variables :
!
!       * V_C      : Coulomb potential in k-space (in UNIT)
!
!
!       
!  Note: In the coding of the different form factors,  
!          we have taken the following convention:
!
!           3D : V(q) = e^2 / (EPS_0 * q^2)  * FF   [SI]
!                       4 pi e^2 / q^2       * FF   [CGS]
!       
!       Q2D,2D : V(q) = e^2 / (2*EPS_0 * q)  * FF   [SI]
!                       2 pi e^2 / q         * FF   [CGS]
!       
!       Q1D,1D : V(q) = e^2 / EPS_0          * FF   [SI]
!                       4 pi e^2             * FF   [CGS] 
!
!      
!   Author :  D. Sébilleau
!
!                                           Last modified :  5 Oct 2020
!
!
      USE REAL_NUMBERS,     ONLY : FOUR,HALF
      USE PI_ETC,           ONLY : PI
      USE CONFIN_VAL,       ONLY : R0
      USE CONFINEMENT_FF,   ONLY : CONFIN_FF
      USE CONSTANTS_P1,     ONLY : E,EPS_0
      USE FERMI_SI,         ONLY : KF_SI
      USE FERMI_AU,         ONLY : KF_AU
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  UNIT
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP)             ::  Q,KS
      REAL (WP)             ::  V_C
      REAL (WP)             ::  X,COEF,Q2,KS2,QKS2
      REAL (WP)             ::  FF
!
      REAL (WP)             ::  SQRT
!
!  Unit-dependent factors
!
      IF(UNIT == 'SIU') THEN                                        !
        COEF = E * E / EPS_0                                        !
        X    = HALF * Q / KF_SI                                     ! X = q / (2 * k_F) 
      ELSE IF(UNIT == 'CGS') THEN                                   !
        COEF = FOUR * PI * E * E                                    !
        X    = HALF * Q / KF_AU                                     ! X = q / (2 * k_F) 
      END IF                                                        !
!
      Q2   = Q * Q                                                  !
      KS2  = KS * KS                                                !
      QKS2 = Q2 + KS2                                               !
!
      IF(DMN == '3D') THEN                                          !
!
        V_C = COEF / QKS2                                           !
!          
      ELSE IF(DMN == '2D') THEN                                     !
!
        FF  = CONFIN_FF(X)                                          !
        V_C = HALF * COEF * FF / SQRT(QKS2)                         !
!
      ELSE IF(DMN == 'Q2') THEN                                     !
!
        FF  = CONFIN_FF(X)                                          !
        V_C = HALF * COEF * FF / SQRT(QKS2)                         !
!
      ELSE IF(DMN == 'Q1') THEN                                     !
!
        FF  = CONFIN_FF(X)                                          !
        V_C = COEF * FF                                             !
!          
      ELSE IF(DMN == '1D') THEN                                     !
!
        FF  = CONFIN_FF(X)                                          !
        V_C = COEF * FF                                             !
!
      END IF                                                        !
!
      END SUBROUTINE COULOMB_FF  
!
END MODULE COULOMB_K
