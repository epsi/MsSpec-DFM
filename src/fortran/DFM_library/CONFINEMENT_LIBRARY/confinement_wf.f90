!
!=======================================================================
!
MODULE CONFINEMENT_WF 
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      FUNCTION CONFIN_WF(R,TH,SB_I,OM0,N_DEP,N_INV,EPSS)
!
!  This function computes the confinement wave function
!
!
!
!  Input parameters:
!
!       * R        : radial parameter
!       * TH       : polar angle
!       * SB_I     : sub-band index (0, 1, 2, ...)  --> limited to 10
!       * N_DEP    : electron concentration in depletion layer (SI)
!       * N_INV    : electron concentration in inversion layer (SI)
!       * EPSS     : dielectric constant of the semiconductor
!       * OM0      : frequency of the confinement potential (SI)
!
!  Intermediate parameters:
!
!       * CONFIN   : type of confinement
!                       CONFIN = 'INF_QWW' 
!                       CONFIN = 'INVLAYE'
!                       CONFIN = 'IQWE_LB'
!                       CONFIN = 'PC1_QWI'
!                       CONFIN = 'PC2_QWI'
!                       CONFIN = 'SWC_QWI'
!       * L        : length of the quantum well (SI)
!
!
!  Output parameters:
!
!       * CONFIN_WF  : wave function
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 10 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO
      USE CONFIN_VAL,       ONLY : CONFIN,R0,L 
!
      IMPLICIT NONE
!
      INTEGER, INTENT(IN)   ::  SB_I
!
      REAL (WP), INTENT(IN) ::  R,TH,OM0
      REAL (WP), INTENT(IN) ::  N_DEP,N_INV,EPSS
      REAL (WP)             ::  CONFIN_WF
!
      IF(CONFIN == 'NO-CONF') THEN                                  !
        CONFIN_WF = ZERO                                            !
      ELSE IF(CONFIN == 'INF_QWW') THEN                             !
        CONFIN_WF = INF_QWW_WF(R,TH,R0,SB_I)                        !
      ELSE IF(CONFIN == 'INVLAYE') THEN                             !
        CONFIN_WF = INVLAYE_WF(R,N_DEP,N_INV,EPSS)                  !
      ELSE IF(CONFIN == 'IQWE_LB') THEN                             !
        CONFIN_WF = IQWE_LB_WF(R,L)                                 !
      ELSE IF(CONFIN == 'PC1_QWI') THEN                             !
        CONFIN_WF = PC1_QWI_WF(R,OM0,SB_I)                          !
      ELSE IF(CONFIN == 'PC2_QWI') THEN                             !
        CONFIN_WF = PC2_QWI_WF(R,OM0)                               !
      ELSE IF(CONFIN == 'SWC_QWI') THEN                             !
        CONFIN_WF = SWC_QWI_WF(R,L)                                 !
      END IF                                                        !
    
!
      END FUNCTION CONFIN_WF
!
!=======================================================================
!
      FUNCTION INF_QWW_WF(R,TH,R0,SB_I)
!
!  This function computes the wave function in the two lower subbands 
!    of a quantum-well wire.
!
!  Reference: (1) A. Gold and A. Ghazali, Phys. Rev. B 41, 7626 (1990)
!
!
!  Input parameters:
!
!       * R        : radial parameter
!       * TH       : polar angle
!       * R0       : radius of the quantum wire (same unit as R)
!       * SB_I     : sub-band index
!
!  Output parameters:
!
!       * INF_QWW_FF : form factor
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 21 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,THREE
      USE COMPLEX_NUMBERS
      USE PI_ETC,           ONLY : PI_INV
!
      IMPLICIT NONE
!
      INTEGER               ::  SB_I
!
      REAL (WP), INTENT(IN) ::  R,TH,R0
      REAL (WP)             ::  R02,R03,R2,R3
      REAL (WP)             ::  COEF
!
      REAL (WP)             ::  SQRT
!
      COMPLEX (WP)          ::  INF_QWW_WF
!
      COMPLEX (WP)          ::  CMPLX,EXP
!
      R02 = R0  * R0                                                !
      R03 = R02 * R0                                                !
      R2  = R  * R                                                  !
      R3  = R2 * R                                                  !
!
!
      IF(SB_I == 1) THEN                                            !
        IF(R < R0) THEN                                             !
          COEF       = SQRT(THREE * PI_INV / R02)                   !
          INF_QWW_WF = CMPLX(COEF * (ONE - R2 / R02),KIND=WP)       !
        ELSE                                                        !
          INF_QWW_WF = ZEROC                                        !
        END IF                                                      !
      ELSE IF(SB_I == 2) THEN                                       !
        IF(R < R0) THEN                                             !
          COEF = SQRT(12.0E0_WP * PI_INV / R02)                     !
          INF_QWW_WF = COEF * (R / R0 - R3 / R03) * EXP(IC * TH)    !
        ELSE                                                        !
          INF_QWW_WF = ZEROC                                        !
        END IF                                                      !
      END IF                                                        !
!
      END FUNCTION INF_QWW_WF
!
!=======================================================================
!
      FUNCTION INVLAYE_WF(Z,N_DEP,N_INV,EPSS)
!
!  This function computes the wave function of the surface inversion layer
!    of a semiconductor in the z direction
!
!  Reference: (1) M. Jonson, J. Phys. C: Solid State Phys. 9, 3055-3071 (1976)
!
!
!  Input parameters:
!
!       * Z        : z coordinate 
!       * N_DEP    : electron concentration in depletion layer (SI)
!       * N_INV    : electron concentration in inversion layer (SI)
!       * EPSS     : dielectric constant of the semiconductor
!
!  Output parameters:
!
!       * INVLAYE_WF : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 21 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : HALF,THIRD
      USE PI_ETC,           ONLY : PI
      USE CONSTANTS_P1,     ONLY : H_BAR,E,M_E
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  Z,N_DEP,N_INV,EPSS
      REAL (WP)             ::  INVLAYE_WF
      REAL (WP)             ::  B,B3
      REAL (WP)             ::  NUM,DEN
!
      REAL (WP)             ::  SQRT,EXP
!
!  Computation of parameter B
!
      NUM = 48.0E0_WP * PI * E * E * M_E                            !
      DEN = EPSS * H_BAR * H_BAR                                    !
!
      B = (NUM * ( N_DEP + 11.0E0_WP *                            & !
                   N_INV / 32.0E0_WP ) / DEN)**THIRD                ! ref. 1 eq. (21)
!
      B3 = B * B * B                                                !
!
      INVLAYE_WF = SQRT(HALF * B3) * Z * EXP(- HALF * B * Z)        ! ref. 1 eq. (19)
!
      END FUNCTION INVLAYE_WF
!
!=======================================================================
!
      FUNCTION IQWE_LB_WF(Z,L)
!
!  This function computes the z-axis wave function of the a quantum well
!    with an infinite barrier when only the lower subband is filled
!
!  Reference: (1) T. Vazifehshenas and T. Salavati-fard, 
!                    Physica E 41, 1297–1300 (2009)
!
!
!  Input parameters:
!
!       * Z        : z value
!       * L        : length of the quantum well (SI)
!
!  Output parameters:
!
!       * IQWE_LB_WF : form factor
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 21 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : TWO
      USE PI_ETC,           ONLY : PI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  Z,L
      REAL (WP)             ::  IQWE_LB_WF
!
      REAL (WP)             ::  SQRT,SIN
!
      IQWE_LB_WF = SQRT(TWO / L) * SIN(PI * Z / L)                  !
!
      END FUNCTION IQWE_LB_WF
!
!=======================================================================
!
      FUNCTION PC1_QWI_WF(Y,OM0,N)
!
!  This function computes the wave function of a quantum wire under 
!    an harmonic confinement potential of the form 1/2 m omega_0^2 y^2
!    in the y direction
!
!  Reference: (1) M. Tas, PhD thesis, Middle East Technical University (2004)
!
!
!  Input parameters:
!
!       * Y        : parameter in the confinement direction
!       * OM0      : frequency of the confinement potential (SI)
!       * N        : sub-band index (0, 1, 2, ...)  --> limited to 10
!
!  Output parameters:
!
!       * PC1_QWI_WF : form factor
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 21 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,HALF
      USE PI_ETC,           ONLY : PI
      USE CONSTANTS_P1,     ONLY : H_BAR,M_E
      USE EXT_FUNCTIONS,    ONLY : H_POLYNOMIAL_VALUE
!
      IMPLICIT NONE
!
      INTEGER, INTENT(IN)   ::  N
!
      REAL (WP), INTENT(IN) ::  Y,OM0
      REAL (WP)             ::  PC1_QWI_WF
      REAL (WP)             ::  B,YB,ZZ,COEF
      REAL (WP)             ::  FC(0:10),X(1),P(1,0:10)
!
      REAL (WP)             ::  SQRT,EXP
!
      DATA  FC       /      1.0E0_WP,       1.0E0_WP,     2.0E0_WP,  & !   
                            6.0E0_WP,      24.0E0_WP,   120.0E0_WP,  & ! factorials
                          720.0E0_WP,    5040.0E0_WP, 40320.0E0_WP,  & !
                       362880.0E0_WP, 3628800.0E0_WP              /    !
!
      IF(N > 10) THEN                                               !
        WRITE(6,10)                                                 !
        STOP                                                        !
      END IF                                                        !
!
!  Characteristic length of the harmonic potential 
!   (serves as effective diameter of quantum wire)
!
      B = SQRT(H_BAR / (M_E * OM0))                                 !
!
      YB = Y / B                                                    !
!
      ZZ = HALF * YB * YB                                           !
!
      COEF = SQRT(ONE / (TWO**N * FC(N) * SQRT(PI * B)))            !
!
!  Computing the Hermite polynomial
!
      X(1) = YB                                                     !
      CALL H_POLYNOMIAL_VALUE(1,N,X,P)                              !
!
      PC1_QWI_WF = COEF * EXP(- ZZ) * P(1,N)                        ! ref. 1 eq. (3.67)
!
!  Format
!
  10  FORMAT(//,5X,'<<<<<  SUB-BAND INDEX TOO LARGE  >>>>>',//)
!
      END FUNCTION PC1_QWI_WF
!
!=======================================================================
!
      FUNCTION PC2_QWI_WF(R,OM0)
!
!  This function computes the wave function of a quantum wire under 
!    an harmonic confinement potential of the form 1/8 m omega_0^2 (x^2+y^2)
!    in the (x,y) direction (lowest sub-band only)
!
!  Reference: (1) M. Tas, PhD thesis, Middle East Technical University (2004)
!
!
!  Input parameters:
!                                                                           ->
!       * R        : parameter in the confinement directions (projection of r 
!                      onto the(xy) plane)
!       * OM0      : frequency of the confinement potential (SI)
!
!  Output parameters:
!
!       * PC2_QWI_WF : form factor
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 21 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,FOURTH
      USE PI_ETC,           ONLY : PI
      USE CONSTANTS_P1,     ONLY : H_BAR,M_E
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  R,OM0
      REAL (WP)             ::  PC2_QWI_WF
      REAL (WP)             ::  B,ZZ,COEF
!
      REAL (WP)             ::  SQRT,EXP
!
!  Characteristic length of the harmonic potential 
!   (serves as effective diameter of quantum wire)
!
      B = SQRT(H_BAR / (M_E * OM0))                                 !
!
      ZZ = FOURTH * R * R / (B * B)                                 !
!
      COEF =ONE / SQRT(TWO * PI * B * B)                            !
!
      PC2_QWI_WF = COEF * EXP(- ZZ)                                 ! ref. 1 eq. (3.74)
!
      END FUNCTION PC2_QWI_WF
!
!=======================================================================
!
      FUNCTION SWC_QWI_WF(Y,A)
!
!  This function computes the wave function of the a quantum-well wire
!    with an infinite barrier when only the lowest subband is filled
!
!  The barrier is from -a/2 to a/2
!
!
!  Reference: (1) M. Tas, PhD thesis, Middle East Technical University (2004)
!
!
!  Input parameters:
!
!       * Y        : parameter along the y axis 
!       * A        : length of the quantum well (SI)
!
!  Output parameters:
!
!       * SWC_QWI_WF : form factor
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 21 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,TWO,HALF
      USE PI_ETC,           ONLY : PI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  Y,A
      REAL (WP)             ::  SWC_QWI_WF
!
      REAL (WP)             ::  COS
!
      IF( (- HALF * A <= Y) .AND. (Y <= HALF * A) ) THEN            !
        SWC_QWI_WF = TWO * COS(PI * Y / A) / A                      ! ref. 1 eq. (3.86)
      ELSE                                                          !
        SWC_QWI_WF = ZERO                                           !
      END IF                                                        !
!
      END FUNCTION SWC_QWI_WF
!
END MODULE CONFINEMENT_WF
