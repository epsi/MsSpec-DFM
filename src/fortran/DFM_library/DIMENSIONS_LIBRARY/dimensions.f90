!
!=======================================================================
!
MODULE DIMENSION_CODE
!
!  This module contains the dimensioning of the epsilon.f90 code
!
!
      IMPLICIT NONE
!
!
      INTEGER               ::  NSIZE                               ! max. number
!                                                                   ! of energy/momentum/r
      PARAMETER (NSIZE=5000)                                        ! points 
!
      INTEGER               ::  NZ_MAX                              ! max. number
!                                                                   ! of z/r/k 
      PARAMETER (NZ_MAX=2500)                                       ! integration points 
!
      INTEGER               ::  ND_MAX                              ! max. number
!                                                                   ! of derivation 
      PARAMETER (ND_MAX=2500)                                       ! points 
!
      INTEGER               ::  MAXITER                             ! max. number
!                                                                   ! of
      PARAMETER(MAXITER = 30)                                       ! iterations
!
      INTEGER               ::  NOFFN                               ! max. number
!                                                                   ! of
      PARAMETER(NOFFN = 92)                                         ! output fortran files
!                                                                   ! of
      INTEGER               ::  L_MAX                               !
!                                                                   ! max. number
      PARAMETER (L_MAX=50)                                          ! of
!                                                                   ! ang. momentum l
END MODULE DIMENSION_CODE
 
