!
!=======================================================================
!
MODULE CALL_CALC_5 
!
      USE ACCURACY_REAL
      USE CALCULATORS_5
      USE OUT_VALUES_5
!
!  This module calls the subroutines of calculator 5 whenever necessary
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE USE_CALC_5
!
      IMPLICIT NONE
!
!  Computing the Fermi properties 
!
      IF(I_EF == 1) THEN                                            !
        CALL CALC_EFF                                               !
      END IF                                                        !
      IF(I_KF == 1) THEN                                            !
        CALL CALC_KFF                                               !
      END IF                                                        !
      IF(I_VF == 1) THEN                                            !
        CALL CALC_VFF                                               !
      END IF                                                        !
      IF(I_TE == 1) THEN                                            !
        CALL CALC_TFF                                               !
      END IF                                                        !
      IF(I_DL == 1) THEN                                            !
        CALL CALC_NFF                                               !
      END IF                                                        !
!
      END SUBROUTINE USE_CALC_5
!
END MODULE CALL_CALC_5
 
