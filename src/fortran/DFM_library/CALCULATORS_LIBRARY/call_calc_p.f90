!
!=======================================================================
!
MODULE CALL_CALC_P 
!
      USE ACCURACY_REAL
      USE PLASMON_DISPERSION
      USE CALCULATORS_P
!
      USE OUT_VALUES_2,       ONLY : I_PD
      USE OUT_VALUES_P
!
!  This module calls the subroutines of calculators requesting 
!    post-processsing
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE USE_CALC_P
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 16 Dec 2020
!
!
      IMPLICIT NONE
!
!  Computing the plasmon dispersion
!
      IF(I_PD == 1 .AND. PL_DISP == '  EXACT') THEN                 ! 
        CALL CALC_EPD                                               !
      END IF                                                        !
!
!  Computing the fluctuation potential
!
      IF(I_FP == 1) THEN                                            ! 
        CALL CALC_FLP                                               !
      END IF                                                        !
!
      END SUBROUTINE USE_CALC_P
!
END MODULE CALL_CALC_P
 
