!
!=======================================================================
!
MODULE CALL_CALC_2 
!
      USE ACCURACY_REAL
      USE CALCULATORS_2
      USE OUT_VALUES_2
!
!  This module calls the subroutines of calculator 2 whenever necessary
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE USE_CALC_2(IQ,X)
!
!
!
!  Input parameters:
!
!       * IQ       : q index  
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  7 Oct 2020
!
!
      IMPLICIT NONE
!
      INTEGER               ::  IQ
!
      REAL (WP)             ::  X
!
!  Computing the electron-hole pairs continua
!
      IF(IQ == 1) THEN                                              !
        IF(I_EH == 1) THEN                                          !
          CALL CALC_EHD                                             !
        END IF                                                      !
        IF(I_E2 == 1) THEN                                          !
          CALL CALC_E2D                                             !
        END IF                                                      !
      END IF                                                        !
!
!  Computing the analytical plasmon dispersion 
!
      IF(I_PD == 1) THEN                                            !
        CALL CALC_PDI(X)                                            !
      END IF                                                        !
!
!  Computing the electron-electron interaction in k-space 
!
      IF(I_CK == 1) THEN                                            !
        CALL CALC_EEK(X)                                            !
      END IF                                                        !
!
!  Computing the electron-electron interaction in real space 
!
      IF(I_CR == 1) THEN                                            !
        CALL CALC_EER(X)                                            !
      END IF                                                        !
!
!  Computing the plasmon kinetic energy 
!
      IF(I_PK == 1) THEN                                            !
        CALL CALC_EKP(X)                                            !
      END IF                                                        !
!
      END SUBROUTINE USE_CALC_2
!
END MODULE CALL_CALC_2
 
