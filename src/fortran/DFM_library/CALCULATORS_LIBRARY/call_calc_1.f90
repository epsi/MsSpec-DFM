!
!=======================================================================
!
MODULE CALL_CALC_1 
!
      USE ACCURACY_REAL
      USE CALCULATORS_1
      USE OUT_VALUES_1
!
!  This module calls the subroutines of calculator 1 whenever necessary
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE USE_CALC_1(X,EN,EPSR,EPSI)
!
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!
!  Output parameters:
!
!       * E        : energy array
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 25 Sep 2020
!
!
      USE DIMENSION_CODE,   ONLY : NSIZE
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X
      REAL (WP)             ::  EPSR(NSIZE),EPSI(NSIZE),EN(NSIZE)
!
!  Computing the dielectric function
!
      IF(I_DF == 1) THEN                                            !
        CALL CALC_EPS(X,EN,EPSR,EPSI)                               !
      END IF                                                        !
!
!  Computing the polarization function
!
      IF(I_PZ == 1) THEN                                            !
        CALL CALC_POL(X)                                            !
      END IF                                                        !
!
!  Computing the susceptibility function
!
      IF(I_SU == 1) THEN                                            !
        CALL CALC_SUS(X)                                            !
      END IF                                                        !
!
!  Computing the conductivity function
!
      IF(I_CD == 1) THEN                                            !
        CALL CALC_CDV(X)                                            !
      END IF                                                        !
!
      END SUBROUTINE USE_CALC_1
!
END MODULE CALL_CALC_1
