!
!=======================================================================
!
MODULE CALL_CALC_8 
!
      USE ACCURACY_REAL
      USE CALCULATORS_8
      USE OUT_VALUES_8
!
!  This module calls the subroutines of calculator 8 whenever necessary
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE USE_CALC_8(X)
!
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Oct 2020
!
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X
!
!  Computing the shear viscosity
!
      IF(I_VI == 1) THEN                                            !
        CALL CALC_VIS(X)                                            !
      END IF                                                        !
!
!  Computing the diffusion coefficient
!
      IF(I_DI == 1) THEN                                            !
        CALL CALC_DIF                                               !
      END IF                                                        !
!
      END SUBROUTINE USE_CALC_8
!
END MODULE CALL_CALC_8
