!
!=======================================================================
!
MODULE CALCULATORS_7
!
      USE ACCURACY_REAL
!
!  This module contains the subroutines allowing to compute 
!    various properties of the electron/plasma liquids:
!
!          * Exchange energy               : CALC_EXX
!          * Exchange-correlation energy   : CALC_EXC
!          * correlation energy            : CALC_ECO
!          * kinetic energy                : CALC_KIN
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_EXX
!
!  This subroutine computes the exchange energy
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Dec 2020
!
!
!
      USE MATERIAL_PROP,            ONLY : RS,DMN
      USE EXT_FIELDS,               ONLY : T
      USE EXCHANGE_ENERGIES
!
      USE ENE_CHANGE,               ONLY : RYD
!
      USE ENERGIES,                 ONLY : EX_TYPE
      USE SPIN_POLARIZATION
!
      USE OUT_VALUES_7,             ONLY : I_EX
      USE PRINT_FILES,              ONLY : IO_EX
!
      REAL (WP)             ::  E_EX
!
      IF(DMN == '3D') THEN                                          !
        E_EX = EX_3D(EX_TYPE,IMODE,RS,T,XI)                         !
      ELSE IF(DMN == '2D') THEN                                     !
        CONTINUE                                                    ! not implemented yet
      ELSE IF(DMN == '1D') THEN                                     !
        CONTINUE                                                    ! not implemented yet
      END IF                                                        !
!
      IF(I_EX == 1) THEN                                            !
        WRITE(IO_EX,*) RS,T,E_EX * RYD                              ! exchange energy in eV
      END IF                                                        !
!
      END SUBROUTINE CALC_EXX
!
!=======================================================================
!
      SUBROUTINE CALC_EXC
!
!  This subroutine computes the exchange and correlation energy
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Dec 2020
!
!
!
      USE MATERIAL_PROP,            ONLY : RS,DMN
      USE EXT_FIELDS,               ONLY : T
      USE XC_ENERGIES
!
      USE ENE_CHANGE,               ONLY : RYD
!
      USE ENERGIES,                 ONLY : FXC_TYPE,EXC_TYPE
!
      USE OUT_VALUES_7,             ONLY : I_XC
      USE PRINT_FILES,              ONLY : IO_XC
!
      IMPLICIT NONE
!
      REAL (WP)             ::  E_XC
!
      IF(EXC_TYPE /= 'NO') THEN                                     !
!
!  Exchange and correlation energy functionals (EXC_TYPE)
!
        IF(DMN == '3D') THEN                                        !
          E_XC = EXC_3D(EXC_TYPE,RS,T)                              !
        ELSE IF(DMN == '2D') THEN                                   !
          CONTINUE                                                  ! not implemented yet
        ELSE IF(DMN == '1D') THEN                                   !
          CONTINUE                                                  ! not implemented yet
        END IF                                                      !
!
      ELSE                                                          !
!
!  Exchange and correlation free energy functionals (FXC_TYPE)
!
        IF(DMN == '3D') THEN                                        !
          CALL FXC_TO_EXC_3D(FXC_TYPE,RS,T,E_XC)                    !
        ELSE IF(DMN == '2D') THEN                                   !
          CONTINUE                                                  ! not implemented yet
        ELSE IF(DMN == '1D') THEN                                   !
          CONTINUE                                                  ! not implemented yet
        END IF                                                      !
!
      END IF
!
      IF(I_XC == 1) THEN                                            !
        WRITE(IO_XC,*) RS,T,E_XC * RYD                              ! XC energy in eV
      END IF                                                        !
!
      END SUBROUTINE CALC_EXC
!
!=======================================================================
!
      SUBROUTINE CALC_ECO
!
!  This subroutine computes the correlation energy
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Dec 2020
!
!
!
      USE MATERIAL_PROP,            ONLY : RS,DMN
      USE EXT_FIELDS,               ONLY : T
      USE CORRELATION_ENERGIES
!
      USE ENE_CHANGE,               ONLY : RYD
!
      USE ENERGIES,                 ONLY : EC_TYPE
      USE SPIN_POLARIZATION
!
      USE OUT_VALUES_7,             ONLY : I_EC
      USE PRINT_FILES,              ONLY : IO_EC
!
      IMPLICIT NONE
!
      REAL (WP)             ::  E_CORR
!
      IF(DMN == '3D') THEN                                          !
        E_CORR = EC_3D(EC_TYPE,IMODE,RS,T)                          !
      ELSE IF(DMN == '2D') THEN                                     !
        E_CORR = EC_2D(EC_TYPE,RS,T)                                !
      ELSE IF(DMN == '1D') THEN                                     !
        E_CORR = EC_1D(EC_TYPE,RS,T)                                !
      END IF                                                        !
!
      IF(I_EC == 1) THEN                                            !
        WRITE(IO_EC,*) RS,T,E_CORR * RYD                            ! correlation energy in eV
      END IF                                                        !
!
      END SUBROUTINE CALC_ECO
!
!=======================================================================
!
      SUBROUTINE CALC_KIN
!
!  This subroutine computes the kinetic energy
!
!
!  Reference: H. T. Tran and J. P. Perdew, Am. J. Phys. 1048-1061 (2003)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Dec 2020
!
!
!
      USE MATERIAL_PROP,            ONLY : RS,DMN
      USE EXT_FIELDS,               ONLY : T
      USE KINETIC_ENERGIES
!
      USE ENE_CHANGE,               ONLY : RYD
!
      USE ENERGIES,                 ONLY : EK_TYPE
      USE SPIN_POLARIZATION
!
      USE OUT_VALUES_7,             ONLY : I_EK
      USE PRINT_FILES,              ONLY : IO_EK
!
      IMPLICIT NONE
!
      REAL (WP)             ::  E_KIN
      REAL (WP)             ::  ALPHA
!
      IF(DMN == '3D') THEN                                          !
        E_KIN = EK_3D(EK_TYPE,RS,T,XI)                              !
      ELSE IF(DMN == '2D') THEN                                     !
        CONTINUE                                                    ! not implemented yet
      ELSE IF(DMN == '1D') THEN                                     !
        CONTINUE                                                    ! not implemented yet
      END IF                                                        !
!
      IF(I_EK == 1) THEN                                            !
        WRITE(IO_EK,*) RS,E_KIN * RYD                               ! kinetic energy in eV
      END IF                                                        !
!
      END SUBROUTINE CALC_KIN
!
END MODULE CALCULATORS_7
