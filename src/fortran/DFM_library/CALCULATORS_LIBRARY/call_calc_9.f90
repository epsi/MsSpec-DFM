!
!=======================================================================
!
MODULE CALL_CALC_9 
!
      USE ACCURACY_REAL
      USE CALCULATORS_9
      USE OUT_VALUES_9
!
!  This module calls the subroutines of calculator 9 whenever necessary
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE USE_CALC_9(X)
!
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 25 Sep 2020
!
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X
!
!  Computing the loss function
!
      IF(I_EL == 1) THEN                                            !
        CALL CALC_LOS(X)                                            !
      END IF                                                        !
!
!  Computing the screened Coulomb interaction V(q,omega)
!
      IF(I_VC == 1) THEN                                            !
        CALL CALC_VSC(X)                                            !
      END IF                                                        !
!
      END SUBROUTINE USE_CALC_9
!
END MODULE CALL_CALC_9
 
