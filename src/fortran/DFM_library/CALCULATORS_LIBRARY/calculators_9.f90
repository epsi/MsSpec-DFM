!
!=======================================================================
!
MODULE CALCULATORS_9
!
      USE ACCURACY_REAL
      USE CALCULATORS_1
!
!  This module contains the subroutines allowing to compute
!    various properties of the electron/plasma liquids:
!
!
!          * loss function              : CALC_LOS
!          * screened potential         : CALC_VSC
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_LOS(X)
!
!  This subroutine computes the loss function L(q, omega),
!    defined by
!                                _               _
!                               |      - 1        |
!              L(q, omega) = Im | --------------- |
!                               |_ EPS(q, omega) _|
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!
!
!  Intermediate parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * DMN      : problem dimension
!
!
!  Output parameters:
!
!       * E        : energy array
!       * VSCR     : real part of the screened potential
!       * VSCI     : imaginary part of the screened potential
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 15 Oct 2020
!
!
      USE DIMENSION_CODE,   ONLY : NSIZE
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,FOURTH,SMALL,TTINY,INF
      USE FERMI_SI,         ONLY : KF_SI
      USE DF_VALUES,        ONLY : ESTDY,EPS_T,D_FUNC
      USE MATERIAL_PROP,    ONLY : RS,DMN
      USE EXT_FIELDS,       ONLY : T,H
!
      USE E_GRID
      USE UNITS,            ONLY : UNIT
!
      USE DFUNC_STATIC
      USE DFUNCT_STAN_DYNAMIC
      USE DFUNCL_STAN_DYNAMIC
      USE DFUNCL_MAGN_DYNAMIC
      USE COULOMB_K
!
      USE OUT_VALUES_9,     ONLY : I_EL
      USE PRINT_FILES,      ONLY : IO_EL
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  D_FUNCL,D_FUNCT
!
      REAL (WP)             ::  X
      REAL (WP)             ::  REPS,IEPS,LOS
      REAL (WP)             ::  Q,Z,EN,VC,A,NU,KS
      REAL (WP)             ::  Y
!
      REAL (WP)             ::  FLOAT
!
      INTEGER               ::  IE
!
      Y = X + X                                                     ! q/k_F
      Q = Y * KF_SI                                                 ! q in SI
!
!  Computing the Coulomb potential
!
      CALL COULOMB_FF(DMN,UNIT,Q,ZERO,VC)                           !
!
      IF(ESTDY == ' STATIC') THEN                                   !
!
!  Static susceptibility function
!
        IF(EPS_T == 'LONG') THEN                                    !
!
          D_FUNCL = D_FUNC                                          !
          CALL DFUNCL_STATIC(X,D_FUNCL,REPS,IEPS)                   ! longitudinal eps
!
        ELSE                                                        !
          D_FUNCT = D_FUNC                                          !
          CONTINUE                                                  ! transverse eps
        END IF                                                      !
!
        LOS = IEPS / (REPS * REPS + IEPS * IEPS)                    !
!
        IF(I_EL == 1) THEN                                          !
          WRITE(IO_EL,*)  Y,LOS                                     !
        END IF                                                      !
!
      ELSE                                                          !
!
!  Loss function
!
        DO IE = 1, N_E                                              ! energy loop
!
          EN = E_MIN + FLOAT(IE - 1) * E_STEP                       ! E = hbar omega / E_F
!
          Z = FOURTH * EN / (X * X)                                 ! Z = omega / omega_q
!
          IF(EPS_T == 'LONG') THEN                                  ! longitudinal eps
!
            D_FUNCL = D_FUNC                                        !
            IF(H < SMALL) THEN                                      !
              CALL DFUNCL_DYNAMIC(X,Z,RS,T,D_FUNCL,IE,REPS,IEPS)    ! no magnetic field
            ELSE                                                    !
              CALL DFUNCL_DYNAMIC_M(X,Z,KS,A,NU,D_FUNCL,REPS,IEPS)  ! magnetic field
            END IF                                                  !
          ELSE                                                      ! transverse eps
            D_FUNCT = D_FUNC                                        !
            IF(H < SMALL) THEN                                      !
              CALL DFUNCT_DYNAMIC(X,Z,D_FUNCT,REPS,IEPS)            ! no magnetic field
            ELSE                                                    !
              CONTINUE                                              ! magnetic field
            END IF                                                  !
          END IF                                                    !
!
          LOS = IEPS / (REPS * REPS + IEPS * IEPS)                  !
!
          IF(I_EL == 1) THEN                                        !  writing to
            WRITE(IO_EL,*)  Y,EN,LOS                                !  file
          END IF                                                    !
!
        END DO                                                      ! end of energy loop
!
      END IF                                                        !
!
      END SUBROUTINE CALC_LOS
!
!=======================================================================
!
      SUBROUTINE CALC_VSC(X)
!
!  This subroutine computes the screened Vc(q, omega)
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!
!
!  Intermediate parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * DMN      : problem dimension
!
!
!  Output parameters:
!
!       * E        : energy array
!       * VSCR     : real part of the screened potential
!       * VSCI     : imaginary part of the screened potential
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 30 Apr 2021
!
!
      USE DIMENSION_CODE,   ONLY : NSIZE
      USE REAL_NUMBERS,     ONLY : ZERO,FOURTH,SMALL,TTINY,INF
      USE FERMI_SI,         ONLY : KF_SI
      USE DF_VALUES,        ONLY : ESTDY,EPS_T,D_FUNC
      USE MATERIAL_PROP,    ONLY : RS,DMN
      USE EXT_FIELDS,       ONLY : T,H
!
      USE E_GRID
      USE UNITS,            ONLY : UNIT
!
      USE DFUNC_STATIC
      USE DFUNCT_STAN_DYNAMIC
      USE DFUNCL_STAN_DYNAMIC
      USE DFUNCL_MAGN_DYNAMIC
      USE COULOMB_K
!
      USE OUT_VALUES_9,     ONLY : I_VC
      USE PRINT_FILES,      ONLY : IO_VC
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  D_FUNCL,D_FUNCT
!
      REAL (WP)             ::  X
      REAL (WP)             ::  VSCR(NSIZE),VSCI(NSIZE),E(NSIZE)
      REAL (WP)             ::  REPS,IEPS,RVSC,IVSC
      REAL (WP)             ::  Q,Z,EN,VC,A,NU,KS
      REAL (WP)             ::  Y
!
      REAL (WP)             ::  FLOAT
!
      INTEGER               ::  IE
!
      Y = X + X                                                     ! q/k_F
      Q = Y * KF_SI                                                 ! q in SI
!
!  Computing the Coulomb potential
!
      CALL COULOMB_FF(DMN,UNIT,Q,ZERO,VC)                           !
!
      IF(ESTDY == ' STATIC') THEN                                   !
!
!  Static dielectric function
!
        IF(EPS_T == 'LONG') THEN                                    !
!
          D_FUNCL = D_FUNC                                          !
          CALL DFUNCL_STATIC(X,D_FUNCL,REPS,IEPS)                   ! longitudinal eps
!
          IF(REPS > TTINY) THEN                                     !
            VSCR(1)= VC / REPS                                      !
          ELSE                                                      !
            VSCR(1) = INF                                           !
          END IF                                                    !
          IF(IEPS > TTINY) THEN                                     !
            VSCI(1) = VC / IEPS                                     !
          ELSE                                                      !
            VSCI(1) = INF                                           !
          END IF                                                    !
!
          IF(I_VC == 1) THEN                                        !
            WRITE(IO_VC,*)  Y,VSCR(1),VSCI(1)                       !
          END IF                                                    !
!
        ELSE                                                        !
          D_FUNCT = D_FUNC                                          !
          CONTINUE                                                  ! transverse eps
        END IF                                                      !
!
      ELSE                                                          !
!
!  Dynamic dielectric function
!
        DO IE = 1, N_E                                              ! energy loop
!
          EN = E_MIN + FLOAT(IE - 1) * E_STEP                       ! E = hbar omega / E_F
!
          Z = FOURTH * EN / (X * X)                                 ! Z = omega / omega_q
!
          IF(EPS_T == 'LONG') THEN                                  ! longitudinal eps
!
            D_FUNCL = D_FUNC                                        !
            IF(H < SMALL) THEN                                      !
              CALL DFUNCL_DYNAMIC(X,Z,RS,T,D_FUNCL,IE,REPS,IEPS)    ! no magnetic field
            ELSE                                                    !
              CALL DFUNCL_DYNAMIC_M(X,Z,KS,A,NU,D_FUNCL,REPS,IEPS)  ! magnetic field
            END IF                                                  !
          ELSE                                                      ! transverse eps
            D_FUNCT = D_FUNC                                        !
            IF(H < SMALL) THEN                                      !
              CALL DFUNCT_DYNAMIC(X,Z,D_FUNCT,REPS,IEPS)            ! no magnetic field
            ELSE                                                    !
              CONTINUE                                              ! magnetic field
            END IF                                                  !
          END IF                                                    !
!
          IF(REPS > TTINY) THEN                                     !
            VSCR(IE) = VC / REPS                                    !
          ELSE                                                      !
            VSCR(IE) = INF                                          !
          END IF                                                    !
          IF(IEPS > TTINY) THEN                                     !
            VSCI(IE) = VC / IEPS                                    !
          ELSE                                                      !
            VSCI(IE) = INF                                          !
          END IF                                                    !
          E(IE) = EN                                                !
!
          IF(I_VC == 1) THEN                                        !  writing to
            WRITE(IO_VC,*)  Y,EN,VSCR(IE),VSCI(IE)                  !  file
          END IF                                                    !
!
        END DO                                                      ! end of energy loop
!
      END IF                                                        !
!
      END SUBROUTINE CALC_VSC
!
END MODULE CALCULATORS_9
