!
!=======================================================================
!
MODULE CALCULATORS_8
!
      USE ACCURACY_REAL
!
!  This module contains the subroutines allowing to compute 
!    various properties of the electron/plasma liquids:
!
!
!          * shear viscosity            : CALC_VIS
!          * diffusion coefficient      : CALC_DIF
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_VIS(X)
!
!  This subroutine computes the shear viscosity
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!
!
!  Output parameters:
!
!       * ETA      : shear viscosity
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Oct 2020
!
!
      USE VISCOSITY
!
      USE OUT_VALUES_8,           ONLY : I_VI
      USE PRINT_FILES,            ONLY : IO_VI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X
      REAL (WP)             ::  ETA
!
!  Computing the viscosity
!
      CALL VISCOSITY_COEF(X,ETA)                                    !
!
      IF(I_VI == 1) THEN                                            !   
        WRITE(IO_VI,*) X,ETA                                        !
      END IF                                                        !
!
      END SUBROUTINE CALC_VIS
!
!=======================================================================
!
      SUBROUTINE CALC_DIF
!
!  This subroutine computes the diffusion coefficient
!
!
!
!  Output parameters:
!
!       * DC       : diffusion coefficient
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Oct 2020
!
!
      USE DIFFUSION_COEFFICIENT
!
      USE OUT_VALUES_8,           ONLY : I_DI
      USE PRINT_FILES,            ONLY : IO_DI
!
      IMPLICIT NONE
!
      REAL (WP)             ::  DC
!
!  Computing the viscosity
!
      CALL DIFFUSION_COEF(DC)                                       !
!
      IF(I_DI == 1) THEN                                            !   
        WRITE(IO_DI,*) DC                                           !
      END IF                                                        !
!
      END SUBROUTINE CALC_DIF
!
END MODULE CALCULATORS_8
