!
!=======================================================================
!
MODULE PRINT_MAT_LENGTHS
!
!  This module prints the scale parameters in the log file
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_CHAR_LENGTHS
!
!  This subroutine computes the material's characteristic lengths
!
      USE REAL_NUMBERS,         ONLY : ZERO
      USE MATERIAL_PROP,        ONLY : RS,DMN
      USE EXT_FIELDS,           ONLY : T,H
      USE MATERIAL_PROPERTIES
!
      IMPLICIT NONE
!
      REAL (WP)             ::  DC,TP
!
!  Temporary !!!!
!
      DC=ZERO                                                       !
      TP=ZERO                                                       !
!
      CALL CHARACTERISTIC_LENGTHS(DMN,RS,T,H,DC,TP)                 !
!
      END SUBROUTINE CALC_CHAR_LENGTHS
!
!=======================================================================
!
      SUBROUTINE PRINT_CHAR_LENGTHS
!
!  This subroutine prints the material's characteristic lengths 
!    into the log file
!
!
!
!   Author :  D. Sébilleau
!
!                                        Last modified : 27 Jul 2020
!
!
      USE MATERIAL_CL
      USE MATERIAL_PROP,    ONLY : RS
      USE CONSTANTS_P1,     ONLY : BOHR
      USE ENE_CHANGE,       ONLY : ANG
!
      IMPLICIT NONE
!
      INTEGER               ::  LOGF
!
      LOGF=6                                                        ! log file unit
!
      CALL CALC_CHAR_LENGTHS                                        !
!
!  Writing the results in Angstroems
!
!
      WRITE(LOGF,15)                                                !
      WRITE(LOGF,5)                                                 !
      WRITE(LOGF,25)                                                !
!
      WRITE(LOGF,10) RS*BOHR/ANG                                    !
      WRITE(LOGF,20) EMFP/ANG                                       !
      WRITE(LOGF,30) FWL/ANG                                        !
      WRITE(LOGF,40) PCL/ANG                                        !
      WRITE(LOGF,50) THL/ANG                                        !
      WRITE(LOGF,60) ML/ANG                                         !
      WRITE(LOGF,70) CR/ANG                                         !
      WRITE(LOGF,80) DL/ANG                                         !
      WRITE(LOGF,90) TFL/ANG                                        !
!
      WRITE(LOGF,75)                                                !
!
!  Formats
!
  10  FORMAT(5X,'|',5X,'average e-e distance      : ',F8.3,'     Å',10X,'|') 
  20  FORMAT(5X,'|',5X,'elastic mean free path    : ',F8.3,'     Å',10X,'|') 
  30  FORMAT(5X,'|',5X,'Fermi wavelength          : ',F8.3,'     Å',10X,'|') 
  40  FORMAT(5X,'|',5X,'phase coherence length    : ',F8.3,'     Å',10X,'|') 
  50  FORMAT(5X,'|',5X,'thermal length            : ',F8.3,'     Å',10X,'|') 
  60  FORMAT(5X,'|',5X,'magnetic length           : ',F8.3,'     Å',10X,'|') 
  70  FORMAT(5X,'|',5X,'cyclotron radius          : ',F8.3,'     Å',10X,'|') 
  80  FORMAT(5X,'|',5X,'Debye length              : ',F8.3,'     Å',10X,'|') 
  90  FORMAT(5X,'|',5X,'Thomas-Fermi length       : ',F8.3,'     Å',10X,'|')  
!
   5  FORMAT(5X,'|',10X,'Characteristic lengths :    ',19X,'|')                            
  15  FORMAT(6X,'_________________________________________________________')
  25  FORMAT(5X,'|                                                         |')                   
  75  FORMAT(5X,'|_________________________________________________________|',/)
! 
      END SUBROUTINE PRINT_CHAR_LENGTHS
!
END MODULE PRINT_MAT_LENGTHS
