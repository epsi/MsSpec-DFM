!
!=======================================================================
!
MODULE PRINT_HEADERS
!
!  This module prints the headers for the log file
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE PRINT_ASCII
!
!  Headers for the FLDF module ascii logo in the log file
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 24 Jul 2020
!
!
      IMPLICIT NONE
!
      INTEGER               ::  I,LOGF
!
      LOGF = 6                                                      ! log file unit
!
      DO I = 1,3                                                    !
        WRITE(LOGF,5)                                               !
      END DO                                                        !
!
      WRITE(LOGF,10)                                                !
      WRITE(LOGF,20)                                                !
      WRITE(LOGF,30)                                                !
      WRITE(LOGF,40)                                                !
      WRITE(LOGF,50)                                                !
      WRITE(LOGF,60)                                                !
      WRITE(LOGF,70)                                                !
      WRITE(LOGF,80)                                                !
      WRITE(LOGF,90)                                                !
!
      WRITE(LOGF,5)                                               !
!
      WRITE(LOGF,100)                                               !
!
      DO I = 1,3                                                    !
        WRITE(LOGF,5)                                               !
      END DO                                                        !
!
!  Formats:
!
   5  FORMAT('      ')
  10  FORMAT(10X,'                         ''~``"  ')
  20  FORMAT(10X,'                        ( o o )" ')
  30  FORMAT(10X,'+------------------.oooO--(_)--Oooo.------------------+')
  40  FORMAT(10X,'|                                                     |')
  50  FORMAT(10X,'|    MsSpec-DFM      .oooO                module      |')
  60  FORMAT(10X,'|                    (   )   Oooo.                    |')
  70  FORMAT(10X,'+---------------------\ (----(   )--------------------+')
  80  FORMAT(10X,'                       \_)    ) /"                     ')
  90  FORMAT(10X,'                             (_/"                      ')
!
 100  FORMAT(10X,'© 2020-2021, Didier Sébilleau, Aditi Mandal and Sylvain Tricot')
!
      END SUBROUTINE PRINT_ASCII
!
END MODULE PRINT_HEADERS
