!
!=======================================================================
!
MODULE THERMODYNAMICS_P 
!
!  This module defines the thermodynamical parameters
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  PP,MU,K0,K,BM,U_IN,U_EX,F_FR
!
END MODULE THERMODYNAMICS_P
!
!=======================================================================
!
!
!=======================================================================
!
MODULE PRINT_THERMODYNAMICS
!
!  This module prints the thermodynamics properties in the log file
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_THERMODYNAMICS_PROP
!
      USE MATERIAL_PROP,            ONLY : RS,DMN
      USE EXT_FIELDS,               ONLY : T
      USE ENERGIES,                 ONLY : EC_TYPE
      USE THERMODYNAMIC_QUANTITIES
!
      USE THERMODYNAMICS_P
!
      IMPLICIT NONE
!
      IF(DMN == '3D') THEN                                          !
        CALL THERMODYNAMICS_3D(EC_TYPE,RS,T,PP,MU,K0,K,BM,    &     !
                               U_IN,U_EX,F_FR)                      !
      ELSE IF(DMN == '2D') THEN                                     !
        CONTINUE                                                    !
      ELSE IF(DMN == '1D') THEN                                     !
        CONTINUE                                                    ! 
      END IF                                                        !
!
      END SUBROUTINE CALC_THERMODYNAMICS_PROP
!
!=======================================================================
!
      SUBROUTINE PRINT_THERMODYNAMICS_PROP
!
!  This subroutine prints the thermodynamics properties in the log file
!
!
!
!   Author :  D. Sébilleau
!
!                                        Last modified : 29 Jul 2020
!
!
      USE ENE_CHANGE,       ONLY : EV
      USE EXT_FIELDS,       ONLY : T
!
      USE THERMODYNAMICS_P
!
      REAL (WP)             ::  TEST
!
      INTEGER               ::  LOGF
!
      LOGF=6                                                        ! log file unit
!
      TEST=90000.0E0_WP                                             !
!
      CALL CALC_THERMODYNAMICS_PROP                                 !
!
      WRITE(LOGF,17)                                                !
      WRITE(LOGF,7) T                                               !
      WRITE(LOGF,27)                                                !
!
      IF(K0 <= TEST) THEN                                           !
        WRITE(LOGF,10) K0                                           !
      ELSE                                                          !
        WRITE(LOGF,15) K0                                           !
      END IF                                                        !
      IF(K <= TEST) THEN                                            !
        WRITE(LOGF,20) K                                            !
      ELSE                                                          !
        WRITE(LOGF,25) K                                            !
      END IF                                                        !
      IF(BM <= TEST) THEN                                           !
        WRITE(LOGF,30) BM                                           !
      ELSE                                                          !
        WRITE(LOGF,35) BM                                           !
      END IF                                                        !
      IF(U_IN/EV <= TEST) THEN                                      !
        WRITE(LOGF,40) U_IN/EV                                      !
      ELSE                                                          !
        WRITE(LOGF,45) U_IN/EV                                      !
      END IF                                                        !
      IF(U_EX/EV <= TEST) THEN                                      !
        WRITE(LOGF,50) U_EX/EV                                      !
      ELSE                                                          !
        WRITE(LOGF,55) U_EX/EV                                      !
      END IF                                                        !
      IF(F_FR/EV <= TEST) THEN                                      !
        WRITE(LOGF,60) F_FR/EV                                      !
      ELSE                                                          !
        WRITE(LOGF,65) F_FR/EV                                      !
      END IF                                                        !
      IF(PP <= TEST) THEN                                           !
        WRITE(LOGF,70) PP                                           !
      ELSE                                                          !
        WRITE(LOGF,75) PP                                           !
      END IF                                                        !
      IF(MU/EV <= TEST) THEN                                        !
        WRITE(LOGF,80) MU/EV                                        !
      ELSE                                                          !
        WRITE(LOGF,85) MU/EV                                        !
      END IF                                                        !
!
      WRITE(LOGF,77)                                                !
!
!  Formats:
!
  10  FORMAT(5X,'|',5X,'compressibility (non-interacting) x n : ',F8.3,' SI |')
  20  FORMAT(5X,'|',5X,'compressibility x n                   : ',F8.3,' SI |')
  30  FORMAT(5X,'|',5X,'bulk modulus                          : ',F8.3,' Pa |')
  40  FORMAT(5X,'|',5X,'internal energy per electron          : ',F8.3,' eV |')
  50  FORMAT(5X,'|',5X,'excess internal energy per el. / k_B T: ',F8.3,' eV |')
  60  FORMAT(5X,'|',5X,'Helmoltz free energy per electron     : ',F8.3,' eV |')
  70  FORMAT(5X,'|',5X,'electron pressure                     : ',F8.3,' SI |')
  80  FORMAT(5X,'|',5X,'chemical potential                    : ',F8.3,' SI |')
!
  15  FORMAT(5X,'|',5X,'compressibility (non-interacting) x n : ',E12.6,' SI |')
  25  FORMAT(5X,'|',5X,'compressibility x n                   : ',E12.6,' SI |')
  35  FORMAT(5X,'|',5X,'bulk modulus                          : ',E12.6,' Pa |')
  45  FORMAT(5X,'|',5X,'internal energy per electron          : ',E12.6,' eV |')
  55  FORMAT(5X,'|',5X,'excess internal energy per el. / k_B T: ',E12.6,' eV |')
  65  FORMAT(5X,'|',5X,'Helmoltz free energy per electron     : ',E12.6,' eV |')
  75  FORMAT(5X,'|',5X,'electron pressure                     : ',E12.6,' SI |')
  85  FORMAT(5X,'|',5X,'chemical potential                    : ',E12.6,' SI |')
!
   7  FORMAT(5X,'|',10X,'Thermodynamics properties at T = ',F8.3,' °K : |') 
  17  FORMAT(6X,'_________________________________________________________')
  27  FORMAT(5X,'|                                                         |')                   
  77  FORMAT(5X,'|_________________________________________________________|',/)
!
      END SUBROUTINE PRINT_THERMODYNAMICS_PROP
!
ENDMODULE PRINT_THERMODYNAMICS  
