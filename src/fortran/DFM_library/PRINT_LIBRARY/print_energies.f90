!
!=======================================================================
!
MODULE ENERGIES_P 
!
!  This module defines the energies parameters
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  E_0,E_X,E_X_HF,E_C,E_XC
      REAL (WP)             ::  E_HF,E_GS,E_KIN,E_POT
!
END MODULE ENERGIES_P
!
!=======================================================================
!
MODULE PRINT_ENERGIES_EL 
!
!  This module prints the energies in the log file
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_ENERGIES_MAT(X,I_SCREEN,K_SC,FF)
!
!  This subroutine computes the different energies (per electron) 
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * I_SCREEN : switch for screened (=1) or unscreened (=0) Coulomb
!       * K_SC     : screening momentum (in SI)
!       * FF       : form factor
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 28 Jul 2020
!
      USE CALC_ENERGIES
      USE MATERIAL_PROP,          ONLY : RS,DMN
      USE EXT_FIELDS,             ONLY : T
      USE ENERGIES,               ONLY : EC_TYPE
!
      USE ENERGIES_P
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X,K_SC,FF
!
      INTEGER               ::  I_SCREEN
!
      IF(DMN == '3D') THEN                                          !
        CALL ENERGIES_3D(X,EC_TYPE,RS,T,I_SCREEN,K_SC,E_0,E_X,    & !
                         E_X_HF,E_C,E_XC,E_HF,E_GS,E_KIN,E_POT)     !
      ELSE IF(DMN == '2D') THEN                                     !
        CALL ENERGIES_2D(X,EC_TYPE,RS,T,E_0,E_X,E_X_HF,E_C,E_XC,  & !
                         E_HF,E_GS,E_KIN,E_POT)                     !
      ELSE IF(DMN == '1D') THEN                                     !
        CALL ENERGIES_1D(EC_TYPE,FF,RS,T,E_0,E_X,E_C,E_XC,E_HF, &   !
                         E_GS,E_KIN,E_POT)                          !
      END IF                                                        !
!
      END SUBROUTINE CALC_ENERGIES_MAT
!
!=======================================================================
!
      SUBROUTINE PRINT_ENERGIES(X,I_SCREEN,K_SC,FF)
!
!  This subroutine prints the different energies (per electron) 
!    in the log file
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * I_SCREEN : switch for screened (=1) or unscreened (=0) Coulomb
!       * K_SC     : screening momentum (in SI)
!       * FF       : form factor
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 28 Jul 2020
!
!
      USE ENE_CHANGE,       ONLY : EV
!
      USE ENERGIES_P
!
      IMPLICIT NONE
!
      REAL (WP)             ::  TEST
      REAL (WP)             ::  X,K_SC,FF
!
      INTEGER               ::  LOGF
      INTEGER               ::  I_SCREEN
!
      LOGF=6                                                        ! log file unit
!
      TEST=90000.0E0_WP                                             !
!
      CALL CALC_ENERGIES_MAT(X,I_SCREEN,K_SC,FF)                    !
!
      WRITE(LOGF,17)                                                !
      WRITE(LOGF,7)                                                 ! 
      WRITE(LOGF,27)                                                !
!
      WRITE(LOGF,10) E_0/EV                                         !
      WRITE(LOGF,20) E_X/EV                                         !
      WRITE(LOGF,30) E_X_HF/EV                                      !
      WRITE(LOGF,40) E_C/EV                                         !
      WRITE(LOGF,50) E_XC/EV                                        !
      WRITE(LOGF,60) E_HF/EV                                        !
      WRITE(LOGF,70) E_GS/EV                                        !
      WRITE(LOGF,80) E_KIN/EV                                       !
      WRITE(LOGF,90) E_POT/EV                                       !
!
      WRITE(LOGF,77)                                                !
!
!  Formats:
!
  10  FORMAT(5X,'|',5X,'energy of non-interacting electron : ',F8.3,' eV',4X,'|')
  20  FORMAT(5X,'|',5X,'exchange energy (1st order)        : ',F8.3,' eV',4X,'|')
  30  FORMAT(5X,'|',5X,'exchange energy (Hartree-Fock)     : ',F8.3,' eV',4X,'|')
  40  FORMAT(5X,'|',5X,'correlation energy                 : ',F8.3,' eV',4X,'|')
  50  FORMAT(5X,'|',5X,'exchange and correlation energy    : ',F8.3,' eV',4X,'|')
  60  FORMAT(5X,'|',5X,'Hartree-Fock energy                : ',F8.3,' eV',4X,'|')
  70  FORMAT(5X,'|',5X,'ground state energy                : ',F8.3,' eV',4X,'|')
  80  FORMAT(5X,'|',5X,'kinetic energy                     : ',F8.3,' eV',4X,'|')
  90  FORMAT(5X,'|',5X,'potential energy                   : ',F8.3,' eV',4X,'|')
!
   7  FORMAT(5X,'|',10X,'Energies at q = 0 : ',27X,'|')                            
  17  FORMAT(6X,'_________________________________________________________')
  27  FORMAT(5X,'|                                                         |')                   
  77  FORMAT(5X,'|_________________________________________________________|',/)
!
      END SUBROUTINE PRINT_ENERGIES
!
END MODULE PRINT_ENERGIES_EL
