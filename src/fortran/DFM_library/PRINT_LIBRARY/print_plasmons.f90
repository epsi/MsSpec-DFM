!
!=======================================================================
!
MODULE PRINT_PLASMONS
!
!  This module prints the plasmons properties in the log file
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE PRINT_PLASMA
! 
!  This subroutine prints the plasmon properties in the log file
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 25 Sep 2020
!
      USE PLASMON_ENE_EV
      USE PLASMON_ENE
      USE PLASMA_SCALE
!
      USE PLASMON_SCALE_P
!
      IMPLICIT NONE
!
      REAL (WP)             ::  TEST
!
      INTEGER               ::  LOGF
!
      LOGF=6                                                        ! log file unit
!
      TEST=90000.0E0_WP                                             !
!
      WRITE(LOGF,17)                                                !
      WRITE(LOGF,7)                                                 !
      WRITE(LOGF,27)                                                !
!
      IF(ENE_P_EV <= TEST) THEN                                     !
        WRITE(LOGF,10) ENE_P_EV                                     !
      ELSE
        WRITE(LOGF,15) ENE_P_EV                                     !
      END IF                                                        !
      IF(NONID <= TEST) THEN                                        !
        WRITE(LOGF,20) NONID                                        !
      ELSE
        WRITE(LOGF,25) NONID                                        !
      END IF                                                        !
      IF(DEGEN <= TEST) THEN                                        !
        WRITE(LOGF,30) DEGEN                                        !
      ELSE
        WRITE(LOGF,35) DEGEN                                        !
      END IF                                                        !
!
      WRITE(LOGF,70)                                                !
!
!  Formats:
!
!
  10  FORMAT(5X,'|',5X,'plasma energy             : ',F10.3,'    eV',5X,'   |')
  20  FORMAT(5X,'|',5X,'plasma nonideality        : ',F10.3,5X,'         |')
  30  FORMAT(5X,'|',5X,'plasma degeneracy         : ',F10.3,5X,'         |')
!
  15  FORMAT(5X,'|',5X,'plasma energy             : ',E12.6,'    eV',3X,'   |')
  25  FORMAT(5X,'|',5X,'plasma nonideality        : ',E12.6,3X,'         |')
  35  FORMAT(5X,'|',5X,'plasma degeneracy         : ',E12.6,3X,'         |')
!
   7  FORMAT(5X,'|',10X,'Plasma parameters : ',27X,'|')                            
  17  FORMAT(6X,'_________________________________________________________')
  27  FORMAT(5X,'|                                                         |')                   
  70  FORMAT(5X,'|_________________________________________________________|',/)
!
      END SUBROUTINE PRINT_PLASMA
!
END MODULE PRINT_PLASMONS
