!
!=======================================================================
!
MODULE SCALE_P
!
!  This module defines the scale parameters
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  G_Q,G_C,R_W
!
END MODULE SCALE_P
!
!=======================================================================
!
MODULE PRINT_SCALE_PARAM
!
!  This module prints the scale parameters in the log file
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_SCALE_PARAM
!
      USE MATERIAL_PROP,    ONLY : RS
      USE EXT_FIELDS,       ONLY : T
      USE PLASMA_SCALE
      USE SCALE_PARAMETERS
!
      USE SCALE_P
!
      IMPLICIT NONE
!
      CALL SCALE_PARAM(RS,T,G_Q,G_C,R_W)                            !
!
      END SUBROUTINE CALC_SCALE_PARAM
!
!=======================================================================
!
      SUBROUTINE PRINT_SCALE_PARAMETERS
!
!  This subroutine prints the scale parameters in the log file
!
!
!
!   Author :  D. Sébilleau
!
!                                        Last modified : 28 Jul 2020
!
      USE SCALE_P
!
      IMPLICIT NONE
!
      REAL (WP)             ::  TEST
!
      INTEGER               ::  LOGF
!
      LOGF=6                                                        ! log file unit
!
      TEST=90000.0E0_WP                                             !
!
      CALL CALC_SCALE_PARAM                                         !
!
      WRITE(LOGF,17)                                                !
      WRITE(LOGF,7)                                                 !
      WRITE(LOGF,27)                                                !
!
      IF(G_Q <= TEST) THEN                                          !
        WRITE(LOGF,10) G_Q                                          !
      ELSE
        WRITE(LOGF,15) G_Q                                          !
      END IF                                                        !
      IF(G_C <= TEST) THEN                                          !
        WRITE(LOGF,20) G_C                                          !
      ELSE
        WRITE(LOGF,25) G_C                                          !
      END IF                                                        !
      IF(R_W <= TEST) THEN                                          !
        WRITE(LOGF,30) R_W                                          !
      ELSE
        WRITE(LOGF,35) R_W                                          !
      END IF                                                        !
!
      WRITE(LOGF,70)                                                !
!
!  Formats:
!
  10  FORMAT(5X,'|',5X,'quantum scale parameter   : ',F10.3,10X,'    |')
  20  FORMAT(5X,'|',5X,'classical scale parameter : ',F10.3,10X,'    |')
  30  FORMAT(5X,'|',5X,'Wilson ratio              : ',F10.3,10X,'    |') 
!
  15  FORMAT(5X,'|',5X,'quantum scale parameter   : ',E12.6,8X,'    |')
  25  FORMAT(5X,'|',5X,'classical scale parameter : ',E12.6,8X,'    |')
  35  FORMAT(5X,'|',5X,'Wilson ratio              : ',E12.6,8X,'    |') 
!
   7  FORMAT(5X,'|',10X,'Scale parameters :    ',25X,'|')                            
  17  FORMAT(6X,'_________________________________________________________')
  27  FORMAT(5X,'|                                                         |')                   
  70  FORMAT(5X,'|_________________________________________________________|',/)
!
      END SUBROUTINE PRINT_SCALE_PARAMETERS
!
END MODULE PRINT_SCALE_PARAM
