!
!=======================================================================
!
MODULE PRINT_FERMI
!
!  This module prints the Fermi values in the log file
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE PRINT_FERMI_SI
!
!  This subroutine prints the Fermi values in the log file
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 15 Oct 2020
!
!
      USE FERMI_SI
      USE OUT_VALUES_5
      USE ENE_CHANGE,     ONLY : ANG,EV
!
      IMPLICIT NONE
!
      INTEGER               ::  LOGF,I_P
!
      LOGF = 6                                                      ! log file unit
      I_P  = I_EF + I_KF + I_VF + I_TE + I_DL                       !
!
      IF(I_P > 0) THEN                                              !
        WRITE(LOGF,15)                                              !
        WRITE(LOGF,5)                                               !
        WRITE(LOGF,25)                                              !
      END IF                                                        !
!
      IF(I_EF == 1) THEN                                            !
        WRITE(LOGF,10) EF_SI / EV                                   !
      END IF                                                        !
!
      IF(I_KF == 1) THEN                                            !
        WRITE(LOGF,20) KF_SI * ANG                                  !
      END IF                                                        !
!
      IF(I_VF == 1) THEN                                            !
        WRITE(LOGF,30) VF_SI                                        !
      END IF                                                        !
!
      IF(I_TE == 1) THEN                                            !
        WRITE(LOGF,40) TF_SI                                        !
      END IF                                                        !
!
      IF(I_DL == 1) THEN                                            !
        WRITE(LOGF,50) NF_SI * EV                                   !
      END IF                                                        !
!
      IF(I_P > 0) THEN                                              !
        WRITE(LOGF,35)                                              !
      END IF                                                        !
!
!  Formats
!
  10  FORMAT(5X,'|',5X,'Fermi energy              : ',F8.3,'     eV',5X,'    |')      
  20  FORMAT(5X,'|',5X,'Fermi wave vector         : ',F8.3,'     Å^{-1}',5X,'|')  
  30  FORMAT(5X,'|',5X,'Fermi velocity            : ',E12.6,' m/s',5X,'   |')        
  40  FORMAT(5X,'|',5X,'Fermi temperature         : ',E12.6,' °K',5X,'    |')         
  50  FORMAT(5X,'|',5X,'Fermi DoS                 : ',E12.6,' / eV',5X,'  |')       
!
   5  FORMAT(5X,'|',10X,'Fermi values :    ',29X,'|')                            
  25  FORMAT(5X,'|                                                         |')                   
  15  FORMAT(6X,'_________________________________________________________')
  35  FORMAT(5X,'|_________________________________________________________|',/)
!
      END SUBROUTINE PRINT_FERMI_SI
!
!=======================================================================
!
      SUBROUTINE PRINT_FERMI_SI_M
!
!  This subroutine prints the Fermi values in the log file
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 24 Jul 2020
!
!
      USE FERMI_SI_M
      USE OUT_VALUES_5
      USE CONSTANTS_P1,   ONLY : BOHR
      USE ENE_CHANGE,     ONLY : EV
!
      IMPLICIT NONE
!
      INTEGER               ::  LOGF
!
      LOGF=6                                                        ! log file unit
!
      WRITE(LOGF,5)                                                 !
!
      IF(I_EF == 1) THEN                                            !
        WRITE(LOGF,10) EF_SI_M/EV                                   !
      END IF                                                        !
!
      IF(I_KF == 1) THEN                                            !
        WRITE(LOGF,20) KF_SI_M*BOHR                                 !
      END IF                                                        !
!
      IF(I_VF == 1) THEN                                            !
        WRITE(LOGF,30) VF_SI_M                                      !
      END IF                                                        !
!
      IF(I_TE == 1) THEN                                            !
        WRITE(LOGF,40) TF_SI_M                                      !
      END IF                                                        !
!
!  Formats
!
   5  FORMAT(10X,'Fermi values :    ',/)                            !
  10  FORMAT(5X,'Fermi energy           : ',F8.3,' eV')             !
  20  FORMAT(5X,'Fermi wave vector      : ',F8.3,' Å^{-1}')         !
  30  FORMAT(5X,'Fermi velocity         : ',E12.6,' m/s')           !
  40  FORMAT(5X,'Fermi temperature      : ',F8.3,' °K')             !
  55  FORMAT('                                 ')                   !
!
      END SUBROUTINE PRINT_FERMI_SI_M
!
END MODULE PRINT_FERMI
