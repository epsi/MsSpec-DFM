!
!=======================================================================
!
MODULE PRINT_MATERIAL_PROP 
!
!  This module prints the material's properties in the log file
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE PRINT_MATERIAL_PROPERTIES(DMN,RS,T,B,DC,TP)
!
!  This subroutine prints the material's properties in the log file
!
!
!  Input parameters:
!
!       * DMN      : dimension  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature in SI
!       * B        : magnetic field in SI
!       * DC       : diffusion coefficient
!       * TP       : phase-breaking relaxation time in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 24 Jun 2020
!
!
      USE MATERIAL_PROPERTIES
!
      IMPLICIT NONE
!
      CALL CHARACTERISTIC_LENGTHS(DMN,RS,T,B,DC,TP)
!
      END SUBROUTINE PRINT_MATERIAL_PROPERTIES
!
END MODULE PRINT_MATERIAL_PROP
