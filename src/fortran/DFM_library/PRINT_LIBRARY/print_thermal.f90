!
!=======================================================================
!
MODULE THERMAL_P 
!
!  This module defines the thermal parameters
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  K_TH,V_TH,L_TH,CV,P,MU_TH
!
END MODULE THERMAL_P
!
!=======================================================================
!
MODULE PRINT_THERMAL
!
!  This module prints the thermal properties in the log file
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_THERMAL_PROP
!
      USE MATERIAL_PROP,          ONLY : RS,DMN
      USE EXT_FIELDS,             ONLY : T
      USE THERMAL_PROPERTIES
      USE CHEMICAL_POTENTIAL,     ONLY : MU
!
      USE THERMAL_P
!
      IMPLICIT NONE
!
      CALL TH_PROP(DMN,RS,T,K_TH,V_TH,L_TH,CV,P)                    !
      MU_TH=MU(DMN,T)                                               !
!
      END SUBROUTINE CALC_THERMAL_PROP
!
!=======================================================================
!
      SUBROUTINE PRINT_THERMAL_PROP
!
!  This subroutine prints the thermal properties in the log file
!
!
!
!   Author :  D. Sébilleau
!
!                                        Last modified : 28 Jul 2020
!
!
      USE CONSTANTS_P1,     ONLY : BOHR
      USE ENE_CHANGE,       ONLY : ANG,EV
      USE EXT_FIELDS,       ONLY : T
!
      USE THERMAL_P
!
      IMPLICIT NONE
!
      REAL (WP)             ::  TEST
!
      INTEGER               ::  LOGF
!
      LOGF=6                                                        ! log file unit
!
      TEST=90000.0E0_WP                                             !
!
      CALL CALC_THERMAL_PROP                                        !
!
      WRITE(LOGF,17)                                                !
      WRITE(LOGF,7) T                                               !
      WRITE(LOGF,27)                                                !
!
      IF(K_TH*BOHR <= TEST) THEN                                    !
        WRITE(LOGF,10) K_TH*ANG                                     !
      ELSE                                                          !
        WRITE(LOGF,15) K_TH*ANG                                     !
      END IF                                                        !
      IF(V_TH <= TEST) THEN                                         !
        WRITE(LOGF,20) V_TH                                         !
      ELSE                                                          !
        WRITE(LOGF,25) V_TH                                         !
      END IF                                                        !
      IF(L_TH <= TEST) THEN                                         !
        WRITE(LOGF,30) L_TH/ANG                                     !
      ELSE                                                          !
        WRITE(LOGF,35) L_TH/ANG                                     !
      END IF                                                        !
      IF(CV <= TEST) THEN                                           !
        WRITE(LOGF,40) CV                                           !
      ELSE                                                          !
        WRITE(LOGF,45) CV                                           !
      END IF                                                        !
      IF(P <= TEST) THEN                                            !
        WRITE(LOGF,50) P                                            !
      ELSE                                                          !
        WRITE(LOGF,55) P                                            !
      END IF                                                        !
      IF(MU_TH/EV <= TEST) THEN                                     !
        WRITE(LOGF,60) MU_TH/EV                                     !
      ELSE                                                          !
        WRITE(LOGF,65) MU_TH/EV                                     !
      END IF                                                        !
!
      WRITE(LOGF,70)                                                !
!
!  Formats:
!
  10  FORMAT(5X,'|',5X,'De Broglie wave vector    : ',F10.3,'     Å^{-1}','   |')
  20  FORMAT(5X,'|',5X,'thermal velocity          : ',F10.3,'     m/s',3X,'   |')
  30  FORMAT(5X,'|',5X,'Landau length             : ',F10.3,'     Å        |')
  40  FORMAT(5X,'|',5X,'electron specific heat    : ',F10.3,'     SI       |')
  50  FORMAT(5X,'|',5X,'electron pressure         : ',F10.3,'     SI       |')
  60  FORMAT(5X,'|',5X,'chemical potential        : ',F10.3,'     eV       |')
!
  15  FORMAT(5X,'|',5X,'De Broglie wave vector    : ',E12.6,'   Å^{-1}   |')
  25  FORMAT(5X,'|',5X,'thermal velocity          : ',E12.6,'   m/s      |')
  35  FORMAT(5X,'|',5X,'Landau length             : ',E12.6,'   Å        |')
  45  FORMAT(5X,'|',5X,'electron specific heat    : ',E12.6,'   SI       |')
  55  FORMAT(5X,'|',5X,'electron pressure         : ',E12.6,'   SI       |')
  65  FORMAT(5X,'|',5X,'chemical potential        : ',E12.6,'   eV       |')
!
   7  FORMAT(5X,'|',10X,'Thermal properties at T = ',F10.3,' °K : ',5X,'|')                            
  17  FORMAT(6X,'_________________________________________________________')
  27  FORMAT(5X,'|                                                         |')                   
  70  FORMAT(5X,'|_________________________________________________________|',/)
!
      END SUBROUTINE PRINT_THERMAL_PROP
!
END MODULE PRINT_THERMAL
