!
!=======================================================================
!
MODULE PRINT_FILES
!
!  This module contains the Fortran unit numbers
!    of the output files 
!
      IMPLICIT NONE
!
      INTEGER               ::  IO_DF,IO_PZ,IO_SU,IO_CD
!
      INTEGER               ::  IO_PD,IO_EH,IO_E2,IO_CK
      INTEGER               ::  IO_CR,IO_PK
!
      INTEGER               ::  IO_LF,IO_IQ,IO_SF,IO_PC
      INTEGER               ::  IO_P2
      INTEGER               ::  IO_VX,IO_DC,IO_MD,IO_LD
      INTEGER               ::  IO_DP,IO_LT,IO_BR,IO_PE
      INTEGER               ::  IO_QC,IO_RL,IO_KS,IO_OQ
      INTEGER               ::  IO_ME,IO_MS,IO_ML,IO_MC
      INTEGER               ::  IO_DE,IO_ZE,IO_SR,IO_CW
      INTEGER               ::  IO_CF,IO_EM,IO_MF,IO_SP
      INTEGER               ::  IO_SE,IO_SB,IO_ES,IO_GR
      INTEGER               ::  IO_FD,IO_BE,IO_MX
      INTEGER               ::  IO_SC,IO_DS,IO_NV,IO_MT
!
      INTEGER               ::  IO_GP,IO_PR,IO_CO,IO_CP
      INTEGER               ::  IO_BM,IO_SH,IO_S0,IO_S1
      INTEGER               ::  IO_DT,IO_PS,IO_IE,IO_EI
      INTEGER               ::  IO_FH,IO_EY
!
      INTEGER               ::  IO_EF,IO_KF,IO_VF,IO_TE,IO_DL
!
      INTEGER               ::  IO_TW,IO_VT,IO_TC
!
      INTEGER               ::  IO_EG,IO_EX,IO_XC,IO_EC
      INTEGER               ::  IO_HF,IO_EK,IO_EP
!
      INTEGER               ::  IO_VI,IO_DI
!
      INTEGER               ::  IO_FP,IO_EL,IO_PO,IO_RF
      INTEGER               ::  IO_VC
!
END MODULE PRINT_FILES 
