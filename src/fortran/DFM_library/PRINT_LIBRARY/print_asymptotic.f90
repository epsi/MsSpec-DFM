!
!=======================================================================
!
MODULE PRINT_ASYMPTOTIC
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE PRINT_ASYMPT_VALUES
!
!  This subroutine prints the asymptotic values gamma_0, gamma_inf 
!    and g(0) in the log file
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  3 Dec 2020
!
!
      USE ASYMPT
!
      IMPLICIT NONE
!
      INTEGER               ::  LOGF 
!
      LOGF = 6                                                      ! log file unit
!
      WRITE(LOGF,10)                                                !
      WRITE(LOGF,30)                                                !
      WRITE(LOGF,20)                                                !
      WRITE(LOGF,40) G0                                             !
      WRITE(LOGF,50) GI                                             !
      WRITE(LOGF,60) GR0                                            !
      WRITE(LOGF,70)                                                !
!
!  Formats
!
  10  FORMAT(6X,'_________________________________________________________')
  20  FORMAT(5X,'|                                                         |')                   
  30  FORMAT(5X,'|          Asymptotic values :                            |') 
  40  FORMAT(5X,'|',5X,'gamma_0                   : ',F8.3,15X,' |') 
  50  FORMAT(5X,'|',5X,'gamma_i                   : ',F8.3,15X,' |') 
  60  FORMAT(5X,'|',5X,'g(0)                      : ',F8.3,15X,' |') 
  70  FORMAT(5X,'|_________________________________________________________|',/)
!
      END SUBROUTINE PRINT_ASYMPT_VALUES
!
END MODULE PRINT_ASYMPTOTIC
