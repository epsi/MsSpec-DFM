!
!=======================================================================
!
MODULE PRINT_CALC_TYPE
!
!  This module prints the type of calculations requested
!
!
CONTAINS
!
!=======================================================================
!
      FUNCTION INDEX_CALC(I)
!
!  This function associates to each unit number I the name of the
!    switch corresponding to the calculation whose result is printed
!    into unit I
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 21 Apr 2021
!
!
      USE DIMENSION_CODE,    ONLY : NOFFN
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)     ::  IND(NOFFN)
      CHARACTER (LEN = 4)     ::  INDEX_CALC
!
      INTEGER, INTENT(IN)     ::  I
!
!
      DATA IND( 1) / '    ' /       ! \
      DATA IND( 2) / '    ' /       !  \
      DATA IND( 3) / '    ' /       !   \>>  Fortran units not used
      DATA IND( 4) / '    ' /       !   />>  for output files
      DATA IND( 5) / '    ' /       !  /
      DATA IND( 6) / '    ' /       ! /
      DATA IND( 7) / 'I_DF' /       ! dielectric function file
      DATA IND( 8) / 'I_PZ' /       ! polarization function
      DATA IND( 9) / 'I_SU' /       ! susceptibility function
      DATA IND(10) / 'I_CD' /       ! electrical conductivity
!
      DATA IND(11) / 'I_PD' /       ! plasmon dispersion file
      DATA IND(12) / 'I_EH' /       ! electron-hole dispersion file
      DATA IND(13) / 'I_E2' /       ! two electron-hole dispersion
      DATA IND(14) / 'I_CK' /       ! screened Coulomb (k-space)
      DATA IND(15) / 'I_CR' /       ! screened Coulomb (real space)
      DATA IND(16) / 'I_PK' /       ! plasmon kinetic energy
!
      DATA IND(17) / 'I_LF' /       ! local-field correction file G(q,om)
      DATA IND(18) / 'I_IQ' /       ! G(q,inf) file
      DATA IND(19) / 'I_SF' /       ! structure factor file S(q,om)
      DATA IND(20) / 'I_PC' /       ! pair correlation function file
      DATA IND(21) / 'I_P2' /       ! pair distribution function file
      DATA IND(22) / 'I_VX' /       ! vertex function Gamma(q,om)
      DATA IND(23) / 'I_DC' /       ! plasmon damping coefficient Im[eps]/q^2
      DATA IND(24) / 'I_MD' /       ! momentum distribution
      DATA IND(25) / 'I_LD' /       ! Landau parameters
      DATA IND(26) / 'I_DP' /       ! damping file
      DATA IND(27) / 'I_LT' /       ! plasmon lifetime file
      DATA IND(28) / 'I_BR' /       ! plasmon broadening
      DATA IND(29) / 'I_PE' /       ! plasmon energy
      DATA IND(30) / 'I_QC' /       ! plasmon q-bounds
      DATA IND(31) / 'I_RL' /       ! relaxation time
      DATA IND(32) / 'I_KS' /       ! screening wave vector
      DATA IND(33) / 'I_DY' /       ! Debye wave vector
      DATA IND(34) / 'I_ME' /       ! moments of epsilon
      DATA IND(35) / 'I_MS' /       ! moments of S(q,omega)
      DATA IND(36) / 'I_ML' /       ! moments of loss function
      DATA IND(37) / 'I_MC' /       ! moments of conductivity
      DATA IND(38) / 'I_DE' /       ! derivative of Re[ dielectric function ]
      DATA IND(39) / 'I_ZE' /       ! Re[ dielectric function ] = 0
      DATA IND(40) / 'I_SR' /       ! sum rules for epsilon
      DATA IND(41) / 'I_CW' /       ! confinement wave function
      DATA IND(42) / 'I_CF' /       ! confinement potential
      DATA IND(43) / 'I_EM' /       ! effective mass
      DATA IND(44) / 'I_MF' /       ! mean free path
      DATA IND(45) / 'I_SP' /       ! spectral function
      DATA IND(46) / 'I_SE' /       ! self-energy
      DATA IND(47) / 'I_SB' /       ! subband energies
      DATA IND(48) / 'I_ES' /       ! Eliashberg function
      DATA IND(49) / 'I_GR' /       ! Grüneisen parameter
      DATA IND(50) / 'I_FD' /       ! Fermi-Dirac distribution
      DATA IND(51) / 'I_BE' /       ! Bose-Einstein distribution
      DATA IND(52) / 'I_MX' /       ! Maxwell distribution
      DATA IND(53) / 'I_SC' /       ! scale parameters
      DATA IND(54) / 'I_DS' /       ! density of states
      DATA IND(55) / 'I_NV' /       ! Nevanlinaa function
      DATA IND(56) / 'I_MT' /       ! memory function
!
      DATA IND(57) / 'I_GP' /       ! grand partition function
      DATA IND(58) / 'I_PR' /       ! electronic pressure
      DATA IND(59) / 'I_CO' /       ! compressibility
      DATA IND(60) / 'I_CP' /       ! chemical potential
      DATA IND(61) / 'I_BM' /       ! bulk modulus
      DATA IND(62) / 'I_SH' /       ! shear modulus
      DATA IND(63) / 'I_S0' /       ! zero sound velocity
      DATA IND(64) / 'I_S1' /       ! first sound velocity
      DATA IND(65) / 'I_DT' /       ! Debye temperature
      DATA IND(66) / 'I_PS' /       ! Pauli paramagnetic susceptibility
      DATA IND(67) / 'I_IE' /       ! internal energy
      DATA IND(68) / 'I_EI' /       ! excess internal energy
      DATA IND(69) / 'I_FH' /       ! Helmholtz free energy
      DATA IND(70) / 'I_EY' /       ! entropy
!
      DATA IND(71) / 'I_EF' /       ! Fermi energy
      DATA IND(72) / 'I_KF' /       ! Fermi momentum
      DATA IND(73) / 'I_VF' /       ! Fermi velocity
      DATA IND(74) / 'I_TE' /       ! Fermi temperature
      DATA IND(75) / 'I_DL' /       ! Fermi density of states
!
      DATA IND(76) / 'I_TW' /       ! thermal De Broglie wavelength
      DATA IND(77) / 'I_VT' /       ! thermal velocity
      DATA IND(78) / 'I_TC' /       ! thermal conductivity
!
      DATA IND(79) / 'I_EG' /       ! ground state energy
      DATA IND(80) / 'I_EX' /       ! exchange energy
      DATA IND(81) / 'I_XC' /       ! exchange correlation energy
      DATA IND(82) / 'I_EC' /       ! correlation energy
      DATA IND(83) / 'I_HF' /       ! Hartree-Fock energy
      DATA IND(84) / 'I_EK' /       ! kinetic energy
      DATA IND(85) / 'I_EP' /       ! potential energy
!
      DATA IND(86) / 'I_VI' /       ! shear viscosity
      DATA IND(87) / 'I_DI' /       ! diffusion coefficient
!
      DATA IND(88) / 'I_FP' /       ! fluctuation potential file
      DATA IND(89) / 'I_EL' /       ! energy loss function
      DATA IND(90) / 'I_PO' /       ! stopping power
      DATA IND(91) / 'I_RF' /       ! refractive index
      DATA IND(92) / 'I_VC' /       ! dynamic screened Coulomb potential V(q,omega)
!
!
      INDEX_CALC = IND(I)                                       !
!
      END FUNCTION INDEX_CALC
!
!=======================================================================
!
      SUBROUTINE PRINT_CALC_INFO
!
!  Prints the calculation types in the log file
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 21 Apr 2021
!
!
      USE DIMENSION_CODE,    ONLY : NOFFN
!
      USE OUT_VALUES_1
      USE OUT_VALUES_2
      USE OUT_VALUES_3
      USE OUT_VALUES_4
      USE OUT_VALUES_5
      USE OUT_VALUES_6
      USE OUT_VALUES_7
      USE OUT_VALUES_8
      USE OUT_VALUES_9
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 40)  ::  CALCTYPE(7:NOFFN),STRING
!
      INTEGER               ::  I,LOGF
      INTEGER               ::  STRING_VALUE
!
      DATA CALCTYPE /                                             & !
                     'dielectric function                     ',  & !
                     'polarization function                   ',  & !
                     'susceptibility function                 ',  & !
                     'electrical conductivity                 ',  & !
                     'plasmon dispersion                      ',  & !
                     'electron-hole dispersion                ',  & !
                     'two electron-hole dispersion            ',  & !
                     'interaction potential in k-space        ',  & !
                     'interaction potential in real space     ',  & !
                     'plasmon kinetic energy                  ',  & !
                     'local-field correction                  ',  & !
                     'G(q,inf)                                ',  & !
                     'structure factor                        ',  & !
                     'pair correlation function               ',  & !
                     'pair distribution function              ',  & !
                     'vertex function                         ',  & !
                     'plasmon damping coefficient             ',  & !
                     'momentum distribution                   ',  & !
                     'Landau parameters                       ',  & !
                     'damping                                 ',  & !
                     'plasmon lifetime                        ',  & !
                     'plasmon broadening                      ',  & !
                     'plasmon energy                          ',  & !
                     'plasmon q-bounds                        ',  & !
                     'relaxation time                         ',  & !
                     'screening wave vector                   ',  & !
                     'omega = q * v_F                         ',  & !
                     'moments of epsilon                      ',  & !
                     'moments of S(q,omega)                   ',  & !
                     'moments of loss function                ',  & !
                     'moments of conductivity                 ',  & !
                     'derivative of Re[ dielectric function ] ',  & !
                     'Re[ dielectric function ] = 0           ',  & !
                     'sum rules for epsilon                   ',  & !
                     'confinement wave function               ',  & !
                     'confinement potential                   ',  & !
                     'effective mass                          ',  & !
                     'mean free path                          ',  & !
                     'spectral function                       ',  & !
                     'self-energy                             ',  & !
                     'subband energies                        ',  & !
                     'Eliashberg function                     ',  & !
                     'Gruneisen parameter                     ',  & !
                     'Fermi-Dirac distribution                ',  & !
                     'Bose-Einstein distribution              ',  & !
                     'Maxwell-Boltzmann distribution          ',  & !
                     'scale parameters                        ',  & !
                     'density of states                       ',  & !
                     'Nevanlinaa/memory function              ',  & !
                     'time domain memory function             ',  & !
                     'grand partition function                ',  & !
                     'electronic pressure                     ',  & !
                     'compressibility                         ',  & !
                     'chemical potential                      ',  & !
                     'bulk modulus                            ',  & !
                     'shear modulus                           ',  & !
                     'zero sound velocity                     ',  & !
                     'first sound velocity                    ',  & !
                     'Debye temperature                       ',  & !
                     'Pauli paramagnetic susceptibility       ',  & !
                     'internal energy                         ',  & !
                     'excess internal energy                  ',  & !
                     'Helmholtz free energy                   ',  & !
                     'entropy                                 ',  & !
                     'Fermi energy                            ',  & !
                     'Fermi momentum                          ',  & !
                     'Fermi velocity                          ',  & !
                     'Fermi temperature                       ',  & !
                     'Fermi density of states                 ',  & !
                     'thermal De Broglie wavelength           ',  & !
                     'thermal velocity                        ',  & !
                     'thermal conductivity                    ',  & !
                     'ground state energy                     ',  & !
                     'exchange energy                         ',  & !
                     'exchange correlation energy             ',  & !
                     'correlation energy                      ',  & !
                     'Hartree-Fock energy                     ',  & !
                     'kinetic energy                          ',  & !
                     'potential energy                        ',  & !
                     'shear viscosity                         ',  & !
                     'diffusion coefficient                   ',  & !
                     'fluctuation potential file              ',  & !
                     'energy loss function                    ',  & !
                     'stopping power                          ',  & !
                     'refractive index                        ',  & !
                     'dynamic screened Coulomb potential      '   & !
                    /
!
      LOGF = 6                                                      ! log file unit
!
      DO I = 1, 3                                                   !
        WRITE(LOGF,5)                                               !
      END DO                                                        !
!
      WRITE(LOGF,10)                                                !
      WRITE(LOGF,20)                                                !
      WRITE(LOGF,30)                                                !
      DO I = 7, NOFFN                                               !
          STRING = INDEX_CALC(I)                                    ! string to variable
          READ(STRING,'(I1)') STRING_VALUE                          ! value of variable
        IF(STRING_VALUE == 1) THEN                                  !
          WRITE(LOGF,40) CALCTYPE(I)                                !
        END IF                                                      !
      END DO                                                        !
      WRITE(LOGF,20)                                                !
      WRITE(LOGF,50)                                                !
!
!  Formats:
!
   5  FORMAT('      ')
  10  FORMAT(6X,'_________________________________________________________')
  20  FORMAT(5X,'|                                                         |')
  30  FORMAT(5X,'|  CALCULATIONS PERFORMED:                                |')
  40  FORMAT(5X,'|          * ',A40,'      |')
  50  FORMAT(5X,'|_________________________________________________________|',/)
!
      END SUBROUTINE PRINT_CALC_INFO
!
END MODULE PRINT_CALC_TYPE

