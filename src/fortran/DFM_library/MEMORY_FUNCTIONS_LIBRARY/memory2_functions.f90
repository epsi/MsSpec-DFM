!
!=======================================================================
!
MODULE MEMORY2_FUNCTIONS_F 
!
!  This modules provides memory functions in terms of the frequency
!    and of the transfer momentum
!
! 
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
!
!=======================================================================
!
      FUNCTION MEMORY2_F(X,Z,T,TAU,MEM_TYPE)
!
!  This function computes the memory function K(q,omega)
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : dimensionless factor   --> Z = omega / omega_q 
!       * T        : temperature in SI
!       * TAU      : relaxation time in SI
!       * MEM_TYPE : type of memory function used
!                      MEM_TYPE  = 'RAYI' --> Raganathan-Yip function
!                      MEM_TYPE  = 'LIHY' --> linearized hydrodynamic function
!
!
!  Remark: The memory function has the dimension of a frequency omega
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 29 Jan 2021
!
!
      USE CALCTYPE
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO
      USE CONSTANTS_P1,        ONLY : M_E,K_B
      USE FERMI_SI,            ONLY : KF_SI,VF_SI
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  MEM_TYPE
!
      REAL (WP), INTENT(IN) ::  X,Z,T,TAU
!
      REAL (WP)             ::  U,V0,U0,U1
      REAL (WP)             ::  Q_SI
      REAL (WP)             ::  OMG
!
      REAL (WP)             ::  SQRT
!
      COMPLEX (WP)          ::  MEMORY_F
!
!  Computing the average velocity
!
      IF(CAL_TYPE == 'QUANTUM') THEN                                !
        V0 = VF_SI                                                  !
      ELSE                                                          !
        V0 = SQRT(TWO * K_B * T / M_E)                              !
      END IF                                                        !
!
      Q_SI = Y * KF_SI                                              ! q in SI
      U    = X * Z                                                  ! U = omega / (q v_F)
      OMG  = Q_SI * VF_SI * U                                       ! omega in SI
!
      U0 = U * VF_SI / V_0                                          ! omega / q v_0
      U1 = ONE / (TAU * Q_SI * V0)                                  !
!
      IF(MEM_TYPE == 'NONE') THEN                                   !
        MEMORY2_F = RAYI(OMG,U0)                                    ! 
      ELSE IF(MEM_TYPE == 'DELT') THEN                              !
        MEMORY2_F = LINY(OMG,TAU,U1)                                ! 
      END IF                                                        !
!
      END FUNCTION MEMORY2_F
!
!=======================================================================
!
      FUNCTION RAYI(OMG,U0)
!
!  This function computes the frequency Ranganathan-Yip memory function 
!  
!
!  Input parameters:
!
!       * OMG      : frequency in SI
!       * U0       : dimensionless parameter omega / q v_0
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 29 Jan 2021
!
!
      USE REAL_NUMBERS,       ONLY : ONE,TWO,FOUR
      USE COMPLEX_NUMBERS,    ONLY : IC
      USE PI_ETC,             ONLY : PI_INV,SQR_PI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  OMG,U0
!
      REAL (WP)             ::  REM,IMM
!
      COMPLEX (WP)          ::  RAYI
!
      REM = ONE / SQR_PI - (FOUR * PI_INC - ONE) * U0 * U0          !
      IMM = (ONE - TWO * PI_INV) * U0                               ! 
!
      RAYI = OMG * (REM + IC * IMM) / U0                            ! 
!
      END FUNCTION RAYI
!
!=======================================================================
!
      FUNCTION LIHY(OMG,TAU,U1)
!
!  This function computes the frequency linearized hydrodynamic memory function 
!  
!
!  Input parameters:
!
!       * OMG      : frequency in SI
!       * TAU      : relaxation time in SI
!       * U1       : dimensionless parameter 1 / (tau q v_0)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 29 Jan 2021
!
!
      USE REAL_NUMBERS,       ONLY : THREE,FIVE,HALF,THIRD,SIXTH,NINTH
      USE COMPLEX_NUMBERS,    ONLY : IC
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  OMG,TAU,U1
!
      COMPLEX (WP)          ::  LIHY
!
      COMPLEX (WP)          ::  NUM,DEN
!
      NUM = IC * OMG + FIVE * SIXTH / U1                            !
      DEN = - OMG * OMG + IC * THREE * HALF / U1 +                & !
              FIVE * NINTH / (U1 * U1) + THIRD                      !
!
      LIHY = HALF * NUM / (DEN * TAU * U1)                          ! 
!
      END FUNCTION LIHY
!
END MODULE MEMORY2_FUNCTIONS_F
