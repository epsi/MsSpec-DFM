!
!=======================================================================
!
MODULE STRUCTURE_FACTOR_DYNAMIC_2
!
      USE ACCURACY_REAL
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE STFACT_DYNAMIC_FROM_EPS(X,Z,RS,T,SQ)
!
!  This subroutine computes a dynamical structure factor S(q,omega)
!    from the knowledge of the dielectric function
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!       * Z        : dimensionless factor   --> Z = omega / omega()_q
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!
!  Output parameters:
!
!       * SQ       : structure factor
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 30 Apr 2021
!
      USE MATERIAL_PROP,    ONLY : DMN
      USE REAL_NUMBERS,     ONLY : ZERO,TWO,INF
      USE FERMI_SI,         ONLY : KF_SI
!
      USE UTILITIES_3,      ONLY : EPS_TO_SQO
      USE DF_VALUES,        ONLY : D_FUNC
      USE SCREENING_TYPE
      USE SCREENING_VEC
      USE COULOMB_K
      USE DFUNCL_STAN_DYNAMIC
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X,Z,RS,T
      REAL (WP), INTENT(OUT)::  SQ
!
      REAL (WP)             ::  KS_SI,Q_SI
      REAL (WP)             ::  VC
      REAL (WP)             ::  EPSR,EPSI
!
      Q_SI = TWO * X * KF_SI                                        ! q in SI
!
      CALL DFUNCL_DYNAMIC(X,Z,RS,T,D_FUNC,1,EPSR,EPSI)              ! eps(q,omega)
      CALL SCREENING_VECTOR(SC_TYPE,DMN,X,RS,T,KS_SI)               ! screening vector
      CALL COULOMB_FF(DMN,'SIU',Q_SI,KS_SI,VC)                      ! Coulomb potential
!
      CALL EPS_TO_SQO(X,Z,T,RS,DMN,EPSR,EPSI,VC,SQ)                 !
!
      END SUBROUTINE STFACT_DYNAMIC_FROM_EPS
!
END MODULE STRUCTURE_FACTOR_DYNAMIC_2
