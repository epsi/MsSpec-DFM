!
!=======================================================================
!
MODULE STRUCTURE_FACTOR_STATIC 
!
      USE ACCURACY_REAL
!
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE STFACT_STATIC(X,RS,T,SQ_TYPE,SQ) 
!
!  This subroutine computes a static structure factor S(q) that 
!   does not involve the local field corrections G(q)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)FACT
!       * T        : temperature in SI
!       * SQ_TYPE  : structure factor approximation (3D)
!                       SQ_TYPE  = 'DEH' Debye-Hückel approximation
!                       SQ_TYPE  = 'GEA' generalized approximation
!                       SQ_TYPE  = 'GOR' Gorobchenko approximation
!                       SQ_TYPE  = 'GR2' computed from g(r) (GR_TO_SQ.f code)
!                       SQ_TYPE  = 'GSB' Gori-Giorgi-Sacchetti-Bachelet approximation
!                       SQ_TYPE  = 'HFA' Hartree-Fock approximation (only exchange)
!                       SQ_TYPE  = 'HUB' Hubbard approximation
!                       SQ_TYPE  = 'ICH' Ichimaru approximation
!                       SQ_TYPE  = 'LEE' Lee ideal Fermi gas
!                       SQ_TYPE  = 'MSA' mean spherical approximation
!                       SQ_TYPE  = 'PKA' Pietiläinen-Kallio
!                       SQ_TYPE  = 'RPA' RPA approximation
!                       SQ_TYPE  = 'SHA' Shaw approximation
!                       SQ_TYPE  = 'SIN' Singh
!                       SQ_TYPE  = 'SPA' Singh-Pathak
!                       SQ_TYPE  = 'TWA' Toigo-Woodruff approximation
!
!  Intermeduate parameters:
!
!       * GQ_TYPE  : local-field correction type (3D)
!       * EC_TYPE  : type of correlation energy functional
!
!
!  Output parameters:
!
!       * SQ       : static structure factor
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  3 Dec 2020
!
!
      USE MATERIAL_PROP,    ONLY : DMN
      USE LF_VALUES,        ONLY : GQ_TYPE
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)    ::  SQ_TYPE
!
      REAL (WP), INTENT(IN)  ::  X,RS,T
      REAL (WP), INTENT(OUT) ::  SQ
!
      IF(DMN == '3D') THEN                                          !
        CALL STFACT_STATIC_3D(X,RS,T,SQ_TYPE,GQ_TYPE,SQ)    !
      ELSE IF(DMN == '2D') THEN                                     !
        CONTINUE                                                    !
      ELSE IF(DMN == '1D') THEN                                     !
        CONTINUE                                                    !
      END IF                                                        !
!
      END SUBROUTINE STFACT_STATIC
!
!=======================================================================
!
      SUBROUTINE STFACT_STATIC_3D(X,RS,T,SQ_TYPE,GQ_TYPE,SQ) 
!
!  This subroutine computes a static structure factor S(q) 
!    for 3D systems
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * SQ_TYPE  : structure factor approximation (3D)
!                       SQ_TYPE  = 'DEH' Debye-Hückel approximation
!                       SQ_TYPE  = 'GEA' generalized approximation
!                       SQ_TYPE  = 'GOR' Gorobchenko approximation
!                       SQ_TYPE  = 'GR2' computed from g(r) (GR_TO_SQ.f code)
!                       SQ_TYPE  = 'GSB' Gori-Giorgi-Sacchetti-Bachelet approximation
!                       SQ_TYPE  = 'HFA' Hartree-Fock approximation (only exchange)
!                       SQ_TYPE  = 'HUB' Hubbard approximation
!                       SQ_TYPE  = 'ICH' Ichimaru approximation
!                       SQ_TYPE  = 'LEE' Lee ideal Fermi gas
!                       SQ_TYPE  = 'MSA' mean spherical approximation
!                       SQ_TYPE  = 'PKA' Pietiläinen-Kallio
!                       SQ_TYPE  = 'RPA' RPA approximation
!                       SQ_TYPE  = 'SHA' Shaw approximation
!                       SQ_TYPE  = 'SIN' Singh
!                       SQ_TYPE  = 'SPA' Singh-Pathak
!                       SQ_TYPE  = 'TWA' Toigo-Woodruff approximation
!       * GQ_TYPE  : local-field correction type (3D)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Jun 2020
!
!
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  GQ_TYPE
      CHARACTER (LEN = 3)   ::  SQ_TYPE
!
      REAL (WP)             ::  X,RS,T
      REAL (WP)             ::  SQ
!
      IF(SQ_TYPE == 'DEH') THEN                         !
        SQ = DEH_SF(X,RS,T)                             !
      ELSE IF(SQ_TYPE == 'HFA') THEN                    !
        SQ = HFA_SF(X)                                  !
      ELSE IF(SQ_TYPE == 'GOR') THEN                    !
        SQ = GOR_SF(X,RS)                               !  
      ELSE IF(SQ_TYPE == 'GSB') THEN                    !
        SQ = GSB_SF(X,RS)                               !  
      ELSE IF(SQ_TYPE == 'HUB') THEN                    !
        SQ = HUB_SF(X)                                  !
      ELSE IF(SQ_TYPE == 'LEE') THEN                    !
        SQ = LEE_SF(X)                                  !
      ELSE IF(SQ_TYPE == 'MSA') THEN                    !
        SQ = MSA_SF(X,RS)                               !
      ELSE IF(SQ_TYPE == 'RPA') THEN                    !
        SQ = RPA_SF(X,RS)                               !
      ELSE IF(SQ_TYPE == 'SHA') THEN                    !
        SQ = SHA_SF(X)                                  !
      ELSE IF(SQ_TYPE == 'TWA') THEN                    !
        SQ = TWA_SF(X,RS)                               !
      END IF                                            !
!
      END SUBROUTINE STFACT_STATIC_3D  
!
!=======================================================================
!
      FUNCTION DEH_SF(X,RS,T)
!
!  This function computes the Debye-Hückel approximation static 
!    structure factor S(q) for 3D systems
!
!  References:  (1) S. Ichimaru, Rev. Mod. Phys. 54, 1017-1059 (1982)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  1 Oct 2020
!
!
      USE FERMI_SI,           ONLY : KF_SI
      USE SCREENING_VEC,      ONLY : DEBYE_VECTOR
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X,RS,T
      REAL (WP)             ::  DEH_SF
      REAL (WP)             ::  Y
      REAL (WP)             ::  Q_SI,Q2_SI,KD_SI
!
      Y     = X + X                                                 ! Y = q / k_F
      Q_SI  = Y * KF_SI                                             ! q in SI
      Q2_SI = Q_SI * Q_SI                                           !
!
!  Computing the Debye screening vector
!
      CALL DEBYE_VECTOR('3D',T,RS,KD_SI)                            !
!
      DEH_SF = Q2_SI / (Q2_SI + KD_SI * KD_SI)                      ! ref. (1) eq. (2.2)
!
      END FUNCTION DEH_SF 
!
!=======================================================================
!
!
      FUNCTION GR2_SF(X,RS,T)
!
!  This function computes the static structure factor S(q) 
!    from the pair correlation g(r)
!
!  References:  
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Sep 2020
!
!
      USE REAL_NUMBERS,       ONLY : SIX
      USE PC_VALUES,          ONLY : GR_TYPE
      USE PD_VALUES,          ONLY : RH_TYPE
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X,RS,T
      REAL (WP)             ::  GR2_SF
      REAL (WP)             ::  Y
      REAL (WP)             ::  SQ
!
      REAL (WP), PARAMETER  ::  MAX_R = SIX       ! upper r-integration value
!
      Y = X + X                                                     ! q/k_F
!
      CALL GR_TO_SQ_3D(Y,MAX_R,T,RS,GR_TYPE,RH_TYPE,SQ)
!
      GR2_SF = SQ                                                   !
!
CONTAINS
!
!-----------------------------------------------------------------------
!
      SUBROUTINE GR_TO_SQ_3D(Q,MAX_R,T,RS,GR_TYPE,RH_TYPE,SQ) 
!
!  This subroutine computes the 3D static structure factor S(q)
!    from the pair correlation function g(r) according to
!
!                    / + inf                          
!                   |               -i q.r
!    S(q) = 1 + n   |   ( g(r)-1 ) e        dr
!                   |
!                  / 0                               
! 
!                            / + inf                          
!                 4 pi n    |
!         = 1 +  --------   |  r sin(qr) ( g(r)-1 ) dr
!                    q      |
!                          / 0                               
!
!
!  Input parameters:
!
!       * Q        : point q where S(q) is computed
!       * MAX_R    : upper integration value
!       * T        : temperature in SI
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * GR_TYPE  : pair correlation function type (3D)
!       * RH_TYPE  : choice of pair distribution function rho_2(r) (3D)
!
!
!  Output variables :
!
!       * SQ       : S(q) at point q
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  4 Jun 2020
!
!
      USE DIMENSION_CODE,   ONLY : NSIZE
      USE REAL_NUMBERS,     ONLY : ONE,FOUR
      USE PI_ETC,           ONLY : PI
      USE UTILITIES_1,      ONLY : RS_TO_N0
      USE INTEGRATION,      ONLY : INTEGR_L
      USE PAIR_CORRELATION, ONLY : PAIR_CORRELATION_3D
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  GR_TYPE,RH_TYPE
!
      REAL (WP), INTENT(IN) ::  Q,T,RS,MAX_R
      REAL (WP)             ::  GR,SQ
      REAL (WP)             ::  N0,R
      REAL (WP)             ::  INTF(NSIZE),XA(NSIZE),H,IN
!
      INTEGER               ::  NMAX,K,N1,ID
!
!  Computing the electron density
!
      N0=RS_TO_N0('3D',RS)                                          !
!
!  Computing the integrand function
!
      N1=NMAX                                                       ! index of upper bound
      DO K=1,NMAX                                                   !
!
        XA(K)=MAX_R*FLOAT(K-1)/FLOAT(NSIZE-1)                       !
        R=XA(K)                                                     !
!
!  Computing the pair correlation factor g(r)
!
        CALL PAIR_CORRELATION_3D(R,RS,T,GR_TYPE,RH_TYPE,GR)         !
!  
        INTF(K)=XA(K)*SIN(Q*XA(K))*(GR-ONE)                        !
!
      END DO                                                        !
!
      H=XA(2)-XA(1)                                                 ! step
      ID=1                                                          !
!
!  Computing the integral
!
      CALL INTEGR_L(INTF,H,NMAX,N1,IN,ID)                           !
!
      SQ=ONE + (FOUR*PI*N0/Q) * IN                                  !
!
      END SUBROUTINE GR_TO_SQ_3D
!
!-----------------------------------------------------------------------
!
      END FUNCTION GR2_SF
!
!=======================================================================
!
      FUNCTION GSB_SF(X,RS)
!
!  This function computes the Gori-Giorgi-Sacchtti-Bachelet 
!    static structure factor S(q) for 3D systems
!
!  References:  (1) P. Gori-Giorgi, F. Sacchetti and G. B. Bachelet,
!                      Phys. Rev. B 61, 7353-7363 (2000)
!               (2) P. Gori-Giorgi, F. Sacchetti and G. B. Bachelet,
!                      Phys. Rev. B 66, 159901 (2002)    <-- Erratum
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!  Note: They write S(q) as 
!
!        S(q) = S_{ex}(q) + S_c^{++}(q) + S_c^{+-}(q) 
!                  |
!                  ---> Hartree-Fock exchange value
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 24 Sep 2020
!
!
      USE REAL_NUMBERS,       ONLY : ONE,TWO,THREE,FOUR,FIVE,SEVEN,EIGHT,NINE, & 
                                     HALF,THIRD,FOURTH,SIXTH
      USE PI_ETC,             ONLY : PI,PI2,PI_INV
      USE ENE_CHANGE,         ONLY : RYD
      USE UTILITIES_1,        ONLY : ALFA
      USE PLASMON_ENE_EV
!
      USE ZETA_RIEMANN
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X,RS
      REAL (WP)             ::  GSB_SF
      REAL (WP)             ::  KF,OP
      REAL (WP)             ::  K,K2,K3,K4,K5,K6,K7,K8,K9,K10,K12
      REAL (WP)             ::  ALPHA
      REAL (WP)             ::  S0,SCP,SCM
      REAL (WP)             ::  AA_P,AA_M,BB_P,BB_M
      REAL (WP)             ::  A_P,B1_P,B_P,A_M,B1_M,B_M
      REAL (WP)             ::  P3,L4_P,L5_P,L6_P,G4_P,G5_P,G6_P
      REAL (WP)             ::  K_3,L4_M,L5_M,L6_M,G4_M,G5_M,G6_M
      REAL (WP)             ::  K_1,K_2,P1,P2
      REAL (WP)             ::  C1_P,C2_P,C3_P,C4_P,C5_P,C6_P
      REAL (WP)             ::  C1_M,C2_M,C3_M,C4_M,C5_M,C6_M
      REAL (WP)             ::  A6_P,A8_P,A10_P,A4_M,A6_M
      REAL (WP)             ::  K2_T,AB2,KOEF
      REAL (WP)             ::  FCT(10)
!
      REAL (WP), PARAMETER  ::  AA   = (ONE - LOG(TWO)) / PI2
      REAL (WP), PARAMETER  ::  BB_D = - 0.0711E0_WP
      REAL (WP), PARAMETER  ::  BB_X = SIXTH * LOG(TWO) - 0.75E0_WP * ZETA(3) / PI2
!
      REAL (WP)             ::  LOG,SQRT,EXP
!
      DATA FCT /        1.0E0_WP,    &  !  -->   1!     Factorials
                        2.0E0_WP,    &  !  -->   2!
                        6.0E0_WP,    &  !  -->   3!
                       24.0E0_WP,    &  !  -->   4! 
                      120.0E0_WP,    &  !  -->   5!   
                      720.0E0_WP,    &  !  -->   6!   
                     5040.0E0_WP,    &  !  -->   7!  
                    40320.0E0_WP,    &  !  -->   8!  
                   362880.0E0_WP,    &  !  -->   9!  
                  3628800.0E0_WP     &  !  -->  10!  
               /
!
      K = X + X                                                     ! q / k_F
!
!  Powers of k
!
      K2  = K  * K                                                  !
      K3  = K2 * K                                                  !
      K4  = K3 * K                                                  !
      K5  = K4 * K                                                  !
      K6  = K5 * K                                                  !
      K7  = K6 * K                                                  !
      K8  = K7 * K                                                  !
      K9  = K8 * K                                                  !
      K10 = K9 * K                                                  !
      K12 = K6 * K6                                                 !
!
      ALPHA = ALFA('3D')                                            !
!
      KF = ALPHA / RS                                               ! k_F in AU
!
      KOEF = FIVE / 11.0E0_WP                                       ! corrected coef. for eq. (44)
!
!  Plasmon energy in Ry
!
      OP = ENE_P_EV / RYD                                           !
!
!  Computation of the exchange Hartree-Fock contribution S0
!
      S0 = HFA_SF(X)                                                !
!
!  Computation of the correlation S_c^{++}(q) contribution SCP
!
      AA_P = HALF * AA                                              ! A^++
      BB_P = HALF * BB_D                                            ! B^++
      A_P  = 1.32E0_WP                                              ! a^++
      B1_P = 3.47E0_WP                                              ! b1^++
      B_P  = ALPHA * PI * SQRT(THREE / RS) + B1_P                   ! b^++ : ref. (1) eq. (51)
      AB2  = A_P * A_P * B_P * B_P                                  ! (a^++ * b^++)^2
      P3   =  0.015E0_WP                                            ! p3
      L4_P =   98.0E0_WP                                            ! lambda_4^++
      L5_P = -295.0E0_WP                                            ! lambda_5^++
      L6_P =  170.0E0_WP                                            ! lambda_6^++
      G4_P = - 36.0E0_WP                                            ! gamma_4^++
      G5_P =   74.0E0_WP                                            ! gamma_5^++
      G6_P = - 13.0E0_WP                                            ! gamma_6^++
      P1   = 18.0E0_WP * PI * A_P * A_P * AA_P / ALPHA              ! p1   : ref. (1) eq. (39)
      P2   = 729.0E0_WP * A_P * A_P / (64.0E0_WP * ALPHA**FOUR) - & !
              21.0E0_WP / (64.0E0_WP * A_P * ALPHA)             + & ! p2   : ref. (1) eq. (40) 
             NINE * HALF * A_P * A_P * PI * (AA_P + TWO * BB_P) / & !
             ALPHA                                                  !
!
      C1_P = THREE / EIGHT                                          !
      C2_P = B_P * C1_P + FOURTH * KF * KF / OP                     ! 
      C3_P = B_P * B_P * HALF * C1_P +                            & ! 
             B_P * FOURTH * KF * KF / OP - ONE / 32.0E0_WP          !
      C4_P = (L4_P + G4_P * RS) / (ONE + RS**1.5E0_WP)              ! c4^++: \
      C5_P = (L5_P + G5_P * RS) / (ONE + RS**1.5E0_WP)              ! c5^++:  > ref. (1) eq. (52)
      C6_P = (L6_P + G6_P * RS) / (ONE + RS**1.5E0_WP)              ! c6^++: /
!
      A6_P = EIGHT * ( ONE - P1 * RS * LOG(ONE + P2 / RS) ) /     & ! alpha_6^++ : ref. (1) eq. (49)
                     ( FIVE * PI * KF * (ONE + P3 * RS * RS) )      !
      A8_P = 2048.0E0_WP * THIRD * PI_INV * A_P**5 * (            & !
               C1_P * (FCT(3) - KOEF * FCT(5)  / AB2) / B_P**4 +  & !
               C2_P * (FCT(4) - KOEF * FCT(6)  / AB2) / B_P**5 +  & !
               C3_P * (FCT(5) - KOEF * FCT(7)  / AB2) / B_P**6 +  & !
               C4_P * (FCT(6) - KOEF * FCT(8)  / AB2) / B_P**7 +  & !
               C5_P * (FCT(7) - KOEF * FCT(9)  / AB2) / B_P**8 +  & ! alpha_8^++ : ref. (1) eq. (44)
               C6_P * (FCT(8) - KOEF * FCT(10) / AB2) / B_P**9    & !              
                                                     )  +         & !  corrected : ref. (2) eq. (2) 
             4096.0E0_WP * A_P**3 / (33.0E0_WP * PI)    -         & !
             A6_P * A_P**3 * ( 2560.0E0_WP * KF / 33.0E0_WP +     & !
                               26.0E0_WP / A_P                    & !
                             )                                      !
      A10_P= 2048.0E0_WP * THIRD * PI_INV * A_P**7 * (            & !
             C1_P * (FCT(5)  / AB2   - 13.0E0_WP * THIRD * FCT(3)) / B_P**4 +  & !
             C2_P * (FCT(6)  / AB2   - 13.0E0_WP * THIRD * FCT(4)) / B_P**5 +  & !
             C3_P * (FCT(7)  / AB2   - 13.0E0_WP * THIRD * FCT(5)) / B_P**6 +  & !
             C4_P * (FCT(8)  / AB2   - 13.0E0_WP * THIRD * FCT(6)) / B_P**7 +  & ! ref. (1) eq. (45)
             C5_P * (FCT(9)  / AB2   - 13.0E0_WP * THIRD * FCT(7)) / B_P**8 +  & !
             C6_P * (FCT(10) / AB2   - 13.0E0_WP * THIRD * FCT(8)) / B_P**9    & !
                                                     )  -         & !
             4096.0E0_WP * A_P**5 / (15.0E0_WP * PI)    +         & !
              A6_P * A_P**5 * THIRD * ( 143.0E0_WP / A_P +        & !
                                        512.0E0_WP * KF           & !
                                      )                             !
!
      SCP = EXP(- B_P * K) * ( C1_P * K  + C2_P * K2   +          & !
                               C3_P * K3 + C4_P * K4   +          & ! ref. (1) eq. (41)
                               C5_P * K5 + C6_P * K6 ) +          & !
            (A10_P * K8 + A8_P * K10 + A6_P * K12)     /          & !
            (A_P * A_P + K2)**NINE                                  !
!
!  Computation of the correlation S_c^{+-}(q) contribution SCM
!
      AA_M = HALF * AA                                              ! A^+-
      BB_M = HALF * BB_D + BB_X                                     ! B^+-
      A_M  = 0.838E0_WP                                             ! a^+-
      B1_M = 3.27E0_WP                                              ! b1^+-
      B_M  = ALPHA * PI * SQRT(THREE / RS) + B1_M                   ! b^+- : ref. (1) eq. (51)
      K_3  =  0.141E0_WP                                            ! k3
      L4_M = - 78.0E0_WP                                            ! lambda_4^+-
      L5_M =  216.0E0_WP                                            ! lambda_5^+-
      L6_M = -140.0E0_WP                                            ! lambda_6^+-
      G4_M =   28.0E0_WP                                            ! gamma_4^+-
      G5_M = -124.0E0_WP                                            ! gamma_5^+-
      G6_M =   55.0E0_WP                                            ! gamma_6^+-
      K_1  = 18.0E0_WP * PI * A_M * A_M * AA_M / ALPHA              ! k1   : ref. (1) eq. (39)
      K_2  = 729.0E0_WP * A_M * A_M / (64.0E0_WP * ALPHA**FOUR) - & !
              21.0E0_WP / (64.0E0_WP * A_M * ALPHA)             + & ! k2   : ref. (1) eq. (40) 
             NINE * HALF * A_M * A_M * PI * (AA_M + TWO * BB_M) / & !
             ALPHA                                                  !
      K2_T = EXP( SEVEN / (384.0E0_WP * A_M * A_M * A_M * AA_M) - & !
                  81.0E0_WP / (128.0E0_WP * ALPHA**3 * AA_M)    - & !
                  BB_M / AA_M - HALF                              & !
                )                                                   !
!
      C1_M = - THREE / EIGHT                                        ! c1^+-: ref. (1) eq. (30)
      C2_M = B_M * C1_M + FOURTH * KF * KF / OP                     ! c2^+-: ref. (1) eq. (31)
      C3_M = B_M * B_M * HALF * C1_M +                            & ! c3^+-: ref. (1) eq. (31)
             B_M * FOURTH * KF * KF / OP + ONE / 32.0E0_WP          !
      C4_M = (L4_M + G4_M * RS) / (ONE + RS**1.5E0_WP)              ! c4^+-: \
      C5_M = (L5_M + G5_M * RS) / (ONE + RS**1.5E0_WP)              ! c5^+-:  > ref. (1) eq. (52)
      C6_M = (L6_M + G6_M * RS) / (ONE + RS**1.5E0_WP)              ! c6^+-: /
!
      A4_M = - (FOUR * (ONE - K_1 * RS * LOG(ONE + K2_T / RS))) / & ! alpha_4^+- : ref. (1) eq. (48)
               (THREE * PI * KF * (ONE  + K_3 * RS * RS))           ! 
      A6_M = A_M * A_M * A_M * (                                  & !
                                  A4_M * ( - 11.0E0_WP /  A_M -   & !
                                  512.0E0_WP * KF / 21.0E0_WP ) - & !
                                  2048.0E0_WP / (21.0E0_WP * PI)* & !
                                  ( THIRD                       + & !           
                                    C1_M * FCT(3) / B_M**4      + & ! alpha_6^+- : ref. (1) eq. (33)
                                    C2_M * FCT(4) / B_M**5      + & ! 
                                    C3_M * FCT(5) / B_M**6      + & ! corrected  : ref. (2) eq. (1) 
                                    C4_M * FCT(6) / B_M**7      + & ! 
                                    C5_M * FCT(7) / B_M**8      + & ! 
                                    C6_M * FCT(8) / B_M**9        & !   
                                  )                               & !
                                )                                   !
!
      SCM = EXP(- B_M * K) * ( C1_M * K  + C2_M * K2   +          & !
                               C3_M * K3 + C4_M * K4   +          & ! ref. (1) eq. (27)
                               C5_M * K5 + C6_M * K6 ) +          & !
            (A6_M * K8 + A4_M * K10) / (A_M * A_M + K2)**SEVEN      !
!
!  Value of S(q)
!
      GSB_SF = S0 + SCP + SCM                                       !
!
      END FUNCTION GSB_SF
!
!=======================================================================
!
      FUNCTION GOR_SF(X,RS)
!
!  This function computes Gorobchenko static structure factor S(q) 
!    for 3D systems
!
!  References:  V. G. Kohn and V. D. Gorobchenko, 
!                  J. Phys. C:Solid State Phys. 15, 2935-2950 (1982)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 10 Jun 2020
!
!
      USE REAL_NUMBERS,       ONLY : ONE,FOUR,EIGHT, & 
                                     THIRD
      USE PI_ETC,             ONLY : PI_INV
      USE UTILITIES_1,        ONLY : ALFA
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X,RS
      REAL (WP)             ::  Y2_INV
      REAL (WP)             ::  Y,Y2 
      REAL (WP)             ::  GOR_SF
      REAL (WP)             ::  Y4,Y4_INV
      REAL (WP)             ::  ALPHA,ZS
!
      ALPHA = ALFA('3D')                                            !
!
      Y  = X + X                                                    ! Y = q / k_F
      Y2 = Y * Y                                                    !
      Y4 = Y2 * Y2                                                  !
!
      Y2_INV = ONE / Y2                                             !
      Y4_INV = ONE / Y4                                             !
!
      ZS = ALPHA * RS * PI_INV / Y4                                 !
!
      GOR_SF  = ONE - FOUR * THIRD * ZS * (                       & !
                   ONE - 0.40E0_WP * Y2_INV -                     & ! ref. (1) eq. (4.14)
                   176.0E0_WP * Y4_INV / 175.0E0_WP               & !
                                           ) +                    & !
                EIGHT * ZS * ZS * THIRD                         !
!
      END FUNCTION GOR_SF  
!
!=======================================================================
!
      FUNCTION HFA_SF(X)
!
!  This function computes Hartree-Fock static structure factor S(q) 
!    for 3D systems
!
!  References: (1) H. B. Singh and K. N. Pathak, Phys. Rev. B 8, 
!                     6035-6937 (1973)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 10 Jun 2020
!
!
      USE REAL_NUMBERS,       ONLY : ONE,TWO
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X
      REAL (WP)             ::  HFA_SF
      REAL (WP)             ::  Y,Y3
!
      Y  = X + X                                                    ! Y = q / k_F 
      Y3 = Y * Y * Y                                                ! 
!
      IF(Y <= TWO) THEN                                             !
        HFA_SF = 0.75E0_WP * Y - Y3 / 16.0E0_WP                     ! 
      ELSE                                                          !
        HFA_SF = ONE                                                ! 
      END IF                                                        !
!
      END FUNCTION HFA_SF  
!
!=======================================================================
!
      FUNCTION HUB_SF(X)
!
!  This function computes Hubbard static structure factor S(q) 
!    for 3D systems
!
!  References: (1) R. W. Shaw, J. Phys. C: Solid State Phys. 3, 
!                     1140-1158 (1970)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Nov 2020
!
!
      USE REAL_NUMBERS,       ONLY : ONE,THREE,FOUR,THIRD
      USE PI_ETC,             ONLY : PI,PI_INV
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X
      REAL (WP)             ::  HUB_SF
      REAL (WP)             ::  Y,Y2
      REAL (WP)             ::  AL2,COEF
!
      Y  = X + X                                                    ! Y = q / k_F
      Y2 = Y * Y                                                    !
!
      AL2   = FOUR / SQRT(THREE * PI)                               !
      COEF  = 16.0E0_WP * THIRD * PI_INV                            !
!
      HUB_SF = ONE - COEF * AL2 / (AL2 + Y2)**3                     ! ref. (1) eq. (5.3)
!
      END FUNCTION HUB_SF  
!
!=======================================================================
!
      FUNCTION LEE_SF(X)
!
!  This function computes Lee's static structure factor S(q) 
!    for 3D systems
!
!  References:  M. H. Lee, J. Math. Phys. 36, 1136-1145 (1995)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Oct 2020
!
!
      USE REAL_NUMBERS,           ONLY : ONE,THREE,HALF
      USE CONFLUENT_HYPGEOM_REAL, ONLY : HYGFX
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X
      REAL (WP)             ::  LEE_SF
      REAL (WP)             ::  A,B,C,X2
      REAL (WP)             ::  HF1,HF2
!
      IF(X < ONE) THEN                                              !
        X2 = X * X                                                  !
        A  = HALF                                                   !
        B  = ONE                                                    !
        C  = THREE * A                                              !
!
        CALL HYGFX(A,-B,C,X2,HF1)                                   !
        CALL HYGFX(A,-B,C,ONE,HF2)                                  !
!
        LEE_SF = X * HF1 / HF2                                      ! ref. (1) eq. (12a)
!
      ELSE                                                          !
        LEE_SF = ONE                                                ! ref. (1) eq. (12b)
      END IF                                                        !
!
      END FUNCTION LEE_SF
!
!=======================================================================
!
      FUNCTION MSA_SF(X,RS)
!
!  This function computes mean spherical static structure factor S(q) 
!    for 3D systems
!
!  References:  A. Gold and L. Calmels, Phys. Rev. B 48,
!                  11622-11637 (1993)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 10 Jun 2020
!
!
      USE REAL_NUMBERS,       ONLY : ONE,FOUR
      USE CONSTANTS_P1,       ONLY : H_BAR,M_E,E,EPS_0
      USE FERMI_SI,           ONLY : KF_SI
      USE UTILITIES_1,        ONLY : RS_TO_N0
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X,Y
      REAL (WP)             ::  MSA_SF
      REAL (WP)             ::  VQ,Q_SI,S0,SP
      REAL (WP)             ::  RS,N0
!
      REAL (WP)             ::  DSQRT
!
      Y=X+X                                                         ! Y = q / k_F
!
      Q_SI=Y*KF_SI                                                  ! q in SI
!
      VQ=E*E/(EPS_0*Q_SI*Q_SI)                                      ! Coulomb potential
!
!  noninteracting electron gas structure factor
!
      S0=HFA_SF(X)                                                  !
!
      N0=RS_TO_N0('3D',RS)                                          !
!
!  plasmon contribution
!
      SP=H_BAR*Q_SI/DSQRT(FOUR*M_E*N0*VQ)                           !
!
      MSA_SF=ONE/DSQRT(ONE/(S0*S0) + ONE/(SP*SP))                   ! ref. (1) eq. (3)
!
      END FUNCTION MSA_SF  
!
!=======================================================================
!
      FUNCTION RPA_SF(X,RS)
!
!  This function computes RPA static structure factor S(q) 
!    for 3D systems
!
!  References:  V. G. Kohn and V. D. Gorobchenko, 
!                  J. Phys. C:Solid State Phys. 15, 2935-2950 (1982)
!                  
!                  
!  Warning : Asymptotic value for q/k_F > 2
!
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Oct 2020
!
!
      USE REAL_NUMBERS,       ONLY : ONE,TWO,THREE,FOURTH,EIGHT,THIRD
      USE PI_ETC,             ONLY : PI,PI_INV
      USE UTILITIES_1,        ONLY : ALFA
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X,RS
      REAL (WP)             ::  RPA_SF
      REAL (WP)             ::  Y,Y2,Y2_INV
      REAL (WP)             ::  Y4,Y4_INV
      REAL (WP)             ::  ALPHA
      REAL (WP)             ::  YS,ZS
!
      REAL (WP)             ::  SQRT
!
      ALPHA = ALFA('3D')                                            !
!
      Y  = X + X                                                    ! Y = q / k_F
      Y2 = Y  * Y                                                   !
      Y4 = Y2 * Y2                                                  !
!
      Y2_INV = ONE / Y2                                             !
      Y4_INV = ONE / Y4                                             !
!
      YS = SQRT(THREE * PI / (ALPHA * RS))                          !
      ZS = ALPHA * RS * PI_INV / Y4                                 !
!
      IF(Y <= 1.E0_WP) THEN                                        !
        RPA_SF = FOURTH * YS * Y2                                   !
      ELSE                                                          !
        RPA_SF = ONE - EIGHT * THIRD * ZS * (                     & !
                   ONE + 0.40E0_WP * Y2_INV +                     & ! ref. (1) eq. (4.12)
                   72.0E0_WP * Y4_INV / 175.0E0_WP                & !
                                             ) +                  & !
                 32.0E0_WP * ZS * ZS * THIRD                        !
      END IF                                                        !
!
      END FUNCTION RPA_SF  
!
!=======================================================================
!
      FUNCTION SHA_SF(X)
!
!  This function computes Shaw static structure factor S(q) 
!    for 3D systems
!
!  References: (1) R. W. Shaw, J. Phys. C: Solid State Phys. 3, 
!                     1140-1158 (1970)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  3 Dec 2020
!
!
      USE REAL_NUMBERS,       ONLY : ONE,NINE,THIRD,FOURTH,TTINY
      USE PI_ETC,             ONLY : PI
!
      USE MINMAX_VALUES
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X
      REAL (WP)             ::  SHA_SF
      REAL (WP)             ::  Y,Y2
      REAL (WP)             ::  AL2,EXPO,EXPA
      REAL (WP)             ::  MAX_EXP,MIN_EXP
!
      REAL (WP)             ::  EXP
!
      CALL MINMAX_EXP(MAX_EXP,MIN_EXP)                              !
!
      Y  = X + X                                                    ! Y = q / k_F
      Y2 = Y * Y                                                    !
!
      AL2  = ONE / (NINE * PI)**THIRD                               !
      EXPO = - FOURTH * Y2 / AL2                                    !
      IF(EXPO  > MIN_EXP) THEN                                      !
        EXPA = EXP(EXPO)                                            !
      ELSE                                                          !
        EXPA = TTINY                                                !
      END IF                                                        !
!
      SHA_SF = ONE - EXPA                                           ! ref. (1) eq. (5.6)
!
      END FUNCTION SHA_SF  
!
!=======================================================================
!
      FUNCTION TWA_SF(X,RS)
!
!  This function computes Toigo-Woodruff static structure factor S(q) 
!    for 3D systems
!
!  References:  V. G. Kohn and V. D. Gorobchenko, 
!                  J. Phys. C:Solid State Phys. 15, 2935-2950 (1982)
!                  
!                  
!  Warning : Asymptotic value for q/k_F > 2
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 10 Jun 2020
!
!
      USE REAL_NUMBERS,       ONLY : ONE,TWO,TEN,THIRD
      USE PI_ETC,             ONLY : PI_INV
      USE UTILITIES_1,        ONLY : ALFA
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X,Y,RS,Y2,Y2_INV
      REAL (WP)             ::  TWA_SF
      REAL (WP)             ::  Y4,Y4_INV
      REAL (WP)             ::  ALPHA,ZS
!
!
      ALPHA = ALFA('3D')                                            !
!
      Y  = X + X                                                    ! Y = q / k_F
      Y2 = Y  * Y                                                   !
      Y4 = Y2 * Y2                                                  !
!
      Y2_INV = ONE / Y2                                             !
      Y4_INV = ONE / Y4                                             !
!
      ZS = ALPHA * RS * PI_INV                                      !
!
      TWA_SF = ONE - TEN * THIRD * THIRD * ZS * Y4_INV * (        & !
                   ONE + 129.0E0_WP * Y2_INV / 250.0E0_WP     +   & !
                   3162.0E0_WP * Y4_INV / 6125.0E0_WP      )  +   & ! ref. (1) eq. (4.13)
               128.0E0_WP * (ZS * Y4_INV)**2  / 81.0E0_WP           !
!
      END FUNCTION TWA_SF  
!
END MODULE STRUCTURE_FACTOR_STATIC
