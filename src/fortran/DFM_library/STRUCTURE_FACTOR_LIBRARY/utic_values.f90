!
!=======================================================================
!
MODULE UTIC_VALUES
!
!  This module stoes the values of the omega-independent 
!      Utsumi-Ichimaru parameters
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  TAU_Q,OM_Q,GAM_Q,MO_Q,MO_0
!
END MODULE UTIC_VALUES 
