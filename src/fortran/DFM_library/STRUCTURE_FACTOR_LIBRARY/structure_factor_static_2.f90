!
!=======================================================================
!
MODULE STRUCTURE_FACTOR_STATIC_2 
!
      USE ACCURACY_REAL
!
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE STFACT_STATIC_2(X,RS,T,SQ_TYPE,SQ) 
!
!  This subroutine computes a static structure factor S(q)  with the 
!    help of the local field correction G(q)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)FACT
!       * T        : temperature in SI
!       * SQ_TYPE  : structure factor approximation (3D)
!                       SQ_TYPE  = 'DEH' Debye-Hückel approximation
!                       SQ_TYPE  = 'GEA' generalized approximation
!                       SQ_TYPE  = 'GOR' Gorobchenko approximation
!                       SQ_TYPE  = 'GR2' computed from g(r) (GR_TO_SQ.f code)
!                       SQ_TYPE  = 'GSB' Gori-Giorgi-Sacchetti-Bachelet approximation
!                       SQ_TYPE  = 'HFA' Hartree-Fock approximation (only exchange)
!                       SQ_TYPE  = 'HUB' Hubbard approximation
!                       SQ_TYPE  = 'ICH' Ichimaru approximation
!                       SQ_TYPE  = 'LEE' Lee ideal Fermi gas
!                       SQ_TYPE  = 'MSA' mean spherical approximation
!                       SQ_TYPE  = 'PKA' Pietiläinen-Kallio
!                       SQ_TYPE  = 'RPA' RPA approximation
!                       SQ_TYPE  = 'SHA' Shaw approximation
!                       SQ_TYPE  = 'SIN' Singh
!                       SQ_TYPE  = 'SPA' Singh-Pathak
!                       SQ_TYPE  = 'TWA' Toigo-Woodruff approximation
!
!  Intermeduate parameters:
!
!       * GQ_TYPE  : local-field correction type (3D)
!       * EC_TYPE  : type of correlation energy functional
!
!
!  Output parameters:
!
!       * SQ       : static structure factor
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Jun 2020
!
!
      USE MATERIAL_PROP,    ONLY : DMN
      USE LF_VALUES,        ONLY : GQ_TYPE
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)    ::  SQ_TYPE
!
      REAL (WP), INTENT(IN)  ::  X,RS,T
      REAL (WP), INTENT(OUT) ::  SQ
!
      IF(DMN == '3D') THEN                                          !
        CALL STFACT_STATIC_3D_2(X,RS,T,SQ_TYPE,GQ_TYPE,SQ)          !
      ELSE IF(DMN == '2D') THEN                                     !
        CONTINUE                                                    !
      ELSE IF(DMN == '1D') THEN                                     !
        CONTINUE                                                    !
      END IF                                                        !
!
      END SUBROUTINE STFACT_STATIC_2
!
!=======================================================================
!
      SUBROUTINE STFACT_STATIC_3D_2(X,RS,T,SQ_TYPE,GQ_TYPE,SQ) 
!
!  This subroutine computes a static structure factor S(q) 
!    for 3D systems
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * SQ_TYPE  : structure factor approximation (3D)
!                       SQ_TYPE  = 'DEH' Debye-Hückel approximation
!                       SQ_TYPE  = 'GEA' generalized approximation
!                       SQ_TYPE  = 'GOR' Gorobchenko approximation
!                       SQ_TYPE  = 'GR2' computed from g(r) (GR_TO_SQ.f code)
!                       SQ_TYPE  = 'GSB' Gori-Giorgi-Sacchetti-Bachelet approximation
!                       SQ_TYPE  = 'HFA' Hartree-Fock approximation (only exchange)
!                       SQ_TYPE  = 'HUB' Hubbard approximation
!                       SQ_TYPE  = 'ICH' Ichimaru approximation
!                       SQ_TYPE  = 'LEE' Lee ideal Fermi gas
!                       SQ_TYPE  = 'MSA' mean spherical approximation
!                       SQ_TYPE  = 'PKA' Pietiläinen-Kallio
!                       SQ_TYPE  = 'RPA' RPA approximation
!                       SQ_TYPE  = 'SHA' Shaw approximation
!                       SQ_TYPE  = 'SIN' Singh
!                       SQ_TYPE  = 'SPA' Singh-Pathak
!                       SQ_TYPE  = 'TWA' Toigo-Woodruff approximation
!       * GQ_TYPE  : local-field correction type (3D)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  3 Dec 2020
!
!
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  GQ_TYPE
      CHARACTER (LEN = 3)   ::  SQ_TYPE
!
      REAL (WP)             ::  X,RS,T
      REAL (WP)             ::  SQ
!
      IF(SQ_TYPE == 'GEA') THEN                         !
        SQ = GEA_SF(X,RS,T,GQ_TYPE)                     !
      ELSE IF(SQ_TYPE == 'ICH') THEN                    !
        SQ = ICH_SF(X,RS,T,GQ_TYPE)                     !  
      ELSE IF(SQ_TYPE == 'PKA') THEN                    !
        SQ = PKA_SF(X,RS,T,GQ_TYPE)                     !
      ELSE IF(SQ_TYPE == 'SIN') THEN                    !
        SQ = SIN_SF(X,RS,T)                             !
      ELSE IF(SQ_TYPE == 'SPA') THEN                    !
        SQ = SPA_SF(X,RS,T)                             !
      END IF                                            !
!
      END SUBROUTINE STFACT_STATIC_3D_2  
!
!=======================================================================
!
      FUNCTION GEA_SF(X,RS,T,GQ_TYPE)
!
!  This function computes generalized approximation static 
!    structure factor S(q) for 3D systems
!
!  References:  A. Gold and L. Calmels, Phys. Rev. B 48,
!                  11622-11637 (1993)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * GQ_TYPE  : local-field correction type (3D)
!       * EC_TYPE  : type of correlation energy functional
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Nov 2020
!
!
      USE REAL_NUMBERS,             ONLY : ONE,FOUR
      USE CONSTANTS_P1,             ONLY : H_BAR,M_E,E,EPS_0
      USE FERMI_SI,                 ONLY : KF_SI
      USE UTILITIES_1,              ONLY : RS_TO_N0
      USE COULOMB_K,                ONLY : COULOMB_FF
      USE  SCREENING_TYPE       
      USE SCREENING_VEC
      USE LOCAL_FIELD_STATIC
      USE STRUCTURE_FACTOR_STATIC,  ONLY : HFA_SF
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  GQ_TYPE
!
      REAL (WP), INTENT(IN) ::  X,RS,T
      REAL (WP)             ::  GEA_SF
      REAL (WP)             ::  Y
      REAL (WP)             ::  VQ,Q_SI,S0,PP,GQ
      REAL (WP)             ::  N0,KS
!
      REAL (WP)             ::  SQRT
!
      Y = X + X                                                     ! Y = q / k_F
!
      Q_SI = Y * KF_SI                                              ! q in SI
!
!  Computing the screening vector
!
      CALL SCREENING_VECTOR(SC_TYPE,'3D',X,RS,T,KS)                 !
!
!  Computing the Coulomb potential
!                          
      CALL COULOMB_FF('3D','SIU',Q_SI,KS,VQ)                        !
!
      N0 = RS_TO_N0('3D',RS)                                        !
!
!  Calling the local-field correction
!
      CALL LOCAL_FIELD_STATIC_3D(X,RS,T,GQ_TYPE,GQ)                 !
!
!  noninteracting electron gas structure factor
!
      S0 = HFA_SF(X)                                                !
!
!  plasmon contribution
!
      PP = H_BAR * Q_SI / SQRT(FOUR * M_E * N0 * VQ *(ONE - GQ))    ! ref. (1) eq. (5)
!
      GEA_SF = ONE / SQRT(ONE / (S0 * S0) + ONE / (PP * PP))        ! ref. (1) eq. (4)
!
      END FUNCTION GEA_SF  
!
!=======================================================================
!
      FUNCTION ICH_SF(X,RS,T,GQ_TYPE)
!
!  This function computes Ichimaru static structure factor S(q) 
!    for 3D systems
!
!  References:  (1) S. Tanaka and S. Ichimaru, Phys. Rev. A 35, 
!                      4754 (1987)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * GQ_TYPE  : local-field correction type (3D)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 17 Sep 2020
!
!
      USE REAL_NUMBERS,       ONLY : ONE
      USE FERMI_SI,           ONLY : KF_SI
      USE SCREENING_VEC,      ONLY : DEBYE_VECTOR
      USE LOCAL_FIELD_STATIC
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  GQ_TYPE
!
      REAL (WP)             ::  X,RS,T,Y
      REAL (WP)             ::  ICH_SF
      REAL (WP)             ::  Q_SI,QD_SI,R,R2
      REAL (WP)             ::  GQ
!
      Y    = X + X                                                  ! Y = q / k_F
      Q_SI = Y * KF_SI                                              ! q   in SI
!
!  Computing the Debye momentum
!
      CALL DEBYE_VECTOR('3D',T,RS,QD_SI)                            !
!
      R  = QD_SI / Q_SI                                             !
      R2 = R * R                                                    !
!
!  Computing the local-field correction
!
      CALL LOCAL_FIELD_STATIC_3D(X,RS,T,GQ_TYPE,GQ)                 !
!
      ICH_SF = ONE / (ONE + R2 * (ONE - GQ))                        ! ref. (1) eq. (7)
!
      END FUNCTION ICH_SF  
!
!=======================================================================
!
      FUNCTION PKA_SF(X,RS,T,GQ_TYPE)
!
!  This function computes Pietiläinen-Kallio static structure factor S(q) 
!    for 3D systems
!
!  References: (1) C. Bowen, G. Sugiyama and B. J. Alder, 
!                     Phys. Rev. B 50, 14838-14848 (1994)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * GQ_TYPE  : local-field correction type (3D)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 10 Jun 2020
!
!
      USE REAL_NUMBERS,       ONLY : ONE,FOUR
      USE FERMI_AU,           ONLY : KF_AU
      USE LOCAL_FIELD_STATIC
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  GQ_TYPE
!
      REAL (WP), INTENT(IN) ::  X,RS,T
      REAL (WP)             ::  PKA_SF
      REAL (WP)             ::  Y
      REAL (WP)             ::  GQ,QR0
!
      REAL (WP)             ::  SQRT
!
      Y = X + X                                                     ! Y = q / k_F 
!
      QR0 = Y * KF_AU * RS                                          ! q * r_s * a0
!
      CALL LOCAL_FIELD_STATIC_3D(X,RS,T,GQ_TYPE,GQ)                 !
!
      PKA_SF = ONE / SQRT( ONE + 12.0E0_WP * RS * (ONE - GQ) /    & !
                           (QR0**4) )                               ! ref. (1) eq. (2.16)
!
      END FUNCTION PKA_SF  
!
!=======================================================================
!
      FUNCTION SIN_SF(X,RS,T)
!
!  This function computes Singh static structure factor S(q) 
!    for 3D systems
!
!  References: (1) H. B. Singh, Phys. Rev. B 12, 1364-1370 (1975)
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!
!  Note: we do not solve self-consistently equation (23)
!        but keep to the initial value with G3(q) calculated
!        with the Ichimaru-Utsumi I(q). In addition, g(0) 
!        is also taken as the Ichimaru value
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Nov 2020
!
!
      USE REAL_NUMBERS,             ONLY : ONE,THREE,FOUR,HALF,THIRD,FOURTH
      USE PI_ETC,                   ONLY : PI
      USE CONSTANTS_P1,             ONLY : H_BAR,M_E,E
      USE FERMI_SI,                 ONLY : KF_SI
      USE UTILITIES_1,              ONLY : ALFA
      USE GR_0,                     ONLY : GR_0_3D
      USE PLASMON_ENE_SI
      USE LOCAL_FIELD_STATIC,       ONLY : IWA4_LFC
      USE STRUCTURE_FACTOR_STATIC,  ONLY : HFA_SF
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X,RS,T
      REAL (WP)             ::  SIN_SF
      REAL (WP)             ::  Y,Q_SI,Q2
      REAL (WP)             ::  ALPHA
      REAL (WP)             ::  C,ETA,KC
      REAL (WP)             ::  OP2,OQ2
      REAL (WP)             ::  SF_0,G3Q,G0
      REAL (WP)             ::  NUM,DEN,AAA
!
      REAL (WP)             ::  SQRT,EXP
!
      Y = X + X                                                     ! Y = q / k_F
!
      Q_SI = Y * KF_SI                                              ! q   in SI
      Q2 = Q_SI * Q_SI                                              ! q^2 in SI
!
      ALPHA = ALFA('3D')                                            !
!
      C = THREE * PI / (16.0E0_WP * ALPHA * RS)                     ! ref. (1) eq. (25)
!
      KC  = HALF / SQRT(C)                                          ! ref. (1) eq. (26) 
      ETA = EXP(- KC * KC / Q2)                                     ! ref. (1) eq. (28)
      OP2 = ENE_P_SI * ENE_P_SI / (H_BAR * H_BAR)                   ! omega_p^2
      OQ2 = FOURTH * H_BAR * H_BAR * Q2 * Q2 / (M_E * M_E)          ! omega_q^2
!
!  Values of S0, G3(q) and g(0)
!
      SF_0 = HFA_SF(X)                                              !
      G3Q  = IWA4_LFC(X,RS)                                         ! ref. (1) eq. (24)
      G0   = GR_0_3D(RS,'ICHI')                                     !
!
      NUM = OP2 +  G3Q + THIRD * ETA * OP2 * (G0 - ONE)             !
      DEN = OQ2                                                     !
!
      AAA = NUM / DEN + ONE / (SF_0 * SF_0)                         ! ref. (1) eq. (11)
!
      SIN_SF = SQRT(ONE / AAA)                                      ! ref. (1) eq. (11)
!
      END FUNCTION SIN_SF
!
!=======================================================================
!
      FUNCTION SPA_SF(X,RS,T)
!
!  This function computes Singh-Pathak static structure factor S(q) 
!    for 3D systems
!
!  References: (1) H. B. Singh and K. N. Pathak, Phys. Rev. B 8, 
!                     6035-6937 (1973)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Nov 2020
!
!
      USE REAL_NUMBERS,             ONLY : ONE,THIRD
      USE PI_ETC,                   ONLY : PI_INV
      USE UTILITIES_1,              ONLY : ALFA
      USE LOCAL_FIELD_STATIC,       ONLY : IWA4_LFC
      USE STRUCTURE_FACTOR_STATIC,  ONLY : HFA_SF
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  GQ_TYPE
!
      REAL (WP), INTENT(IN) ::  X,RS,T
      REAL (WP)             ::  SPA_SF
      REAL (WP)             ::  Y,Y2,Y4
      REAL (WP)             ::  G3Q,S0,C,ALPHA
      REAL (WP)             ::  NUM,DEN
!
      REAL (WP)             ::  SQRT
!
      Y  = X + X                                                    ! Y = q / k_F 
      Y2 = Y * Y                                                    ! 
      Y4 = Y2 * Y2                                                  !
!
      ALPHA = ALFA('3D')                                            !
!
      C = ONE / (16.0E0_WP * THIRD * PI_INV * ALPHA * RS)           ! ref. (1) eq. (8)
!
      G3Q = IWA4_LFC(X,RS)                                          !
!
      S0 = HFA_SF(X)                                                !
!
      NUM = C * Y4                                                  !
      DEN = ONE + G3Q + NUM / (S0 * S0)                             !
!
      SPA_SF = SQRT(NUM / DEN)                                      ! ref. (1) eq. (7)
!
      END FUNCTION SPA_SF  
!
END MODULE STRUCTURE_FACTOR_STATIC_2
 
