!
!=======================================================================
!
MODULE STRUCTURE_FACTOR_DYNAMIC 
!
      USE ACCURACY_REAL
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE STFACT_DYNAMIC(X,Z,RS,T,SQO_TYPE,SQ_TYPE,SQ)
!
!  This subroutine computes a dynamical structure factor S(q,omega) 
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : dimensionless factor   --> Z = omega / omega()_q 
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * SQO_TYPE : structure factor approximation (3D)
!                       SQO_TYPE  = 'ABA' Arista-Brandt approximation
!                       SQO_TYPE  = 'HFA' Hartree-Fock approximation
!                       SQO_TYPE  = 'HYD' hyrodynamic approximation
!                       SQO_TYPE  = 'IGA' ideal gas approximation
!                       SQO_TYPE  = 'ITA' Ichimaru-Tanaka approximation
!                       SQO_TYPE  = 'MFA' Hansen-McDonald-Pollock approximation
!                       SQO_TYPE  = 'MFD' memory function model
!                       SQO_TYPE  = 'NAI' Nakano-Ichimaru approximation
!                       SQO_TYPE  = 'NIC' Nakano-Ichimaru approximation
!                       SQO_TYPE  = 'VLA' linearized Vlasov approximation
!                       SQO_TYPE  = 'UTI' Utsumi-Ichimaru approximation
!       * SQ_TYPE  : static structure factor approximation (3D)
!
!
!  Output parameters:
!
!       * SQ       : structure factor
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 19 Jun 2020
!
      USE MATERIAL_PROP,    ONLY : DMN
!
      IMPLICIT NONE
!                                   
      CHARACTER (LEN = 3)   ::  SQO_TYPE,SQ_TYPE
!
      REAL (WP), INTENT(IN) ::  X,Z,RS,T
      REAL (WP), INTENT(OUT)::  SQ
!                                   
      IF(DMN == '3D') THEN                                          !
        CALL STFACT_DYNAMIC_3D(X,Z,RS,T,SQO_TYPE,SQ_TYPE,SQ)        !
      ELSE IF(DMN == '2D') THEN                                     !
        CONTINUE                                                    !
      ELSE IF(DMN == '1D') THEN                                     !
        CONTINUE                                                    !
      END IF                                                        !
!
      END SUBROUTINE STFACT_DYNAMIC
!
!=======================================================================
!
      SUBROUTINE STFACT_DYNAMIC_3D(X,Z,RS,T,SQO_TYPE,SQ_TYPE,SQ) 
!
!  This subroutine computes a dynamical structure factor S(q,omega) 
!    for 3D systems
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : dimensionless factor   --> Z = omega / omega_q 
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * SQO_TYPE : structure factor approximation (3D)
!                       SQO_TYPE  = 'ABA' Arista-Brandt approximation
!                       SQO_TYPE  = 'HFA' Hartree-Fock approximation
!                       SQO_TYPE  = 'HYD' hyrodynamic approximation
!                       SQO_TYPE  = 'IGA' ideal gas approximation
!                       SQO_TYPE  = 'ITA' Ichimaru-Tanaka approximation
!                       SQO_TYPE  = 'MFA' Hansen-McDonald-Pollock approximation
!                       SQO_TYPE  = 'MFD' memory function model
!                       SQO_TYPE  = 'NAI' Nakano-Ichimaru approximation
!                       SQO_TYPE  = 'NIC' Nakano-Ichimaru approximation
!                       SQO_TYPE  = 'VLA' linearized Vlasov approximation
!                       SQO_TYPE  = 'UTI' Utsumi-Ichimaru approximation
!       * SQ_TYPE  : static structure factor approximation (3D)
!
!  Intermediate parameters:
!
!       * GQ_TYPE  : local-field correction type (3D)
!       * EC_TYPE  : type of correlation energy functional
!       * IQ_TYPE  : type of approximation for I(q)
!
!
!  Output parameters:
!
!       * SQ       : structure factor
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 30 Nov 2020
!
!
!
      USE LF_VALUES,       ONLY : GQ_TYPE,IQ_TYPE
      USE ENERGIES,        ONLY : EC_TYPE
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  SQO_TYPE,SQ_TYPE
!
      REAL (WP), INTENT(IN) ::  X,Z,RS,T
      REAL (WP), INTENT(OUT)::  SQ
!
      IF(SQO_TYPE == 'ABA') THEN                                    !
        SQ = ABA_SF(X,Z,RS,T)                                       !
      ELSE IF(SQO_TYPE == 'HFA') THEN                               !
        SQ = HFA_SF(X,Z)                                            !
      ELSE IF(SQO_TYPE == 'HYD') THEN                               !
        SQ = HYD_SF(X,Z,RS,T,GQ_TYPE,EC_TYPE,SQ_TYPE,IQ_TYPE)       !
      ELSE IF(SQO_TYPE == 'IGA') THEN                               !
        SQ = IGA_SF(X,Z,T)                                          !
      ELSE IF(SQO_TYPE == 'ITA') THEN                               !
        SQ = ITA_SF(X,Z,RS,T,SQ_TYPE,GQ_TYPE,IQ_TYPE)               !
      ELSE IF(SQO_TYPE == 'MFA') THEN                               !
        SQ = MFA_SF(X,Z,T)                                          !
      ELSE IF(SQO_TYPE == 'MFD') THEN                               !
        SQ = MFD_SF(X,Z,RS,T,SQ_TYPE,GQ_TYPE)                       !
      ELSE IF(SQO_TYPE == 'NIC') THEN                               !
        SQ = NIC_SF(X,Z,T)                                          !
      ELSE IF(SQO_TYPE == 'UTI') THEN                               !
        SQ = UTI_SF(X,Z,T,RS,SQ_TYPE,GQ_TYPE,EC_TYPE,IQ_TYPE)       !
      ELSE IF(SQO_TYPE == 'VLA') THEN                               !    
        SQ = VLA_SF(X,Z,RS,T,GQ_TYPE,SQ_TYPE)                       !
      END IF                                                        !
!
      END SUBROUTINE STFACT_DYNAMIC_3D  
!
!=======================================================================
!
      FUNCTION ABA_SF(X,Z,RS,T)
!
!  This function computes the Arista-Brandt approximation for
!    the dynamical structure factor S(q,omega) for 3D systems
!
!  References: (1) N. R. Arista and W. Brandt, Phys. Rev. A 29,
!                     1471-1480 (1984)
!
!
!  Input parameters: 
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : omega / omega_q        --> dimensionless
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Oct 2020
!
!
      USE REAL_NUMBERS,       ONLY : ZERO,ONE,TWO,EIGHTH
      USE CONSTANTS_P1,       ONLY : H_BAR,M_E,E,K_B
      USE FERMI_SI,           ONLY : EF_SI,KF_SI,VF_SI
      USE PI_ETC,             ONLY : PI
      USE UTILITIES_1,        ONLY : RS_TO_N0
      USE SCREENING_VEC
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X,Z,RS,T
      REAL (WP)             ::  ABA_SF
      REAL (WP)             ::  Y,U
      REAL (WP)             ::  Q_SI,OM
      REAL (WP)             ::  KBT,THETA
      REAL (WP)             ::  C1,C2,C3
      REAL (WP)             ::  NUM,DEN
      REAL (WP)             ::  K_TF_SI,KD_SI
      REAL (WP)             ::  N0
      REAL (WP)             ::  M2E2,HB3,KO
!
      REAL (WP)             ::  EXP
!  
      Y  = X + X                                                    ! Y = q / k_F 
      U  = X * Z                                                    ! U = omega / (q v_F)
!
      Q_SI = Y * KF_SI                                              ! q in SI
!  
      OM = Q_SI * VF_SI * U                                         ! omega in SI
!
!  Computing the number density
!
      N0 = RS_TO_N0('3D',RS)                                        !
!
      KBT   = K_B * T                                               ! 
      THETA = KBT / EF_SI                                           ! 1 / degeneracy
!
      M2E2 = M_E * M_E * E * E                                      ! m^2 e^2
      HB3  = H_BAR * H_BAR * H_BAR                                  ! h_bar^3
      KO   = Q_SI * OM                                              ! q omega
!
!  Computation of the Thomas-Fermi screening vector
!
      CALL THOMAS_FERMI_VECTOR('3D',K_TF_SI)                        !
!
!  Computation of the Debye screening vector
!      
      CALL DEBYE_VECTOR('3D',T,RS,KD_SI)                            !
!
!  Computation of S(q,omega)
!
      C1 = M2E2 * KO                                                !
!
      IF(THETA < ONE) THEN                                          ! case (a)
!
        C2 = (Q_SI * Q_SI + K_TF_SI * K_TF_SI)**2                   !
!
        IF(X < ONE) THEN                                            !
!
          ABA_SF = TWO * C1 / (HB3 * C2)                            ! ref. (1) eq. (33)
!
        ELSE                                                        !
!
          ABA_SF = ZERO                                             !
!
        END IF                                                      !
!
      ELSE                                                          !
!
        C2 = (Q_SI * Q_SI + KD_SI * KD_SI)**2                       !
        C3 = ONE / (M_E * KBT)                                      !
!
        ABA_SF = N0 * C1 / C2 * (TWO * PI * C3)**1.5E0_WP *       & !
                 EXP(- EIGHTH * H_BAR* H_BAR * Q_SI * Q_SI * C3)    !
!
      END IF                                                        !
!
      END FUNCTION ABA_SF  
!
!=======================================================================
!
      FUNCTION HFA_SF(X,Z)
!
!  This function computes the Hartree-Fock approximation for
!    the dynamical structure factor S(q,omega) for 3D systems
!
!  References: (1) J. S\'{o}lyom, "Fundamentals of the Physics 
!                     of Solids" Vol. III (Springer, 2010) 
!
!  Input parameters:  
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : omega / omega_q        --> dimensionless
!
!  Intermediate parameters:  
!
!       * U        : dimensionless factor   --> U = omega / (q * v_F)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 30 Nov 2020
!
!
      USE REAL_NUMBERS,       ONLY : ZERO,ONE,THREE,HALF
      USE PI_ETC,             ONLY : PI
      USE CONSTANTS_P1,       ONLY : H_BAR
      USE FERMI_SI,           ONLY : EF_SI,KF_SI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X,Z 
      REAL (WP)             ::  HFA_SF
      REAL (WP)             ::  Y,U
      REAL (WP)             ::  COEF
      REAL (WP)             ::  BND1,BND2,BND3
!
      Y  = X  + X                                                   ! Y = q / k_F 
!
      U  = X * Z                                                    ! omega / (q * v_F)
!
      COEF = THREE * PI * HALF * H_BAR / EF_SI                      !
!
!  Various bounds
!
      BND1 =  ONE - X                                               !
      BND2 =  ONE + X                                               !
      BND3 =  X - ONE                                               !
!
      IF(X < ONE) THEN                                              ! ref. (1) eq. (28.4.102)
!
        IF( U < BND1 ) THEN                                         !
          HFA_SF = COEF * U                                         !
        ELSE IF( BND1 <= U .AND. U <= BND2 ) THEN                   !
          HFA_SF = COEF * ( ONE - (U - X)**2 ) / X                  ! 
        ELSE                                                        !
          HFA_SF = ZERO                                             !
        END IF                                                      !
! 
      ELSE                                                          ! ref. (1) eq. (28.4.103) 
! 
        IF( U < BND3 ) THEN                                         !
          HFA_SF = ZERO                                             !
        ELSE IF( BND3 <= U .AND. U <= BND2 ) THEN                   ! 
          HFA_SF = COEF * ( ONE - (U - X)**2 ) / X                  ! 
        ELSE                                                        !
          HFA_SF = ZERO                                             !
        END IF                                                      !
!
      END IF                                                        !
!
      END FUNCTION HFA_SF
!
!=======================================================================
!
      FUNCTION HYD_SF(X,Z,RS,T,GQ_TYPE,EC_TYPE,SQ_TYPE,IQ_TYPE)
!
!  This function computes the hydrodynamic approximation for
!    the dynamical structure factor S(q,omega) for 3D systems
!
!  References: (1) S. Tanaka and S. Ichimaru, Phys. Rev. A 35, 
!                  4743-4754 (1987)
!
!  Input parameters: 
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : omega / omega_q        --> dimensionless
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * GQ_TYPE  : local-field correction type (3D)
!       * EC_TYPE  : type of correlation energy functional
!       * SQ_TYPE  : static structure factor approximation (3D)
!       * IQ_TYPE  : type of approximation for I(q)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Oct 2020
!
!
!
      USE REAL_NUMBERS,           ONLY : ONE,TWO,HALF
      USE CONSTANTS_P1,           ONLY : H_BAR,M_E
      USE FERMI_SI,               ONLY : KF_SI,VF_SI
      USE PI_ETC,                 ONLY : PI_INV
      USE SCREENING_VEC,          ONLY : DEBYE_VECTOR
      USE IQ_FUNCTIONS_1
      USE LOCAL_FIELD_STATIC
      USE RELAXATION_TIME_STATIC, ONLY : TAIQ_RT_3D
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 6)   ::  EC_TYPE
      CHARACTER (LEN = 4)   ::  GQ_TYPE
      CHARACTER (LEN = 3)   ::  SQ_TYPE,IQ_TYPE
!
      REAL (WP), INTENT(IN) ::  X,Z,RS,T
      REAL (WP)             ::  HYD_SF
      REAL (WP)             ::  Y,U
      REAL (WP)             ::  Q_SI,KD_SI,OM
      REAL (WP)             ::  TAU_K,GQ,IQ
      REAL (WP)             ::  NUM,DEN
!  
      Y  = X + X                                                    ! Y = q / k_F 
      U  = X * Z                                                    ! U = omega / (q v_F)
!
      Q_SI = Y * KF_SI                                              ! q in SI
!  
      OM = Q_SI * VF_SI * U                                         ! omega in SI
!
!  Computing the Debye momentum
!
      CALL DEBYE_VECTOR('3D',T,RS,KD_SI)                            !
!
!  Computing the local-field correction
!
      CALL LOCAL_FIELD_STATIC_3D(X,RS,T,GQ_TYPE,GQ)                 !
      CALL IQ_3D(X,RS,IQ_TYPE,IQ)                                   !
!
!  Computing the q-dependent relaxation time TAU_K
!
      TAU_K = TAIQ_RT_3D(X,RS,T)                                    !
      print *, TAU_K, Q_SI/KD_SI
!
      NUM = PI_INV * Q_SI * Q_SI * TAU_K * (GQ - IQ)                !
      DEN = KD_SI * KD_SI * (ONE + OM * OM * TAU_K * TAU_K)         !
!
      HYD_SF = NUM / DEN                                            ! ref. (1) eq. (23)
!
      END FUNCTION HYD_SF  
!
!=======================================================================
!
      FUNCTION IGA_SF(X,Z,T)
!
!  This function computes the ideal gas approximation for
!    the dynamical structure factor S(q,omega) for 3D systems
!
!  References:  
!
!  Input parameters: (1) J. P. Mithen, J. Daligault and G. Gregori,
!                           Phys. Rev. E 85, 056407 (2012)
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : omega / omega_q        --> dimensionless
!       * T        : temperature in SI
!
!
!  Note: As IGA_SF will be scaled by EF_SI / H_BAR for plotting, 
!        we check that IGA_SF * EF_SI / H_BAR can be represented
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 22 Dec 2020
!
!
      USE REAL_NUMBERS,       ONLY : ZERO,ONE,FOUR,HALF,TTINY,MIC
      USE CONSTANTS_P1,       ONLY : H_BAR,M_E,K_B
      USE FERMI_SI,           ONLY : KF_SI,VF_SI
      USE PI_ETC,             ONLY : PI_INV
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X,Z,T
!
      REAL (WP)             ::  IGA_SF
!
      REAL (WP)             ::  Y,U
      REAL (WP)             ::  OM,OMQ,OMT
      REAL (WP)             ::  LOGA,LOGB
      REAL (WP)             ::  Q_SI,COEF 
!
      REAL (WP)             ::  LOG,EXP 
!
      Y  = X + X                                                    ! Y = q / k_F 
      U  = X * Z                                                    ! U = omega / (q v_F)
!
      Q_SI = Y * KF_SI                                              ! q in SI
!  
      OM  = Q_SI * VF_SI * U                                        ! omega in SI
      OMT = K_B * T / H_BAR                                         ! omega_T
      OMQ = HALF * H_BAR * Q_SI * Q_SI / M_E                        ! omega_q
!
      COEF = ONE / (FOUR * OMT * OMQ)                               ! 1 / (4 omega_T omega_q)
!
      LOGA = HALF * LOG(COEF * PI_INV) - COEF * OM * OM             ! Log[S(q,omega)]
!
!
      IF(LOGA > MIC) THEN                                           !  
        IGA_SF = EXP(LOGA)                                          ! ref. (1) eq. (9)
      ELSE                                                          !
        IGA_SF = ZERO                                               !
      END IF                                                        !
!
      END FUNCTION IGA_SF  
!
!=======================================================================
!
      FUNCTION ITA_SF(X,Z,RS,T,SQ_TYPE,GQ_TYPE,IQ_TYPE)
!
!  This function computes the Ichimaru-Tanaka approximation for
!    the dynamical structure factors S(q,omega) for 3D systems
!
!  References:  
!
!  Input parameters: (1) S. Ichimaru and S. Tanaka, Phys. Rev. Lett. 56,
!                           2815-2818 (1986)
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : omega / omega_q        --> dimensionless
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * SQ_TYPE  : static structure factor approximation (3D)
!       * GQ_TYPE  : local-field correction type (3D)
!       * IQ_TYPE  : type of approximation for I(q)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 22 Oct 2020
!
!
      USE REAL_NUMBERS,           ONLY : ONE,HALF
      USE CONSTANTS_P1,           ONLY : BOHR,H_BAR,M_E,K_B
      USE FERMI_SI,               ONLY : KF_SI,VF_SI
      USE PI_ETC,                 ONLY : PI,PI_INV
      USE SCREENING_VEC,          ONLY : DEBYE_VECTOR
      USE UTILITIES_1,            ONLY : RS_TO_N0
      USE VISCOSITY,              ONLY : LHPO_VISC_3D
      USE RELAXATION_TIME_STATIC, ONLY : TAI0_RT_3D
      USE IQ_FUNCTIONS_1
      USE LOCAL_FIELD_STATIC
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 6)   ::  EC_TYPE
      CHARACTER (LEN = 4)   ::  GQ_TYPE
      CHARACTER (LEN = 3)   ::  SQ_TYPE,IQ_TYPE
!
      REAL (WP), INTENT(IN) ::  X,Z,RS,T
      REAL (WP)             ::  ITA_SF
      REAL (WP)             ::  Y,U
      REAL (WP)             ::  OM,Q_SI
      REAL (WP)             ::  GQ,IQ
      REAL (WP)             ::  A,ETA,XI,TAU_M,TAU_Q,ETA_L
      REAL (WP)             ::  KD_SI,R,R2,R4
      REAL (WP)             ::  N0
      REAL (WP)             ::  NUM,DEN
!
      REAL (WP)             ::  EXP
!
      XI = 2.7E0_WP                                                 !
!
      Y  = X + X                                                    ! Y = q / k_F 
      U  = X * Z                                                    ! U = omega / (q v_F)
!
      Q_SI = Y * KF_SI                                              ! q in SI
!
      OM   = Q_SI * VF_SI * U                                       ! omega in SI
      A    = RS * BOHR                                              ! r_s in SI
!
!  Computing the viscosity (LHPO case only)
!
      ETA = LHPO_VISC_3D(RS,T)                                      !
!
!  Computing the static relaxation time
!
      TAU_M = TAI0_RT_3D(RS,T)                                      !
!
      TAU_Q = TAU_M * EXP(- (A * Q_SI / XI)**2)                     !
!
      N0 = RS_TO_N0('3D',RS)                                        !
!
!  Computing the Debye momentum
!
      CALL DEBYE_VECTOR('3D',T,RS,KD_SI)                            !
!
      R  = Q_SI / KD_SI                                             ! 
      R2 = R * R                                                    ! 
      R4 = R2 * R2                                                  !
!
!  Computing the static functions G(q) and I(q)
!
      CALL LOCAL_FIELD_STATIC_3D(X,RS,T,GQ_TYPE,GQ)                 !
      CALL IQ_3D(X,RS,IQ_TYPE,IQ)                                   !
!
!  Computing the dynamical viscosity
!
      ETA_L = TAU_Q * N0 * K_B * T * (GQ - IQ) / R2                 ! ref. (1) eq. (8)
!
      NUM = R4 * ETA_L                                              !
      DEN = PI * K_B * T * ( ONE + (TAU_Q * OM)**2)                 !
!
      ITA_SF = NUM / DEN                                            ! ref. (1) eq. (6)
!
      END FUNCTION ITA_SF  
!
!=======================================================================
!
      FUNCTION MFA_SF(X,Z,T)
!
!  This function computes the Hansen-McDonald-Pollock  approximation for
!    the dynamical structure factor S(q,omega) for 3D systems
!
!  References: (1) J. P. Hansen, I. R. McDonald and E. L. Pollock,
!                     Phys. Rev. A 11, 1025-1039 (1975)
!
!  Input parameters: 
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : omega / omega_q        --> dimensionless
!       * T        : temperature in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Oct 2020
!
!
      USE REAL_NUMBERS,       ONLY : ONE,TWO,THREE,HALF
      USE CONSTANTS_P1,       ONLY : H_BAR,M_E,K_B
      USE FERMI_SI,           ONLY : KF_SI,VF_SI
      USE PI_ETC,             ONLY : PI,PI_INV
      USE EXT_FUNCTIONS,      ONLY : DAWSON                         ! Dawson function D(x)
      USE PLASMON_SCALE_P,    ONLY : DEGEN
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X,Z,T
      REAL (WP)             ::  MFA_SF
      REAL (WP)             ::  Y,U
      REAL (WP)             ::  OQ,Q_SI,GAM,COEF,S0
      REAL (WP)             ::  VQ,ZZ,PHI,OM,OMP
      REAL (WP)             ::  NUM,DEN
!
      REAL (WP)             ::  EXP,SQRT
!
      Y  = X + X                                                    ! Y = q / k_F 
      U  = X * Z                                                    ! U = omega / (q v_F)
!
      Q_SI = Y * KF_SI                                              ! q in SI
      OM   = Q_SI * VF_SI * U                                       ! omega in SI
      OMP  = H_BAR * OM / ENE_P_SI                                  ! omega in unit of omega_p
!
!
      GAM  = DEGEN                                                  ! Gamma
      COEF = THREE * HALF * GAM * PI_INV / (Q_SI *  Q_SI)           ! 3 Gamma / 2 pi q^2 (dimensionless)
      S0   = SQRT(COEF) * EXP(- COEF * PI * OMP * OMP)              ! IGA approximation
!
      VQ    = TWO * PI * COEF                                       !
      ZZ    = SQRT(PI * COEF) * OMP                                 !
      PHI   = - ( ONE - TWO * ZZ * DAWSON(ZZ) )                     !
!
      NUM = S0                                                      !
      DEN = (ONE - VQ * PHI)**2 + (OMP * PI * VQ * S0)**2           !
!
      MFA_SF = NUM / DEN                                            ! ref. (1) eq. (4.2)
!
      END FUNCTION MFA_SF  
!
!=======================================================================
!
      FUNCTION MFD_SF(X,Z,RS,T,SQ_TYPE,GQ_TYPE)
!
!  This function computes the memory function model for
!    the dynamical structure factor S(q,omega) for 3D systems
!
!  References: (1) J. P. Hansen, I. R. McDonald and E. L. Pollock,
!                     Phys. Rev. A 11, 1025-1039 (1975)
!
!  Input parameters: 
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : omega / omega_q        --> dimensionless
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * SQ_TYPE  : static structure factor approximation (3D)
!       * GQ_TYPE  : local-field correction type (3D)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Oct 2020
!
!
      USE DIMENSION_CODE,          ONLY : NSIZE
!
      USE REAL_NUMBERS,            ONLY : ONE,TWO,THREE,HALF
      USE CONSTANTS_P1,            ONLY : BOHR,H_BAR,M_E,K_B
      USE FERMI_SI,                ONLY : KF_SI,VF_SI
      USE PI_ETC,                  ONLY : PI,PI_INV
!
      USE PLASMON_SCALE_P,         ONLY : DEGEN
      USE PC_VALUES,               ONLY : GR_TYPE
      USE PD_VALUES
!
      USE EXT_FUNCTIONS,           ONLY : DAWSON          ! Dawson function D(x)
      USE RELAXATION_TIME_STATIC,  ONLY : TAIQ_RT_3D
      USE PLASMON_ENE_SI
      USE STRUCTURE_FACTOR_STATIC
      USE SPECIFIC_INT_3
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  GQ_TYPE
      CHARACTER (LEN = 3)   ::  SQ_TYPE
!
      REAL (WP), INTENT(IN) ::  X,Z,RS,T
      REAL (WP)             ::  MFD_SF
      REAL (WP)             ::  Y,U
      REAL (WP)             ::  Q_SI,OM,OMP
      REAL (WP)             ::  GAM,SQ
      REAL (WP)             ::  KK,IN,IQ
      REAL (WP)             ::  O2K,O2L,O2,O4
      REAL (WP)             ::  TAU_K,TKO
      REAL (WP)             ::  K2PP,K2PS
      REAL (WP)             ::  NUM,DEN
!
      REAL (WP), PARAMETER  ::  X_MAX = 4.0E0_WP
!
      REAL (WP)             ::  EXP,SQRT
!
      Y  = X + X                                                    ! Y = q / k_F 
      U  = X * Z                                                    ! U = omega / (q v_F)
!
      Q_SI = Y * KF_SI                                              ! q in SI
      OM   = Q_SI * VF_SI * U                                       ! omega in SI
      OMP  = H_BAR * OM / ENE_P_SI                                  ! omega in unit of omega_p
!
      GAM = DEGEN                                                   !
!
      KK = Q_SI * BOHR                                              ! q in units of 1 / a_0
!
!  Computing the static structure factor
!
      CALL STFACT_STATIC_3D(X,RS,T,SQ_TYPE,GQ_TYPE,SQ)              !
!
!  Computing the I(q) factor
!
      CALL INT_GRM1(NSIZE,X_MAX,6,RS,T,KK,0,GR_TYPE,RH_TYPE,IN)     !
      IQ = - IN                                                     !
!
!  Computing the < omega_k^2> and < omega_L^2> factors (ref. 1 appendix)
!
      O2K = K_B * T * Q_SI / (M_E * SQ)                             ! < omega_k^2>
      O2L = KK * KK / GAM + ONE - TWO * IQ                          ! < omega_L^2>
!
!  Computing the q-dependent relaxation time TAU_K
!
      TAU_K = TAIQ_RT_3D(X,RS,T)                                    !
!
      TKO = TAU_K * OM * SQRT(PI_INV)                               !
!
!  Computing the k^2 phi' and k^2 phi" coeffcients
!
      K2PP = TAU_K * (O2L - O2K) * EXP(- TKO * TKO)                 ! ref. (1) eq. (7)
      K2PS = TWO * SQRT(PI_INV) * TAU_K * (O2L - O2K) * DAWSON(TKO) ! ref. (1) eq. (8)
!
      NUM = SQ * PI_INV * O2K * K2PP                                !
      DEN = (OMP * OMP - O2K - OMP * K2PS)**2 + (OMP * K2PP)**2     !
!
      MFD_SF = NUM / DEN                                            ! ref. (1) eq. (4) 
!
      END FUNCTION MFD_SF  
!
!=======================================================================
!
      FUNCTION NIC_SF(X,Z,T)
!
!  This function computes the Nakano-Ichimaru approximation for
!    the dynamical structure factor S(q,omega) for 3D systems
!
!  References:  
!
!  Input parameters: (1) A. Nakano and S. Ichimaru, Phys. Rev. B 39, 
!                           4938-4944 (1989)
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : omega / omega_q        --> dimensionless
!       * T        : temperature in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Dec 2020
!
!
      USE MATERIAL_PROP,      ONLY : RS
      USE REAL_NUMBERS,       ONLY : ONE,TWO,THREE,FOUR,FIVE,EIGHT,  &
                                     HALF,THIRD,FOURTH,TTINY
      USE PI_ETC,             ONLY : PI,PI_INV
      USE UTILITIES_1,        ONLY : ALFA
      USE GAMMA_ASYMPT,       ONLY : GAMMA_0_3D
      USE MINMAX_VALUES
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X,Z,T
      REAL (WP)             ::  NIC_SF
!
      REAL (WP)             ::  Y,Y2,Y4,Y6
      REAL (WP)             ::  R1,R2,R3,R4,R5,R6,R7,R8,R9,R10
      REAL (WP)             ::  A(0:5),B(0:4),C(0:10),D(0:5)
      REAL (WP)             ::  E(0:5),F(0:5),G(0:5),H(0:10),I(0:5)
      REAL (WP)             ::  K1,K2,K3,K4,KS,AL,OM,CM,G0
      REAL (WP)             ::  FPK,FMK,FSK,OPK,GPK,OMK
      REAL (WP)             ::  ALPHA,OMG,KTF,OMP
      REAL (WP)             ::  PDC,GAM_0,KKP
      REAL (WP)             ::  NUM1,DEN1,NUM2,DEN2
      REAL (WP)             ::  MAX_EXP,MIN_EXP
      REAL (WP)             ::  EXPO,EXP22
      REAL (WP)             ::  SPK,SSK,SMK
!
      REAL (WP)             ::  SQRT,EXP
!
      DATA A   / 3.6589E0_WP  ,  0.41154E0_WP  , - 0.13214E0_WP    , &  ! k1 polynomial 
                 0.020511E0_WP,- 0.0016079E0_WP,   4.9763E-05_WP     /  ! coefficients
!
      DATA B   / 0.30078E0_WP ,  0.47133E0_WP  , - 0.068996E0_WP   , &  ! k2 polynomial
                 0.0064815E0_WP,- 0.00023811E0_WP                    /  ! coefficients
!
      DATA C   /-1.6111E0_WP  , 14.373E0_WP      , - 24.339E0_WP    , &  ! k3 polynomial
                19.883E0_WP   ,- 9.2979E0_WP     ,    2.7058E0_WP   , &  ! coefficients
                -0.50785E0_WP ,  0.061592E0_WP   ,  - 0.0046652E0_WP, &  ! 
                 0.00020069E0_WP,- 3.7434E-06_WP                      /  !
!
      DATA D   / 0.73153E0_WP ,  0.59597E0_WP    ,  - 0.15927E0_WP  , &  ! k4 polynomial 
                 0.020467E0_WP, -0.0013217E0_WP,    3.44945E-05_WP    /  ! coefficients
!  
      DATA E   / 1.6346E0_WP  ,  0.48593E0_WP    ,  - 0.14513E0_WP  , &  ! ks polynomial 
                 0.021578E0_WP, -0.0016023E0_WP  ,    4.6925E-05_WP   /  ! coefficients
!
      DATA F   / 1.2013E0_WP  , - 0.85002E0_WP   ,    0.30359E0_WP  , &  ! alpha polynomial 
                -0.054964E0_WP,   0.0047306E0_WP , - 0.00015434E0_WP  /  ! coefficients
!
      DATA G   / 0.41083E0_WP ,   0.84227E0_WP   ,  - 0.17643E0_WP  , &  ! omega_m polynomial
                 0.027536E0_WP, - 0.00222936E0_WP,  7.083E-05_WP      /  ! coefficients
!
      DATA H   / 2.2149E0_WP  , - 5.6161E0_WP    ,    9.2856E0_WP   , &  ! c_m polynomial
                -7.6535E0_WP  ,   3.624E0_WP     ,  - 1.0662E0_WP   , &  ! coefficients
                 0.20187E0_WP , - 0.02465E0_WP   ,    0.001877E0_WP , &  !
                -8.1083E-05_WP,   1.5173E-06_WP                       /  !
!
      DATA I   / 0.48603E0_WP , - 0.28313E0_WP   ,    0.074042E0_WP , &  ! g0 polynomial
                -0.010109E0_WP,   0.0006944E0_WP , - 1.8894E-05_WP    /  ! coefficients  
!
!  Computing the max and min value of the exponent of e^x
!
      CALL MINMAX_EXP(MAX_EXP,MIN_EXP)                              !
!
      Y  = X  + X                                                   ! Y = q / k_F 
      Y2 = Y  * Y                                                   !
      Y4 = Y2 * Y2                                                  !
      Y6 = Y4 * Y2                                                  !
!
      ALPHA = ALFA('3D')                                            !
!
!  Powers of RS
!
      R1 = RS                                                       !
      R2 = R1 * R1                                                  !
      R3 = R2 * R1                                                  !
      R4 = R3 * R1                                                  !
      R5 = R4 * R1                                                  !
      R6 = R5 * R1                                                  !
      R7 = R6 * R1                                                  !
      R8 = R7 * R1                                                  !
      R9 = R8 * R1                                                  !
      R10= R9 * R1                                                  !
!
!  Thomas-Fermi screening vector and plasmon energy
!
      KTF = SQRT(FOUR * ALPHA * RS * PI_INV)                        ! k_TF / k_F
      OMP = SQRT(16.0E0_WP * ALPHA * RS * PI_INV * THIRD)           ! h_bar omega_p / E_F
!
      OMG = FOUR * Z * X * X                                        ! omega / omega_F
!
      PDC = THREE / (FIVE * OMP) - FOURTH * OMP * GAMMA_0_3D(RS,T)  !
!
!  Fitting parameters as a function of RS
!
      K1 = A(0) + A(1) * R1 + A(2) * R2 + A(3) * R3 +             & !
                  A(4) * R4 + A(5) * R5                             !
      K2 = B(0) + B(1) * R1 + B(2) * R2 + B(3) * R3 +             & !
                  B(4) * R4                                         !
      K3 = C(0) + C(1) * R1 + C(2) * R2 + C(3) * R3 +             & !
                  C(4) * R4 + C(5) * R5 + C(6) * R6 +             & !
                  C(7) * R7 + C(8) * R8 + C(9) * R9 +             & !
                  C(10) * R10                                       !
      K4 = D(0) + D(1) * R1 + D(2) * R2 + D(3) * R3 +             & !
                  D(4) * R4 + D(5) * R5                             !
      KS = E(0) + E(1) * R1 + E(2) * R2 + E(3) * R3 +             & !
                  E(4) * R4 + E(5) * R5                             !
      AL = F(0) + F(1) * R1 + F(2) * R2 + F(3) * R3 +             & !
                  F(4) * R4 + F(5) * R5                             !
      OM = G(0) + G(1) * R1 + G(2) * R2 + G(3) * R3 +             & !
                  G(4) * R4 + G(5) * R5                             !
      CM = H(0) + H(1) * R1 + H(2) * R2 + H(3) * R3 +             & !
                  H(4) * R4 + H(5) * R5 + H(6) * R6 +             & !
                  H(7) * R7 + H(8) * R8 + H(9) * R9 +             & !
                  H(10) * R10                                       !
      G0 = I(0) + I(1) * R1 + I(2) * R2 + I(3) * R3 +             & !
                  I(4) * R4 + I(5) * R5                             !
!
      KKP = KTF * KTF * K3 * K3                                     !
!
      FPK = ( ONE - ( ONE - HALF * ( EXP(- Y2 / (K1 * K1)) +      & !
                                     EXP(- Y4 / K2**4)            & !
                                   )                              & !
                    ) *                                           & !! ref. (1) eq. (5)
              KKP  / (Y4 + KKP)                                   & !
            ) * EXP( - Y6 / K4**6)                                  !
                    
      FMK = (ONE - EXP(- Y2 / (K1 * K1))) * KKP / (TWO * (Y4 + KKP))! ref. (1) eq. (6)
      FSK = ONE - FPK - FMK                                         ! ref. (1) eq. (7) 
      OPK = OMP + TWO * PDC * Y2                                    ! ref. (1) eq. (8)
      GPK = ( SQRT(PI) * OMP**6 * Y2 * THIRD /                    & ! ref. (1) eq. (9)
              (OM**5 * K1 * K1) ) * EXP(- OMP * OMP / (OM * OM))    !
      OMK = OM + CM * Y2                                            ! ref. (1) eq. (10)
      goto 10
!
!  Plasmon contribution to S(q,omega)
!
      NUM1 = FPK * Y2                                               !
      DEN1 = SQRT(PI) * OPK * GPK                                   !
      NUM2 = (OMG - OPK)**2                                         !
      DEN2 = GPK * GPK                                              !
!
      EXPO = - NUM2 / DEN2                                          !
!
      IF(EXPO > MIN_EXP) THEN                                       !
        EXP22 = EXP(EXPO)                                           !
      ELSE                                                          !
        EXP22 = TTINY                                               !
      END IF                                                        !
!
      SPK = NUM1 * EXP22 / DEN1                                     ! ref. (1) eq. (2)
!
!  Single pair contribution to S(q,omega)
!
  10  SSK = FSK * HFA_SF(X,Z)                                       ! ref. (1) eq. (3)
      goto 20
!
!  Multipair contribution to S(q,omega)
!
      NUM1 = EIGHT * THIRD * FMK * Y2 * OMG**3                      !
      DEN1 = SQRT(PI) * OMK**5                                      !
      NUM2 = OMG * OMG                                              !
      DEN2 = OMK * OMK                                              !
!
      EXPO = - NUM2 / DEN2                                          !
!
      IF(EXPO > MIN_EXP) THEN                                       !
        EXP22 = EXP(EXPO)                                           !
      ELSE                                                          !
        EXP22 = TTINY                                               !
      END IF                                                        !
!
      SMK = NUM1 * EXP22 / DEN1                                     ! ref. (1) eq. (3)
!
!      NIC_SF = SPK + SSK + SMK                                      !
  20  NIC_SF = SSK                                      !
!
      END FUNCTION NIC_SF
!
!=======================================================================
!
      FUNCTION UTI_SF(X,Z,T,RS,SQ_TYPE,GQ_TYPE,EC_TYPE,IQ_TYPE)
!
!  This function computes the Utsumi-Ichimaru approximation for
!    the dynamical structure factor S(q,omega) for 3D systems
!
!  Reference: (1) K. Utsumi and S. Ichimaru, 
!                    Phys. Rev. B 22, 1522-1533 (1980)
!             (2) S. Ichimaru, "Statistical Plasma Physics", Vol2, 
!                    (CRC Press,2019)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : omega / omega_q        --> dimensionless
!       * T        : temperature  (in SI)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * SQ_TYPE  : static structure factor approximation (3D)
!       * GQ_TYPE  : local-field correction type (3D)
!       * EC_TYPE  : type of correlation energy functional
!       * IQ_TYPE  : type of approximation for I(q)
!
!  Output parameters:
!
!       * UTI_SF   : dynamic structure factor
!
!
!  Warning: We note in eq. (5.1) that the S(q,omega) they define 
!           is N times the standard definition. Therefore, all 
!           results have to be divided by N
!
!  Note: ref. (2) rectifies the awkward k_{TF} of ref. (1) by k_F 
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 22 Dec 2020
!
!
      USE REAL_NUMBERS,              ONLY : ZERO,ONE,TWO,THREE,FOUR,HALF,TTINY
      USE CONSTANTS_P1,              ONLY : H_BAR,M_E
      USE FERMI_SI,                  ONLY : EF_SI,KF_SI,VF_SI
      USE PI_ETC,                    ONLY : PI,PI2,PI_INV
!
      USE PLASMON_ENE_SI
      USE UTIC_VALUES
      USE MINMAX_VALUES
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 6)   ::  EC_TYPE
      CHARACTER (LEN = 4)   ::  GQ_TYPE
      CHARACTER (LEN = 3)   ::  SQ_TYPE,IQ_TYPE
!
      REAL (WP), INTENT(IN) ::  X,Z,T,RS
      REAL (WP)             ::  UTI_SF
!      
      REAL (WP)             ::  Y,U,V
      REAL (WP)             ::  Q_SI
      REAL (WP)             ::  OMP,OMQ,OME,OMF
      REAL (WP)             ::  KS,RAT1,RAT2
      REAL (WP)             ::  NUM,DEN
      REAL (WP)             ::  S_PL,S_SP,S_MP 
      REAL (WP)             ::  MAX_EXP,MIN_EXP
      REAL (WP)             ::  EXPO,EXPA
!
      REAL (WP)             ::  SQRT,EXP 
!
!  Computing the max and min value of the exponent of e^x
!
      CALL MINMAX_EXP(MAX_EXP,MIN_EXP)                              !
!
      Y = X + X                                                     ! Y = q / k_F 
      U = X * Z                                                     ! omega / (q * v_F)
      V = Z * Y * Y                                                 ! omega / omega_{k_F}
!
      Q_SI  = Y * KF_SI                                             !  q in SI
!
!  Computing the different frequencies involved
!
      OMQ = HALF  * H_BAR * Q_SI * Q_SI / M_E                       ! omega_q in SI
      OME = Z * OMQ                                                 ! omega   in SI
      OMP = ENE_P_SI / H_BAR                                        ! omega_p in SI
      OMF = EF_SI / H_BAR                                           ! omega_F in SI
!
      RAT1 = (OME / MO_Q)**2                                        ! (omega / Omega(q))^2
      RAT2 = (OME / (U * OMP) )**2                                  ! (omega / (U omega_p))^2
!
!  Plasmon contribution to S(q,omega)
!
      NUM = - PI_INV * OMQ * GAM_Q                                  !
      DEN =   OM_Q * ( (OME - OM_Q)**2 + GAM_Q * GAM_Q )            ! 
!
      S_PL = NUM / DEN                                              ! ref. (1) eq. (5.14)
!
!  Single-pair contribution to S(q,omega)
!
      IF(U < ONE) THEN                                              !
        NUM = THREE * Y**4 * U                                      !
        DEN = PI2 * OMF * (U * U + FOUR / PI2)                      !
!
        S_SP = NUM / DEN                                            ! ref. (1) eq. (5.15)
      ELSE                                                          !
        S_SP = ZERO                                                 !
      END IF                                                        !
!
!  Multipair contribution to S(q,omega)
!
      EXPO = - HALF * RAT1                                          !
      IF(EXPO > MIN_EXP / TWO) THEN                                 !
        EXPA = EXP(EXPO)                                            !
      ELSE                                                          !
        EXPA = TTINY                                                !
      END IF                                                        !
!
      IF(OME < OMP) THEN                                            !
!
        NUM = OME * RAT2 * EXPA                                     !
        DEN = TWO * PI * OMF * OMP * OMP * TAU_Q                    !
!
      ELSE                                                          !
!
        NUM = RAT2 * EXPA                                           !
        DEN = TWO * PI * OMF * OME * TAU_Q                          !
!
      END IF                                                        !
! 
      S_MP = NUM / DEN                                              ! ref. (1) eq. (5.19)
! 
!      UTI_SF = S_PL + S_SP + S_MP                                   !
      UTI_SF = S_PL + S_MP                                   !
!
      END FUNCTION UTI_SF  
!
!=======================================================================
!
      FUNCTION VLA_SF(X,RS,Z,T,GQ_TYPE,SQ_TYPE)
!
!  This function computes the linearized Vlasov approximation for
!    the dynamical structure factor S(q,omega) for 3D systems
!
!  It has been derived for classical fluids.
!
!  References: (1) M. Nelkin and S. Ranganathan, Phys. Rev. 164,
!                     222-227 (1967)
!
!  Input parameters: 
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : omega / omega_q        --> dimensionless
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * GQ_TYPE  : local-field correction type (3D)
!       * SQ_TYPE  : static structure factor approximation (3D)
!
!
!  Intermediate parameters:
!
!       * Y        : dimensionless factor   --> Y = X+X  =  q / k_F 
!
!
!   Author :  D. Sébilleau
!barb
!                                           Last modified :  4 Dec 2020
!
!
      USE REAL_NUMBERS,            ONLY : ONE,TWO,TTINY
      USE CONSTANTS_P1,            ONLY : M_E,K_B
      USE FERMI_SI,                ONLY : KF_SI,VF_SI
      USE PI_ETC,                  ONLY : PI
      USE EXT_FUNCTIONS,           ONLY : DAWSON                         ! Dawson function D(x)
      USE STRUCTURE_FACTOR_STATIC
      USE STRUCTURE_FACTOR_STATIC_2
      USE MINMAX_VALUES
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  GQ_TYPE
      CHARACTER (LEN = 3)   ::  SQ_TYPE
!
      REAL (WP), INTENT(IN) ::  X,Z,RS,T
      REAL (WP)             ::  VLA_SF
      REAL (WP)             ::  Y,U,U2
      REAL (WP)             ::  Q_SI
      REAL (WP)             ::  SQ,V0_SI,N0C
      REAL (WP)             ::  AX,BX,OOSQ
      REAL (WP)             ::  NUM,DEN
      REAL (WP)             ::  MAX_EXP,MIN_EXP
!
      REAL (WP)             ::  EXP,SQRT
!
      INTEGER               ::  I_MODE
!
!  Computing the max and min value of the exponent of e^x
!
      CALL MINMAX_EXP(MAX_EXP,MIN_EXP)                              !
!
      Y = X + X                                                     ! Y = q / k_F 
      U = X * Z                                                     ! U = omega / q v_F
!
      Q_SI = Y * KF_SI                                              ! q in SI
!
!  Choice of the velocity:  --> I_MODE = 0 : v = v_F
!                           --> I_MODE = 1 : v = v_0 = sqrt(2 * k_B * T / m)
!
      I_MODE = 1                                                    !
!
      IF(I_MODE == 1) THEN                                          !
        V0_SI = SQRT(TWO * K_B * T / M_E)                           !
        U     = U * VF_SI / V0_SI                                   ! omega / (q * v_0)
      ELSE                                                          !
        V0_SI = VF_SI                                               ! 
      END IF                                                        !
!
      U2 = U * U                                                    ! 
!
!  Computing the static structure factor
!
      IF(SQ_TYPE /= 'GEA' .AND. SQ_TYPE /= 'ICH' .AND.            & !
         SQ_TYPE /= 'PKA' .AND. SQ_TYPE /= 'SIN' .AND.            & !
         SQ_TYPE /= 'SPA') THEN                                     !
        CALL STFACT_STATIC_3D(X,RS,T,SQ_TYPE,GQ_TYPE,SQ)            !
      ELSE                                                          !
        CALL STFACT_STATIC_3D_2(X,RS,T,SQ_TYPE,GQ_TYPE,SQ)          !
      END IF                                                        !
!
      OOSQ = ONE / SQ                                               !
!
      N0C = ONE - OOSQ                                              !
      IF(- U2 > MIN_EXP / TWO) THEN                                 !
        AX = SQRT(PI) * EXP(- U2)                                   ! ref. (1) eq. (14)
      ELSE                                                          !
        AX = SQRT(PI) * TTINY                                       !
      END IF                                                        !
      BX  = TWO * DAWSON(U)                                         !
!
      NUM = TWO * AX                                                !
      DEN = Q_SI * V0_SI * (                                      & !
            ( OOSQ +  N0C * U * BX )**2 + (N0C * U * AX)**2 & !
                           )                                        !
!
      VLA_SF =  NUM / DEN                                           ! ref. (1) eq. (13)
!
      END FUNCTION VLA_SF  
!
END MODULE STRUCTURE_FACTOR_DYNAMIC
