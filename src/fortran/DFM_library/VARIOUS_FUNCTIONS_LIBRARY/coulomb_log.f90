!
!=======================================================================
!
MODULE COULOMB_LOG 
!
!  This module provides Coulomb logarithms
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      FUNCTION COU_LOG(I_CL,DMN,T,RS)
!
!  This function computes the Coulomb logarithm Log(Gamma)
!
!  References: (1) F. L. Hinton, chapter 1.5, in "Handbook of Plasma Physics", 
!                                Eds. M. N. Rosenbluth and R. Z. Sagdeev, 
!                                Vol.1 (1983)
!              (2) https://ocw.mit.edu/courses/nuclear-engineering/
!                        22-611j-introduction-to-plasma-physics-i-fall-2006/
!                        readings/chap3.pdf
!              (3) http://homepages.cae.wisc.edu/~callen/chap2.pdf
!              (4) https://www.nrl.navy.mil/ppd/sites/www.nrl.navy.mil.ppd/
!                         files/pdfs/NRL_FORMULARY_18.pdf
!
!  Input parameters:
!
!       * I_CL     : Switch to compute the Coulomb logarithm 
!                      I_CL  = 1  -->  using reference (1)
!                      I_CL  = 2  -->  using reference (2)
!                      I_CL  = 3  -->  using reference (3)
!                      I_CL  = 4  -->  using reference (4)
!                      I_CL  = 5  -->  using reference (5)
!       * DMN      : dimension of the system
!                       DMN  = '3D' 
!                       DMN  = '2D' 
!                       DMN  = '1D' 
!       * T        : system temperature in SI
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!  Output parameters:
!
!       * COU_LOG  : Coulomb logarithm Log(Gamma)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,THIRD 
      USE CONSTANTS_P1,        ONLY : H_BAR,M_E,E,EPS_0,COULOMB,K_B
      USE PI_ETC,              ONLY : PI_INV
      USE SCREENING_VEC,       ONLY : DEBYE_VECTOR
      USE ENE_CHANGE,          ONLY : EV
!
      IMPLICIT NONE
!
      CHARACTER*2 DMN
!
      INTEGER I_CL
!
      REAL*8 T,RS
      REAL*8 COU_LOG,G,LG
      REAL*8 KD_SI,V_TH,T_TH,LD,B0,B1,B2,BM,N0
!
!  Computing the Debye vector
!
      CALL DEBYE_VECTOR(DMN,T,RS,KD_SI)                             !
!
      V_TH=DSQRT(TWO*K_B*T/M_E)                                     ! thermal velocity in 3D
      T_TH=K_B*T/EV                                                 ! temperature in eV
!
      IF(I_CL.EQ.1) THEN                                            !
!
        LD=ONE/KD_SI                                                ! Spitzer value
        B0=E*E*THIRD/(K_B*T)                                        !
        COU_LOG=DLOG(LD/B0)                                         ! ref. (2) eq. (5)
!
      ELSE IF(I_CL.EQ.2) THEN                                       !
!
        G=DSQRT(EPS_0*T/(N0*E*E))*M_E*V_TH*V_TH/COULOMB             ! ref. (2) eq. (3.63)
        COU_LOG=DLOG(G)                                             !
!
      ELSE IF(I_CL.EQ.3) THEN                                       !
!
        B1=KD_SI*KD_SI*PI_INV/(12.0E0_WP*N0)                        !
        B2=H_BAR/(TWO*M_E*V_TH)                                     !
        BM=MAX(B1,B2)                                               ! ref. (3) eq. (2.11)
        G=KD_SI/BM                                                  !
        COU_LOG=DLOG(G)                                             !
!
      ELSE IF(I_CL.EQ.4) THEN                                       !
!
        LG=23.5E0_WP - DLOG(DSQRT(N0)*(T_TH**(-1.25E0_WP))) -     & !
                DSQRT(1.0E-5_WP + (DLOG(T_TH)-TWO)**2 / 16.0E0_WP)  !
        COU_LOG=LG                                             !
!
      ELSE IF(I_CL.EQ.5) THEN                                       !
!
        CONTINUE
!
      END IF                                                        !
!
      END FUNCTION COU_LOG  
!
!
!=======================================================================
!
      FUNCTION DALI_CL_3D(X)
!
!  This function computes Daligault' expression of the Coulomb logarithm
!
!
!  Reference: (1) J. Daligault, Phys. Rev. Lett. 119, 045002 (2017) 
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!
!
!  Note: It is defined as 
!
!            / A           3
!           |             q
!   CL =    |      ---------------- dq   with    q  the screening vector
!           |          2      2  2                s
!          / 0      ( q   +  q  )
!                             s
!                     _____________ 
!                    /
!    and A ~ k   \  / 4 Theta / 3    with Theta the degeneracy parameter
!             F   \/
!
!
!    We use here the fact that CL writes 
!                _                                _
!               |       2                          | A
!               |      q                           |
!           1   |       s            (  2     2 )  |
!          ---  | -----------  +  Log( q  +  q  )  |
!           2   |   2      2         (        s )  |
!               |  q   +  q                        |
!               |_          s                     _| 0
!
!
!   Author :  D. Sébilleau
!
!
!                                           Last modified : 12 Oct 2020
!
!
      USE MATERIAL_PROP,       ONLY : RS
      USE EXT_FIELDS,          ONLY : T
!
      USE REAL_NUMBERS,        ONLY : ONE,FOUR,HALF,THIRD
      USE FERMI_SI,            ONLY : KF_SI
!
      USE PLASMON_SCALE_P,     ONLY : NONID
      USE SCREENING_TYPE
      USE SCREENING_VEC
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X
      REAL (WP)             ::  DALI_CL_3D
      REAL (WP)             ::  TH
      REAL (WP)             ::  KS,DQT2,NUM,DEN
      REAL (WP)             ::  INT_0,INT_A
!
      REAL (WP)             ::  LOG
!
      TH   = ONE / NONID                                            ! Theta
      DQT2 = KF_SI * KF_SI * FOUR * THIRD * TH                      ! (upper integration bound)^2
!
!  Computing the screening vector
!
      IF(SC_TYPE == 'NO') THEN                                      !
        CALL SCREENING_VECTOR('TF','3D',X,RS,T,KS)                  !
      ELSE                                                          !
        CALL SCREENING_VECTOR(SC_TYPE,'3D',X,RS,T,KS)               ! in SI
      END IF                                                        !
!
      NUM = KS * KS                                                 !
      DEN = NUM + DQT2                                              !
!
      INT_0 = HALF * ( ONE + LOG(NUM) )                             !
      INT_A = HALF * ( NUM / DEN + LOG(DEN) )                       !
!
      DALI_CL_3D = INT_A - INT_0                                    !
!
      END FUNCTION DALI_CL_3D
!
END MODULE COULOMB_LOG
