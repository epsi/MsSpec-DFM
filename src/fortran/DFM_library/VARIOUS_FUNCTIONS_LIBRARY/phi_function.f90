!
!=======================================================================
!
MODULE PHI_FUNCTION
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      FUNCTION PHI(X,THETA)
!
!  This function compute the phi(x,theta) function 
!    defined in reference (1)
!
!
!  References: (1) R. G. Dandrea, N. W. Ashcroft and A. E. Carlsson, 
!                     Phys. Rev. B 34, 2097-2111 (1986)
!
!  Input parameters:
!
!       * X        : input parameter 
!       * THETA    : dimensionless temperature k_B T / E_F  
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 21 Jul 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,SIX,SEVEN,EIGHT,TEN,  &
                                   HALF
      USE PI_ETC,           ONLY : PI2,SQR_PI
      USE SQUARE_ROOTS,     ONLY : SQR2
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X,THETA,PHI,PHI_TILDE
      REAL (WP)             ::  S_NUM,S_DEN
!
      REAL (WP)             ::  A2,A4,A6,A8
      REAL (WP)             ::  B2,B4,B6,B8,B10
      REAL (WP)             ::  NUM,DEN,JJ,KK
      REAL (WP)             ::  A(5),B(6),C(6),D(6)
      REAL (WP)             ::  J(5),K(5)
      REAL (WP)             ::  T,TS,T2,T3,T4,T5,T6,T7,T8,T9,T10
      REAL (WP)             ::  X2,X4,X6,X8,X10
!
      DATA A / -0.2280E0_WP,  0.4222E0_WP, -0.6466E0_WP,        &   ! coef. eq. (A21)
               0.70572E0_WP, 5.88200E0_WP               /           !
      DATA B / -3.0375E0_WP,  64.646E0_WP , 19.608E0_WP,        &   ! coef. eq. (A22)
               -96.978E0_WP,  423.66E0_WP, -331.01E0_WP /           !
      DATA C / -0.1900E0_WP, 0.36538E0_WP, -2.2575E0_WP,        &   ! coef. eq. (A23)
                22.942E0_WP, -43.492E0_WP,  106.40E0_WP /           !
      DATA D / -7.1316E0_WP,  22.725E0_WP,  58.092E0_WP,        &   ! coef. eq. (A24)
               -436.02E0_WP, -826.51E0_WP,  4912.9E0_WP /           !                        
!
      DATA J /  3248.8E0_WP,  -691.47E0_WP,-3207700.E0_WP,      &   ! coef. eq. (A19)
               -4535.6E0_WP,-462400.0E0_WP              /           ! 
      DATA K / -4.8780E0_WP,   473.25E0_WP,  -2337.5E0_WP,      &   ! coef. eq. (A20)
                348.31E0_WP,   1517.3E0_WP              /           !
!
!  Powers of theta
!
      T  =THETA                                                     !
      T2 =T*T                                                       !
      T3 =T2*T                                                      !
      T4 =T3*T                                                      !
      T5 =T4*T                                                      !
      T6 =T5*T                                                      !
      T7 =T6*T                                                      !
      T8 =T7*T                                                      !
      T9 =T8*T                                                      !
      T10=T9*T                                                      !
!
!  Powers of x
!
      X2 =X*X                                                       !
      X4 =X2*X2                                                     !
      X6 =X4*X2                                                     !
      X8 =X6*X2                                                     !
      X10=X8*X2                                                     !
!
      NUM=A(1)+T                                                    !
      DEN=A(2) + A(3)*T**A(4) + A(5)*T2                             !
      A2=NUM/DEN                                                    ! eq. (A21)
!
      NUM=ONE+B(1)*T+B(2)*T2                                        !
      DEN=B(3)+B(4)*T+B(5)*T2+B(6)*T3+20.833E0_WP*B(2)*T4           !
      A4=NUM/DEN                                                    ! eq. (A22)
!
      NUM=C(1)+T                                                    !
      DEN=C(2)+C(3)*T+C(4)*T2+C(5)*T3+C(6)*T4                       !
      A6=NUM/DEN                                                    ! eq. (A23)
!
      NUM=0.91E0_WP-6.4453E0_WP*T+12.2324E0_WP*T2                   !
      DEN=ONE+D(1)*T+D(2)*T2+D(3)*T3+D(4)*T4+D(5)*T5+D(6)*T6        !
      A8=NUM/DEN                                                    ! eq. (A24)
!
      NUM=ONE+J(1)*T2+J(2)*T4+J(3)*T7                               ! 
      DEN=ONE+ (J(1)-PI2/SIX)*T2 + J(4)*T4 + J(5)*T6 +           &  !
                (0.75E0_WP*SQR2*J(3)/SQR_PI)*T7*TS +             &  ! 
                 0.75E0_WP*J(3)*T9                                  !
      JJ=NUM/DEN                                                    ! eq. (A19)
!
      NUM=ONE+K(1)*T2+K(2)*T4+K(3)*T7                               !
      DEN=ONE+ (K(1)-0.75E0_WP*PI2)*T2 + K(4)*T4 + K(5)*T7 -     &  !
               (SEVEN*SQR2*K(3)/(EIGHT*SQR_PI))*T8*TS -          &  !
               (THREE*K(3)/EIGHT)*T10                               !
      KK=NUM/DEN                                                    ! eq. (A20)
!
      B10=1.5E0_WP*TS*FD_APP(THETA,'M1_2')*A8                       ! eq. (A12)
      B8 =1.5E0_WP*TS*FD_APP(THETA,'M1_2')*A6 -                  &  ! eq. (A13) 
          HALF*TS*T2*FD_APP(THETA,'P3_2')*B10                       !
      B6 =1.5E0_WP*TS*FD_APP(THETA,'M1_2')*A4 -                  &  ! eq. (A14)
          HALF*TS*T2*FD_APP(THETA,'P3_2')*B8 -                   &  !
          THREE*TS*T3*FD_APP(THETA,'P5_2')*B10 /TEN                 !
      B2 =A2+TWO*JJ/(THREE*TS*FD_APP(THETA,'M1_2'))                 ! eq. (A15) 
      B4 =B2*B2-A2*B2+A4+TWO*KK/(15.0E0_WP*TS*FD_APP(THETA,'M1_2')) ! eq. (A16)   
!
!  Calculation of PHI_TILDE
!
      S_NUM=ONE+A2*X2+A4*X4+A6*X6+A8*X8                             !
      S_DEN=ONE+B2*X2+B4*X4+B6*X6+B8*X8+B10*X10                     !
!
      PHI_TILDE=S_NUM/S_DEN                                         ! eq. (4.8b)
!
      PHI=TS*FD_APP(THETA,'M1_2')*X*PHI_TILDE                       ! eq. (4.8a)
!
      END FUNCTION PHI   
!
!=======================================================================
!
      FUNCTION FD_APP(THETA,TYP)
!
!  This function compute the Padé approximants for the Fermi integrals, 
!    as defined in reference (1)
!
!
!  References: (1) R. G. Dandrea, N. W. Ashcroft and A. E. Carlsson, 
!                     Phys. Rev. B 34, 2097-2111 (1986)
!
!  Input parameters:
!
!       * THETA    : dimensionless temperature k_B T / E_F 
!       * TYP      : type of Fermi integral
!                       TYP  =  'M1_2'  --> eq. (A1)
!                       TYP  =  'D1_2'  --> eq. (A2)
!                       TYP  =  'P3_2'  --> eq. (A3)
!                       TYP  =  'P5_2'  --> eq. (A4)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 21 Jul 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,FOUR,FIVE,SIX,SEVEN,& 
                                   HALF,FOURTH,FIFTH,SIXTH
      USE PI_ETC,           ONLY : PI2,SQR_PI
      USE SQUARE_ROOTS,     ONLY : SQR2
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  TYP
!
      REAL (WP)             ::  THETA
      REAL (WP)             ::  FD_APP
      REAL (WP)             ::  T,T2,T3,T4,T5,T6,T7,T8,T9,SQR_T
      REAL (WP)             ::  C1,C2,C3,C4,C5
      REAL (WP)             ::  COEF,NUM,DEN
!
!  Powers of theta
!
      T  =THETA                                                     !
      T2 =T*T                                                       !
      T3 =T2*T                                                      !
      T4 =T3*T                                                      !
      T5 =T4*T                                                      !
      T6 =T5*T                                                      !
      T7 =T6*T                                                      !
      T8 =T7*T                                                      !
      T9 =T8*T                                                      !
      SQR_T=DSQRT(T)                                                !
!
      IF(TYP == 'M1_2') THEN                                        !
!
      C1=41.775E0_WP                                                !
      C2=27.390E0_WP                                                !
      C3=4287.2E0_WP                                                ! table I.
      C4=50.605E0_WP                                                !
      COEF=TWO/SQR_T                                                !
      NUM=ONE + C1*T2 + C2*T4 + C3*T6                               !
      DEN=ONE + (C1 + PI2/12.0E0_WP)*T2 + C4*T4 +              &    !
                (C3 / (SQR2*SQR_PI))*T5*SQR_T   + (THREE*HALF*C3)*T7!
!
      FD_APP=COEF*NUM/DEN                                           ! ref. (1) eq. (A1)
!
      ELSEIF(TYP == 'D1_2') THEN                                    !
!
      C1=2.2277E0_WP                                                !
      C2=126.92E0_WP                                                !
      C3=5248.0E0_WP                                                !
      C4=97.720E0_WP                                                ! table I.
      C5=861.30E0_WP                                                !
      COEF=SQR_T                                                    !
      NUM=ONE + C1*T2 + C2*T4 + C3*T7                               !
      DEN=ONE + (C1 - PI2/SIXTH)*T2 + C4*T4 + C5*T6 +          &    !
                (THREE*SQR2*FOURTH*C3/SQR_PI)*T7*SQR_T +       &    !
                (THREE*FOURTH*C3)*T9                                !
!
      FD_APP=COEF*NUM/DEN                                           ! ref. (1) eq. (A2)
!
      ELSEIF(TYP == 'P3_2') THEN                                    !
!
      C1= 5.3588E0_WP                                               !
      C2=-2.5433E0_WP                                               !
      C3= 432.89E0_WP                                               !
      C4= 1.8800E0_WP                                               ! table I.
      COEF=TWO / (FIVE*T2*SQR_T)                                    !
      NUM=ONE + C1*T2 + C2*T4 + C3*T8                               !
      DEN=ONE + (C1 - FIVE*PI2/12.0E0_WP)*T2 + C4*T4 -         &    !
                (TWO*C3 / (15.0E0_WP*SQR2*SQR_PI))*T5*SQR_T +  &    !
                (TWO*FIFTH*C3)*T7                                   !
!
      FD_APP=COEF*NUM/DEN                                           ! ref. (1) eq. (A3)
!
      ELSEIF(TYP == 'P5_2') THEN                                    !
!
      C1=-8.61640E0_WP                                              !
      C2=-357.410E0_WP                                              ! table I.
      C3= 5711.10E0_WP                                              !
      COEF=TWO / (SEVEN*T3*SQR_T)                                   !
      NUM=ONE + C1*T2 + C2*T4 + C3*T7*SQR_T                         !
      DEN=ONE + (C1-SEVEN*PI2*SIXTH)*T2 -                      &    !
                (SQR2*C3 / (35.0E0_WP*SQR_PI))*T4 +            &    !
                (FOUR*C3*FIFTH)*T5*SQR_T                            !
!
      FD_APP=COEF*NUM/DEN                                           ! ref. (1) eq. (A4)
!
      END IF                                                        !
!
      END FUNCTION FD_APP
!
END MODULE PHI_FUNCTION
