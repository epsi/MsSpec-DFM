!
!=======================================================================
!
MODULE SPH_BESSEL 
!
!  This module provides the spherical Bessel functions 
!
!  It contains the following functions:
!
!             * FUNCTION SPH_BESSJ0(X)    --> j_0(X)
!             * FUNCTION SPH_BESSJ1(X)    --> j_1(X)
!             * FUNCTION SPH_BESSN0(X)    --> n_0(X)
!             * FUNCTION SPH_BESSN1(X)    --> n_1(X)
!             * FUNCTION SPH_BESSI0(X)    --> i_0(X)
!             * FUNCTION SPH_BESSI1(X)    --> i_1(X)
!             * FUNCTION SPH_BESSK0(X)    --> k_0(X)
!             * FUNCTION SPH_BESSK1(X)    --> k_1(X)
!
!  Modules used: ACCURACY_REAL
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      FUNCTION SPH_BESSJ0(X)
!
!  This function calculates the first kind spherical Bessel function 
!     of order 0 
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)      ::   X
      REAL (WP)                  ::   SPH_BESSJ0
!
      REAL (WP)                  ::   DSIN
!
      SPH_BESSJ0 = DSIN(X) / X                                      !
!
      END FUNCTION SPH_BESSJ0
!
!=======================================================================
!
      FUNCTION SPH_BESSJ1(X)
!
!  This function calculates the first kind spherical Bessel function 
!     of order 1 
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)      ::   X
      REAL (WP)                  ::   SPH_BESSJ1
!
      REAL (WP)                  ::   DSIN,DCOS
!
      SPH_BESSJ1 = DSIN(X) / (X*X) - DCOS(X) / X                    !
!
      END FUNCTION SPH_BESSJ1
!
!=======================================================================
!
      FUNCTION SPH_BESSN0(X)
!
!  This function calculates the second kind spherical Bessel function 
!     of order 0 
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)      ::   X
      REAL (WP)                  ::   SPH_BESSN0
!
      REAL (WP)                  ::   DCOS
!
      SPH_BESSN0 = - DCOS(X) / X                                    !
!
      END FUNCTION SPH_BESSN0
!
!=======================================================================
!
      FUNCTION SPH_BESSN1(X)
!
!  This function calculates the second kind spherical Bessel function 
!     of order 1 
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)      ::   X
      REAL (WP)                  ::   SPH_BESSN1
!
      REAL (WP)                  ::   DSIN,DCOS
!
      SPH_BESSN1 = - DCOS(X) / (X*X) - DSIN(X) / X                  !
!
      END FUNCTION SPH_BESSN1
!
!=======================================================================
!
      FUNCTION SPH_BESSI0(X)
!
!  This function calculates the first kind modified spherical Bessel function 
!     of order 0 
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)      ::   X
      REAL (WP)                  ::   SPH_BESSI0
!
      REAL (WP)                  ::   DSINH
!
      SPH_BESSI0 = DSINH(X) / X                                     !
!
      END FUNCTION SPH_BESSI0
!
!=======================================================================
!
      FUNCTION SPH_BESSI1(X)
!
!  This function calculates the first kind modified spherical Bessel function 
!     of order 1 
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)      ::   X
      REAL (WP)                  ::   SPH_BESSI1
!
      REAL (WP)                  ::   DSINH,DCOSH
!
      SPH_BESSI1 = (X*DCOSH(X) - DSINH(X)) / (X*X)                  !
!
      END FUNCTION SPH_BESSI1
!
!=======================================================================
!
      FUNCTION SPH_BESSK0(X)
!
!  This function calculates the second kind modified spherical Bessel function 
!     of order 0 
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)      ::   X
      REAL (WP)                  ::   SPH_BESSK0
!
      REAL (WP)                  ::   DEXP
!
      SPH_BESSK0 = DEXP(-X) / X                                     !
!
      END FUNCTION SPH_BESSK0
!
!=======================================================================
!
      FUNCTION SPH_BESSK1(X)
!
!  This function calculates the second kind modified spherical Bessel function 
!     of order 1 
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)      ::   X
      REAL (WP)                  ::   SPH_BESSK1
!
      REAL (WP)                  ::   DEXP
!
      SPH_BESSK1 = (DEXP(-X)*(X + 1.0E0_WP)) / (X*X)                !
!
      END FUNCTION SPH_BESSK1
!
END MODULE SPH_BESSEL 
