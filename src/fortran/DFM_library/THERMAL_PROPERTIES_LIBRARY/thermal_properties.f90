!
!=======================================================================
!
MODULE THERMAL_PROPERTIES 
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE TH_PROP(DMN,RS,T,K_TH,V_TH,L_TH,CV,P)
!
!  This subroutine computes the thermal variations of different 
!    physical properties
!
!  References: (1) J.-S. Chen, J. Stat. Mech. L08002 (2009)
!              (2) M. Selmke, https://photonicsdesign.jimdo.com/app/
!                             download/5512592980/SommerfeldExpansion.pdf?t=1418743530
!
!  Input parameters:
!
!       * DMN      : dimension    
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)     
!
!  Output parameters:
!
!       * K_TH     : De Broglie momentum
!       * V_TH     : thermal velocity
!       * L_TH     : Landau length
!       * CV       : electron specific heat
!       * P        : electron pressure
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 28 Jul 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,FIVE,SIX, & 
                                   HALF,THIRD
      USE CONSTANTS_P1,     ONLY : H_BAR,M_E,E,COULOMB,K_B
      USE FERMI_SI,         ONLY : EF_SI,TF_SI
      USE PI_ETC,           ONLY : PI,PI2,PI3
      USE UTILITIES_1,      ONLY : RS_TO_N0
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP)             ::  RS,T,BETA,RAT
      REAL (WP)             ::  T_F,K_TH,V_TH,L_TH,CV,P
      REAL (WP)             ::  N0
!
      N0=RS_TO_N0(DMN,RS)                                           !
!
      BETA=ONE/(K_B*T)
!
      RAT=T/TF_SI                                                   ! T / T_F ratio
!
      K_TH=ONE/DSQRT(TWO*PI*H_BAR*H_BAR*BETA/M_E)                   ! De Broglie momentum
      V_TH=DSQRT(ONE/(M_E*BETA))                                    ! thermal velocity
      L_TH=E*E*COULOMB*BETA                                         ! Landau length
!
      IF(DMN == '3D') THEN
        CV=K_B*N0 * ( HALF*PI2*RAT -                              & ! ref. 2 eq. 31
                      THREE*PI2*PI2*RAT*RAT*RAT / 20.0E0_WP -     & !
                      247.0E0_WP*PI3*PI3*RAT*RAT*RAT*RAT*RAT /    & !
                      2016.0E0_WP                                 & !
                    )                                               !
        P=TWO*THIRD*EF_SI*N0 * (ONE + FIVE*PI2*RAT*RAT/12.0E0_WP -& ! ref. 1 eq. (23)
                                PI2*PI2*RAT*RAT*RAT*RAT/16.0E0_WP)  !
      ELSE IF(DMN == '2D') THEN                                     !
        CV=K_B*N0 * THIRD*PI2*RAT                                   !
      ELSE IF(DMN == '1D') THEN                                     !
        CV=K_B*N0 * (PI2*RAT/SIX)                              !
      END IF                                                        !
!
      END SUBROUTINE TH_PROP  
 
!
END MODULE THERMAL_PROPERTIES
