!
!=======================================================================
!
MODULE CHEMICAL_POTENTIAL
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      FUNCTION MU_RS(RS,EC_TYPE)
!
!  This function computes the chemical potential as a function of r_s
!    for a 3D system
!
!  References: (1) G. E. Simion and G. F. Giuliani, Phys. Rev. B 77,
!                     035131 (2008)
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * EC_TYPE  :  type of correlation energy functional     
!
!  Output parameters:
!
!       * MU_RS    : chemical potential in SI
!
!
!  Warning : all correlation energies are given in Ryd
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  2 Nov 2020
!
!
      USE REAL_NUMBERS,         ONLY : ZERO,HALF,THIRD
      USE CONSTANTS_P1,         ONLY : BOHR,E
      USE PI_ETC,               ONLY : PI_INV
      USE UTILITIES_1,          ONLY : ALFA
      USE CORRELATION_ENERGIES, ONLY : EC_3D,DERIVE_EC_3D
      USE ENE_CHANGE,           ONLY : EV,RYD
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 6)   ::  EC_TYPE
!
      REAL (WP), INTENT(IN) ::  RS
      REAL (WP)             ::  MU_RS
      REAL (WP)             ::  ALPHA,COEF,K
      REAL (WP)             ::  EC,D_EC_1,D_EC_2
! 
      ALPHA = ALFA('3D')                                            !
      COEF  = E * E / (BOHR * RS)                                   ! e^2 /(a_0 * r_s)
      K     = EV * RYD                                              ! conversion Ryd --> SI
!
!  Computing the correlation energy derivatives
!
      CALL DERIVE_EC_3D(EC_TYPE,1,5,RS,ZERO,D_EC_1,D_EC_2)          !
!
      MU_RS = HALF * COEF / (RS * ALPHA * ALPHA) -                & !
              PI_INV * COEF / ALPHA              +                & ! ref. (1) eq. (50)
              EC_3D(EC_TYPE,1,RS,ZERO) * K       -                & !
              THIRD * RS * D_EC_1 * K                               !
!
      END FUNCTION MU_RS
!
!=======================================================================
!
      FUNCTION MU(DMN,T)
!
!  This function computes the chemical potential as a function of T, 
!    for small values of T
!
!  References: (1) M. Selmke, https://photonicsdesign.jimdo.com/app/
!                             download/5512592980/SommerfeldExpansion.pdf?t=1418743530
!              (2) E. Cetina, F. Magana and A. A. Valladares,
!                             Am. J. Phys. 45, 960-963 (1977)
!
!  Input parameters:
!
!       * DMN      : dimension    
!       * T        : temperature (SI)     
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  2 Nov 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE
      USE CONSTANTS_P1,     ONLY : K_B
      USE FERMI_SI,         ONLY : EF_SI
      USE PI_ETC,           ONLY : PI2
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP), INTENT(IN) ::  T
      REAL (WP)             ::  MU
      REAL (WP)             ::  BETA,THETA,THETA2,THETA4
      REAL (WP)             ::  PI4
!
      REAL (WP)             ::  LOG,EXP
!
      BETA   = ONE / (K_B * T)                                      !
      THETA  = ONE / (BETA * EF_SI)                                 ! k_B T / E_F
      THETA2 = THETA * THETA                                        !
      THETA4 = THETA2 * THETA2                                      !
! 
      PI4 = PI2 * PI2                                               !
!
      IF(DMN == '3D') THEN                                          !
!
        MU = EF_SI * (ONE - PI2 * THETA2 / 12.0E0_WP  -           & ! ref. 1 eq. (29)
                            PI4 * THETA4 / 80.0E0_WP)               !
!
      ELSE IF(DMN == '2D') THEN                                     !
!
        MU = EF_SI * (ONE + THETA * LOG(ONE - EXP(- ONE / THETA)))  ! ref. 2 eq. (13)
!
      ELSE IF(DMN == '1D') THEN                                     !
!
        MU = EF_SI * (ONE + PI2 * THETA2 / 12.0E0_WP)               !
!
      END IF                                                        !
!
      END FUNCTION MU  
!
!=======================================================================
!
      FUNCTION MU_T(DMN,T)
!
!  This function computes the chemical potential as a function of T
!    for any value if T
!
!  References: (1) N.G. Nilsson, Phys. Stat. Sol. (a) 19, K75-K78 (1973)
!                            
!                            
!  Note : we use here Nilsson' approximation for eta = mu / (k_B T), as in 3D,
!
!         2/3 D^{3/2) = F_{1/2}(eta)          <--  Fermi-Dirac integral
!                            
!
!  Input parameters:
!
!       * DMN      : dimension    
!       * T        : temperature (SI)     
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  3 Nov 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,TWO,THREE,HALF,THIRD,FOURTH
      USE CONSTANTS_P1,     ONLY : K_B
      USE FERMI_SI,         ONLY : EF_SI
      USE PI_ETC,           ONLY : PI2,SQR_PI
!
      USE SPECIFIC_INT_7,   ONLY : FD
!
      USE OUT_VALUES_10,    ONLY : I_WR
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      INTEGER               ::  NN,I,NE
      INTEGER               ::  LOGF
!
      REAL (WP), INTENT(IN) ::  T
      REAL (WP)             ::  MU_T
      REAL (WP)             ::  ETA,TH
      REAL (WP)             ::  KBT
      REAL (WP)             ::  U,V,W,FF,FDF
      REAL (WP)             ::  FDF1,FDF2,FDF3,FDF4,FDF5
      REAL (WP)             ::  ETA1,ETA2,ETA3,ETA4,ETA5
      REAL (WP)             ::  GF
      REAL (WP)             ::  D
      REAL (WP)             ::  DD,ET_STEP,ET,ET_M,ET_AV
!
      REAL (WP)             ::  FLOAT,LOG,SQRT,ABS
!
      LOGF = 6                                                      !
!
      GF = HALF * SQR_PI                                            ! Gamma(3/2)
!
      KBT = K_B * T                                                 !
      TH  = KBT / EF_SI                                             !
!
!  Calculating eta
!
      IF(DMN == '3D') THEN                                          !
!
        D    = ONE / TH                                             ! E_F / k_B T
        U    = TWO * THIRD * (D**1.5E0_WP)  / GF                    !
        W    = THREE * SQR_PI * U * FOURTH                          !
        V    = W**(TWO * THIRD)                                     ! 
!
!  Computing Nilsson's approximations to eta
!
!
        ETA1 = LOG(U)                                               !
        FDF1 = FD(ETA1,0.5D0)                                       !
!
        IF(U < 3.703704D0) THEN                                     !
          ETA2 = LOG( U / (1.0D0 - 0.27D0 * U) )                    !
          FDF2 = FD(ETA2,0.5D0)                                     !
        END IF                                                      !
!
        IF(V * V > PI2 / 6.0D0) THEN                                !
          ETA3 = SQRT(  V * V - PI2 / 6.0D0 )                       !
          FDF3 = FD(ETA3,0.5D0)                                     !
        END IF                                                      !
!
        IF(U /= 1.0D0) THEN                                         !
          ETA4 = LOG(U) / (1.0D0 - U) + V + 2.0D0 * V /           & !
                                            (3.0D0 + V)**2          !
          FDF4 = FD(ETA4,0.5D0)                                     !
        END IF                                                      !
!
        IF(U /= 1.0D0) THEN                                         !
          ETA5 = LOG(U) / (1.0D0 - U * U) + V -                   & !
                  V / (0.24D0 + 1.08D0 * V)**2                      !
          FDF5 = FD(ETA5,0.5D0)                                     !
        END IF                                                      !
!
        FF   =  2.0D0 * THIRD * D**1.5D0
!
        IF(U == 1.0D0) THEN                                         !
          ET_AV = ETA3                                              !
          DD    = U                                                 !
        ELSE                                                        !
          ET_AV = ETA4                                              !
          DD    = ABS(ETA5 - ETA4) / ABS(ETA5)                      !
        END IF                                                      !
!
        IF(DD > 0.001D0) THEN                                       !
!
          ET_M    = DD * 5.0D0                                      !
          ET_STEP = 2.0D0 * ET_M /  FLOAT(NE - 1)                   !
!
          DO I = 1, NE                                              !
            ET  = ET_AV - ET_M + FLOAT(I - 1) * ET_STEP             !
            FDF = FD(ET,0.5D0)                                      !
            IF(ABS((FDF - FF)/FF) < 0.001D0) GO TO 5                !
          END DO                                                    !
   5      ETA = ET                                                  !
        ELSE                                                        !
          ETA = ETA5                                                !
          FDF = FDF5                                                !
        END IF                                                      !
!
      ELSE IF(DMN == '2D') THEN                                     !
!
        ETA = LOG(EXP(- TH) - ONE)                                  ! 
!
      ELSE IF(DMN == '1D') THEN                                     !
!
        CONTINUE                                                    ! not implemented yet
!
      END IF                                                        !
! 
      IF(I_WR == 1) THEN                                            !
        WRITE(LOGF,10) ETA1,FDF1,FF                                 !
        WRITE(LOGF,20) ETA2,FDF2,FF                                 !
        WRITE(LOGF,30) ETA3,FDF3,FF                                 !
        WRITE(LOGF,40) ETA4,FDF4,FF                                 !
        WRITE(LOGF,50) ETA5,FDF5,FF                                 !
        WRITE(LOGF,60) ETA ,FDF ,FF                                 !
      END IF                                                        !
! 
      MU_T = ETA * KBT                                              !
!
!  Formats:
!
  10  FORMAT(//,5X,'eta_1 = ',F12.6,'  F_{1/2} approx. = ',F12.6,  &
                                    '  F_{1/2} exact.  = ',F12.6)
  20  FORMAT(//,5X,'eta_2 = ',F12.6,'  F_{1/2} approx. = ',F12.6,  &
                                    '  F_{1/2} exact.  = ',F12.6)
  30  FORMAT(//,5X,'eta_3 = ',F12.6,'  F_{1/2} approx. = ',F12.6,  &
                                    '  F_{1/2} exact.  = ',F12.6)
  40  FORMAT(//,5X,'eta_4 = ',F12.6,'  F_{1/2} approx. = ',F12.6,  &
                                    '  F_{1/2} exact.  = ',F12.6)
  50  FORMAT(//,5X,'eta_5 = ',F12.6,'  F_{1/2} approx. = ',F12.6,  &
                                    '  F_{1/2} exact.  = ',F12.6)
  60  FORMAT(//,5X,'eta   = ',F12.6,'  F_{1/2} approx. = ',F12.6,  &
                                    '  F_{1/2} exact.  = ',F12.6)
!
      END FUNCTION MU_T  
!
END MODULE CHEMICAL_POTENTIAL
