!
!=======================================================================
!
MODULE THERMODYNAMIC_PROPERTIES 
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE SPECIFIC_HEAT(DMN,I_ORD,RS,T,CV) 
!
!  This subroutine computes the specific heat
!    (per electron) in the 2D and 3D system                
!
!  References: (1) A. Isihara, "Electron Liquids", 2nd edition,
!                     Springer Series in Solid-State Sciences 96,
!
!  Input parameters:
!
!       * DMN      : dimension  
!       * I_ORD    : order of the truncation of the r_s series 
!                      I_ORD  = 1 : r_s only --> ref. 1 eq. (7.1.16)
!                      I_ORD  = 2 : r_s^2    --> ref. 1 eq. (7.1.22)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature in SI
!
!
!  Output parameters:
!
!       * CV       : specific heat                           in SI
!       * F_FR     : Helmhotz free energy per electron       in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,FOUR,NINE, & 
                                   HALF,THIRD
      USE CONSTANTS_P1,     ONLY : K_B
      USE FERMI_SI,         ONLY : KF_SI
      USE PI_ETC,           ONLY : PI,PI2,PI_INV
      USE SQUARE_ROOTS,     ONLY : SQR2
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      INTEGER               ::  I_ORD
!
      REAL (WP)             ::  RS,T
      REAL (WP)             ::  CV
      REAL (WP)             ::  C1,CV0,A,C,RAT,ETA_0
!
      REAL (WP)             ::  LOG,SQRT
!
      C1    = (FOUR / (NINE * PI2 * PI2))**THIRD                    !
      A     = -4.035E0_WP                                           !
      C     = 0.02553E0_WP                                          !
      RAT   = KF_SI / (K_B * T)                                     !
      ETA_0 = KF_SI * RAT                                           !
!
!  specific heat at T = 0
!
      IF(DMN == '3D') THEN                                          !
        CV0 = HALF * THIRD * K_B* K_B * T * KF_SI                   ! ref. (1) eq. (7.1.17)
      ELSE IF(DMN == '2D') THEN                                     !
        CV0 = THIRD * PI2 * K_B * K_B * T / (KF_SI * KF_SI)         ! ref. (1) eq. (7.1.27)
      END IF                                                        !
!
!  r_s expansion
!
      IF(DMN == '3D') THEN                                          !
        IF(I_ORD == 1) THEN                                         !
          CV = CV0 * ( ONE + C1 * RS * ( 2.5E0_WP +               & !
                                         THREE * A / PI2 -        & ! ref. (1) eq. (7.1.16)
                                         LOG(RAT * RAT)           & !
                                       )                          & !
                     )                                              !
        ELSE IF(I_ORD == 2) THEN                                    !
          CV = CV0 * ( ONE + 0.162E0_WP * RS -                    & !
                       0.166E0_WP * RS * LOG(ETA_0) -             & !
                       0.157E0_WP * RS * RS +                     & ! ref. (1) eq. (7.1.22)
                       0.0138E0_WP * RS * RS * LOG(RS) +          & !
                       RS * RS * ( 0.0282E0_WP * LOG(ETA_0) +     & !
                       0.0275E0_WP * LOG(ETA_0) * LOG(ETA_0) )    & !
                     )                                              !
        END IF                                                      !
      ELSE IF(DMN == '2D') THEN                                     !
        CV = CV0 * ( ONE + RS * ( 0.75E0_WP * SQR2 * PI_INV *     & ! 
                                  (ONE - 16.0E0_WP * C) -         & ! ref. (1) eq. (7.1.26)
                                  LOG(ETA_0) / SQRT(TWO*PI)       & !
                                )                                 & !
                   )                                                !
      END IF                                                        !
!
      END SUBROUTINE SPECIFIC_HEAT  
!
!=======================================================================
!
      FUNCTION PRESSURE_3D(RS)
!
!  This function computes the electron pressure in a 3D system  
!
!
!  References: (1) G. Giuliani and G. Vignale, "Quantum Theory of the 
!                     Electron Liquid", Cambridge Uiversity Press (2005)
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!  Note: We write the ground-state energy E = E_0 + E_X + E_C as 
!
!                        E = E_HF + E_C
!
!      and use eq. (2.56) of ref. (1) to compute the derivatives of E_HF
!
!      E_HF    =       Ad / rs^2  -     Bd / rs
!      D_EHF_1 =  -2 * Ad / rs^3  +     Bd / rs^2 
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Oct 2020
!
!
      USE REAL_NUMBERS,         ONLY : TWO,HALF,THIRD
      USE CONSTANTS_P2,         ONLY : HARTREE
      USE PI_ETC,               ONLY : PI_INV
      USE UTILITIES_1,          ONLY : ALFA
      USE EXT_FIELDS,           ONLY : T
      USE ENERGIES,             ONLY : EC_TYPE
      USE CORRELATION_ENERGIES 
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  RS
      REAL (WP)             ::  PRESSURE_3D
      REAL (WP)             ::  ALPHA
      REAL (WP)             ::  A3,B3,RS2,RS3
      REAL (WP)             ::  D_EC_1,D_EC_2,D_EHF_1
!
      ALPHA = ALFA('3D')                                            !
      A3    = 0.6E0_WP / ALPHA                                      ! ref. (1) table (2.1)
      B3    = 1.5E0_WP * PI_INV / ALPHA                             ! idem
!
      RS2 = RS  * RS                                                !
      RS3 = RS2 * RS                                                !
!
!  Computing the derivatives of the correlation energy 
!
      CALL DERIVE_EC_3D(EC_TYPE,1,5,RS,T,D_EC_1,D_EC_2)             !
!
!  Computation of the first derivative of E_HF
!
      D_EHF_1 = - TWO * A3 / RS3 + B3 / RS2                         !
!
      PRESSURE_3D = - THIRD * RS * (D_EHF_1 + D_EC_1) *           & !
                                    HALF * HARTREE                  ! ref. (1) eq. (1.137)
!
      END FUNCTION PRESSURE_3D
!
!=======================================================================
!
      FUNCTION MU_3D(RS)
!
!  This function computes the chemical potential in a 3D system  
!
!
!  References: (1) G. Giuliani and G. Vignale, "Quantum Theory of the 
!                     Electron Liquid", Cambridge Uiversity Press (2005)
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!  Note: We write the ground-state energy E = E_0 + E_X + E_C as 
!
!                        E = E_HF + E_C
!
!      and use eq. (2.56) of ref. (1) to compute the derivatives of E_HF
!
!      E_HF    =       Ad / rs^2  -     Bd / rs
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Oct 2020
!
!
      USE REAL_NUMBERS,         ONLY : HALF
      USE PI_ETC,               ONLY : PI_INV
      USE CONSTANTS_P2,         ONLY : HARTREE
      USE UTILITIES_1,          ONLY : ALFA
      USE EXT_FIELDS,           ONLY : T
      USE ENERGIES,             ONLY : EC_TYPE
      USE CORRELATION_ENERGIES 
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  RS
      REAL (WP)             ::  MU_3D
      REAL (WP)             ::  E_C,E_HF
      REAL (WP)             ::  ALPHA
      REAL (WP)             ::  A3,B3,RS2 
      REAL (WP)             ::  P
!
      ALPHA = ALFA('3D')                                            !
      A3    = 0.6E0_WP / ALPHA                                      ! ref. (1) table (2.1)
      B3    = 1.5E0_WP * PI_INV / ALPHA                             ! idem
!
      RS2 = RS  * RS                                                !
!
!  Computing the correlation energy 
!
      E_C = EC_3D(EC_TYPE,1,RS,T) * HALF * HARTREE                  ! 
!
!  Computation of E_HF
!
      E_HF = A3 / RS2 - B3 / RS                                     ! 
!
!  Computing the electronic pressure
!
      P = PRESSURE_3D(RS)
!
      MU_3D = E_HF + E_C + P                                        ! ref. (1) eq. (1.138)
!
      END FUNCTION MU_3D
!
!=======================================================================
!
      FUNCTION KAPPA_0_3D(RS)
!
!  This function computes the non-interacting compressibility 
!     in a 3D system  
!
!
!  References: (1) N. Iwamoto, Phys. Rev. A 30, 3289-3304 (1984)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Oct 2020
!
!
      USE UTILITIES_1,          ONLY : ALFA
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  RS
      REAL (WP)             ::  KAPPA_0_3D
      REAL (WP)             ::  RS2
      REAL (WP)             ::  ALPHA
!
      RS2 = RS * RS                                                 !
!
      ALPHA=ALFA('3D')                                              !
!
      KAPPA_0_3D = 1.5E0_WP * ALPHA * ALPHA * RS2                   ! ref. (1) eq. (2.17)
!
      END FUNCTION KAPPA_0_3D
!
!=======================================================================
!
      FUNCTION BULK_MOD_3D(RS)
!
!  This function computes the bulk modulus in a 3D system  
!
!
!  References: (1) G. Giuliani and G. Vignale, "Quantum Theory of the 
!                     Electron Liquid", Cambridge Uiversity Press (2005)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Oct 2020
!
!
      USE REAL_NUMBERS,         ONLY : TWO,SIX,HALF,THIRD
      USE PI_ETC,               ONLY : PI_INV
      USE CONSTANTS_P2,         ONLY : HARTREE
      USE UTILITIES_1,          ONLY : ALFA
      USE EXT_FIELDS,           ONLY : T
      USE ENERGIES,             ONLY : EC_TYPE
      USE CORRELATION_ENERGIES 
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  RS
      REAL (WP)             ::  BULK_MOD_3D
      REAL (WP)             ::  ALPHA
      REAL (WP)             ::  A3,B3,RS2,RS3,RS4
      REAL (WP)             ::  D_EC_1,D_EC_2,D_EHF_1,D_EHF_2
!
      ALPHA = ALFA('3D')                                            !
      A3    = 0.6E0_WP / ALPHA                                      ! ref. (1) table (2.1)
      B3    = 1.5E0_WP * PI_INV / ALPHA                             ! idem
!
      RS2 = RS  * RS                                                !
      RS3 = RS2 * RS                                                !
      RS4 = RS3 * RS                                                !
!
!  Computing the derivatives of the correlation energy 
!
      CALL DERIVE_EC_3D(EC_TYPE,1,5,RS,T,D_EC_1,D_EC_2)             !
!
!  Computation of the derivatives of E_HF
!
      D_EHF_1 = - TWO * A3 / RS3 + B3 / RS2                         !
      D_EHF_2 = - SIX * A3 / RS4 - TWO * B3 / RS3                   !
!
      BULK_MOD_3D = THIRD * RS * ( THIRD * RS *                   & !
                                   (D_EHF_2 + D_EC_2) -           & !
                                   TWO * THIRD * RS *             & !
                                   (D_EHF_1+D_EC_1)               & ! ref. (1) eq. (1.139)
                                  ) * HALF * HARTREE                !
!
      END FUNCTION BULK_MOD_3D
!
!=======================================================================
!
      FUNCTION KAPPA_3D(RS)
!
!  This function computes the interacting compressibility 
!     in a 3D system  
!
!  References: (1) G. Giuliani and G. Vignale, "Quantum Theory of the 
!                     Electron Liquid", Cambridge Uiversity Press (2005)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Oct 2020
!
!
      USE REAL_NUMBERS,         ONLY : ONE
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  RS
      REAL (WP)             ::  KAPPA_3D
!
      KAPPA_3D = ONE / BULK_MOD_3D(RS)                              ! ref. (1) eq. (1.140)
!
      END FUNCTION KAPPA_3D
!
!=======================================================================
!
      FUNCTION U_IN_3D(RS,T)
!
!  This function computes the internal energy per electron
!     in a 3D system  
!
!  References: (1)  S. Ichimaru, Rev. Mod. Phys. 54, 1017-1059 (1982)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature in SI
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Oct 2020
!
!
      USE CONSTANTS_P1,         ONLY : K_B
      USE PLASMON_SCALE_P,      ONLY : NONID
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  RS,T
      REAL (WP)             ::  U_IN_3D
      REAL (WP)             ::  A0,B0,C0,D0,G4
!
!  Slattery-Doolen-DeWitt parameters                                ! ref (1) eq. (2.12)
!
      A0=-0.89752E0_WP                                              ! 
      B0= 0.94544E0_WP                                              ! 1 < NONID < 160
      C0= 0.17954E0_WP                                              !
      D0=-0.80049E0_WP                                              !
!
      U_IN_3D = K_B * T * (A0 * NONID + B0 * G4 +                 & ! ref. (1) eq. (2.8) 
                           C0 / G4 + D0 + 1.5E0_WP)                 ! and  (2.11)
!
      END FUNCTION U_IN_3D
!
!=======================================================================
!
      FUNCTION U_EX_3D(RS,T)
!
!  This function computes the excess internal energy per electron
!     in a 3D system  
!
!  References: (1)  S. Ichimaru, Rev. Mod. Phys. 54, 1017-1059 (1982)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature in SI
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Oct 2020
!
!
      USE CONSTANTS_P1,         ONLY : K_B
      USE PLASMON_SCALE_P,      ONLY : NONID
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  RS,T
      REAL (WP)             ::  U_EX_3D
      REAL (WP)             ::  A0,B0,C0,D0,G4
!
!  Slattery-Doolen-DeWitt parameters                                ! ref (1) eq. (2.12)
!
      A0=-0.89752E0_WP                                              ! 
      B0= 0.94544E0_WP                                              ! 1 < NONID < 160
      C0= 0.17954E0_WP                                              !
      D0=-0.80049E0_WP                                              !
!
      U_EX_3D = A0 * NONID + B0 * G4 + C0 / G4 + D0                 ! ref. (3) eq. (2.11)
!
      END FUNCTION U_EX_3D
!
!=======================================================================
!
      FUNCTION U_IT_3D(T)
!
!  This function computes the total interaction energy per electron
!     in a 3D system  
!
!  References: (1)  K. Tago, K. Utsumi and S. Ichimaru,
!                      Prog. Theor. Phys. 65, 54-65 (1981)
!
!
!  Input parameters:
!
!       * T        : system temperature in SI
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Oct 2020
!
!
      USE REAL_NUMBERS,         ONLY : ONE,HALF,FOURTH
      USE SQUARE_ROOTS,         ONLY : SQR3
      USE CONSTANTS_P1,         ONLY : K_B
      USE PLASMON_SCALE_P,      ONLY : NONID
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  T
      REAL (WP)             ::  U_IT_3D
!
      REAL (WP)             ::  A1,A2,A3
      REAL (WP)             ::  B1,B2
!
!  TUI coefficients
!
      A1 = - 0.89461E0_WP                                           ! \
      A2 =   0.8165E0_WP                                            !  |
      A3 = - 0.5012E0_WP                                            !   > ref. (1), eq. (32)-(33)
      B1 = - 0.8899E0_WP                                            !  |
      B2 =   1.50E0_WP                                              ! /
!
      IF(NONID < ONE) THEN                                          !
        U_IT_3D = - SQR3 * (NONID**1.5E0_WP) * HALF                 !
      ELSE IF(ONE <= NONID .AND. NONID <= 40.0E0_WP) THEN           !
        U_IT_3D = A1 * NONID + A2 * (NONID**FOURTH) + A3            !
      ELSE                                                          !
        U_IT_3D = B1 * NONID + B2
      END IF                                                        !
!
      END FUNCTION U_IT_3D
!
!=======================================================================
!
      FUNCTION F_FR_3D(RS,T)
!
!  This function computes the Helmholtz free energy per electron
!     in a 3D system  
!
!  References: (1)  S. Ichimaru, Rev. Mod. Phys. 54, 1017-1059 (1982)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature in SI
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Oct 2020
!
!
      USE REAL_NUMBERS,         ONLY : THREE,FOUR
      USE CONSTANTS_P1,         ONLY : K_B
      USE PLASMON_SCALE_P,      ONLY : NONID
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  RS,T
      REAL (WP)             ::  F_FR_3D
      REAL (WP)             ::  A0,B0,C0,D0,G4
!
      REAL (WP)             ::  LOG
!
!  Slattery-Doolen-DeWitt parameters                                ! ref (1) eq. (2.12)
!
      A0=-0.89752E0_WP                                              ! 
      B0= 0.94544E0_WP                                              ! 1 < NONID < 160
      C0= 0.17954E0_WP                                              !
      D0=-0.80049E0_WP                                              !
!
      F_FR_3D = K_B * T * ( A0 * NONID +                          & !
                            FOUR * (B0 * G4 - C0 / G4) +          & !
                           (D0 + THREE) * LOG(NONID) -            & ! ref. (1) eq. (2.14)  
                           ( A0 + FOUR * B0 -                     & !
                             FOUR * C0 + 1.135E0_WP)              & !
                          )                                         ! 
!
      END FUNCTION F_FR_3D
!
END MODULE THERMODYNAMIC_PROPERTIES
