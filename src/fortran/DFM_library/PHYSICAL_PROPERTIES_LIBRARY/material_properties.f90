!
!=======================================================================
!
MODULE MATERIAL_CL
!
!  This module defines the material characteristic lengths
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  EMFP,FWL,PCL,THL,ML,CR,DL,TFL
!
END MODULE MATERIAL_CL
!
!=======================================================================
!
MODULE MATERIAL_PROPERTIES 
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE CHARACTERISTIC_LENGTHS(DMN,RS,T,B,DC,TP)
!
!  This subroutine computes and prints out characteristic lengths
!    in the input material
!
!  References: (1) H. van Houten, B. J. van Wees and C.W.J. Beenakker
!                  in "Physics and Technology of Submicron Structures",
!                  H. Heinrich, G. Bauer and F. Kuchar eds.,
!                  Springer Series in Solid State Science Vol. 83, 
!                  pp. 198-207 (1988)
!
!
!  Input parameters:
!
!       * DMN      : dimension  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature in SI
!       * B        : magnetic field in SI
!       * DC       : diffusion coefficient
!       * TP       : phase-breaking relaxation time in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 24 Jul 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,TWO,TTINY
      USE CONSTANTS_P1,     ONLY : H_BAR,E,K_B
      USE FERMI_SI,         ONLY : KF_SI,VF_SI
      USE PI_ETC,           ONLY : PI
      USE UTILITIES_1,      ONLY : D
      USE SCREENING_VEC
!
      USE MATERIAL_CL
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP)             ::  RS,T,B,DC,TP
      REAL (WP)             ::  KD,K_TF
!
      INTEGER               ::  LOGF
!
      IF(B == ZERO) B = TTINY                                       !
!
      EMFP=D(DMN)*DC/VF_SI                                          ! elastic MFP
      FWL=TWO*PI/KF_SI                                              ! Fermi wavelength
      PCL=DSQRT(DC*TP)                                              ! phase coherence length
      THL=DSQRT(H_BAR*DC/(K_B*T))                                   ! thermal length
      ML=DSQRT(H_BAR/(E*B))                                         ! magnetic length
      CR=H_BAR*KF_SI/(E*B)                                          ! cyclotron radius
!
      CALL DEBYE_VECTOR(DMN,T,RS,KD)                                !
      DL=ONE/KD                                                     ! Debye length
!
      CALL THOMAS_FERMI_VECTOR(DMN,K_TF)                            !
      TFL=ONE/K_TF                                                  ! Thomas-Fermi length
!
      END SUBROUTINE CHARACTERISTIC_LENGTHS  
!
END MODULE MATERIAL_PROPERTIES
