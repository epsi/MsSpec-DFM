!
!=======================================================================
!
MODULE THERMODYNAMIC_QUANTITIES 
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE SPECIFIC_HEAT(DMN,I_ORD,RS,T,CV) 
!
!  This subroutine computes the specific heat
!    (per electron) in the 2D and 3D system                
!
!  References: (1) A. Isihara, "Electron Liquids", 2nd edition,
!                     Springer Series in Solid-State Sciences 96,
!
!  Input parameters:
!
!       * DMN      : dimension  
!       * I_ORD    : order of the truncation of the r_s series 
!                      I_ORD  = 1 : r_s only --> ref. 1 eq. (7.1.16)
!                      I_ORD  = 2 : r_s^2    --> ref. 1 eq. (7.1.22)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature in SI
!
!
!  Output parameters:
!
!       * CV       : specific heat                           in SI
!       * F_FR     : Helmhotz free energy per electron       in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,FOUR,NINE, & 
                                   HALF,THIRD
      USE CONSTANTS_P1,     ONLY : K_B
      USE FERMI_SI,         ONLY : KF_SI
      USE PI_ETC,           ONLY : PI,PI2,PI_INV
      USE SQUARE_ROOTS,     ONLY : SQR2
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP)             ::  RS,T
      REAL (WP)             ::  CV
      REAL (WP)             ::  C1,CV0,A,C,RAT,ETA_0
!
      INTEGER               ::  I_ORD
!
      C1=(FOUR/(NINE*PI2*PI2))**THIRD                               !
      A=-4.035E0_WP                                                 !
      C=0.02553E0_WP                                                !
      RAT=KF_SI/(K_B*T)                                             !
      ETA_0=KF_SI*RAT                                               !
!
!  specific heat at T = 0
!
      IF(DMN == '3D') THEN                                          !
        CV0=HALF*THIRD*K_B*K_B*T*KF_SI                              ! ref. (1) eq. (7.1.17)
      ELSE IF(DMN == '2D') THEN                                     !
        CV0=THIRD*PI2*K_B*K_B*T/(KF_SI*KF_SI)                       ! ref. (1) eq. (7.1.27)
      END IF                                                        !
!
!  r_s expansion
!
      IF(DMN == '3D') THEN                                          !
        IF(I_ORD == 1) THEN                                         !
          CV=CV0*(ONE + C1*RS*(2.5E0_WP + THREE*A/PI2 -            &! ref. (1) eq. (7.1.16)
                               DLOG(RAT*RAT)) )                     !
        ELSE IF(I_ORD == 2) THEN                                    !
          CV=CV0*(ONE + 0.162E0_WP*RS - 0.166E0_WP*RS*DLOG(ETA_0) -&!
                  0.157E0_WP*RS*RS + 0.0138E0_WP*RS*RS*DLOG(RS) +  &!
                  RS*RS*( 0.0282E0_WP*DLOG(ETA_0) +                &! ref. (1) eq. (7.1.22)
                          0.0275E0_WP*DLOG(ETA_0)*DLOG(ETA_0) )    &!
                 )                                                  !
        END IF                                                      !
      ELSE IF(DMN == '2D') THEN                                     !
        CV=CV0*(ONE + RS*( 0.75E0_WP*SQR2*PI_INV *                 &! 
                           (ONE-16.0E0_WP*C) -                     &! ref. (1) eq. (7.1.26)
                           DLOG(ETA_0)/DSQRT(TWO*PI)               &!
                         )                                         &!
               )                                                    !
      END IF                                                        !
!
      END SUBROUTINE SPECIFIC_HEAT  
!
!=======================================================================
!
      SUBROUTINE HIGH_T_THERMO_3D(RS,T,TH_PROP,P,U_IN,U_EX,F_FR) 
!
!  This subroutine computes high-temperature thermodynamics properties
!    (per electron) in the 3D system                              --> 3D  
!
!  References: (1) A. Isihara, "Electron Liquids", 2nd edition,
!                     Springer Series in Solid-State Sciences 96,
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature in SI
!       * TH_PROP   : type of calculation
!                      TH_PROP  = 'CLAS' : classical approximation 
!                      TH_PROP  = 'QUAN' : quantum approximation 
!
!
!  Output parameters:
!
!       * P        : electron pressure                           in SI
!       * U_IN     : internal energy per electron                in SI
!       * U_EX     : excess internal energy per electron / k_B T in SI
!       * F_FR     : Helmhotz free energy per electron           in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 22 Jul 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,FOUR,TEN, & 
                                   HALF,THIRD,FOURTH
      USE CONSTANTS_P1,     ONLY : BOHR,H_BAR,M_E,E,K_B
      USE PI_ETC,           ONLY : PI,SQR_PI
      USE EULER_CONST,      ONLY : EUMAS
      USE SCREENING_VEC,    ONLY : DEBYE_VECTOR
      USE UTILITIES_1,      ONLY : RS_TO_N0
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   :: TH_PROP 
!
      REAL (WP)             ::  RS,T
      REAL (WP)             ::  P,U_IN,U_EX,F_FR
      REAL (WP)             ::  EPS,LAMBDA,GAMMA
      REAL (WP)             ::  KD_SI,N0
      REAL (WP)             ::  A(4),B(4)
      REAL (WP)             ::  DEN1,DEN2,DEN3,DEN4
      REAL (WP)             ::  X,X2,X3,X4,X6,ETA,GX
!
      DATA A /   -0.895929E0_WP, 0.11340656E0_WP,     &             !
               -0.90872827E0_WP,-0.11614773E0_WP /                  ! Hansen 
      DATA B /   4.6664860E0_WP,  13.675411E0_WP,     &             ! parameters
                 1.8905603E0_WP,  1.0277554E0_WP /                  ! 
!
!  Computing the Debye screening vector
!
      CALL DEBYE_VECTOR('3D',T,RS,KD_SI)                            !
!
!  Computing the electron density
!
      N0=RS_TO_N0('3D',RS)                                          !
!
      EPS=E*E*KD_SI/(K_B*T)                                         ! ref. (1) eq. (1.1.2)
      LAMBDA=TWO*PI*H_BAR/DSQRT(TWO*PI*M_E*K_B*T)                   ! de Broglie thermal wavelength
      ETA=TWO*PI*BOHR/LAMBDA                                        ! ref. (1) p. 3
      GAMMA=(THIRD*EPS*EPS)**THIRD                                  ! ref. (1) eq. (1.1.4)
      X=ETA/DSQRT(TWO*PI)                                           !
!
      X2=X*X                                                        !
      X3=X2*X                                                       !
      X4=X2*X2                                                      !
      X6=X4*X2                                                      !
!
!  Calculation of function g(x)
!
      IF(X <= ONE) THEN                                             !
        GX=TWO*EUMAS  +DLOG(THREE) - FOUR*THIRD - HALF*X2 +       & !
           X4/TEN + TWO*X6/21.0E0_WP                                ! ref. (1) eq. (3.2.13)
      ELSE                                                          !
        GX=-0.75E0_WP*SQR_PI*X3 + 1.5E0_WP*X2 -                   & !
            0.75E0_WP*SQR_PI*(ONE+DLOG(TWO))*X + DLOG(X) +        & ! ref. (1) eq. (3.2.13)
            HALF*EUMAS + DLOG(THREE) + 0.411E0_WP                   ! 
      END IF                                                        !
!
      IF(TH_PROP == 'CLASS') THEN                                   !
        P=ONE-HALF*THIRD*EPS*(ONE + EPS*( EUMAS - TWO*THIRD +     & ! ref. (1) eq. (3.1.19)
                                          HALF*DLOG(THREE*EPS) ) )  ! 
      ELSE                                                          !
        P=ONE-HALF*THIRD*EPS - FOURTH*THIRD*EPS*EPS*DLOG(EPS) -   & !
              FOURTH*THIRD*EPS*EPS*( TWO*EUMAS + DLOG(THREE) -    & ! ref. (1) eq. (3.2.12)
                                     FOUR*THIRD + 12.0E0_WP*GX    & !
                                   )                                !
      END IF                                                        !
!
      F_FR=DLOG(N0*LAMBDA*LAMBDA*LAMBDA/E) - HALF*THIRD* (        & !
                EPS+EPS + EPS*EPS*( EUMAS - 11.0E0_WP/12.0E0_WP + & ! ref. (1) eq. (3.1.18)
                                    HALF*DLOG(THREE*EPS) ) )        !
!
      U_IN=1.5E0_WP - HALF*( EPS + EPS*EPS*( EUMAS - TWO*THIRD +  & ! ref. (1) eq. (3.1.16)
                                        HALF*DLOG(THREE*EPS) ) )    !
!
!  Hansen's expansion
!
      DEN1=DSQRT(B(1)+GAMMA)                                        !
      DEN2=B(2)+GAMMA                                               !
      DEN3=DSQRT(B(3)+GAMMA)**3                                     !
      DEN4=(B(4)+GAMMA)**2                                          !
!
      U_EX=DSQRT(GAMMA**3)*( A(1)/DEN1 + A(2)/DEN2 +              & !
                             A(3)/DEN3 + A(4)/DEN4                & !
                           )                                        !
!
      END SUBROUTINE HIGH_T_THERMO_3D  
!
!=======================================================================
!
      SUBROUTINE THERMODYNAMICS_3D(EC_TYPE,RS,T,P,MU,K0,K,BM,     &
                                   U_IN,U_EX,F_FR)
!
!  This subroutine computes thermodynamics properties (per electron) 
!    in the 3D system                                    --> 3D  
!
!  References: (1) G. Giuliani and G. Vignale, "Quantum Theory of the 
!                     Electron Liquid", Cambridge Uiversity Press (2005)
!              (2) N. Iwamoto, Phys. Rev. A 30, 3289-3304 (1984)
!              (3) S. Ichimaru, Rev. Mod. Phys. 54, 1017-1059 (1982)
!
!  Input parameters:
!
!       * EC_TYPE  : type of correlation energy functional
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature in SI
!
!
!  Output parameters:
!
!       * P        : electron pressure                           in SI
!       * MU       : chemical potential                          in SI
!       * K0       : compressibility (non-interacting) * n       in SI
!       * K        : compressibility * n                         in SI
!       * BM       : bulk modulus                                in SI
!       * U_IN     : internal energy per electron                in SI
!       * U_EX     : excess internal energy per electron / k_B T in SI
!       * F_FR     : Helmholtz free energy per electron          in SI
!
!  Note: We write the ground-state energy E = E_0 + E_X + E_C as 
!
!                        E = E_HF + E_C
!
!      and use eq. (2.56) of ref. (1) to compute the derivatives of E_HF
!
!      E_HF    =       Ad / rs^2  -     Bd / rs
!      D_EHF_1 =  -2 * Ad / rs^3  +     Bd / rs^2 
!      D_EHF_2 =   6 * Ad / rs^4  - 2 * Bd / rs^3 
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,         ONLY : ONE,TWO,THREE,FOUR,SIX, & 
                                       HALF,THIRD,FOURTH
      USE CONSTANTS_P1,         ONLY : K_B
      USE CONSTANTS_P2,         ONLY : HARTREE
      USE PI_ETC,               ONLY : PI_INV
      USE UTILITIES_1,          ONLY : ALFA
      USE PLASMA_SCALE
      USE CORRELATION_ENERGIES 
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 6)   ::  EC_TYPE
!
      REAL (WP)             ::  RS,T
      REAL (WP)             ::  P,MU,K0,K,U_IN,U_EX,F_FR
      REAL (WP)             ::  C_3D,E_C,D_EC_1,D_EC_2,E_HF,D_EHF_1,D_EHF_2
      REAL (WP)             ::  ALPHA,A3,B3,RS2,RS3,RS4,BM
      REAL (WP)             ::  NONID,DEGEN
      REAL (WP)             ::  A0,B0,C0,D0,G4
!
      ALPHA=ALFA('3D')                                              !
      A3=0.6E0_WP/ALPHA                                             ! ref. (1) table (2.1)
      B3=1.5E0_WP*PI_INV/ALPHA                                      ! idem
!
      RS2=RS*RS                                                     !
      RS3=RS2*RS                                                    !
      RS4=RS3*RS                                                    !
!
!  Computing the plasma scale parameters
!
      CALL PLASMON_SCALE(RS,T,ONE,NONID,DEGEN)                      !
!
      G4=NONID**(FOURTH)                                            ! 
!
!  Slattery-Doolen-DeWitt parameters                                ! ref (3) eq. (2.12)
!
      A0=-0.89752E0_WP                                              ! 
      B0= 0.94544E0_WP                                              ! 1 < NONID < 160
      C0= 0.17954E0_WP                                              !
      D0=-0.80049E0_WP                                              !
!
!  Computing the correlation energy and its derivatives
!
      CALL DERIVE_EC_3D(EC_TYPE,1,5,RS,T,D_EC_1,D_EC_2)             !
      E_C=EC_3D(EC_TYPE,1,RS,T)*HALF*HARTREE                        ! 
!
!  Computation of the derivatives of E_HF
!
      E_HF   =          A3/RS2 -       B3/RS                        ! 
      D_EHF_1=-     TWO*A3/RS3 +       B3/RS2                       !
      D_EHF_2=-     SIX*A3/RS4 -   TWO*B3/RS3                       !
!
      P=-THIRD*RS*(D_EHF_1+D_EC_1) * HALF*HARTREE                   ! ref. (1) eq. (1.137)
      MU=E_HF+E_C + P                                               ! ref. (1) eq. (1.138)
      K0=1.5E0_WP*ALPHA*ALPHA*RS2                                   ! ref. (2) eq. (2.17)
      BM=THIRD*RS*(      THIRD*RS*(D_EHF_2+D_EC_2) -             &  !
                     TWO*THIRD*RS*(D_EHF_1+D_EC_1)               &  ! ref. (1) eq. (1.139)
                  ) * HALF*HARTREE                                  !
      K=ONE/BM                                                      ! ref. (1) eq. (1.140)
!
      U_IN=K_B*T*(A0*NONID + B0*G4 +                             &  ! ref. (3) eq. (2.8) 
                      C0/G4 +D0 + 1.5E0_WP)                         ! and  (2.11)
      U_EX=A0*NONID + B0*G4 + C0/G4 + D0                            ! ref. (3) eq. (2.11)
      F_FR=K_B*T*(A0*NONID + FOUR*(B0*G4 - C0/G4) +              &  !
                    (D0+THREE)*DLOG(NONID) -                     &  ! ref. (3) eq. (2.14)  
                    (A0+FOUR*B0-FOUR*C0+1.135E0_WP))                ! 
!
      END SUBROUTINE THERMODYNAMICS_3D  
!
END MODULE THERMODYNAMIC_QUANTITIES 
