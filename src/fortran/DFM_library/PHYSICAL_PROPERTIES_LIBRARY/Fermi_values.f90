!
!=======================================================================
!
MODULE FERMI_SI
!
!  This module defines the Fermi level physical quantities 
!
!
!                    --> SI version  <--
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  KF_SI,EF_SI,VF_SI,TF_SI,NF_SI
!
END MODULE FERMI_SI
!
!=======================================================================
!
MODULE FERMI_SI_M
!
!  This module defines the Fermi level physical quantities 
!    in the presence of an external magnetic field
!
!
!                    --> SI version  <--
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  KF_SI_M,EF_SI_M,VF_SI_M,TF_SI_M
!
END MODULE FERMI_SI_M
!
!=======================================================================
!
MODULE FERMI_AU
!
!  This module defines the Fermi level physical quantities
!
!
!                    --> AU version  <--
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  KF_AU,EF_AU,VF_AU
!
END MODULE FERMI_AU
!
!=======================================================================
!
MODULE FERMI_AU_M
!
!  This module defines the Fermi level physical quantities
!    in the presence of an external magnetic field
!
!
!                    --> AU version  <--
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  KF_AU_M,EF_AU_M,VF_AU_M
!
END MODULE FERMI_AU_M
