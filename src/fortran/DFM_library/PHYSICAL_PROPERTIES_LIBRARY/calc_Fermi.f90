!
!=======================================================================
!
MODULE FERMI_VALUES
!
!  This module computes the Fermi level physical quantities
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_FERMI(DMN,RS)
!
!  This subroutine computes Fermi level quantities
!
!
!       * DMN      : dimension    
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 27 Jul 2020
!
      USE REAL_NUMBERS,  ONLY : ONE
      USE ENE_CHANGE,    ONLY : EV,RYD
      USE CONSTANTS_P1,  ONLY : BOHR,H_BAR,M_E,K_B
      USE REAL_NUMBERS,  ONLY : TWO,HALF
      USE PI_ETC,        ONLY : PI,PI2
      USE UTILITIES_1,   ONLY : ALFA
!
      USE FERMI_SI
      USE FERMI_AU
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP), INTENT(IN) ::  RS
      REAL (WP)             ::  KF,EF
      REAL (WP)             ::  NUM,DEN
!
      KF = ONE / (ALFA(DMN) * RS)                                   ! a_0 * k_F
      EF = KF * KF * RYD                                            ! Fermi energy in eV
!
      KF_SI = KF / BOHR                                             ! Fermi momentum in 1/m
      EF_SI = EF * EV                                               ! Fermi energy in J
      VF_SI = H_BAR * KF_SI / M_E                                   ! Fermi velocity in m/s 
      TF_SI = EF_SI / K_B                                           ! Fermi temperature in K
!
!  Computation of n(E_F)
!
      IF(DMN == '3D') THEN                                          !
        NUM = M_E * KF_SI                                           !
        DEN = H_BAR * H_BAR * PI2                                   !
      ELSE IF(DMN == '2D') THEN                                     !
        NUM = M_E                                                   !
        DEN = H_BAR * H_BAR * PI                                    !
      ELSE IF(DMN == '1D') THEN                                     !
        NUM = TWO * M_E                                             !
        DEN = H_BAR  *H_BAR * PI * KF_SI                            !
      END IF                                                        !
      NF_SI = NUM / DEN                                             !
!
      KF_AU = KF                                                    ! Fermi momentum in AU
      EF_AU = HALF * KF * KF                                        ! Fermi energy in AU
      VF_AU = KF                                                    ! Fermi velocity in AU
!
      END SUBROUTINE CALC_FERMI
!
!=======================================================================
!
END MODULE FERMI_VALUES
!
!=======================================================================
!
MODULE FERMI_VALUES_M
!
!  This module computes the Fermi level physical quantities
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_FERMI_M(DMN,RS)
!
!  This subroutine computes Fermi level quantities 
!    in the presence of an external magnetic field
!
!
!       * DMN      : dimension    
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 22 Jun 2020
!
      USE EXT_FIELDS,    ONLY : T,H
      USE REAL_NUMBERS,  ONLY : HALF
      USE CONSTANTS_P1,  ONLY : BOHR,H_BAR,M_E,K_B
      USE CONSTANTS_P3,  ONLY : MU_B
!
      USE FERMI_SI_M
      USE FERMI_AU_M
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP)   ::   RS
      REAL (WP)   ::   A 
!
      A=MU_B*H                                                      !  --> check B or H 
!
!  Computing k_F in AU
!
      IF(DMN == '3D') THEN                                          !
        KF_AU_M=KF_M_3D(RS,T,A)                                     !
      ELSE IF(DMN == '2D') THEN                                     !
        KF_AU_M=KF_M_2D(RS,T,A)                                     !
      ELSE IF(DMN == '1D') THEN                                     !
        CONTINUE                                                    !
      END IF                                                        !
!
      EF_AU_M=HALF*KF_AU_M*KF_AU_M                                  !
      VF_AU_M=KF_AU_M                                               !
!
      KF_SI_M=KF_AU_M/BOHR                                          !
      EF_SI_M=HALF*H_BAR*H_BAR*KF_SI_M*KF_SI_M/M_E                  !
      VF_SI_M=H_BAR*KF_SI_M/M_E                                     !
      TF_SI_M=EF_SI_M/K_B                                           !
!
      END SUBROUTINE CALC_FERMI_M
!
!=======================================================================
!
      FUNCTION KF_M_3D(RS,T,A) 
!
!  This function computes the temperature-dependent Fermi wave vector 
!    in the presence of an external magnetic field for 3D systems
!
!  References: (7) A. Isihara and D. Y. Kojima, Phys. Rev. B 10, 
!                     4925-4931 (1974)
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature in SI
!       * A        : sqrt(mu_B * B) : magnitude of the magnetic field
!
!
!  Output parameters:
!
!       * KF_M_3D   : Fermi wave number               in AU
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,  ONLY : ONE,FIVE,THIRD,FOURTH,SMALL
      USE CONSTANTS_P1,  ONLY : K_B
      USE FERMI_AU,      ONLY : KF_AU
      USE PI_ETC,        ONLY : PI2
      USE G_FACTORS,     ONLY : G_E
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS,T,A
      REAL (WP)             ::  KF_M_3D
      REAL (WP)             ::  BETA,ETA0,LNE0,KFT,KFM,RS2,LRS
      REAL (WP)             ::  A4
      REAL (WP)             ::  KG1,KG2,KG3
!
      A4=A*A*A*A                                                    !
!
      BETA=ONE/(K_B*T)                                              !
      ETA0=BETA*KF_AU*KF_AU                                         !
      LNE0=DLOG(ETA0)                                               !
!
      RS2=RS*RS                                                     !
      LRS=DLOG(RS)                                                  !
!
      KG1=FOURTH*G_E*G_E                                            !
      KG2=KG1 - THIRD                                               !
      KG3=KG1 - FIVE/18.0E0_WP                                      !
!
!  No magnetic field contribution
!
      KFT=KF_AU*(ONE - 0.16586E0_WP*RS + RS2*(                    & !
             0.0084411E0_WP*LRS - 0.027620E0_WP ) -               & !
             PI2/(24.0E0_WP*ETA0*ETA0)* (                         & !
                  ONE - 0.16586E0_WP*RS + RS2*(                   & !
                  0.20808E0_WP + 0.022197E0_WP*LRS -              & ! ref. (7) eq. (5.1)
                  0.00072406E0_WP*LNE0 - 0.027510E0_WP*LNE0*LNE0  & !
                                              )                   & !
                                        )                         & !
                )                                                   !
!
!  Magnetic field contribution
!
      KFM=-A4/(8.0E0_WP*KF_AU*KF_AU*KF_AU) * (                    & !
               KG2 + 0.16586E0_WP*RS*KG3 + RS2 *   (              & !
               0.054782E0_WP*KG1 - 0.018387E0_WP +                & !
               (0.033135E0_WP - 0.072955E0_WP*KG1)*LRS) +         & !
               PI2/(24.0E0_WP*ETA0*ETA0)* (                       & !
               7.0E0_WP*KG2 + 0.16586E0_WP*(1.6462E0_WP-KG1)*RS + & !
               RS2* ( 1.2783E0_WP - 4.9163E0_WP*KG1 +             & ! ref. (7) eq. (5.3)
               (1.25192E0_WP-0.40312E0_WP*KG1)*LRS +              & !
               (0.0016895E0_WP-0.0050178E0_WP*KG1)*LNE0 +         & !
               (0.064190E0_WP-0.19257E0_WP*KG1)*LNE0*LNE0         & !
                    )                                             & !
                                          )                       & !
                                             )                      !
!
      IF(A > SMALL) THEN                                            !
        KF_M_3D=KFT+KFM                                             !
      ELSE                                                          !
        KF_M_3D=KFT                                                 !
      END IF                                                        !
!
      END FUNCTION KF_M_3D
!
!=======================================================================
!
      FUNCTION KF_M_2D(RS,T,A) 
!
!  This function computes the temperature-dependent Fermi wave vector 
!    in the presence of an external magnetic field for 3D systems
!
!  References: (2) A. Isihara and T. Toyoda, Phys. Rev. B 19,
!                     831-845 (1979)
!              (3) A. Isihara and D. Y. Kojima, Phys. Rev. B 19,
!                     846-855 (1979)
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature in SI
!       * A        : sqrt(mu_B * B) : magnitude of the magnetic field
!       * FLD      : strength of the field
!                       FLD = 'WF' weak field          --> ref. (2)
!                       FLD = 'IF' intermediate field  --> ref. (3)
!
!
!  Output parameters:
!
!       * KF_M_2D   : Fermi wave number               in AU
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,  ONLY : ZERO,ONE,FOUR,HALF,THIRD,SMALL
      USE CONSTANTS_P1,  ONLY : E,K_B
      USE FERMI_AU,      ONLY : KF_AU
      USE PI_ETC,        ONLY : PI,PI2
      USE G_FACTORS,     ONLY : G_E
      USE EXT_FIELDS,    ONLY : FLD
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS,T,A
      REAL (WP)             ::  KF_M_2D
      REAL (WP)             ::  BETA,KFT,KFM,RS2,LRS
      REAL (WP)             ::  S0,ETA0,GAM0
      REAL (WP)             ::  ALPHA
      REAL (WP)             ::  A4,LS1,LS2
      REAL (WP)             ::  G2,XL,X1,X2,X3,SI,I
!
      INTEGER               ::  L,LMAX
!
      LMAX=100                                                      ! max. value of l-sums
!
      G2=G_E*G_E                                                    !
!
      RS2=RS*RS                                                     !
      LRS=DLOG(RS)                                                  !
!
      A4=A*A*A*A                                                    !
      ALPHA=A4/(RS2*RS2*KF_AU*KF_AU*KF_AU*KF_AU)                    !
!
      BETA=ONE/(K_B*T)                                              !
      ETA0=BETA*KF_AU*KF_AU                                         !
      S0=E*E/KF_AU                                                  !
      GAM0=A*A/(KF_AU*KF_AU )                                       !
!
      IF(FLD == 'WF') THEN                                          !
!
!  No magnetic field contribution
!
        KFT=KF_AU*(ONE - 0.4501E0_WP*RS - 0.1427E0_WP*RS2)          ! ref. (2) eq. (7.4)
!
!  Magnetic field contribution
!
        KFM=KF_AU*ALPHA*RS2*RS2*(                                &  !
                  (1.407E-2_WP*G2 - 3.997E-3_WP)*RS -            &  !
                   4.689E-3_WP*RS*LRS +                          &  !
                  (3.372E-3_WP*G2 + 2.287E-2_WP)*RS2 -           &  !
                   6.333E-3_WP*RS*LRS                            &  !
                                )                                   !
!
        IF(A > SMALL) THEN                                          !
          KF_M_2D=KFT+KFM                                           !
        ELSE
          KF_M_2D=KFT                                               !
        END IF                                                      !
!
      ELSE IF(FLD == 'IF') THEN                                     !
!
        I=0.8149E0_WP                                               ! ref. (3) eq. (5.19)
!
!  Calculation of l-sums
!
        SI=-ONE                                                     ! init. of sign
        LS1=ZERO                                                    !
        LS2=ZERO                                                    !
        DO L=1,LMAX                                                 !
          XL=DFLOAT(L)                                              !
          X1=XL*PI/GAM0                                             !
          X2=HALF*G_E*XL*PI                                         !
          X3=XL*PI2/ALPHA                                           !
          SI=-SI                                                    ! (-1)^(l+1)
          LS1=LS1+SI*DSIN(X1)*DCOS(X2)/DSINH(X3)                    !
          LS2=LS2+SI*DSIN(X1)*DCOS(X2)/DSIN(X3)                     !
        END DO                                                      !
!
        KF_M_2D=KF_AU*(ONE + PI*LS1/ETA0 -                        & !
                         THIRD*I*(S0**(FOUR*THIRD))*              & ! ref. (3) eq. (6.1)
                         (ONE-PI*LS2/ETA0)                        & !
                      )                                             !
!
      END IF                                                        !
!
      END FUNCTION KF_M_2D
!
END MODULE FERMI_VALUES_M
 
