!
!=======================================================================
!
MODULE PAIR_CORRELATION
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE PAIR_CORRELATION_3D(R,RS,T,GR_TYPE,RH_TYPE,GR)
!
!  This subroutine computes the pair correlation function g(r)
!    for 3D systems.
!
!
!  Input parameters:
!
!       * R        : grid point          (in units of a_0)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * GR_TYPE  : structure factor approximation (3D)
!                       GR_TYPE  = 'CDF' from chain diagram formula of PDF (long distance)
!                       GR_TYPE  = 'DHA' Debye-Hückel approximation 
!                       GR_TYPE  = 'DWA' DeWitt approximation 
!                       GR_TYPE  = 'FBA' Frieman-Book approximation 
!                       GR_TYPE  = 'HFA' Hartree-Fock approximation (only exchange)
!                       GR_TYPE  = 'HUB' Hubbard approximation
!                       GR_TYPE  = 'LLA' Lee-Long approximation
!                       GR_TYPE  = 'ORB' Ortiz-Ballone approximation
!                       GR_TYPE  = 'PDF' from pair distribution function 
!                       GR_TYPE  = 'SHA' Shaw approximation
!                       GR_TYPE  = 'WIG' Wigner approximation
!       * RH_TYPE  : choice of pair distribution function rho_2(r) (3D)
!
!  Output parameters:
!
!       * GR       : value of the pair correlation function
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 20 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE
      USE FERMI_AU,         ONLY : KF_AU
      USE PI_ETC,           ONLY : PI2
      USE UTILITIES_1,      ONLY : RS_TO_N0
      USE PAIR_DISTRIBUTION
!
      IMPLICIT NONE
!
      REAL (WP)             ::  R,RS,T,GR,R2,HFA,I2
      REAL (WP)             ::  N0
!
      CHARACTER (LEN = 3)   ::  GR_TYPE,RH_TYPE
!
      N0=RS_TO_N0('3D',RS)                                          !
!
      IF(GR_TYPE == 'DHA') THEN                                     !
        GR = DHA_PCF(R,RS,T)                                        !
      ELSE IF(GR_TYPE == 'DWA') THEN                                !
        GR = DWA_PCF(R,RS,T)                                        !
      ELSE IF(GR_TYPE == 'FBA') THEN                                !
        GR = FBA_PCF(R,RS,T)                                        !
      ELSE IF(GR_TYPE == 'HFA') THEN                                !
        GR = HFA_PCF(R)                                             !
      ELSE IF(GR_TYPE == 'HUB') THEN                                !
        GR = HUB_PCF(R)                                             !
      ELSE IF(GR_TYPE == 'LLA') THEN                                !
        GR = LLA_PCF(R)                                             !
      ELSE IF(GR_TYPE == 'SHA') THEN                                !
        GR = SHA_PCF(R)                                             !
      ELSE IF(GR_TYPE == 'ORB') THEN                                !
        GR = ORB_PCF(R)                                             !
      ELSE IF(GR_TYPE == 'PDF') THEN                                !
        CALL PAIR_DISTRIBUTION_3D(R,RS,T,RH_TYPE,R2)                !
        GR = R2 / (N0 * N0)                                         !
      ELSE IF(GR_TYPE == 'CDF') THEN                                !
        CALL PAIR_DISTRIBUTION_3D(R,RS,T,RH_TYPE,R2)                ! 
        HFA = HFA_PCF(R)                                            !
        I2  = (ONE-HFA)*KF_AU*KF_AU*KF_AU/(18.0E0_WP*PI2*PI2)       ! ref. (1) RH_TYPE
        GR  = (R2-N0*N0+I2)/(N0*N0)                                 ! eq. (4.1.1)
      ELSE IF(GR_TYPE == 'WIG') THEN                                !
        GR = WIG_PCF(R)                                             !
      END IF                                                        !
!
      END SUBROUTINE PAIR_CORRELATION_3D  
!
!=======================================================================
!
      FUNCTION DHA_PCF(R,RS,T)
!
!  This function computes Debye-Hückel pair correlation function g(r) 
!    for 3D systems
!
!  References: (1) H. E. DeWitt, Phys. Rev. 140, A466-A470 (1965)
!
!  Input parameters:
!
!       * R        : grid point                            in unit of a_0
!       * T        : temperature in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  4 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE
      USE CONSTANTS_P1,     ONLY : BOHR,E,COULOMB,K_B
      USE SCREENING_VEC,    ONLY : DEBYE_VECTOR
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  R,RS,T
      REAL (WP)             ::  DHA_PCF
      REAL (WP)             ::  BETA,KD_AU,X,US
      REAL (WP)             ::  KD_SI
!
      REAL (WP)             ::  EXP
!
!  Computing the Debye momentum
!
      CALL DEBYE_VECTOR('3D',T,RS,KD_SI)                            !
!
      BETA  = ONE / K_B * T                                         !
      KD_AU = KD_SI * BOHR                                          !
      X     = R * KD_AU                                             !
      US    = COULOMB * E * E * EXP(- X) / R                        !
!
      DHA_PCF = ONE - BETA * US                                     ! ref. (1) eq. (1)
!
      END FUNCTION DHA_PCF  
!
!=======================================================================
!
      FUNCTION DWA_PCF(R,RS,T)
!
!  This function computes DeWitt pair correlation function g(r) 
!    for 3D systems
!
!  References: (1) H. E. DeWitt, Phys. Rev. 140, A466-A470 (1965)
!
!  Input parameters:
!
!       * R        : grid point                            in unit of a_0
!       * T        : temperature in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 13 Oct 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,FOUR,EIGHT, &
                                   HALF,THIRD
      USE CONSTANTS_P1,     ONLY : BOHR,E,COULOMB,K_B
      USE EULER_CONST,      ONLY : EUMAS
      USE SCREENING_VEC,    ONLY : DEBYE_VECTOR
      USE EXT_FUNCTIONS,    ONLY : DEI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  R,RS,T
      REAL (WP)             ::  DWA_PCF
      REAL (WP)             ::  BETA,KD_AU,X,US
      REAL (WP)             ::  G2B,G22B,G22C,GAMMA,GAM
      REAL (WP)             ::  KD_SI
!
      REAL (WP)             ::  EXP,SINH,LOG
!
!  Computing the Debye momentum
!
      CALL DEBYE_VECTOR('3D',T,RS,KD_SI)                            !
!
      BETA  = ONE / K_B * T                                         !
      KD_AU = KD_SI * BOHR                                          !
      X     = R * KD_AU                                             !
      US    = COULOMB * E * E * DEXP(- X) / R                       !
      GAMMA = BETA * COULOMB * E * E * KD_SI                        !
      GAM   = EUMAS
!
      IF(X < ONE) THEN                                              !
        G22B = - GAMMA * GAMMA * SINH(X) * LOG(THIRD / (GAM*X)) / X !  --> eq. (23)
        G22C =   HALF * GAMMA * GAMMA * (HALF * THIRD +           & !  --> eq. (27) 
                 X / (EIGHT * LOG(ONE / (THREE * GAM * X))))        !
      ELSE
        G22B = - HALF * GAMMA * GAMMA * (LOG(THREE) * EXP(-X) / X & !  --> eq. (24)
                        - TWO * THIRD*  EXP(- TWO * X) / (X * X))   !
        G22C =   HALF * GAMMA * GAMMA / (FOUR * X) * (            & !   
                   (ONE + X) * EXP(- X) * LOG(THREE)          -   & !
                   FOUR * THIRD * (EXP(- X) - EXP(- TWO * X)) +   & !  --> eq. (26)
                   (ONE + X) * EXP(- X) * DEI(- X)            -   & !
                   (ONE - X) * EXP(X) * DEI(- THREE * X)          & !
                                                     )              !
      ENDIF                                                         !
!
      G2B = G22B                                                    !
!
      DWA_PCF = EXP(- BETA * US) * (ONE + G2B + G22C)               ! ref. (1) eq. (29)
!
      END FUNCTION DWA_PCF  
!
!=======================================================================
!
      FUNCTION FBA_PCF(R,RS,T)
!
!  This function computes Frieman-Book pair correlation function g(r) 
!    for 3D systems
!
!  References: (1) H. E. DeWitt, Phys. Rev. 140, A466-A470 (1965)
!
!  Input parameters:
!
!       * R        : grid point                            in unit of a_0
!       * T        : temperature in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  4 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE
      USE CONSTANTS_P1,     ONLY : BOHR,E,COULOMB,K_B
      USE SCREENING_VEC,    ONLY : DEBYE_VECTOR
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  R,RS,T
      REAL (WP)             ::  FBA_PCF
      REAL (WP)             ::  BETA,KD_AU,X,US
      REAL (WP)             ::  KD_SI
!
      REAL (WP)             ::  EXP
!
!  Computing the Debye momentum
!
      CALL DEBYE_VECTOR('3D',T,RS,KD_SI)                            !
!
      BETA  = ONE / K_B * T                                         !
      KD_AU = KD_SI * BOHR                                          !
      X     = R * KD_AU                                             !
      US    = COULOMB * E * E * EXP(- X) / R                        !
!
      FBA_PCF = EXP(- BETA * US)                                    ! ref. (1) eq. (4)
!
      END FUNCTION FBA_PCF  
!
!=======================================================================
!
      FUNCTION HFA_PCF(R)
!
!  This function computes Hartree-Fock pair correlation function g(r) 
!    for 3D systems
!
!  References: (1) F. Brouers, Phys. Stat. Sol. 19, 867-871 (1967)
!
!  Input parameters:
!
!       * R        : grid point                            in unit of a_0
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  4 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE
      USE FERMI_AU,         ONLY : KF_AU
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  R
      REAL (WP)             ::  HFA_PCF
      REAL (WP)             ::  KR1,KR3
!
      REAL (WP)             ::  SIN,COS
!
      KR1 = KF_AU * R                                               !
      KR3 = KR1 * KR1 * KR1                                         !
!
      HFA_PCF = ONE - 4.5E0_WP * ( (SIN(KR1) - KR1 * COS(KR1)) /  & !
                                    KR3 )**2                        ! ref. (1) eq. (5)
!
      END FUNCTION HFA_PCF  
!
!=======================================================================
!
      FUNCTION HUB_PCF(R)
!
!  This function computes Shaw pair correlation function g(r) 
!    for 3D systems
!
!  References: (1) R. W. Shaw, J. Phys. C: Solid State Phys. 3, 
!                     1140-1158 (1970)
!
!  Input parameters:
!
!       * R        : grid point                            in unit of a_0
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Nov 2020
!
!
      USE REAL_NUMBERS,       ONLY : ONE,TWO,THREE,FOUR,HALF
      USE PI_ETC,             ONLY : PI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  R
      REAL (WP)             ::  HUB_PCF
      REAL (WP)             ::  AL2,AL
!
      REAL (WP)             ::  SQRT,EXP
!   
      AL2   = FOUR / SQRT(THREE * PI)                               !
      AL    = SQRT(AL2)                                             !
!                                    
      HUB_PCF = ONE - HALF * (ONE + AL * R) * EXP(- AL * R)         ! ref. (1) eq. (5.2)
!
      END FUNCTION HUB_PCF  
!
!=======================================================================
!
      FUNCTION LLA_PCF(R)
!
!  This function computes Lee-Long pair correlation function g(r) 
!    for 3D systems
!
!  References: (1) H. Lee and M. Long, Phys. Rev. B 52, 189-195 (1995)
!
!  Input parameters:
!
!       * R        : grid point                            in unit of a_0
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Nov 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,THREE,HALF
      USE FERMI_AU,         ONLY : KF_AU
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  R
      REAL (WP)             ::  LLA_PCF
      REAL (WP)             ::  X,X2
      REAL (WP)             ::  J1
!
      REAL (WP)             ::  SQRT,SIN,COS
!
      X  = KF_AU * R                                                !
      X2 = X * X                                                    !
!
!  Computation of the Bessel function:
!
!     J_{3/2}(x) = sqrt(2x/pi} j_1(x)
!
      J1 =  SIN(X) / X2 - COS(X) / X                                ! j_1(x)
!
      LLA_PCF = ONE - HALF * (THREE * J1 / X)**2                    ! ref. 1 eq. (11)
!
      END FUNCTION LLA_PCF  
!
!=======================================================================
!
      FUNCTION ORB_PCF(R)
!
!  This function computes Ortiz-Ballone parametrization of the 
!    pair correlation function g(r) for 3D systems
!
!  References: (1) G. Ortiz and P. Ballone, Phys. Rev. B 50, 
!                     1391-1405 (1994)
!
!  Input parameters:
!
!       * R        : grid point                            in unit of a_0
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 20 Sep 2020
!
!
      USE MATERIAL_PROP,    ONLY : RS
      USE REAL_NUMBERS,     ONLY : ONE
      USE INTERPOLATION,    ONLY : LAG_4P_INTERP
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  R
      REAL (WP)             ::  ORB_PCF
      REAL (WP)             ::  U,U2,U3,U4,U5
      REAL (WP)             ::  AA,BB,CC,DD,EE,FF
      REAL (WP)             ::  NNU
      REAL (WP)             ::  A(4),B(4),C(4),D(4),E(4),F(4)
      REAL (WP)             ::  NU(4),X(4)
!
      REAL (WP)             ::  EXP
!
      DATA X    /  1.0000E0_WP,  3.0000E0_WP,  5.0000E0_WP, 10.0000E0_WP / ! 
      DATA A    / -1.0000E0_WP, -1.0000E0_WP, -1.0000E0_WP, -1.0000E0_WP / !
      DATA B    /  0.0000E0_WP,  0.0000E0_WP,  0.0000E0_WP,  0.0000E0_WP / ! ref. (1)
      DATA C    /  1.0567E0_WP,  0.7512E0_WP,  0.6293E0_WP,  0.5442E0_WP / ! table IX:
      DATA D    / -0.8804E0_WP, -0.3827E0_WP, -0.2161E0_WP, -0.1142E0_WP / !
      DATA E    /  0.3029E0_WP,  0.0711E0_WP,  0.0064E0_WP, -0.0313E0_WP / ! case ++
      DATA F    / -0.0383E0_WP, -0.0047E0_WP,  0.0026E0_WP,  0.0068E0_WP / !
      DATA NU   /  0.3808E0_WP,  0.2177E0_WP,  0.1587E0_WP,  0.1711E0_WP / !
!
      U  = R / RS                                                   !
      U2 = U  * U                                                   !
      U3 = U2 * U                                                   !
      U4 = U3 * U                                                   !
      U5 = U4 * U                                                   !
!
!  Calculation the coefficients for the excat RS
!
      AA  = LAG_4P_INTERP(X,A,RS)                                   !
      BB  = LAG_4P_INTERP(X,B,RS)                                   !
      CC  = LAG_4P_INTERP(X,C,RS)                                   !
      DD  = LAG_4P_INTERP(X,D,RS)                                   !
      EE  = LAG_4P_INTERP(X,E,RS)                                   !
      FF  = LAG_4P_INTERP(X,F,RS)                                   !
      NNU = LAG_4P_INTERP(X,NU,RS)                                  !
!
      ORB_PCF = ONE + ( AA + BB * U  + CC * U2 + DD * U3 +        & ! Ref. (1) eq. 36)
                             EE * U4 + FF * U5 ) * EXP(- NNU * U2)  !
!
      END FUNCTION ORB_PCF
!
!=======================================================================
!
      FUNCTION SHA_PCF(R)
!
!  This function computes Shaw pair correlation function g(r) 
!    for 3D systems
!
!  References: (1) R. W. Shaw, J. Phys. C: Solid State Phys. 3, 
!                     1140-1158 (1970)
!
!  Input parameters:
!
!       * R        : grid point                            in unit of a_0
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Nov 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,NINE,THIRD
      USE PI_ETC,           ONLY : PI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  R
      REAL (WP)             ::  SHA_PCF
      REAL (WP)             ::  AL2,R2
!
      REAL (WP)             ::  EXP
!
      R2 = R * R                                                    !
!
      AL2 = ONE / (NINE * PI)**THIRD                                !
!
      SHA_PCF = ONE - EXP(- AL2 * R2)                               ! ref. (1) eq. (5.5)
!
      END FUNCTION SHA_PCF   
!
!=======================================================================
!
      FUNCTION WIG_PCF(R)
!
!  This function computes Wigner pair correlation function g(r) 
!    for 3D systems
!
!  References: (1) E. Wigner, Phys. Rev. 46, 1002-1011 (1934)
!
!  Input parameters:
!
!       * R        : grid point                            in unit of a_0
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 20 Sep 2020
!
!
      USE MATERIAL_PROP,    ONLY : RS
      USE REAL_NUMBERS,     ONLY : ONE
      USE UTILITIES_1,      ONLY : ALFA
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  R
      REAL (WP)             ::  WIG_PCF
      REAL (WP)             ::  DD,ROD
!
      REAL (WP)             ::  EXP
!
      DD = ALFA('3D') * RS                                          ! ref. (1) eq. (6)
!
      ROD = R / DD                                                  !
!
      WIG_PCF = ONE - EXP(-1.6E0_WP * ROD) *                      & !
                ( ONE + 1.6E0_WP * ROD + 1.2E0_WP * ROD * ROD )     ! ref. (1) eq. (8)
!
      END FUNCTION WIG_PCF
!
END MODULE PAIR_CORRELATION
