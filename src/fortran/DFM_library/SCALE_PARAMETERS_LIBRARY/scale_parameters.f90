!
!=======================================================================
!
MODULE SCALE_PARAMETERS 
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE SCALE_PARAM(R_S,T,G_Q,G_C,R_W)
!
!  This subroutine computes different scale parameters: 
!      - the quantum scale parameter g_Q 
!      - the classical scale parameter g_C
!      - the Wilson ratio R_W
!
!
!
!  Input parameters:
!
!       * R_S      : dimensionless electron Wigner-Seitz radius
!       * T        : temperature in Kelvin
!
!
!  Output variables :
!
!       * G_Q      : quantum scale parameter
!       * G_C      : classical scale parameter
!       * R_W      : Wilson ratio
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  4 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : TWO,FOUR,NINE, & 
                                   THIRD
      USE CONSTANTS_P1,     ONLY : BOHR,H_BAR,M_E,K_B
      USE G_FACTORS,        ONLY : G_E
      USE PI_ETC,           ONLY : PI2
!
      IMPLICIT NONE
!
      REAL (WP)             :: R_S,T
      REAL (WP)             :: G_Q,G_C,R_W
      REAL (WP)             :: COEF1,COEF2,NUM,DENOM
!
      COEF1=TWO*(16.0E0_WP/(NINE*PI2))**THIRD                       !
      COEF2=(48.0E0_WP*PI2)**THIRD                                  !
!
!  Quantum scale parameter
!
      G_Q=COEF1*R_S                                                 !
!
!  Classical scale parameter
!
      NUM=H_BAR*H_BAR*COEF2                                         !
      DENOM=M_E*BOHR*BOHR*K_B*T                                     !
      G_C=(NUM/DENOM)/R_S                                           !
!
!  Wilson ratio (HEG value)
!
      R_W=FOUR/(G_E*G_E)                                            !
!
      END SUBROUTINE SCALE_PARAM  
!
END MODULE SCALE_PARAMETERS
