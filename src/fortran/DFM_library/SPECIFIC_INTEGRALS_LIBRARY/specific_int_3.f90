!
!=======================================================================
!
MODULE SPECIFIC_INT_3
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE INT_GRM1(NMAX,X_MAX,IN_MODE,RS,T,A,L,GR_TYPE,  &
                          RH_TYPE,IN) 
!
!  This subroutine computes several integrals involving (g(r)-1), 
!    where g(r) is the pair correlation function. 
!
!
!  Input parameters:
!
!       * NMAX     : dimensioning of the arrays
!       * X_MAX    : upper integration value
!       * IN_MODE  : type of integral computed (see below)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * A        : dimensionless screening vector / coefficient / X
!       * L        : power of X or 1/X
!       * GR_TYPE  : pair correlation approximation (3D)
!       * RH_TYPE  : choice of pair distribution function rho_2(r) (3D)
!
!
!  Output parameters:
!
!       * IN       : integral result
!
!
!
!
!                 / x_max                       
!                |              
!   IN   =       |        (g(X) - 1 ) dX                     :  IN_MODE = 1
!                |               
!               / 0                               
!
!
!                 / x_max                          
!                |                   
!   IN   =       |         X^L (g(X) - 1 ) dX                :  IN_MODE = 2
!                |                 
!               / 0                               
!
!
!                 / x_max                          
!                |          (g(X) - 1 )          
!   IN   =       |         ------------- dX                  :  IN_MODE = 3 
!                |              X^L    
!               / 0                               
!
!
!                 / x_max                         
!                |          2  sin(AX)        
!   IN   =       |         X  -------- (g(X) - 1 ) dX        :  IN_MODE = 4
!                |               AX      
!               / 0                               
!
!
!                 / x_max                         
!                |                     
!   IN   =       |           j (AX) (g(X) - 1 ) dX           :  IN_MODE = 5
!                |            1         
!               / 0                               
!
!
!                 / x_max                         
!                |          1         
!   IN   =       |         --- j (AX) (g(X) - 1 ) dX         :  IN_MODE = 6
!                |          X   2         
!               / 0                               
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 22 Oct 2020
!
!
      USE DIMENSION_CODE,           ONLY : NZ_MAX
      USE REAL_NUMBERS,             ONLY : ZERO,ONE,THREE
      USE PAIR_CORRELATION,         ONLY : PAIR_CORRELATION_3D
      USE INTEGRATION,              ONLY : INTEGR_L
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  GR_TYPE,RH_TYPE
!
      INTEGER, INTENT(IN)   ::  NMAX,IN_MODE,L
      INTEGER               ::  ID,IR,N1
! 
      REAL (WP), INTENT(IN) ::  X_MAX,RS,T,A
      REAL (WP), INTENT(OUT)::  IN
      REAL (WP)             ::  X_MIN,X_STEP,EPS
      REAL (WP)             ::  XA(NZ_MAX),INTF(NZ_MAX)
      REAL (WP)             ::  GR,XX,H,FXA
      REAL (WP)             ::  J0,J1,J2
!
      ID = 1                                                        !
!
      EPS = 1.0E-3_WP                                               !
!
      X_MIN  = EPS      
      X_STEP = (X_MAX -X_MIN)  / FLOAT(NMAX - 1)                    !
!
      N1 = NMAX                                                     ! index of upper bound
!
      DO IR = 1, NMAX                                               !
!
        XX     = X_MIN + FLOAT(IR - 1) * X_STEP                     ! x grid
        XA(IR) = XX                                                 !
!
!  Computing the pair correlation function g(r)
!
        CALL PAIR_CORRELATION_3D(XX,RS,T,GR_TYPE,RH_TYPE,GR)        !
!
!  Computing the integrand function
!
        IF(IN_MODE == 1) THEN                                       !
!
          INTF(IR) = GR - ONE                                       !
!
        ELSE IF(IN_MODE == 2) THEN                                  !
!
          INTF(IR) = XX**L * (GR - ONE)                             !                      
!
        ELSE IF(IN_MODE == 3) THEN                                  !
!
          INTF(IR) = (GR - ONE) / XX**L                             !                      
!
        ELSE IF(IN_MODE == 4) THEN                                  !
!
          J0 = SIN(A * XX) / (A * XX)                               !
!
          INTF(IR) = XX * XX * (GR - ONE) * J0                      !                      
!
        ELSE IF(IN_MODE == 5) THEN                                  !
!
          J1 = SIN(A * XX) / (A * XX)**2 - COS(A * XX) / (A * XX)   !
!
          INTF(IR) = (GR - ONE) * J1                                !
!
        ELSE IF(IN_MODE == 6) THEN                                  !
!
          J2 = (THREE - (A * XX)**2) * SIN(A * XX) / (A * XX)**3 - &!
                THREE * COS(A * XX) / (A * XX)**2                   !
!
          INTF(IR) = (GR - ONE) * J2 / XX                           !
!
        END IF                                                      !
! 
      END DO                                                        !
!
      H  = XA(2) - XA(1)                                            ! step
!
!  Computing the integral
!
      CALL INTEGR_L(INTF,H,NMAX,N1,IN,ID)                           !
!
      END SUBROUTINE INT_GRM1  
!
END MODULE SPECIFIC_INT_3
