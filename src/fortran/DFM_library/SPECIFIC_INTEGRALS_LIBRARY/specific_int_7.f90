!
!=======================================================================
!
MODULE SPECIFIC_INT_7
!
!  This module computes the Fermi-Dirac integrals
!
!
      USE ACCURACY_REAL
      USE ACCURACY_INTEGER
      USE MINMAX_VALUES
!
CONTAINS
!
!=======================================================================
!
      FUNCTION FD(ETA,NU)
!
!  This function return a Fermi-Dirac integral
!
!                     / + INF         nu
!                    |               x
!       F  (eta)  =  |        ------------------  dx
!        nu          |         exp(x - eta) + 1
!                   / 0
!
!
!  Input parameters:
!
!       * ETA      : parameter
!       * NU       : power of x
!
!
!  Output parameters:
!
!       * FD       : value of the Fermi-Dirac integral
!
!
!   Author :  D. Sébilleau
!
!
!                                           Last modified :  4 Nov 2020
!
!
      USE REAL_NUMBERS,             ONLY : ONE,SMALL,MIC
!
      USE INTEGRATION,              ONLY : INTEGR_L
!
      IMPLICIT NONE
!
      INTEGER (IW)            ::  IX
      INTEGER (IW)            ::  ID
!
      INTEGER (IW), PARAMETER ::  NX = 1000             ! number of integration points
!
      REAL (WP), INTENT(IN)   ::  ETA,NU
      REAL (WP)               ::  FD
      REAL (WP)               ::  X_MIN,X_STEP,X
      REAL (WP)               ::  IN
      REAL (WP)               ::  INTG(NX)
      REAL (WP)               ::  XMETA
      REAL (WP)               ::  MAX_EXP,MIN_EXP
!
      REAL (WP), PARAMETER    ::  X_MAX = 20.0E0_WP     ! upper bound for integration
!
      REAL (WP)               ::  FLOAT,EXP
!
!  Computing the max and min value of the exponent of e^x
!
      CALL MINMAX_EXP(MAX_EXP,MIN_EXP)                              !
!
      ID = 2                                                        !
!
      X_MIN  = SMALL                                                !
      X_STEP = (X_MAX - X_MIN) / FLOAT(NX - 1)                      !
!
!  Computing the integrand function                                 ! 
!
      DO IX = 1, NX                                                 !
!
        X  = X_MIN + FLOAT(IX - 1) * X_STEP                         !
!
        XMETA = X - ETA                                             !
        IF(XMETA > MIN_EXP) THEN                                    !
          INTG(IX) = X**NU / (ONE + EXP(X - ETA))                   !
        ELSE                                                        !
          INTG(IX) = X**NU                                          ! pathological case
        END IF                                                      !
!
      END DO                                                        !
!
!  Computing the integral
!
      CALL INTEGR_L(INTG,X_STEP,NX,NX,IN,ID)                        !
!
      FD = IN                                                       !
!
      END FUNCTION FD
!
END MODULE SPECIFIC_INT_7
