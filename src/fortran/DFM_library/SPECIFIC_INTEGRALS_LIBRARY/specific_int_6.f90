!
!=======================================================================
!
MODULE SPECIFIC_INT_6
!
!  This module computes Macke function and related integrals
!
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE MACKE_FUNC(Q,IQ) 
!
!  This subroutine computes Macke function
!
!
!  Reference: (1) P. Ziesche, Ann. Phys. (Berlin) 522, 739-765 (2010)
!
!
!  Input parameters:
!
!       * Q        : point q at which the function is computed
!
!
!  Output parameters:
!
!       * IQ       : Macke function at q
!
!
!   Author :  D. Sébilleau
!
!
!                                           Last modified :  9 Oct 2020
!
!
      USE DIMENSION_CODE,           ONLY : NZ_MAX
!
      USE REAL_NUMBERS,             ONLY : EIGHT,SMALL
      USE PI_ETC,                   ONLY : PI
!
      USE INTEGRATION,              ONLY : INTEGR_L
!
      IMPLICIT NONE
!
      INTEGER               ::  IU
      INTEGER               ::  ID
!
      INTEGER, PARAMETER    ::  NU = 200           ! number of integration points
!
      REAL (WP), INTENT(IN) ::  Q
      REAL (WP), INTENT(OUT)::  IQ
      REAL (WP)             ::  U_MIN,U_STEP
      REAL (WP)             ::  IN
      REAL (WP)             ::  RQ(NZ_MAX),RQ2(NZ_MAX)
!
      REAL (WP), PARAMETER  ::  U_MAX = 20.0E0_WP  ! upper bound for intergration
!
      REAL (WP)             ::  FLOAT,LOG,ATAN
!
      U_MIN  = SMALL                                                !
      U_STEP = (U_MAX - U_MIN) / FLOAT(NU - 1)                      !
!
!  Computing the R(q,u) function                                    ! ref. (1) eq. (C12)
!
      CALL RQU(Q,RQ)                                                !
!
!  Computing the integrand function                                 ! ref. (1) eq. (C12)
!
      DO IU = 1, NU                                                 !
!
        RQ2(IU) = RQ(IU) * RQ(IU)                                   !
!
      END DO                                                        !
!
!  Computing the integral
!
      CALL INTEGR_L(RQ2,U_STEP,NZ_MAX,NU,IN,ID)                     !
!
      IQ = EIGHT * PI * Q * IN                                      !
!
      END SUBROUTINE MACKE_FUNC
!
!=======================================================================
!
      SUBROUTINE RQU(Q,RQ)
!
!  This subroutine computes the R(q,u) function
!
!  Reference: (1) P. Ziesche, Ann. Phys. (Berlin) 522, 739-765 (2010)
!
!
!
!   Author :  D. Sébilleau
!
!
!                                           Last modified :  9 Oct 2020
!
!
      USE DIMENSION_CODE,           ONLY : NZ_MAX
!
      USE REAL_NUMBERS,             ONLY : ONE,HALF,FOURTH,SMALL
!
      IMPLICIT NONE
!
      INTEGER               ::  IU
!
      INTEGER, PARAMETER    ::  NU = 200           ! number of integration points
!
      REAL (WP), INTENT(IN) ::  Q
      REAL (WP)             ::  Q2,U_MIN,U_STEP,U,U2
      REAL (WP)             ::  RQ(NZ_MAX)
!
      REAL (WP), PARAMETER  ::  U_MAX = 20.0E0_WP  ! upper bound for intergration
!
      REAL (WP)             ::  FLOAT,LOG,ATAN
!
      Q2     = Q * Q                                                !
      U_MIN  = SMALL                                                !
      U_STEP = (U_MAX - U_MIN) / FLOAT(NU - 1)                      !
!
!  Computing the function R(q,u)                                    ! ref. (1) eq. (B1)
!
      DO IU = 1, NU                                                 !
!
        U  = U_MIN + FLOAT(IU - 1) * U_STEP                         !
        U2 = U * U                                                  !
!
        RQ(IU) = HALF * ( ONE + HALF *                            & !
                          (ONE + U2 - FOURTH * Q2) / Q    *       & !
                          LOG( ((HALF * Q + ONE)**2 + U2) /       & !
                               ((HALF * Q - ONE)**2 + U2)         & !
                             ) -                                  & !
                          U * ( ATAN((ONE + HALF * Q)/U) +        & !   
                                ATAN((ONE - HALF * Q)/U)          & !
                              )                                   & !
                        )                                           !
!
      END DO                                                        !
!
      END SUBROUTINE RQU
!
!=======================================================================
!
      SUBROUTINE R0U(R0)
!
!  This subroutine computes the R_0(u) function
!
!  Reference: (1) P. Ziesche, Ann. Phys. (Berlin) 522, 739-765 (2010)
!
!
!
!   Author :  D. Sébilleau
!
!
!                                           Last modified :  9 Oct 2020
!
!
      USE DIMENSION_CODE,           ONLY : NZ_MAX
!
      USE REAL_NUMBERS,             ONLY : ONE,SMALL
!
      IMPLICIT NONE
!
      INTEGER               ::  IU
!
      INTEGER, PARAMETER    ::  NU = 200           ! number of integration points
!
      REAL (WP), INTENT(OUT)::  R0(NZ_MAX)
      REAL (WP)             ::  U_MIN,U_STEP
      REAL (WP)             ::  U
!
      REAL (WP), PARAMETER  ::  U_MAX = 20.0E0_WP  ! upper bound for intergration
!
      REAL (WP)             ::  FLOAT,ATAN
!
      U_MIN  = SMALL                                                !
      U_STEP = (U_MAX - U_MIN) / FLOAT(NU - 1)                      !
!
!  Computing the function R(q,u)                                    ! ref. (1) eq. (B1)
!
      DO IU = 1, NU                                                 !
!
        U  = U_MIN + FLOAT(IU - 1) * U_STEP                         !
!
        R0(IU) = ONE - U * ATAN(ONE / U)                            !
!
      END DO                                                        !
!
      END SUBROUTINE R0U
!
!=======================================================================
!
      SUBROUTINE RPA_CONSTANTS(A,BPR)
!
!  This subroutine computes the main RPA constants
!
!  Reference: (1) P. Ziesche, Ann. Phys. (Berlin) 522, 739-765 (2010)
!
!
!
!   Author :  D. Sébilleau
!
!
!                                           Last modified :  9 Oct 2020
!
!
      USE DIMENSION_CODE,           ONLY : NZ_MAX
!
      USE REAL_NUMBERS,             ONLY : ONE,THREE,SMALL
      USE PI_ETC,                   ONLY : PI3
!
      USE INTEGRATION,              ONLY : INTEGR_L
!
      IMPLICIT NONE
!
      INTEGER               ::  IU
      INTEGER               ::  ID
!
      INTEGER, PARAMETER    ::  NU = 200           ! number of integration points
!
      REAL (WP), INTENT(OUT)::  A,BPR
      REAL (WP)             ::  U_MIN,U_STEP
      REAL (WP)             ::  R0(NZ_MAX)
      REAL (WP)             ::  INT1(NZ_MAX),INT2(NZ_MAX)
      REAL (WP)             ::  IN1,IN2
!
      REAL (WP), PARAMETER  ::  U_MAX = 20.0E0_WP  ! upper bound for intergration
!
      REAL (WP)             ::  FLOAT,LOG
!
      ID = 2                                                        !
!
      U_MIN  = SMALL                                                !
      U_STEP = (U_MAX - U_MIN) / FLOAT(NU - 1)                      !
!
!  Computing the R0(u) function                                     !
!
      CALL R0U(R0)                                                  !
!
!  Computing the integrand function                                 ! 
!
      DO IU = 1, NU                                                 !
!
        INT1(IU) = R0(IU) * R0(IU)                                  !
        INT2(IU) = R0(IU) * R0(IU) * LOG(R0(IU))                    !
!
      END DO                                                        !
!
      CALL INTEGR_L(INT1,U_STEP,NZ_MAX,NU,IN1,ID)                   !
      CALL INTEGR_L(INT2,U_STEP,NZ_MAX,NU,IN2,ID)                   !  
!
      A   = THREE * IN1 / PI3                                       !
      BPR = THREE * IN2 / PI3                                       !
!
      END SUBROUTINE RPA_CONSTANTS
!
END MODULE SPECIFIC_INT_6
