!
!=======================================================================
!
MODULE SPECIFIC_INT_1
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      FUNCTION SQQZ_INT(Q,D)
!
!  This function computes the integral
!
!                                   / + pi/d
!                d                 |                dqz
!     S(q)  =  ------  sinh(q*d)   |         ----------------------
!               2 pi               |          cosh(q*d) - cos(qz*d)
!                                 / - pi/d
!
!
!                                   / + pi
!                1                 |                 dx
!           =  ------  sinh(q*d)   |        ----------------------
!               2 pi               |          cosh(q*d) - cos(x)
!                                 / - pi
!
!
!     appearing in the calculation of the dielectric function of a 
!       stacking of 2D electron gas sheets separated by d
!
!
!  Input parameters:
!
!       * Q        : momentum (in SI)
!       * D        : distance between the 2D sheets (in SI)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  4 Jun 2020
!
!
      USE DIMENSION_CODE,   ONLY : NZ_MAX
      USE REAL_NUMBERS,     ONLY : ONE,TWO
      USE PI_ETC,           ONLY : PI
      USE INTEGRATION,      ONLY : INTEGR_L
!
      IMPLICIT NONE
!
      REAL (WP)             ::  Q,D
      REAL (WP)             ::  SQQZ_INT
      REAL (WP)             ::  QD,COEF,X,H,F(NZ_MAX),IN
!
      REAL (WP)             ::  FLOAT,SINH,COSH,COS
!
      INTEGER               ::  J,ID
!
      QD = Q * D                                                    !
      ID = 1                                                        !
      H  = TWO * PI / FLOAT(NZ_MAX - 1)                             ! step
!
      COEF = SINH(QD) / (TWO * PI)                                  !
!
!  Computing the integrand function
!
      DO J = 1,NZ_MAX                                               !
        X    = - PI + FLOAT(J - 1) * H                              !
        F(J) = ONE / (COSH(QD) -DCOS(X))                            !
      END DO                                                        !
!
!  Computing the integral
!
      CALL INTEGR_L(F,H,NZ_MAX,NZ_MAX,IN,ID)                        !
!
      SQQZ_INT = COEF * IN                                          !
!
      END FUNCTION SQQZ_INT  
!
!=======================================================================
!
      FUNCTION STEI_INT(X,P)
!
!  This function computes Steinberg's J_p(x) integral function
!
!               / x
!              |             z^p
!  J_p(x)  =   |    -----------------------  dz
!              |     (e^z - 1)(1 - e^(-z))
!             / 0
!
!    for p >= 2 so that the integrand does not diverge in 0
!
!
!  Input parameters:
!
!       * X        : upper bound of the integral
!       * P        : order of the function
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  4 Jun 2020
!
!
      USE DIMENSION_CODE,   ONLY : NZ_MAX
      USE REAL_NUMBERS,     ONLY : ZERO,ONE
      USE INTEGRATION,      ONLY : INTEGR_L
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X
      REAL (WP)             ::  STEI_INT
      REAL (WP)             ::  Z
      REAL (WP)             ::  INTF(NZ_MAX),H,IN
!
      REAL (WP)             ::  FLOAT,EXP
!
      INTEGER               ::  P,J,ID
      INTEGER               ::  LOGF
!
      LOGF = 6                                                      !
!
!  Checking for the value of P
!
      IF(P < 2) THEN                                                !
        WRITE(LOGF,10)                                              !
        STOP                                                        !
      ELSE IF(P == 2) THEN                                          !
        ID      = 2                                                 ! integrand not = 0 in 0
        INTF(1) = ONE                                               !
      ELSE                                                          !
        ID      = 1                                                 ! integrand = 0 in 0
        INTF(1) = ZERO                                              !
      END IF                                                        !
!
      H = X / FLOAT(NZ_MAX - 1)                                     ! step
!
!  Computing the integrand function
!
      DO J = 2,NZ_MAX                                               !
        Z       = FLOAT(J - 1) * H                                  ! 
        INTF(J) = Z**P / ( (EXP(Z) - ONE) * (ONE - EXP(-Z)) )       !
      END DO                                                        !
!
!  Computing the integral
!
      CALL INTEGR_L(INTF,H,NZ_MAX,NZ_MAX,IN,ID)                     !
!
      STEI_INT = IN                                                 !
!
  10  FORMAT(5X,'<<<<<  ERROR IN STEI_INT FUNCTION  >>>>>',/,    &   
             5X,'<<<<<   P SHOULD BE AT LEAST = 2   >>>>>')         !
!
      END FUNCTION STEI_INT  
!
END MODULE SPECIFIC_INT_1
