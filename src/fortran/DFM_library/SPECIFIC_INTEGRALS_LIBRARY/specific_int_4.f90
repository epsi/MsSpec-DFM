!
!=======================================================================
!
MODULE SPECIFIC_INT_4
!
!  This module provides six integrals used by Kugler to compute
!   the local field correction G(q)
!
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE INT_KUG(X,INTGR_1,INTGR_2,INTGR_3,INTGR_4,INTGR_5,  & 
                           INTGR_6)
!
!  This subroutine computes six integrals used by Kugler to compute
!   the local field correction G(q)
!
!
!  Reference: (1) A. A. Kugler, J. Stat. Phys. 12, 35-87  (1975)
!
! 
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!
!  Output parameters:
!
!       * INTGR_1   : integral 1
!       * INTGR_2   : integral 2
!       * INTGR_3   : integral 3
!       * INTGR_4   : integral 4
!       * INTGR_5   : integral 5
!       * INTGR_6   : integral 6
!
!
!                / 1
!               |            1 
!  INTGR_1 =    |      -------------  Log |y^2 - 1|  dy
!               |       ( y + x/2)
!              / 1-x
!
!
!                / 1+x
!               |            1 
!  INTGR_2 =    |      -------------  Log |y^2 - 1|  dy
!               |       ( y - x/2)
!              / 1
!
!
!                / 1
!               |            1            | y + 1 |
!  INTGR_3 =    |      -------------  Log |-------|  dy
!               |       ( y + x/2)        | y - 1 | 
!              / 1-x
!
!
!                / 1+x
!               |            1            | y + 1 | 
!  INTGR_4 =    |      -------------  Log |-------|  dy
!               |       ( y - x/2)        | y - 1 | 
!              / 1
!
!
!                / 1                   _                                   _
!               |            1        |                                     |
!  INTGR_5 =    |      -------------  | Log |y^2 - 1| + Log |(y + x)^2| - 1 |  dy
!               |       ( y + x/2)    |_                                   _|
!              / -1
!
!
!                / 1                   _                                  _
!               |            1        |     | y + 1 |       | y + x + 1 |  |
!  INTGR_6 =    |      -------------  | Log |-------| - Log |-----------|  |  dy
!               |       ( y + x/2)    |     | y - 1 |       | y + x - 1 | _|
!              / -1
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 20 Sep 2020
!
!
      USE DIMENSION_CODE,           ONLY : NZ_MAX
      USE REAL_NUMBERS,             ONLY : ZERO,ONE,TWO,HALF
      USE INTEGRATION,              ONLY : INTEGR_L
!
      IMPLICIT NONE
!
      INTEGER, PARAMETER     ::  N_I = 100             ! number of integration steps
      INTEGER                ::  I                     ! loop index
      INTEGER                ::  ID                    ! integration variable
!
      REAL (WP), INTENT(IN)  ::  X
      REAL (WP)              ::  INTGR_1,INTGR_2
      REAL (WP)              ::  INTGR_3,INTGR_4
      REAL (WP)              ::  INTGR_5,INTGR_6
!
      REAL (WP)              ::  I_STEP_1,I_STEP_2
      REAL (WP)              ::  DO_1,DO_2,DO_3
      REAL (WP)              ::  DO_4,DO_5,DO_6
      REAL (WP)              ::  Y1,Y2,Y3,Y4,Y5,Y6     ! integration variables
      REAL (WP)              ::  F1(NZ_MAX),F2(NZ_MAX)
      REAL (WP)              ::  F3(NZ_MAX),F4(NZ_MAX)
      REAL (WP)              ::  F5(NZ_MAX),F6(NZ_MAX)
!
      REAL (WP)              ::  FLOAT,LOG,ABS
!
      ID = 1                                                        !
!
!  Initialization of integrand functions
!
      DO I = 1, NZ_MAX                                              !
        F1(I) = ZERO                                                !
        F2(I) = ZERO                                                !
        F3(I) = ZERO                                                !
        F4(I) = ZERO                                                !
        F5(I) = ZERO                                                !
        F6(I) = ZERO                                                !
      END DO                                                        !
!
      I_STEP_1 = X   / FLOAT(N_I -1)                                ! step for INTGR_1-INTGR_4
      I_STEP_2 = TWO / FLOAT(N_I -1)                                ! step for INTGR_5-INTGR_6
!
      DO_1 =   ONE - X                                              ! \
      DO_2 =   ONE                                                  !  \
      DO_3 =   ONE - X                                              !   \  lower integration
      DO_4 =   ONE                                                  !   /  bounds
      DO_5 = - ONE                                                  !  /
      DO_6 = - ONE                                                  ! /
!
      DO I = 1, N_I                                                 !
!
        Y1 = DO_1 + FLOAT(I - 1) * I_STEP_1                         ! \
        Y2 = DO_2 + FLOAT(I - 1) * I_STEP_1                         !  \
        Y3 = DO_3 + FLOAT(I - 1) * I_STEP_1                         !   \  integration
        Y4 = DO_4 + FLOAT(I - 1) * I_STEP_1                         !   /   variable
        Y5 = DO_5 + FLOAT(I - 1) * I_STEP_2                         !  /
        Y6 = DO_6 + FLOAT(I - 1) * I_STEP_2                         ! /
!
!  Integrand functions
!
        F1(I) = LOG(ABS(Y1 * Y1 - ONE)) / (Y1 + HALF * X)           ! \
        F2(I) = LOG(ABS(Y2 * Y2 - ONE)) / (Y2 - HALF * X)           !  \
        F3(I) = LOG(ABS((Y3 + ONE) / (Y3 - ONE))) / (Y3 + HALF * X) !   \  integrand
        F4(I) = LOG(ABS((Y4 + ONE) / (Y4 - ONE))) / (Y4 - HALF * X) !   /  function
        F5(I) = ( LOG(ABS(Y5 * Y5 - ONE)) +                       & !  /
                  LOG(ABS((Y5 + X)**2 - ONE)) ) / (Y5 + HALF * X)   ! /
        F6(I) = ( LOG(ABS((Y6 + ONE) / (Y6 - ONE))) -             & ! |
                  LOG(ABS((Y6 + X + ONE) / (Y6 + X - ONE))) ) /   & ! |
                 (Y6 + HALF * X)                                    ! | 
!
      END DO                                                        !
!
!  Computing the integrals
!
      CALL INTEGR_L(F1,I_STEP_1,NZ_MAX,N_I,INTGR_1,ID)              ! \
      CALL INTEGR_L(F2,I_STEP_1,NZ_MAX,N_I,INTGR_2,ID)              !  \
      CALL INTEGR_L(F3,I_STEP_1,NZ_MAX,N_I,INTGR_3,ID)              !   \  integration
      CALL INTEGR_L(F4,I_STEP_1,NZ_MAX,N_I,INTGR_4,ID)              !   /  results
      CALL INTEGR_L(F5,I_STEP_2,NZ_MAX,N_I,INTGR_5,ID)              !  /
      CALL INTEGR_L(F6,I_STEP_2,NZ_MAX,N_I,INTGR_6,ID)              ! /
!
      END SUBROUTINE INT_KUG
!
END MODULE SPECIFIC_INT_4
