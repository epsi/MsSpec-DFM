!
!=======================================================================
!
MODULE SPECIFIC_INT_10
!
!  This module provides integrals involving the static dielectric function
!
!
      USE ACCURACY_REAL
      USE ACCURACY_INTEGER
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE INT_PINO(INTG)
!
!
!  Reference: (1) D. Pines and P. Nozi\`{e}res, 
!                    "The Theory of Quantum Liquids -- Normal Fermi Liquids", 
!                    (Benjamin, 1966)
!
!
!  Input parameters:
!
!       * NONE
!
!  Output parameters:
!
!       * INTG     : integral 1
!
!
!
!
!                            / 2 
!                 1         |                1                 
!  INTG(x) =   ------       |      --------------------  dy
!                  3        |        4               2
!               k_F        / 0      y    | eps(y,0) |
!
! 
! 
!  Note : for the RPA dielectric function, 
! 
!
!              eps = 1 + (k_TF / q)^2 * L(y) 
!                                        |
!                                        |---> dimensionless Lindhard function
! 
!         so that we rewrite
!
!
!              y^4 | eps(y,0) |^2  =  | y^2 + (k_TF / k_F)^2 * L(y) |^2
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Dec 2020
!
!
      USE REAL_NUMBERS,             ONLY : ZERO,ONE,TWO,FOUR,HALF,TTINY
      USE PI_ETC,                   ONLY : PI_INV
      USE CONSTANTS_P1,             ONLY : BOHR
      USE FERMI_SI,                 ONLY : KF_SI 
      USE LINDHARD_FUNCTION,        ONLY : LINDHARD_S
      USE SCREENING_VEC,            ONLY : THOMAS_FERMI_VECTOR
      USE INTEGRATION,              ONLY : INTEGR_L
!
      IMPLICIT NONE
!
      INTEGER (IW), PARAMETER ::  N_I = 1000            ! number of integration steps
      INTEGER (IW)            ::  I                     ! loop index
      INTEGER (IW)            ::  ID                    ! integration variable
!
      REAL (WP), INTENT(OUT)  ::  INTG
!
      REAL (WP)               ::  Y_MIN,Y_MAX,Y_STEP
      REAL (WP)               ::  F(N_I)
      REAL (WP)               ::  Y,Y2,X
      REAL (WP)               ::  K_SI,RAT
      REAL (WP)               ::  LR,LI
      REAL (WP)               ::  Q2EPS
!
      ID = 2                                                        ! integrand /= 0 at 0
!
      Y_MIN  = 0.010E0_WP                                           !
      Y_MAX  = TWO                                                  !
      Y_STEP = (Y_MAX - Y_MIN) / FLOAT(N_I - 1)                     !
!
!  Computing (K_TF / k_F)^2
!
      RAT = FOUR * PI_INV / (BOHR * KF_SI)                          !
!
!  Initialization of integrand function
!
      DO I = 1, N_I                                                 !
        F(I) = TTINY                                                !
      END DO                                                        !
!
!  Computing the integrand function
!
      DO I = 1, N_I                                                 !
!
        Y    = Y_MIN + FLOAT(I - 1) * Y_STEP                        ! q / k_F
        Y2   = Y * Y                                                !
        X    = HALF * Y                                             ! q / 2k_F
        K_SI = Y * KF_SI                                            ! q in SI
!
!  Calculating the Lindhard function
!
        CALL LINDHARD_S(X,'3D',LR,LI)                               !
!
        Q2EPS = Y2 + RAT * LR                                       ! | q^2 * RPA epsilon |
!
        F(I) = ONE / (Q2EPS * Q2EPS)                                !
!
      END DO                                                        !
!
!  Computing the integral
!
      CALL INTEGR_L(F,Y_STEP,N_I,N_I,INTG,ID)                       !  
!
      INTG = INTG / (KF_SI * KF_SI * KF_SI)                         !
!
      END SUBROUTINE INT_PINO
!
END MODULE SPECIFIC_INT_10
 
