!
!=======================================================================
!
MODULE SPECIFIC_INT_2
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE INT_SQM1(NMAX,X_MAX,IN_MODE,RS,T,A,L,SQ_TYPE,   &
                          GQ_TYPE,IN) 
!
!  This subroutine computes several integrals involving (S(q)-1), 
!    where S(q) is the static structure factor. q is represented 
!    in reduced units X = q / (2*k_F)
!
!
!  Input parameters:
!
!       * NMAX     : dimensioning of the arrays
!       * X_MAX    : upper integration value
!       * IN_MODE  : type of integral computed (see below)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * A        : dimensionless screening vector / coefficient / X
!       * L        : power of X or 1/X
!       * SQ_TYPE  : structure factor approximation (3D)
!       * GQ_TYPE  : local-field correction type (3D)
!
!  Output parameters:
!
!       * IN       : integral result
!
!
!
!
!                 / x_max                       
!                |              
!   IN   =       |        (SQ(X) - 1 ) dX                     :  IN_MODE = 1
!                |               
!               / 0                               
!
!
!                 / x_max                          
!                |             X^2        
!   IN   =       |        ------------- (SQ(X) - 1 ) dX       :  IN_MODE = 2
!                |         (X^2 + A^2)         
!               / 0                               
!
!
!                 / x_max                          
!                |             X^3        
!   IN   =       |        ---------------- (SQ(X) - 1 ) dX    :  IN_MODE = 3 
!                |         (X^2 + A^2)^2         
!               / 0                               
!
!
!                 / x_max                        
!                |                   
!   IN   =       |         X sin(XA) (SQ(X) - 1 ) dX          :  IN_MODE = 4
!                |                 
!               / 0                               
!
!
!                 / x_max                          
!                |                   
!   IN   =       |         X^L (SQ(X) - 1 ) dX                :  IN_MODE = 5
!                |                 
!               / 0                               
!
!
!                 / x_max                          
!                |         (SQ(X) - 1 )          
!   IN   =       |         ------------- dX                   :  IN_MODE = 6 
!                |              X^L    
!               / 0                               
!
!
!                 / x_max                          
!                |                   
!   IN   =       |         X F(X,A) (SQ(X) - 1 ) dX           :  IN_MODE = 7
!                |                 
!               / 0    
!                           5      X^2       A   (  X^2 - A^2  )^2      | X + A |
!            with F(X,A) = --- - ------- + ----- ( ----------- )     Ln |-------|
!                           6     2 A^2     4 X  (     A^2     )        | X - A |
!
!
!                                          | X + A |
!  Note: for X = A , (  X^2 - A^2  )^2  Ln |-------| = 0
!                                          | X - A |
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  3 Dec 2020
!
!
      USE DIMENSION_CODE,           ONLY : NZ_MAX
      USE REAL_NUMBERS,             ONLY : ZERO,ONE,FIVE,HALF,THIRD
      USE STRUCTURE_FACTOR_STATIC,  ONLY : STFACT_STATIC_3D
      USE INTEGRATION,              ONLY : INTEGR_L
!
      IMPLICIT NONE
!
      INTEGER, INTENT(IN)    ::  NMAX,IN_MODE,L
!
      INTEGER                ::  K                                  ! loop index
      INTEGER                ::  ID
      INTEGER                ::  N1
!
      CHARACTER (LEN = 4)    ::  GQ_TYPE
      CHARACTER (LEN = 3)    ::  SQ_TYPE
!
      REAL (WP), INTENT(IN)  ::  X_MAX,RS,T,A
      REAL (WP), INTENT(OUT) ::  IN
!
      REAL (WP)              ::  X_MIN
      REAL (WP)              ::  XA(NZ_MAX),INTF(NZ_MAX)
      REAL (WP)              ::  SQQ,XX,H,FXA
      REAL (WP)              ::  X_STEP,X,EPS
!
      REAL (WP)              ::  FLOAT,ABS,SIN,LOG
!
      ID = 1                                                        !
      N1 = NMAX                                                     ! index of upper bound
!
      EPS = 1.0E-3_WP                                               !
!
      X_MIN  = EPS      
      X_STEP = (X_MAX -X_MIN)  / FLOAT(NMAX - 1)                    !
!
      DO K = 1,NMAX                                                 !
!
        XX    = X_MIN + FLOAT(K - 1) * X_STEP                       ! x grid     
        XA(K) = XX                                                  !
        X     = HALF * XX                                           ! input for S(q)
!
!  Computing the static structure factor S(q)
!
        CALL STFACT_STATIC_3D(X,RS,T,SQ_TYPE,GQ_TYPE,SQQ)           !
!
!  Computing the integrand function
!
        IF(IN_MODE == 1) THEN                                       !
!
          INTF(K) = SQQ - ONE                                       !
!
        ELSE IF(IN_MODE == 2) THEN                                  !
!
          INTF(K) = XX * XX * (SQQ - ONE) / (XX * XX + A * A)       !                      
!
        ELSE IF(IN_MODE == 3) THEN                                  !
!
          INTF(K) = XX * XX * XX * (SQQ - ONE) /                  & !
                                   (XX * XX + A * A)**2             ! 
!
        ELSE IF(IN_MODE == 4) THEN                                  !
!
          INTF(K) = XX * SIN(A * XX) * (SQQ - ONE)                  !
!
        ELSE IF(IN_MODE == 5) THEN                                  !
!
          INTF(K) = XX**L * (SQQ - ONE)                             !
!
        ELSE IF(IN_MODE == 6) THEN                                  !
!
          INTF(K) = (SQQ - ONE) / ( XX**L )                         !
!
!
        ELSE IF(IN_MODE == 7) THEN                                  !
!
          IF(XX == A) THEN                                          !
            FXA = THIRD                                             !
          ELSE                                                      !
            FXA = FIVE * HALF * THIRD - HALF * XX * XX /( A * A) + & !
                  HALF * HALF * A / XX * ( XX * XX /               & !
                                         (A * A) - ONE )**2 *      & ! F(X,A)
                  LOG( ABS( (XX + A)/(XX - A) ) )                   !
          END IF                                                    !
          INTF(K) = XX * FXA * (SQQ - ONE)                          !
!
        END IF                                                      !
!
      END DO                                                        !
!
!
      H  = XA(2) - XA(1)                                            ! step
!
!  Computing the integral
!
      CALL INTEGR_L(INTF,H,NMAX,N1,IN,ID)                           !
!
      END SUBROUTINE INT_SQM1  
!
END MODULE SPECIFIC_INT_2
