!
!=======================================================================
!
MODULE SPECIFIC_INT_5
!
!  This module numerically computes moments of F(q,omega) functions
!
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE MOMENTS_FQO(I_F,P,M)
!
!  This subroutines computes moments of a F(q,omega) function
!
!  Input parameters:
!
!       * I_F      : type of F(q,omega) function
!                       I_F  = 1  -->  epsilon(q,omega)
!                       I_F  = 2  -->  Im [ - 1 / epsilon(q,omega) ]
!                       I_F  = 3  -->  Pi(q,omega)
!                       I_F  = 4  -->  chi(q,omega)
!                       I_F  = 5  -->  S(q,omega)
!       * P        : moment order
!
!
!  Output parameters:
!
!       * M        : resulting moment
!
!
!
!   Author :  D. Sébilleau
!
!
!                                           Last modified :  9 Oct 2020
!
!
      USE DIMENSION_CODE,           ONLY : NSIZE
!
      USE MATERIAL_PROP,            ONLY : DMN,RS
      USE EXT_FIELDS,               ONLY : T,H
      USE DF_VALUES,                ONLY : EPS_T,D_FUNC
      USE SF_VALUES,                ONLY : SQ_TYPE,SQO_TYPE
!
      USE REAL_NUMBERS,             ONLY : ZERO,HALF,FOURTH,SMALL,INF
!
      USE Q_GRID
      USE E_GRID
      USE UNITS,                    ONLY : UNIT
!
      USE UTILITIES_3,
      USE COULOMB_K,                ONLY : COULOMB_FF
      USE DFUNC_STATIC
      USE DFUNCT_STAN_DYNAMIC
      USE DFUNCL_STAN_DYNAMIC
      USE DFUNCL_MAGN_DYNAMIC
      USE STRUCTURE_FACTOR_DYNAMIC
      USE INTEGRATION,              ONLY : INTEGR_L
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  D_FUNCL,D_FUNCT
!
      INTEGER, INTENT(IN)   ::  I_F,P
      INTEGER               ::  IQ,IE
      INTEGER               ::  ID
!
      REAL (WP)             ::  FQO(NSIZE),RQO(NSIZE),IQO(NSIZE)
      REAL (WP)             ::  Q,X,E,Z
      REAL (WP)             ::  VC,KS,A,NU
      REAL (WP)             ::  REPS,IEPS
      REAL (WP)             ::  RPOL,IPOL
      REAL (WP)             ::  RSUS,ISUS
      REAL (WP)             ::  SQO
      REAL (WP)             ::  IN
      REAL (WP)             ::  M(N_Q),MR(N_Q),MI(N_Q)
!
      REAL (WP)             ::  FLOAT
!
      KS  = ZERO                                                    ! temporary
      A   = ZERO                                                    ! temporary
      NU  = ZERO                                                    ! temporary
!
!  Computing the Coulomb potential
!
      CALL COULOMB_FF(DMN,UNIT,Q,ZERO,VC)                           !
!
      DO IQ = 1, N_Q                                                ! q-loop
!
          Q = Q_MIN + FLOAT(IQ - 1) * Q_STEP                        ! Q = q/k_F
!
          X = HALF * Q                                              ! X = q/(2k_f)

!
        DO IE = 1, N_E                                              ! energy loop
!
          E = E_MIN + FLOAT(IE - 1) * E_STEP                        ! E = hbar omega / E_F
!
          Z = FOURTH * E / (X * X)                                  ! Z = omega / omega_q
!
!  Computing the integrand
!
          IF(I_F < 5) THEN                                          !
!
            IF(EPS_T == 'LONG') THEN                                ! longitudinal eps
!
              D_FUNCL=D_FUNC                                        !
              IF(H < SMALL) THEN                                    !
                CALL DFUNCL_DYNAMIC(X,Z,RS,T,D_FUNCL,IE,REPS,IEPS)  ! no magnetic field
              ELSE                                                  !
                CALL DFUNCL_DYNAMIC_M(X,Z,KS,A,NU,D_FUNCL,REPS,IEPS)! magnetic field
              END IF                                                !
            ELSE                                                    ! transverse eps
              D_FUNCT=D_FUNC                                        !
              IF(H < SMALL) THEN                                    !
                CALL DFUNCT_DYNAMIC(X,Z,D_FUNCT,REPS,IEPS)          ! no magnetic field
              ELSE                                                  !
                CONTINUE                                            ! magnetic field
              END IF                                                !
            END IF                                                  !
!
            IF(I_F == 1) THEN                                       !
              RQO(IE) = E**P * REPS                                 !
              IQO(IE) = E**P * IEPS                                 !
            ELSE IF(I_F == 2) THEN                                  !
              FQO(IE) = E**P * IEPS / (REPS**2 + IEPS**2)           !
              ID      = 1                                           !
            ELSE IF(I_F == 3) THEN                                  !
              CALL EPS_TO_PI(REPS,IEPS,VC,RPOL,IPOL)
              RQO(IE) = E**P * RPOL                                 !
              IQO(IE) = E**P * IPOL                                 !
            ELSE IF(I_F == 4) THEN                                  !
              CALL EPS_TO_CHI(REPS,IEPS,VC,RSUS,ISUS)               !
              RQO(IE) = E**P * RSUS                                 !
              IQO(IE) = E**P * ISUS                                 !
            END IF                                                  !
!
          ELSE IF(I_F == 5) THEN                                    !
!
            CALL STFACT_DYNAMIC(X,Z,RS,T,SQO_TYPE,SQ_TYPE,SQO)      !
            FQO(IE) = E**P * SQO                                    !
            ID      = 1                                             !
!
          END IF                                                    !
!
        END DO                                                      ! end of energy loop
!
!  Computation of the integral
!
        IF(I_F == 2 .OR. I_F == 5) THEN                             !
          CALL INTEGR_L(FQO,E_STEP,NSIZE,N_E,IN,ID)                 !
          M(IQ) = IN                                                !
        ELSE
          CALL INTEGR_L(RQO,E_STEP,NSIZE,N_E,IN,2)                  !
          MR(IQ) = IN                                               !
          CALL INTEGR_L(IQO,E_STEP,NSIZE,N_E,IN,1)                  !
          MI(IQ) = IN                                               !
        END IF                                                      !
!
      END DO                                                        ! end of q loop
!
      END SUBROUTINE MOMENTS_FQO
!
END MODULE SPECIFIC_INT_5
!
