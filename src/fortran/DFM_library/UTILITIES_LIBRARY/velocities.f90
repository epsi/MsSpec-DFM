!
!=======================================================================
!
MODULE VELOCITIES 
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE VELOCITIES_3D(RS,T,EC_TYPE,VE2,V_INT_2)
!
!  This subroutine computes velocities as a function of the 
!    correlation energy
!
!
!  References: (1)  I. M. Tkachenko, J. Alcober and J. L. Munoz-Cobo,
!                      Contrib. Plasma Phys. 5, 467-475 (2002)
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)     
!       * EC_TYPE  : type of correlation energy functional
!
!  Output parameters:
!
!       * VE2      : square of the average kinetic energy velocity
!       * V_INT_2  : square of the correlation energy velocity
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 18 Jun 2020
!
!
      USE REAL_NUMBERS,         ONLY : TWO
      USE CONSTANTS_P1,         ONLY : H_BAR,E
      USE CORRELATION_ENERGIES
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 6)   ::  EC_TYPE
!
      REAL (WP)             ::  RS,T
      REAL (WP)             ::  VE2,V_INT_2
      REAL (WP)             ::  COEF,EC,D_EC_1,D_EC_2
!
      COEF= (E*E/H_BAR)**2                                            !
!
      EC  = EC_3D(EC_TYPE,1,RS,T)                                     !
      CALL DERIVE_EC_3D(EC_TYPE,1,5,RS,T,D_EC_1,D_EC_2)               !
!
!  Velocities (with EC per electron in Ryd)                           !
!
      VE2     = COEF*(2.21E0_WP/(RS*RS) - EC - RS*D_EC_1)             ! ref (1) eq. (22)
      V_INT_2 = -TWO*COEF/15.0E0_WP *                               & !
                    (-0.916E0_WP/RS + TWO*EC + RS*D_EC_1)             ! ref (1) eq. (22)
!
      END SUBROUTINE VELOCITIES_3D
!
END MODULE VELOCITIES
 
