!
!=======================================================================
!
MODULE UTILITIES_1 
!
      USE ACCURACY_REAL
!
!  It contains the following functions/subroutines:
!
!             * FUNCTION ADD_RT(TAU_E,TAU_P,TAU_I)
!             * FUNCTION ALFA(DMN)
!             * FUNCTION D(DMN)
!             * FUNCTION DOS_EF(DMN)
!             * SUBROUTINE MSTAR_TO(MS,GV)
!             * FUNCTION RS_TO_N0(DMN,RS)
!             * SUBROUTINE VELOCITIES_3D(RS,EC_TYPE,VE2,V_INT_2)
!
!
CONTAINS
!
!
!=======================================================================
!
      FUNCTION ADD_RT(TAU_E,TAU_P,TAU_I)
!
!  This function computes the total relaxtion time from 
!    the knowledge of e-e, e-phonon and e-impurities relaxation times
!
!  Input parameters:
!
!       * TAU_E    : electron-electron relaxation time
!       * TAU_P    : electron-phonon relaxation time
!       * TAU_I    : electron-impurity relaxation time
!
!  Output variables :
!
!       * ADD_RT   : resulting relaxation time
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  4 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE
!
      IMPLICIT NONE
!
      REAL (WP)             ::  TAU_E,TAU_P,TAU_I
      REAL (WP)             ::  ADD_RT
      REAL (WP)             ::  SSUM
!
      SSUM=ONE/TAU_E + ONE/TAU_P + ONE/TAU_I                        !
!
      ADD_RT=ONE/SSUM                                               !
!
      END FUNCTION ADD_RT  
!
!=======================================================================
!
      FUNCTION ALFA(DMN)
!
!  This function computes the constant alpha occuring in the 
!    electron liquid theory
!
!  References: (1) G. F. Giuliani and G. Vignale, 
!                     "Quantum Theory of the Electron Liquid",
!                    (Cambridge University Press 2005)
!                     eq. (1.79)
!
!  Input parameters:
!
!       * DMN      : problem dimension
!
!
!  Output variables :
!
!       * ALFA     : alpha coefficient
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  3 Jun 2020
!
!
      USE REAL_NUMBERS, ONLY : ONE,THIRD,FOUR
      USE SQUARE_ROOTS, ONLY : SQR2
      USE PI_ETC,       ONLY : PI_INV
!
      IMPLICIT NONE
!
      REAL (WP)             ::  ALFA
!
      CHARACTER (LEN = 2)   ::  DMN
!
      IF(DMN == '3D') THEN                                          !
        ALFA = (FOUR * THIRD * THIRD * PI_INV)**THIRD               !
      ELSE IF(DMN == '2D') THEN                                     !
        ALFA = ONE / SQR2                                           !
      ELSE IF(DMN == 'Q2') THEN                                     ! to be checked !
        ALFA = ONE / SQR2                                           !
      ELSE IF(DMN == 'BL') THEN                                     ! to be checked !
        ALFA = ONE / SQR2                                           !
      ELSE IF(DMN == 'ML') THEN                                     ! to be checked !
        ALFA = ONE / SQR2                                           !
      ELSE IF(DMN == '1D') THEN                                     !
        ALFA = FOUR * PI_INV                                        !
      ELSE IF(DMN == 'Q1') THEN                                     ! to be checked !
        ALFA = FOUR * PI_INV                                        !
      ELSE IF(DMN == 'Q0') THEN                                     ! to be checked !
        ALFA = FOUR * PI_INV                                        !
      END IF                                                        !
!
      END FUNCTION ALFA
!
!=======================================================================
!
      FUNCTION D(DMN)
!
!  This function computes the dimensionality
!
!  Input parameters:
!
!       * DMN      : problem dimension
!
!
!  Output variables :
!
!       * D        : dimensionality
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  4 Jun 2020
!
      USE REAL_NUMBERS, ONLY : ONE,TWO,THREE
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP)             ::  D
!
      IF(DMN == '3D') THEN                                          !
        D = THREE                                                   !
      ELSE IF(DMN == '2D') THEN                                     !
        D = TWO                                                     !
      ELSE IF(DMN == '1D') THEN                                     !
        D = ONE                                                     !
      END IF                                                        !
!
      END FUNCTION D  
!
!=======================================================================
!
      FUNCTION DOS_EF(DMN)
!
!  This function computes the density of states at the Fermi level. 
!
!  Note: it is NOT spin-resolved. In order to obtain the DoS per spin,
!        the values should be divided by 2
!
!  Input parameters:
!
!       * DMN      : problem dimension
!
!
!  Output variables :
!
!       * DOS_EF   : DoS at EF
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Apr 2020
!
      USE REAL_NUMBERS,     ONLY : TWO
      USE CONSTANTS_P1,     ONLY : H_BAR,M_E
      USE FERMI_SI,         ONLY : KF_SI
      USE PI_ETC,           ONLY : PI,PI2
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP)             ::  DOS_EF
!
      IF(DMN == '3D') THEN                                          !
        DOS_EF = M_E * KF_SI /(PI2 * H_BAR * H_BAR)                 !
      ELSE IF(DMN == '2D') THEN                                     !
        DOS_EF = M_E / (PI * H_BAR * H_BAR)                         !
      ELSE IF(DMN == '1D') THEN                                     !
        DOS_EF = TWO * M_E/ (PI * H_BAR * H_BAR * KF_SI)            ! 
      END IF                                                        !
!
      END FUNCTION DOS_EF  
!
!=======================================================================
!
      FUNCTION KF_TO_N0(DMN,KF)
!
!  This function computes the electron density from the Wigner-Seitz
!    radius.
!
!  Input parameters:
!
!       * DMN      : problem dimension
!       * KF       : Fermi wave vector in SI
!
!  Output variables :
!
!       * KF_TO_N0 : electron density in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  4 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : TWO,HALF,THIRD
      USE PI_ETC,           ONLY : PI_INV
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP)             ::  KF,KF_TO_N0
! 
      IF(DMN == '3D') THEN                                          !
        KF_TO_N0=THIRD*PI_INV*PI_INV*KF*KF*KF                       ! in 1/m^3
      ELSE IF(DMN == '2D') THEN                                     !
        KF_TO_N0=HALF*PI_INV*KF*KF                                  ! in 1/m^2
      ELSE IF(DMN == '1D') THEN                                     !
        KF_TO_N0=TWO*PI_INV*KF                                      ! in 1/m 
      END IF                                                        !
!
      END FUNCTION KF_TO_N0  
!
!=======================================================================
!
      SUBROUTINE MSTAR_TO(MS,GV)
!
!  This subroutine recomputes all fundamental quantities depending 
!    on the mass of a particle. In practice, it modifies the values 
!    stored in the different common blocks
!
!
!  Input parameters: 
!
!       * MS       : m* --> mass of the electron/hole considered
!       * GV       : valley degeneracy
!
!
!  Output variables :
!
!       * BOHR     : 
!       * RYD      : 
!       * HAR      :
!       * ALPHA    :
!       * MU_B     :
!       * EPS      :
!       * RS       :
!       * GV       : 
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  4 Jun 2020
!
!
      USE CONSTANTS_P1,     ONLY : BOHR,M_E
      USE CONSTANTS_P2 
      USE CONSTANTS_P3,     ONLY : MU_B
!
      IMPLICIT NONE
!
      REAL (WP)             ::  MS,GV
      REAL (WP)             ::  RT
!
      RT=MS/M_E                                                     ! ratio m*/m
!
      BOHR=BOHR/RT                                                  !
      M_E=M_E*RT                                                    !
      ALPHA=ALPHA/RT                                                !
      MU_B=MU_B/RT                                                  !
      HARTREE=HARTREE/RT                                            !
      RYDBERG=RYDBERG/RT                                            !
!
      END SUBROUTINE MSTAR_TO 
!
!=======================================================================
!
      FUNCTION RS_TO_N0(DMN,RS)
!
!  This function computes the electron density from the Wigner-Seitz
!    radius.
!
!  Input parameters:
!
!       * DMN      : problem dimension
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!  Output variables :
!
!       * RS_TO_N0 : electron density in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  4 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : HALF
      USE CONSTANTS_P1,     ONLY : BOHR
      USE PI_ETC,           ONLY : PI_INV
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP), INTENT(IN) ::  RS
      REAL (WP)             ::  RS_TO_N0
      REAL (WP)             ::  R_S
!
      R_S = RS * BOHR                                               ! RS in SI (meters)
!
      IF(DMN.EQ.'3D') THEN                                          !
        RS_TO_N0 = 0.750E0_WP * PI_INV / (R_S * R_S * R_S)          ! in 1/m^3
      ELSE IF(DMN.EQ.'2D') THEN                                     !
        RS_TO_N0 = PI_INV / (R_S * R_S)                             ! in 1/m^2
      ELSE IF(DMN.EQ.'1D') THEN                                     !
        RS_TO_N0 = HALF / R_S                                       ! in 1/m 
      END IF                                                        !
!
      END FUNCTION RS_TO_N0  
!
END MODULE UTILITIES_1
