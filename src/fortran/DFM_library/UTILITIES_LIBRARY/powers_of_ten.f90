!
!=======================================================================
!
MODULE POWERS_OF_TEN 
!
!  This module defines the prefixes in the SI
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP), PARAMETER   ::   KILO      =   1.0E+03_WP
      REAL (WP), PARAMETER   ::   MEGA      =   1.0E+06_WP
      REAL (WP), PARAMETER   ::   GIGA      =   1.0E+09_WP
      REAL (WP), PARAMETER   ::   TERA      =   1.0E+12_WP
      REAL (WP), PARAMETER   ::   PETA      =   1.0E+15_WP
      REAL (WP), PARAMETER   ::   EXA       =   1.0E+18_WP
      REAL (WP), PARAMETER   ::   ZETTA     =   1.0E+21_WP
      REAL (WP), PARAMETER   ::   YOTTA     =   1.0E+24_WP
!
      REAL (WP), PARAMETER   ::   MILLI     =   1.0E-03_WP
      REAL (WP), PARAMETER   ::   MICRO     =   1.0E-06_WP
      REAL (WP), PARAMETER   ::   NANO      =   1.0E-09_WP
      REAL (WP), PARAMETER   ::   PICO      =   1.0E-12_WP
      REAL (WP), PARAMETER   ::   FEMTO     =   1.0E-15_WP
      REAL (WP), PARAMETER   ::   ATTO      =   1.0E-18_WP
      REAL (WP), PARAMETER   ::   ZEPTO     =   1.0E-21_WP
      REAL (WP), PARAMETER   ::   YOCTO     =   1.0E-24_WP
!
END MODULE POWERS_OF_TEN
