!
!=======================================================================
!
MODULE UTIC_PARAMETERS 
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE UTIC_PARAM(X,RS,T,OMQ,OM0)
!
!  This subroutine computes the OMEGA(q) and OMEGA(0) parameters 
!    entering the Utsumi-Ichimaru dielectric function approach
!
!  Reference: (1) K. Utsumi and S. Ichimaru, 
!                    Phys. Rev. B 22, 1522-1533 (1980)
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature  (in SI)
!
!
!  Intermediate parameters:
!
!       * SQ_TYPE  : structure factor approximation (3D)
!       * GQ_TYPE  : local-field correction type (3D)
!       * EC_TYPE  : type of correlation energy functional
!       * IQ_TYPE  : type of approximation for I(q)
!
!  Output parameters:
!
!       * OMQ      : OMEGA(q) characteristic frequency
!       * OM0      : OMEGA(0) parameter = lim_{q --> 0} OMEGA(q)
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 3 Dec 2020
!
!
      USE REAL_NUMBERS,            ONLY : ZERO,ONE,TWO,HALF
      USE CONSTANTS_P1,            ONLY : H_BAR
      USE FERMI_SI,                ONLY : EF_SI,KF_SI
      USE PI_ETC,                  ONLY : PI,PI_INV
      USE LF_VALUES,               ONLY : GQ_TYPE,IQ_TYPE
      USE SF_VALUES,               ONLY : SQ_TYPE
      USE ASYMPT,                  ONLY : G0,GI
      USE RELAXATION_TIME_STATIC,  ONLY : UTIC_RT_3D
      USE IQ_FUNCTIONS_1
      USE LOCAL_FIELD_STATIC
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)  ::  X,RS,T
      REAL (WP), INTENT(OUT) ::  OMQ,OM0
      REAL (WP)              ::  Y,OMP
      REAL (WP)              ::  IQ,GQ,TAU_Q
      REAL (WP)              ::  COEF
!
      REAL (WP)              ::  SQRT
!
      Y   = X + X                                                   ! q / k_F
      OMP = ENE_P_SI / H_BAR                                        ! omega_p in SI
!
!  Computing the static values I(q) and  G(q) 
!
      CALL IQ_3D(X,RS,IQ_TYPE,IQ)                                   !
      CALL LOCAL_FIELD_STATIC_3D(X,RS,T,GQ_TYPE,GQ)                 !
!
!  Computing the relaxation time TAU_Q
!
      TAU_Q = UTIC_RT_3D(X,RS,T,SQ_TYPE,GQ_TYPE)                    !
!
      COEF = SQRT(HALF * PI) * OMP * OMP * TAU_Q                    !
!
      OMQ = COEF * (GQ - IQ)                                        ! ref. 1 eq. (3.19)
      OM0 = COEF * Y * Y * (G0 - GI)                                ! ref. 1 eq. (5.7)
!
      END SUBROUTINE UTIC_PARAM  
!
END MODULE UTIC_PARAMETERS 
