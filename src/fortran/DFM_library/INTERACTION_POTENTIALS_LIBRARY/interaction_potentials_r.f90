!
!=======================================================================
!
MODULE INTERACTION_POTENTIALS_R
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE INTERACT_POT_R_3D(UNIT,R,Q1,Q2,KS,VR)
!
!  This subroutine computes interaction potentials in the
!    r-space in 3D.
!
!
!  Input parameters:
!
!       * UNIT     : system unit
!                       UNIT  = 'SIU' international system
!                       UNIT  = 'CGS' CGS system
!       * R        : point at which the potential is computed
!       * Q1       : charge of particle 1
!       * Q2       : charge of particle 2
!       * KS       : screening momentum  (unit indifferent)
!
!
!  Intermediate INT_POT parameter:
!
!                       INT_POT = 'COULO' Coulomb interaction
!                       INT_POT=  'YUKAW' Yukawa interaction
!                       INT_POT=  'SOFTS' soft sphere
!                       INT_POT=  'LNJNS' Lennard-Jones
!                       INT_POT=  'HCLNJ' hard-core Lennard-Jones
!                       INT_POT=  'KIHAR' Kihara
!                       INT_POT=  'MIE_P' Mie
!                       INT_POT=  'VANDW' Van der Waals
!                       INT_POT=  'MORSE' Morse
!                       INT_POT=  'G_EXP' generalised exponential
!                       INT_POT=  'EXP_6' exp-6
!                       INT_POT=  'MBUCK' modified Buckingham
!                       INT_POT=  'N_COU' neutralised Coulomb
!                       INT_POT=  'H_COR' hard-core
!                       INT_POT=  'P_SPH' penetrable sphere
!                       INT_POT=  'ST-JO' Starkloff-Joannopoulos soft-core
!                       INT_POT=  'LR_OS' long-range oscillatory
!                       INT_POT=  'STOCK' Stockmayer
!
!
!  Output parameters:
!
!       * VR       : interaction potential (energy)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Apr 2021
!
!
      USE EL_ELE_INTER
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)     ::  UNIT
!
      REAL (WP), INTENT(IN)   ::  Q1,Q2,KS,R
      REAL (WP), INTENT(OUT)  ::  VR
!
      IF(INT_POT == 'COULO') THEN                                   !
        VR = V_COUL_R(UNIT,Q1,Q2,R)                                 !
      ELSE IF(INT_POT == 'YUKAW') THEN                              !
        VR = V_YUKA_R(UNIT,Q1,Q2,KS,R)                              !
      ELSE IF(INT_POT == 'SOFTS') THEN                              !
        VR = V_SOFT_R(S,EPS,R)                                      !
      ELSE IF(INT_POT == 'LNJNS') THEN                              !
        VR = V_LEJO_R(S,EPS,R)                                      !
      ELSE IF(INT_POT == 'HCLNJ') THEN                              !
        VR = V_HCLJ_R(S,EPS,RC,R)                                   !
      ELSE IF(INT_POT == 'KIHAR') THEN                              !
        VR = V_KIHA_R(S,EPS,M,N,R)                                  !
      ELSE IF(INT_POT == 'MIE_P') THEN                              !
        VR = V_MIEP_R(S,EPS,M,N,R)                                  !
      ELSE IF(INT_POT == 'VANDW') THEN                              !
        VR = V_VDWP_R(S,EPS,R)                                      !
      ELSE IF(INT_POT == 'MORSE') THEN                              !
        VR = V_MORS_R(S,EPS,ALF,R)                                  !
      ELSE IF(INT_POT == 'G_EXP') THEN                              !
        VR = V_GEXP_R(S,EPS,ALF,R)                                  !
      ELSE IF(INT_POT == 'EXP_6') THEN                              !
        VR = V_EXP6_R(S,EPS,ALF,R)                                  !
      ELSE IF(INT_POT == 'MBUCK') THEN                              !
        VR = V_MBUC_R(S,EPS,ALF,R)                                  !
      ELSE IF(INT_POT == 'N_COU') THEN                              !
        VR = V_NCOU_R(UNIT,Q1,Q2,S,R)                               !
      ELSE IF(INT_POT == 'H_COR') THEN                              !
        VR = V_HACO_R(RC,R)                                         !
      ELSE IF(INT_POT == 'P_SPH') THEN                              !
        VR = V_PSPH_R(EPS,RC,R)                                     !
      ELSE IF(INT_POT == 'ST-JO') THEN                              !
        VR = V_STJO_R(S,EPS,ALF,A1,R)                               !
      ELSE IF(INT_POT == 'LR_OS') THEN                              !
        VR = V_LROS_R(S,EPS,DELTA,A1,A2,A3,A4,R)                    !
      ELSE IF(INT_POT == 'STOCK') THEN                              !
        VR = V_STOC_R(S,EPS,DELTA,R)                                !
      END IF                                                        !
!
      END SUBROUTINE INTERACT_POT_R_3D
!
!=======================================================================
!
      FUNCTION V_COUL_R(UNIT,Q1,Q2,R)
!
!  This function computes the Coulomb interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * UNIT     : system unit
!                       UNIT  = 'SIU' international system
!                       UNIT  = 'CGS' CGS system
!       * Q1       : charge of particle 1
!       * Q2       : charge of particle 2
!       * R        : point
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 16 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : FOUR
      USE CONSTANTS_P1,     ONLY : EPS_0
      USE PI_ETC,           ONLY : PI
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  UNIT
!
      REAL (WP)             ::  Q1,Q2,R
      REAL (WP)             ::  V_COUL_R
      REAL (WP)             ::  COEF
!
      IF(UNIT == 'SIU') THEN                                        !
        COEF = Q1 * Q2 / (FOUR * PI * EPS_0)                        !
      ELSE IF(UNIT == 'CGS') THEN                                   !
        COEF = Q1 * Q2                                              !
      END IF                                                        !
!
      V_COUL_R = COEF / R                                           !
!
      END FUNCTION V_COUL_R
!
!=======================================================================
!
      FUNCTION V_YUKA_R(UNIT,Q1,Q2,KS,R)
!
!  This function computes the Yukawa interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * UNIT     : system unit
!                       UNIT  = 'SIU' international system
!                       UNIT  = 'CGS' CGS system
!       * Q1       : charge of particle 1
!       * Q2       : charge of particle 2
!       * KS       : screening wave vector
!       * R        : point
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 16 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : FOUR
      USE CONSTANTS_P1,     ONLY : EPS_0
      USE PI_ETC,           ONLY : PI
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  UNIT
!
      REAL (WP)             ::  Q1,Q2,KS,R
      REAL (WP)             ::  V_YUKA_R
      REAL (WP)             ::  COEF
!
      REAL (WP)             ::  EXP
!
      IF(UNIT == 'SIU') THEN                                        !
        COEF = Q1 * Q2 / (FOUR * PI * EPS_0)                        !
      ELSE IF(UNIT == 'CGS') THEN                                   !
        COEF = Q1 * Q2                                              !
      END IF                                                        !
!
      V_YUKA_R = COEF * EXP(- KS * R) / R                           !
!
      END FUNCTION V_YUKA_R
!
!=======================================================================
!
      FUNCTION V_SOFT_R(S,EPS,R)
!
!  This function computes the soft-sphere interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * R        : point
!       * S        : \
!       * EPS      : /  parameters of the soft-sphere potential
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 16 Sep 2020
!
      IMPLICIT NONE
!
      REAL (WP)             ::  S,EPS,R
      REAL (WP)             ::  V_SOFT_R
!
      V_SOFT_R = EPS * (S / R)**12.0E0_WP                           !
!
      END FUNCTION V_SOFT_R
!
!=======================================================================
!
      FUNCTION V_LEJO_R(S,EPS,R)
!
!  This function computes the Lennard-Jones interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * R        : point
!       * S        : \
!       * EPS      : /  parameters of the Lennard-Jones potential
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 16 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : FOUR,SIX
!
      IMPLICIT NONE
!
      REAL (WP),INTENT(IN)  ::  S,EPS,R
      REAL (WP)             ::  V_LEJO_R
!
      V_LEJO_R = FOUR * EPS * ( (S / R)**12.0E0_WP - (S / R)**SIX ) !
!
      END FUNCTION V_LEJO_R
!
!=======================================================================
!
      FUNCTION V_HCLJ_R(S,EPS,RC,R)
!
!  This function computes the hard-core Lennard-Jones interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * R        : point
!       * S        : \
!       * EPS      : /  parameters of the Lennard-Jones potential
!       * RC       : hard-core radius
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Apr 2021
!
!
      USE REAL_NUMBERS,     ONLY : TWO,FOUR,SIX
!
      IMPLICIT NONE
!
      REAL (WP),INTENT(IN)  ::  S,EPS,RC,R
      REAL (WP)             ::  V_HCLJ_R
!
      REAL (WP)             ::  NUM,DEN
!
      NUM = S - TWO * RC
      DEN = R - TWO * RC
!
      V_HCLJ_R = FOUR * EPS * (                                   & !
                      (NUM / DEN)**12.0E0_WP - (NUM / DEN)**SIX   & !
                              )                                     !
!
      END FUNCTION V_HCLJ_R
!
!=======================================================================
!
      FUNCTION V_KIHA_R(S,EPS,M,N,R)
!
!  This function computes the Kihara interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * R        : point
!       * S        : \
!       * EPS      : /  parameters of the Lennard-Jones potential
!       * M        : \  exponents
!       * N        : /
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Apr 2021
!
!
!
      IMPLICIT NONE
!
      INTEGER, INTENT(IN)   ::  M,N
!
      REAL (WP),INTENT(IN)  ::  S,EPS,R
      REAL (WP)             ::  V_KIHA_R
!
      REAL (WP)             ::  FLOAT
!
      V_KIHA_R = EPS * (                                          & !
                 FLOAT(M) * (S / R)**N - FLOAT(N) * (S / R)**M    & !
                       ) /  FLOAT(N - M)                            !
!
      END FUNCTION V_KIHA_R
!
!=======================================================================
!
      FUNCTION V_MIEP_R(S,EPS,M,N,R)
!
!  This function computes the Mie interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * R        : point
!       * S        : \
!       * EPS      : /  parameters of the Lennard-Jones potential
!       * M        : \  exponents
!       * N        : /
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Apr 2021
!
!
!
      IMPLICIT NONE
!
      INTEGER, INTENT(IN)   ::  M,N
!
      REAL (WP),INTENT(IN)  ::  S,EPS,R
      REAL (WP)             ::  V_MIEP_R
!
      REAL (WP)             ::  K1,K2,K3,COEF
!
      REAL (WP)             ::  FLOAT
!
      K1   = FLOAT(N) / FLOAT(N - M)                                !
      K2   = FLOAT(N) / FLOAT(M)                                    !
      K3   = FLOAT(M) / FLOAT(N - M)                                !
      COEF = K1 * K2**K3                                            !
!
      V_MIEP_R = COEF * EPS * ( (S / R)**N - (S / R)**M )           !
!
      END FUNCTION V_MIEP_R
!
!=======================================================================
!
      FUNCTION V_VDWP_R(S,EPS,R)
!
!  This function computes the Van der Waals interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * R        : point
!       * S        : \
!       * EPS      : /  parameters of the Lennard-Jones potential
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Apr 2021
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,FOUR,SIX
!
      IMPLICIT NONE
!
      REAL (WP),INTENT(IN)  ::  S,EPS,R
      REAL (WP)             ::  V_VDWP_R
!
      REAL (WP)             ::  SR2
!
      REAL (WP)             ::  LOG
!
      SR2 = (S / R)**2                                              !
!
      V_VDWP_R = - EPS * (                                        & !
                   TWO * SR2 / (ONE - FOUR * SR2)  +  TWO * SR2 + & !
                   LOG( ONE - FOUR * SR2 )                        & !
                         ) / SIX                                    !
!
      END FUNCTION V_VDWP_R
!
!=======================================================================
!
      FUNCTION V_MORS_R(S,EPS,ALF,R)
!
!  This function computes the Morse interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * R        : point
!       * S        : \
!       * EPS      : /  parameters of the Lennard-Jones potential
!       * ALF      : potential stiffness
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Apr 2021
!
!
      USE REAL_NUMBERS,     ONLY : ONE
!
      IMPLICIT NONE
!
      REAL (WP),INTENT(IN)  ::  S,EPS,ALF,R
      REAL (WP)             ::  V_MORS_R
!
      REAL (WP)             ::  EXP
!
      V_MORS_R = EPS * ( ONE - EXP( ALF * (R - S) )  )              !
!
      END FUNCTION V_MORS_R
!
!=======================================================================
!
      FUNCTION V_GEXP_R(S,EPS,ALF,R)
!
!  This function computes the generalised exponential interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * R        : point
!       * S        : \
!       * EPS      : /  parameters of the Lennard-Jones potential
!       * ALF      : potential stiffness
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Apr 2021
!
!
      IMPLICIT NONE
!
      REAL (WP),INTENT(IN)  ::  S,EPS,ALF,R
      REAL (WP)             ::  V_GEXP_R
!
      REAL (WP)             ::  EXP
!
      V_GEXP_R = EPS * EXP( - ALF * (R / S) )                       !
!
      END FUNCTION V_GEXP_R
!
!=======================================================================
!
      FUNCTION V_EXP6_R(S,EPS,ALF,R)
!
!  This function computes the exp-6 interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * R        : point
!       * S        : \
!       * EPS      : /  parameters of the Lennard-Jones potential
!       * ALF      : potential stiffness
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Apr 2021
!
!
      USE REAL_NUMBERS,     ONLY : ONE,SIX
!
      IMPLICIT NONE
!
      REAL (WP),INTENT(IN)  ::  S,EPS,ALF,R
      REAL (WP)             ::  V_EXP6_R
!
      REAL (WP)             ::  EXP
!
      V_EXP6_R = EPS * (                                          & !
                 SIX * EXP( ALF * (ONE - R / S) )  -              & !
                 ALF * (S / R)**SIX                               & !
                       ) / (ALF - SIX)                              !
!
      END FUNCTION V_EXP6_R
!
!=======================================================================
!
      FUNCTION V_MBUC_R(S,EPS,ALF,R)
!
!  This function computes the modified Buckingham interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * R        : point
!       * S        : \
!       * EPS      : /  parameters of the Lennard-Jones potential
!       * ALF      : potential stiffness
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 SeAprp 2021
!
!
      USE REAL_NUMBERS,     ONLY : LARGE
!
      IMPLICIT NONE
!
      REAL (WP),INTENT(IN)  ::  S,EPS,ALF,R
      REAL (WP)             ::  V_MBUC_R
!
      REAL (WP)             ::  EXP
!
      IF(R <= S) THEN
        V_MBUC_R = V_EXP6_R(S,EPS,ALF,R) / ALF                      !
      ELSE                                                          !
        V_MBUC_R = LARGE                                            !
      END IF                                                        !
!
      END FUNCTION V_MBUC_R
!
!=======================================================================
!
      FUNCTION V_NCOU_R(UNIT,Q1,Q2,S,R)
!
!  This function computes the neutralised Coulomb interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * UNIT     : system unit
!                       UNIT  = 'SIU' international system
!                       UNIT  = 'CGS' CGS system
!       * Q1       : charge of particle 1
!       * Q2       : charge of particle 2
!       * S        : cut-off value
!       * R        : point
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Apr 2021
!
!
      USE REAL_NUMBERS,     ONLY : FOUR
      USE CONSTANTS_P1,     ONLY : EPS_0
      USE PI_ETC,           ONLY : PI
!
      USE EXT_FUNCTIONS,    ONLY : ERF,ERFC
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  UNIT
!
      REAL (WP), INTENT(IN) ::  Q1,Q2,S,R
      REAL (WP)             ::  V_NCOU_R
      REAL (WP)             ::  COEF
!
      IF(UNIT == 'SIU') THEN                                        !
        COEF = Q1 * Q2 / (FOUR * PI * EPS_0)                        !
      ELSE IF(UNIT == 'CGS') THEN                                   !
        COEF = Q1 * Q2                                              !
      END IF                                                        !
!
      V_NCOU_R = COEF * ( ERFC(R/S) + ERFC(R/S) ) / R               !
!
      END FUNCTION V_NCOU_R
!
!=======================================================================
!
      FUNCTION V_HACO_R(RC,R)
!
!  This function computes the hard-core interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * RC       : hard-core radius
!       * R        : point
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Apr 2021
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,LARGE
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  RC,R
      REAL (WP)             ::  V_HACO_R
!
      IF(R <= RC) THEN
        V_HACO_R = LARGE                                            !
      ELSE                                                          !
        V_HACO_R = ZERO                                             !
      END IF                                                        !
!
      END FUNCTION V_HACO_R
!
!=======================================================================
!
      FUNCTION V_PSPH_R(EPS,RC,R)
!
!  This function computes the hard-core interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * EPS      : value of the hard-core potential
!       * RC       : hard-core radius
!       * R        : point
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Apr 2021
!
!
      USE REAL_NUMBERS,     ONLY : ZERO
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EPS,RC,R
      REAL (WP)             ::  V_PSPH_R
!
      IF(R <= RC) THEN
        V_PSPH_R = EPS                                              !
      ELSE                                                          !
        V_PSPH_R = ZERO                                             !
      END IF                                                        !
!
      END FUNCTION V_PSPH_R
!
!=======================================================================
!
      FUNCTION V_STJO_R(S,EPS,ALF,A1,R)
!
!  This function computes the Starkloff-Joannopoulos soft-core interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * R        : point
!       * S        : \
!       * EPS      : /  parameters of the Lennard-Jones potential
!       * ALF      : potential stiffness
!       * A1       : magnitude of first term
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Apr 2021
!
      USE REAL_NUMBERS,     ONLY : ZERO,ONE
!
      IMPLICIT NONE
!
      REAL (WP),INTENT(IN)  ::  S,EPS,ALF,A1,R
      REAL (WP)             ::  V_STJO_R
!
      REAL (WP)             ::  V1,V2
      REAL (WP)             ::  NUM,DEN
!
      REAL (WP)             ::  EXP
!
      NUM = - A1 * ( ONE - EXP( - ALF * R ) )                       !
      DEN = R * ( ONE + EXP( ALF * (S - R) ) )                      !
      V1  = NUM / DEN                                               !
!
      IF(R <= S) THEN                                               !
        V2 = ZERO                                                   !
      ELSE                                                          !
        V2 = EPS * ( EXP( ALF * (S - R) ) - ONE )                   !
      END IF                                                        !
!
      V_STJO_R = V1 + V2                                            !
!
      END FUNCTION V_STJO_R
!
!=======================================================================
!
      FUNCTION V_LROS_R(S,EPS,DELTA,A1,A2,A3,A4,R)
!
!  This function computes the long-range oscillatory interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!  This potential of of the form
!
!  V(r) = EPS * EXP(A1 - A2 * R / S) - DELTA * (S/R)^3 * COS(A3 * [R/S + A4])
!
!  Input parameters:
!
!       * R        : point
!       * S        : \
!       * EPS      : /  parameters of the Lennard-Jones potential
!       * A1       : \
!       * A2       :  \
!       * A3       :  /
!       * A4       : /
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Apr 2021
!
      USE REAL_NUMBERS,     ONLY : THREE
!
      IMPLICIT NONE
!
      REAL (WP),INTENT(IN)  ::  S,EPS,DELTA,R
      REAL (WP),INTENT(IN)  ::  A1,A2,A3,A4
      REAL (WP)             ::  V_LROS_R
!
      REAL (WP)             ::  RT,TR,V1,V2
!
      REAL (WP)             ::  EXP,COS
!
      RT = R / S                                                    !
      TR = S / R                                                    !
!
      V1  = EPS * EXP( A1 - A2 * RT)                                !
      V2 = - DELTA * TR**THREE * COS( A3 * (RT + A4) )              !
!
      V_LROS_R = V1 + V2                                            !
!
      END FUNCTION V_LROS_R
!
!=======================================================================
!
      FUNCTION V_STOC_R(S,EPS,DELTA,R)
!
!  This function computes the Stockmayer interaction energy
!    between to particles in the r-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * R        : point
!       * S        : \
!       * EPS      :  >  parameters of the Stockmayer potential
!       * DELTA    : /
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Apr 2021
!
!
      USE REAL_NUMBERS,     ONLY : THREE,FOUR,SIX
!
      IMPLICIT NONE
!
      REAL (WP),INTENT(IN)  ::  S,EPS,DELTA,R
      REAL (WP)             ::  V_STOC_R
!
      REAL (WP)             ::  RT
!
      RT = S / R                                                    !
!
      V_STOC_R = FOUR * EPS * ( RT**12.0E0_WP - RT**SIX -         & !
                                DELTA * RT**THREE )                 !
!
      END FUNCTION V_STOC_R
!
END MODULE INTERACTION_POTENTIALS_R
