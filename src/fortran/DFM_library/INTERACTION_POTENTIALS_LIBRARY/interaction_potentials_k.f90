!
!=======================================================================
!
MODULE INTERACTION_POTENTIALS_K
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE INTERACT_POT_K_3D(INT_POT,UNIT,UNIK,Q1,Q2,C1,C2, &
                                   K,KS,VQ)
!
!  This subroutine computes interaction potentials in the
!    K-space in 3D.
!
!
!  Input parameters:
!
!       * INT_POT  : type of interaction potential (3D)
!                       INT_POT = 'COULO' Coulomb interaction
!                       INT_POT=  'YUKAW' Yukawa interaction
!                       INT_POT=  'RPAPO' RPA interaction
!                       INT_POT = 'OVER1' Overhauser interaction
!                       INT_POT = 'OVER2' modified Overhauser interaction
!                       INT_POT = 'DEUTS' Deutsch interaction
!                       INT_POT = 'PHOLE' particle-hole interaction
!                       INT_POT = 'KELBG' Kelbg interaction
!                       INT_POT = 'ASHPS' Ashcroft empty-core pseudopotential
!       * UNIT     : system unit
!                       UNIT  = 'SIU' international system
!                       UNIT  = 'CGS' CGS system
!       * UNIK     : K unit
!                       UNIK  = 'SI' international system
!                       UNIK  = 'AU' atomic units
!       * Q1       : charge of particle 1
!       * Q2       : charge of particle 2
!       * C1       : concentration of particles 1
!       * C2       : concentration of particles 2
!       * K        : momentum  (unit indifferent)
!       * KS       : screening momentum  (unit indifferent)
!
!
!  Output parameters:
!
!       * VQ       : interaction potential (energy)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 16 Sep 2020
!
!
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  UNIK
      CHARACTER (LEN = 3)   ::  UNIT
      CHARACTER (LEN = 5)   ::  INT_POT
!
      REAL (WP)             ::  Q1,Q2,C1,C2,KS,K
      REAL (WP)             ::  VQ
!
      IF(INT_POT == 'COULO') THEN                                   !
!
        VQ=V_COUL_K(UNIT,Q1,Q2,K)                                   !
!
      ELSE IF(INT_POT == 'YUKAW') THEN                              !
!
        VQ=V_YUKA_K(UNIT,Q1,Q2,KS,K)                                !
!
      ELSE IF(INT_POT == 'RPAPO') THEN                              !
!
        VQ=V_RPAP_K(UNIT,Q1,Q2,KS,K)                                !
!
      ELSE IF(INT_POT == 'OVER1') THEN                              !
!
        CONTINUE                                                    !
!
      ELSE IF(INT_POT == 'OVER2') THEN                              !
!
        VQ=V_OVE2_K(UNIT,UNIK,Q1,Q2,K)                              !
!
      ELSE IF(INT_POT == 'DEUTS') THEN                              !
!
        VQ=V_DEUT_K(UNIT,Q1,Q2,C1,C2,KS,K)                          !
!
      ELSE IF(INT_POT == 'PHOLE') THEN                              !
!
        CONTINUE                                                    !
!
      ELSE IF(INT_POT == 'KELBG') THEN                              !
!
        VQ=V_KELB_K(UNIT,Q1,Q2,KS,K)                                !
!
      END IF                                                        !
!
      END SUBROUTINE INTERACT_POT_K_3D
!
!=======================================================================
!
      FUNCTION V_COUL_K(UNIT,Q1,Q2,K)
!
!  This function computes the Coulomb interaction energy
!    between to particles in the K-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * UNIT     : system unit
!                       UNIT  = 'SIU' international system
!                       UNIT  = 'CGS' CGS system
!       * Q1       : charge of particle 1
!       * Q2       : charge of particle 2
!       * K        : momentum  (unit indifferent)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : FOUR
      USE CONSTANTS_P1,     ONLY : EPS_0
      USE PI_ETC,           ONLY : PI
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  UNIT
!
      REAL (WP)             ::  Q1,Q2,K
      REAL (WP)             ::  V_COUL_K
      REAL (WP)             ::  COEF
!
      IF(UNIT == 'SIU') THEN                                        !
        COEF=Q1*Q2/EPS_0                                            !
      ELSE IF(UNIT == 'CGS') THEN                                   !
        COEF=FOUR*PI*Q1*Q2                                          !
      END IF                                                        !
!
      V_COUL_K=COEF/(K*K)                                           !
!
      END FUNCTION V_COUL_K
!
!=======================================================================
!
      FUNCTION V_DEUT_K(UNIT,Q1,Q2,C1,C2,KS,K)
!
!  This function computes the Deutsch interaction energy
!    between to particles in the K-space in a given unit system (SI or CGS)
!
!  Warning: Two-component plasma only
!
!
!  References: (3) C. Deutsch, Phys. Lett. A 60, 317-318 (1977)
!
!
!  Input parameters:
!
!       * UNIT     : system unit
!                       UNIT  = 'SIU' international system
!                       UNIT  = 'CGS' CGS system
!       * Q1       : charge of particle 1
!       * Q2       : charge of particle 2
!       * C1       : concentration of particles 1
!       * C2       : concentration of particles 2
!       * KS       : screening momentum  (unit indifferent)
!       * K        : momentum  (unit indifferent)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,FOUR
      USE CONSTANTS_P1,     ONLY : EPS_0
      USE PI_ETC,           ONLY : PI
      USE SQUARE_ROOTS,     ONLY : SQR2
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  UNIT
!
      REAL (WP)             ::  Q1,Q2,C1,C2,KS,K
      REAL (WP)             ::  V_DEUT_K
      REAL (WP)             ::  COEF,AL1,AL2
!
      IF(UNIT == 'SIU') THEN                                        !
        COEF=Q1*Q2/EPS_0                                            !
      ELSE IF(UNIT == 'CGS') THEN                                   !
        COEF=FOUR*PI*Q1*Q2                                          !
      END IF                                                        !
!
      AL1=C1/SQR2 * SQRT( ONE - SQRT(ONE-FOUR*KS*KS/(C1*C1)) )      !
      AL2=C2/SQR2 * SQRT( ONE + SQRT(ONE-FOUR*KS*KS/(C2*C2)) )      !
!
      V_DEUT_K=COEF*(ONE/(K*K+AL1*AL1) - ONE/(K*K+AL2*AL2))         ! ref. (3) eq. (2)
!
      END FUNCTION V_DEUT_K
!
!=======================================================================
!
      FUNCTION V_KELB_K(UNIT,Q1,Q2,KS,K)
!
!  This function computes the Kelbg interaction energy
!    between to particles in the K-space in a given unit system (SI or CGS)
!
!
!  References: (5) W. Ebeling, V. E. Fortov and V. Filinov,
!                     "Quantum Statistics of Dense Gases and Nonideal Plasmas",
!                     Springer Series in Plasma Science and Technology,
!                     (Springer, 2017) p. 150
!
!
!  Input parameters:
!
!       * UNIT     : system unit
!                       UNIT  = 'SIU' international system
!                       UNIT  = 'CGS' CGS system
!       * Q1       : charge of particle 1
!       * Q2       : charge of particle 2
!       * KS       : screening momentum  (unit indifferent)
!       * K        : momentum  (unit indifferent)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : FOUR,HALF,FOURTH
      USE CONSTANTS_P1,     ONLY : EPS_0
      USE PI_ETC,           ONLY : PI
      USE EXT_FUNCTIONS,    ONLY : CONHYP                           ! Confluent hypergeometric
!                                                                   !   function 1F1(a,b;z)
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  UNIT
!
      REAL (WP)             ::  Q1,Q2,KS,K
      REAL (WP)             ::  V_KELB_K
      REAL (WP)             ::  COEF,ZZ
!
      COMPLEX (WP)          ::  A,B,Z
!
      IF(UNIT == 'SIU') THEN                                        !
        COEF=Q1*Q2/EPS_0                                            !
      ELSE IF(UNIT == 'CGS') THEN                                   !
        COEF=FOUR*PI*Q1*Q2                                          !
      END IF                                                        !
!
      ZZ=-FOURTH*K*K/(KS*KS)                                        !
!
!  Parameters/arguments of confluent hypergeometric function
!
      A=CMPLX(HALF,KIND=WP)                                         !
      B=CMPLX(1.5E0_WP,KIND=WP)                                     !
      Z=CMPLX(ZZ,KIND=WP)                                           !
!
      V_KELB_K=COEF * EXP(ZZ) *  REAL(CONHYP(A,B,Z,0,0),KIND=WP)    ! ref. (5) eq. (3.119)
!
      END FUNCTION V_KELB_K
!
!=======================================================================
!
      FUNCTION V_OVE2_K(UNIT,UNIK,Q1,Q2,K)
!
!  This function computes the Overhauser interaction energy
!    between to particles in the K-space in a given unit system (SI or CGS)
!
!
!  References: (2b) I. Nagy and P. M. Echenique, Phys. Rev. B 85, 115131 (2012)
!
!
!  Input parameters:
!
!       * UNIT     : system unit
!                       UNIT  = 'SIU' international system
!                       UNIT  = 'CGS' CGS system
!       * UNIK     : K unit
!                       UNIK  = 'SI' international system
!                       UNIK  = 'AU' atomic units
!       * Q1       : charge of particle 1
!       * Q2       : charge of particle 2
!       * K        : momentum  (unit indifferent)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,FOUR,HALF,THIRD
      USE CONSTANTS_P1,     ONLY : EPS_0
      USE FERMI_SI,         ONLY : KF_SI
      USE FERMI_AU,         ONLY : KF_AU
      USE PI_ETC,           ONLY : PI
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  UNIK
      CHARACTER (LEN = 3)   ::  UNIT
!
      REAL (WP)             ::  Q1,Q2,K
      REAL (WP)             ::  V_OVE2_K
      REAL (WP)             ::  COEF,X
!
      IF(UNIT == 'SIU') THEN                                        !
        COEF=Q1*Q2/EPS_0                                            !
      ELSE IF(UNIT == 'CGS') THEN                                   !
        COEF=FOUR*PI*Q1*Q2                                          !
      END IF                                                        !
!
      IF(UNIK == 'SI') THEN                                         !
        X=HALF*K/KF_SI                                              !
      ELSE IF(UNIK == 'AU') THEN                                    !
        X=HALF*K/KF_AU                                              !
      END IF                                                        !
!
      IF(X <= ONE) THEN                                             !
        V_OVE2_K=COEF*1.5E0_WP*X*(ONE-THIRD*X*X)                    ! ref. (2b) eq. (6)
      ELSE                                                          !
        V_OVE2_K=COEF/(K*K)                                         !
      END IF                                                        !
!
      END FUNCTION V_OVE2_K
!
!=======================================================================
!
      FUNCTION V_RPAP_K(UNIT,Q1,Q2,KS,K)
!
!  This function computes the RPA interaction energy
!    between to particles in the K-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * UNIT     : system unit
!                       UNIT  = 'SIU' international system
!                       UNIT  = 'CGS' CGS system
!       * Q1       : charge of particle 1
!       * Q2       : charge of particle 2
!       * KS       : screening momentum  (unit indifferent)
!       * K        : momentum  (unit indifferent)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,FOUR,HALF,FOURTH
      USE CONSTANTS_P1,     ONLY : EPS_0
      USE PI_ETC,           ONLY : PI
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  UNIT
!
      REAL (WP)             ::  Q1,Q2,KS,K
      REAL (WP)             ::  V_RPAP_K
      REAL (WP)             ::  COEF,X,GX
!
      IF(UNIT == 'SIU') THEN                                        !
        COEF=Q1*Q2/EPS_0                                            !
      ELSE IF(UNIT == 'CGS') THEN                                   !
        COEF=FOUR*PI*Q1*Q2                                          !
      END IF                                                        !
!
      X=K/KS                                                        !
!
!  Computing Lindhard function g(x)
!
      GX=HALF - HALF * (ONE+FOURTH*X*X) *                         & !
                        LOG(ABS((TWO+X)/(TWO-X))) / X               !
!
      V_RPAP_K=COEF*(K*K + KS*KS*GX)                                !
!
      END FUNCTION V_RPAP_K
!
!=======================================================================
!
      FUNCTION V_YUKA_K(UNIT,Q1,Q2,KS,K)
!
!  This function computes the Yukawa interaction energy
!    between to particles in the K-space in a given unit system (SI or CGS)
!
!
!  Input parameters:
!
!       * UNIT     : system unit
!                       UNIT  = 'SIU' international system
!                       UNIT  = 'CGS' CGS system
!       * Q1       : charge of particle 1
!       * Q2       : charge of particle 2
!       * KS       : screening momentum  (unit indifferent)
!       * K        : momentum  (unit indifferent)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : FOUR
      USE CONSTANTS_P1,     ONLY : EPS_0
      USE PI_ETC,           ONLY : PI
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  UNIT
!
      REAL (WP)             ::  Q1,Q2,KS,K
      REAL (WP)             ::  V_YUKA_K
      REAL (WP)             ::  COEF
!
      IF(UNIT == 'SIU') THEN                                        !
        COEF=Q1*Q2/EPS_0                                            !
      ELSE IF(UNIT == 'CGS') THEN                                   !
        COEF=FOUR*PI*Q1*Q2                                          !
      END IF                                                        !
!
      V_YUKA_K=COEF/(K*K + KS*KS)                                   !
!
      END FUNCTION V_YUKA_K
!
END MODULE INTERACTION_POTENTIALS_K
