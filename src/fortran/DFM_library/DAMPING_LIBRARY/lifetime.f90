!
!=======================================================================
!
MODULE LIFETIME 
!
      USE ACCURACY_REAL
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE LIFETIME_COEF(X,LFT)
!
!  This subroutine computes the lifetime of a quasiparticle 
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!
!
!  Output parameters:
!
!       * LFT      : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Nov 2020
!
!
      USE MATERIAL_PROP,          ONLY : DMN,RS
      USE EXT_FIELDS,             ONLY : T
!
      USE REAL_NUMBERS,           ONLY : TWO,HALF
      USE CONSTANTS_P1,           ONLY : H_BAR,M_E 
      USE FERMI_SI,               ONLY : KF_SI
!
      USE SCREENING_TYPE          
      USE DAMPING_VALUES,         ONLY : LT_TYPE
      USE CLASSICAL_FLUID_VALUES, ONLY : SL_TYPE
      USE EL_ELE_INTER,           ONLY : S,EPS
!
      USE SCREENING_VEC
      USE SCATTERING_LENGTH
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)  ::  X
      REAL (WP), INTENT(OUT) ::  LFT
!
      REAL (WP)              ::  Q_SI
      REAL (WP)              ::  EK
      REAL (WP)              ::  A_SC,KS_SI
!
      Q_SI = TWO * KF_SI * X                                        ! q in SI
!
!  Computing the quasiparticle energy
!
      EK = HALF * H_BAR* H_BAR * Q_SI * Q_SI / M_E                  !
!
!  Computing the screening vector
!
      CALL SCREENING_VECTOR(SC_TYPE,'3D',X,RS,T,KS_SI)              !
!
      IF(DMN == '3D') THEN                                          !
        A_SC = SCAT_LENGTH_3D(EPS,S,Q_SI,KS_SI,SL_TYPE)             !
        CALL LIFETIME_3D(EK,RS,T,A_SC,LT_TYPE,LFT)                  !
      ELSE IF(DMN == '2D') THEN                                     !
        CONTINUE                                                    ! not yet implemented
      ELSE IF(DMN == '1D') THEN                                     !
        CONTINUE                                                    ! not yet implemented
      END IF                                                        !
!
      END SUBROUTINE LIFETIME_COEF
!
!------ 1) 3D case --------------------------------------------
!
!=======================================================================
!
      SUBROUTINE LIFETIME_3D(EK,RS,T,A_SC,LT_TYPE,TAU)
!
!  This subroutine computes the lifetime of a quasiparticle 
!    in a  3D systems.
!
!  Input parameters:
!
!       * EK       : quasiparticle energy in SI
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature   in SI
!       * A_SC     : quasiparticle scattering length in SI
!       * LT_TYPE  : approximation used
!                       LT_TYPE = 'QUFE' Quinn-Ferrell formula
!                       LT_TYPE = 'GIVI' Giuliani-Vignale formula 
!                       LT_TYPE = 'DAVI' Davies formula
!                       LT_TYPE = 'QIVI' Qian-Vignale formula
!                       LT_TYPE = 'INPE' Inogamov-Petrov formula
!                       LT_TYPE = 'LUBR' Lugovskoy-Bray formula
!                       LT_TYPE = 'GALI' Galitskii formula
!                       LT_TYPE = 'NAEC' Nagy-Echenique formula
!                       LT_TYPE = 'GIQU' Giuliani-Quinn formula
!
!
!  Output parameters:
!
!       * TAU      : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  LT_TYPE
!
      REAL (WP)             ::  EK,RS,T,A_SC
      REAL (WP)             ::  TAU
!
      IF(LT_TYPE == 'QUFE') THEN                                     !
        TAU = QUFE_LT_3D(EK,RS)                                      !
      ELSE IF(LT_TYPE == 'GIVI') THEN                                !
        TAU = GIVI_LT_3D(EK,RS,T)                                    !
      ELSE IF(LT_TYPE == 'DAVI') THEN                                !
        TAU = DAVI_LT_3D(EK,T)                                       !
      ELSE IF(LT_TYPE == 'QIVI') THEN                                !
        TAU = QIVI_LT_3D(EK,T)                                       !
      ELSE IF(LT_TYPE == 'INPE') THEN                                !
        TAU = INPE_LT_3D(EK,T)                                       !
      ELSE IF(LT_TYPE == 'LUBR') THEN                                !
        TAU = LUBR_LT_3D(EK,T)                                       !
      ELSE IF(LT_TYPE == 'GALI') THEN                                !
        TAU = GALI_LT_3D(EK,A_SC,RS)                                 !
      ELSE IF(LT_TYPE == 'NAEC') THEN                                !
        TAU = NAEC_LT_3D(EK,RS)                                      !
      ELSE IF(LT_TYPE == 'GIQU') THEN                                !
        TAU = GIQU_LT_3D(EK,T)                                       !
      END IF                                                         !
!
      END SUBROUTINE LIFETIME_3D  
!
!=======================================================================
!
      FUNCTION DAVI_LT_3D(EK,T)
!
!  This function computes Davies approximation for 
!    the quasiparticle lifetime in the 3D case
!
!  References: (1) R. W. Davies, J. Phys. Chem. Solids 28, 
!                     1001-1008 (1967)
!
!  Input parameters:
!
!       * EK       : quasiparticle energy in SI
!       * T        : system temperature   in SI
!
!  Output parameters:
!
!       * DAVI_LT_3D : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,FOUR
      USE CONSTANTS_P1,        ONLY : H_BAR,E,K_B
      USE FERMI_SI,            ONLY : EF_SI,KF_SI
      USE PI_ETC,              ONLY : PI
      USE SCREENING_VEC,       ONLY : THOMAS_FERMI_VECTOR
      USE CHEMICAL_POTENTIAL,  ONLY : MU
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,T
      REAL (WP)             ::  DAVI_LT_3D
      REAL (WP)             ::  UAT
      REAL (WP)             ::  R4,K_TF_SI
      REAL (WP)             ::  CP,DELTA,EMKT,GAMMA
!
      REAL (WP)             ::  ATAN
!
!  Computing the Thomas-Fermi momentum
!
      CALL THOMAS_FERMI_VECTOR('3D',K_TF_SI)                        !
!
      R4    = KF_SI / K_TF_SI                                       !
      CP    = MU('3D',T)                                            ! chemical potential
      DELTA = EK - CP                                               !
      EMKT  = DELTA**2 + (PI * K_B * T)**2                          !
!
      GAMMA = E * E * R4 * ( ATAN(TWO * R4) +                     & !
                             TWO * R4 / (ONE + FOUR * R4 * R4) ) *& ! ref. (1) eq. (A.10)
                            R4 * EMKT / (32.0E0_WP * EF_SI * EF_SI) !
!
      UAT = TWO * GAMMA / H_BAR                                     ! ref. (1) eq. (28)
!
      DAVI_LT_3D = ONE / UAT                                        !
!
      END FUNCTION DAVI_LT_3D  
!
!=======================================================================
!
      FUNCTION GALI_LT_3D(EK,A_SC,RS)
!
!  This function computes Galitskii approximation for 
!    the quasiparticle lifetime in the 3D case
!
!  References: (1) I. Nagy and P. M. Echenique, Phys. Rev. B 85, 
!                     115131 (2012)
!
!  Input parameters:
!
!       * EK       : quasiparticle energy in SI
!       * A_SC     : quasiparticle scattering length in SI
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!  Output parameters:
!
!       * GALI_LT_3D : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,THREE,FOUR,FIVE,  & 
                                      SEVEN,EIGHT,HALF
      USE CONSTANTS_P1,        ONLY : H_BAR,M_E
      USE FERMI_SI,            ONLY : EF_SI,VF_SI
      USE PI_ETC,              ONLY : PI
      USE UTILITIES_1,         ONLY : RS_TO_N0
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,A_SC,RS
      REAL (WP)             ::  GALI_LT_3D
      REAL (WP)             ::  UAT
      REAL (WP)             ::  R1,V
      REAL (WP)             ::  N0
!
      REAL (WP)             ::  SQRT
!
      N0 = RS_TO_N0('3D',RS)                                        !
!
      R1 = EK / EF_SI                                               !
      V  = FOUR * PI * A_SC * H_BAR * H_BAR / M_E                   ! interaction potential
!                                                                   ! 
      UAT = N0 * THREE * VF_SI * ( EIGHT * V * V /                & !
                                   (15.0E0_WP * SQRT(R1)) *   (   & !
                                   (TWO - R1)**2.5E0_WP +         & ! ref. (1) eq. (1)
                                    HALF * (FIVE * R1 - SEVEN) )  & !
                                 ) / (16.0E0_WP * PI)               !
!
      GALI_LT_3D = ONE / UAT                                        !
!
      END FUNCTION GALI_LT_3D  
!=======================================================================
!
      FUNCTION GIQU_LT_3D(EK,T)
!
!  This function computes Giuliani-Quinn approximation for 
!    the quasiparticle lifetime in the 3D case
!
!  References: (1) G. F. Giuliani and J. J. Quinn, Phys. Rev. B 26, 
!                     4421-4428 (1982)
!
!  Input parameters:
!
!       * EK       : quasiparticle energy in SI
!       * T        : system temperature   in SI
!
!  Output parameters:
!
!       * GIQU_LT_3D : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,FOURTH
      USE CONSTANTS_P1,        ONLY : H_BAR,E
      USE FERMI_SI,            ONLY : EF_SI,KF_SI
      USE SCREENING_VEC,       ONLY : THOMAS_FERMI_VECTOR
      USE CHEMICAL_POTENTIAL,  ONLY : MU
!
      IMPLICIT NONE
!
      REAL (WP)             ::  EK,T
      REAL (WP)             ::  GIQU_LT_3D
      REAL (WP)             ::  UAT
      REAL (WP)             ::  R4,K_TF_SI
      REAL (WP)             ::  CP,DELTA
!
      REAL (WP)             ::  ATAN
!
      CP = MU('3D',T)                                               ! chemical potential
!
      DELTA = EK - CP                                               !
!
!  Computing the Thomas-Fermi momentum
!
      CALL THOMAS_FERMI_VECTOR('3D',K_TF_SI)                        !
!
      R4 = KF_SI / K_TF_SI                                          ! k_F / k_TF
!
      UAT = E * E * KF_SI * ( ONE / (ONE + FOURTH / (R4 * R4)) +  & !
                              TWO * R4 * ATAN(TWO * R4)           & ! 
                            )   *                                 & ! ref. (1) eq. (C1)
                             (DELTA / EF_SI)**2                   & !
                           / (32.0E0_WP * H_BAR)                    !
!
      GIQU_LT_3D = ONE / UAT                                        !
!
      END FUNCTION GIQU_LT_3D  
!
!=======================================================================
!
      FUNCTION GIVI_LT_3D(EK,RS,T)
!
!  This function computes Giuliani-Vignale approximation for 
!    the quasiparticle lifetime in the 3D case
!
!  References: (1) G. Giuliani and G. Vignale, "Quantum Theory of the 
!                     Electron Liquid", Cambridge Uiversity Press (2005)
!
!  Input parameters:
!
!       * EK       : quasiparticle energy in SI
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature   in SI
!
!  Output parameters:
!
!       * GIVI_LT_3D : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,EIGHT,HALF,FOURTH
      USE CONSTANTS_P1,        ONLY : H_BAR,K_B
      USE FERMI_SI,            ONLY : EF_SI
      USE PI_ETC,              ONLY : PI
      USE UTILITIES_1,         ONLY : ALFA
      USE CHEMICAL_POTENTIAL,  ONLY : MU
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,RS,T
      REAL (WP)             ::  GIVI_LT_3D
      REAL (WP)             ::  UAT
      REAL (WP)             ::  R3,ALPHA
      REAL (WP)             ::  CP,DELTA,EMKT,ZETA,EXPO
!
      REAL (WP)             ::  SQRT,TAN,EXP
!
      ALPHA = ALFA('3D')                                            !
!
      R3    = PI / (ALPHA * RS)                                     !
      CP    = MU('3D',T)                                            ! chemical potential
      DELTA = EK - CP                                               !
      EMKT  = DELTA**2 + (PI * K_B * T)**2                          !
!
      ZETA = SQRT(FOURTH / R3) * TAN(SQRT(R3)) + HALF / (ONE + R3)  ! ref. (1) eq. (8.92)
      EXPO = EXP(- DELTA / (K_B * T))                               !
!
      UAT = PI * EMKT * ZETA /                                    & ! ref. (1) eq. (8.93)
            (EIGHT * H_BAR * EF_SI * (ONE + EXPO))                  !
!
      GIVI_LT_3D = ONE / UAT                                        !
!
      END FUNCTION GIVI_LT_3D  
!
!=======================================================================
!
      FUNCTION INPE_LT_3D(EK,T)
!
!  This function computes Inogamov-Petrov approximation for 
!    the quasiparticle lifetime in the 3D case
!
!  References: (1) N. A. Inogamov and Yu. V. Petrov, JETP 110, 505-529 (2010)
!
!  Input parameters:
!
!       * EK       : quasiparticle energy in SI
!       * T        : system temperature   in SI
!
!  Output parameters:
!
!       * INPE_LT_3D : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,SEVEN,EIGHT
      USE CONSTANTS_P1,        ONLY : H_BAR,M_E,E,K_B
      USE PI_ETC,              ONLY : PI
      USE SCREENING_VEC,       ONLY : THOMAS_FERMI_VECTOR
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,T
      REAL (WP)             ::  INPE_LT_3D
      REAL (WP)             ::  UAT
      REAL (WP)             ::  R4,COEF1,COEF2
      REAL (WP)             ::  K_TF_SI,K1
      REAL (WP)             ::  G,ETA
!
      REAL (WP)             ::  SQRT,ATAN
!
!  Computing the Thomas-Fermi momentum
!
      CALL THOMAS_FERMI_VECTOR('3D',K_TF_SI)                        !
!
      K1 = SQRT(TWO * M_E * EK) / H_BAR                             ! k 
      R4 = K1 / K_TF_SI                                             ! k / k_TF
!
      COEF1 = M_E * M_E * M_E * E * E * E * E                       ! m^3 * e^4
      COEF2 = H_BAR**SEVEN * K1 * K1 * K1 * K1                      ! h_bar^7 * k^4
      ETA   = TWO * R4                                              !
      G     = ETA**4 / (ONE + ETA * ETA) + ETA**3 * ATAN(ETA)       !
!
      UAT = PI * COEF1 * G *K_B * T * K_B * T / (EIGHT * COEF2)     ! ref. (1) eq. (24)
!
      INPE_LT_3D = ONE / UAT                                        !
!
      END FUNCTION INPE_LT_3D  
!
!=======================================================================
!
      FUNCTION LUBR_LT_3D(EK,T)
!
!  This function computes Lugovskoy-Bray approximation for 
!    the quasiparticle lifetime in the 3D case
!
!  References: (1) 
!
!  Input parameters:
!
!       * EK       : quasiparticle energy in SI
!       * T        : system temperature   in SI
!
!  Output parameters:
!
!       * LUBR_LT_3D : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,SEVEN,FOUR
      USE CONSTANTS_P1,        ONLY : H_BAR,M_E,E,K_B
      USE PI_ETC,              ONLY : PI,PI2
      USE SCREENING_VEC,       ONLY : THOMAS_FERMI_VECTOR
      USE CHEMICAL_POTENTIAL,  ONLY : MU
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,T
      REAL (WP)             ::  LUBR_LT_3D
      REAL (WP)             ::  UAT
      REAL (WP)             ::  R4,COEF1,COEF2
      REAL (WP)             ::  K_TF_SI,K1
      REAL (WP)             ::  CP,DELTA
      REAL (WP)             ::  G,ETA
!
      REAL (WP)             ::  SQRT,ATAN
!
!  Computing the Thomas-Fermi momentum
!
      CALL THOMAS_FERMI_VECTOR('3D',K_TF_SI)                        !
!
      CP = MU('3D',T)                                               ! chemical potential
!
      DELTA = EK - CP                                               !
!
      K1 = SQRT(TWO * M_E * EK) / H_BAR                             ! k 
      R4 = K1 / K_TF_SI                                             ! k / k_TF
!
      COEF1 = M_E * M_E * M_E * E * E * E * E                       ! m^3 * e^4
      COEF2 = H_BAR**SEVEN * K1 * K1 * K1 * K1                      ! h_bar^7 * k^4
      ETA   = TWO * R4                                              !
      G     = ETA**4 / (ONE + ETA * ETA) + ETA**3 * ATAN(ETA) -   & !
              ATAN(ETA * SQRT(ETA * ETA + TWO)) /                 & !
              DSQRT(ETA * ETA + TWO)                                !
!
      UAT = PI * COEF1 * G / (FOUR * COEF2)  *   (                & ! 
                             (K_B * T)**2 + DELTA * DELTA / PI2   & !
                                                 )                  !
!
      LUBR_LT_3D = ONE / UAT                                        !
!
      END FUNCTION LUBR_LT_3D  
!
!=======================================================================
!
      FUNCTION NAEC_LT_3D(EK,RS)
!
!  This function computes Nagy_Echenique approximation for 
!    the quasiparticle lifetime in the 3D case
!
!  References: (1) I. Nagy and P. M. Echenique, Phys. Rev. B 85, 
!                     115131 (2012)
!
!  Input parameters:
!
!       * EK       : quasiparticle energy in SI
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!  Output parameters:
!
!       * NAEC_LT_3D : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,THREE,FOUR,EIGHT,THIRD
      USE FERMI_SI,            ONLY : EF_SI,KF_SI,VF_SI
      USE PI_ETC,              ONLY : PI
      USE UTILITIES_1,         ONLY : RS_TO_N0
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,RS
      REAL (WP)             ::  NAEC_LT_3D
      REAL (WP)             ::  UAT,KF2
      REAL (WP)             ::  R1,G1,G2
      REAL (WP)             ::  N0
!
      REAL (WP)             ::  LOG,SQRT,ABS
!
      N0  = RS_TO_N0('3D',RS)                                       !
      KF2 = KF_SI * KF_SI                                           !
!
      R1 = EK / EF_SI                                               !
      G1 = LOG(R1 - ONE) + EIGHT * THIRD - TWO * LOG(TWO)           !
      G2 = TWO * THIRD * (TWO - R1)**1.5E0_WP +                   & !
           TWO * SQRT(TWO - R1) +                                 & !
           LOG(ABS((SQRT(TWO - R1) - ONE) / (SQRT(TWO - R1) + ONE)))!
!
      UAT = N0 * THREE * VF_SI *( (THREE * PI / KF2)**2 *         & ! ref. (1) eq. (7)
                                   FOUR * (G1 - G2) * THIRD /     & !
                                   SQRT(R1)                       & ! 
                                )                                   ! pb of units !
!
      NAEC_LT_3D = ONE / UAT                                        !
!
      END FUNCTION NAEC_LT_3D  
!
!=======================================================================
!
      FUNCTION QIVI_LT_3D(EK,T)
!
!  This function computes Qian-Vignale approximation for 
!    the quasiparticle lifetime in the 3D case
!
!  References: (1) Z. Qian and G. Vignale, Phys. Rev. B 71, 
!                      075112 (2005)
!
!  Input parameters:
!
!       * EK       : quasiparticle energy in SI
!       * T        : system temperature   in SI
!
!  Output parameters:
!
!       * QIVI_LT_3D : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,HALF
      USE CONSTANTS_P1,        ONLY : H_BAR,M_E,E,K_B
      USE FERMI_SI,            ONLY : KF_SI
      USE PI_ETC,              ONLY : PI
      USE SCREENING_VEC,       ONLY : THOMAS_FERMI_VECTOR
      USE CHEMICAL_POTENTIAL,  ONLY : MU
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,T
      REAL (WP)             ::  QIVI_LT_3D
      REAL (WP)             ::  UAT
      REAL (WP)             ::  COEF1,COEF2
      REAL (WP)             ::  K_TF_SI,KS_SI,K
      REAL (WP)             ::  CP,DELTA,EMKT,LAMBDA,EXPO
      REAL (WP)             ::  UAT_E,UAT_D
!
      REAL (WP)             ::  SQRT,EXP,ATAN
!
!  Computing the Thomas-Fermi momentum
!
      CALL THOMAS_FERMI_VECTOR('3D',K_TF_SI)                        !
!
      CP = MU('3D',T)                                               ! chemical potential
!
      DELTA = EK - CP                                               !
!
      EMKT = DELTA**2 + (PI * K_B * T)**2                           !
!
      KS_SI  = K_TF_SI                                              !
      K      = SQRT(TWO * M_E * EK / (H_BAR * H_BAR))               ! quasiparticle k in SI
      COEF1  = M_E * M_E * M_E * E * E * E * E                      ! m^3 * e^4
      COEF2  = H_BAR *K * KS_SI * KS_SI * KS_SI                     ! p * k_s^3
      LAMBDA = TWO * KF_SI / KS_SI                                  !
      EXPO   = EXP(- DELTA / (K_B * T))                             !
!
      UAT_E = - COEF1 / (PI * COEF2) *                            & !
                EMKT / (ONE + EXPO) * ONE /                       & !
                SQRT(LAMBDA * LAMBDA + TWO) *                     & ! ref. (1) eq. (32)
               ( HALF * PI -                                      & !
                 ATAN(ONE / (LAMBDA * SQRT(LAMBDA * LAMBDA + TWO)))&!
               )                                                    !
!
      UAT_D = PI * COEF1 / (TWO * COEF2) * (K_B * T)**2 *         & ! ref. (1) eq. (33)
               (LAMBDA / (LAMBDA * LAMBDA + ONE) + ATAN(LAMBDA))    !
!
      UAT = UAT_D + UAT_E                                           ! ref. (1) eq. (3)
!
      QIVI_LT_3D = ONE / UAT                                        !
!
      END FUNCTION QIVI_LT_3D  
!
!=======================================================================
!
      FUNCTION QUFE_LT_3D(EK,RS)
!
!  This function computes Quinn-Ferrel approximation for 
!    the quasiparticle lifetime in the 3D case
!
!  References: (1) W. S. Fann et al, Phys. Rev. B 46, 13592-13595 (1992)
!                     4421-4428 (1982)
!
!  Input parameters:
!
!       * EK       : quasiparticle energy in SI
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!  Output parameters:
!
!       * QUFE_LT_3D : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,THREE
      USE CONSTANTS_P1,        ONLY : H_BAR
      USE FERMI_SI,            ONLY : EF_SI
      USE PI_ETC,              ONLY : PI2
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,RS
      REAL (WP)             ::  QUFE_LT_3D
      REAL (WP)             ::  UAT
      REAL (WP)             ::  R1
!
      REAL (WP)             ::  SQRT
!
      R1 = EK / EF_SI                                               !
!
      UAT = PI2 * SQRT(THREE) * ENE_P_SI * (R1 - ONE) *           & !
                (R1 - ONE) / (128.0E0_WP * H_BAR)                   ! ref. (1) eq. (1)-(2)
!
      QUFE_LT_3D = ONE / UAT                                        !
!
      END FUNCTION QUFE_LT_3D  
!
!------ 2) 2D case --------------------------------------------
!
!
!=======================================================================
!
      SUBROUTINE LIFETIME_2D(EK,RS,T,A_SI,LT_TYPE,TAU)
!
!  This subroutine computes the lifetime of a quasiparticle 
!    in a  2D systems.
!
!  Input parameters:
!
!       * EK       : quasiparticle energy in SI
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature   in SI
!       * A_SI     : layer interspacing   in SI
!       * LT_TYPE  : approximation used
!                       LT_TYPE = 'GIVI' Giuliani-Vignale formula
!                       LT_TYPE = 'GIQ1' Giuliani-Quinn formula for e-h loss
!                       LT_TYPE = 'GIQ2' Giuliani-Quinn formula for plasmon loss
!                       LT_TYPE = 'QIVI' Qian-Vignale formula
!                       LT_TYPE = 'MELA' Menashe-Laikhtman formula
!                       LT_TYPE = 'HAWR' Hawrylak formula
!
!
!  Output parameters:
!
!       * TAU      : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 June 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)    ::  LT_TYPE
!
      REAL (WP), INTENT(IN)  ::  EK,RS,T,A_SI
      REAL (WP), INTENT(OUT) ::  TAU
!
      IF(LT_TYPE == 'GIVI') THEN                                    !
        TAU = GIVI_LT_2D(EK,RS,T)                                   !
      ELSE IF(LT_TYPE == 'GIQ1') THEN                               !
        TAU = GIQ1_LT_2D(EK,T)                                      !
      ELSE IF(LT_TYPE == 'GIQ2') THEN                               !
        TAU = GIQ2_LT_2D(EK,T)                                      !
      ELSE IF(LT_TYPE == 'QIVI') THEN                               !
        TAU = QIVI_LT_2D(EK,RS,T)                                   !
      ELSE IF(LT_TYPE == 'MELA') THEN                               !
        TAU = MELA_LT_2D(EK,T)                                      !
      ELSE IF(LT_TYPE == 'HAWR') THEN                               !
        TAU = HAWR_LT_2D(EK,A_SI,RS)                                !
      END IF                                                        !
!
      END SUBROUTINE LIFETIME_2D  
!
!=======================================================================
!
      FUNCTION GIQ1_LT_2D(EK,T)
!
!  This function computes Giuliani-Quinn approximation for 
!    the quasiparticle lifetime in the 2D case
!
!  In this approximation, the lifetime is limited by the decay into 
!    an electron-hole pair
!
!  References: (1) G. F. Giuliani and J. J. Quinn, Phys. Rev. B 26, 
!                     4421-4428 (1982)
!
!  Input parameters:
!
!       * EK       : quasiparticle energy in SI
!       * T        : system temperature   in SI
!
!  Output parameters:
!
!       * GIQ1_LT_2D : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,HALF,FOUR
      USE CONSTANTS_P1,        ONLY : H_BAR,K_B
      USE FERMI_SI,            ONLY : EF_SI,KF_SI
      USE PI_ETC,              ONLY : PI
      USE SCREENING_VEC,       ONLY : THOMAS_FERMI_VECTOR
      USE CHEMICAL_POTENTIAL,  ONLY : MU
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,T
      REAL (WP)             ::  GIQ1_LT_2D
      REAL (WP)             ::  UAT
      REAL (WP)             ::  R1,R2,K_TF_SI
      REAL (WP)             ::  CP,DELTA
      REAL (WP)             ::  SMALL
!
      REAL (WP)             ::  LOG
!
      SMALL = 1.E-1_WP                                              !
!
!  Computing the Thomas-Fermi momentum
!
      CALL THOMAS_FERMI_VECTOR('3D',K_TF_SI)                        !
!
      CP    = MU('2D',T)                                            ! chemical potential
      DELTA = EK - CP                                               !
      R1    = DELTA / EF_SI                                         !
      R2    = K_B * T / EF_SI                                       !
!                                                                   ! decay into e-h pair
      IF(T <= SMALL) THEN                                           !
!
        UAT = - EF_SI * R1 * R1 *( LOG(R1) - HALF -               & !
                                   LOG(TWO * K_TF_SI / KF_SI)     & ! ref. (1) eq. (13)
                                 ) / (FOUR * PI * H_BAR)            !
!
      ELSE                                                          !
! 
        UAT = - EF_SI * R2 * R2*( LOG(R2) - LOG(K_TF_SI / KF_SI) -& !
                                  LOG(TWO) - ONE ) /              & ! ref. (1) eq. (14)
                             (TWO * PI * H_BAR)                     !
!
      END IF                                                        !
!
      GIQ1_LT_2D = ONE / UAT                                        !
!
      END FUNCTION GIQ1_LT_2D  
!
!=======================================================================
!
      FUNCTION GIQ2_LT_2D(EK,T)
!
!  This function computes Giuliani-Quinn approximation for 
!    the quasiparticle lifetime in the 2D case. 
!
!  In this approximation, the lifetime is limited by the decay into 
!    a plasmon mode
!
!  References: (1) G. F. Giuliani and J. J. Quinn, Phys. Rev. B 26, 
!                     4421-4428 (1982)
!
!  Input parameters:
!
!       * EK       : quasiparticle energy in SI
!       * T        : system temperature   in SI
!
!  Output parameters:
!
!       * GIQ2_LT_2D : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO
      USE CONSTANTS_P1,        ONLY : H_BAR,E,M_E
      USE FERMI_SI,            ONLY : EF_SI
      USE CHEMICAL_POTENTIAL,  ONLY : MU
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,T
      REAL (WP)             ::  GIQ2_LT_2D
      REAL (WP)             ::  UAT
      REAL (WP)             ::  R1
      REAL (WP)             ::  CP,DELTA
!
      REAL (WP)             ::  SQRT
!
      CP    = MU('2D',T)                                            ! chemical potential
      DELTA = EK - CP                                               !
      R1    = DELTA / EF_SI                                         !
!                                                                   ! decay into plasmon mode
      UAT = TWO * E * E * E * E * M_E * DSQRT(R1) /               & !
                 (H_BAR * H_BAR * H_BAR)                            ! ref. (1) eq. (22)
!
      GIQ2_LT_2D = ONE / UAT                                        !
!
      END FUNCTION GIQ2_LT_2D  
!
!=======================================================================
!
      FUNCTION GIVI_LT_2D(EK,RS,T)
!
!  This function computes Giuliani-Vignale approximation for 
!    the quasiparticle lifetime in the 2D case
!
!  References: (1) G. Giuliani and G. Vignale, "Quantum Theory of the 
!                     Electron Liquid", Cambridge Uiversity Press (2005)
!
!  Input parameters:
!
!       * EK       : quasiparticle energy in SI
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature   in SI
!
!  Output parameters:
!
!       * GIVI_LT_2D : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,FOUR,EIGHT,HALF
      USE CONSTANTS_P1,        ONLY : H_BAR,K_B
      USE FERMI_SI,            ONLY : EF_SI
      USE PI_ETC,              ONLY : PI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,RS,T
      REAL (WP)             ::  GIVI_LT_2D
      REAL (WP)             ::  UAT
      REAL (WP)             ::  ZETA
      REAL (WP)             ::  SMALL
!
      REAL (WP)             ::  LOG,ABS
!
      SMALL = 1.E-1_WP                                              !
!
      ZETA = ONE + HALF * (RS / (RS + SQRT(TWO)))**2                ! ref. (1) eq. (8.100)
!
      IF(T <= SMALL) THEN                                           !
        UAT = ZETA * (EK - EF_SI)**2 * LOG( FOUR * EF_SI /        & !
                                            DABS(EK - EF_SI)      & !
                                          ) /                     & ! ref. (1) eq. (8.102)
                                       (FOUR * PI * H_BAR * EF_SI)  !
      ELSE                                                          !
        UAT = ZETA * (PI * K_B * T)**2 * LOG( FOUR * EF_SI /      & !
                                              (K_B * T)           & !
                                            ) /                   & ! ref. (1) eq. (8.103)
                                        (EIGHT * PI * H_BAR * EF_SI)!
      END IF                                                        !                                        
!
      GIVI_LT_2D = ONE / UAT                                        !
!
      END FUNCTION GIVI_LT_2D  
!
!=======================================================================
!
      FUNCTION HAWR_LT_2D(EK,A_SI,RS)
!
!  This function computes Hawrylak approximation for 
!    the quasiparticle lifetime in the 2D case
!
!  This is for a layered system 
!
!  References: (1) P. Hawrylak, Phys. Rev. Lett. 59, 485-488 (1987)
!
!  Input parameters:
!
!       * EK       : quasiparticle energy in SI
!       * A_SI     : layer interspacing   in SI
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!  Output parameters:
!
!       * HAWR_LT_2D : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,EIGHT
      USE CONSTANTS_P1,        ONLY : BOHR,H_BAR,M_E
      USE FERMI_SI,            ONLY : KF_SI
      USE ENE_CHANGE,          ONLY : RYD
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,A_SI,RS
      REAL (WP)             ::  HAWR_LT_2D
      REAL (WP)             ::  A,CC
      REAL (WP)             ::  K,KC
      REAL (WP)             ::  UAT
!
      REAL (WP)             ::  SQRT
!
      A  = A_SI / BOHR                                              ! spacing in a.u.
      CC = ONE + ONE / A                                            !
!
      K  = SQRT(TWO * M_E * EK) / H_BAR                             ! k
      KC = KF_SI * (CC /DSQRT(CC * CC - ONE))                       ! k_c
!
      UAT = EIGHT * RYD *A * ( CC / (CC * CC - ONE) )**1.5E0_WP * & !
                             (K / KC - ONE)**2   /                & ! ref. (1) eq. (8)
                             (SQRT(TWO) * RS * RS)                  !
!
      HAWR_LT_2D = ONE / UAT                                        !
!
      END FUNCTION HAWR_LT_2D  
!
!=======================================================================
!
      FUNCTION MELA_LT_2D(EK,T)
!
!  This function computes Menashe-Laikhtman approximation for 
!    the quasiparticle lifetime in the 2D case
!
!  In this approximation, the lifetime is limited by 
!    electron-electron scattering
!
!  References: (1) D. Menashe and B. Laikhtman, Phys. Rev. B 54,
!                     11561-11574 (1996)
!
!  Input parameters:
!
!       * EK       : quasiparticle energy in SI
!       * T        : system temperature   in SI
!
!  Output parameters:
!
!       * MELA_LT_2D : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE
      USE CONSTANTS_P1,        ONLY : H_BAR,K_B
      USE FERMI_SI,            ONLY : EF_SI
      USE PI_ETC,              ONLY : PI
      USE CHEMICAL_POTENTIAL,  ONLY : MU
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,T
      REAL (WP)             ::  MELA_LT_2D
      REAL (WP)             ::  UAT
      REAL (WP)             ::  CP,DELTA
      REAL (WP)             ::  R1,R2
!
      REAL (WP)             ::  LOG,ABS
!
      CP = MU('2D',T)                                               ! chemical potential
!
      DELTA = EK - CP                                               !
      R1    = DELTA / EF_SI                                         !
      R2    = K_B * T / EF_SI                                       !
!                                                                   ! e-e scattering
      IF(R2 <= R1) THEN                                             ! k_B T << EK-CP
!
        UAT = EF_SI * ((EK - EF_SI)**2 / EF_SI**2) *              & !
                      LOG(EF_SI / (ABS(EK - EF_SI))) /            & ! ref. (1) eq. (27)
                      16.0E0_WP * PI * H_BAR                        !
!
      ELSE                                                          !
!
        UAT = PI * EF_SI * (K_B * T / EF_SI)**2 *                 & !
                   LOG(EF_SI / (K_B * T)) /                       & ! ref. (1) eq. (27)
                   16.0E0_WP * H_BAR                                !
!  
      END IF                                                        !
!
      MELA_LT_2D=ONE/UAT                                            !
!
      END FUNCTION MELA_LT_2D  
!
!=======================================================================
!
      FUNCTION QIVI_LT_2D(EK,RS,T)
!
!  This function computes Qian-Vignale approximation for 
!    the quasiparticle lifetime in the 2D case
!
!  References: (1) Z. Qian and G. Vignale, Phys. Rev. B 71, 
!                      075112 (2005)
!
!  Input parameters:
!
!       * EK       : quasiparticle energy in SI
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature   in SI
!
!  Output parameters:
!
!       * QIVI_LT_2D : lifetime in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,FOUR,EIGHT,HALF
      USE CONSTANTS_P1,        ONLY : K_B
      USE FERMI_SI,            ONLY : EF_SI
      USE PI_ETC,              ONLY : PI
      USE CHEMICAL_POTENTIAL,  ONLY : MU
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,RS,T
      REAL (WP)             ::  QIVI_LT_2D
      REAL (WP)             ::  UAT
      REAL (WP)             ::  CP,DELTA
      REAL (WP)             ::  BRAK,R1,R2
!
      REAL (WP)             ::  SQRT,LOG
!
      CP = MU('2D',T)                                               ! chemical potential
!
      DELTA = EK - CP                                               !
      BRAK  = 0.75E0_WP - RS / (SQRT(TWO) * (RS + SQRT(TWO)))**2    ! ref. (1) eq. (61)
!                                        
      R1 = DELTA / EF_SI                                            !
      R2 = K_B * T / EF_SI                                          !
!
      IF(R2 <= R1) THEN                                             ! k_B T << EK-CP
!  
        UAT = DELTA * DELTA * BRAK * LOG(TWO / R1) /              & !
                      (FOUR * PI * EF_SI)                           ! ref. (1) eq. (60) 
!
      ELSE                                                          !
!
        UAT =  -PI * EF_SI * R2 * R2 * BRAK * LOG(HALF * R2) / EIGHT! ref. (1) eq. (72)
!
      END IF                                                        !
!
      QIVI_LT_2D = ONE / UAT                                        !
!
      END FUNCTION QIVI_LT_2D  
!
END MODULE LIFETIME 
