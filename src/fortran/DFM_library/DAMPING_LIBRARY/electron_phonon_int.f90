!
!=======================================================================
!
MODULE ELECTRON_PHONON_INT 
!
      USE ACCURACY_REAL
!
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE EL_PHONON_INT_3D(X,EPH,EPS0,EPSI,IP)
!
!  This subroutine computes the electron-phonon interaction 
!   for 3D systems
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * EPH      : phonon energy -SI)
!       * EPS0     :
!       * EPSI     :
!       * IP_TYPE  : type of electron-phonon interaction
!                       IP_TYPE = 'DEHI'   --> Degani-Hipolito approximation
!
!  Output parameters:
!
!       * IP       : electron-phonon interaction
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  IP_TYPE
!
      REAL (WP), INTENT(IN) ::  X,EPH,EPS0,EPSI
!
      COMPLEX (WP)          ::  IP
!
      IF(IP_TYPE == 'DEHI') THEN                                    !
        IP = DEHI_EP_3D(X,EPH,EPS0,EPSI)                            !
      END IF                                                        !
!
      END SUBROUTINE EL_PHONON_INT_3D
!
!=======================================================================
!
      FUNCTION DEHI_EP_3D(X,EPH,EPS0,EPSI)
!
!  This function computes the Fourier coefficient of the electron-phonon
!    interaction
!
!  Reference: (1) M. H. Degani and O. Hipolito, Phys. Rev. B 35, 9345-9348 (1987)
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * EPH      : phonon energy -SI)
!       * EPS0     :
!       * EPSI     :
!
!  Output parameters:
!
!       * DEHI_EPI : Fourier coefficient
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO
      USE COMPLEX_NUMBERS,     ONLY : IC
      USE CONSTANTS_P1,        ONLY : E
      USE FERMI_SI,            ONLY : KF_SI
      USE PI_ETC,              ONLY : PI
!
!
      REAL (WP), INTENT(IN) :: X,EPH,EPS0,EPSI
      REAL (WP)             :: Q,SQR
!
      REAL (WP)             :: SQRT
!
      COMPLEX (WP)          :: DEHI_EP_3D
!
      Q   = TWO * X * KF_SI                                         ! phonon momentum
      SQR = SQRT(TWO * PI * E * E * (ONE / EPSI - ONE / EPS0) /EPH) !
!
      DEHI_EP_3D = - IC * EPH * SQR / Q                             ! ref. 1 eq. (2)
!
      END FUNCTION DEHI_EP_3D  
!
END MODULE ELECTRON_PHONON_INT 
