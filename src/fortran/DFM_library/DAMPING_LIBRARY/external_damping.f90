!
!=======================================================================
!
MODULE EXTERNAL_DAMPING
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_POWER(POWER,POW)
!
!  This subroutine sets up the power coefficient of the damping
!
!
!  Input parameters:
!
!       * POWER    : string for the power coefficient
!
!
!  Output parameters:
!
!       * POW      : value of the power coefficient
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 30 Apr 2021
!
!
      USE POWERS_OF_TEN
      USE DAMPING_VALUES
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(OUT) ::  POW
!
      CHARACTER (LEN = 5)    ::  POWER
!
      IF(POWER == ' KILO') THEN                                     !
        POW = KILO                                                  !
      ELSE IF(POWER == ' MEGA') THEN                                !
        POW = MEGA                                                  !
      ELSE IF(POWER == ' GIGA') THEN                                !
        POW = GIGA                                                  !
      ELSE IF(POWER == ' TERA') THEN                                !
        POW = TERA                                                  !
      ELSE IF(POWER == ' PETA') THEN                                !
        POW = PETA                                                  !
      ELSE IF(POWER == '  EXA') THEN                                !
        POW = EXA                                                   !
      ELSE IF(POWER == 'ZETTA') THEN                                !
        POW = ZETTA                                                 !
      ELSE IF(POWER == 'YOTTA') THEN                                !
        POW = YOTTA                                                 !
!
      ELSE IF(POWER == 'MILLI') THEN                                !
        POW = MILLI                                                 !
      ELSE IF(POWER == 'MICRO') THEN                                !
        POW = MICRO                                                 !
      ELSE IF(POWER == ' NANO') THEN                                !
        POW = NANO                                                  !
      ELSE IF(POWER == ' PICO') THEN                                !
        POW = PICO                                                  !
      ELSE IF(POWER == 'FEMTO') THEN                                !
        POW = FEMTO                                                 !
      ELSE IF(POWER == ' ATTO') THEN                                !
        POW = ATTO                                                  !
      ELSE IF(POWER == 'ZEPTO') THEN                                !
        POW = ZEPTO                                                 !
      ELSE IF(POWER == 'YOCTO') THEN                                !
        POW = YOCTO                                                 !
      END IF                                                        !
!
      END SUBROUTINE CALC_POWER
!
END MODULE  EXTERNAL_DAMPING

