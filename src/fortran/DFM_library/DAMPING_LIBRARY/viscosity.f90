!
!=======================================================================
!
MODULE VISCOSITY 
!
      USE ACCURACY_REAL
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE VISCOSITY_COEF(X,ETA)
!
!  This subroutine computes the shear viscosity
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Oct 2020
!
!
      USE MATERIAL_PROP,          ONLY : DMN,RS
      USE EXT_FIELDS,             ONLY : T
      USE PLASMA,                 ONLY : ZION
!
      USE SCREENING_VEC,          ONLY : DEBYE_VECTOR
      USE DAMPING_VALUES,         ONLY : VI_TYPE
!
      USE EL_PHO_INTER,           ONLY : NA,MA,RA,DEBYE_T,EP_C
!
      USE REAL_NUMBERS,           ONLY : ZERO
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)  ::  X
      REAL (WP), INTENT(OUT) ::  ETA
      REAL (WP)              ::  LR,S_L
      REAL (WP)              ::  KD_SI
!
      LR     = ZERO                                                 ! residual mfp (temporary)
      S_L    = ZERO                                                 ! scattering length (temporary)
!
!  Computing the Debye momentum 
!
      CALL DEBYE_VECTOR('3D',T,RS,KD_SI)                            !
!
      IF(DMN == '3D') THEN                                          !
        CALL VISCOSITY_3D(RS,T,ZION,KD_SI,X,ZERO,NA,MA,RA,        & !
                          DEBYE_T,EP_C,LR,VI_TYPE,ETA)              !
      ELSE IF(DMN == '2D') THEN                                     !                                     
        CALL VISCOSITY_2D(T,S_L,VI_TYPE,ETA)                        !
      ELSE IF(DMN == '1D') THEN                                     !                                     
        ETA = ZERO                                                  ! not yet implemented
      END IF                                                        !
!
      END SUBROUTINE VISCOSITY_COEF
!
!=======================================================================
!
      SUBROUTINE VISCOSITY_3D(RS,T,ZION,K_SC,X,Z,NA,MA,RA,TH,CA,LR, &
                              VI_TYPE,ETA) 
!
!  This subroutine computes the shear viscosity for 3D electron gas
!    at a given value of the temperature T
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)     
!       * ZION     : atomic number of the ions of the plasma
!       * K_SC     : screening vector in SI
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : omega / omega_q        --> dimensionless
!       * NA       : number of atoms per unit volume
!       * MA       : mass of lattice atoms
!       * RA       : radius of atoms
!       * TH       : Debye temperature of the material in SI
!       * CA       : electron-phonon coupling
!       * LR       : residual mean free path
!       * VI_TYPE  : viscosity in 3D
!                       VI_TYPE = 'AMPP' Angilella et al hard-sphere fluid     --> T-dependent
!                       VI_TYPE = 'DRBA' Daligault-Rasmussen-Baalrud (plasmas) --> T-dependent
!                       VI_TYPE = 'KHRA' Khrapak for Yukawa fluid  --> T-dependent
!                       VI_TYPE = 'LHPO' Longuet-Higgins-Pope      --> T-dependent
!                       VI_TYPE = 'LLPA' Landau-Lifshitz-Pitaevskii--> T-dependent
!                       VI_TYPE = 'SCHA' Schäfer                   --> T-dependent
!                       VI_TYPE = 'SCHD' Schäfer (dynamic)         --> T-dependent
!                       VI_TYPE = 'SHTE' Shternin                  --> T-dependent
!                       VI_TYPE = 'STEI' Steinberg low-temperature --> T-dependent
!
!
!  Output parameters:
!
!       * ETA      : shear viscosity in SI
!
!
!  Internal parameters:
!
!       * I_F      : switch for choice of formula
!                       I_F  = 1 --> eq. (31) ref. 1
!                       I_F  = 2 --> eq. (32) ref. 1 Landau-Spitzer formula
!                       I_F  = 3 --> eq. (34) ref. 1 
!                       I_F  = 4 --> after eq. (34) ref. 1 Bastea formula
!                       I_F  = 5 --> eq. (38) ref. 1 Braun formula
!                       I_F  = 6 --> eq. (36)-(40) ref. 1 Tanaka-Ichimaru formula
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   :: VI_TYPE 
!
      REAL (WP)             ::  RS,T,ZION,K_SC,X,Z,NA,MA,RA,TH,CA,LR
      REAL (WP)             ::  ETA
!
      INTEGER               ::  I_F
!
      IF(VI_TYPE == 'AMPP') THEN                                    !
        ETA=AMPP_VISC_3D(RS,T)                                      !
      ELSE IF(VI_TYPE == 'DRBA') THEN                               !
        ETA=DRBA_VISC_3D(RS,T,ZION,I_F)                             !
      ELSE IF(VI_TYPE == 'KHRA') THEN                               !
        ETA=KHRA_VISC_3D(RS,T,ZION,K_SC,I_F)                        !
      ELSE IF(VI_TYPE == 'LHPO') THEN                               !
        ETA=LHPO_VISC_3D(RS,T)                                      !
      ELSE IF(VI_TYPE == 'SCHA') THEN                               !
        ETA=SCHA_VISC_3D(T)                                         !
      ELSE IF(VI_TYPE == 'SCHD') THEN                               !
        ETA=SCHA_VISC_3D_D(X,Z,T)                                   !
      ELSE IF(VI_TYPE == 'SHTE') THEN                               !
        ETA=SHTE_VISC_3D(RS,T)                                      !
      ELSE IF(VI_TYPE == 'STEI') THEN                               !
        ETA=STEI_VISC_LT_3D(RS,T,NA,MA,RA,TH,CA,LR)                 !
      END IF                                                        !
!
      END SUBROUTINE VISCOSITY_3D
!
!=======================================================================
!
      FUNCTION AMPP_VISC_3D(RS,T) 
!
!  This function computes the Angilella shear viscosity 
!    for 3D hard-sphere fluid at a given value of the temperature T
!
!  References: (1) G. G. N. Angilella et al, Phys. Lett. A, 992-998 (2009)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)     
!
!
!  Output parameters:
!
!       * AMPP_VISC_3D
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,FIVE,HALF,THIRD
      USE CONSTANTS_P1,     ONLY : BOHR,M_E,K_B
      USE PI_ETC,           ONLY : PI
      USE UTILITIES_1,      ONLY : RS_TO_N0
!
      IMPLICIT NONE
!
      REAL (WP)             ::  T,RS
      REAL (WP)             ::  AMPP_VISC_3D
      REAL (WP)             ::  N0
      REAL (WP)             ::  SI,PF,D0,D,X,X4,NUM,DEN
!
      REAL (WP)             ::  DSQRT
!
!  Computing the electron density
!
      N0=RS_TO_N0('3D',RS)                                          !
!
!  Hard-sphere diameter
!
      SI=TWO*RS*BOHR                                                ! sigma
!
!  Computing the packing fraction PF
!
      PF=HALF*THIRD*PI*N0 * SI**3                                   !
!
      D0=(THREE*0.125E0_WP*SI/N0) * DSQRT(K_B*T/(PI*M_E))           ! ref. 1 eq. (9) 
!
!  Speedy diffusion coefficient
!
      X=N0*SI*SI*SI                                                 !
      X4=X*X*X*X                                                    ! x^4
      D=D0*(ONE - X/1.09E0_WP) * (ONE + X4*(0.4E0_WP-0.83E0_WP*X4)) ! ref. 1 eq. (8)
!
      NUM=D0*THREE*PF*K_B*T                                         !
      DEN=D*FIVE*PI*SI*D0                                           !
      AMPP_VISC_3D=NUM/DEN                                          ! ref. 1 eq. (A5)
!
      END FUNCTION AMPP_VISC_3D  
!
!=======================================================================
!
      FUNCTION DRBA_VISC_3D(RS,T,ZION,I_F) 
!
!  This function computes the Daligault-Rasmussen_Baalrud shear viscosity 
!    for 3D plasmas at a given value of the temperature T
!
!  References: (1) J. Daligault, K. O. Rasmussen and S. D. Baalrud, 
!                     Phys. Rev. E 90, 033105 (2014)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)     
!       * ZION     : atomic number of the ions of the plasma
!       * I_F      : switch for choice of formula
!                       I_F  = 1 --> eq. (31) ref. 1
!                       I_F  = 2 --> eq. (32) ref. 1 Landau-Spitzer formula
!                       I_F  = 3 --> eq. (34) ref. 1 
!                       I_F  = 4 --> after eq. (34) ref. 1 Bastea formula
!                       I_F  = 5 --> eq. (38) ref. 1 Braun formula
!                       I_F  = 6 --> eq. (36)-(40) ref. 1 Tanaka-Ichimaru formula
!
!
!  Output parameters:
!
!       * DRBA_VISC_3D
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,HALF
      USE COMPLEX_NUMBERS,  ONLY : IC
      USE CONSTANTS_P1,     ONLY : H_BAR,M_E,E,COULOMB,K_B
      USE PI_ETC,           ONLY : PI
      USE SCREENING_VEC,    ONLY : DEBYE_VECTOR
      USE UTILITIES_1,      ONLY : RS_TO_N0
      USE EXT_FUNCTIONS,    ONLY : E1Z                              ! Exponential integral
      USE PLASMON_ENE_SI
      USE PLASMA_SCALE
!
      IMPLICIT NONE
!
      REAL (WP)             ::  T,RS,ZION
      REAL (WP)             ::  DRBA_VISC_3D
      REAL (WP)             ::  N0
      REAL (WP)             ::  NONID,DEGEN
      REAL (WP)             ::  KD_SI,ETA_0,DELTA,C,LD,RL,Q2
      REAL (WP)             ::  G1,G2,G3,G4,OP
      REAL (WP)             ::  A,B,A1,A2,A3,B1,B2,B3,B4,K
      REAL (WP)             ::  TTI,GG
      REAL (WP)             ::  NUM,DEN
!
      REAL (WP)             ::  DLOG,DSQRT,DREAL
!
      INTEGER               ::  I_F
!
      COMPLEX (WP)          ::  CE1,CE2                   
!
!  Computing the electron density and plasmon properties
!
      IF( (I_F == 3) .OR. (I_F == 4) ) THEN                         !
        CALL  PLASMON_SCALE(RS,T,ZION,NONID,DEGEN)                  !
        N0=RS_TO_N0('3D',RS)                                        !
        G1=DEGEN                                                    ! Gamma
        G2=G1*G1                                                    !
        G3=G2*G1                                                    ! powers of Gamma
        G4=G3*G1                                                    !
        OP=ENE_P_SI/H_BAR                                           ! omega_p
        A=0.794811E0_WP                                             ! 
        B=0.862151E0_WP                                             !
        A1=0.0425698E0_WP                                           !
        A2=0.00205782E0_WP                                          !
        A3=7.03658E-5_WP                                            ! ref. 1 table IV
        B1=0.0429942E0_WP                                           !
        B2=-0.000270798E0_WP                                        !
        B3=3.25441E-6_WP                                            !
        B4=-1.15019E-8_WP                                           !
        K=G1**2.5E0_WP * DLOG(ONE + B / G1**1.5E0_WP)               !
      END IF                                                        !
!
!  Computing the Debye vector
!
      CALL DEBYE_VECTOR('3D',T,RS,KD_SI)                            !
!
      LD=ONE/KD_SI                                                  ! Debye length
      DELTA=0.466E0_WP                                              !
      C=1.493E0_WP                                                  !
      Q2=E*E*COULOMB                                                !
      RL=Q2/(K_B*T)                                                 !
      ETA_0=1.25E0_WP*DSQRT(M_E/PI)* (K_B*T)**2.5E0_WP / (Q2*Q2)    !
      GG=LD/RL                                                      !
!
      IF(I_F == 1) THEN                                             !
        DRBA_VISC_3D=ETA_0*DELTA / DLOG(ONE + C*GG)                 ! ref. 1 eq. (31)
      ELSE IF(I_F == 2) THEN                                        !
        DRBA_VISC_3D=ETA_0/DLOG(GG)                                 ! ref. 1 eq. (32)
      ELSE IF(I_F == 3) THEN                                        !
        NUM=A*(ONE+A1*G1+A2*G2+A3*G3)                               !
        DEN=K*(ONE+B1*G1+B2*G2+B3*G3+B4*G4)                         !
        DRBA_VISC_3D=M_E*N0*A*A*OP*NUM/DEN                          !
      ELSE IF(I_F == 4) THEN                                        !
        DRBA_VISC_3D=M_E*N0*A*A*OP* (                             & !
                     0.482E0_WP/G2 + 0.629E0_WP/(G1**0.878E0_WP)  & !
                                    )                               !
      ELSE IF(I_F == 6) THEN                                        !
        DRBA_VISC_3D=ETA_0/DLOG(LD/RL) /                          & ! ref. 1 eq. (38)
                     (ONE + 0.346E0_WP/DLOG(GG))                    !
      ELSE IF(I_F == 5) THEN                                        !
        CALL E1Z( IC/GG,CE1)                                        !
        CALL E1Z(-IC/GG,CE2)                                        !
        TTI=HALF*DREAL( CE1*CDEXP( IC/GG) +                       & ! ref. 1 eq. (40)
                         CE2*CDEXP(-IC/GG)                        & ! 
                       )                                            !
        DRBA_VISC_3D=ETA_0/TTI                                      !
      END IF                                                        !
!
      END FUNCTION DRBA_VISC_3D  
!
!=======================================================================
!
      FUNCTION KHRA_VISC_3D(RS,T,ZION,K_SC,I_F) 
!
!  This function computes the Khrapak shear viscosity 
!    for 3D Yukawa fluid at a given value of the temperature T
!
!  References: (1) S. Khrapak, AIP Advances 8, 105226 (2018)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)     
!       * ZION     : atomic number of the ions of the plasma
!       * K_SC     : screening vector in SI
!       * I_F      : switch for choice of formula
!                       I_F  = 1 --> eq. (7) ref. 1
!                       I_F  = 2 --> eq. (8) ref. 1
!
!
!  Output parameters:
!
!       * KHRA_VISC_3D
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,HALF,FOUR,THIRD,FOURTH
      USE CONSTANTS_P1,     ONLY : BOHR,M_E,K_B
      USE UTILITIES_1,      ONLY : ALFA
      USE PLASMA_SCALE
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS,T,ZION,K_SC
      REAL (WP)             ::  KHRA_VISC_3D
      REAL (WP)             ::  AL,GA,GA_M,KA,X,ETA_R
      REAL (WP)             ::  NONID,DEGEN
      REAL (WP)             ::  BETA,V_TH,F1,F2
!
      REAL (WP)             ::  DSQRT,DEXP
!
      INTEGER               ::  I_F
!
      AL=ALFA('3D')                                                 ! parameter alpha
!
!  Computing the plasma degeneracy
!
      CALL PLASMON_SCALE(RS,T,ZION,NONID,DEGEN)                     !
!
!  Computing the thermal velocity
!
      BETA=ONE/(K_B*T)                                              !
      V_TH=DSQRT(M_E/BETA)                                          ! thermal velocity
!
      KA=K_SC*RS*BOHR                                               ! parameter kappa
!
!  Coupling parameters
!
      GA=DEGEN                                                      ! ref. 1 notation
      GA_M=(172.0E0_WP*DEXP(AL*KA))/(ONE+AL*KA+HALF*AL*AL*KA*KA)    ! ref. 1  eq. (2)
      X=GA/GA_M                                                     !    
!
      IF(I_F == 1) THEN                                             !
        F1=0.104E0_WP / X**0.4E0_WP                                 ! ref. 1  eq. (5)
        F2=0.126E0_WP * DEXP(3.64E0_WP*DSQRT(X))                    ! ref. 1  eq. (6)
        ETA_R=(F1**FOUR + F2**FOUR)**FOURTH                         ! ref. 1  eq. (7)
      ELSE IF(I_F == 2) THEN                                        !
        ETA_R=0.00022E0_WP / X**1.5E0_WP   +                    &   !
              0.096E0_WP   / X**0.378E0_WP +                    &   ! ref. 1  eq. (8)
              4.68E0_WP * X**1.5E0_WP                               !
      END IF                                                        !
!
      KHRA_VISC_3D=ETA_R*M_E*V_TH*( AL*AL*RS*RS*BOHR*BOHR )         ! ref. 1  eq. (4)
!
      END FUNCTION KHRA_VISC_3D  
!
!=======================================================================
!
      FUNCTION LHPO_VISC_3D(RS,T) 
!
!  This function computes the Longuet-Higgins and Pope shear viscosity 
!    for 3D hard-sphere fluid at a given value of the temperature T
!
!  References: (1) G. G. N. Angilella et al, Phys. Lett. A, 992-998 (2009)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)     
!
!
!  Output parameters:
!
!       * LHPO_VISC_3D
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Dec 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,HALF,THIRD
      USE CONSTANTS_P1,     ONLY : BOHR,M_E,K_B
      USE PI_ETC,           ONLY : PI
      USE UTILITIES_1,      ONLY : RS_TO_N0
      USE PACKING_FRACTION
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  RS,T
      REAL (WP)             ::  LHPO_VISC_3D
!
      REAL (WP)             ::  N0
      REAL (WP)             ::  COEF
      REAL (WP)             ::  PF,PF2,PF3,PR,SI
!
      REAL (WP)             ::  SQRT
!
!  Computing the electron density
!
      N0 = RS_TO_N0('3D',RS)                                        !
!
      COEF = SQRT(M_E * K_B * T / PI)                               !
!
!  Hard-sphere diameter
!
      SI = TWO * RS * BOHR                                          ! sigma
!
!  Computing the packing fraction PF
!
      PF  = PACK_FRAC_2D(N0,SI,'HSM')                               !
      PF2 = PF  * PF                                                !
      PF3 = PF2 * PF                                                !
!
      PR = (ONE + PF + PF2 - PF3) / (ONE - PF)**3  - ONE            ! ref. 1 eq. (2)
!
      LHPO_VISC_3D = 0.40E0_WP * N0 * SI * COEF * PR                ! ref. 1 eq. (1)
!
      END FUNCTION LHPO_VISC_3D  
!
!=======================================================================
!
      FUNCTION LLPA_VISC_3D(X,RS,T) 
!
!  This function computes the Landau-Lifschitz-Pitaevskii shear viscosity 
!    for 3D hard-sphere fluid at a given value of the temperature T
!
!  Reference: (1) J. Daligault, Phys. Rev. Lett. 119, 045002 (2017) 
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)     
!
!
!  Output parameters:
!
!       * LLPA_VISC_3D
!
!
!  Note: we find that the average over solid angles in note [13] ref. (1) is
!
!                                 _                     _
!                                |                    2  |
!                         e^2    |  2     1  (  k_s  )   |     1
!     / __ \    =    ----------- | --- + --- ( ----- )   | -------
!     \    /          epsilon_0  |  3     2  (  k_F  )   |  k_F^2
!                                |_                     _|
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Oct 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,FOUR,FIVE,EIGHT, &
                                      HALF,THIRD,EIGHTH
      USE FERMI_SI,            ONLY : EF_SI,KF_SI
      USE CONSTANTS_P1,        ONLY : BOHR,H_BAR,M_E,K_B,EPS_0
      USE PI_ETC,              ONLY : PI,PI3,PI_INV
!
      USE PLASMON_SCALE_P,     ONLY : NONID
      USE COULOMB_LOG,         ONLY : DALI_CL_3D
      USE SCREENING_TYPE
      USE SCREENING_VEC
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X,T,RS
      REAL (WP)             ::  LLPA_VISC_3D
      REAL (WP)             ::  Y,Q,Q2,Q4
      REAL (WP)             ::  CL,KBT,KS,TH
      REAL (WP)             ::  NUM,DEN,BRA
!
      REAL (WP)             ::  SQRT
!
      TH   = ONE / NONID                                            ! Theta
!
      Y = X + X                                                     ! q / k_F
!
      Q  = Y * KF_SI                                                ! q   in SI
      Q2 = Q  * Q                                                   ! q^2 in SI
      Q4 = Q2 * Q2                                                  ! q^4 in SI
!
      KBT = K_B * T                                                 !
!
!  Computing the screening vector
!
      IF(SC_TYPE == 'NO') THEN                                      !
        CALL SCREENING_VECTOR('TF','3D',X,RS,T,KS)                  !
      ELSE                                                          !
        CALL SCREENING_VECTOR(SC_TYPE,'3D',X,RS,T,KS)               ! in SI
      END IF                                                        !
!
!  Computing the Coulomb logarithm
!
      CL = DALI_CL_3D(X)                                            !
!
      IF(TH >= ONE) THEN                                            !
        NUM = FIVE * EIGHTH * SQRT(PI * M_E) * KBT**2.5E0_WP        ! \
        DEN = Q4 * CL                                               !  > ref. (1) note [13]
        LLPA_VISC_3D = NUM /  DEN                                   ! /
      ELSE                                                          !
        NUM = 16.0E0_WP * EF_SI * BOHR * KF_SI                      ! \
        DEN = 45.0E0_WP * TH * TH * EIGHT * PI3 * Q4                !  \
        BRA = TWO * THIRD + HALF * (KS / KF_SI)**2                  !  /
        LLPA_VISC_3D = NUM / (DEN + BRA)                            ! /
      END IF                                                        !
!
      END FUNCTION LLPA_VISC_3D
!
!=======================================================================
!
      FUNCTION SCHA_VISC_3D(T) 
!
!  This function computes the Schäfer shear viscosity for 
!    3D systems at a given value of the temperature T
!
!  References: (1) T. Schäfer, Phys. Rev. A 85, 033623 (2012)
!
!
!  Input parameters:
!
!       * T        : temperature (SI)     
!
!
!  Output parameters:
!
!       * SCHA_VISC_3D
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE CONSTANTS_P1,     ONLY : M_E
      USE PI_ETC,           ONLY : SQR_PI
!
      IMPLICIT NONE
!
      REAL (WP)             ::  T
      REAL (WP)             ::  SCHA_VISC_3D
!
      SCHA_VISC_3D=15.0E0_WP*(M_E*T)**1.5E0_WP / (32.0E0_WP*SQR_PI) ! ref. 1 eq. (14)
!
      END FUNCTION SCHA_VISC_3D  
!
!=======================================================================
!
      FUNCTION SCHA_VISC_3D_D(X,Z,T) 
!
!  This function computes the dynamic Schäfer shear viscosity for 
!    3D systems at a given value of the low temperature T
!
!  References: (2) C. Chafin and T. Schäfer, Phys. Rev. A 87, 023629 (2013)
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : omega / omega_q        --> dimensionless
!       * T        : temperature (SI)     
!
!
!  Output parameters:
!
!       * SCHA_VISC_3D
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : SEVEN,HALF
      USE CONSTANTS_P1,     ONLY : H_BAR,M_E
      USE FERMI_SI,         ONLY : KF_SI
      USE PI_ETC,           ONLY : PI
      USE UTILITIES_1,      ONLY : KF_TO_N0
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X,Y,Y2,Z,V,T
      REAL (WP)             ::  SCHA_VISC_3D_D
      REAL (WP)             ::  OMEGA,ETA,D_ETA
      REAL (WP)             ::  N0
      REAL (WP)             ::  NUM,DEN
!
      REAL (WP)             ::  DSQRT
!
      Y=X+X                                                         ! Y = q / k_F 
      Y2=Y*Y                                                        !
      V=Z*Y2                                                        ! omega / omega_{k_F}
      OMEGA=V*HALF*H_BAR*KF_SI*KF_SI/M_E                            ! omega
!
!  Computing the static Schäfer viscosity
!
      ETA=SCHA_VISC_3D(T)                                           !
!
!  Computing the electron density
!
      N0=KF_TO_N0('3D',KF_SI)                                       !
!
      D_ETA=ETA/N0                                                  ! momentum diffusion constant
      NUM=SEVEN + (1.5E0_WP)**1.5E0_WP                              !
      DEN=240.0E0_WP*PI * D_ETA**1.5E0_WP                           !
!
      SCHA_VISC_3D_D=ETA - DSQRT(OMEGA)*T * NUM/DEN                 ! ref. 1 eq. (33)
!
      END FUNCTION SCHA_VISC_3D_D  
!
!=======================================================================
!
      FUNCTION SHTE_VISC_3D(RS,T) 
!
!  This function computes the Shternin shear viscosity 
!    for 3D electron gas at a given value of the temperature T
!
!  References: (1) P. S. Shternin, J. Phys. A: Math. Theor. 41 205501 (2008)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)     
!
!
!  Output parameters:
!
!       * SHTE_VISC_3D
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,SIX,EIGHT,TEN,THIRD
      USE CONSTANTS_P1,     ONLY : H_BAR,K_B
      USE CONSTANTS_P2,     ONLY : ALPHA
      USE CONSTANTS_P3,     ONLY : C
      USE FERMI_SI,         ONLY : KF_SI,VF_SI
      USE PI_ETC,           ONLY : PI,PI3
      USE SQUARE_ROOTS,     ONLY : SQR3
      USE UTILITIES_1,      ONLY : KF_TO_N0
      USE SCREENING_VEC,    ONLY : THOMAS_FERMI_VECTOR
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS,T
      REAL (WP)             ::  SHTE_VISC_3D
      REAL (WP)             ::  N0
      REAL (WP)             ::  NUM,DEN
      REAL (WP)             ::  K_TF_SI
      REAL (WP)             ::  I_ETA,I_L,I_T,I_TL
      REAL (WP)             ::  TH,T_TPE,U,SCF
      REAL (WP)             ::  XI
!
      REAL (WP)             ::  DLOG
!
      INTEGER               ::  REGIME
!
      SCF=1.0E-2_WP                                                 ! <<
      XI=1.813174518048293088675271480395889E0_WP                   ! xi ref. 1 eq. (15)
!
!  Computing the electron density
!
      N0=KF_TO_N0('3D',KF_SI)                                       !
!
!  Computing the Thomas-Fermi screening verctor
!
      CALL THOMAS_FERMI_VECTOR('3D',K_TF_SI)                        !
!
!  Computing TH, T_TPE and U
!
      TH=H_BAR*VF_SI*K_TF_SI/(K_B*T)                                ! ref. 1 eq. (11)
      T_TPE=SQR3/TH                                                 ! T / T_pe (idem)
      U=VF_SI/C                                                     ! idem
!
!  Checking the regime
!
      IF(U < SCF) THEN                                              !
        IF(T_TPE < SCF) THEN                                        !
          REGIME=2                                                  !
        ELSE                                                        !
          REGIME=1                                                  !
        END IF                                                      ! ref. 1 table 1.
      ELSE                                                          !
        IF(T_TPE < SCF) THEN                                        !
          REGIME=4                                                  !
        ELSE                                                        !
          REGIME=3                                                  !
        END IF                                                      !
      END IF                                                        !
!
!  Calculation of the integral I_ETA
!
      IF(REGIME == 1) THEN                                          !
        I_L=TWO*THIRD*(DLOG(ONE/TH)+1.919E0_WP)                     !
        I_T=EIGHT*U*U*U*U*(DLOG(ONE/(U*TH))+3.413E0_WP)/35.0E0_WP   ! ref. 1 eq. (13)
        I_TL=EIGHT*U*U*(DLOG(ONE/TH)+2.512E0_WP)/15.0E0_WP          !
      ELSE IF(REGIME == 3) THEN                                     !
        I_L=TWO*THIRD*(DLOG(ONE/TH)+1.919E0_WP)                     !
        I_T=THIRD*(DLOG(ONE/TH)+2.742E0_WP)                         ! ref. 1 eq. (14)
        I_TL=TWO*THIRD*(DLOG(ONE/TH)+2.052E0_WP)                    ! 
      ELSE                                                          !
        I_L=PI3 / (12.0E0_WP*TH)                                    !
        I_T=XI * U**(TEN*THIRD) / (TH*TH)**THIRD                    ! ref. 1 eq. (15) 
        I_TL=PI3 * U*U / (SIX*TH)                                   !
      END IF                                                        !
!
      I_ETA=I_L+I_T+I_TL                                            ! ref. 1 eq. (12)
!
      NUM=PI*H_BAR*H_BAR*N0*KF_SI*VF_SI*VF_SI*VF_SI                 !
      DEN=60.0E0_WP*ALPHA*ALPHA*C*C*K_B*T*I_ETA                     !
!
      SHTE_VISC_3D=NUM/DEN                                          ! ref. 1 eq. (9)
!
      END FUNCTION SHTE_VISC_3D  
!
!=======================================================================
!
      FUNCTION STEI_VISC_LT_3D(RS,T,NA,MA,RA,TH,CA,LR)
!
! This function computes Steinberg's low-temperature viscosity.
!
! In this model; the electron interacts with acoustic lattice vibrations
!
!
!  Reference:  (1) M. S. Steinberg, Phys. Rev. 109, 1486 (1958)
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature  (in SI)
!       * NA       : number of atoms per unit volume
!       * MA       : mass of lattice atoms
!       * RA       : radius of atoms
!       * TH       : Debye temperature of the material in SI
!       * CA       : electron-phonon coupling
!       * LR       : residual mean free path
!
!  Output parameters:
!
!       * STEI_VISC_LT_3D  : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : TWO,THREE,FOUR,FIVE,HALF,THIRD
      USE CONSTANTS_P1,     ONLY : H_BAR,M_E,K_B
      USE FERMI_SI,         ONLY : EF_SI
      USE PI_ETC,           ONLY : PI,PI2
      USE UTILITIES_1,      ONLY : ALFA,RS_TO_N0
      USE SPECIFIC_INT_1,   ONLY : STEI_INT
!
      IMPLICIT NONE
!
      REAL (WP)             ::  T,NA,MA,RA,TH,CA,RS
      REAL (WP)             ::  STEI_VISC_LT_3D
      REAL (WP)             ::  N0
      REAL (WP)             ::  H,AL,GA,D
      REAL (WP)             ::  NUM,DEN,NU1,DE1,DE2,A02,D00
      REAL (WP)             ::  X,J5,J7,X5,LR
!
      REAL (WP)             ::  DSQRT
!
!  Computation of the electron density
!
      N0=RS_TO_N0('3D',RS)                                          !
!
      AL=ALFA('3D')                                                 !
      H=TWO*PI*H_BAR                                                !
      GA=AL * (FOUR*MA*RA*K_B*TH) / (THREE*H*H*CA*CA)               !
      D=EF_SI / ( TWO**THIRD * (N0/NA)**(TWO*THIRD) )               !
!
!  Computation of the J_p(x) functions
!
      X=TH/T                                                        !
      J5=STEI_INT(X,5)                                              !
      J7=STEI_INT(X,7)                                              !
!
      X5=X*X*X*X*X                                                  !
!
      NUM=FOUR*DSQRT(TWO*M_E* M_E*M_E)                              !
      DEN=15.0E0_WP*PI2* H_BAR*H_BAR*H_BAR                          !
      A02=EF_SI**FIVE                                               !
      NU1=THREE*DSQRT(M_E+M_E)*D*EF_SI                              !
      DE1=H_BAR*H_BAR*GA*X5                                         !
      DE2=J5 - J7/(X*X* 16.0E0_WP**THIRD * (N0/NA)**(TWO*THIRD) )   !
      D00=NU1/(DE1*DE2) +                                         & !
          TWO*EF_SI*EF_SI*EF_SI/(DSQRT(HALF*M_E)*LR)                !
!
      STEI_VISC_LT_3D=NUM*A02 / (DEN*D00)                           ! ref. 1 eq. (7.13)
!
      END FUNCTION STEI_VISC_LT_3D  
!
!=======================================================================
!
      SUBROUTINE VISCOSITY_2D(T,S_L,VI_TYPE,ETA) 
!
!  This subroutine computes the shear viscosity for 2D electron gas
!    at a given value of the temperature T
!
!
!  Input parameters:
!
!       * T        : temperature (SI)     
!       * S_L      : scattering length (SI)     
!       * VI_TYPE  : viscosity in 2D
!                       VI_TYPE = 'SCHA' Schäfer                   --> T-dependent
!
!
!  Output parameters:
!
!       * ETA      : shear viscosity in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   :: VI_TYPE 
!
      REAL (WP)             ::  T,S_L
      REAL (WP)             ::  ETA
!
      IF(VI_TYPE == 'SCHA') THEN                                    !
        ETA=SCHA_VISC_2D(T,S_L)                                     !
      END IF                                                        !
!
      END SUBROUTINE VISCOSITY_2D  
!
!=======================================================================
!
      FUNCTION SCHA_VISC_2D(T,S_L) 
!
!  This function computes the Schäfer shear viscosity for 
!    2D systems at a given value of the temperature T
!
!  References: (1) T. Schäfer, Phys. Rev. A 85, 033623 (2012)
!
!
!  Input parameters:
!
!       * T        : temperature (SI)     
!       * S_L      : scattering length (SI)     
!
!
!  Output parameters:
!
!       * SCHA_VISC_2D
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO
      USE CONSTANTS_P1,     ONLY : M_E
      USE PI_ETC,           ONLY : PI2
!
      IMPLICIT NONE
!
      REAL (WP)             ::  T,S_L
      REAL (WP)             ::  SCHA_VISC_2D
      REAL (WP)             ::  T_A2D
!
      REAL (WP)             ::  DLOG
!
      T_A2D=ONE/(M_E*M_E*S_L*S_L)                                   ! 
!
      SCHA_VISC_2D=M_E*T* (                                    &    !
                            DLOG(2.5E0_WP*T/T_A2D)**2 + PI2    &    ! ref. 1 eq. (10)
                          ) / (TWO*PI2)                             !
!
      END FUNCTION SCHA_VISC_2D  
!
END MODULE VISCOSITY 
