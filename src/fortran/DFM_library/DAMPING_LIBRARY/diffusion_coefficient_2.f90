!
!=======================================================================
!
MODULE DIFFUSION_COEFFICIENT_2
!
      USE ACCURACY_REAL
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE DIFFUSION_COEF2(X,DC)
!
!  This subroutine computes the diffusion coefficient from
!    the knowledge of the static susceptibility
!
!  Reference: (1) M. Le Bellac, F. Mortessagne and G. G. Batrouni,
!                    "Equilibrium and Non-Equilibrium Statistical
!                    Thermodynamics", (Cambridge University Press, 2004)
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!
!  Output parameters:
!
!       * DC       : diffusion coefficient
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 30 Apr 2021
!
!
      USE MATERIAL_PROP,          ONLY : DMN,RS
      USE EXT_FIELDS,             ONLY : T
      USE UNITS,                  ONLY : UNIT
!
      USE REAL_NUMBERS,           ONLY : ZERO,ONE,TWO
      USE CONSTANTS_P1,           ONLY : M_E
      USE FERMI_SI,               ONLY : KF_SI
!
      USE LF_VALUES,              ONLY : GQ_TYPE
      USE DF_VALUES,              ONLY : EPS_T,D_FUNC
!
      USE DFUNC_STATIC
      USE LOCAL_FIELD_STATIC
      USE DAMPING_COEF
      USE COULOMB_K,              ONLY : COULOMB_FF
      USE UTILITIES_1,            ONLY : RS_TO_N0
      USE DAMPING_SI
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)    ::  D_FUNCL
!
      REAL (WP), INTENT(IN)  ::  X
      REAL (WP), INTENT(OUT) ::  DC
!
      REAL (WP)              ::  EPSR,EPSI
      REAL (WP)              ::  GQ,CHI_0,CHI_Q
      REAL (WP)              ::  NUM,DEN
      REAL (WP)              ::  Q_SI,VC
      REAL (WP)              ::  NN
!
      Q_SI = TWO * X * KF_SI                                        ! q in SI
!
      IF(EPS_T == 'LONG') THEN                                      !
        D_FUNCL = D_FUNC                                            !
      END IF                                                        !
!
!  Computing the Coulomb potential VC
!
      CALL COULOMB_FF(DMN,UNIT,Q_SI,ZERO,VC)                        ! Coulomb pot.
!
!  Computing the static dielectric function and
!    the static local field correction
!
      CALL DFUNCL_STATIC(X,D_FUNCL,EPSR,EPSI)                       !
      CALL LFIELD_STATIC(X,RS,T,GQ_TYPE,GQ)                         !
!
      CHI_0 = (ONE - EPSR) / VC                                     !
!
      NUM   = CHI_0                                                 !
      DEN   = ONE + VC * (GQ - ONE) * CHI_0                         !
      CHI_Q = NUM / DEN                                             !
!
!  Computing the density NN
!
      NN = RS_TO_N0(DMN,RS)                                         !
!
      DC = TAU * NN / (M_E * CHI_Q)                                 ! ref. 1 eq. (9.91)
!
      END SUBROUTINE DIFFUSION_COEF2
!
END MODULE DIFFUSION_COEFFICIENT_2


