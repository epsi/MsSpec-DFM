!
!=======================================================================
!
MODULE DIFFUSION_COEFFICIENT 
!
      USE ACCURACY_REAL
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE DIFFUSION_COEF(DC)
!
!  This subroutine computes the diffusion coefficient
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Oct 2020
!
!
      USE MATERIAL_PROP,          ONLY : DMN,RS
      USE EXT_FIELDS,             ONLY : T
!
      USE DAMPING_VALUES,         ONLY : DC_TYPE
!
      USE EL_ELE_INTER,           ONLY : S,EPS
!
      USE REAL_NUMBERS,           ONLY : ZERO
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(OUT) ::  DC
!
      IF(DMN == '3D') THEN                                          !
        CALL DIFFUSION_COEFFICIENT_3D(T,S,EPS,RS,DC_TYPE,DC)
      ELSE IF(DMN == '2D') THEN                                     !                                     
        DC = ZERO                                                   ! not yet implemented
      ELSE IF(DMN == '1D') THEN                                     !  
        DC = ZERO                                                   ! not yet implemented
      END IF                                                        !
!
      END SUBROUTINE DIFFUSION_COEF
!
!=======================================================================
!
      SUBROUTINE DIFFUSION_COEFFICIENT_3D(T,S,EPS,RS,DC_TYPE,DC) 
!
!  This subroutine computes the diffusion coefficient for 3D systems
!
!  Input parameters:
!
!       * T        : temperature (SI)     
!       * S        : \
!       * EPS      : /  parameters of the soft-sphere potential
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * DC_TYPE  : diffusion coefficient in 3D
!                       DC_TYPE = 'ASHO'   --> Ashurst-Hoover
!
!
!  Output parameters:
!
!       * DC       : diffusion coefficient
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  DC_TYPE
!
      REAL (WP)             ::  T,S,EPS,RS
      REAL (WP)             ::  DC
!
      IF(DC_TYPE == 'ASHO') THEN                                    !
        DC = ASHO_DC_3D(T,S,EPS,RS)                                 !
      END IF                                                        !
!
      END SUBROUTINE DIFFUSION_COEFFICIENT_3D 
!
!=======================================================================
!
      FUNCTION ASHO_DC_3D(T,S,EPS,RS) 
!
!  This function computes the Ashurst-Hoover diffusion coefficient 
!    for 3D hard-sphere fluid at a given value of the temperature T
!
!  References: (1) W. T. Ashusrt and W. G. Hoover, Phys. Rev. A 11,
!                     658 (1975)
!
!  Note: the model uses a soft-sphere interaction potential given by
!
!                 V_SS(R) = EPS * ( S/R )**12
!
!  Warning: the result is valid for reduced densities XX > 0.6
!                          
!           with XX = N0 * (EPS / K_B*T)**0.25
!                        
!
!  Input parameters:
!
!       * T        : temperature (SI)     
!       * S        : \
!       * EPS      : /  parameters of the soft-sphere potential
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!  Output parameters:
!
!       * ASHO_DC_3D
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : TWO,FOURTH
      USE CONSTANTS_P1,        ONLY : H_BAR,M_E,K_B
      USE PI_ETC,              ONLY : PI
      USE UTILITIES_1,         ONLY : RS_TO_N0
!
      IMPLICIT NONE
!
      REAL (WP)             ::  T,S,EPS,RS
      REAL (WP)             ::  ASHO_DC_3D
      REAL (WP)             ::  N0
      REAL (WP)             ::  D0,CC,XX,KK,S3
!
      REAL (WP)             ::  SQRT,EXP
!
      KK = 0.416666666666666666666666666666666667E0_WP              ! 5/12
!
      S3 = S * S * S                                                !
!
!  Computing the electron density
!
      N0 = RS_TO_N0('3D',RS)                                        !
!
      D0 = 4.9E0_WP                                                 !
      CC = 6.3E0_WP                                                 !
      XX = N0 * S3 * (EPS / (K_B*T))**FOURTH / SQRT(TWO)            ! reduced density p. 663
!
      ASHO_DC_3D = S * SQRT(EPS / M_E) * (K_B * T / EPS)**KK *    & !
                       D0 * EXP(- CC * XX)                          ! ref. 1 p. 666
!
      END FUNCTION ASHO_DC_3D  
!
END MODULE DIFFUSION_COEFFICIENT 
