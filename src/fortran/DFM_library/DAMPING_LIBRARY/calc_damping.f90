!
!=======================================================================
!
MODULE DAMPING_SI
!
!  This module defines the damping coefficients in SI
!
!
!                    --> SI version  <--
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  LFT,TAU,TAU2,NNU,DIF,ETA
!
END MODULE DAMPING_SI
!
!=======================================================================
!
MODULE DAMPING_COEF
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_DAMPING(IQ,X)
!
!  This subroutine calculates and stores the value of the damping
!    coefficient selected
!
!
!  Input parameters:
!
!       * IQ       : q_loop index
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!
!
!  Output parameters:
!
!       * LFT      : quasiparticle lifetime in seconds
!       * TAU      : 1st relaxation time in seconds
!       * TAU2     : 2nd relaxation time in seconds
!       * NNU      : decay rate in 1/s
!       * DIF      : diffusion coefficient in SI
!       * ETA      : shear viscosity in SI
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  6 Aug 2021
!
!
      USE OUT_VALUES_10,       ONLY : I_WR
!
      USE REAL_NUMBERS,        ONLY : ZERO,LARGE
!
      USE DAMPING_VALUES
!
      USE LIFETIME
      USE RELAXATION_TIME_STATIC
      USE DECAY_RATE
      USE DIFFUSION_COEFFICIENT
      USE VISCOSITY
      USE EXTERNAL_DAMPING
!
      USE DAMPING_SI
!
      IMPLICIT NONE
!
      INTEGER, INTENT(IN)    ::  IQ
!
      INTEGER                ::  I,LOGF
!
      REAL (WP), INTENT(IN)  ::  X                ! q / 2k_F
!
      REAL (WP)              ::  POW1,POW2
!
      LOGF = 6                                                      ! log file unit
!
!  Initialization
!
      LFT  = LARGE                                                  !
      TAU  = LARGE                                                  !
      TAU2 = LARGE                                                  !
      NNU  = ZERO                                                   !
      DIF  = ZERO                                                   !
      ETA  = ZERO                                                   !
!
!  Power for external value
!
      CALL CALC_POWER(POWER_1,POW1)                                 !
!
      IF(DAMPING == 'RELA' .AND. RT_TYPE == 'EX2') THEN             !
        CALL CALC_POWER(POWER_2,POW2)                               !
      END IF                                                        !
!
!  Setting up the selected kind of damping
!
      IF(DAMPING == 'LFTM') THEN                                    !
        IF(LT_TYPE == 'EXTE') THEN                                  !
          LFT = D_VALUE_1 * POW1                                    !
        ELSE                                                        !
          CALL LIFETIME_COEF(X,LFT)                                 !
        END IF                                                      !
      ELSE IF (DAMPING == 'RELA') THEN                              !
        IF(RT_TYPE == 'EX1') THEN                                   !
          TAU = D_VALUE_1 * POW1                                    !
        ELSE IF(RT_TYPE == 'EX2') THEN                              !
          TAU  = D_VALUE_1 * POW1                                   !
          TAU2 = D_VALUE_2 * POW2                                   !
        ELSE                                                        !
          CALL RELAXATION_TIME(X,TAU)                               !
        END IF                                                      !
      ELSE IF (DAMPING == 'DECA') THEN                              !
        IF(DR_TYPE == 'EXTE') THEN                                  !
          NNU = D_VALUE_1 * POW1                                    !
        ELSE                                                        !
          CALL DECAY_RATE_COEF(X,NNU)                               !
        END IF                                                      !
      ELSE IF (DAMPING == 'DIFF') THEN                              !
        IF(DC_TYPE == 'EXTE') THEN                                  !
          DIF = D_VALUE_1 * POW1                                    !
        ELSE                                                        !
          CALL DIFFUSION_COEF(DIF)                                  !
        END IF                                                      !
      ELSE IF (DAMPING == 'VISC') THEN                              !
        IF(VI_TYPE == 'EXTE') THEN                                  !
          ETA = D_VALUE_1 * POW1                                    !
        ELSE                                                        !
          CALL VISCOSITY_COEF(X,ETA)                                !
        END IF                                                      !
      END IF                                                        !
!
!  Printing the results
!
      IF( (I_WR == 1) .OR. ( (I_WR == 2) .AND. (IQ == 1) )  ) THEN  !
        DO I = 1, 3                                                 !
          WRITE(LOGF,5)                                             !
        END DO                                                      !
        IF(DAMPING /= 'NONE') THEN                                  !
          WRITE(LOGF,10)                                            !
          WRITE(LOGF,20)                                            !
          IF(DAMPING == 'LFTM') THEN                                !
            WRITE(LOGF,31) LFT                                      !
          ELSE IF (DAMPING == 'RELA') THEN                          !
            WRITE(LOGF,32) TAU                                      !
            IF(RT_TYPE == 'EX2') WRITE(LOGF,33) TAU2                !
          ELSE IF (DAMPING == 'DECA') THEN                          !
            WRITE(LOGF,34) NNU                                      !
          ELSE IF (DAMPING == 'DIFF') THEN                          !
            WRITE(LOGF,35) DIF                                      !
          ELSE IF (DAMPING == 'VISC') THEN                          !
            WRITE(LOGF,36) ETA                                      !
          END IF                                                    !
          WRITE(LOGF,40)                                            !
        END IF                                                      !
      END IF                                                        !
!
!  Formats:
!
   5  FORMAT('      ')
  10  FORMAT(6X,'_________________________________________________________')
  20  FORMAT(5X,'|                                                         |')
  31  FORMAT(5X,'|  DAMPING CHOSEN: lifetime              = ',E12.6,' s |')
  32  FORMAT(5X,'|  DAMPING CHOSEN: relaxation time       = ',E12.6,' s |')
  33  FORMAT(5X,'|  DAMPING CHOSEN: relaxation time 2     = ',E12.6,' s |')
  34  FORMAT(5X,'|  DAMPING CHOSEN: decay time            = ',E12.6,' /s|')
  35  FORMAT(5X,'|  DAMPING CHOSEN: diffusion coefficient = ',E12.6,' SI|')
  36  FORMAT(5X,'|  DAMPING CHOSEN: viscosity             = ',E12.6,' SI|')
  40  FORMAT(5X,'|_________________________________________________________|',/)
!
      END SUBROUTINE CALC_DAMPING
!
END MODULE  DAMPING_COEF
