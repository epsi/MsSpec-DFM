!
!=======================================================================
!
MODULE RELAXATION_TIME_STATIC
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE RELAXATION_TIME(X,TAU)
!
!  This subroutine computes the rélaxation time
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Dec 2020
!
!
      USE MATERIAL_PROP,          ONLY : DMN,RS,MSOM,EPS_B
      USE EXT_FIELDS,             ONLY : T
      USE MULTILAYER,             ONLY : DL,H_TYPE
      USE LF_VALUES,              ONLY : GQ_TYPE
      USE SF_VALUES,              ONLY : SQ_TYPE
      USE DAMPING_VALUES
!
      USE EL_ELE_INTER
      USE EL_PHO_INTER
      USE EL_IMP_INTER
!
      USE REAL_NUMBERS,           ONLY : ZERO,ONE,INF
      USE CONSTANTS_P1,           ONLY : M_E
      USE ENE_CHANGE,             ONLY : EV
!
      IMPLICIT NONE
!
      INTEGER               ::  I_ET
      INTEGER               ::  I_E,I_P,I_I
!
      REAL (WP), INTENT(IN) ::  X
      REAL (WP), INTENT(OUT)::  TAU
!
      REAL (WP)             ::  EK_SI
      REAL (WP)             ::  TAU_EE,TAU_EP,TAU_EI
      REAL (WP)             ::  INV_EE,INV_EP,INV_EI
      REAL (WP)             ::  SUM_INV
      REAL (WP)             ::  MASS_E,LR
      REAL (WP)             ::  TEI,TAU_E,S_L
!
!  Kinetic energy of the electrin in SI
!
      EK_SI  = EK * EV                                              !
!
      MASS_E = MSOM * M_E                                           ! effective mass of electron
      LR     = ZERO                                                 ! residual mfp (temporary)
      TEI    = ZERO                                                 ! e-imp collision time (temporary)
      TAU_E  = ZERO                                                 ! elastic scattering time (temporary)
      S_L    = ZERO                                                 ! scattering length (temporary)
!
      I_E = 0                                                       ! e-e calculation switch
      I_P = 0                                                       ! e-p calculation switch
      I_I = 0                                                       ! e-i calculation switch
!
!  Relaxation time initialization
!
      IF(RT_TYPE == ' NO') THEN                                     !
        INV_EE = ZERO                                               !
        INV_EP = ZERO                                               !
        INV_EI = ZERO                                               !
        GO TO 10                                                    !
      ELSE IF(RT_TYPE == 'E-E') THEN                                !
        INV_EP = ZERO                                               !
        INV_EI = ZERO                                               !
        I_E    = 1                                                  !
      ELSE IF(RT_TYPE == 'E-P') THEN                                !
        INV_EE = ZERO                                               !
        INV_EI = ZERO                                               !
        I_P    = 1                                                  !
      ELSE IF(RT_TYPE == 'E-I') THEN                                !
        INV_EE = ZERO                                               !
        INV_EP = ZERO                                               !
        I_I    = 1                                                  !
      ELSE IF(RT_TYPE == 'ALL') THEN                                !
        I_E    = 1                                                  !
        I_P    = 1                                                  !
        I_I    = 1                                                  !
      END IF                                                        !
!
!  Computation of the electron-electron inverse relaxation time
!
      IF(EE_TYPE /= 'NONE' .AND. I_E == 1) THEN                     !
        IF(DMN == '3D') THEN                                        !
          CALL EE_RT_3D(EE_TYPE,SQ_TYPE,GQ_TYPE,X,RS,T,           & !
                        EK_SI,EI_C,TAU_EE)                          !
        ELSE IF(DMN == '2D') THEN                                   !
          I_ET = 2                                                  ! temporary
          CALL EE_RT_2D(EE_TYPE,RS,T,EK_SI,TAU_E,EPS_B,EI_C,DL,   & !
                        I_ET,H_TYPE,TEI,TAU_EE)                     !
        ELSE IF(DMN == '1D') THEN                                   !
          CALL EE_RT_1D(EE_TYPE,EK_SI,TEI,TAU_EE)                   !
        END IF                                                      !
        INV_EE = ONE / TAU_EE                                       !
      END IF                                                        !
!
!  Computation of the electron-phonon inverse relaxation time
!
      IF(EP_TYPE /= 'NONE' .AND. I_P == 1) THEN                     !
        IF(DMN == '3D') THEN                                        !
          CALL EP_RT_3D(EP_TYPE,T,NA,MA,RA,DEBYE_T,EP_C,EK_SI,    & !
                        RS,TAU_EP)      !
        ELSE IF(DMN == '2D') THEN                                   !
          INV_EP = ZERO                                             ! not yet implemented
        ELSE IF(DMN == '1D') THEN                                   !
          INV_EP = ZERO                                             ! not yet implemented
        END IF                                                      !
        INV_EP = ONE / TAU_EP                                       !
      END IF                                                        !
!
!  Computation of the electron-impurity inverse relaxation time
!
      IF(EI_TYPE /= 'NONE' .AND. I_I == 1) THEN                     !
        IF(DMN == '3D') THEN                                        !
          CALL EI_RT_3D(EI_TYPE,EK_SI,T,RS,NI,EPS_B,MASS_E,TAU_EI)  !
        ELSE IF(DMN == '2D') THEN                                   !
          INV_EI = ZERO                                             ! not yet implemented
        ELSE IF(DMN == '1D') THEN                                   !
          INV_EI = ZERO                                             ! not yet implemented
        END IF                                                      !
        INV_EI = ONE / TAU_EI                                       !
      END IF                                                        !
!
  10  SUM_INV = INV_EE + INV_EP + INV_EI                            !
      IF(SUM_INV /= ZERO) THEN                                      !
        TAU = ONE / SUM_INV                                         !
      ELSE                                                          !
        TAU = INF                                                   !
      END IF                                                        !
!
      END SUBROUTINE RELAXATION_TIME
!
!  Specific cases:
!
!------ 1) electron-phonon case --------------------------------------------
!
!=======================================================================
!
      SUBROUTINE EP_RT_3D(EP_TYPE,T,NA,MA,RA,TH,CA,EE,RS,TAU)
!
!  This subroutine computes the electron-phonon relaxation time
!    in 3D systems
!
!
!  Input parameters:
!
!       * EP_TYPE  : relaxation time functional for electron-phonon
!                       EP_TYPE = 'STEH'   --> Steinberg high-T approximation
!                       EP_TYPE = 'STEL'   --> Steinberg low-T approximation
!       * T        : temperature  (in SI)
!       * MA       : mass of lattice atoms
!       * RA       : radius of atoms
!       * TH       : Debye temperature of the material in SI
!       * CA       : electron-phonon coupling
!       * EE       : energy of electron in SI
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  EP_TYPE
!
      REAL (WP)             ::  T,NA,MA,RA,TH,CA,EE,RS
      REAL (WP)             ::  TAU
!
      IF(EP_TYPE == 'STEH') THEN                                    !
        TAU = STEH_RT_3D(T,MA,RA,TH,CA,EE,RS)                       !
      ELSE IF(EP_TYPE == 'STEL') THEN                               !
        TAU = STEL_RT_3D(T,MA,RA,TH,CA,RS)                          !
      END IF                                                        !
!
      END SUBROUTINE EP_RT_3D
!
!=======================================================================
!
      FUNCTION STEH_RT_3D(T,MA,RA,TH,CA,EE,RS)
!
! This function computes Steinberg's high-temperature relaxation time.
!
! In this model; the electron interacts with acoustic lattice vibrations
!
!
!  Reference:  (1) M. S. Steinberg, Phys. Rev. 109, 1486 (1958)
!
!  Input parameters:
!
!       * T        : temperature  (in SI)
!       * MA       : mass of lattice atoms
!       * RA       : radius of atoms
!       * TH       : Debye temperature of the material in SI
!       * CA       : electron-phonon coupling
!       * EE       : energy of electron in SI
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!  Output parameters:
!
!       * STEH_RT_3D  : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Oct 2020
!
!
      USE REAL_NUMBERS,     ONLY : TWO,THREE,FOUR,SIX,HALF,THIRD
      USE CONSTANTS_P1,     ONLY : H_BAR,M_E,K_B
      USE PI_ETC,           ONLY : PI,PI2
!
      IMPLICIT NONE
!
      REAL (WP)             ::  T,NA,MA,RA,TH,CA,EE,RS
      REAL (WP)             ::  STEH_RT_3D
      REAL (WP)             ::  N0
      REAL (WP)             ::  H,AL,GA,D
      REAL (WP)             ::  NUM,DEN1,DEN2
!
      REAL (WP)             ::  SQRT
!
      AL = (FOUR * PI * THIRD)**THIRD                               ! (4 pi / 4)^{1/3}
!
      H  = TWO * PI * H_BAR                                         ! h = 2 pi * h_bar
!
      GA = AL * (FOUR * MA * RA * K_B * TH) /                     & !
                (THREE * H * H * CA * CA)                           ! /\
!
      D  =  (SIX * PI2)**(TWO * THIRD) * H_BAR * H_BAR /          & ! D
            (FOUR * M_E * RA * RA)                                  !
!
      NUM  = TWO * H_BAR * H_BAR * GA * TH * EE**1.5E0_WP           !
      DEN1 = D * SQRT(HALF * M_E) * T                               !
      DEN2 = THREE - D / EE                                         !
!
      STEH_RT_3D = NUM / (DEN1 * DEN2)                              ! ref. 1 eq. (4.2)
!
      END FUNCTION STEH_RT_3D
!
!=======================================================================
!
      FUNCTION STEL_RT_3D(T,MA,RA,TH,CA,RS)
!
! This function computes Steinberg's low-temperature relaxation time.
!
! In this model; the electron interacts with acoustic lattice vibrations
!
!
!  Reference:  (1) M. S. Steinberg, Phys. Rev. 109, 1486 (1958)
!
!  Input parameters:
!
!       * T        : temperature  (in SI)
!       * MA       : mass of lattice atoms
!       * RA       : radius of atoms
!       * TH       : Debye temperature of the material in SI
!       * CA       : electron-phonon coupling
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!  Output parameters:
!
!       * STEL_RT_3D  : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,FOUR,SIX,THIRD,LARGE
      USE CONSTANTS_P1,     ONLY : H_BAR,M_E,K_B
      USE FERMI_SI,         ONLY : EF_SI
      USE PI_ETC,           ONLY : PI,PI2
      USE SPECIFIC_INT_1,   ONLY : STEI_INT
!
      IMPLICIT NONE
!
      REAL (WP)             ::  T,NA,MA,RA,TH,CA,RS
      REAL (WP)             ::  STEL_RT_3D
      REAL (WP)             ::  H,AL,GA,D,EF3,M3
      REAL (WP)             ::  NUM,DEN1,DEN2
      REAL (WP)             ::  X,J5,J7,X5
!
      REAL (WP)             ::  SQRT
!
      AL = (FOUR * PI * THIRD)**THIRD                               ! (4 pi / 4)^{1/3}
!
      H  = TWO * PI * H_BAR                                         ! h = 2 pi * h_bar
!
      GA = AL * (FOUR * MA * RA * K_B * TH) /                     & !
                (THREE * H * H * CA * CA)                           ! /\
!
      D  =  (SIX * PI2)**(TWO * THIRD) * H_BAR * H_BAR /          & ! D
            (FOUR * M_E * RA * RA)                                  !
!
      EF3 = EF_SI * EF_SI * EF_SI                                   !
      M3  = M_E * M_E * M_E                                         !
!
!  Computation of the J_p(x) functions
!
      X  = TH/T                                                     !
      X5 = X * X * X * X * X                                        !
!
      NUM  = H_BAR * H_BAR * GA * SQRT(EF3) * X5                    !
      DEN1 = THREE * SQRT(TWO * M3) * D                             !
!
!  Computation of the J_p(x) functions
!
      IF(T > ONE) THEN                                              !
!
        J5 = STEI_INT(X,5)                                          !
        J7 = STEI_INT(X,7)                                          !
!
        DEN2 = J5 - J7 / ( X*X / (TWO * EF_SI) )                    !
!
      ELSE                                                          !
!
        J5 = STEI_INT(LARGE,5)                                      !
!
        DEN2 = J5                                                   !
!
      END IF                                                        !
!
      STEL_RT_3D = NUM / (DEN1 * DEN2)                              ! ref. 1 eq. (4.9)
!                                                                   ! ref. 1 eq. (4.10)
      END FUNCTION STEL_RT_3D
!
!------ 2) electron-electron case --------------------------------------------
!
!
!=======================================================================
!
      SUBROUTINE EE_RT_3D(EE_TYPE,SQ_TYPE,GQ_TYPE,X,RS,T, &
                          EK,U,TAU)
!
!  This subroutine computes the electron-electron relaxation time
!    in 3D systems
!
!
!  Input parameters:
!
!       * EE_TYPE  : relaxation time functional for electron-phonon
!                       EE_TYPE = 'ALAR'   --> Al'tshuler-Aronov approximation
!                       EE_TYPE = 'ALA2'   --> Al'tshuler-Aronov approximation
!                       EE_TYPE = 'BACA'   --> Barriga-Carrasco approximation
!                       EE_TYPE = 'FSTB'   --> Fann et al approximation
!                       EE_TYPE = 'PIN1'   --> Pines-Nozières 1st approximation
!                       EE_TYPE = 'PIN2'   --> Pines-Nozières 2nd approximation
!                       EE_TYPE = 'QIV2'   --> Qian-Vignale high-density limit
!                       EE_TYPE = 'QIVI'   --> Qian-Vignale approximation
!                       EE_TYPE = 'RASM'   --> Rammer-Smith approximation
!                       EE_TYPE = 'TAI0'   --> Tanaka-Ichimaru approximation (q = 0)
!                       EE_TYPE = 'TAIQ'   --> q-dependent Tanaka-Ichimaru approximation
!                       EE_TYPE = 'UTIC'   --> Utsumi-Ichimaru approximation
!       * SQ_TYPE  : structure factor approximation (3D)
!       * GQ_TYPE  : local-field correction type (3D)
!       * EC_TYPE  : type of correlation energy functional (3D)
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)
!       * EK       : electron kinetic energy (SI)
!       * U        : strength of impurity scattering
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Dec 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  EE_TYPE,GQ_TYPE
      CHARACTER (LEN = 3)   ::  SQ_TYPE
!
      REAL (WP)             ::  X,RS,T,EK,U,DC
      REAL (WP)             ::  TAU
!
      IF(EE_TYPE == 'ALAR') THEN                                    !
        TAU = ALAR_RT_3D(X,EK)                                      !
      ELSE IF(EE_TYPE == 'ALA2') THEN                               !
        TAU = ALAR_RT_ND('3D',X,EK)                                 !
      ELSE IF(EE_TYPE == 'BACA') THEN                               !
        TAU = BACA_RT_3D(RS,T)                                      !
      ELSE IF(EE_TYPE == 'FSTB') THEN                               !
        TAU = FSTB_RT_3D(RS)                                        !
      ELSE IF(EE_TYPE == 'PIN1') THEN                               !
        TAU = PIN1_RT_3D(EK,T)                                      !
      ELSE IF(EE_TYPE == 'PIN2') THEN                               !
        TAU = PIN2_RT_3D(EK,T)                                      !
      ELSE IF(EE_TYPE == 'QIV2') THEN                               !
        TAU = QIV2_RT_3D(EK,X,T)                                    !
      ELSE IF(EE_TYPE == 'QIVI') THEN                               !
        TAU = QIVI_RT_3D(EK,X,T)                                    !
      ELSE IF(EE_TYPE == 'RASM') THEN                               !
        TAU = RASM_RT_3D(EK,T,RS,U)                                 !
      ELSE IF(EE_TYPE == 'TAI0') THEN                               !
        TAU = TAI0_RT_3D(RS,T)                                      !
      ELSE IF(EE_TYPE == 'TAIQ') THEN                               !
        TAU = TAIQ_RT_3D(X,RS,T)                                    !
      ELSE IF(EE_TYPE == 'UTIC') THEN                               !
        TAU = UTIC_RT_3D(X,RS,T,SQ_TYPE,GQ_TYPE)                    !
      END IF                                                        !
!
      END SUBROUTINE EE_RT_3D
!
!=======================================================================
!
      FUNCTION ALAR_RT_3D(X,EK)
!
!  This function computes Al'tshuler-Aronov approximation for
!    the relaxation time in the presence of impurities for 3D systems
!
!  Reference: (1) B. L. Al'tshuler and A. G. Aronov, JETP Lett. 30,
!                       482-484 (1979)
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!       * EK       : electron kinetic energy (SI)
!
!  Output parameters:
!
!       * ALAR_RT_3D : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 30 Apr 2021
!
!
      USE REAL_NUMBERS,         ONLY : ONE,TWO,THREE,HALF,THIRD
      USE CONSTANTS_P1,         ONLY : H_BAR
      USE FERMI_SI,             ONLY : KF_SI,EF_SI,VF_SI
      USE PI_ETC,               ONLY : PI2,PI3
      USE SQUARE_ROOTS,         ONLY : SQR2
      USE SCREENING_VEC,        ONLY : THOMAS_FERMI_VECTOR
      USE DAMPING_VALUES,       ONLY : DC_TYPE,D_VALUE_1,POWER_1
      USE DIFFUSION_COEFFICIENT
      USE EXTERNAL_DAMPING
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X,EK
      REAL (WP)             ::  ALAR_RT_3D
!
      REAL (WP)             ::  ZK
      REAL (WP)             ::  D,L,TAU
      REAL (WP)             ::  POW
      REAL (WP)             ::  Q,NU0
      REAL (WP)             ::  KS
      REAL (WP)             ::  NUM,DEN,XXX
!
      REAL (WP)             ::  SQRT
!
      ZK = EK - EF_SI                                               ! xi_p in SI
!
!  Computing the diffusion coefficient
!
      IF(DC_TYPE == 'EXTE') THEN                                    !
        CALL CALC_POWER(POWER_1,POW)                                !
        D = D_VALUE_1 * POW                                         !
      ELSE                                                          !
        CALL DIFFUSION_COEF(D)                                      !
      END IF                                                        !
!
      L    = THREE * D / VF_SI                                      ! mean free path in SI
      TAU  = L / VF_SI                                              !
      Q    = X * TWO * KF_SI                                        ! q in SI
!
      NU0  = HALF * KF_SI * KF_SI * KF_SI / (PI2 * EF_SI)           ! DoS at Fermi level
!
!  Computing the Thomas-Fermi screening vector
!
      CALL THOMAS_FERMI_VECTOR('3D',KS)                             !
!
      IF( (Q < ONE / L) .AND. (KS * L > ONE) ) THEN                 !
!
        NUM = ZK**1.5E0_WP                                          !
        DEN = 12.0E0_WP * SQR2 * PI3 * H_BAR * NU0 *              & !
              (H_BAR * D)**1.5E0_WP                                 !
!
        XXX = NUM / DEN                                             ! ref. 1 eq. (8)
!
      ELSE                                                          !
!
        NUM = PI2 * KS * ZK * ZK                                    !
        DEN = 64.0E0_WP * H_BAR * KF_SI * EF_SI                     !
!
        XXX = NUM / DEN                                             ! ref. 1 eq. (8)
!
      END IF                                                        !
!
      ALAR_RT_3D = ONE / XXX                                        ! ref. 1 eq. (8)
!
      END FUNCTION ALAR_RT_3D
!
!=======================================================================
!
      FUNCTION ALAR_RT_ND(DMN,X,EK)
!
!  This function computes Al'tshuler-Aronov approximation for
!    the relaxation time in the presence of impurities
!
!  Reference: (1) B. L. Al'tshuler and A. G. Aronov, in
!                    "Electron-Electron Interactions in Disordered
!                     Solids", A. L. Efros and M. Pollak eds.
!                    (North-Holland,1985)
!
!
!  Input parameters:
!
!       * DMN      : system dimension
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!       * EK       : electron kinetic energy (SI)
!
!  Output parameters:
!
!       * ALAR_RT_ND : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 30 Apr 2021
!
!
      USE REAL_NUMBERS,         ONLY : ONE,TWO,THREE,FOUR,HALF,FOURTH
      USE CONSTANTS_P1,         ONLY : H_BAR
      USE FERMI_SI,             ONLY : KF_SI,EF_SI
      USE PI_ETC,               ONLY : PI,PI_INV
      USE SCREENING_VEC,        ONLY : THOMAS_FERMI_VECTOR
      USE UTILITIES_1,          ONLY : D,DOS_EF
      USE DAMPING_VALUES,       ONLY : DC_TYPE,D_VALUE_1,POWER_1
      USE DIFFUSION_COEFFICIENT
      USE EXTERNAL_DAMPING
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP), INTENT(IN) ::  X,EK
      REAL (WP)             ::  ALAR_RT_ND
!
      REAL (WP)             ::  ZK
      REAL (WP)             ::  DC,POW,KS
      REAL (WP)             ::  NF,DI,DD
      REAL (WP)             ::  XX,DS,F,OD
      REAL (WP)             ::  K1,K2,NUM,DEN,XXX
!
      REAL (WP)             ::  SQRT,LOG,SIN
!
      ZK = EK - EF_SI                                               ! xi_p in SI
!
!  Computing the diffusion coefficient
!
      IF(DC_TYPE == 'EXTE') THEN                                    !
        CALL CALC_POWER(POWER_1,POW)                                !
        DC = D_VALUE_1 * POW                                        !
      ELSE                                                          !
        CALL DIFFUSION_COEF(DC)                                     !
      END IF                                                        !
!
!  Computing the Thomas-Fermi screening vector
!
      CALL THOMAS_FERMI_VECTOR(DMN,KS)                              !
!
! Computing the density of state at Fermi level
!
      NF = DOS_EF(DMN)                                              !
!
      DI = D(DMN)                                                   !
      DD = HALF * DI                                                !
!
      XX = TWO * KF_SI / KS                                         !
!
      IF(DMN == '2D') THEN                                          !
        DS = SQRT(XX * XX - ONE)                                    !
        F  = PI_INV * LOG((XX + DS)/(XX - DS)) / DS                 ! ref. 1 eq. (3.36a)
      ELSE                                                          !
        F= TWO * LOG(ONE + XX * XX)/(XX * XX)                       ! ref. 1 eq. (3.36b)
      END IF                                                        !
!
      IF(DMN == '3D') THEN                                          !
        OD = FOURTH * PI_INV * PI_INV                               !
      ELSE IF(DMN == '2D') THEN                                     !
        OD = HALF * PI_INV                                          !
      ELSE IF(DMN == '1D') THEN                                     !
        OD = PI_INV                                                 !
      END IF                                                        !
!
      K1  = ONE - THREE * F * ( ONE - (ONE + HALF * F)**DD )      & !
                            / (FOUR + F)                            !
      K2  = OD / ( TWO * DI ** SIN(FOURTH * PI * DI) )              !
                                                                    !
      NUM = ZK**DD                                                  !
      DEN = H_BAR * NF * (H_BAR * DC)**DD                           !
!
      XXX = K1 * K2 * NUM / DEN                                     ! ref. 1 eq. (4.4)
!
      ALAR_RT_ND = ONE / XXX                                        ! ref. 1 eq. (4.4)
!
      END FUNCTION ALAR_RT_ND
!
!=======================================================================
!
      FUNCTION BACA_RT_3D(RS,T)
!
!  This function computes Barriga-Carrasco approximation for
!    the relaxation time in the 3D case
!
!  References: (1) M. D. Barriga-Carrasco, Phys. Rev. E 76, 016405 (2007)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature  (in SI)
!
!  Output parameters:
!
!       * BACA_RT_3D  : relaxation time in seconds
!
!  Note: result given in a.u. in ref. (1) eq. (17) --> h nu in Hartree
!
!     --> h nu * Hartree in J
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,NINE
      USE PI_ETC,              ONLY : PI,SQR_PI
      USE CONSTANTS_P2,        ONLY : HARTREE
      USE CONSTANTS_P3,        ONLY : PLANCK
      USE SQUARE_ROOTS,        ONLY : SQR2
      USE UTILITIES_1,         ONLY : RS_TO_N0
      USE PLASMON_SCALE_P,     ONLY : NONID
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  RS,T
      REAL (WP)             ::  BACA_RT_3D
!
      REAL (WP)             ::  N0
      REAL (WP)             ::  CL,NU
!
      REAL (WP)             ::  SQRT,LOG
!
      N0 = RS_TO_N0('3D',RS)                                        !
!
      CL = LOG(SQRT(N0)) / NONID**(1.5E0_WP)                        ! Coulomb logarithm
!
      NU = 16.0E0_WP * SQR2 * (NONID**(1.5E0_WP)) * CL /          & ! ref. 1 eq. (17)
                              (NINE * PI * SQR_PI)                  ! in a.u.
!
      BACA_RT_3D = ONE / (NU  * HARTREE /  PLANCK)                  ! in SI
!
      END FUNCTION BACA_RT_3D
!
!=======================================================================
!
      FUNCTION FSTB_RT_3D(RS)
!
!  This function computes Fann et al approximation for
!    the relaxation time in the 3D case
!
!  References: (1) W. S. Fann et al, Phys. Rev. B 46, 13592-13595 (1992)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!  Output parameters:
!
!       * FSTB_RT  : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE CONSTANTS_P1,        ONLY : H_BAR
      USE PI_ETC,              ONLY : PI2
      USE SQUARE_ROOTS,        ONLY : SQR3
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  RS
      REAL (WP)             ::  FSTB_RT_3D
!
      FSTB_RT_3D = 128.0E0_WP * H_BAR / (PI2 * SQR3 * ENE_P_SI)     ! ref. (1) eq. (2)
!
      END FUNCTION FSTB_RT_3D
!
!=======================================================================
!
      FUNCTION PIN1_RT_3D(EK,T)
!
!  This function computes Pines-Nozières approximation for
!    the relaxation time in the 3D case
!
!  Reference: (1) D. Pines and P. Nozi\`{e}res,
!                    "The Theory of Quantum Liquids -- Normal Fermi Liquids",
!                    (Benjamin, 1966)
!
!
!  Input parameters:
!
!       * EK       : electron kinetic energy (SI)
!       * T        : temperature (SI)
!
!  Output parameters:
!
!       * PIN1_RT_3D : relaxation time in seconds
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Dec 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,HALF
      USE PI_ETC,              ONLY : PI2
      USE CONSTANTS_P1,        ONLY : H_BAR
      USE FERMI_SI,            ONLY : KF_SI
      USE SQUARE_ROOTS,        ONLY : SQR3
      USE PLASMON_ENE_SI
      USE CHEMICAL_POTENTIAL,  ONLY : MU
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,T
      REAL (WP)             ::  PIN1_RT_3D
!
      REAL (WP)             ::  MM,ZK
      REAL (WP)             ::  OMP
      REAL (WP)             ::  XXX
!
      OMP = ENE_P_SI / H_BAR                                        ! omega_p
!
!  Computing the chemical potential
!
      MM = MU('3D',T)                                               ! chemical potential is SI
!
      ZK = EK - MM                                                  ! xi_p in SI
!
      XXX = PI2 * SQR3 * OMP * (ZK / MM)**2 / 128.0E0_WP            !
!
      PIN1_RT_3D = ONE / XXX                                        ! ref. (1) eq. (5.134c)
!
      END FUNCTION PIN1_RT_3D
!
!=======================================================================
!
      FUNCTION PIN2_RT_3D(EK,T)
!
!  This function computes Pines-Nozières approximation for
!    the relaxation time in the 3D case
!
!  Reference: (1) D. Pines and P. Nozi\`{e}res,
!                    "The Theory of Quantum Liquids -- Normal Fermi Liquids",
!                    (Benjamin, 1966)
!
!
!  Input parameters:
!
!       * EK       : electron kinetic energy (SI)
!       * T        : temperature (SI)
!
!  Output parameters:
!
!       * PIN2_RT_3D : relaxation time in seconds
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Dec 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,HALF
      USE PI_ETC,              ONLY : PI_INV
      USE CONSTANTS_P1,        ONLY : BOHR,H_BAR,E,M_E
      USE FERMI_SI,            ONLY : KF_SI,VF_SI
      USE CHEMICAL_POTENTIAL,  ONLY : MU
      USE SPECIFIC_INT_10
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,T
      REAL (WP)             ::  PIN2_RT_3D
!
      REAL (WP)             ::  MM,ZK,K
      REAL (WP)             ::  NUM,DEN,KOEF
      REAL (WP)             ::  INTG
      REAL (WP)             ::  XXX
!
      REAL (WP)             ::  SQRT
!
!  Computing the chemical potential
!
      MM = MU('3D',T)                                               ! chemical potential is SI
!
      ZK = EK - MM                                                  ! xi_p in SI
      K  = SQRT(TWO * M_E * EK) / H_BAR                             ! k in SI
!
      NUM  = TWO * PI_INV * E * E * KF_SI * ZK * ZK                 !
      DEN  = H_BAR * H_BAR * H_BAR * BOHR * VF_SI * VF_SI           !
      KOEF = NUM / DEN                                              !
!
!  Computing the integral
!
      CALL INT_PINO(INTG)                                           !
!
      XXX = KOEF * INTG                     !
!
      PIN2_RT_3D = ONE / XXX                                        ! ref. (1) eq. (5.134b)
!
      END FUNCTION PIN2_RT_3D
!
!=======================================================================
!
      FUNCTION QIVI_RT_3D(EK,X,T)
!
!  This function computes Qian-Vignale approximation for
!    the relaxation time in the 3D case
!
!  Reference: (1) Z. Qian and G. Vignale,
!                    Phys. Rev. B 71, 075112 (2005)
!             (2) J. Daligault, Phys. Rev. Lett. 119, 045002 (2017)
!
!
!  Input parameters:
!
!       * EK       : electron kinetic energy (SI)
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!       * T        : temperature (SI)
!
!  Output parameters:
!
!       * QIVI_RT_3D : relaxation time in seconds
!
!
!  Note: There is a factor 1/ h_bar^6 missing in ref. (1) (see eq. (5) ref. (2))
!
!        In order to avoid dealing with too large or small numbers, we rewrite
!
!         3  4
!        m  e          m          1
!       -------  =  --------   -------
!             6           2        2
!        h_bar       h_bar        a
!                                  0
!
!
!       so that the first coefficient becomes
!
!            1          1     k         1
!       ------------  ----  ----  -------------     =  K0
!        2 pi h_bar    EK    KS     (a_0 KS)^2
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Dec 2020
!
!
      USE MATERIAL_PROP,       ONLY : RS
      USE REAL_NUMBERS,        ONLY : ZERO,ONE,TWO,HALF,TTINY,INF
      USE CONSTANTS_P1,        ONLY : BOHR,H_BAR,M_E,K_B
      USE FERMI_SI,            ONLY : KF_SI
      USE PI_ETC,              ONLY : PI,PI2,PI_INV
      USE SCREENING_TYPE
      USE SCREENING_VEC
      USE CHEMICAL_POTENTIAL,  ONLY : MU
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,X,T
      REAL (WP)             ::  QIVI_RT_3D
!
      REAL (WP)             ::  MM,ZK,K
      REAL (WP)             ::  KS,LD
      REAL (WP)             ::  ND,NE,BE
      REAL (WP)             ::  NUM,DEN,CC
      REAL (WP)             ::  R0,R1,R2
      REAL (WP)             ::  K0,K1,K2
!
      REAL (WP)             ::  SQRT,EXP,LOG,ATAN
!
!  Computing the chemical potential
!
      MM = MU('3D',T)                                               ! chemical potential is SI
!
      ZK = EK - MM                                                  ! xi_p in SI
      K  = SQRT(TWO * M_E * EK) / H_BAR                             ! k in SI
!
!  Pathological cases
!
      IF(EK <= MM) THEN                                             !
        QIVI_RT_3D = INF                                            !
        GO TO 10                                                    !
      END IF                                                        !
!
!  Computing the screening vector
!
      IF(SC_TYPE == 'NO') THEN                                      !
        CALL SCREENING_VECTOR('TF','3D',X,RS,T,KS)                  !
      ELSE                                                          !
        CALL SCREENING_VECTOR(SC_TYPE,'3D',X,RS,T,KS)               ! in SI
      END IF                                                        !
!
      R0 = HALF * PI_INV / H_BAR
      R1 = K / KS                                                   !
      R2 = ONE / (BOHR * KS)**2                                     !
!
      LD = TWO * KF_SI / KS                                         ! lambda
      BE = ONE / (K_B * T)                                          ! beta
!
!  Direct contribution ND and exchange contribution NE
!
      IF(ZK < LOG(TTINY)/BE) THEN                                   !
        CC = EXP(- BE * ZK)                                         !
      ELSE                                                          ! <-- pathological
        CC = ZERO                                                   !     case
      END IF                                                        !
!
      NUM = PI2 / (BE * BE) + ZK * ZK                               !
      DEN = ONE + CC                                                !
!
      K0  = R0 * R1 * R2 / EK                                       ! see Note
      K1  = NUM / DEN                                               !
      K2  = ONE / SQRT(LD * LD + TWO)                               !
!
      ND =   K0 * K1 * ( LD / (LD * LD + ONE) + ATAN(LD) )          ! ref. 1 eq. (29)
      NE = - K0 * K1 * K2 * ( HALF * PI - ATAN(SQRT(K2 / LD)) )     ! ref. 1 eq. (32)
!
      QIVI_RT_3D = ONE / (ND + NE)                                  ! ref. 1 eq. (3)
!
  10  RETURN
!
      END FUNCTION QIVI_RT_3D
!
!=======================================================================
!
      FUNCTION QIV2_RT_3D(EK,X,T)
!
!  This function computes the high-density limit Qian-Vignale approximation
!    for the relaxation time in the 3D case (direct term onmy)
!
!  Reference: (1) Z. Qian and G. Vignale,
!                    Phys. Rev. B 71, 075112 (2005)
!             (2) J. Daligault, Phys. Rev. Lett. 119, 045002 (2017)
!
!
!  Input parameters:
!
!       * EK       : electron kinetic energy (SI)
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!       * T        : temperature (SI)
!
!  Output parameters:
!
!       * QIV2_RT_3D : relaxation time in seconds
!
!
!  Note: There is a factor 1/ h_bar^6 missing in ref. (1) (see eq. (5) ref. (2))
!
!        In order to avoid dealing with too large or small numbers, we rewrite
!
!         3  4
!        m  e          m          1
!       -------  =  --------   -------
!             6           2        2
!        h_bar       h_bar        a
!                                  0
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Dec 2020
!
!
      USE MATERIAL_PROP,       ONLY : RS
      USE REAL_NUMBERS,        ONLY : ONE,TWO,HALF,FOURTH
      USE CONSTANTS_P1,        ONLY : BOHR,H_BAR,M_E
      USE FERMI_SI,            ONLY : KF_SI
      USE PI_ETC,              ONLY : PI
      USE UTILITIES_1,         ONLY : ALFA
      USE SCREENING_TYPE
      USE SCREENING_VEC
      USE CHEMICAL_POTENTIAL,  ONLY : MU
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  EK,X,T
      REAL (WP)             ::  QIV2_RT_3D
!
      REAL (WP)             ::  MM,ZK,K
      REAL (WP)             ::  KS
      REAL (WP)             ::  R0,R1,R2,DEN
!
      REAL (WP)             ::  SQRT
!
!  Computing the chemical potential
!
      MM = MU('3D',T)                                               ! chemical potential is SI
!
      ZK = EK - MM                                                  ! xi_p in SI
      K  = SQRT(TWO * M_E * EK) / H_BAR                             ! k in SI
!
!  Computing the screening vector
!
!
      IF(SC_TYPE == 'NO') THEN                                      !
        CALL SCREENING_VECTOR('TF','3D',X,RS,T,KS)                  !
      ELSE                                                          !
        CALL SCREENING_VECTOR(SC_TYPE,'3D',X,RS,T,KS)               ! in SI
      END IF                                                        !
!
      R0 = FOURTH / H_BAR                                           !
      R1 = K / KS                                                   !
      R2 = ONE / (BOHR * KS)**2                                     !
!
      DEN = R0 * R1 * R2 * ZK * ZK / EK                             !
!
      QIV2_RT_3D = ONE / DEN                                        !
!
      END FUNCTION QIV2_RT_3D
!
!=======================================================================
!
      FUNCTION RASM_RT_3D(EK,T,RS,U)
!
!  This function computes Rammer-Smith approximation for
!    the e-e relaxation time in the 3D case
!
!  Reference: (1) J. Rammer and H. Smith, Rev. Mod. Phys. 58, 323 (1986)
!
!  Note: uses the e-impurity scattering time as a reference time
!
!
!  Input parameters:
!
!       * EK       : electron energy (SI)
!       * T        : temperature  (in SI)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * U        : strength of impurity scattering
!
!  Output parameters:
!
!       * RASM_RT_3D : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,THREE,FOUR,SIX,EIGHT
      USE CONSTANTS_P1,        ONLY : E
      USE FERMI_SI,            ONLY : KF_SI,VF_SI
      USE PI_ETC,              ONLY : PI,PI2,PI3
      USE SCREENING_VEC,       ONLY : THOMAS_FERMI_VECTOR
      USE CHEMICAL_POTENTIAL,  ONLY : MU
      USE UTILITIES_1,         ONLY : DOS_EF
!
      IMPLICIT NONE
!
      REAL (WP)             ::  EK,T,RS,U
      REAL (WP)             ::  RASM_RT_3D
      REAL (WP)             ::  KS,IS,NF
      REAL (WP)             ::  MM,L,NUM,DEN
      REAL (WP)             ::  EPS,ZZ
!
      REAL (WP)             ::  SQRT
!
      EPS = 1.E-2_WP                                                !
      ZZ  = 2.612375348685488343348567567924071630571E0_WP          ! zeta(3/2)
!
!  Computing the Thomas-Fermi screening vector
!
      CALL THOMAS_FERMI_VECTOR('3D',KS)                             !
!
!  Computing the chemical potential
!
      MM = MU('3D',T)                                               !
!
!  Computing the density of states at Fermi level
!
      NF = DOS_EF('3D')                                             !
!
!  Impurity scattering rate (1/tau)
!
      IS = PI * U * U * NF                                          ! ref. 1 eq. (4.13)
!
!  Mean free path
!
      L = VF_SI / IS                                                !
!
      IF(T < EPS) THEN                                              ! zero temperature
!
        IF(EK < IS)  THEN                                           !
!
          NUM = FOUR * SQRT(IS) * KF_SI * KF_SI * L * L             !
          DEN = EK * SQRT(SIX * EK)                                 !
!
          RASM_RT_3D = NUM / DEN                                    ! ref. 1 eq. (4.73)
!
        ELSE                                                        !
!
          IF(KS < KF_SI) THEN                                       !
!
            NUM = 64.0E0_WP * KF_SI * MM                            !
            DEN = PI2 * KS * EK * EK                                !
!
            RASM_RT_3D = NUM / DEN                                  ! ref. 1 eq. (4.74)
!
          ELSE                                                      !
!
            NUM = 16.0E0_WP * MM                                    !
            DEN = PI * EK * EK                                      !
!
            RASM_RT_3D = NUM / DEN                                  ! ref. 1 eq. (4.74)
!
          END IF                                                    !
        END IF                                                      !
!
      ELSE                                                          !
!
        IF(T < IS) THEN                                             !
!
          NUM = 16.0E0_WP * KF_SI * L * MM                          !
          DEN = THREE * SQRT(THREE * PI) * ZZ *                   & !
                       (SQRT(EIGHT) - ONE) * SQRT(IS * T * T * T )  !
!
          RASM_RT_3D = NUM / DEN                                    ! ref. 1 eq. (4.76)
!
        ELSE                                                        !
!
          IF(KS < KF_SI) THEN                                       !
!
            NUM = EIGHT * VF_SI * VF_SI * KS                        !
            DEN = PI3 * E * E * T * T                               !
!
            RASM_RT_3D = NUM / DEN                                  ! ref. 1 eq. (4.77)
!
          ELSE                                                      !
!
            NUM = 16.0E0_WP * MM                                    !
            DEN = PI3 * T * T                                       !
!
            RASM_RT_3D = NUM / DEN                                  ! ref. 1 eq. (4.77)
!
          END IF                                                    !
        END IF                                                      !
!
      END IF                                                        !
!
      END FUNCTION RASM_RT_3D
!
!=======================================================================
!
      FUNCTION TAI0_RT_3D(RS,T)
!
!  This function computes Tanaka-Ichimaru approximation for
!    the relaxation time at q = 0 in the 3D case
!
!  References: (1) S. Tanaka and S. Ichimaru, Phys. Rev. A 35,
!                     4743-4754 (1987)
!              (2) S. Ichimaru, Rev. Mod. Phys. 54, 1017-1059 (1982)
!
!
!  Validity: This relaxation time is valid for strongly coupled,
!            classical one-component plasma
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature  (in SI)
!
!  Output parameters:
!
!       * TAI0_RT_3D  : relaxation time in seconds
!
!
!  Formula: Following the derivation in the user's guide, we obtain
!
!
!                                                1
!      TAU  =  ETA_L    x   ---------------------------------------------------
!                             n k_B T  +   1    -   n h_bar omega_p^2 Gamma_I
!                                        -----     ---------------------------
!                                         K_T               omega_F
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Dec 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,FOUR,HALF,THIRD
      USE PI_ETC,           ONLY : PI_INV
      USE CONSTANTS_P1,     ONLY : H_BAR,K_B
      USE FERMI_SI,         ONLY : EF_SI,KF_SI
      USE UTILITIES_1,      ONLY : ALFA,RS_TO_N0
      USE VISCOSITY,        ONLY : LHPO_VISC_3D
      USE ASYMPT,           ONLY : G0,GI
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  RS,T
      REAL (WP)             ::  TAI0_RT_3D
!
      REAL (WP)             ::  ETA,ETA_L
      REAL (WP)             ::  N0,ALPHA,KBT
      REAL (WP)             ::  OMP,OMF
      REAL (WP)             ::  E1,E2,E3
      REAL (WP)             ::  NUM,DEN,TAU_0
!
!  Computing the viscosity (LHPO case only)
!
      ETA = LHPO_VISC_3D(RS,T)                                      !
!
      ETA_L = FOUR * THIRD * ETA                                    ! longitudinal viscosity
!
!  Computing the electron density
!
      N0 = RS_TO_N0('3D',RS)                                        !
!
      ALPHA = ALFA('3D')                                            !
!
      KBT = K_B * T                                                 !
      OMP = ENE_P_SI / H_BAR                                        !
      OMF = EF_SI / H_BAR                                           !
!
!  Computing the energies in the denominator
!
      E1 = KBT                                                      !
      E2 = TWO * THIRD * EF_SI * (ONE - FOUR * PI_INV *           & !
                                  ALPHA * RS * G0)                  !
      E3 = - HALF * ENE_P_SI * OMP * GI / OMF                       !
!
      NUM = ETA_L                                                   !
      DEN = N0 * (E1 + E2 + E3)                                     !
!
      TAI0_RT_3D = NUM / DEN                                        !
!
      END FUNCTION TAI0_RT_3D
!
!=======================================================================
!
      FUNCTION TAIQ_RT_3D(X,RS,T)
!
!  This function computes the relaxation time as a function of q
!
!  References: (1) S. Tanaka and S. Ichimaru, Phys. Rev. A 35,
!                     4743-4754 (1987)
!
!
!  Validity: This relaxation time is valid for strongly coupled,
!            classical one-component plasma
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)
!
!  Output parameters:
!
!       * TAIQ_RT_3D : relaxation time in seconds
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Dec 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO
      USE FERMI_SI,         ONLY : KF_SI
      USE CONSTANTS_P1,     ONLY : BOHR
      USE DAMPING_VALUES,   ONLY : QD_TYPE,ZETA
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X,RS,T
      REAL (WP)             ::  Q_SI,Q_FACT,XX,X2,TAU_0
      REAL (WP)             ::  TAIQ_RT_3D
!
      REAL (WP)             ::  EXP
!
      Q_SI = TWO * X * KF_SI                                        ! q in SI
!
      XX = RS * BOHR * Q_SI / ZETA                                  !
      X2 = XX * XX                                                  !
!
!  Computing the q-dependent factor
!
      IF(QD_TYPE == 'NONE') THEN                                    !
!
        Q_FACT = ONE                                                !
!
      ELSE IF(QD_TYPE == 'GAUS') THEN                               !
!
        Q_FACT = EXP(- X2)                                          !
!
      ELSE IF(QD_TYPE == 'LORE') THEN                               !
!
       Q_FACT = ONE / (ONE + X2)                                    !
!
      END IF                                                        !
!
!  Computing the q = 0 value (TAIC case only)
!
      TAU_0 = TAI0_RT_3D(RS,T)                                      !
!
      TAIQ_RT_3D = TAU_0 * Q_FACT                                   !
!
      END FUNCTION TAIQ_RT_3D
!
!=======================================================================
!
      FUNCTION UTIC_RT_3D(X,RS,T,SQ_TYPE,GQ_TYPE)
!
!  This function computes low-q Utsumi-Ichimaru approximation for
!    the relaxation time in the 3D case
!
!  Reference: (1) K. Utsumi and S. Ichimaru,
!                    Phys. Rev. B 22, 1522-1533 (1980)
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature  (in SI)
!       * SQ_TYPE  : structure factor approximation (3D)
!       * GQ_TYPE  : local-field correction type (3D)
!
!  Output parameters:
!
!       * UTIC_RT  : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  2 Dec 2020
!
!
      USE REAL_NUMBERS,        ONLY : TWO,FOUR,HALF
      USE CONSTANTS_P1,        ONLY : H_BAR,M_E
      USE FERMI_SI,            ONLY : EF_SI,KF_SI
      USE PI_ETC,              ONLY : PI
      USE SQUARE_ROOTS,        ONLY : SQR3
      USE SCREENING_VEC,       ONLY : THOMAS_FERMI_VECTOR
      USE SPECIFIC_INT_2,      ONLY : INT_SQM1
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X,RS,T
      REAL (WP)             ::  UTIC_RT_3D
!
      REAL (WP)             ::  MAX_X
      REAL (WP)             ::  Q_SI
      REAL (WP)             ::  OMQ,OMP,OMF
      REAL (WP)             ::  KS,X_TF
      REAL (WP)             ::  IN
      REAL (WP)             ::  NUM,DEN
!
      INTEGER               ::  IN_MODE,NSIZE,LL
!
      CHARACTER (LEN = 4)   ::  GQ_TYPE
      CHARACTER (LEN = 3)   ::  SQ_TYPE
!
      IN_MODE = 3                                                   !
!
      MAX_X   = TWO                                                ! integral upper bound
      NSIZE   = 200                                                 ! number of integration points
      LL      = 0                                                   ! unused parameter for INT_SQM1
!
      Q_SI  = TWO * X * KF_SI                                       !  q in SI
!
      OMQ = HALF  * H_BAR * Q_SI * Q_SI / M_E                       ! omega_q in SI
      OMP = ENE_P_SI / H_BAR                                        ! omega_p in SI
      OMF = EF_SI / H_BAR                                           ! omega_F in SI
!
!  Computing Thomas-Fermi screening vector
!
      CALL THOMAS_FERMI_VECTOR('3D',KS)                             !
!
      X_TF = KS / KF_SI                                             ! q_{TF} / k_F
!
!  Computing the integral
!
      CALL INT_SQM1(NSIZE,MAX_X,IN_MODE,RS,T,X_TF,LL,SQ_TYPE,      &!
                    GQ_TYPE,IN)                                     !
!
      NUM = - FOUR * SQR3 * OMF * KF_SI                             !
      DEN =   PI * OMQ * OMP * KS * IN                              !
!
      UTIC_RT_3D = NUM / DEN                                        ! ref. 1 eq. (5.4)
!
      END FUNCTION UTIC_RT_3D
!
!=======================================================================
!
      SUBROUTINE EE_RT_2D(EE_TYPE,RS,T,EK,TAU_E,EPS,U,B,I_ET,  &
                          H_TYPE,TEI,TAU)
!
!  This subroutine computes the electron-electron relaxation time
!    in 2D systems
!
!
!  Input parameters:
!
!       * EE_TYPE  : relaxation time functional for electron-phonon
!                       EE_TYPE = 'FUAB'   --> Fukuyama-Abrahams approx.
!                       EE_TYPE = 'LUFO'   --> Lucas-Fong approx. -graphene-
!                       EE_TYPE = 'QIVI'   --> Quinn-Vignale approx.
!                       EE_TYPE = 'RASM'   --> Rammer-Smith approx.
!                       EE_TYPE = 'REWI'   --> Reizer-Wilkins approx.
!                       EE_TYPE = 'SHAS'   --> Sharma-Ashraf approx.
!                       EE_TYPE = 'ZHDA'   --> Zhang-Das Sarma approx.
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)
!       * EK       : electron kinetic energy (SI)
!       * TAU_E    : ELASTIC scattering time (SI)
!       * EPS      : substrate dielectric constant
!       * U        : strength of impurity scattering
!       * B        : interlayer distance (SI)
!       * I_ET     : switch
!                       I_ET  =  1 --> relaxation time = f(T)
!                       I_ET  =  2 --> relaxation time = f(EK)
!       * H_TYPE   : heterostructure type
!                       H_TYPE  =  'SSL1' semiconductor superlattice of type I
!                       H_TYPE  =  'SSL2' semiconductor superlattice of type II
!       * TEI      : electron-impurity collision time
!
!
!  Internal parameter
!
!       * I_F      : switch for choice of formula (for 'LUFO')
!                       I_F  = 1 --> eq.   (4) ref. 1 graphene
!                       I_F  = 2 --> eq.  (20) ref. 1 2D Fermi liquid
!                       I_F  = 3 --> eq. (129) ref. 1 collinear scattering
!                       I_F  = 4 --> eq. (134) ref. 1 imbalance mode
!                       I_F  = 5 --> eq. (143) ref. 1 graphene in Fermi liquid limit
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  EE_TYPE,H_TYPE
!
      REAL (WP)             ::  RS,T,EK,TAU_E,EPS,U,B,TEI
      REAL (WP)             ::  TAU
!
      INTEGER               ::  I_ET,I_F
!
      I_F=1                                                         !
!
      IF(EE_TYPE == 'FUAB') THEN                                    !
        TAU=FUAB_RT_2D(TAU_E,T)                                     !
      ELSE IF(EE_TYPE == 'LUFO') THEN                               !
        TAU=LUFO_RT_2D(T,EPS,I_F)                                   !
      ELSE IF(EE_TYPE == 'QIVI') THEN                               !
        TAU=QIVI_RT_2D(EK,T,RS)                                     !
      ELSE IF(EE_TYPE == 'RASM') THEN                               !
        TAU=RASM_RT_2D(EK,T,RS,U)                                   !
      ELSE IF (EE_TYPE == 'REWI') THEN                              !
        TAU=REWI_RT_2D(EK,T,RS,B,I_ET,H_TYPE)                       !
      ELSE IF(EE_TYPE == 'SHAS') THEN                               !
        TAU=SHAS_RT_2D(EK,TEI)                                      !
      ELSE IF(EE_TYPE == 'ZHDA') THEN                               !
        TAU=ZHDA_RT_2D(EK,T)                                        !
      END IF                                                        !
!
      END SUBROUTINE EE_RT_2D
!
!=======================================================================
!
      FUNCTION FUAB_RT_2D(TAU,T)
!
!  This function computes Fukuyama-Abrahams approximation for
!    the INELASTIC scattering time in 2D disordered metals
!
!  Reference: (1) H. Fukuyama and E. Abrahams, Phys. Rev. B 27,
!                    5976-5980 (1983)
!
!
!  Input parameters:
!
!       * TAU      : ELASTIC scattering time (SI)
!       * T        : temperature (SI)
!
!  Output parameters:
!
!       * FUAB_RT_2D : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : TWO,FOUR,HALF
      USE CONSTANTS_P1,        ONLY : H_BAR,M_E,E,K_B
      USE FERMI_SI,            ONLY : EF_SI,VF_SI
      USE PI_ETC,              ONLY : PI
!
      IMPLICIT NONE
!
      REAL (WP)             ::  TAU,T
      REAL (WP)             ::  FUAB_RT_2D
      REAL (WP)             ::  KBT,HBT,D,T1,KK
!
      REAL (WP)             ::  DLOG
!
      KBT=K_B*T                                                     !
      HBT=H_BAR/TAU                                                 !
      D=HALF*VF_SI*VF_SI*TAU                                        ! diffusion coefficient
      KK=TWO*M_E*E*E                                                !
      T1=FOUR*EF_SI*EF_SI*TAU*TAU*D*KK*KK                           ! ref. 1 eq. (2.25)
!
      IF(KBT > HBT) THEN                                            !
        FUAB_RT_2D=HALF*PI*KBT*KBT*DLOG(EF_SI/KBT)/EF_SI            ! ref. 1 eq. (3.1)
      ELSE                                                          !
        FUAB_RT_2D=HALF*KBT*DLOG(T1/KBT)/(EF_SI*TAU)                ! ref. 1 eq. (3.2)
      END IF                                                        !
!
      END FUNCTION FUAB_RT_2D
!
!=======================================================================
!
      FUNCTION LUFO_RT_2D(T,EPS,I_F)
!
! This function computes Lucas-Fong 2D relaxation time in graphene
!   due to e-e interactions
!
!
!  Reference:  (1) A. Lucas and K. C. Fong, J. Phys.: Condens. Matter
!                     30,  053001 (2018)
!
!  Input parameters:
!
!       * T        : temperature  (in SI)
!       * EPS      : substrate dielectric constant
!       * I_F      : switch for choice of formula
!                       I_F  = 1 --> eq.   (4) ref. 1 graphene
!                       I_F  = 2 --> eq.  (20) ref. 1 2D Fermi liquid
!                       I_F  = 3 --> eq. (129) ref. 1 collinear scattering
!                       I_F  = 4 --> eq. (134) ref. 1 imbalance mode
!                       I_F  = 5 --> eq. (143) ref. 1 graphene in Fermi liquid limit
!
!  Output parameters:
!
!       * LUFO_RT_2D  : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE
      USE CONSTANTS_P1,        ONLY : H_BAR,E,K_B
      USE FERMI_SI,            ONLY : EF_SI,VF_SI
      USE PI_ETC,              ONLY : PI
!
      IMPLICIT NONE
!
      REAL (WP)             ::  T,EPS
      REAL (WP)             ::  LUFO_RT_2D
      REAL (WP)             ::  AL,NEE
!
      REAL (WP)             ::  MIN,DLOG
!
      INTEGER               ::  I_F
!
      AL=E*E/(EPS*H_BAR*VF_SI)                                      ! eff. fine struct. const.
!
      IF(I_F == 1) THEN                                             !
        NEE=AL*AL*K_B*T*MIN(ONE,K_B*T/EF_SI)/H_BAR                  !
        LUFO_RT_2D=ONE/NEE                                          ! ref. 1 eq. (4)
      ELSE IF(I_F == 2) THEN                                        !
        LUFO_RT_2D=H_BAR*EF_SI/(AL*AL*K_B*T*K_B*T)                  ! ref. 1 eq. (20)
      ELSE IF(I_F == 3) THEN                                        !
        NEE=AL*AL*K_B*T/(H_BAR*DLOG(ONE/AL))                        !
        LUFO_RT_2D=ONE/NEE                                          ! ref. 1 eq. (129)
      ELSE IF(I_F == 4) THEN                                        !
        NEE=AL*AL*AL*AL*K_B*T/H_BAR                                 !
        LUFO_RT_2D=ONE/NEE                                          ! ref. 1 eq. (134)
      ELSE IF(I_F == 5) THEN                                        !
        NEE=T*T/DLOG(K_B*T/EF_SI)                                   !
        LUFO_RT_2D=ONE/NEE                                          ! ref. 1 eq. (143)
      END IF                                                        !
!
      END FUNCTION LUFO_RT_2D
!
!=======================================================================
!
      FUNCTION QIVI_RT_2D(EK,T,RS)
!
!  This function computes Qian-Vignale approximation for
!    the relaxation time in the 2D case
!
!  Reference: (1) Z. Qian and G. Vignale,
!                    Phys. Rev. B 71, 075112 (2005)
!
!
!  Input parameters:
!
!       * EK       : electron energy (SI)
!       * T        : temperature  (in SI)
!       * RS       : Wigner-Seitz radius (dimensionless factor)
!
!  Output parameters:
!
!       * QIVI_RT_2D : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,HALF,FOURTH
      USE CONSTANTS_P1,        ONLY : K_B
      USE FERMI_SI,            ONLY : EF_SI
      USE PI_ETC,              ONLY : PI,PI_INV
      USE SQUARE_ROOTS,        ONLY : SQR2
      USE SCREENING_VEC,       ONLY : THOMAS_FERMI_VECTOR
      USE CHEMICAL_POTENTIAL,  ONLY : MU
!
      IMPLICIT NONE
!
      REAL (WP)             ::  EK,T,RS
      REAL (WP)             ::  QIVI_RT_2D
      REAL (WP)             ::  MM,ZK,KS,KT
      REAL (WP)             ::  NN
      REAL (WP)             ::  K0,K1,K2
!
      REAL (WP)             ::  DLOG
!
!  Computing the chemical potential
!
      MM=MU('2D',T)                                                 !
!
      ZK=EK-MM                                                      !
      KT=K_B*T                                                      !
!
!  Computing the Thomas-Fermi screening vector
!
      CALL THOMAS_FERMI_VECTOR('2D',KS)                             !
!
      K0=FOURTH*ZK*ZK*PI_INV/EF_SI                                  !
      K1=0.75E0_WP - ( RS/(SQR2 * (RS+SQR2)**2) )                   ! ref. 1 eq. (61)
!
      IF(KT < ZK) THEN                                              !
!
        K0=FOURTH*ZK*ZK*PI_INV/EF_SI                                !
        K2=TWO*EF_SI/ZK                                             !
        NN=K0*K1*DLOG(K2)                                           ! ref. 1 eq. (60)
!
      ELSE                                                          !
!
        K0=-0.125E0_WP*PI*KT*KT/EF_SI                               !
        K2=HALF*KT/EF_SI                                            !
        NN=K0*K1*DLOG(K2)                                           ! ref. 1 eq. (72)
!
      END IF                                                        !
!
      QIVI_RT_2D=ONE/NN                                             !
!
      END FUNCTION QIVI_RT_2D
!
!=======================================================================
!
      FUNCTION RASM_RT_2D(EK,T,RS,U)
!
!  This function computes Rammer-Smith approximation for
!    the e-e relaxation time in the 2D case
!
!  Reference: (1) J. Rammer and H. Smith, Rev. Mod. Phys. 58, 323 (1986)
!
!  Note: uses the e-impurity scattering time as a reference time
!
!
!  Input parameters:
!
!       * EK       : electron energy (SI)
!       * T        : temperature  (in SI)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * U        : strength of impurity scattering
!
!  Output parameters:
!
!       * RASM_RT_3D : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : TWO,EIGHT,HALF
      USE CONSTANTS_P1,        ONLY : M_E
      USE FERMI_SI,            ONLY : VF_SI
      USE PI_ETC,              ONLY : PI,PI2
      USE SCREENING_VEC,       ONLY : THOMAS_FERMI_VECTOR
      USE CHEMICAL_POTENTIAL,  ONLY : MU
      USE UTILITIES_1,         ONLY : RS_TO_N0
!
      IMPLICIT NONE
!
      REAL (WP)             ::  EK,T,RS,U
      REAL (WP)             ::  RASM_RT_2D
      REAL (WP)             ::  KS,IS,MM
      REAL (WP)             ::  N0,NUM,DEN
      REAL (WP)             ::  D,T1
      REAL (WP)             ::  EPS
!
      REAL (WP)             ::  DLOG,DABS
!
      EPS=1.0E-2_WP                                                 !
!
!  Computing the Thomas-Fermi screening vector
!
      CALL THOMAS_FERMI_VECTOR('2D',KS)                             !
!
!  Computing the chemical potential
!
      MM=MU('2D',T)                                                 !
!
!  Computing the electron density
!
      N0=RS_TO_N0('2D',RS)                                          !
!
!  Impurity scattering rate (1/tau)
!
      IS=PI*U*U*N0                                                  ! ref. 1 eq. (4.13)
!
!  2D diffusion coefficient
!
      D=HALF*VF_SI*VF_SI/IS                                         !
!
      T1=(TWO*M_E*D)**2 * D*KS*KS                                   !
!
      IF(T < EPS) THEN                                              ! zero temperature
!
        NUM=EIGHT*PI2*MM                                            !
        DEN=EK*EK*DLOG(DABS(EK/MM))                                 !
        RASM_RT_2D=NUM/DEN                                          ! ref. 1 eq. (4.81)
!
      ELSE
!
        IF(T < IS) THEN                                             !
          NUM=TWO*M_E*D                                             !
          DEN=T*DLOG(T1/T)                                          !
          RASM_RT_2D=NUM/DEN                                        ! ref. 1 eq. (4.83)
        ELSE                                                        !
          NUM=TWO*PI*MM                                             !
          DEN=T*T*DLOG(MM/T)                                        !
          RASM_RT_2D=NUM/DEN                                        ! ref. 1 eq. (4.84)
        END IF                                                      !
!
      END IF                                                        !
!
      END FUNCTION RASM_RT_2D
!
!=======================================================================
!
      FUNCTION REWI_RT_2D(EK,T,RS,B,I_ET,H_TYPE)
!
!  This function computes Reizer-Wilkins approximation for
!    the electron-electron relaxation time in heterostructures
!
!  Reference: (1) M. Reizer and J. W. Wilkins, Phys. Rev. B 55,
!                    R7363-R7366 (1997)
!
!
!  Input parameters:
!
!       * EK       : electron energy (SI)
!       * T        : temperature (SI)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * B        : interlayer distance (SI)
!       * I_ET     : switch
!                       I_ET  =  1 --> relaxation time = f(T)
!                       I_ET  =  2 --> relaxation time = f(EK)
!       * H_TYPE   : heterostructure type
!                       H_TYPE  =  'SSL1' semiconductor superlattice of type I
!                       H_TYPE  =  'SSL2' semiconductor superlattice of type II
!
!  Output parameters:
!
!       * REWI_RT_2D : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,FOUR
      USE CONSTANTS_P1,        ONLY : K_B
      USE FERMI_SI,            ONLY : EF_SI,KF_SI
      USE PI_ETC,              ONLY : PI,PI_INV
      USE SCREENING_VEC,       ONLY : DEBYE_VECTOR
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  H_TYPE
!
      INTEGER               ::  I_ET,LOGF
!
      REAL (WP)             ::  EK,T,RS,B
      REAL (WP)             ::  REWI_RT_2D
      REAL (WP)             ::  K0,K1,KD,KBT,KDB,TKDB,PB
      REAL (WP)             ::  CF,NUM,DEN,NEE
!
      REAL (WP)             ::  DLOG
!
      LOGF=6                                                        !
!
      KBT=K_B*T                                                     !
      K0=KBT/EF_SI                                                  !
      K1=EK/EF_SI                                                   !
      PB=MIN(TWO*KF_SI*B,ONE)                                       !  ref. 1 eq. (19)
!
!  Computing the Debye screening vector
!
      CALL DEBYE_VECTOR('2D',T,RS,KD)                               !
!
      KDB=KD*B                                                      !
      TKDB=KD*B+KD*B                                                !
!
      IF(H_TYPE == 'SSL1') THEN                                     !
        IF(I_ET == 1) THEN                                          !
          CF=0.125E0_WP*PI*KBT*KBT/EF_SI                            !
          NUM=TWO*(TWO*KF_SI-K0)*KD                                 !
          DEN=(TWO*KF_SI+KD)*(KD+K0)                                !
          NEE=CF*( DLOG(FOUR*EF_SI/KBT) -                        &  !
                   DLOG(TWO*KF_SI/(KD+K0)) - NUM/DEN             &  !
                 )                                                  !  ref. 1 eq. (13)
        ELSE IF(I_ET == 2) THEN                                     !
          CF=0.125E0_WP*EK*EK*PI_INV/EF_SI                          !
          NUM=TWO*(TWO*KF_SI-K1)*KD                                 !
          DEN=(TWO*KF_SI+KD)*(KD+K1)                                !
          NEE=CF*( DLOG(FOUR*EF_SI/KBT) -                        &  !  ref. 1 eq. (14)
                   DLOG(TWO*KF_SI/(KD+K1)) - NUM/DEN             &  !
                 )                                                  !
        END IF                                                      !
      ELSE IF(H_TYPE == 'SSL2') THEN                                !
        IF(I_ET == 1) THEN                                          !
          CF=PI*KBT*KBT/( 32.0E0_WP*EF_SI*(ONE+KDB)*(ONE+KDB) )     !
          NUM=FOUR*(ONE+(ONE+TKDB)**2) * KDB * (PB-K0*B)*(ONE+KDB)  !
          DEN=(PB+TKDB*(ONE+KDB))*(K0*B+TKDB*(ONE+KDB))             !
          NEE=CF*( ( TWO+(ONE+TKDB)**2 ) *                        & !
                   ( DLOG(TWO*EF_SI*PB/(KBT*KF_SI*B)) -           & !  ref. 1 eq. (19)
                     DLOG(PB+TKDB*(ONE+KDB)/(K0*B+TKDB*(ONE+KDB)))& !
                   ) - NUM/DEN                                    & !
                 )                                                  !
        ELSE IF(I_ET == 2) THEN                                     !
          WRITE(LOGF,10)                                            !
          STOP                                                      !
        END IF                                                      !
      END IF                                                        !
!
      REWI_RT_2D=ONE/NEE                                            !
!
!  Format
!
  10  FORMAT(//,5X,'<<<<<  I_ET=2 not defined for SSL2  >>>>>',//)
!
      END FUNCTION REWI_RT_2D
!
!=======================================================================
!
      FUNCTION SHAS_RT_2D(EK,TEI)
!
!  This function computes Sharma-Ashraf approximation for
!    the relaxation time in the presence of a random
!    impurity potential for a quantum well
!
!  Reference: (1) A. C. Sharma and S. S. Z. Ashraf, J. Phys.:
!                    Condens. Matter 16, 3117-3132 (2004)
!
!
!  Input parameters:
!
!       * EK       : electron energy (SI)
!       * TEI      : electron-impurity collision time
!
!  Output parameters:
!
!       * SHAS_RT_2D : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : FOUR
      USE CONSTANTS_P1,        ONLY : H_BAR
      USE FERMI_SI,            ONLY : EF_SI,KF_SI
      USE PI_ETC,              ONLY : PI
      USE SCREENING_VEC,       ONLY : THOMAS_FERMI_VECTOR
!
      IMPLICIT NONE
!
      REAL (WP)             ::  EK,TEI
      REAL (WP)             ::  SHAS_RT_2D
      REAL (WP)             ::  KS,K,X,S,K2,S2,X2,NN
      REAL (WP)             ::  NU1,NU2,DE1,DE2
!
      REAL (WP)             ::  DLOG
!
!  Computing the Thomas-Fermi screening vector
!
      CALL THOMAS_FERMI_VECTOR('2D',KS)                             !
!
!  Dimensionless variables
!
      K=KS/KF_SI                                                    !
      X=EK/EF_SI                                                    !
      S=H_BAR/(TEI*EF_SI)                                           !
      K2=K*K                                                        !
      S2=S*S                                                        !
      X2=X*X                                                        !
!
      NU1=FOUR*K2+S2                                                !
      DE1=NU1-X2                                                    !
      NU2=NU1-X2                                                    !
      DE2=S2+S2                                                     !
      NN=NU1*DLOG(NU1/DE1) + X2*DLOG(NU2/DE2) - X2                  !
!
      SHAS_RT_2D=16.0E0_WP*PI*H_BAR/(NN*EF_SI)                      ! ref. 1 eq. (17)
!
      END FUNCTION SHAS_RT_2D
!
!=======================================================================
!
      FUNCTION ZHDA_RT_2D(EK,T)
!
!  This function computes Zhang-Das Sarma approximation for
!    the relaxation time in the 2D case
!
!  References: (1) L. Zhang and S. Das Sarma, Phys. Rev. B 53,
!                     9964-9967 (1996)
!
!  Input parameters:
!
!       * EK       : electron energy in J
!       * T        : temperature  (in SI)
!
!  Output parameters:
!
!       * ZHDA_RT_2D : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : TWO,FOURTH
      USE CONSTANTS_P1,        ONLY : H_BAR,K_B
      USE FERMI_SI,            ONLY : EF_SI
      USE PI_ETC,              ONLY : PI,PI_INV
!
      IMPLICIT NONE
!
      REAL (WP)             ::  EK,T
      REAL (WP)             ::  ZHDA_RT_2D
      REAL (WP)             ::  R1,R2,R3,DE
      REAL (WP)             ::  GAMMA
!
      REAL (WP)             ::  DLOG
!
      DE=EK-EF_SI                                                   ! energy with respect to E_F
      R1=K_B*T/EF_SI                                                !
      R2=DE/EF_SI                                                   !
      R3=K_B*T/DE                                                   !
!
      IF(R3 > DE) THEN                                              !
        GAMMA=-FOURTH*PI*EF_SI*R1*R1*DLOG(R1)/H_BAR                 ! ref. (1) eq. (7)
      ELSE                                                          !
        GAMMA=-FOURTH*PI_INV*EF_SI*R2*R2*DLOG(R2)/H_BAR             ! ref. (1) eq. (8)
      END IF                                                        !
!
      ZHDA_RT_2D=TWO/GAMMA                                          !
!
      END FUNCTION ZHDA_RT_2D
!
!=======================================================================
!
      SUBROUTINE EE_RT_1D(EE_TYPE,EK,TEI,TAU)
!
!  This subroutine computes the electron-electron relaxation time
!    in 1D systems
!
!
!  Input parameters:
!
!       * EE_TYPE  : relaxation time functional for electron-phonon
!                       EE_TYPE = 'SHAS'   --> Sharma-Ashraf approx.
!
!       * EK       : electron energy (SI)
!       * TEI      : electron-impurity collision time
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  EE_TYPE
!
      REAL (WP)             ::  EK,TEI
      REAL (WP)             ::  TAU
!
      IF(EE_TYPE == 'SHAS') THEN                                    !
        TAU=SHAS_RT_1D(EK,TEI)                                      !
      END IF                                                        !
!
      END SUBROUTINE EE_RT_1D
!
!=======================================================================
!
      FUNCTION SHAS_RT_1D(EK,TEI)
!
!  This function computes Sharma-Ashraf approximation for
!    the relaxation time in the presence of a random
!    impurity potential for a quantum wire
!
!  Reference: (1) A. C. Sharma and S. S. Z. Ashraf, J. Phys.:
!                    Condens. Matter 16, 3117-3132 (2004)
!
!
!  Input parameters:
!
!       * EK       : electron energy (SI)
!       * TEI      : electron-impurity collision time
!
!  Output parameters:
!
!       * QIVI_RT_1D : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : TWO
      USE CONSTANTS_P1,        ONLY : H_BAR
      USE FERMI_SI,            ONLY : EF_SI
      USE PI_ETC,              ONLY : PI,PI_INV
!
      IMPLICIT NONE
!
      REAL (WP)             ::  EK,TEI
      REAL (WP)             ::  SHAS_RT_1D
      REAL (WP)             ::  X,S,S2,X2,NN
      REAL (WP)             ::  NU1,NU2,DE1,DE2
!
      REAL (WP)             ::  DSQRT,DLOG
!
!  Dimensionless variables
!
      X=EK/EF_SI                                                    !
      S=H_BAR/(TEI*EF_SI)                                           !
      S2=S*S                                                        !
      X2=X*X                                                        !
!
      NU1=S2                                                        !
      DE1=TWO*PI*DSQRT(S2+X2)                                       !
      NU2=DSQRT(S2+X2) - X                                          !
      DE2=DSQRT(S2+X2)+ X                                           !
      NN=X*PI_INV + NU1*DLOG(NU2/DE2)/DE1                           !
!
      SHAS_RT_1D=H_BAR/(NN*EF_SI)                                   ! ref. 1 eq. (33)
!
      END FUNCTION SHAS_RT_1D
!
!------ 3) electron-impurity case --------------------------------------------
!
!
!=======================================================================
!
      SUBROUTINE EI_RT_3D(EI_TYPE,EK,T,RS,NI,EPS_B,MASS_E,TAU)
!
!  This subroutine computes the electron-impurity relaxation time
!    in 3D systems
!
!
!  Input parameters:
!
!       * EI_TYPE  : relaxation time functional for electron-impurity
!                       EI_TYPE = 'HEAP'   --> Hertel-Appel approximation
!       * EK       : electron energy
!       * T        : temperature  (in SI)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * NI       : impurity concentration (SI)
!       * EPS_B    : background dielectric constant
!       * MASS_E   : electron effective mass
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  EI_TYPE
!
      REAL (WP)             ::  EK,T,RS,NI
      REAL (WP)             ::  EPS_B,MASS_E
      REAL (WP)             ::  TAU
!
      IF(EI_TYPE == 'HEAP') THEN                                    !
        TAU=HEAP_RT_3D(EK,T,RS,NI,EPS_B,MASS_E)                     !
      END IF                                                        !
!
      END SUBROUTINE EI_RT_3D
!
!=======================================================================
!
      FUNCTION HEAP_RT_3D(EK,T,RS,NI,EPS_B,MASS_E)
!
!  This function computes Hertel and Appel approximation for
!    the electron-impurity relaxation time in the 3D case
!
!  References: (1) P. Hertel and J. Appel, Phys. Rev. B 26, 5730-5742 (1982)
!
!
!  Input parameters:
!
!       * EK       : electron energy
!       * T        : temperature  (in SI)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * NI       : impurity concentration (SI)
!       * EPS_B    : background dielectric constant
!       * MASS_E   : electron effective mass
!
!  Output parameters:
!
!       * HEAP_RT_3D  : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO
      USE CONSTANTS_P1,        ONLY : E
      USE PI_ETC,              ONLY : PI
      USE SQUARE_ROOTS,        ONLY : SQR2
      USE UTILITIES_1,         ONLY : RS_TO_N0
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP)             ::  EK,T,RS,NI
      REAL (WP)             ::  EPS_B,MASS_E
      REAL (WP)             ::  HEAP_RT_3D
      REAL (WP)             ::  B,NU,E2,E4
      REAL (WP)             ::  EPS_INF
      REAL (WP)             ::  N0
!
      REAL (WP)             ::  DSQRT,DLOG
!
      E2=E*E                                                        !
      E4=E2*E2                                                      !
!
!  Computing the electron density
!
      N0=RS_TO_N0('3D',RS)                                          !
!
      B=TWO*EPS_B*MASS_E*T/(PI*E2*N0)                               ! ref. (1) eq. (41)
      NU=PI*NI*E4*(DLOG(ONE+B*EK) - B*EK/(ONE+B*EK))/            &  !
           (SQR2*EPS_INF*EPS_INF*DSQRT(MASS_E)*(EK**(1.5E0_WP)))    !
!
      HEAP_RT_3D=ONE/NU                                             !
!
      END FUNCTION HEAP_RT_3D
!
!------ 4) ion-plasma case --------------------------------------------
!
!
!=======================================================================
!
      SUBROUTINE IP_RT_3D(IP_TYPE,RS,T,TAU)
!
!  This subroutine computes the ion plasma relaxation time
!    in 3D systems
!
!
!  Input parameters:
!
!       * IP_TYPE  : relaxation time functional for ion plasma
!                       IP_TYPE = 'SEMO'   --> Selchow-Morawetz
!                       IP_TYPE = 'SPIT'   --> Spitzer
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature  (in SI)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  IP_TYPE
!
      REAL (WP)             ::  RS,T
      REAL (WP)             ::  TAU
!
      IF(IP_TYPE == 'SEMO') THEN                                    !
        TAU=SEMO_RT_3D(RS,T)                                        !
      ELSE IF(IP_TYPE == 'BLAN') THEN                               !
        TAU=SPIT_RT_3D(RS,T)                                        !
      END IF                                                        !
!
      END SUBROUTINE IP_RT_3D
!
!=======================================================================
!
      FUNCTION SEMO_RT_3D(RS,T)
!
!  This function computes Selchow-Morawetz approximation for
!    the relaxation time in the 3D case
!
!  References: (1) A. Selchow and K. Morawetz, Phys. Rev. E 59, 1015-1023 (1999)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature  (in SI)
!
!  Output parameters:
!
!       * SEMO_RT  : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,TWO,THIRD
      USE CONSTANTS_P1,        ONLY : M_E,E,EPS_0,K_B
      USE PI_ETC,              ONLY : PI
      USE UTILITIES_1,         ONLY : RS_TO_N0
      USE COULOMB_LOG,         ONLY : COU_LOG
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP)             ::  T
      REAL (WP)             ::  SEMO_RT_3D
      REAL (WP)             ::  E2,G,BETA,V_TH
      REAL (WP)             ::  RS,N0
!
      REAL (WP)             ::  DSQRT,DEXP
!
      INTEGER               ::  ICL
!
      ICL=1                                                         ! choice of Coulomb
!                                                                   !   logarithm
      BETA=ONE/(K_B*T)                                              !
      V_TH=DSQRT(TWO/(BETA*M_E))                                    ! thermal velocity
      E2=E*E                                                        !
!
      N0=RS_TO_N0('3D',RS)                                          !
!
      G=DEXP(COU_LOG(ICL,'3D',T,RS))                                !
!
      SEMO_RT_3D=ONE/(N0*TWO*THIRD*PI*E2*G*BETA*V_TH/EPS_0)         ! ref. (1) eq. (7)
!
      END FUNCTION SEMO_RT_3D
!
!=======================================================================
!
      FUNCTION SPIT_RT_3D(RS,T)
!
!  This function computes Spitzer approximation for
!    the relaxation time in the 3D case
!
!  References: (1) A. Selchow, G. Röpke and A. Wierling,
!                     Contrib. Plasma Phys. 42, 43-54 (2002)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature  (in SI)
!
!  Output parameters:
!
!       * SPIT_RT  : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ONE,HALF
      USE CONSTANTS_P1,        ONLY : M_E,E,COULOMB,K_B
      USE UTILITIES_1,         ONLY : RS_TO_N0
      USE PLASMA,              ONLY : ZION
      USE PLASMA_SCALE
!
      IMPLICIT NONE
!
      REAL (WP)             ::  T
      REAL (WP)             ::  SPIT_RT_3D
      REAL (WP)             ::  BETA,M_EI,E2,E4
      REAL (WP)             ::  NONID,DEGEN
      REAL (WP)             ::  RS,N0
!
      REAL (WP)             ::  DSQRT,DLOG
!
      M_EI=M_E                                                      !
      BETA=ONE/(K_B*T)                                              !
      E2=E*E                                                        !
      E4=E2*E2                                                      !
!
      N0=RS_TO_N0('3D',RS)                                          !
!
!  Computing the plasmon scales
!
      CALL PLASMON_SCALE(RS,T,ZION,NONID,DEGEN)                     !
      SPIT_RT_3D=0.591E0_WP*DSQRT(M_EI)/(COULOMB*COULOMB*N0*      & ! ref. (1) eq. (30)
                 (BETA**1.5E0_WP)*E4*HALF*DLOG(1.5E0_WP/(NONID**3)))!
!
      END FUNCTION SPIT_RT_3D
!
!------ 5) phase-breaking case --------------------------------------------
!
!
!=======================================================================
!
      SUBROUTINE PB_RT_3D(PB_TYPE,T,D,DC,A,TAU)
!
!  This subroutine computes the phase-breaking relaxation time
!    in 3D systems
!
!
!  Input parameters:
!
!       * PB_TYPE  : relaxation time functional for phase_breaking
!                       PB_TYPE = 'ALAR'   --> Al'tshuler-Aronov
!                       PB_TYPE = 'BLAN'   --> Blanter
!
!       * T        : temperature (SI)
!       * D        : diffusion coefficient (SI)
!       * DC       : diffusion constant (due to imputities)
!       * A        : width of 2D systems (SI)                  (unused)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  PB_TYPE
!
      REAL (WP)             ::  T,D,DC,A
      REAL (WP)             ::  TAU
!
      IF(PB_TYPE.EQ.'ALAR') THEN                                    !
        TAU=ALAR_PB_ND(T,DC,A,'3D')                                 !
      ELSE IF(PB_TYPE.EQ.'BLAN') THEN                               !
        TAU=BLAN_PB_3D(T,D)                                         !
      END IF                                                        !
!
      END SUBROUTINE PB_RT_3D
!
!=======================================================================
!
      FUNCTION ALAR_PB_ND(T,DC,A,DMN)
!
!  This function computes Al'tshuler-Aronov approximation for
!    the phase relaxation time in the presence of impurities
!
!  Reference: (1) B. L. Al'tshuler and A. G. Aronov, in
!                    "Electron-Electron Interactions in Disordered
!                     Solids", A. L. Efros and M. Pollak eds.
!                    (North-Holland,1985)
!
!
!  Input parameters:
!
!       * T        : temperature (SI)
!       * DC       : diffusion constant (due to imputities)
!       * A        : width of 2D systems (SI)
!       * DMN      : system dimension
!
!  Output parameters:
!
!       * ARAL_PB_ND : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : ZERO,ONE,TWO,FOUR,HALF
      USE CONSTANTS_P1,        ONLY : H_BAR,M_E,E,COULOMB
      USE FERMI_SI,            ONLY : KF_SI,VF_SI
      USE PI_ETC,              ONLY : PI
      USE UTILITIES_1,         ONLY : D,DOS_EF,KF_TO_N0
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::   DMN
!
      REAL (WP)             ::  T,DC,A
      REAL (WP)             ::  ALAR_PB_ND
      REAL (WP)             ::  N0,DD,L,EX,TS,SG
      REAL (WP)             ::  N,TAU
      REAL (WP)             ::  K1,NUM,DEN
!
      REAL (WP)             ::  DLOG,DSQRT
!
      DD=HALF*D(DMN)                                                !
!
! Computing the density of state at Fermi level
!
      N0=DOS_EF(DMN)                                                !
!
      EX=TWO/(FOUR-D(DMN))                                          !
!
!  Computing the elastic mean free path from:
!
!         D = l^2 /(tau*d) and D = v_F^2 * tau / d
!
      L=DC*D(DMN)/VF_SI                                             !
!
!  Computing the electron density
!
      N=KF_TO_N0(DMN,KF_SI)                                         !
!
      TAU=L/VF_SI                                                   !
!
!  Computing the Drude conductivity:
!
!         sigma  = e^2 tau * N / m
!
      SG=E*E*TAU*N/M_E                                              !
!
      IF(DMN == '1D') THEN                                          !
        TS=ZERO                                                     !
        K1=ONE                                                      !
        NUM=T**EX                                                   !
        DEN=(DC**DD * N0 * H_BAR*H_BAR)**EX                         !
      ELSE IF(DMN == '2D') THEN                                     !
        TS=ZERO                                                     !
        K1=DLOG( KF_SI*KF_SI*L*A/(H_BAR*H_BAR) )                    !
        NUM=T**EX                                                   !
        DEN=(DC**DD * N0 * H_BAR*H_BAR)**EX                         !
      ELSE IF(DMN == '3D') THEN                                     !
        TS=(KF_SI*L)**2 / (DSQRT(L/VF_SI) * T**1.5E0_WP)            ! tau*
        K1=-TS*DLOG(T*TS/H_BAR)                                     !
        NUM=E*E*COULOMB*T*DSQRT(TS)                                 !
        DEN=TWO*PI*SG*DSQRT(DC)*H_BAR*H_BAR                         !
      END IF                                                        !
!
      ALAR_PB_ND=TS+K1*NUM/DEN                                      ! ref. 1 eq. (4.9)
!
      END FUNCTION ALAR_PB_ND
!
!=======================================================================
!
      FUNCTION BLAN_PB_3D(T,D)
!
!  This function computes Blanter approximation for phase-breaking
!    relaxation time in 3D.
!
!  Reference: (1) Ya. M. Blanter, Phys. Rev. B 54, 12807 (1996)
!
!
!  Input parameters:
!
!       * T        : temperature (SI)
!       * D        : diffusion coefficient (SI)
!
!  Output parameters:
!
!       * BLAN_PB_3D : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE CONSTANTS_P1,        ONLY : H_BAR,M_E
      USE FERMI_SI,            ONLY : KF_SI
      USE PI_ETC,              ONLY : PI2
!
      IMPLICIT NONE
!
      REAL (WP)             ::  T,D
      REAL (WP)             ::  BLAN_PB_3D
      REAL (WP)             ::  N3
!
      N3=M_E*KF_SI/(PI2*H_BAR*H_BAR)                                !
!
      BLAN_PB_3D=(D/T)**1.5E0_WP * N3                               ! ref. 1 eq. (1)
!
      RETURN
!
      END FUNCTION BLAN_PB_3D
!
!=======================================================================
!
      SUBROUTINE PB_RT_2D(PB_TYPE,T,D,DC,A,DMN,TAU)
!
!  This subroutine computes the phase-breaking relaxation time
!    in 2D systems
!
!
!  Input parameters:
!
!       * PB_TYPE  : relaxation time functional for phase-breaking
!                       PB_TYPE = 'ALAR'   --> Al'tshuler-Aronov
!                       PB_TYPE = 'BLAN'   --> Blanter
!
!       * T        : temperature (SI)
!       * D        : diffusion coefficient (SI)
!       * DC       : diffusion constant (due to imputities)
!       * A        : width of 2D systems (SI)
!       * DMN      : dimension of the system
!                       DMN  = '3D'
!                       DMN  = '2D'
!                       DMN  = 'Q2'  quasi-2D
!                       DMN  = '1D'
!                       DMN  = 'Q1'  quasi-1D
!                       DMN  = 'Q0'  quasi-0D
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 22 May 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
      CHARACTER (LEN = 4)   ::  PB_TYPE
!
      REAL (WP)             ::  T,D,DC,A
      REAL (WP)             ::  TAU
!
      IF(PB_TYPE == 'ALAR') THEN                                    !
        TAU=ALAR_PB_ND(T,DC,A,'2D')                                 !
      ELSE IF(PB_TYPE == 'BLAN') THEN                               !
        TAU=BLAN_PB_2D(T,D,DMN,A)                                       !
      END IF                                                        !
!
      END SUBROUTINE PB_RT_2D
!
!=======================================================================
!
      FUNCTION BLAN_PB_2D(T,D,DMN,A)
!
!  This function computes Blanter approximation for phase-breaking
!    relaxation time in 2D.
!
!  Reference: (1) Ya. M. Blanter, Phys. Rev. B 54, 12807 (1996)
!
!
!  Input parameters:
!
!       * T        : temperature (SI)
!       * D        : diffusion coefficient (SI)
!       * DMN      : dimension of the system
!       * A        : length of confined direction (SI)
!
!  Output parameters:
!
!       * BLAN_PB_2D : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : TWO
      USE FERMI_SI,            ONLY : EF_SI,KF_SI,VF_SI
      USE PI_ETC,              ONLY : PI
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::   DMN
!
      REAL (WP)             ::  T,D,A
      REAL (WP)             ::  BLAN_PB_2D
      REAL (WP)             ::  N2,TAU,L
!
      REAL (WP)             ::  DLOG,DSQRT
!
      N2=KF_SI*KF_SI/(TWO*PI*EF_SI)                                 !
      TAU=TWO*D / (VF_SI*VF_SI)                                     ! elastic scattering time
      L=DSQRT(TWO*D*TAU)                                            ! mean free path
!
      IF(DMN == '2D') THEN                                          !
        BLAN_PB_2D=N2*D/T / DLOG(KF_SI*L)                           ! ref. 1 eq. (10)
      ELSE IF(DMN == 'Q2') THEN                                     !
        BLAN_PB_2D=N2*D/T * KF_SI*A  / DLOG(KF_SI*KF_SI*L*A)        ! ref. 1 eq. (10)
      END IF                                                        !
!
      END FUNCTION BLAN_PB_2D
!
!=======================================================================
!
      SUBROUTINE PB_RT_1D(PB_TYPE,T,D,DC,A,TAU)
!
!  This subroutine computes the phase-breaking relaxation time
!    in 1D systems
!
!
!  Input parameters:
!
!       * PB_TYPE  : relaxation time functional for phase breaking
!                       PB_TYPE = 'ALAR'   --> Al'tshuler-Aronov
!                       PB_TYPE = 'BLAN'   --> Blanter
!
!       * T        : temperature (SI)
!       * D        : diffusion coefficient (SI)
!       * DC       : diffusion constant (due to imputities)
!       * A        : length of confined direction (SI)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::   PB_TYPE
!
      REAL (WP)             ::  T,D,DC,A
      REAL (WP)             ::  TAU
!
      IF(PB_TYPE == 'ALAR') THEN                                    !
        TAU=ALAR_PB_ND(T,DC,A,'1D')                                 !
      ELSE IF(PB_TYPE == 'BLAN') THEN                               !
        TAU=BLAN_PB_1D(T,D,A)                                       !
      END IF                                                        !
!
      END SUBROUTINE PB_RT_1D
!
!=======================================================================
!
      FUNCTION BLAN_PB_1D(T,D,A)
!
!  This function computes Blanter approximation for phase-breaking
!    relaxation time in 1D.
!
!  Reference: (1) Ya. M. Blanter, Phys. Rev. B 54, 12807 (1996)
!
!
!  Input parameters:
!
!       * T        : temperature (SI)
!       * D        : diffusion coefficient (SI)
!       * A        : length of confined direction (SI)
!
!  Output parameters:
!
!       * BLAN_PB_1D : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : TWO,FOUR,THIRD
      USE FERMI_SI,            ONLY : KF_SI,VF_SI
!
      IMPLICIT NONE
!
      REAL (WP)             ::  T,D,A
      REAL (WP)             ::  BLAN_PB_1D
      REAL (WP)             ::  TAU
!
      TAU=TWO*D / (VF_SI*VF_SI)                                     ! elastic scattering time
!
      BLAN_PB_1D=TAU**THIRD * (KF_SI*A)**(FOUR*THIRD) /          &  !
                              T**(TWO*THIRD)                        ! ref. 1 eq. (13)
!
      END FUNCTION BLAN_PB_1D
!
END MODULE RELAXATION_TIME_STATIC
