!
!=======================================================================
!
MODULE DECAY_RATE 
!
      USE ACCURACY_REAL
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE DECAY_RATE_COEF(X,DR)
!
!  This subroutine computes the decay rate
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!
!
!  Output parameters:
!
!       * DR       : decay rate
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Nov 2020
!
!
      USE MATERIAL_PROP,          ONLY : DMN,RS
      USE EXT_FIELDS,             ONLY : T
      USE LF_VALUES,              ONLY : GQ_TYPE,IQ_TYPE
      USE SF_VALUES,              ONLY : SQ_TYPE
!
      USE DAMPING_VALUES,         ONLY : DR_TYPE
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)  ::  X
      REAL (WP), INTENT(OUT) ::  DR
!
      IF(DMN == '3D') THEN                                          !
          CALL DECAY_RATE_3D(X,T,RS,DR_TYPE,SQ_TYPE,GQ_TYPE,      & !
                            IQ_TYPE,DR)                             !
      ELSE IF(DMN == '2D') THEN                                     !
        CONTINUE                                                    ! not implemented yet
      ELSE IF(DMN == '1D') THEN                                     !
        CONTINUE                                                    ! not implemented yet
      END IF                                                        !
!
      END SUBROUTINE DECAY_RATE_COEF
!
!=======================================================================
!
      SUBROUTINE DECAY_RATE_3D(X,T,RS,DR_TYPE,SQ_TYPE,GQ_TYPE,   &
                               IQ_TYPE,DR) 
!
!  This subroutine computes the plasmon decay rate
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!       * T        : temperature  (in SI)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * DR_TYPE  : type of decay rate
!                       DR_TYPE = 'UTIC'   --> Utsumi-Ichimaru approximation
!                       DR_TYPE = 'VLAS'   --> Vlasov approximation
!       * SQ_TYPE  : structure factor approximation (3D)
!       * GQ_TYPE  : local-field correction type (3D)
!       * IQ_TYPE  : type of approximation for I(q)
!
!  Output parameters:
!
!       * DR       : decay rate
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  DR_TYPE,GQ_TYPE
      CHARACTER (LEN = 3)   ::  SQ_TYPE,IQ_TYPE
!
      REAL (WP)             ::  X,T,RS
      REAL (WP)             ::  DR
!
      IF(DR_TYPE == 'UTIC') THEN                                    !
        DR=UTIC_DR_3D(X,RS,T,SQ_TYPE,GQ_TYPE,IQ_TYPE)               !
      ELSE IF(DR_TYPE == 'VLAS') THEN                               !
        DR=VLAS_DR_3D(X,T,RS)                                       !
      END IF                                                        !
!
      END SUBROUTINE DECAY_RATE_3D
!
!=======================================================================
!
      FUNCTION UTIC_DR_3D(X,RS,T,SQ_TYPE,GQ_TYPE,IQ_TYPE)
!
!  This function computes Utsumi-Ichimaru approximation for 
!    the decay rate in the 3D case
!
!  Reference: (1) K. Utsumi and S. Ichimaru, 
!                    Phys. Rev. B 22, 1522-1533 (1980)
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature  (in SI)
!       * SQ_TYPE  : structure factor approximation (3D)
!       * GQ_TYPE  : local-field correction type (3D)
!       * IQ_TYPE  : type of approximation for I(q)
!
!  Output parameters:
!
!       * UTIC_DR  : decay rate
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  3 Dec 2020
!
!
      USE REAL_NUMBERS,             ONLY : HALF
      USE CONSTANTS_P1,             ONLY : H_BAR 
      USE UTIC_PARAMETERS,          ONLY : UTIC_PARAM
      USE RELAXATION_TIME_STATIC,   ONLY : UTIC_RT_3D
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  GQ_TYPE
      CHARACTER (LEN = 3)   ::  SQ_TYPE,IQ_TYPE
!
      REAL (WP), INTENT(IN) ::  X,RS,T
      REAL (WP)             ::  UTIC_DR_3D
!
      REAL (WP)             ::  OMP
      REAL (WP)             ::  TAU_Q,OMQ,OM0
!
      REAL (WP)             ::  EXP
!
      OMP = ENE_P_SI / H_BAR                                        ! omega_p in SI
!
!  Computing the Utsumi-Ichimaru parameters OMEGA(q) and OMEGA(0)
!
      CALL UTIC_PARAM(X,RS,T,OMQ,OM0)                               !
!
!  Computing the relaxation time TAU_Q
!
      TAU_Q = UTIC_RT_3D(X,RS,T,SQ_TYPE,GQ_TYPE)                    !
!
      UTIC_DR_3D = - HALF * EXP(- HALF * (OMP / OM0)**2) / TAU_Q    ! ref. 1 eq. (5.13)
!
      END FUNCTION UTIC_DR_3D  
!
!=======================================================================
!
      FUNCTION VLAS_DR_3D(X,RS,T)
!
!  This function computes Vlasov approximation for 
!    the decay rate in the 3D case
!
!  Reference: (1) S. Ichimaru, "Statistical Plasma Physics - Vol1", 
!                               CRC Press (2004)
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature  (in SI)
!
!  Output parameters:
!
!       * VLAS_DR  : relaxation time in seconds
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Jun 2020
!
!
      USE REAL_NUMBERS,        ONLY : TWO,THREE,HALF
      USE CONSTANTS_P1,        ONLY : H_BAR,M_E,K_B
      USE FERMI_SI,            ONLY : KF_SI
      USE PI_ETC,              ONLY : PI
      USE SCREENING_VEC,       ONLY : DEBYE_VECTOR
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X,RS,T
      REAL (WP)             ::  VLAS_DR_3D
      REAL (WP)             ::  Q_SI,KD_SI,OQ2,AA
!
      REAL (WP)             ::  DSQRT,DEXP
!
      Q_SI=TWO*X*KF_SI                                              ! q
!
!  Computing the Debye screening vector
!
      CALL DEBYE_VECTOR('3D',T,RS,KD_SI)                            !
!
!  Computing the square of the Vlasov plasmon dispersion
!
      OQ2=ENE_P_SI*ENE_P_SI + THREE*K_B*T*Q_SI*Q_SI/M_E             ! ref. 1 eq. (4.30)
!
      AA=HALF*M_E*OQ2/(Q_SI*Q_SI*K_B*T)                             !
!
      VLAS_DR_3D=DSQRT(0.125E0_WP*PI)*ENE_P_SI*(KD_SI/Q_SI)**3 *  & ! ref. 1 eq. (4.31)
                 DEXP(-AA) / H_BAR                                  !                  
!
      END FUNCTION VLAS_DR_3D  
!
END MODULE DECAY_RATE
