!
!=======================================================================
!
MODULE PACKING_FRACTION 
!
      USE ACCURACY_REAL
!
!
CONTAINS
!
!
!=======================================================================
!
      FUNCTION PACK_FRAC_3D(N0,DIA,PF_TYPE) 
!
!  This function computes the 3D packing fraction of a hard-sphere fluid
!
!  References: (1) J.-M. Bomont and J.-L. Bretonnet, 
!                     Chem. Phys. 439, 85-94 (2014)
!                        
!
!  Input parameters:
!
!       * N0       : number density                 ! in same 
!       * DIA      : diameter of particles          !  units
!       * PF_TYPE  : type of packing fraction
!                       PF_TYPE = 'HSM'   --> hard sphere model
!                       PF_TYPE = 'RCP'   --> random closed-packed
!                       PF_TYPE = 'FCC'   --> FCC closed-packed
!                       PF_TYPE = 'FRE'   --> freezing
!                       PF_TYPE = 'MEL'   --> melting
! 
!
!  Output parameters:
!
!       * PACK_FRAC_3D
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Dec 2020
!
!
      USE REAL_NUMBERS,     ONLY : TWO,SIXTH
      USE PI_ETC,           ONLY : PI
      USE SQUARE_ROOTS,     ONLY : SQR2
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  PF_TYPE
!
      REAL (WP), INTENT(IN) ::  N0,DIA
      REAL (WP)             ::  PACK_FRAC_3D
!
      IF(PF_TYPE == 'HSM') THEN                                     !
        PACK_FRAC_3D = PI * N0 * DIA * DIA * DIA * SIXTH            !
      ELSE IF(PF_TYPE == 'RCP') THEN                                !
        PACK_FRAC_3D = 0.64E0_WP                                    !
      ELSE IF(PF_TYPE == 'FCC') THEN                                !
        PACK_FRAC_3D = PI * SQR2 * SIXTH                            !
      ELSE IF(PF_TYPE == 'FRE') THEN                                !
        PACK_FRAC_3D = 0.494E0_WP                                   !
      ELSE IF(PF_TYPE == 'MEL') THEN                                !
        PACK_FRAC_3D = 0.545E0_WP                                   !
      END IF                                                        !
!
      END FUNCTION PACK_FRAC_3D
!
!=======================================================================
!
      FUNCTION PACK_FRAC_2D(N0,DIA,PF_TYPE) 
!
!  This function computes the 2D packing fraction of a hard-sphere fluid
!
!  References: (1) R. Garcia-Rojo, S. Luding and J. J. Brey, 
!                     Phys. Rev. E 74, 061395 (2006)
!                        
!
!  Input parameters:
!
!       * N0       : number density                 ! in same 
!       * DIA      : diameter of disks              !  units
!       * PF_TYPE  : type of packing fraction
!                       PF_TYPE = 'HDM'   --> hard disk model
! 
!
!  Output parameters:
!
!       * PACK_FRAC_2D
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 10 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : FOURTH
      USE PI_ETC,           ONLY : PI
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  PF_TYPE
!
!
      REAL (WP), INTENT(IN) ::  N0,DIA
      REAL (WP)             ::  PACK_FRAC_2D
!
      IF(PF_TYPE == 'HDM') THEN                                     !
        PACK_FRAC_2D = PI * N0 * DIA * DIA * FOURTH                 !
      END IF                                                        !
!
      END FUNCTION PACK_FRAC_2D
!
END MODULE PACKING_FRACTION
