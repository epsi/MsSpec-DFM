!
!=======================================================================
!
MODULE PAIR_DISTRIBUTION 
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE PAIR_DISTRIBUTION_3D(R,RS,T,RH_TYPE,R2)
!
!  This subroutine computes the pair distribution function rho2(r)
!    for 3D systems.
!
!
!  Input parameters:
!
!       * R        : grid point          (in units of a_0)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * RH_TYPE  : pair distribution function approximation (3D)
!                       RH_TYPE  = 'CDI' chain diagram improved
!                       RH_TYPE  = 'CEG' classical electron gas
!                       RH_TYPE  = 'DEB' Debye electron gas 
!                       RH_TYPE  = 'FUA' correct to order 2 in epsilon
!                       RH_TYPE  = 'SDC' short-distance correlations
!                       RH_TYPE  = 'WDA' watermelon diagrams summed
!
!  Output parameters:
!
!       * R2       : value of the pair correlation function
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  4 Jun 2020
!
!
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  RH_TYPE
!
      REAL (WP)             ::  R,RS,T,R2
!
      IF(RH_TYPE == 'CDI') THEN                                     !
        R2=CDI_PDF(R,RS,T)                                          !
      ELSE IF(RH_TYPE == 'CEG') THEN                                !
        R2=CEG_PDF(R,RS,T)                                          !
      ELSE IF(RH_TYPE == 'DEB') THEN                                !
        R2=DEB_PDF(R,RS,T)                                          !
      ELSE IF(RH_TYPE == 'FUA') THEN                                !
        R2=FUA_PDF(R,RS,T)                                          !
      ELSE IF(RH_TYPE == 'SDC') THEN                                !
        R2=SDC_PDF(R,RS,T)                                          !
      ELSE IF(RH_TYPE == 'WDA') THEN                                !
        R2=WDA_PDF(R,RS,T)                                          !
      ENDIF                                                         !
!
      END SUBROUTINE PAIR_DISTRIBUTION_3D  
!
!=======================================================================
!
      FUNCTION CDI_PDF(R,RS,T)
!
!  This function computes the electron gas pair distribution function 
!    rho2(r) for 3D systems, with a chain diagram improved
!
!  References: (1) A. Isihara, "Electron Liquids", 2nd edition,
!                     Springer Series in Solid-State Sciences 96,
!                     (Springer, 1998) p. 33
!
!  Input parameters:
!
!       * R        : grid point                            in unit of a_0
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  2 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO
      USE CONSTANTS_P1,     ONLY : BOHR,E,K_B
      USE SQUARE_ROOTS,     ONLY : SQR2
      USE SCREENING_VEC,    ONLY : DEBYE_VECTOR
      USE UTILITIES_1,      ONLY : RS_TO_N0
      USE EXT_FUNCTIONS,    ONLY : ERFC                             ! Error function
!
      IMPLICIT NONE
!
      REAL (WP)             ::  R,RS,T
      REAL (WP)             ::  CDI_PDF
      REAL (WP)             ::  X,EPS,ALP,BETA
      REAL (WP)             ::  N0,KD_SI
!
      REAL (WP)             ::  DSQRT,DEXP
!
      N0=RS_TO_N0('3D',RS)                                          !
!
!  Computing the Debye screening vector
!
      CALL DEBYE_VECTOR('3D',T,RS,KD_SI)                            !
!
      X=KD_SI*R*BOHR                                                !
      EPS=E*E*KD_SI/(K_B*T)                                         ! ref. (1) eq. (1.1.2)
      BETA=ONE/(K_B*T)                                              !
      ALP=KD_SI*DSQRT(BETA)                                         !
!
      CDI_PDF=N0*N0*( ONE -EPS*(                                &   !
                                 DEXP(-X)/X -                   &   !
                                 DEXP(-X/(TWO*ALP))/X +         &   ! ref. (1) eq. (3.3.14)
                                 SQR2*ERFC(X/(SQR2*ALP))/ALP    &   !
                               )                                &   !
                    )                                               !
!
      END FUNCTION CDI_PDF  
!
!=======================================================================
!
      FUNCTION CEG_PDF(R,RS,T)
!
!  This function computes very dilute classical electron gas 
!    pair distribution function rho2(r) for 3D systems
!
!  References: (1) A. Isihara, "Electron Liquids", 2nd edition,
!                     Springer Series in Solid-State Sciences 96,
!                     (Springer, 1998) p. 33
!
!  Input parameters:
!
!       * R        : grid point                            in unit of a_0
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 30 Jul 2020
!
!
      USE CONSTANTS_P1,     ONLY : BOHR,E,K_B
      USE UTILITIES_1,      ONLY : RS_TO_N0
!
      IMPLICIT NONE
!
      REAL (WP)             ::  R,RS,T
      REAL (WP)             ::  CEG_PDF
      REAL (WP)             ::  N0
!
      REAL (WP)             ::  DEXP
!
      N0=RS_TO_N0('3D',RS)                                          !
!
      CEG_PDF=N0*N0*DEXP(-E*E/(R*BOHR*K_B*T))                       ! ref. (1) eq. (3.1.1)
!
      END FUNCTION CEG_PDF  
!
!=======================================================================
!
      FUNCTION DEB_PDF(R,RS,T)
!
!  This function computes Debye electron gas 
!    pair distribution function rho2(r) for 3D systems
!
!  References: (1) A. Isihara, "Electron Liquids", 2nd edition,
!                     Springer Series in Solid-State Sciences 96,
!                     (Springer, 1998) p. 33
!
!  Input parameters:
!
!       * R        : grid point                            in unit of a_0
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  4 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : FOUR
      USE CONSTANTS_P1,     ONLY : BOHR
      USE PI_ETC,           ONLY : PI
      USE UTILITIES_1,      ONLY : RS_TO_N0
      USE SCREENING_VEC,    ONLY : DEBYE_VECTOR
!
      IMPLICIT NONE
!
      REAL (WP)             ::  R,RS,T
      REAL (WP)             ::  DEB_PDF
      REAL (WP)             ::  RA
      REAL (WP)             ::  N0,KD_SI
!
      REAL (WP)             ::  DEXP
!
      N0=RS_TO_N0('3D',RS)                                          !
!
!  Computing the Debye screening vector
!
      CALL DEBYE_VECTOR('3D',T,RS,KD_SI)                            !
!
      RA=R*BOHR                                                     ! r in SI
!
      DEB_PDF=N0*N0 - N0*KD_SI*KD_SI*DEXP(-KD_SI*RA)/(FOUR*PI*RA)   ! ref. (1) eq. (3.1.4)
!
      END FUNCTION DEB_PDF  
!
!=======================================================================
!
      FUNCTION FUA_PDF(R,RS,T)
!
!  This function computes the electron gas pair distribution function 
!    rho2(r) for 3D systems, correct to order 2 in epsilon, the plasma 
!    parameter
!
!  References: (1) A. Isihara, "Electron Liquids", 2nd edition,
!C                     (Springer, 1998) p. 33
!
!  Input parameters:
!
!       * R        : grid point                            in unit of a_0
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 21 Jul 2020
!
!
      USE REAL_NUMBERS,     ONLY : TWO,THREE,HALF,THIRD,FOURTH
      USE CONSTANTS_P1,     ONLY : BOHR,E,K_B
      USE UTILITIES_1,      ONLY : RS_TO_N0
      USE SCREENING_VEC,    ONLY : DEBYE_VECTOR
      USE EXT_FUNCTIONS,    ONLY : DEI
!
      IMPLICIT NONE
!
      REAL (WP)             ::  R,RS,T
      REAL (WP)             ::  FUA_PDF
      REAL (WP)             ::  ARG_E
      REAL (WP)             ::  X,EPS,TX
      REAL (WP)             ::  N0,KD_SI
!
      REAL (WP)             ::  DEXP,DLOG
!
      N0=RS_TO_N0('3D',RS)                                          !
!
!  Computing the Debye screening vector
!
      CALL DEBYE_VECTOR('3D',T,RS,KD_SI)                            !
!
      X=KD_SI*R*BOHR                                                !
      EPS=E*E*KD_SI/(K_B*T)                                         ! ref. (1) eq. (1.1.2)
      TX=THREE*X                                                    !
!
      ARG_E=EPS*DEXP(-X)/X + HALF*EPS*EPS/X * (                   & !
                DEXP(-X)*( -THREE*FOURTH*DLOG(THREE) +            & !
                                X*FOURTH*DLOG(THREE) - THIRD ) +  & !
            FOURTH*DEXP(X)*(X*DEI(-TX))  +                        & ! ref. (1) eq. (3.1.13)
            FOURTH*DEXP(-X)*(X*DEI(-X))  +                        & !
            THREE*FOURTH*DEXP(X)*DEI(-TX)-                        & !
            THREE*FOURTH*DEXP(-X)*DEI(-X)+                        & !
            THIRD*DEXP(-TWO*X)                )                     !
!
      FUA_PDF=N0*N0*DEXP(-ARG_E)                                    ! ref. (1) eq. (3.1.12)
!
      END FUNCTION FUA_PDF  
!
!=======================================================================
!
      FUNCTION SDC_PDF(R,RS,T)
!
!  This function computes the electron gas pair distribution function 
!    rho2(r) for 3D systems, for short-distance correlations
!
!
!  --> Warning: valid if T >> k_B * (a_0 / 2)^2
!
!
!  References: (1) A. Isihara, "Electron Liquids", 2nd edition,
!                     Springer Series in Solid-State Sciences 96,
!                     (Springer, 1998) p. 33
!
!  Input parameters:
!
!       * R        : grid point          (in units of a_0)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  4 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,HALF,THIRD
      USE CONSTANTS_P1,     ONLY : BOHR,K_B
      USE PI_ETC,           ONLY : PI
      USE UTILITIES_1,      ONLY : RS_TO_N0
      USE SCREENING_VEC,    ONLY : DEBYE_VECTOR
!
      IMPLICIT NONE
!
      REAL (WP)             ::  R,RS,T
      REAL (WP)             ::  R2,R3
      REAL (WP)             ::  SDC_PDF
      REAL (WP)             ::  BETA,DELTA
      REAL (WP)             ::  N0,KD_SI
!
      REAL (WP)             ::  DSQRT
!
      R2=R*R*BOHR*BOHR                                              ! r^2 in SI
      R3=R2*R*BOHR                                                  ! r^3 in SI
!
      N0=RS_TO_N0('3D',RS)                                          !
!
!  Computing the Debye screening vector
!
      CALL DEBYE_VECTOR('3D',T,RS,KD_SI)                            !
!
      BETA=ONE/(K_B*T)                                              !
      DELTA=TWO*DSQRT(BETA)/BOHR                                    ! ref. (1) eq. (3.3.7)
!
      SDC_PDF=HALF*N0*N0*(                                       &  !
                           ONE - DSQRT(HALF*PI)*DELTA +          &  !
                           HALF*R + HALF*R2/BETA  -              &  !
                           TWO*THIRD*DSQRT(HALF*PI)*R2 /   (     &  ! ref. (1) eq. (3.3.10)
                              BOHR*DSQRT(BETA)             ) -   &  !
                           HALF*R3/(BOHR*BETA)                   &  !
                         )                                          !
!
      END FUNCTION SDC_PDF  
!
!=======================================================================
!
      FUNCTION WDA_PDF(R,RS,T)
!
!  This function computes watermelon diagrams-summed electron gas 
!    pair distribution function rho2(r) for 3D systems
!
!  References: (1) A. Isihara, "Electron Liquids", 2nd edition,
!                     Springer Series in Solid-State Sciences 96,
!                     (Springer, 1998) p. 33
!
!  Input parameters:
!
!       * R        : grid point                            in unit of a_0
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  4 Jun 2020
!
!
      USE CONSTANTS_P1,     ONLY : BOHR,E,K_B
      USE UTILITIES_1,      ONLY : RS_TO_N0
      USE SCREENING_VEC,    ONLY : DEBYE_VECTOR
!
      IMPLICIT NONE
!
      REAL (WP)             ::  R,RS,T
      REAL (WP)             ::  WDA_PDF
      REAL (WP)             ::  PHI_D,RA
      REAL (WP)             ::  N0,KD_SI
!
      REAL (WP)             ::  DSQRT
!
      N0=RS_TO_N0('3D',RS)                                          !
!
!  Computing the Debye screening vector
!
      CALL DEBYE_VECTOR('3D',T,RS,KD_SI)                            !
!
      RA=R*BOHR                                                     ! r in SI
!
      PHI_D=E*E*DEXP(-KD_SI*RA)/RA                                  ! ref. (1) eq. (3.1.7)
!
      WDA_PDF=N0*N0*DEXP(-PHI_D/(K_B*T))                            ! ref. (1) eq. (3.1.5)
!
      END FUNCTION WDA_PDF  
!
END MODULE PAIR_DISTRIBUTION
 
