!
!=======================================================================
!
MODULE SUM_RULES 
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE DF_SUM_RULES(IQ,Q,Z,RS,EPSR,EPSI,N_Z,Z_MAX)
!
!  This program computes the sum rules involving the dielectric function. 
!
!
!  Input parameters:
!
!       * IQ       : index of the external array containing Q
!       * Q        : value of Q = q/k_F
!       * Z        : array containing Z = hbar omega / E_F
!       * EPSR     : array Re [ epsilon(hbar omega) ]
!       * EPSI     : array Im [ epsilon(hbar omega) ]
!       * N_Z      : size of the Z and EPS arrays
!       * ENE_P_SI : plasmon energy at q = 0 (SI)
!       * Z_MAX    : largest value of Z for which epsilon(Q,Z) is defined
!
!
!  Output parameters:
!
!       * SR1      : conductivity sum rule
!       * SR2      : compressibility sum rule
!       * SR3      : f sum rule
!       * SR4      : screening sum rule
!       * SR5      : Bethe sum rule
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 22 Sep 2020
!
!
      USE DIMENSION_CODE,   ONLY : NSIZE  
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,TWO,  &
                                   HALF
      USE PI_ETC,           ONLY : PI,PI2
      USE UTILITIES_1,      ONLY : RS_TO_N0
      USE INTEGRATION,      ONLY : INTEGR_L,INTDE
      USE PLASMON_ENE_SI
!
      USE OUT_VALUES_3
      USE PRINT_FILES
!
      IMPLICIT NONE
!
      INTEGER               ::  I,IQ,N_Z,ID
!
      REAL (WP)             ::  Q,Z_MAX
      REAL (WP)             ::  Z(NSIZE),EPSR(NSIZE),EPSI(NSIZE)
      REAL (WP)             ::  F1(NSIZE),F2(NSIZE),F3(NSIZE),F4(NSIZE),F5(NSIZE)      ! integrands
      REAL (WP)             ::  SR1,SR2,SR3,SR4,SR5
      REAL (WP)             ::  EX1,EX2,EX3,EX4,EX5
      REAL (WP)             ::  ALPHA_Q                                                ! screening parameter
      REAL (WP)             ::  PIO2                                                   ! pi/2
      REAL (WP)             ::  A,B
      REAL (WP)             ::  EPS,ERR1,ERR2,ERR3,ERR4,ERR5
      REAL (WP)             ::  H
      REAL (WP)             ::  EI
      REAL (WP)             ::  RS,N0
      REAL (WP)             ::  TTINY
!
      REAL (WP)             ::  ABS,SIGN
!
      ID = 2                                                        !
!
      N0 = RS_TO_N0('3D',RS)                                        !
!
      H = Z(2) - Z(1)                                               !
!
      ALPHA_Q = EPSR(1)                                             !
      PIO2    = HALF * PI                                           !
      EPS     = 0.01E0_WP                                           !
      TTINY   = 1.0E-30_WP                                          !
!
      A = ZERO                                                      !
      B = Z_MAX                                                     !
!
!  Exact values of sum rules:
!
      EX1 =   PIO2 * ENE_P_SI * ENE_P_SI                            !
      EX2 =   PIO2 * ALPHA_Q                                        !
      EX3 = - PIO2 * ENE_P_SI * ENE_P_SI                            !
      EX4 = - PIO2  *ALPHA_Q / (ONE + ALPHA_Q)                      !
      EX5 = - TWO * PI2 * N0                                        !
!
!  Integrands for the sum rules
!
      DO I = 1,N_Z                                                  !
!
        EI = EPSI(I)                                                !
        IF(ABS(EPSI(I)) < TTINY) EPSI(I )= SIGN(TTINY,EI)           !
        F1(I) = EPSI(I) * Z(I)                                      !
        F2(I) = EPSI(I) / Z(I)                                      !
        F3(I) = ONE / F2(I)                                         !
        F4(I) = ONE / F1(I)                                         !
        IF(IQ == 1) THEN                                            !
          F5(I) = F3(I)                                             !
        END IF                                                      !
!
      END DO                                                        !
!
!  Computing the integrals from 0 to Z_MAX
!
      CALL INTEGR_L(F1,H,NSIZE,N_Z,A,ID)                            !
      CALL INTDE(F1,Z,N_Z,A,B,EPS,SR1,ERR1)                         !
      CALL INTDE(F2,Z,N_Z,A,B,EPS,SR2,ERR2)                         !
      CALL INTDE(F3,Z,N_Z,A,B,EPS,SR3,ERR3)                         !
      CALL INTDE(F4,Z,N_Z,A,B,EPS,SR4,ERR4)                         !
      CALL INTDE(F5,Z,N_Z,A,B,EPS,SR5,ERR5)                         !
!
!  Writing the results (EXn - SRn)
!
      IF(I_SR == ONE) THEN                                          !
        WRITE(IO_SR,*) Q,EX1-SR1,EX2-SR2,EX3-SR3,EX4-SR4,EX5-SR5    !
      END IF                                                        !
!
      END SUBROUTINE DF_SUM_RULES
!
END MODULE SUM_RULES
