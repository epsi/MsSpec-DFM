!
!=======================================================================
!
MODULE MOMENTS_EXACT 
!
      USE ACCURACY_REAL
!
!  It contains the following functions/subroutines:
!
!             * 
!
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE MOMENTS_POL_RPA_2D(X,RS,M1,M3)
!
!  This subroutine computes the first moments of the polarization 
!    Pi(q,omega) in the RPA model
!
!
!  References: (1) N. Iwamoto, Phys. Rev. A 30, 3289-3304 (1984)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!  Output parameter:
!
!       * M1       : moment of order 1
!       * M3       : moment of order 3
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  5 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : TWO,THREE,HALF
      USE CONSTANTS_P1,     ONLY : H_BAR,M_E
      USE FERMI_SI,         ONLY : EF_SI,KF_SI
      USE UTILITIES_1,      ONLY : RS_TO_N0
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X
      REAL (WP)             ::  M1,M3
      REAL (WP)             ::  Q_SI,OM_0
      REAL (WP)             ::  COEF
      REAL (WP)             ::  RS,N0
!
      Q_SI=TWO*X*KF_SI                                              ! q in SI
      OM_0=HALF*H_BAR*Q_SI*Q_SI/M_E                                 ! E_q / hbar
!
      N0=RS_TO_N0('2D',RS)                                          !
!
      COEF=N0*Q_SI*Q_SI/M_E                                         ! 
!
      M1=COEF                                                       ! ref. (1) eq. (2.25a)
      M3=COEF*( OM_0*OM_0 + THREE*EF_SI*OM_0/H_BAR )                ! ref. (1) eq. (2.25b)
!
      END SUBROUTINE MOMENTS_POL_RPA_2D  
!
!=======================================================================
!
      SUBROUTINE MOMENTS_POL_3D(X,RS,T,SQ_TYPE,GQ_TYPE,EC_TYPE,  &
                                IQ_TYPE,M1,M3)
!
!  This subroutine computes the first moments of the polarization 
!    Pi(q,omega) 
!
!
!  References: (1) S. Ichimaru, Rev. Mod. Phys. 54, 1017-1059 (1982)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * SQ_TYPE  : structure factor approximation (3D)
!       * GQ_TYPE  : local-field correction type (3D)
!       * EC_TYPE  : type of correlation energy functional
!       * IQ_TYPE  : type of approximation for I(q)
!
!
!  Output parameter:
!
!       * M1       : moment of order 1
!       * M3       : moment of order 3
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  5 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,FIVE,HALF
      USE CONSTANTS_P1,     ONLY : H_BAR,M_E
      USE FERMI_SI,         ONLY : EF_SI,KF_SI
      USE UTILITIES_1,      ONLY : RS_TO_N0
      USE IQ_FUNCTIONS_1
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 6)   ::  EC_TYPE
      CHARACTER (LEN = 4)   ::  GQ_TYPE
      CHARACTER (LEN = 3)   ::  SQ_TYPE,IQ_TYPE
!
      REAL (WP)             ::  X,RS,T
      REAL (WP)             ::  M1,M3
      REAL (WP)             ::  Q_SI,OM_0
      REAL (WP)             ::  COEF
      REAL (WP)             ::  IQ
      REAL (WP)             ::  N0
!
      Q_SI=TWO*X*KF_SI                                              ! q in SI
      OM_0=HALF*H_BAR*Q_SI*Q_SI/M_E                                 ! E_q / hbar
!
      N0=RS_TO_N0('3D',RS)                                          !
!
      COEF=N0*Q_SI*Q_SI/M_E                                         !
!
!  Computing the IQ function
!
      CALL IQ_3D(X,RS,IQ_TYPE,IQ)                                   !
!
!  Computing the moments
!
      M1=COEF                                                       ! ref. (1) eq. (3.34)
      M3=COEF*(                                                   & !
                OM_0*OM_0 + 12.0E0_WP*EF_SI*OM_0/(FIVE*H_BAR) +   & ! ref. (1) eq. (3.35)
                ENE_P_SI*ENE_P_SI/(H_BAR*H_BAR) * (ONE-IQ)        & !
              )                                                     !
!
      END SUBROUTINE MOMENTS_POL_3D  
!
!=======================================================================
!
      SUBROUTINE MOMENTS_LOS_3D(X,RS,T,C0,C2,C4)
!
!  This subroutine computes the first moments of the loss function: 
!  
!
!       Cn(q) = 1/pi int_{-inf}^{+inf} omega^n L(q,omega) d omega
!
!    with the loss function given by
!
!       L(q,omega) = -1/omega * Im[ 1/epsilon(q,omega) ]
!
!
!  References: (1) Yu. V. Arkhipov et al, Contrib. Plasma Phys. 55, 381-389 (2015)
!              (2) Yu. V. Arkhipov et al, Contrib. Plasma Phys. 59, e201800171 (2019)
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!
!  Output parameters:
!
!       * C0       : 0-th order moment of the loss function
!       * C2       : 2-th order moment of the loss function
!       * C4       : 4-th order moment of the loss function
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 18 Jun 2020
!
!
      USE REAL_NUMBERS,       ONLY : ZERO,ONE,THREE,FOUR,  &
                                     HALF,THIRD
      USE CONSTANTS_P1,       ONLY : H_BAR,M_E,K_B
      USE FERMI_SI,           ONLY : KF_SI
      USE PI_ETC,             ONLY : PI
      USE UTILITIES_1,        ONLY : RS_TO_N0
      USE VELOCITIES,         ONLY : VELOCITIES_3D
      USE PLASMA_SCALE
      USE PLASMA,             ONLY : PL_TYPE,ZION
      USE ENERGIES,           ONLY : EC_TYPE
      USE SCREENING_VEC,      ONLY : DEBYE_VECTOR
      USE PLASMON_ENE_SI
!      
      REAL (WP)             ::  X,Y,RS,T
      REAL (WP)             ::  C0,C2,C4
      REAL (WP)             ::  OP2
      REAL (WP)             ::  KQ4,KD2,K,U,H
      REAL (WP)             ::  Q2,Q4
      REAL (WP)             ::  N0
      REAL (WP)             ::  NONID,DEGEN
      REAL (WP)             ::  KD_SI
      REAL (WP)             ::  BETA,COEF
      REAL (WP)             ::  VE2,V_INT_2
!
      Y=X+X                                                         ! Y = q / k_F
!
      N0=RS_TO_N0('3D',RS)                                          !
!
      CALL DEBYE_VECTOR('3D',T,RS,KD_SI)                            !
      CALL PLASMON_SCALE(RS,T,ZION,NONID,DEGEN)                     !
      CALL VELOCITIES_3D(RS,T,EC_TYPE,VE2,V_INT_2)                  !
!
      KD2=KD_SI*KD_SI                                               !
      KQ4=16.0E0_WP*PI*N0*M_E*M_E/(H_BAR*H_BAR)                     !
!
      BETA=ONE/(K_B*T)                                              !
      OP2=(ENE_P_SI/H_BAR)**2                                       ! omega_p^2
      Q2=Y*Y*KF_SI*KF_SI                                            !
      Q4=Q2*Q2                                                      !
!
!  Parameter the calculation of U
!
      COEF=-FOUR*Q2*(NONID**1.5E0_WP)/(15.0E0_WP*BETA*M_E)          !
!
      KD2=KD_SI*KD_SI                                               !
      KQ4=16.0E0_WP*PI*N0*M_E*M_E/(H_BAR*H_BAR)                     !
!
      K=VE2*Q2/((ENE_P_SI/H_BAR)**2)  +                          &  ! ref. (2) eq. (12)
        (HALF*H_BAR/M_E)**2 * Q4/OP2                                !
      U=COEF*(-0.9052E0_WP/DSQRT(0.6322E0_WP+NONID) +            &  ! ref. (2) eq. (14) 
               0.27243E0_WP/(ONE+NONID))                            !
!
      IF(PL_TYPE == 'OCP') THEN                                     ! no ion case
        H=ZERO                                                      !
      ELSE IF(PL_TYPE == 'DCP') THEN                                !
        H=FOUR*THIRD*ZION*RS*DSQRT(NONID)/                       &  ! ref. (3) eq. (6)
          DSQRT(THREE*ZION*NONID*NONID + FOUR*RS +               &  !
                FOUR*NONID*DSQRT(THREE*(ONE+ZION)*RS))              !
      END IF                                                        !
!
!  Moments of the loss function
!
      C0=KD2*KQ4/(Q2*KQ4+Q4*KD2+KQ4*KD2)                            ! ref. (2) eq. (9)
      C2=OP2                                                        ! ref. (2) eq. (10)
      C4=OP2*OP2 * (ONE + K + U + H)                                ! ref. (1) eq. (4)
!
      END SUBROUTINE MOMENTS_LOS_3D  
!
END MODULE MOMENTS_EXACT 
