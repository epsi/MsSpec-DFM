!
!=======================================================================
!
MODULE LOSS_MOMENTS
!
      USE ACCURACY_REAL
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE LOSS_MOMENTS_AN(X,C0,C2,C4) 
!
!  This subroutine computes "analytically" the first three moments 
!    of the loss function.
!
!
!  References: (1) Yu. V. Arkhipov et al, EPL, 104,  35003 (2013)
!
!
!  Warning: This subroutine makes us of the Arkhipov et al definition
!           of the loss function
!
!                  / + INF             _           _
!            1    |            n-1    |       1     |
!   C_n  =  ----  |        omega   Im | - --------- |  d omega
!            pi   |                   |_   epsilon _|
!                / - INF
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!
!
!  Outpu parameters:
!
!       * C0       :  \
!       * C2       :   >  moments of the loss function 
!       * C4       :  /
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Nov 2020
!
!
      USE MATERIAL_PROP,      ONLY : DMN,RS
      USE LF_VALUES,          ONLY : IQ_TYPE
!
      USE REAL_NUMBERS,       ONLY : ONE,TWO,FOUR,HALF
      USE CONSTANTS_P1,       ONLY : H_BAR,M_E
      USE FERMI_SI,           ONLY : KF_SI,EF_SI
      USE PLASMON_ENE_SI
      USE DFUNC_STATIC,       ONLY : RPA1_EPS_S_LG
      USE UTILITIES_1,        ONLY : D
      USE IQ_FUNCTIONS_1
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)  ::  X
      REAL (WP), INTENT(OUT) ::  C0,C2,C4
!
      REAL (WP)              ::  Q_SI
      REAL (WP)              ::  OMP,OMQ
      REAL (WP)              ::  OMP2,OMQ2
      REAL (WP)              ::  EPSR,EPSI
      REAL (WP)              ::  DIME,AV_KE,IQ
!
!  Computing the plasmon frequency
!
      OMP = ENE_P_SI / H_BAR                                        ! omega_p
!
!  Computing the plasmon kinetic frequency
!
      Q_SI = TWO * X * KF_SI                                        ! q       in SI
      OMQ  = HALF * H_BAR * Q_SI * Q_SI / M_E                       ! omega_q in SI
!
      OMP2 = OMP * OMP                                              !
      OMQ2 = OMQ * OMQ                                              !
!
!  Computing the dimensionality
!
      DIME = D(DMN)                                                 !
!
!  Computing the static RPA dielectric function
!
      CALL RPA1_EPS_S_LG(X,DMN,EPSR,EPSI)                           !
!
!  Computing I(q)
!
      IF(DMN == '3D') THEN                                          !
        CALL IQ_3D(X,RS,IQ_TYPE,IQ)                                 !
      ELSE IF(DMN == '2D') THEN                                     !
        CONTINUE                                                    ! not implemented yet
      ELSE IF(DMN == '1D') THEN                                     !
        CONTINUE                                                    ! not implemented yet
      END IF                                                        !
!
!  Computing the average ground state kinetic energy :
!                       
!                    d     
!       <t>   = --------- E_F 
!          0      (d + 2)      
!
      AV_KE = DIME * EF_SI  / (DIME + TWO)                          ! <t>_0  in SI
!                                                                        
!  Getting the moments in SI
!
      C0 = ONE - ONE / EPSR                                         ! \
      C2 = OMP2                                                     !  |
      C4 = OMP2 * ( FOUR *  AV_KE * OMQ / H_BAR +                 & !  |> ref. (1) eq. (3)-(4)
                    OMQ2 + OMP2 * (ONE - IQ)                      & !  |
                  )                                                 ! /
!
      END SUBROUTINE LOSS_MOMENTS_AN
!
END MODULE LOSS_MOMENTS
