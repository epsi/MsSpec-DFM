!
!=======================================================================
!
MODULE PLASMON_DISP_EXACT 
!
!  This module computes the exact plasmon dispersion  
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE PLASMON_DISP_EX(IS,IC,YB,ENE_P_Q)
!
!  This subroutine computes exact plasmon dispersion from the 
!    dielectric function
!
!
!  Output variables :
!
!       * YB       : q / k_F
!       * ENE_P_Q  : plasmon energy at q in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 16 Dec 2020
!
!
      USE Q_GRID 
!
      USE REAL_NUMBERS,     ONLY : ZERO
      USE FERMI_SI,         ONLY : EF_SI
!
      USE CALCULATORS_3
      USE RE_EPS_0_TREATMENT
!
      IMPLICIT NONE
!
      INTEGER, PARAMETER     ::  N_ZERO = 10000 ! max number of zeros
!
      INTEGER, INTENT(OUT)   ::  IS,IC
!
      INTEGER                ::  IQ
      INTEGER                ::  NB
      INTEGER                ::  IB
!
      REAL (WP), INTENT(OUT) ::  ENE_P_Q(N_ZERO)
      REAL (WP), INTENT(OUT) ::  YB(N_ZERO)
!
      REAL (WP)              ::  Y,X
      REAL (WP)              ::  ZERO_B(N_ZERO)
      REAL (WP)              ::  QS,QC
!
      REAL (WP)              ::  FLOAT 
!
!  Initialisation of ENE_P_Q
!
      DO IB = 1,N_ZERO                                              !
        ENE_P_Q(IB) = ZERO                                          !
      END DO                                                        !
!
!  Extract the upper branch of Re[ eps(q,omega) ] = 0 
!   which is contained in unit IO_ZE
!
      CALL SELECT_BRANCH(2,NB,YB,ZERO_B)                            !
!
!  Get the q-bounds
!
      CALL COMPUTE_QBOUNDS_3D(NB,YB,ZERO_B,IS,IC,QS,QC)             !
!
!  Storing the plasmon energy in SI
!
      DO IB = IS,IC                                                 !
        ENE_P_Q(IB) = ZERO_B(IB) * EF_SI                            !
      END DO                                                        !
!
      END SUBROUTINE PLASMON_DISP_EX
!
END MODULE PLASMON_DISP_EXACT 
