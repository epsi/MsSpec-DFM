!
!=======================================================================
!
MODULE PLASMON_DISP_REAL
!
!  This module computes analytical plasmon dispersion without damping
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE PLASMON_DISP_R(X,RS,T,PL_DISP,ENE_P_Q)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!       * RS       : dimensionless factor
!       * T        : temperature                                 in SI
!       * PL_DISP  : method used to compute the dispersion
!
!
!  Output variables :
!
!       * ENE_P_Q  : plasmon energy at q in J
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 26 Oct 2020
!
!
      USE MATERIAL_PROP,    ONLY : DMN
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 7)    ::  PL_DISP
!
      REAL (WP), INTENT(IN)  ::  X,RS,T
      REAL (WP), INTENT(OUT) ::  ENE_P_Q
!
      IF(DMN == '3D') THEN                                          !
        CALL PLASMON_DISP_3D(X,RS,T,PL_DISP,ENE_P_Q)                !
      ELSE IF(DMN == '2D') THEN                                     !
        CALL PLASMON_DISP_2D(X,RS,T,PL_DISP,ENE_P_Q)                !
      ELSE IF(DMN == '1D') THEN                                     !
        CALL PLASMON_DISP_1D(X,RS,T,PL_DISP,ENE_P_Q)                !
      END IF                                                        !
!
      END SUBROUTINE PLASMON_DISP_R
!
!=======================================================================
!
      SUBROUTINE PLASMON_DISP_1D(X,RS,T,PL_DISP,ENE_P_Q)
!
!  This subroutine computes the coefficients AR of the plasmon dispersion
!    according to:
!
!           ENE_Q^2 = AR(0) + AR(1)*Q   + AR(2)*Q^2 + AR(3)*Q^3 +
!                             AR(4)*Q^4 + AR(5)*Q^5 + AR(6)*Q^6
!
!                   = ALPHA2 + BETA2 * Q^2 + GAMMA2 * Q^4 + DELTA^2 Q^6
!
!    where Q = q/ k_F
!
!     --> This is the real (q, hbar omega_q) case (no damping)
!
!  Warnings:  i) all input parameters are in SI units
!            ii) in the literature, beta^2 and gamma^2 are given
!                in atomic units (AU). Here, we work in SI.
!                We recall:
!
!                       ENE    : J             --> energy
!                       Q      : 1 / M         --> wave vector
!                       V      : M / S         --> speed
!                       OMEGA  : 1 / S         --> angular momentum
!                       HBAR   : J S           --> Planck constant / 2 pi
!
!                In order to obtain ENE_Q^2 in J^2, a dimension analysis
!                  shows that:
!
!     BETA2_AU  = COEF * V_F^2              --> BETA2_SI  = HBAR^2 * BETA2_AU
!     GAMMA2_AU = 1/4                       --> GAMMA2_SI = HBAR^2 * GAMMA2_AU / M^2
!     GAMMA2_AU = COEF * V_F^4 / OMEGA_P^2  --> GAMMA2_SI = HBAR^2 * GAMMA2_AU
!
!                Alternatively, we can work in atomic units and then change
!                  from Hartrees to Joules. This is what is coded here.
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!       * T        : temperature                                 in SI
!       * PL_DISP  : method used to compute the dispersion (3D)
!                      PL_DISP = 'HYDRODY' hydrodynamic model
!                      PL_DISP = 'RPA_MOD' RPA model
!
!
!  Output variables :
!
!       * ENE_P_Q  : plasmon energy at q in J
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 17 Nov 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,TWO
      USE CONSTANTS_P2,     ONLY : HARTREE
      USE FERMI_AU,         ONLY : KF_AU
!
      USE DISP_COEF_REAL
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 7)    ::  PL_DISP
!
!
      REAL (WP), INTENT(IN)  ::  X,RS,T
      REAL (WP), INTENT(OUT) ::  ENE_P_Q
!
      REAL (WP)              ::  Q,Q2,Q3,Q4,Q5,Q6
      REAL (WP)              ::  ENE2
!
      REAL (WP)              ::  SQRT
!
      INTEGER                ::  I
!
!
      DO I = 0, 6                                                   !
        AR(I) = ZERO                                                ! initialization
      END DO                                                        !
!
      Q  = TWO * X * KF_AU                                          ! q  in AU
      Q2 = Q  * Q                                                   !
      Q3 = Q2 * Q                                                   ! powers of q
      Q4 = Q3 * Q                                                   !
      Q5 = Q4 * Q                                                   !
      Q6 = Q5 * Q                                                   !
!
      IF(PL_DISP == 'HYDRODY') THEN                                 !
        CALL HYDRODY_DP_1D(RS,T,Q,AR)                               !
      ELSE IF(PL_DISP == 'RPA_MOD') THEN                            !
        CALL RPA_MOD_DP_1D(RS,T,Q,AR)                               !
      END IF                                                        !
!
      ENE2 = AR(0) + AR(1) * Q  + AR(2) * Q2 + AR(3) * Q3 +       & !energy^2 in AU
                     AR(4) * Q4 + AR(5) * Q5 + AR(6) * Q6           !
!
      ENE_P_Q = SQRT(ENE2)                                          ! plasmon energy at q in AU
!
!  Change of units: AU --> SI
!
      ENE_P_Q = ENE_P_Q * HARTREE                                   !
!
      END SUBROUTINE PLASMON_DISP_1D
!
!=======================================================================
!
      SUBROUTINE PLASMON_DISP_2D(X,RS,T,PL_DISP,ENE_P_Q)
!
!  This subroutine computes the coefficients AR of the plasmon dispersion
!    according to:
!
!           ENE_Q^2 = AR(0) + AR(1)*Q   + AR(2)*Q^2 + AR(3)*Q^3 +
!                             AR(4)*Q^4 + AR(5)*Q^5 + AR(6)*Q^6
!
!                   = ALPHA2 + BETA2 * Q^2 + GAMMA2 * Q^4 + DELTA^2 Q^6
!
!    where Q = q/ k_F
!
!     --> This is the real (q, hbar omega_q) case (no damping)
!
!  Warnings:  i) all input parameters are in SI units
!            ii) in the literature, beta^2 and gamma^2 are given
!                in atomic units (AU). Here, we work in SI.
!                We recall:
!
!                       ENE    : J             --> energy
!                       Q      : 1 / M         --> wave vector
!                       V      : M / S         --> speed
!                       OMEGA  : 1 / S         --> angular momentum
!                       HBAR   : J S           --> Planck constant / 2 pi
!
!                In order to obtain ENE_Q^2 in J^2, a dimension analysis
!                  shows that:
!
!     BETA2_AU  = COEF * V_F^2              --> BETA2_SI  = HBAR^2 * BETA2_AU
!     GAMMA2_AU = 1/4                       --> GAMMA2_SI = HBAR^2 * GAMMA2_AU / M^2
!     GAMMA2_AU = COEF * V_F^4 / OMEGA_P^2  --> GAMMA2_SI = HBAR^2 * GAMMA2_AU
!
!                Alternatively, we can work in atomic units and then change
!                  from Hartrees to Joules. This is what is coded here.
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature                                 in SI
!       * PL_DISP  : method used to compute the dispersion (3D)
!                      PL_DISP = 'HYDRODY' hydrodynamic model
!                      PL_DISP = 'RPA_MOD' RPA model
!                      PL_DISP = 'RAJAGOP' Rajagopal formula
!
!
!  Output variables :
!
!       * ENE_P_Q  : plasmon energy at q in J
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 17 Nov 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,TWO
      USE CONSTANTS_P2,     ONLY : HARTREE
      USE FERMI_AU,         ONLY : KF_AU
!
      USE DISP_COEF_REAL
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 7)    ::  PL_DISP
!
      REAL (WP), INTENT(IN)  ::  X,RS,T
      REAL (WP), INTENT(OUT) ::  ENE_P_Q
!
      REAL (WP)              ::  Q,Q2,Q3,Q4,Q5,Q6
      REAL (WP)              ::  ENE2
!
      REAL (WP)              ::  SQRT
!
      INTEGER                ::  I
!
      DO I = 0, 6                                                   !
        AR(I) = ZERO                                                ! initialization
      END DO                                                        !
!
      Q  = TWO * X * KF_AU                                          ! q  in AU
      Q2 = Q  * Q                                                   !
      Q3 = Q2 * Q                                                   ! powers of q
      Q4 = Q3 * Q                                                   !   in AU
      Q5 = Q4 * Q                                                   !
      Q6 = Q5 * Q                                                   !
!
      IF(PL_DISP == 'HYDRODY') THEN                                 !
        CALL HYDRODY_DP_2D(X,RS,T,AR)                               !
      ELSE IF(PL_DISP == 'RPA_MOD') THEN                            !
        CALL RPA_MOD_DP_2D(X,RS,T,AR)                               !
      ELSE IF(PL_DISP == 'RAJAGOP') THEN                            !
        CALL RAJAGOP_DP_2D(X,RS,T,AR)                               !
      END IF                                                        !
!
      ENE2 = AR(0) + AR(1) * Q  + AR(2) * Q2 + AR(3) * Q3 +       & !energy^2 in AU
                     AR(4) * Q4 + AR(5) * Q5 + AR(6) * Q6           !
!
      ENE_P_Q = SQRT(ENE2)                                          ! plasmon energy at q in AU
!
!  Change of units: AU --> SI
!
      ENE_P_Q = ENE_P_Q * HARTREE                                   !
!
      END SUBROUTINE PLASMON_DISP_2D
!
!=======================================================================
!
      SUBROUTINE PLASMON_DISP_3D(X,RS,T,PL_DISP,ENE_P_Q)
!
!  This subroutine computes the coefficients AR of the plasmon dispersion
!    according to:
!
!           ENE_Q^2 = AR(0) + AR(1)*Q   + AR(2)*Q^2 + AR(3)*Q^3 +
!                             AR(4)*Q^4 + AR(5)*Q^5 + AR(6)*Q^6
!
!                   = ALPHA2 + BETA2 * Q^2 + GAMMA2 * Q^4 + DELTA^2 Q^6    (1)
!
!   or
!
!           ENE_Q = omega_p + 2 alpha omega_F q^2 / k_F^2                  (2)
!
!    where Q = q/ k_F
!
!     --> This is the real (q, hbar omega_q) case (no damping)
!
!  Warnings:  i) all input parameters are in SI units
!            ii) in the literature, beta^2 and gamma^2 are given
!                in atomic units (AU). Here, we work in SI.
!                We recall:
!
!                       ENE    : J             --> energy
!                       Q      : 1 / M         --> wave vector
!                       V      : M / S         --> speed
!                       OMEGA  : 1 / S         --> angular momentum
!                       HBAR   : J S           --> Planck constant / 2 pi
!
!                In order to obtain ENE_Q^2 in J^2, a dimension analysis
!                  shows that:
!
!     BETA2_AU  = COEF * V_F^2              --> BETA2_SI  = HBAR^2 * BETA2_AU
!     GAMMA2_AU = 1/4                       --> GAMMA2_SI = HBAR^2 * GAMMA2_AU / M^2
!     GAMMA2_AU = COEF * V_F^4 / OMEGA_P^2  --> GAMMA2_SI = HBAR^2 * GAMMA2_AU
!
!                Alternatively, we can work in atomic units and then change
!                  from Hartrees to Joules. This is what is coded here.
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature                                 in SI
!       * PL_DISP  : method used to compute the dispersion (3D)
!                      PL_DISP = 'ELASTIC' elastic model
!                      PL_DISP = 'GOA_MOD' Gorobchenko model
!                      PL_DISP = 'HER_APP' Hertel-Appel model  <-- temperature-dependent
!                      PL_DISP = 'HUBBARD' Hubbard model
!                      PL_DISP = 'HYDRODY' hydrodynamic model
!                      PL_DISP = 'SGBBN_M' SGBBN model
!                      PL_DISP = 'RP1_MOD' RPA model up to q^2
!                      PL_DISP = 'RP2_MOD' RPA model up to q^4
!                      PL_DISP = 'AL0_MOD' gamma_0   limit
!                      PL_DISP = 'ALI_MOD' gamma_inf limit
!                      PL_DISP = 'NOP_MOD' Nozières-Pines model
!                      PL_DISP = 'UTI_MOD' Utsumi-Ichimaru model
!                      PL_DISP = 'TWA_MOD' Toigo-Woodruff model
!                      PL_DISP = 'SUM_RU2' f-sum rule sum_rule
!                      PL_DISP = 'SUM_RU3' 3rd-frequency sum_rule
!
!                  from Hartrees to Joules. This is what is coded here.
!
!  Intermediate parameters:
!
!       * I_SI  :  switch for unit of energy
!                    I_SI = 0  --> in AU unit
!                    I_SI = 1  --> in SI unit
!       * I_SQ  :  switch for computation of E or E^2
!                    I_SQ = 0  --> energy computed
!                    I_SQ = 1  --> energy^2 computed
!       * I_FO  :  switch for formula used to compute energy
!                    I_FO = 1  --> formula (1)
!                    I_FO = 2  --> formula (2)
!
!
!  Output variables :
!
!       * ENE_P_Q  : plasmon energy at q in J
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  7 Dec 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,TWO,THREE
      USE CONSTANTS_P2,     ONLY : HARTREE
      USE FERMI_AU,         ONLY : KF_AU
      USE LF_VALUES,        ONLY : GQ_TYPE,IQ_TYPE
      USE SF_VALUES,        ONLY : SQ_TYPE
      USE PLASMON_ENE_SI
!
      USE DISP_COEF_REAL
!
      USE OUT_VALUES_3,       ONLY : I_ZE
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 7)    ::  PL_DISP
!
      REAL (WP), INTENT(IN)  ::  X,RS,T
      REAL (WP), INTENT(OUT) ::  ENE_P_Q
!
      REAL (WP)              ::  Q,Q2,Q3,Q4,Q5,Q6
      REAL (WP)              ::  ENE2,EN_P,C1,C2
!
      REAL (WP)              ::  SQRT
!
      INTEGER                ::  CHANGE,I
      INTEGER                ::  I_SI,I_SQ,I_FO
!
      C1     = THREE / 32.0E0_WP                                    ! coefficient for 'GOA_MOD'
      CHANGE = 0                                                    ! set to 1 only for 'GOA_MOD'
!
      I_SI   = 0                                                    ! switch for energy computed in SI
      I_SQ   = 0                                                    ! omega(q) computed [ not omega^2(q) ]
      I_FO   = 1                                                    ! formula (1) used to compute energy
!
      Q  = TWO * X * KF_AU                                          ! q  in AU
      Q2 = Q  * Q                                                   !
      Q3 = Q2 * Q                                                   ! powers of q
      Q4 = Q3 * Q                                                   !
      Q5 = Q4 * Q                                                   !
      Q6 = Q5 * Q                                                   !
!
!  Computing the plasmon energy in atomic units
!
      EN_P = ENE_P_SI / HARTREE                                     ! plasmon energy in AU
!
      IF(PL_DISP == 'GOA_MOD') THEN                                 !
        PL_DISP = 'RP1_MOD'                                         !
        CHANGE  = 1                                                 !
      END IF                                                        !
!
      DO I = 0, 6                                                   !
        AR(I) = ZERO                                                ! initialization
      END DO                                                        !
!
      IF(PL_DISP == 'ELASTIC') THEN                                 !
        CALL ELASTIC_DP_3D(RS,T,AR)                                 !
        I_SQ = 1                                                    !
      ELSE IF(PL_DISP == '  EXACT') THEN                            !
        I_ZE = 1                                                    ! post-processing calculation
      ELSE IF(PL_DISP == 'HER_APP') THEN                            !
        CALL HER_APP_DP_3D(RS,T,AR)                                 !
        I_SQ = 1                                                    !
      ELSE IF(PL_DISP == 'HUBBARD') THEN                            !
        CALL HUBBARD_DP_3D(RS,T,AR)                                 !
        I_SQ = 1                                                    !
      ELSE IF(PL_DISP == 'HYDRODY') THEN                            !
        CALL HYDRODY_DP_3D(RS,T,AR)                                 !
        I_SQ = 1                                                    !
      ELSE IF(PL_DISP == 'RP2_MOD') THEN                            !
        CALL RP2_MOD_DP_3D(RS,T,AR)                                 !
        I_SQ = 1                                                    !
      ELSE IF(PL_DISP == 'SGBBN_M') THEN                            !
        CALL SGBBN_M_DP_3D(RS,T,AR)                                 !
        I_SQ = 1                                                    !
      ELSE IF(PL_DISP == 'SUM_RU2') THEN                            !
        CALL SUMRULE1_DP_3D(X,ENE2)                                 !
        I_SQ = 1                                                    !
        I_SI = 1                                                    !
        I_FO = 2                                                    !
      ELSE IF(PL_DISP == 'SUM_RU3') THEN                            !
        CALL SUMRULE3_DP_3D(X,ENE2)                                 !
        I_SQ = 1                                                    !
        I_SI = 1                                                    !
        I_FO = 2                                                    !
      ELSE                                                          !
        CALL PLASMON_DISP_3D_2(X,RS,T,PL_DISP,ENE_P_Q)              !
        I_SI = 1                                                    !
        I_FO = 2                                                    !
      END IF                                                        !
!
      IF(I_FO == 1) THEN                                            !
          ENE2 = AR(0) + AR(1) * Q  + AR(2) * Q2 + AR(3) * Q3 +   & !energy^2 in AU
                         AR(4) * Q4 + AR(5) * Q5 + AR(6) * Q6       !
      END IF                                                        !
!
      IF(I_SQ == 1) THEN                                            !
        ENE_P_Q = SQRT(ENE2)                                        !
      END IF                                                        !
!
      IF(CHANGE == 1) THEN                                          !
        ENE_P_Q = ENE_P_Q / HARTREE - C1 * EN_P * Q2                ! 'GOA_MOD' case
        PL_DISP = 'GOA_MOD'                                         !
        CHANGE  = 0                                                 !
        I_SI    = 0                                                 !
        I_SQ    = 0                                                 !
        I_FO    = 1                                                 !
      END IF                                                        !
!
!  Change of units: AU --> SI
!
      IF(I_SI == 0) THEN                                            !
        ENE_P_Q = ENE_P_Q * HARTREE                                 !
      END IF                                                        !
!
      END SUBROUTINE PLASMON_DISP_3D
!
!=======================================================================
!
      SUBROUTINE ELASTIC_DP_3D(R_S,T,AD)
!
!  This subroutine computes the coefficients of the plasmon dispersion
!    within the elastic approach in 3D systems according to:
!
!           ENE_Q^2 = AD(0) + AD(1)*Q   + AD(2)*Q^2 + AD(3)*Q^3 +
!                             AD(4)*Q^4 + AD(5)*Q^5 + AD(6)*Q^6
!
!                   = ALPHA2 + BETA2 * Q^2 + GAMMA2 * Q^4 + DELTA^2 Q^6
!
!    where Q = q/ k_F
!
!
!  References: (1) Ll. Serra, F. Garcias, M. Barranco, N. Barberan
!                      and J. Navarro, Phys. Rev. B 44, 1492-1498 (1991)
!
!
!     --> This is the real (q, hbar omega_q) case (no damping)
!
!  Warnings:  i) all input parameters are in SI units
!            ii) in the literature, beta^2 and gamma^2 are given
!                in atomic units (AU). Here, we work in SI.
!                We recall:
!
!                       ENE    : J             --> energy
!                       Q      : 1 / M         --> wave vector
!                       V      : M / S         --> speed
!                       OMEGA  : 1 / S         --> angular momentum
!                       HBAR   : J S           --> Planck constant / 2 pi
!
!                In order to obtain ENE_Q^2 in J^2, a dimension analysis
!                  shows that:
!
!     BETA2_AU  = COEF * V_F^2              --> BETA2_SI  = HBAR^2 * BETA2_AU
!     GAMMA2_AU = 1/4                       --> GAMMA2_SI = HBAR^2 * GAMMA2_AU / M^2
!     GAMMA2_AU = COEF * V_F^4 / OMEGA_P^2  --> GAMMA2_SI = HBAR^2 * GAMMA2_AU
!
!                Alternatively, we can work in atomic units and then change
!                  from Hartrees to Joules. This is what is coded here.
!
!  Input parameters:
!
!       * R_S      : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!  Output variables :
!
!       * AD(0:6)  : coefficients of the squared dispersion in AU
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  7 Dec 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,NINE,THREE,FOURTH
      USE CONSTANTS_P2,     ONLY : HARTREE
      USE FERMI_AU,         ONLY : VF_AU
      USE PI_ETC,           ONLY : PI
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)  ::  R_S,T
      REAL (WP), INTENT(OUT) ::  AD(0:6)
!
      REAL (WP)              ::  HAR2,VF2,RS
      REAL (WP)              ::  DENOM,XI
!
      INTEGER                ::  I
!
!  Initialisations
!
      DO I = 0, 6                                                   !
        AD(I) = ZERO                                                !
      END DO                                                        !
!
      HAR2 = HARTREE * HARTREE                                      !
      RS    = R_S                                                   ! rs in AU
      VF2   = VF_AU * VF_AU                                         ! v_F^2 in AU
      DENOM = NINE * (7.8E0_WP + RS)**3                             !
      XI    = 0.88E0_WP * RS * (7.80E0_WP + RS + RS) / DENOM        !
!
!  Coefficients in AU
!
      AD(0) = ENE_P_SI * ENE_P_SI / HAR2                            !
      AD(2) = (0.60E0_WP * VF2 - VF_AU / (THREE * PI) - XI)         ! ref. (1) eq. (6)
      AD(4) = FOURTH                                                !
!
      END SUBROUTINE ELASTIC_DP_3D
!
!=======================================================================
!
      SUBROUTINE HER_APP_DP_3D(RS,T,AD)
!
!  This subroutine computes the coefficients of the plasmon dispersion
!    within the Hertel-Appel approach in 3D systems according to:
!
!           ENE_Q^2 = AD(0) + AD(1)*Q   + AD(2)*Q^2 + AD(3)*Q^3 +
!                             AD(4)*Q^4 + AD(5)*Q^5 + AD(6)*Q^6
!
!                   = ALPHA2 + BETA2 * Q^2 + GAMMA2 * Q^4 + DELTA^2 Q^6
!
!    where Q = q/ k_F
!
!
!  References: (1) P. Hertel and J. Appel, Phys. Rev. B 26, 5730-5742 (1982)
!
!
!     --> This is the real (q, hbar omega_q) case (no damping)
!
!  Warnings:  i) all input parameters are in SI units
!            ii) in the literature, beta^2 and gamma^2 are given
!                in atomic units (AU). Here, we work in SI.
!                We recall:
!
!                       ENE    : J             --> energy
!                       Q      : 1 / M         --> wave vector
!                       V      : M / S         --> speed
!                       OMEGA  : 1 / S         --> angular momentum
!                       HBAR   : J S           --> Planck constant / 2 pi
!
!                In order to obtain ENE_Q^2 in J^2, a dimension analysis
!                  shows that:
!
!     BETA2_AU  = COEF * V_F^2              --> BETA2_SI  = HBAR^2 * BETA2_AU
!     GAMMA2_AU = 1/4                       --> GAMMA2_SI = HBAR^2 * GAMMA2_AU / M^2
!     GAMMA2_AU = COEF * V_F^4 / OMEGA_P^2  --> GAMMA2_SI = HBAR^2 * GAMMA2_AU
!
!                Alternatively, we can work in atomic units and then change
!                  from Hartrees to Joules. This is what is coded here.
!
!  Input parameters:
!
!       * R_S      : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!  Output variables :
!
!       * AD(0:6)  : coefficients of the squared dispersion in AU
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  7 Dec 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,THREE,SIX,HALF
      USE CONSTANTS_P1,     ONLY : H_BAR,M_E,K_B
      USE CONSTANTS_P2,     ONLY : HARTREE
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)  ::  RS,T
      REAL (WP), INTENT(OUT) ::  AD(0:6)
!
      REAL (WP)              ::  KBT,H2M
      REAL (WP)              ::  HAR2,EN_P2
!
      INTEGER                ::  I
!
!  Initialisations                                                  !
!
      DO I = 0, 6                                                   !
        AD(I) = ZERO                                                !
      END DO                                                        !
!
      KBT = K_B * T / HARTREE                                       ! k_B T in AU
!
      HAR2 = HARTREE * HARTREE                                      !
!
      EN_P2 = ENE_P_SI * ENE_P_SI / HAR2                            ! (plasmon energy)^2 in AU
!
!  Coefficients in AU
!
      AD(0) = EN_P2                                                 !
      AD(2) = THREE * KBT                                           !
!
      END SUBROUTINE HER_APP_DP_3D
!
!=======================================================================
!
      SUBROUTINE HUBBARD_DP_3D(RS,T,AD)
!
!  This subroutine computes the coefficients of the plasmon dispersion
!    within the Hubbard approach in 3D systems according to:
!
!           ENE_Q^2 = AD(0) + AD(1)*Q   + AD(2)*Q^2 + AD(3)*Q^3 +
!                             AD(4)*Q^4 + AD(5)*Q^5 + AD(6)*Q^6
!
!                   = ALPHA2 + BETA2 * Q^2 + GAMMA2 * Q^4 + DELTA^2 Q^6
!
!    where Q = q/ k_F
!
!
!  References: (1) P. Jewsbury, Aust. J. Phys. 32, 361-368 (1979)
!
!
!     --> This is the real (q, hbar omega_q) case (no damping)
!
!  Warnings:  i) all input parameters are in SI units
!            ii) in the literature, beta^2 and gamma^2 are given
!                in atomic units (AU). Here, we work in SI.
!                We recall:
!
!                       ENE    : J             --> energy
!                       Q      : 1 / M         --> wave vector
!                       V      : M / S         --> speed
!                       OMEGA  : 1 / S         --> angular momentum
!                       HBAR   : J S           --> Planck constant / 2 pi
!
!                In order to obtain ENE_Q^2 in J^2, a dimension analysis
!                  shows that:
!
!     BETA2_AU  = COEF * V_F^2              --> BETA2_SI  = HBAR^2 * BETA2_AU
!     GAMMA2_AU = 1/4                       --> GAMMA2_SI = HBAR^2 * GAMMA2_AU / M^2
!     GAMMA2_AU = COEF * V_F^4 / OMEGA_P^2  --> GAMMA2_SI = HBAR^2 * GAMMA2_AU
!
!                Alternatively, we can work in atomic units and then change
!                  from Hartrees to Joules. This is what is coded here.
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!  Output variables :
!
!       * AD(0:6)  : coefficients of the squared dispersion in AU
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  7 Dec 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,NINE,THIRD,FOURTH
      USE CONSTANTS_P1,     ONLY : BOHR,H_BAR
      USE CONSTANTS_P2,     ONLY : HARTREE
      USE FERMI_AU,         ONLY : KF_AU,VF_AU
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)  ::  RS,T
      REAL (WP), INTENT(OUT) ::  AD(0:6)
!
      REAL (WP)              ::  HAR2
      REAL (WP)              ::  VF2,VF4,KF2,KF4
      REAL (WP)              ::  OMP2,G1,G2
!
      INTEGER                ::  I
!
!  Initialisations                                                  !
!
      DO I = 0, 6                                                   !
        AD(I) = ZERO                                                !
      END DO                                                        !
!
      HAR2 = HARTREE * HARTREE                                      !
!
      VF2  = VF_AU * VF_AU                                          ! v_F^2 in AU
      VF4  = VF2 * VF2                                              ! v_F^4 in AU
      KF2  = KF_AU * KF_AU                                          ! k_F^2 in AU
      KF4  = KF2 * KF2                                              ! k_F^4 in AU
!
      OMP2 = ENE_P_SI * ENE_P_SI / HAR2                             ! omega_p^2 in AU
!
      G1 = FOURTH + 0.00636E0_WP * RS                               ! ref. (1) eq. (16)
      G2 = - 0.0391E0_WP + 0.00248E0_WP * RS                        ! ref. (1) eq. (16)
!
!  Coefficients in AU
!
      AD(0) = OMP2                                                  !
      AD(2) = 0.60E0_WP * VF2  - G1 * OMP2 / KF2                    ! ref. (1) eq. (18)
      AD(4) = FOURTH + 12.0E0_WP * VF4 / (175.0E0_WP * OMP2) -    & !
                       OMP2 * (G2 + G1 * G1) / KF4                  ! ref. (1) eq. (18)
!
      END SUBROUTINE HUBBARD_DP_3D
!
!=======================================================================
!
      SUBROUTINE HYDRODY_DP_1D(RS,T,Q_SI,AD)
!
!  This subroutine computes the coefficients of the plasmon dispersion
!    within the hydrodynamic approach in 1D systems according to:
!
!           ENE_Q^2 = AD(0) + AD(1)*Q   + AD(2)*Q^2 + AD(3)*Q^3 +
!                             AD(4)*Q^4 + AD(5)*Q^5 + AD(6)*Q^6
!
!                   = ALPHA2 + BETA2 * Q^2 + GAMMA2 * Q^4 + DELTA^2 Q^6
!
!    where Q = q/ k_F
!
!     --> This is the real (q, hbar omega_q) case (no damping)
!
!  Warnings:  i) all input parameters are in SI units
!            ii) in the literature, beta^2 and gamma^2 are given
!                in atomic units (AU). Here, we work in SI.
!                We recall:
!
!                       ENE    : J             --> energy
!                       Q      : 1 / M         --> wave vector
!                       V      : M / S         --> speed
!                       OMEGA  : 1 / S         --> angular momentum
!                       HBAR   : J S           --> Planck constant / 2 pi
!
!                In order to obtain ENE_Q^2 in J^2, a dimension analysis
!                  shows that:
!
!     BETA2_AU  = COEF * V_F^2              --> BETA2_SI  = HBAR^2 * BETA2_AU
!     GAMMA2_AU = 1/4                       --> GAMMA2_SI = HBAR^2 * GAMMA2_AU / M^2
!     GAMMA2_AU = COEF * V_F^4 / OMEGA_P^2  --> GAMMA2_SI = HBAR^2 * GAMMA2_AU
!
!                Alternatively, we can work in atomic units and then change
!                  from Hartrees to Joules. This is what is coded here.
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * Q_SI     : plasmon wave vector                         in 1/m
!
!  Output variables :
!
!       * AD(0:6)  : coefficients of the squared dispersion in AU
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 28 Jul 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,TWO,THIRD
      USE CONSTANTS_P1,     ONLY : BOHR
      USE CONSTANTS_P2,     ONLY : HARTREE
      USE FERMI_AU,         ONLY : VF_AU
      USE CONFIN_VAL,       ONLY : R0
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS,T,Q_SI
      REAL (WP)             ::  HAR2,Q,QR0,EN_P2,VF2
      REAL (WP)             ::  AD(0:6)
!
      INTEGER               ::  I
!
!  Initialisations
!
      DO I=0,6                                                      !
        AD(I)=ZERO                                                  !
      END DO                                                        !
!
      HAR2=HARTREE*HARTREE                                          !
      Q=Q_SI*BOHR                                                   ! q in atomic units
      QR0=Q*R0                                                      !
      VF2=VF_AU*VF_AU                                               ! v_F^2 in AU
!
      EN_P2=ENE_P_SI*ENE_P_SI/HAR2                                  ! (plasmon energy)^2 in AU
!
!  Coefficients in AU
!
      AD(2)=EN_P2*(DLOG(TWO/QR0)+                               &   !
                        THIRD*VF2)                                  !
!
      END SUBROUTINE HYDRODY_DP_1D
!
!=======================================================================
!
      SUBROUTINE HYDRODY_DP_2D(X,RS,T,AD)
!
!  This subroutine computes the coefficients of the plasmon dispersion
!    within the hydrodynamic approach in 2D systems according to:
!
!           ENE_Q^2 = AD(0) + AD(1)*Q   + AD(2)*Q^2 + AD(3)*Q^3 +
!                             AD(4)*Q^4 + AD(5)*Q^5 + AD(6)*Q^6
!
!                   = ALPHA2 + BETA2 * Q^2 + GAMMA2 * Q^4 + DELTA^2 Q^6
!
!    where Q = q/ k_F
!
!     --> This is the real (q, hbar omega_q) case (no damping)
!
!  Warnings:  i) all input parameters are in SI units
!            ii) in the literature, beta^2 and gamma^2 are given
!                in atomic units (AU). Here, we work in SI.
!                We recall:
!
!                       ENE    : J             --> energy
!                       Q      : 1 / M         --> wave vector
!                       V      : M / S         --> speed
!                       OMEGA  : 1 / S         --> angular momentum
!                       HBAR   : J S           --> Planck constant / 2 pi
!
!                In order to obtain ENE_Q^2 in J^2, a dimension analysis
!                  shows that:
!
!     BETA2_AU  = COEF * V_F^2              --> BETA2_SI  = HBAR^2 * BETA2_AU
!     GAMMA2_AU = 1/4                       --> GAMMA2_SI = HBAR^2 * GAMMA2_AU / M^2
!     GAMMA2_AU = COEF * V_F^4 / OMEGA_P^2  --> GAMMA2_SI = HBAR^2 * GAMMA2_AU
!
!                Alternatively, we can work in atomic units and then change
!                  from Hartrees to Joules. This is what is coded here.
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!  Output variables :
!
!       * AD(0:6)  : coefficients of the squared dispersion in AU
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 28 Jul 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,HALF
      USE CONSTANTS_P2,     ONLY : HARTREE
      USE FERMI_AU,         ONLY : VF_AU
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X,RS,T
      REAL (WP)             ::  HAR2
      REAL (WP)             ::  AD(0:6)
!
      INTEGER               ::  I
!
!  Initialisations
!
      DO I = 0, 6                                                      !
        AD(I)=ZERO                                                  !
      END DO                                                        !
!
      HAR2=HARTREE*HARTREE                                          !
!
!  Coefficients in AU
!
      AD(0)=ENE_P_SI*ENE_P_SI/HAR2                                  !
      AD(2)=HALF*VF_AU*VF_AU                                        !
!
      END SUBROUTINE HYDRODY_DP_2D
!
!=======================================================================
!
      SUBROUTINE HYDRODY_DP_3D(RS,T,AD)
!
!  This subroutine computes the coefficients of the plasmon dispersion
!    within the hydrodynamic approach in 3D systems according to:
!
!           ENE_Q^2 = AD(0) + AD(1)*Q   + AD(2)*Q^2 + AD(3)*Q^3 +
!                             AD(4)*Q^4 + AD(5)*Q^5 + AD(6)*Q^6
!
!                   = ALPHA2 + BETA2 * Q^2 + GAMMA2 * Q^4 + DELTA^2 Q^6
!
!    where Q = q/ k_F
!
!     --> This is the real (q, hbar omega_q) case (no damping)
!
!  Warnings:  i) all input parameters are in SI units
!            ii) in the literature, beta^2 and gamma^2 are given
!                in atomic units (AU). Here, we work in SI.
!                We recall:
!
!                       ENE    : J             --> energy
!                       Q      : 1 / M         --> wave vector
!                       V      : M / S         --> speed
!                       OMEGA  : 1 / S         --> angular momentum
!                       HBAR   : J S           --> Planck constant / 2 pi
!
!                In order to obtain ENE_Q^2 in J^2, a dimension analysis
!                  shows that:
!
!     BETA2_AU  = COEF * V_F^2              --> BETA2_SI  = HBAR^2 * BETA2_AU
!     GAMMA2_AU = 1/4                       --> GAMMA2_SI = HBAR^2 * GAMMA2_AU / M^2
!     GAMMA2_AU = COEF * V_F^4 / OMEGA_P^2  --> GAMMA2_SI = HBAR^2 * GAMMA2_AU
!
!                Alternatively, we can work in atomic units and then change
!                  from Hartrees to Joules. This is what is coded here.
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!  Output variables :
!
!       * AD(0:6)  : coefficients of the squared dispersion in AU
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 28 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,THIRD,FOURTH
      USE CONSTANTS_P2,     ONLY : HARTREE
      USE FERMI_AU,         ONLY : VF_AU
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)  ::  RS,T
      REAL (WP), INTENT(OUT) ::  AD(0:6)
!
      REAL (WP)              ::  HAR2
!
      INTEGER                ::  I
!
!  Initialisations
!
      DO I = 0, 6                                                   !
        AD(I) = ZERO                                                !
      END DO                                                        !
!
      HAR2 = HARTREE * HARTREE                                      !
!
!  Coefficients in AU
!
      AD(0) = ENE_P_SI * ENE_P_SI / HAR2                            !
      AD(2) = THIRD * VF_AU * VF_AU                                 !
      AD(4) = FOURTH                                                !
!
      END SUBROUTINE HYDRODY_DP_3D
!
!=======================================================================
!
      SUBROUTINE RPA_MOD_DP_1D(RS,T,Q_SI,AD)
!
!  This subroutine computes the coefficients of the plasmon dispersion
!    within the RPA approach in 1D systems according to:
!
!           ENE_Q^2 = AD(0) + AD(1)*Q   + AD(2)*Q^2 + AD(3)*Q^3 +
!                             AD(4)*Q^4 + AD(5)*Q^5 + AD(6)*Q^6
!
!                   = ALPHA2 + BETA2 * Q^2 + GAMMA2 * Q^4 + DELTA^2 Q^6
!
!    where Q = q/ k_F
!
!     --> This is the real (q, hbar omega_q) case (no damping)
!
!  Warnings:  i) all input parameters are in SI units
!            ii) in the literature, beta^2 and gamma^2 are given
!                in atomic units (AU). Here, we work in SI.
!                We recall:
!
!                       ENE    : J             --> energy
!                       Q      : 1 / M         --> wave vector
!                       V      : M / S         --> speed
!                       OMEGA  : 1 / S         --> angular momentum
!                       HBAR   : J S           --> Planck constant / 2 pi
!
!                In order to obtain ENE_Q^2 in J^2, a dimension analysis
!                  shows that:
!
!     BETA2_AU  = COEF * V_F^2              --> BETA2_SI  = HBAR^2 * BETA2_AU
!     GAMMA2_AU = 1/4                       --> GAMMA2_SI = HBAR^2 * GAMMA2_AU / M^2
!     GAMMA2_AU = COEF * V_F^4 / OMEGA_P^2  --> GAMMA2_SI = HBAR^2 * GAMMA2_AU
!
!                Alternatively, we can work in atomic units and then change
!                  from Hartrees to Joules. This is what is coded here.
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * Q_SI     : plasmon wave vector                         in 1/m
!
!  Output variables :
!
!       * AD(0:6)  : coefficients of the squared dispersion in AU
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 17 Nov 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,TWO
      USE CONSTANTS_P1,     ONLY : BOHR
      USE CONSTANTS_P2,     ONLY : HARTREE
      USE FERMI_AU,         ONLY : VF_AU
      USE CONFIN_VAL,       ONLY : R0
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)  ::  RS,T,Q_SI
      REAL (WP), INTENT(OUT) ::  AD(0:6)
!
      REAL (WP)              ::  HAR2,Q,QR0,EN_P2,VF2
!
      REAL (WP)              ::  LOG
!
      INTEGER                ::  I
!
!  Initialisations
!
      DO I = 0,6                                                    !
        AD(I) = ZERO                                                !
      END DO                                                        !
!
      HAR2 = HARTREE * HARTREE                                      !
!
      VF2 = VF_AU * VF_AU                                           ! v_F^2 in AU
      Q   = Q_SI * BOHR                                             ! q in atomic units
      QR0 = Q * R0                                                  !
!
      EN_P2 = ENE_P_SI * ENE_P_SI / HAR2                            ! (plasmon energy)^2 in AU
!
!  Coefficients in AU
!
      AD(2) = EN_P2 * ( DLOG(TWO / QR0) + VF2 )                     !
!
      END SUBROUTINE RPA_MOD_DP_1D
!
!=======================================================================
!
      SUBROUTINE RPA_MOD_DP_2D(X,RS,T,AD)
!
!  This subroutine computes the coefficients of the plasmon dispersion
!    within the RPA approach in 2D systems according to:
!
!           ENE_Q^2 = AD(0) + AD(1)*Q   + AD(2)*Q^2 + AD(3)*Q^3 +
!                             AD(4)*Q^4 + AD(5)*Q^5 + AD(6)*Q^6
!
!                   = ALPHA2 + BETA2 * Q^2 + GAMMA2 * Q^4 + DELTA^2 Q^6
!
!    where Q = q/ k_F
!
!     --> This is the real (q, hbar omega_q) case (no damping)
!
!  Warnings:  i) all input parameters are in SI units
!            ii) in the literature, beta^2 and gamma^2 are given
!                in atomic units (AU). Here, we work in SI.
!                We recall:
!
!                       ENE    : J             --> energy
!                       Q      : 1 / M         --> wave vector
!                       V      : M / S         --> speed
!                       OMEGA  : 1 / S         --> angular momentum
!                       HBAR   : J S           --> Planck constant / 2 pi
!
!                In order to obtain ENE_Q^2 in J^2, a dimension analysis
!                  shows that:
!
!     BETA2_AU  = COEF * V_F^2              --> BETA2_SI  = HBAR^2 * BETA2_AU
!     GAMMA2_AU = 1/4                       --> GAMMA2_SI = HBAR^2 * GAMMA2_AU / M^2
!     GAMMA2_AU = COEF * V_F^4 / OMEGA_P^2  --> GAMMA2_SI = HBAR^2 * GAMMA2_AU
!
!                Alternatively, we can work in atomic units and then change
!                  from Hartrees to Joules. This is what is coded here.
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!  Output variables :
!
!       * AD(0:6)  : coefficients of the squared dispersion in AU
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 17 Nov 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,TWO
      USE CONSTANTS_P1,     ONLY : M_E
      USE CONSTANTS_P2,     ONLY : HARTREE
      USE FERMI_SI,         ONLY : KF_SI
      USE FERMI_AU,         ONLY : VF_AU
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)  ::  X,RS,T
      REAL (WP), INTENT(OUT) ::  AD(0:6)
!
      REAL (WP)              ::  Q_SI
      REAL (WP)              ::  HAR2,EN_P2,VF2
!
      INTEGER                ::  I
!
!  Initialisations
!
      DO I = 0, 6                                                   !
        AD(I) = ZERO                                                !
      END DO                                                        !
!
      HAR2 = HARTREE * HARTREE                                      !
!
      Q_SI = TWO * X * KF_SI                                        !
!
      EN_P2 = ENE_P_SI * ENE_P_SI * Q_SI / HAR2                     !
!
      VF2 = VF_AU * VF_AU                                           !
!
!  Coefficients in AU
!
      AD(0) = EN_P2                                                  !
      AD(2) = 0.75E0_WP * VF2                                       !
!
      END SUBROUTINE RPA_MOD_DP_2D
!
!=======================================================================
!
      SUBROUTINE RAJAGOP_DP_2D(X,RS,T,AD)
!
!  This subroutine computes the coefficients of the plasmon dispersion
!    within the Rajagopal approach in 2D systems according to:
!
!           ENE_Q^2 = AD(0) + AD(1)*Q   + AD(2)*Q^2 + AD(3)*Q^3 +
!                             AD(4)*Q^4 + AD(5)*Q^5 + AD(6)*Q^6
!
!                   = ALPHA2 + BETA2 * Q^2 + GAMMA2 * Q^4 + DELTA^2 Q^6
!
!    where Q = q/ k_F
!
!     --> This is the real (q, hbar omega_q) case (no damping)
!
!  Warnings:  i) all input parameters are in SI units
!            ii) in the literature, beta^2 and gamma^2 are given
!                in atomic units (AU). Here, we work in SI.
!                We recall:
!
!                       ENE    : J             --> energy
!                       Q      : 1 / M         --> wave vector
!                       V      : M / S         --> speed
!                       OMEGA  : 1 / S         --> angular momentum
!                       HBAR   : J S           --> Planck constant / 2 pi
!
!                In order to obtain ENE_Q^2 in J^2, a dimension analysis
!                  shows that:
!
!     BETA2_AU  = COEF * V_F^2              --> BETA2_SI  = HBAR^2 * BETA2_AU
!     GAMMA2_AU = 1/4                       --> GAMMA2_SI = HBAR^2 * GAMMA2_AU / M^2
!     GAMMA2_AU = COEF * V_F^4 / OMEGA_P^2  --> GAMMA2_SI = HBAR^2 * GAMMA2_AU
!
!                Alternatively, we can work in atomic units and then change
!                  from Hartrees to Joules. This is what is coded here.
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!  Output variables :
!
!       * AD(0:6)  : coefficients of the squared dispersion in AU
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 17 Nov 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,TWO,THREE,FOUR,NINE,TEN
      USE PI_ETC,           ONLY : PI
      USE SQUARE_ROOTS,     ONLY : SQR2
      USE CONSTANTS_P1,     ONLY : M_E
      USE CONSTANTS_P2,     ONLY : HARTREE
      USE FERMI_SI,         ONLY : KF_SI
      USE FERMI_AU,         ONLY : VF_AU
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)  ::  X,RS,T
      REAL (WP), INTENT(OUT) ::  AD(0:6)
!
      REAL (WP)              ::  Q_SI
      REAL (WP)              ::  HAR2,EN_P2,VF2
!
      INTEGER                ::  I
!
!  Initialisations
!
      DO I = 0, 6                                                   !
        AD(I) = ZERO                                                !
      END DO                                                        !
!
      HAR2 = HARTREE * HARTREE                                      !
!
      Q_SI = TWO * X * KF_SI                                        !
!
      EN_P2 = ENE_P_SI * ENE_P_SI * Q_SI / HAR2                     !
!
      VF2 = VF_AU * VF_AU                                           !
!
!  Coefficients in AU
!
      AD(0) = EN_P2                                                 !
      AD(2) = EN_P2 * ( THREE * SQR2 / (FOUR * RS) ) * (          & !
                   ONE - TEN * RS / (NINE * SQR2 * PI) )            !
!
      END SUBROUTINE RAJAGOP_DP_2D
!
!=======================================================================
!
      SUBROUTINE RP2_MOD_DP_3D(RS,T,AD)
!
!  This subroutine computes the coefficients of the plasmon dispersion
!    within the RPA approach in 3D systems according to:
!
!           ENE_Q^2 = AD(0) + AD(1)*Q   + AD(2)*Q^2 + AD(3)*Q^3 +
!                             AD(4)*Q^4 + AD(5)*Q^5 + AD(6)*Q^6
!
!                   = ALPHA2 + BETA2 * Q^2 + GAMMA2 * Q^4 + DELTA^2 Q^6
!
!    where Q = q/ k_F
!
!
!  References: (1) Ll. Serra, F. Garcias, M. Barranco, N. Barberan
!                      and J. Navarro, Phys. Rev. B 44, 1492-1498 (1991)
!
!
!     --> This is the real (q, hbar omega_q) case (no damping)
!
!  Warnings:  i) all input parameters are in SI units
!            ii) in the literature, beta^2 and gamma^2 are given
!                in atomic units (AU). Here, we work in SI.
!                We recall:
!
!                       ENE    : J             --> energy
!                       Q      : 1 / M         --> wave vector
!                       V      : M / S         --> speed
!                       OMEGA  : 1 / S         --> angular momentum
!                       HBAR   : J S           --> Planck constant / 2 pi
!
!                In order to obtain ENE_Q^2 in J^2, a dimension analysis
!                  shows that:
!
!     BETA2_AU  = COEF * V_F^2              --> BETA2_SI  = HBAR^2 * BETA2_AU
!     GAMMA2_AU = 1/4                       --> GAMMA2_SI = HBAR^2 * GAMMA2_AU / M^2
!     GAMMA2_AU = COEF * V_F^4 / OMEGA_P^2  --> GAMMA2_SI = HBAR^2 * GAMMA2_AU
!
!                Alternatively, we can work in atomic units and then change
!                  from Hartrees to Joules. This is what is coded here.
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!  Output variables :
!
!       * AD(0:6)  : coefficients of the squared dispersion in AU
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 28 Jul 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,FOURTH
      USE CONSTANTS_P2,     ONLY : HARTREE
      USE FERMI_AU,         ONLY : VF_AU
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)  ::  RS,T
      REAL (WP), INTENT(OUT) ::  AD(0:6)
!
      REAL (WP)              ::  HAR2,VF2,VF4
      REAL (WP)              ::  EN_P2
!
      INTEGER                ::  I
!
!  Initialisations
!
      DO I = 0, 6                                                   !
        AD(I) = ZERO                                                !
      END DO                                                        !
!
      HAR2 = HARTREE * HARTREE                                      !
!
      VF2 = VF_AU * VF_AU                                           !
      VF4 = VF2 * VF2                                               !
!
      EN_P2 = ENE_P_SI * ENE_P_SI / HAR2                            !
!
!  Coefficients in AU
!
      AD(0) = EN_P2                                                 !
      AD(2) = 0.60E0_WP * VF2                                       !
      AD(4) = FOURTH + 12.0E0_WP * VF4 / (175.0E0_WP * EN_P2)       !
!
      END SUBROUTINE RP2_MOD_DP_3D
!
!=======================================================================
!
      SUBROUTINE SGBBN_M_DP_3D(R_S,T,AD)
!
!  This subroutine computes the coefficients of the plasmon dispersion
!    within the SGBBN_M_DISP_3D approach in 3D systems according to:
!
!           ENE_Q^2 = AD(0) + AD(1)*Q   + AD(2)*Q^2 + AD(3)*Q^3 +
!                             AD(4)*Q^4 + AD(5)*Q^5 + AD(6)*Q^6
!
!                   = ALPHA2 + BETA2 * Q^2 + GAMMA2 * Q^4 + DELTA^2 Q^6
!
!    where Q = q/ k_F
!
!
!  References: (1) Ll. Serra, F. Garcias, M. Barranco, N. Barberan
!                      and J. Navarro, Phys. Rev. B 44, 1492-1498 (1991)
!
!
!     --> This is the real (q, hbar omega_q) case (no damping)
!
!  Warnings:  i) all input parameters are in SI units
!            ii) in the literature, beta^2 and gamma^2 are given
!                in atomic units (AU). Here, we work in SI.
!                We recall:
!
!                       ENE    : J             --> energy
!                       Q      : 1 / M         --> wave vector
!                       V      : M / S         --> speed
!                       OMEGA  : 1 / S         --> angular momentum
!                       HBAR   : J S           --> Planck constant / 2 pi
!
!                In order to obtain ENE_Q^2 in J^2, a dimension analysis
!                  shows that:
!
!     BETA2_AU  = COEF * V_F^2              --> BETA2_SI  = HBAR^2 * BETA2_AU
!     GAMMA2_AU = 1/4                       --> GAMMA2_SI = HBAR^2 * GAMMA2_AU / M^2
!     GAMMA2_AU = COEF * V_F^4 / OMEGA_P^2  --> GAMMA2_SI = HBAR^2 * GAMMA2_AU
!
!                Alternatively, we can work in atomic units and then change
!                  from Hartrees to Joules. This is what is coded here.
!
!  Input parameters:
!
!       * R_S      : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!  Output variables :
!
!       * AD(0:6)  : coefficients of the squared dispersion in AU
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  8 Dec 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,THREE,NINE,  &
                                   THIRD,FOURTH,NINTH
      USE CONSTANTS_P1,     ONLY : BOHR
      USE CONSTANTS_P2,     ONLY : HARTREE
      USE FERMI_AU,         ONLY : KF_AU,VF_AU
      USE PI_ETC,           ONLY : PI
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)   ::  R_S,T
      REAL (WP), INTENT(OUT)  ::  AD(0:6)
!
      REAL (WP)               ::  HAR2,VF2,RS
      REAL (WP)               ::  DENOM,XI
      REAL (WP)               ::  BETA
!
      INTEGER                 ::  I
!
      BETA = NINTH                                                  ! Weizsacker coefficient
!
!  Initialisations
!
      DO I = 0, 6                                                   !
        AD(I) = ZERO                                                !
      END DO                                                        !
!
      HAR2  = HARTREE * HARTREE                                     !
      RS    = R_S                                                   ! rs in AU
      VF2   = VF_AU * VF_AU                                         ! v_F^2 in AU
      DENOM = NINE * (7.8E0_WP+RS)**3                               !
      XI    = 0.88E0_WP * RS * (7.80E0_WP + RS + RS) / DENOM        !
!
!  Coefficients in AU
!
      AD(0) = ENE_P_SI * ENE_P_SI / HAR2                            !
      AD(2) = (THIRD * VF2 - VF_AU / (THREE * PI) - XI)             !
      AD(4) = FOURTH * BETA                                         !
      AD(6) = ONE / (270.0E0_WP * VF2)                              !
!
      END SUBROUTINE SGBBN_M_DP_3D
!
!=======================================================================
!
      SUBROUTINE SUMRULE1_DP_3D(X,OM12)
!
!  This subroutine computes the coefficients of the plasmon dispersion
!    within the 3rd-frequency sum rule approach in 3D systems.
!
!     --> This is the real (q, hbar omega_q) case (no damping)
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!
!  Output variables :
!
!       * OM12     : squared dispersion frequency in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Nov 2020
!
!
      USE CONSTANTS_P1,     ONLY : H_BAR
      USE LOSS_MOMENTS
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)  ::  X
      REAL (WP), INTENT(OUT) ::  OM12
!
      REAL (WP)              ::  C0,C2,C4
!
!  Computing the moments of the loss function
!
      CALL LOSS_MOMENTS_AN(X,C0,C2,C4)                              !
!
!  Squared energy in SI
!
      OM12 = H_BAR * H_BAR * C2 / C0                                !
!
      END SUBROUTINE SUMRULE1_DP_3D
!
!=======================================================================
!
      SUBROUTINE SUMRULE3_DP_3D(X,OM22)
!
!  This subroutine computes the coefficients of the plasmon dispersion
!    within the 3rd-frequency sum rule approach in 3D systems.
!
!     --> This is the real (q, hbar omega_q) case (no damping)
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!
!  Output variables :
!
!       * OM22     : squared dispersion frequency in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Nov 2020
!
!
      USE CONSTANTS_P1,     ONLY : H_BAR
      USE LOSS_MOMENTS
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)  ::  X
      REAL (WP), INTENT(OUT) ::  OM22
!
      REAL (WP)              ::  C0,C2,C4
!
!  Computing the moments of the loss function
!
      CALL LOSS_MOMENTS_AN(X,C0,C2,C4)                              !
!
!  Squared energy in SI
!
      OM22 = H_BAR * H_BAR * C4 / C2                                !
!
      END SUBROUTINE SUMRULE3_DP_3D
!
!=======================================================================
!
      SUBROUTINE PLASMON_DISP_3D_2(X,RS,T,PL_DISP,ENE_P_Q)
!
!  This subroutine computes the plasmon dispersion omega(q)
!    in the form
!
!  Reference: (1) S. Ichimaru, Rev. Mod. Phys. 54, 1018-1059 (1982)
!
!
!                                                  2
!                                         (   q   )
!    omega(q) = omega_p + 2 alpha omega_F ( ----- )
!                                         (  k_F  )
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature                                 in SI
!       * PL_DISP  : method used to compute the dispersion (3D)
!                      PL_DISP = 'RP1_MOD' RPA model up to q^2
!                      PL_DISP = 'AL0_MOD' gamma_0   limit
!                      PL_DISP = 'ALI_MOD' gamma_inf limit
!                      PL_DISP = 'NOP_MOD' Nozières-Pines model
!                      PL_DISP = 'UTI_MOD' Utsumi-Ichimaru model
!                      PL_DISP = 'TWA_MOD' Toigo-Woodruff model
!
!
!  Output variables :
!
!       * ENE_P_Q  : plasmon energy at q in J
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  7 Dec 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,FOURTH,FIFTH
      USE SQUARE_ROOTS,     ONLY : SQR2
      USE CONSTANTS_P1,     ONLY : H_BAR
      USE FERMI_SI,         ONLY : EF_SI
      USE PLASMON_ENE_SI
      USE UTIC_PARAMETERS,  ONLY : UTIC_PARAM
      USE ASYMPT,           ONLY : G0,GI
      USE EXT_FUNCTIONS,    ONLY : PDF                              ! Plasma dispersion function Z(x)
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 7)    ::  PL_DISP
!
      REAL (WP), INTENT(IN)  ::  X,RS,T
      REAL (WP), INTENT(OUT) ::  ENE_P_Q
!
      REAL (WP)              ::  Y
      REAL (WP)              ::  OMP,OMF,RPF,RFP
      REAL (WP)              ::  AL_RPA,ALPHA
      REAL (WP)              ::  OMQ,OM0
      REAL (WP)              ::  R1,R2,W
!
      Y = X + X                                                     ! Y = q / k_F
!
      OMP = ENE_P_SI / H_BAR                                        ! omega_p in SI
      OMF = EF_SI / H_BAR                                           ! omega_F in SI
      RPF = OMP / OMF                                               !
      RFP = OMF / OMP                                               !
!
!  Computing alpha
!
      AL_RPA = THREE * FIFTH * RFP                                  !
!
      IF(PL_DISP == 'RP1_MOD') THEN                                 !
        ALPHA = AL_RPA                                              ! ref. (1) eq. (3.97)
      ELSE IF(PL_DISP == 'AL0_MOD') THEN                            !
        ALPHA = AL_RPA - FOURTH * RPF * G0                          ! ref. (1) eq. (3.98)
      ELSE IF(PL_DISP == 'ALI_MOD') THEN                            !
        ALPHA = AL_RPA - FOURTH * RPF * GI                          ! ref. (1) eq. (3.100)
      ELSE IF(PL_DISP == 'NOP_MOD') THEN                            !
        ALPHA = AL_RPA - THREE * RPF / 80.0E0_WP                    ! ref. (1) eq. (3.101)
      ELSE IF(PL_DISP == 'UTI_MOD') THEN                            !
!
!  Computing the Utsumi-Ichimaru parameters Omega(q) and Omega(0)
!
        CALL UTIC_PARAM(X,RS,T,OMQ,OM0)                             !
!
        R1 = OMP / OM0                                              ! argument of W
        R2 = R1 / SQR2                                              ! argument of Z
!
!  Computing the W function
!
        W = ONE + R1 * REAL(PDF(R2),KIND = WP)                      !
!
        ALPHA = AL_RPA - FOURTH * RPF * (GI * (G0 - GI) * W)        ! ref. (1) eq. (3.96)
      ELSE IF(PL_DISP == 'TWA_MOD') THEN                            !
        ALPHA = AL_RPA - RPF / 16.0E0_WP                            ! ref. (1) eq. (3.99)
      END IF                                                        !
!
      ENE_P_Q = H_BAR * (OMP + TWO * ALPHA * OMF * Y * Y)           ! ref. (1) eq. (3.95)
!
      END SUBROUTINE PLASMON_DISP_3D_2
!
END MODULE PLASMON_DISP_REAL
!
!=======================================================================
!
MODULE PLASMON_DISP_CPLX
!
!  This module computes analytical plasmon dispersion with damping
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE PLASMON_DISP_D_2D(PL_DISP,Q,RS,T,MASS_E,TAU,  &
                                   ENE_Q,Q_STAR)
!
!  This subroutine computes the plasmon dispersion relation with damping
!    for different cases, in the 2D case
!
!  Warning: all input parameters are in SI units
!
!
!  Input parameters:
!
!       * PL_DISP   : method used to compute the dispersion
!                       PL_DISP = 'GIQUI_D' Jewsbury' model
!       * Q        : plasmon wave vector
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in Kelvin
!       * MASS_E   : mass enhancement m*/m
!       * TAU      : relaxation time (used for damping)  in SI
!
!
!  Output variables :
!
!       * ENE_Q    : plasmon energy at q in J * 1 / bar
!       * Q_STAR   : critical vector (2D) before which
!                      no plasmon can be excited
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  5 Jun 2020
!
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 7)   ::  PL_DISP
!
      REAL (WP)             ::  RS,T,MASS_E,TAU
      REAL (WP)             ::  Q_STAR
!
      COMPLEX (WP)          ::  Q,ENE_Q
!
      IF(PL_DISP == 'GIQUI_D') THEN                                 !
!
        CALL GIQUI_D_DP_2D(Q,RS,T,MASS_E,TAU,ENE_Q,Q_STAR)          !
!
      END IF                                                        !
!
      END SUBROUTINE PLASMON_DISP_D_2D
!
!=======================================================================
!
      SUBROUTINE PLASMON_DISP_D_3D(PL_DISP,Q,RS,T,TAU,ENE_Q)
!
!  This subroutine computes the plasmon dispersion relation with damping
!    for different cases, in the 3D case
!
!  Warning: all input parameters are in SI units
!
!
!  Input parameters:
!
!       * PL_DISP   : method used to compute the dispersion
!                       PL_DISP = 'JEWSB_D' Jewsbury' model
!                       PL_DISP = 'HALEV_D' Halevi model
!       * Q        : plasmon wave vector
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in Kelvin
!       * TAU      : relaxation time (used for damping)  in SI
!
!
!  Output variables :
!
!       * ENE_Q    : plasmon energy at q in J * 1 / bar
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  5 Jun 2020
!
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 7)   ::  PL_DISP
!
      REAL (WP)             ::  RS,T,TAU
!
      COMPLEX (WP)          ::  Q,ENE_Q
!
      IF(PL_DISP == 'JEWSB_D') THEN                                 !
!
        CALL JEWSB_D_DP_3D(Q,RS,T,ENE_Q)                            !
!
      ELSEIF(PL_DISP == 'HALEV_D') THEN                             !
!
        CALL HALEV_D_DP_3D(Q,RS,TAU,ENE_Q)                          !
!
      ENDIF                                                         !
!
      END SUBROUTINE PLASMON_DISP_D_3D
!
!=======================================================================
!
      SUBROUTINE GIQUI_D_DP_2D(Q,RS,T,MASS_E,TAU,ENE_Q,Q_STAR)
!
!  This subroutine computes the plasmon dispersion relation with damping
!    for different cases, up to order 2 in q. The dispersion is written as
!
!           ENE_Q^2 = ALPHA2 + BETA2 * Q^2
!
!  --> This is the complex (q, hbar omega_q) case (includes damping)
!
!  Warning: all input parameters are in SI units
!
!
!  References: (1) L. Kong, B. Yan and X. Hu, Plasma Science and
!                     Technology 9, 519 (2007)
!              (2) G. F. Giuliani and J. J. Quinn, Phys. Rev. B 26,
!                     4421 (1982)
!
!
!  Input parameters:
!
!       * Q        : plasmon wave vector
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in Kelvin
!       * MASS_E   : mass enhancement m*/m
!       * TAU      : relaxation time (used for damping)  in SI
!
!
!  Output variables :
!
!       * ENE_Q    : plasmon energy at q in J * 1 / bar
!       * Q_STAR   : critical vector (2D) before which
!                      no plasmon can be excited
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 5 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,TWO
      USE COMPLEX_NUMBERS,  ONLY : ZEROC,IC
      USE CONSTANTS_P1,     ONLY : BOHR
      USE CONSTANTS_P2,     ONLY : HARTREE
      USE FERMI_SI,         ONLY : VF_SI
      USE PLASMON_ENE_SI
!
      USE DISP_COEF_COMP
!
      IMPLICIT NONE
!
      INTEGER               ::  BYPASS,I
!
      REAL (WP)             ::  RS,T
      REAL (WP)             ::  ENE_P2,VF2
      REAL (WP)             ::  MASS_E,TAU
      REAL (WP)             ::  K,Q_STAR
!
      COMPLEX (WP)          ::  Q,Q2,Q4,Q6,ENE_Q,ENE2,X
      COMPLEX (WP)          ::  ALPHA2,BETA2,GAMMA2
!
!  Initialisation
!
      Q_STAR=ZERO                                                   !
      BYPASS=0                                                      !
      ALPHA2=ZEROC                                                  !
      BETA2=ZEROC                                                   !
      GAMMA2=ZEROC                                                  !
!
      DO I=0,6                                                      !
        AC(I)=ZEROC                                                 !
      END DO                                                        !
!
      Q2=Q*Q                                                        !
      Q4=Q2*Q2                                                      !
      Q6=Q4*Q2                                                      !
!
      VF2=VF_SI*VF_SI                                               !
!
!  Initialization of ALPHA2 = (hbar omega_p)^2
!
      ENE_P2=ENE_P_SI*ENE_P_SI                                      !
      ALPHA2=ENE_P2                                                 !
!                                                                   ! mass-renormalized
      K=MASS_E*TWO/BOHR                                             ! Thomas-Fermi vector
      X=Q/K                                                         !
      ENE_Q=DCMPLX((ONE+X)/(TWO+X))*                             &  !
                   (CDSQRT(X*(TWO+X)*VF2*K*K -                   &  !
                    ONE/(TAU*TAU)) - IC/TAU)                        !
      BYPASS=1                                                      !
      Q_STAR=K*(DSQRT(ONE + ONE/(VF2*K*K*TAU*TAU)) - ONE)           !
!
!  Dispersion relation:
!
      IF(BYPASS == 0) THEN                                          !
!
          AC(1)=ALPHA2                                              !
          AC(2)=BETA2                                               !
          AC(4)=GAMMA2                                              !
!
      END IF                                                         !

      END SUBROUTINE GIQUI_D_DP_2D
!
!=======================================================================
!
      SUBROUTINE HALEV_D_DP_3D(Q,RS,TAU,ENE_Q)
!
!  This subroutine computes the plasmon dispersion relation with damping
!    for different cases, up to order 2 in q. The dispersion is written as
!
!           ENE_Q^2 = ALPHA2 + BETA2 * Q^2
!
!  --> This is the complex (q, hbar omega_q) case (includes damping)
!
!  Warning: all input parameters are in SI units
!
!
!  Input parameters:
!       * Q        : plasmon wave vector
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in Kelvin
!       * TAU      : relaxation time (used for damping)  in SI
!
!
!  Output variables :
!
!       * ENE_Q    : plasmon energy at q in J * 1 / bar
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  5 Jun 2020
!
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,THREE,FOUR,TEN, &
                                   HALF
      USE COMPLEX_NUMBERS,  ONLY : ZEROC,IC
      USE CONSTANTS_P1,     ONLY : BOHR,H_BAR
      USE FERMI_SI,         ONLY : VF_SI
      USE PLASMON_ENE_SI
!
      USE DISP_COEF_COMP
!
      IMPLICIT NONE
!
      INTEGER               ::  BYPASS,I
!
      REAL (WP)             ::  RS,T
      REAL (WP)             ::  ENE_P2,VF2
      REAL (WP)             ::  TAU
      REAL (WP)             ::  GAMMA,O_P2
!
      COMPLEX (WP)          ::  Q,Q2,ENE_Q,RAT
      COMPLEX (WP)          ::  ALPHA2,BETA2,GAMMA2,DELTA2
!
!  Initialisation
!
      BYPASS=0                                                      !
      ALPHA2=ZEROC                                                  !
      BETA2=ZEROC                                                   !
      GAMMA2=ZEROC                                                  !
      DELTA2=ZEROC                                                  !
!
      DO I=0,6                                                      !
        AC(I)=ZEROC                                                 !
      END DO                                                        !
!
      Q2=Q*Q                                                        !
!
      VF2=VF_SI*VF_SI                                               !
!
!  Initialization of ALPHA2 = (hbar omega_p)^2
!
      ENE_P2=ENE_P_SI*ENE_P_SI                                      !
      ALPHA2=ENE_P2                                                 !
!
      GAMMA=ONE/TAU                                                 !
      O_P2=ENE_P2/(H_BAR*H_BAR)                                     !
      RAT=Q2*VF2/O_P2                                               !
!
!
      ENE_Q=DSQRT(ENE_P2) + THREE*DSQRT(ENE_P2)*RAT/TEN  -       &  !
            IC*HALF*GAMMA*(ONE+FOUR*RAT/15.0E0_WP)                  !
!
!  Dispersion relation:
!
      IF(BYPASS == 0) THEN                                          !
          AC(0)=ALPHA2                                              !
          AC(2)=BETA2                                               !
          AC(4)=GAMMA2                                              !
          AC(6)=DELTA2                                              !
      END IF                                                        !
!
      END SUBROUTINE HALEV_D_DP_3D
!
!=======================================================================
!
      SUBROUTINE JEWSB_D_DP_3D(Q,RS,T,ENE_Q)
!
!  This subroutine computes the plasmon dispersion relation with damping
!    for different cases, up to order 2 in q. The dispersion is written as
!
!           ENE_Q^2 = ALPHA2 + BETA2 * Q^2
!
!  --> This is the complex (q, hbar omega_q) case (includes damping)
!
!  Warning: all input parameters are in SI units
!
!
!  Input parameters:
!
!       * Q        : complex plasmon wave vector
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in Kelvin
!
!
!  Output variables :
!
!       * ENE_Q    : plasmon energy at q in J * 1 / bar
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  5 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : TWO,FIVE,SIX,  &
                                   HALF,FOURTH
      USE COMPLEX_NUMBERS
      USE CONSTANTS_P1,     ONLY : BOHR
      USE FERMI_SI,         ONLY : EF_SI,KF_SI
      USE PLASMON_ENE_SI
!
      USE DISP_COEF_COMP
!
      IMPLICIT NONE
!
      INTEGER               ::  BYPASS,I
!
      REAL (WP)             ::  RS,T
      REAL (WP)             ::  G1,BG0,BG1
      REAL (WP)             ::  NUM,DENOM
      REAL (WP)             ::  KF2,EF2
!
      COMPLEX (WP)          ::  Q,Q2,Q4,Q6,ENE_Q,ENE2
      COMPLEX (WP)          ::  A,B,ALPHA2,BETA2,GAMMA2,DELTA2
!
!  Initialisation
!
      BYPASS=0                                                      !
      ALPHA2=ZEROC                                                  !
      BETA2=ZEROC                                                   !
      GAMMA2=ZEROC                                                  !
      DELTA2=ZEROC                                                  !
!
      DO I=0,6                                                      !
        AC(I)=ZEROC                                                 !
      END DO                                                        !
!
      Q2=Q*Q                                                        !
      Q4=Q2*Q2                                                      !
      Q6=Q4*Q2                                                      !
!
      KF2=KF_SI*KF_SI                                               !
      EF2=EF_SI*EF_SI                                               !
!
!  Initialization of ALPHA2 = (hbar omega_p)^2
!
      ALPHA2=ENE_P_SI*ENE_P_SI                                      !
!
      G1=FOURTH+0.00636E0_WP*RS                                     !
      BG0=0.033E0_WP*RS                                             !
      BG1=0.15E0_WP*DSQRT(RS)                                       !
      NUM=SIX*EF2                                                   !
      DENOM=FIVE*ENE_P_SI*ENE_P_SI                                  !
      A=(ONEC-HALF*IC*BG0)                                          !
      B=(NUM/DENOM-HALF*(G1+IC*BG1))/KF2                            !
!
      ALPHA2=DCMPLX(A*A)*ENE_P_SI*ENE_P_SI                          !
      BETA2=DCMPLX(TWO*A*B)                                         !
      GAMMA2=DCMPLX(B*B)                                            !
!
!
!  Dispersion relation:
!
      IF(BYPASS == 0) THEN                                          !
        AC(0)=ALPHA2                                                !
        AC(2)=BETA2                                                 !
        AC(4)=GAMMA2                                                !
        AC(6)=DELTA2                                                !
      END IF                                                        !
!
      ENE2=AC(0)+AC(1)*Q+AC(2)*Q2+AC(3)*Q*Q2+AC(4)*Q4+           &  !
                 AC(5)*Q*Q4+AC(6)*Q6                                !
!
      ENE_Q=CDSQRT(ENE2)                                            !
!
      END SUBROUTINE JEWSB_D_DP_3D
!
END MODULE PLASMON_DISP_CPLX
