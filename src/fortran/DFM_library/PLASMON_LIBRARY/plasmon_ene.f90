!
!=======================================================================
!
MODULE PLASMON_ENE_SI 
!
!  This modules defines the plasmon energy 
!
!              --> SI version  <--
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  ENE_P_SI
!
END MODULE PLASMON_ENE_SI
!
!=======================================================================
!
MODULE PLASMON_ENE_EV 
!
!  This modules defines the plasmon energy 
!
!              --> eV version  <--
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  ENE_P_EV
!
END MODULE PLASMON_ENE_EV
!
!=======================================================================
!
MODULE PLASMON_SCALE_P 
!
!  This modules defines the plasmon scale parameters 
!
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  NONID,DEGEN
!
END MODULE PLASMON_SCALE_P
!
!=======================================================================
!
MODULE PLASMON_ENE
!
!  This modules computes the plasmon energy 
!
!              --> SI version  <--
!
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE CALC_PLASMON_ENE
!
      USE MATERIAL_PROP,      ONLY : RS,DMN
      USE ENE_CHANGE,         ONLY : EV
!
      USE PLASMON_ENE_SI
      USE PLASMON_ENE_EV
!
      IMPLICIT NONE
!
      CALL PLASMON_ENERGY(DMN,RS,ENE_P_SI)                          !
!
      ENE_P_EV = ENE_P_SI / EV                                      !
!
      END SUBROUTINE CALC_PLASMON_ENE
!
!=======================================================================
!
      SUBROUTINE PLASMON_ENERGY(DMN,RS,ENE_P_SI)
!
!  This subroutine calculates the plasmon energy at q = 0 
!    for all dimensionalities
!
!  Important note: In 2D and 1D, the plasmon energy is q-dependent. We 
!    have removed here this q-dependency. In reality, OMEGA_P is zero 
!    in 2D and 1D for q = 0
!
!  Input parameters:
!
!       * DMN      : problem dimension
!                       DIM = '3D'
!                       DIM = '2D'
!                       DIM = '1D'
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!  Output variables :
!
!       * ENE_P_SI : plasmon energy in J
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 17 Nov 2020
!
!
      USE ACCURACY_REAL
      USE REAL_NUMBERS,     ONLY : HALF
      USE CONSTANTS_P1,     ONLY : H_BAR,M_E,E,EPS_0
      USE FERMI_SI,         ONLY : KF_SI
      USE PI_ETC,           ONLY : PI_INV
      USE UTILITIES_1,      ONLY : RS_TO_N0
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  ENE_P_SI
      REAL (WP)             ::  COEF,N0
!
      REAL (WP)             ::  SQRT
!
      COEF = E * E / (M_E * EPS_0)                                  !
!
      N0 = RS_TO_N0(DMN,RS)                                         !
! 
      IF(DMN == '3D') THEN                                          !
!
!..........  3D case  ..........
!
        ENE_P_SI = H_BAR * SQRT(COEF * N0)                          !
!
      ELSE IF(DMN == '2D') THEN                                     !
!
!..........  2D case  ..........
!
        ENE_P_SI = H_BAR * SQRT(HALF * COEF * N0)                   ! * sqrt(q)
!                                 
      ELSE IF(DMN == '1D') THEN                                     !
!
!..........  1D case  ..........
!
        ENE_P_SI = H_BAR * SQRT(HALF * HALF * COEF * PI_INV * N0)   ! * q * sqrt(Vc)
!
      END IF                                                        !
!
      END SUBROUTINE PLASMON_ENERGY    
!
END MODULE PLASMON_ENE
!
!=======================================================================
!
MODULE PLASMA_SCALE
!
      USE ACCURACY_REAL
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE CALC_PLASMA_SCALE
!
      USE MATERIAL_PROP,    ONLY : RS
      USE EXT_FIELDS,       ONLY : T
      USE PLASMA,           ONLY : ZION
!
      USE PLASMON_SCALE_P
!
      IMPLICIT NONE
!
      CALL PLASMON_SCALE(RS,T,ZION,NONID,DEGEN)                     !
!
      END SUBROUTINE CALC_PLASMA_SCALE
!
!=======================================================================
!
      SUBROUTINE PLASMON_SCALE(RS,T,ZION,NONID,DEGEN)
!
!  This subroutine calculates the plasmon scale parameters
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * ZION     : atomic number of the ions of the plasma
!
!  Output variables :
!
!       * NONID    : nonideality of plasmon --> dimensionless
!       * DEGEN    : plasmon degeneracy
!
!
!   Author :  D. Sébilleau
!
!                                        Last modified : 25 Sep 2020
!
!
!
      USE CONSTANTS_P1,     ONLY : BOHR,E,EPS_0,K_B
      USE FERMI_SI,         ONLY : EF_SI
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)  :: RS,T,ZION
      REAL (WP), INTENT(OUT) :: NONID,DEGEN
!
      NONID = EF_SI / (K_B * T)                                     !
      DEGEN = ZION * ZION * E * E / (EPS_0 * K_B * T * RS * BOHR)   !
!
      END SUBROUTINE PLASMON_SCALE  
!
END MODULE PLASMA_SCALE
