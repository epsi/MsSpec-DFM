!
!=======================================================================
!
MODULE SCREENING_VEC 
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE SCREENING_VECTOR(SC_TYPE,DMN,X,RS,T,KS_SI)
!
!  This subroutine computes the screening vector
!
!
!  Input parameters:
!
!       * SC_TYPE  : type of screeening
!                       SC_TYPE  = 'NO' no screening
!                       SC_TYPE  = 'DH' Debye-Hückel
!                       SC_TYPE  = 'TF' Thomas-Fermi
!       * DMN      : dimension of the system
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature in SI
!
!
!  Output parameters:
!
!       * KS_SI    : screening vector expressed in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Oct 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  SC_TYPE,DMN
!
      REAL (WP), INTENT(IN) ::  X,RS,T
      REAL (WP)             ::  KS_SI
!
      IF(SC_TYPE == 'NO') THEN                                      !
        KS_SI = ZERO                                                !
      ELSE IF(SC_TYPE == 'DH') THEN                                 !
        CALL DEBYE_VECTOR(DMN,T,RS,KS_SI)                           !
      ELSE IF(SC_TYPE == 'TF') THEN                                 !
        CALL THOMAS_FERMI_VECTOR(DMN,KS_SI)                         !
      END IF                                                        !
!
      END SUBROUTINE SCREENING_VECTOR
!
!=======================================================================
!
      SUBROUTINE THOMAS_FERMI_VECTOR(DMN,K_TF_SI)
!
!  This subroutine computes the Thomas-Fermi screening vector
!
!
!  Input parameters:
!
!       * DMN      : dimension of the system
!                       DMN  = '3D' 
!                       DMN  = '2D' 
!                       DMN  = '1D' 
!
!
!  Output parameters:
!
!       * K_TF_SI  : screening vector expressed in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  4 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : TWO,FOUR,EIGHT
      USE CONSTANTS_P1,     ONLY : BOHR
      USE PI_ETC,           ONLY : PI
      USE FERMI_SI,         ONLY : KF_SI
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP)             ::  K_TF_SI
      REAL (WP)             ::  KOEF
!
      REAL (WP)             ::  SQRT
!
      KOEF = BOHR                                                   !
!
      IF(DMN == '3D') THEN                                          !
        K_TF_SI = SQRT(FOUR * KF_SI / (PI * KOEF))                  !
      ELSE IF(DMN == '2D') THEN                                     !
        K_TF_SI = TWO / KOEF                                        !
      ELSE IF(DMN == '1D') THEN                                     !
        K_TF_SI = SQRT(EIGHT / (KOEF * KF_SI))                      !
      END IF                                                        !
!
      END SUBROUTINE THOMAS_FERMI_VECTOR  
!
!=======================================================================
!
      SUBROUTINE DEBYE_VECTOR(DMN,T,RS,KD_SI)
!
!  This subroutine computes the Debye screening vector
!
!
!  Input parameters:
!
!       * DMN      : dimension of the system
!                       DMN  = '3D' 
!                       DMN  = '2D' 
!                       DMN  = '1D' 
!       * T        : system temperature in SI
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!  Output parameters:
!
!       * KD_SI    : screening vector expressed in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 27 Jul 2020
!
!
      USE CONSTANTS_P1,     ONLY : EPS_0,E,K_B
      USE UTILITIES_1,      ONLY : RS_TO_N0
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP)             ::  T,RS
      REAL (WP)             ::  KD_SI
      REAL (WP)             ::  N0
!
      REAL (WP)             ::  SQRT
!
!  Computing the electron density
!
      N0 = RS_TO_N0(DMN,RS)                                        !
!
      KD_SI = SQRT(E * E * N0 / (EPS_0 * K_B * T))                 ! 
!
      END SUBROUTINE DEBYE_VECTOR  
!
END MODULE SCREENING_VEC
