!
!=======================================================================
!
MODULE SCREENING_VEC2 
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE SCREENING_VECTOR2(SC_TYPE,DMN,X,RS,T,KS_SI)
!
!  This subroutine computes the screening vector
!
!
!  Input parameters:
!
!       * SC_TYPE  : type of screeening
!                       SC_TYPE  = 'NO' no screening
!                       SC_TYPE  = 'IS' Tago-Utsumi-Ichimaru
!                       SC_TYPE  = 'KL' Kleinman
!                       SC_TYPE  = 'OC' one-component plasma
!                       SC_TYPE  = 'RP' RPA
!                       SC_TYPE  = 'ST' Streitenberger
!                       SC_TYPE  = 'UI' Utsumi-Ichimaru
!                       SC_TYPE  = 'YT' Yasuhara-Takada
!       * DMN      : dimension of the system
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature in SI
!
!
!  Output parameters:
!
!       * KS_SI    : screening vector expressed in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Oct 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  SC_TYPE,DMN
!
      INTEGER               ::  I_KL
!
      REAL (WP), INTENT(IN) ::  X,RS,T
      REAL (WP)             ::  KS_SI
!
      I_KL = 1                                                      ! Kleinman switch
!
      IF(SC_TYPE == 'NO') THEN                                      !
        KS_SI = ZERO                                                !
      ELSE IF(SC_TYPE == 'IS') THEN                                 !
        CALL TUI_VECTOR(DMN,T,RS,KS_SI)                             !
      ELSE IF(SC_TYPE == 'KL') THEN                                 !
        CALL KLEINMAN_VECTOR(DMN,X,I_KL,KS_SI)                      !
      ELSE IF(SC_TYPE == 'OC') THEN                                 !
        CALL OCP_VECTOR(DMN,T,KS_SI)                                !
      ELSE IF(SC_TYPE == 'RP') THEN                                 !
        CALL RPA_VECTOR(DMN,X,KS_SI)                                !
      ELSE IF(SC_TYPE == 'ST') THEN                                 !
        CALL STREITENBERGER_VECTOR(DMN,RS,KS_SI)                    !
      ELSE IF(SC_TYPE == 'UI') THEN                                 !
        CALL UTSUMI_ICHIMARU_VECTOR(DMN,RS,KS_SI)                   !
      ELSE IF(SC_TYPE == 'YT') THEN                                 !
        CALL YASUHARA_TAKADA_VECTOR(DMN,RS,T,KS_SI)                 !
      END IF                                                        !
!
      END SUBROUTINE SCREENING_VECTOR2
!
!=======================================================================
!
      SUBROUTINE UTSUMI_ICHIMARU_VECTOR(DMN,RS,K_WS_SI)
!
!  This subroutine computes the Utsumi-Ichimaru screening vector
!   used for computing the screening static structure factor
!
!  Reference: K. Utsumi and S. Ichimaru, Phys. Rev. B 22, 
!                5203-5212 (1980)
!
!
!  Input parameters:
!
!       * DMN      : dimension of the system
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!  Output parameters:
!
!       * K_WS_SI  : screening vector expressed in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  5 Oct 2020
!
!
      USE REAL_NUMBERS,        ONLY : ZERO,TWO,THREE,HALF
      USE PI_ETC,              ONLY : PI_INV
      USE FERMI_SI,            ONLY : KF_SI
      USE UTILITIES_1,         ONLY : ALFA
      USE ENERGIES,            ONLY : EC_TYPE
      USE CORRELATION_ENERGIES
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)    ::  DMN
!
      REAL (WP), INTENT(IN)  ::  RS
      REAL (WP), INTENT(OUT) ::  K_WS_SI
!
      REAL (WP)              ::  ALPHA,EC,D_EC_1,D_EC_2
!
      IF(DMN /= '3D') THEN                                          !
        K_WS_SI = ZERO                                              !
        GO TO 10                                                    !
      END IF                                                        !
!
      ALPHA = ALFA('3D')                                            ! 
!
!  Computing the correlation energy and its derivatives
!
      EC = EC_3D(EC_TYPE,1,RS,ZERO)                                 !
      CALL DERIVE_EC_3D(EC_TYPE,1,5,RS,ZERO,D_EC_1,D_EC_2)          !
!
      K_WS_SI = KF_SI * ( THREE * HALF * PI_INV - ALPHA *  (      & !
                          RS * RS * EC + TWO * RS * D_EC_1 )      & ! ref. (1) eq. (38)
                        )                                           !
!
  10  RETURN
!
      END SUBROUTINE UTSUMI_ICHIMARU_VECTOR
!
!=======================================================================
!
      SUBROUTINE KLEINMAN_VECTOR(DMN,X,I_KL,K_KL_SI)
!
!  This subroutine computes the Kleinman screening vector
!
!  Reference: (1) : P. R. Antoniewicz and L. Kleinman, Phys. Rev. B 2, 
!                      2808-2811 (1970)
!
!
!  Input parameters:
!
!       * DMN      : dimension of the system
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * I_KL     : switch
!                       I_KL  = 1 : for coefficient A
!                       I_KL  = 2 : for coefficient B
!
!
!
!  Output parameters:
!
!       * K_KL_SI  : screening vector expressed in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  5 Oct 2020
!
!
!
      USE REAL_NUMBERS,       ONLY : ZERO,ONE,TWO,HALF
      USE FERMI_SI,           ONLY : KF_SI
      USE LINDHARD_FUNCTION,  ONLY : LINDHARD_S
      USE SCREENING_VEC,      ONLY : THOMAS_FERMI_VECTOR
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)    ::  DMN
!
      INTEGER, INTENT(IN)    ::  I_KL
!
      REAL (WP), INTENT(IN)  ::  X
      REAL (WP), INTENT(OUT) ::  K_KL_SI
!
      REAL (WP)              ::  ALF,K2
      REAL (WP)              ::  Q_SI,K_TF_SI,LR,LI,Z2
!
      REAL (WP)              ::  EXP,SQRT
!
      IF(DMN /= '3D') THEN                                          !
        K_KL_SI = ZERO                                              !
        GO TO 10                                                    !
      END IF                                                        !
!
      Q_SI = TWO * X * KF_SI                                        ! q in SI
!
      ALF = HALF * (ONE + EXP(-X))                                  ! ref. (1) eq. (21)
!
      IF(I_KL == 1) THEN                                            !
        K2 = TWO * ALF * KF_SI * KF_SI                              !
      ELSE IF(I_KL == 2) THEN                                       !
        K2 = TWO * KF_SI * KF_SI * (ALF + TWO * X * X)              !
      END IF                                                        !
!
!  Computing the Thomas-Fermi screening vector
!
      CALL THOMAS_FERMI_VECTOR(DMN,K_TF_SI)                         !
!
      Z2 = K_TF_SI * K_TF_SI / (Q_SI * Q_SI)                        ! (q_{TF}/q)^2
!
!  Computing the RPA static dielectric function:
!
!    epsilon(q) = 1 + (q_{TF}/q)^2 * LR 
!
      CALL LINDHARD_S(X,DMN,LR,LI)                                  !
!
      K_KL_SI = SQRT(K2 * Z2 * LR)                                  ! ref. (1) eq. (20)
!
  10  RETURN
!
      END SUBROUTINE KLEINMAN_VECTOR
!
!=======================================================================
!
      SUBROUTINE STREITENBERGER_VECTOR(DMN,RS,K_ST_SI)
!
!  This subroutine computes the Streintenberger screening vector
!
!  Reference: (1) : P. Streintenberger, Phys. Stat. Sol. (b) 125, 
!                      681-692 (1984)
!
!
!  Input parameters:
!
!       * DMN      : dimension of the system
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!
!
!  Output parameters:
!
!       * K_ST_SI  : screening vector expressed in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  6 Oct 2020
!
!
!
      USE DIMENSION_CODE,     ONLY : NSIZE
      USE REAL_NUMBERS,       ONLY : ZERO,ONE,FOUR,HALF
      USE PI_ETC,             ONLY : PI 
      USE FERMI_SI,           ONLY : KF_SI
      USE FIND_ZERO,          ONLY : FIND_ZERO_FUNC
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)    ::  DMN
!
      INTEGER                ::  I
!
      INTEGER, PARAMETER     ::  N_MAX = 400         ! max. number of points
!
      REAL (WP), INTENT(IN)  ::  RS
      REAL (WP), INTENT(OUT) ::  K_ST_SI
!
      REAL (WP)              ::  CC,YY,ZEROF
      REAL (WP)              ::  Y(NSIZE),F(NSIZE)
!
      REAL (WP), PARAMETER   ::  Y_MAX = 16.0E0_WP   ! max. value of (K_ST_SI / KF_SI)^2
!
      REAL (WP)              ::  FLOAT,LOG,SQRT
!
      IF(DMN /= '3D') THEN                                          !
        K_ST_SI = ZERO                                              !
        GO TO 10                                                    !
      END IF                                                        !
!
      CC = PI * KF_SI                                               !
!
!  Constructing the function whose zero is seeked
!
!  Abscissa : Y = (K_ST_SI / KF_SI)^2
!
      DO I = 1, N_MAX                                               !
        Y(I) = FLOAT(I) * Y_MAX / FLOAT(N_MAX)                      !
        YY   = Y(I)
        F(I) = YY * (CC  - HALF + YY * LOG(ONE + FOUR / YY)) - FOUR ! ref. (1) eq. (59)
      END DO                                                        !
!
!  Finding the zero
!
      CALL FIND_ZERO_FUNC(Y,F,N_MAX,ZEROF)                          !
!
      K_ST_SI = KF_SI * SQRT(ZEROF)                                 !
!
  10  RETURN
!
      END SUBROUTINE STREITENBERGER_VECTOR
!
!=======================================================================
!
      SUBROUTINE YASUHARA_TAKADA_VECTOR(DMN,RS,T,K_YT_SI)
!
!  This subroutine computes the Yasuhara-Takada screening vector
!
!  Reference: (1) : H. Yasuhara and Y. Takada, Phys. Rev. B 43,
!                      7200-7211 (1991)
!
!
!  Input parameters:
!
!       * DMN      : dimension of the system
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature in SI
!
!
!
!  Output parameters:
!
!       * K_YT_SI  : screening vector expressed in SI
!
!
!  Note: We use here the fact that the isothermal compressibility
!           can be expressed as :
!
!                K_T^0       4
!           1 - -------  =  ----  alpha RS * gamma_0(RS)
!                 K_T        pi
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  6 Oct 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,TWO,FOUR
      USE PI_ETC,           ONLY : PI_INV
      USE GAMMA_ASYMPT,     ONLY : GAMMA_0_3D
      USE UTILITIES_1,      ONLY : ALFA
      USE SCREENING_VEC,    ONLY : THOMAS_FERMI_VECTOR
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)    ::  DMN
!
      REAL (WP), INTENT(IN)  ::  RS,T
      REAL (WP), INTENT(OUT) ::  K_YT_SI
!
      REAL (WP)              ::  G0,ALPHA
      REAL (WP)              ::  K0K,K_TF_SI
!
      REAL (WP)              ::  SQRT
!
      IF(DMN /= '3D') THEN                                          !
        K_YT_SI = ZERO                                              !
        GO TO 10                                                    !
      END IF                                                        !
!
      G0 = GAMMA_0_3D(RS,T)                                         ! gamma_0(RS)
!
      ALPHA = ALFA('3D')                                            !
!
      K0K = ONE - FOUR * PI_INV * ALPHA * RS * G0                   ! K_T^0 /  K_T
!
      CALL THOMAS_FERMI_VECTOR('3D',K_TF_SI)                        !
!
      K_YT_SI = K_TF_SI / SQRT(TWO - K0K)                           ! ref. (1) eq. (3.31)
!
  10  RETURN
!
      END SUBROUTINE YASUHARA_TAKADA_VECTOR
!
!=======================================================================
!
      SUBROUTINE OCP_VECTOR(DMN,T,K_OC_SI)
!
!  This subroutine computes the one-component plasma screening vector
!
!  Reference: (1) : S. V. Adamjan, I. M. Tkachenko, 
!                      J.L. Munoz-Cobo Gonzalez and G. Verdu Martin,
!                      Phys. Rev. E 48, 2067-2072 (1993)
!             (2) : N. G. Nilsson, Phys. Stat. Sol. (a) 19, K75 (1973)
!
!
!  Input parameters:
!
!       * DMN      : dimension of the system
!       * T        : system temperature in SI
!
!
!
!  Output parameters:
!
!       * K_YT_SI  : screening vector expressed in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Oct 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,HALF,THIRD,FOURTH
      USE PI_ETC,           ONLY : SQR_PI
!
      USE PLASMON_SCALE_P,  ONLY : NONID
      USE SPECIFIC_INT_7
      USE SCREENING_VEC,    ONLY : THOMAS_FERMI_VECTOR
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP), INTENT(IN) ::  T
      REAL (WP), INTENT(OUT)::  K_OC_SI
!
      REAL (WP)             ::  K_TF_SI
      REAL (WP)             ::  TH,U,V,ETA
      REAL (WP)             ::  G3O2,FM1O2
!
      REAL (WP)             ::  LOG,SQRT
! 
      TH = ONE / NONID                                              !
!      
!  Computing the Thomas-Fermi screening vector    
! 
      CALL THOMAS_FERMI_VECTOR(DMN,K_TF_SI)                         !
!
!  Calculation of eta = mu / k_B T from the 
!   relation:
!                 3       1
!     F  (eta) = ---  -----------
!      1/2        2     TH^{3/2}
!
!    with 
!
!     F  (eta) approximated from ref. (2)
!      1/2 
!
      G3O2 = HALF * SQR_PI                                          ! Gamma(3/2)
      U    = G3O2 * THREE * HALF / (TH*1.5E0_WP)                    ! ref. (1) eq. (3.5)
!
      V    = (THREE * SQR_PI * U * FOURTH)**(TWO * THIRD)           ! ref. (2) eq. (8)
      ETA  = LOG(U) / (ONE - U*U) + V -                           & !
             V / (0.24E0_WP + 1.08E0_WP * V)**2                     !
!
!  Computing the Fermi-Dirac integral F    (eta)
!                                      -1/2
!
      FM1O2 = FD(ETA,-HALF)                                         !
!
      K_OC_SI = K_TF_SI * SQRT(HALF * SQRT(TH) * FM1O2)             ! ref. (1) eq. (3.3)  
!
      END SUBROUTINE OCP_VECTOR
!
!=======================================================================
!
      SUBROUTINE RPA_VECTOR(DMN,X,K_RP_SI)
!
!  This subroutine computes the RPA screening vector
!
!
!  Input parameters:
!
!       * DMN      : dimension of the system
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!
!
!
!  Output parameters:
!
!       * K_RP_SI  : screening vector expressed in SI
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Oct 2020
!
!
      USE LINDHARD_FUNCTION, ONLY : LINDHARD_S
      USE SCREENING_VEC,     ONLY : THOMAS_FERMI_VECTOR
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP), INTENT(IN) ::  X
      REAL (WP), INTENT(OUT)::  K_RP_SI
!
      REAL (WP)             ::  K_TF_SI
      REAL (WP)             ::  LR,LI
!      
!  Computing the Thomas-Fermi screening vector    
! 
      CALL THOMAS_FERMI_VECTOR(DMN,K_TF_SI)                         !
!      
!  Computing the Lindhard static function   
! 
      CALL LINDHARD_S(X,DMN,LR,LI)                                  !
!
      K_RP_SI = K_TF_SI * LR                                        !
!
      END SUBROUTINE RPA_VECTOR
!
!=======================================================================
!
      SUBROUTINE TUI_VECTOR(DMN,RS,T,K_IS_SI)
!
!  This subroutine computes the Tago-Utsumi-Ichimaru screening vector
!
!  Reference: (1) : K. Tago, K. Utsumi and S. Ichimaru,
!                      Prog. Theor. Phys. 65, 54-65 (1981)
!
!
!  Input parameters:
!
!       * DMN      : dimension of the system
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : system temperature in SI
!
!
!
!  Output parameters:
!
!       * K_IS_SI  : screening vector expressed in SI
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Dec 2020
!
!
      USE REAL_NUMBERS,              ONLY : TWO
      USE CONSTANTS_P1,              ONLY : BOHR,K_B
      USE PLASMON_SCALE_P,           ONLY : NONID
      USE THERMODYNAMIC_PROPERTIES,  ONLY : U_IT_3D
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP), INTENT(IN) ::  RS,T
      REAL (WP), INTENT(OUT)::  K_IS_SI
!
      K_IS_SI = - TWO * U_IT_3D(T) / (RS * BOHR * NONID)            ! ref. (1) eq. (31)
!
      END SUBROUTINE TUI_VECTOR
!
END MODULE SCREENING_VEC2
 
