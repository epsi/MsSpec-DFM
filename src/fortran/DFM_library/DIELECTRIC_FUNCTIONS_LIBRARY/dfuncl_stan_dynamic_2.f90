!
!=======================================================================
!
MODULE DFUNCL_STAN_DYNAMIC_2 
!
      USE ACCURACY_REAL
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE DFUNCL_DYNAMIC_FROM_SQO(X,V,Z,RS,T,FLAG,EPSR,EPSI)
!
!  This subroutine computes the longitudinal dynamic 
!    dielectric functions in 3D from the dynamical structure factor
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * V        : dimensionless factor   --> V = hbar * omega / E_F
!       * Z        : dimensionless factor   --> Z = omega / omega_q 
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * FLAG     : current index of the omega loop calling this subroutine
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 26 Oct 2020
!
!
      USE REAL_NUMBERS,            ONLY : ONE,FOURTH
!
      USE E_GRID
!
      USE SF_VALUES
      USE STRUCTURE_FACTOR_DYNAMIC
      USE UTILITIES_3,             ONLY : SQO_TO_EPSI
      USE TRANSFORMS,              ONLY : KK
!
      IMPLICIT NONE
!
      INTEGER, INTENT(IN)    ::  FLAG
!
      INTEGER                ::  IE
!
      REAL (WP), INTENT(IN)  ::  X,V,Z,RS,T
      REAL (WP), INTENT(OUT) ::  EPSR,EPSI
!
      REAL (WP)             ::  E,ZZ,SQO,IEPS
      REAL (WP)             ::  EN(N_E),U_INP(N_E),U_OUT(N_E)
!
      DO IE = 1, N_E                                                !
!
        E  = E_MIN + FLOAT(IE - 1) * E_STEP                         !
        ZZ = FOURTH * E / (X * X)                                   ! ZZ = omega / omega_q
!
!  Computing the dynamic structure factor
!
        CALL STFACT_DYNAMIC(X,ZZ,RS,T,SQO_TYPE,SQ_TYPE,SQO)         ! 
!
!  Computing the imaginary part of epsilon
!
        CALL SQO_TO_EPSI(X,Z,T,RS,SQO,IEPS)                         !
!
        U_INP(IE) = IEPS                                            !
        EN(IE)    = E                                               !
!
      END DO                                                        !
!
!  Computing the real part of epsilon through Kramers-Kronig
!
      CALL KK('I2R',N_E,EN,U_INP,ONE,U_OUT)                         !
!
      EPSR = U_OUT(FLAG)                                            !
      EPSI = U_INP(FLAG)                                            !
!
      END SUBROUTINE DFUNCL_DYNAMIC_FROM_SQO
!
END MODULE DFUNCL_STAN_DYNAMIC_2
