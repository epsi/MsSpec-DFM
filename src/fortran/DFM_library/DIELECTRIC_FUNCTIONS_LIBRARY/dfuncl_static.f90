!
!=======================================================================
!
MODULE DFUNC_STATIC 
!
      USE ACCURACY_REAL
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE DFUNCL_STATIC(X,D_FUNCL,EPSR,EPSI)
!
!  This subroutine computes the longitudinal static 
!    dielectric functions 
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * D_FUNCL  : type of longitudinal dielectric function 
!                      D_FUNCL = 'LRPA' random phase approximation
!                      D_FUNCL = 'THFE' Thomas-Fermi
!
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 16 Jun 2020
!
!
      USE MATERIAL_PROP,    ONLY : DMN
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  D_FUNCL
!
      REAL (WP)             ::  X
      REAL (WP)             ::  EPSR,EPSI
!
      IF(D_FUNCL == 'LRPA') THEN                                    !
        CALL RPA1_EPS_S_LG(X,DMN,EPSR,EPSI)                         !
      ELSE IF(D_FUNCL == 'THFE') THEN                               !
        CALL THFE_EPS_S_LG(X,DMN,EPSR,EPSI)                         !
      END IF                                                        !
!
      END SUBROUTINE DFUNCL_STATIC
!
!=======================================================================
!
      SUBROUTINE RPA1_EPS_S_LG(X,DMN,EPSR,EPSI)
!
!  This subroutine computes the longitudinal static RPA dielectric function
!
!  References: (1) J. Solyom, "Fundamental of the Physics of Solids", Vol3, Chap. 29
!                        p. 61-138, Springer
!
!  Notation: hbar omega_q = hbar^2 q^2 / 2m
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)       
!       * DMN      : problem dimension
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!  In the RPA case, we have
!
!                  EPS(RPA) = 1 - V_C * Pi_0   Pi_0 : RPA polarisability
!
!    which we will write as 
!
!                  EPS(RPA) = 1 + Z * L        Z : (q_{TF}/q)^2 
!                                              L : Lindhard function
!
!    where q_{TF} is the Thomas-Fermi screening vector, and  EPS(TF) = 1 + Z
!    is the Thomas-Fermi dielectric function.
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 16 Jun 2020
!
!
      USE REAL_NUMBERS,       ONLY : ONE,TWO,FOUR,EIGHT
      USE CONSTANTS_P1,       ONLY : BOHR
      USE FERMI_SI,           ONLY : KF_SI
      USE PI_ETC,             ONLY : PI
      USE LINDHARD_FUNCTION,  ONLY : LINDHARD_S
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP)             ::  X
      REAL (WP)             ::  EPSR,EPSI
      REAL (WP)             ::  LR,LI,Q_SI,Z
!
      Q_SI=TWO*X*KF_SI                                              !
!
!  Coefficient Z: (q_{TF}/q)^2   --> dimension-dependent
!
      IF(DMN == '3D') THEN                                          ! 3D case
!
        Z=FOUR*KF_SI/(PI*BOHR*Q_SI*Q_SI)                            !                                    
!
      ELSEIF(DMN == '2D') THEN                                      ! 2D case 
!
        Z=FOUR/(BOHR*Q_SI*Q_SI)                                     !                                          
!
      ELSEIF(DMN == '1D') THEN                                      ! 1D case  
!
        Z=EIGHT/(BOHR*KF_SI*Q_SI*Q_SI)                              !                            
!
      END IF                                                        !
!
!  Calling the static Lindhard function
!
      CALL LINDHARD_S(X,DMN,LR,LI)                                  !
!
!  Calculation of the RPA dielectric function
!                                                                   !
      EPSR=ONE+Z*LR                                                 !
      EPSI=Z*LI                                                     ! EPS(RPA) =  1 + Z * L
!
      END SUBROUTINE RPA1_EPS_S_LG  
!
!=======================================================================
!
      SUBROUTINE THFE_EPS_S_LG(X,DMN,EPSR,EPSI)
!
!  This subroutine computes the longitudinal static RPA dielectric function
!
!  References: (1) J. Solyom, "Fundamental of the Physics of Solids", Vol3, Chap. 29
!                        p. 61-138, Springer
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)       
!       * DMN      : problem dimension
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 16 Jun 2020
!
!
      USE REAL_NUMBERS,       ONLY : ZERO,ONE,TWO,FOUR,EIGHT
      USE CONSTANTS_P1,       ONLY : BOHR
      USE FERMI_SI,           ONLY : KF_SI
      USE PI_ETC,             ONLY : PI
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  DMN
!
      REAL (WP)             ::  X
      REAL (WP)             ::  EPSR,EPSI
      REAL (WP)             ::  Q_SI,Z
!
      Q_SI=TWO*X*KF_SI                                              !
!
!  Coefficient Z: (q_{TF}/q)^2   --> dimension-dependent
!
      IF(DMN == '3D') THEN                                          ! 3D case
!
        Z=FOUR*KF_SI/(PI*BOHR*Q_SI*Q_SI)                            !                                           
!
      ELSEIF(DMN == '2D') THEN                                      ! 2D case 
!
        Z=FOUR/(BOHR*Q_SI*Q_SI)                                     !                                            
!
      ELSEIF(DMN == '1D') THEN                                      ! 1D case  
!
        Z=EIGHT/(BOHR*KF_SI*Q_SI*Q_SI)                              !                 
!
      ENDIF                                                         !
!
!  Calculation of the TF dielectric function
!  
      EPSR=ONE+Z                                                    !
      EPSI=ZERO                                                     ! EPS =  1 + Z 
!
      END SUBROUTINE THFE_EPS_S_LG  
!
!=======================================================================
!
      SUBROUTINE DFUNCL_STATIC_2D_M(X,KS,A,D_FUNCL,EPSR,EPSI)
!
!  This subroutine computes the longitudinal static 
!    dielectric functions in 2D in the presence of an external
!    magnetic field
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * KS       : screening wave vector                            in SI 
!       * A        : sqrt(mu_B * B) : magnitude of the magnetic field in SI
!       * D_FUNCL  : type of longitudinal dielectric function 
!                      D_FUNCL = 'LRPA' random phase approximation
!
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 26 Feb 2020
!
!
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  D_FUNCL
!
      REAL (WP)             ::  X,KS,A
      REAL (WP)             ::  EPSR,EPSI
!
      IF(D_FUNCL == 'LRPA') THEN                                    !
        CALL  RPA2_EPS_S_LG_2D(X,KS,A,EPSR,EPSI)                    !
      END IF                                                        !
!
      END SUBROUTINE DFUNCL_STATIC_2D_M
!
!=======================================================================
!
      SUBROUTINE RPA2_EPS_S_LG_2D(X,KS,A,EPSR,EPSI)
!
!  This subroutine computes the longitudinal static 2D RPA 
!    dielectric function in the presence of a magnetic field 
!    for an integer filling factor of 1
!
!  References: (1) G. F. Giuliani and G. Vignale, "Quantum Theory of 
!                     the Electron Liquid", (Cambridge, 2005) p. 579
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)       
!       * KS       : screening wave vector                            in SI 
!       * A        : sqrt(mu_B * B) : magnitude of the magnetic field in SI
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 23 Jun 2020
!
!
      USE REAL_NUMBERS,       ONLY : ZERO,ONE,TWO,HALF
      USE CONSTANTS_P1,       ONLY : H_BAR,M_E,E,EPS_0
      USE FERMI_SI,           ONLY : KF_SI
      USE PI_ETC,             ONLY : PI_INV
      USE SQUARE_ROOTS,       ONLY : SQR2
      USE EULER_CONST,        ONLY : EUMAS
      USE EXT_FUNCTIONS,      ONLY : DEI
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X,KS,A
      REAL (WP)             ::  Q_SI,Q2,KS2,COEF,V_C
      REAL (WP)             ::  HOC,QL2,L2O
      REAL (WP)             ::  EPSR,EPSI
!
      REAL (WP)             ::  DSQRT,DLOG,DEXP
!
      COEF=E*E/EPS_0                                                !
!
      Q_SI=TWO*X*KF_SI                                              !
      Q2=Q_SI*Q_SI                                                  !
      KS2=KS*KS                                                     !
!
      HOC=SQR2*A                                                    ! hbar * omega_c
      QL2=HALF*H_BAR*H_BAR*Q_SI*Q_SI/(M_E*HOC)                      ! q^2 l^2 / 2
      L2O=H_BAR*H_BAR/M_E                                           ! l^2 hbar omega_c
      V_C=HALF*COEF/DSQRT(Q2+KS2)                                   ! 2D Coulomb pot.
!
      EPSR=ONE+V_C*PI_INV*(DEI(QL2)-DLOG(QL2)-EUMAS)*DEXP(-QL2)/L2O !
      EPSI=ZERO                                                     !
!
      END SUBROUTINE RPA2_EPS_S_LG_2D  
!
END MODULE DFUNC_STATIC
