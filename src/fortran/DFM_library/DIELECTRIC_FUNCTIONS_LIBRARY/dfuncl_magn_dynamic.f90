!
!=======================================================================
!
MODULE DFUNCL_MAGN_DYNAMIC 
!
      USE ACCURACY_REAL
!
!
CONTAINS
!
!
!=======================================================================
!
!    Longitudinal Dielectric Functions with External Magnetic Field
!
!=======================================================================
!
!
      SUBROUTINE DFUNCL_DYNAMIC_M(X,Z,KS,A,NU,D_FUNCL,EPSR,EPSI)
!
!  This subroutine computes the longitudinal dynamic 
!    dielectric functions in the presence of an external
!    magnetic field
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : dimensionless factor   --> Z = omega / omega_q 
!       * KS       : screening wave vector                            in SI 
!       * A        : sqrt(mu_B * B) : magnitude of the magnetic field in SI
!       * NU       : dimensionless filling factor
!       * D_FUNCL  : type of longitudinal dielectric function (2D)
!
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 18 Jun 2020
!
!
      USE MATERIAL_PROP,    ONLY : DMN
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  D_FUNCL
!
      REAL (WP)             ::  X,Z,KS,A,NU
      REAL (WP)             ::  EPSR,EPSI
!                                   
      IF(DMN == '3D') THEN                                          !
        CONTINUE                                                    !
      ELSE IF(DMN == '2D') THEN                                     !
        CALL DFUNCL_DYNAMIC_2D_M(X,Z,KS,A,NU,D_FUNCL,EPSR,EPSI)     !
      ELSE IF(DMN == '1D') THEN                                     !
        CONTINUE                                                    !
      END IF                                                        !
!
      END SUBROUTINE DFUNCL_DYNAMIC_M
!
!=======================================================================
!
!    1) 3D case
!
!=======================================================================
!
!    2) 2D case
!
!=======================================================================
!
      SUBROUTINE DFUNCL_DYNAMIC_2D_M(X,Z,KS,A,NU,D_FUNCL,EPSR,EPSI)
!
!  This subroutine computes the longitudinal dynamic 
!    dielectric functions in 2D in the presence of an external
!    magnetic field
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : dimensionless factor   --> Z = omega / omega_q 
!       * KS       : screening wave vector                            in SI 
!       * A        : sqrt(mu_B * B) : magnitude of the magnetic field in SI
!       * NU       : dimensionless filling factor
!       * D_FUNCL  : type of longitudinal dielectric function (2D)
!                      D_FUNCL = 'RPA3' random phase approximation
!
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 18 Jun 2020
!
!
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  D_FUNCL
!
      REAL (WP)             ::  X,Z,KS,A,NU
      REAL (WP)             ::  EPSR,EPSI
!
      IF(D_FUNCL == 'RPA3') THEN                                    !
        CALL RPA3_EPS_D_LG_2D(X,Z,KS,A,NU,EPSR,EPSI)                !
      END IF                                                        !
!
      END SUBROUTINE DFUNCL_DYNAMIC_2D_M
!
!=======================================================================
!
      SUBROUTINE RPA3_EPS_D_LG_2D(X,Z,KS,A,NU,EPSR,EPSI)
!
!  This subroutine computes the longitudinal 2D dynamical
!    RPA dielectric function with an external magnetic field
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)       
!       * Z        : dimensionless factor   --> Z = omega / omega_q 
!       * KS       : screening wave vector                            in SI 
!       * A        : sqrt(mu_B * B) : magnitude of the magnetic field in SI
!       * NU       : dimensionless filling factor
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 22 Jul 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,TWO,HALF
      USE COMPLEX_NUMBERS,  ONLY : ZEROC,IC
      USE CONSTANTS_P1,     ONLY : H_BAR,M_E,E,EPS_0
      USE FERMI_SI,         ONLY : KF_SI,VF_SI 
      USE PI_ETC,           ONLY : PI_INV
      USE SQUARE_ROOTS,     ONLY : SQR2
      USE EXT_FUNCTIONS,    ONLY : CONHYP
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X,Z,KS,A,NU,U
      REAL (WP)             ::  Q_SI,Q2,KS2,COEF,V_C
      REAL (WP)             ::  HOC,QL2,L2O,OM,KK
      REAL (WP)             ::  EPSR,EPSI
      REAL (WP)             ::  GLD(1000)
      REAL (WP)             ::  CONV,DELTA
      REAL (WP)             ::  SUMJ
      REAL (WP)             ::  LKJ,BIN
!
      INTEGER               ::  K,KMAX,J,JMIN,JMAX,I,NFAC
!
      COMPLEX (WP)          ::  EPS,SUMK,KSUM
      COMPLEX (WP)          ::  NUM1,NUM2
      COMPLEX (WP)          ::  AA,BB,ZZ
!
      CONV = 1.0E-6_WP                                              ! Convergence value
      DELTA= 1.0E-6_WP                                              ! imaginary part
!
      U=X*Z                                                         ! omega / (q * v_F)
!
      KMAX=100                                                      !
!
      COEF=E*E/EPS_0                                                !
!
      Q_SI=TWO*X*KF_SI                                              !
      Q2=Q_SI*Q_SI                                                  !
      KS2=KS*KS                                                     !
      OM=Q_SI*VF_SI*U                                               ! omega
!
      HOC=SQR2*A                                                    ! hbar * omega_c
      QL2=HALF*H_BAR*H_BAR*Q_SI*Q_SI/(M_E*HOC)                      ! q^2 l^2 / 2
      L2O=H_BAR*H_BAR/M_E                                           ! l^2 hbar omega_c
      V_C=HALF*COEF/DSQRT(Q2+KS2)                                   ! 2D Coulomb pot.
      KK=DEXP(-QL2)*PI_INV/L2O                                      !
!
!  Storage of the logarithms of the factorials
!
      NFAC=KMAX+INT(NU)                                             !
      GLD(1)=ZERO                                                   !
      DO I=2,NFAC                                                   !                                         
        J=I-1                                                       !                                             
        GLD(I)=GLD(J)+DLOG(DFLOAT(J))                               !
      END DO                                                        !
!
      SUMK=ZEROC                                                    !
      DO K=1,KMAX                                                   !
        NUM1=H_BAR*OM - DFLOAT(K)*HOC + IC*DELTA                    !
        NUM2=H_BAR*OM + DFLOAT(K)*HOC + IC*DELTA                    !
        AA=DCMPLX(-DFLOAT(K))                                       !
        JMIN=MAX(0,INT(NU)-K)                                       !
        JMAX=INT(NU)                                                !
        SUMJ=ZERO                                                   !
        DO J=JMIN,JMAX                                              !
!
!  Computing the generalized Laguerre polynomials from 
!    the confluent hypergeometric function:
!
!     L(n,k,x) = (n+k) * 1F1(-n,k+1;x)
!                ( n )                          
!
          BIN=DEXP(GLD(K+J+1)/(GLD(K+1)*GLD(J+1)))                  ! binomial coefficient
          BB=DCMPLX(DFLOAT(J)+ONE)                                  !
          ZZ=DCMPLX(QL2)                                            !
          LKJ=BIN*DREAL(CONHYP(AA,BB,ZZ,0,10))                      !
!
          SUMJ=SUMJ+DEXP(GLD(J+1)-GLD(J+K+1))*LKJ*LKJ               !
        END DO                                                      !
        KSUM=SUMJ*(ONE/NUM1 - ONE/NUM2)*(QL2**K)                    !
        SUMK=SUMK+KSUM                                              !
        IF(CDABS(KSUM).LT.CONV) GO TO 10                            !
      END DO                                                        !
!
  10  EPS=ONE-V_C*KK*SUMK                                           !
!
      EPSR=DREAL(EPS)                                               !
      EPSI=DIMAG(EPS)                                               !
!
      END SUBROUTINE RPA3_EPS_D_LG_2D  
!
!=======================================================================
!
!    1) 1D case
!
!=======================================================================
!
!
END MODULE DFUNCL_MAGN_DYNAMIC
