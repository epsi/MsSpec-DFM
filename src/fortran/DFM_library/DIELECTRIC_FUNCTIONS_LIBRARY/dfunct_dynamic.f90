!
!=======================================================================
!
MODULE DFUNCT_STAN_DYNAMIC 
!
      USE ACCURACY_REAL
!
!
CONTAINS
!
!
!=======================================================================
!
!     Transverse Dielectric Functions
!
!=======================================================================
!
      SUBROUTINE DFUNCT_DYNAMIC(X,Z,D_FUNCT,EPSR,EPSI)
!
!  This subroutine computes the transverse dynamic 
!    dielectric functions 
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : dimensionless factor   --> Z = omega / omega_q 
!       * D_FUNCT  : type of transverse dielectric function 
!
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 19 Jun 2020
!
!
      USE MATERIAL_PROP,    ONLY : DMN
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  D_FUNCT
!
      REAL (WP)             ::  X,Z,RS,T,TAU
      REAL (WP)             ::  EPSR,EPSI
!
      IF(DMN == '3D') THEN                                          !
        CALL DFUNCT_DYNAMIC_3D(X,Z,D_FUNCT,EPSR,EPSI)               !
      ELSE IF(DMN == '2D') THEN                                     !
        CALL DFUNCT_DYNAMIC_2D(X,Z,D_FUNCT,EPSR,EPSI)               !
      ELSE IF(DMN == '1D') THEN                                     !
        CONTINUE                                                    !
      END IF                                                        !
!
      END SUBROUTINE DFUNCT_DYNAMIC
!
!=======================================================================
!
!    1) 3D case
!
!=======================================================================
!
      SUBROUTINE DFUNCT_DYNAMIC_3D(X,Z,D_FUNCT,EPSR,EPSI)
!
!  This subroutine computes the transverse dynamic 
!    dielectric functions in 3D
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : dimensionless factor   --> Z = omega / omega_q 
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * D_FUNCT  : type of transverse dielectric function (3D)
!                      D_FUNCT = 'RPA1' random phase approximation
!                      D_FUNCT = 'RPA2' random phase approximation
!                      D_FUNCT = 'LVLA' linearized Vlasov
!                      D_FUNCT = 'MER1' Mermin
!                      D_FUNCT = 'BLTZ' Boltzmann
!
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 16 Jun 2020
!
!
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  D_FUNCT
!
      REAL (WP)             ::  X,Z,RS,T,TAU
      REAL (WP)             ::  EPSR,EPSI
!
      IF(D_FUNCT == 'RPA1') THEN                                    !
        CALL RPA1_EPS_D_TR_3D(X,Z,RS,EPSR,EPSI)                     !
      ELSE IF(D_FUNCT == 'RPA2') THEN                               !
        CALL RPA2_EPS_D_TR_3D(X,Z,RS,T,EPSR,EPSI)                   !
      ELSE IF(D_FUNCT == 'LVLA') THEN                               !
        CALL LVLA_EPS_D_TR_3D(X,Z,RS,T,EPSR,EPSI)                   !
      ELSE IF(D_FUNCT == 'MER1') THEN                               !
        CALL MER1_EPS_D_TR_3D(X,Z,RS,TAU,EPSR,EPSI)                 !
      ELSE IF(D_FUNCT == 'BLTZ') THEN                               !
        CALL BLTZ_EPS_D_TR_3D(X,Z,RS,TAU,EPSR,EPSI)                 !
      END IF                                                        !
!
      END SUBROUTINE DFUNCT_DYNAMIC_3D
!
!=======================================================================
!
      SUBROUTINE RPA1_EPS_D_TR_3D(X,Z,RS,EPSR,EPSI)
!
!  This subroutine computes the transvere 
!    RPA dielectric function EPS(q,omega) for 3D systems.
!
!  References: (1) Z. H. Levine and E. Cockayne, 
!                      J. Res. Natl. Inst. Stand. Technol. 113, 299-304 (2008)
!              (2) J. Solyom, "Fundamental of the Physics of Solids", 
!                      Vol3, Chap. 29, p. 117, Springer
!
!  Notation: hbar omega_q = hbar^2 q^2 / 2m
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)       
!       * Z        : dimensionless factor   --> Z = omega / omega_q 
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!  Intermediate parameters:
!
!       * U        : dimensionless factor   --> U = omega / q v_F       
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!  Note:
!
!  The connection between the two reference is obtained through the relation:
!
!                 ( k_{TF}/q )^2 = 3 U^2 ( omega_p/omega )^2
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 16 Jun 2020
!
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,TWO,THREE,EIGHT
      USE CONSTANTS_P1,     ONLY : H_BAR
      USE FERMI_SI,         ONLY : KF_SI,VF_SI  
      USE PI_ETC,           ONLY : PI
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X,Z,U,RS
      REAL (WP)             ::  EPSR,EPSI
      REAL (WP)             ::  X1,X2
      REAL (WP)             ::  Q_SI,O_SI,COEF
!
      U=X*Z                                                         ! omega / (q * v_F)
!
      X1=X-U                                                        !
      X2=X+U                                                        !
!
      Q_SI=TWO*X*KF_SI                                              ! q     in SI
      O_SI=U*Q_SI*VF_SI                                             ! omega in SI
!
      COEF=ENE_P_SI*ENE_P_SI/(H_BAR*H_BAR*O_SI*O_SI)                ! 
!
!  Real part
!
      EPSR=ONE - COEF * (                                         & !
                         THREE*(X*X + THREE*U*U + ONE)/EIGHT -    & !
                         THREE*(                                  & !
                                 (ONE-X2*X2)**2 *                 & !
                                  DLOG(DABS((X2+ONE)/(X2-ONE))) + & ! eq. (7) ref. 1
                                 (ONE-X1*X1)**2 *                 & ! eq. (29.6.87) ref. 2
                                  DLOG(DABS((X1+ONE)/(X1-ONE)))   & !
                               ) / (32.0E0_WP*X)                  & !
                        )                                           !
!
!  Imaginary part
!
      IF( U < DABS(ONE-X) ) THEN                                    !
!
        IF(X <= ONE) THEN                                           !
          EPSI=0.75E0_WP*PI*COEF*U*(ONE-U*U-X*X)                    ! eq. (6) ref. 1
        ELSE                                                        !
          EPSI=ZERO                                                 ! eq. (29.6.89) ref. 2
        END IF                                                      !
!
      ELSE IF( (DABS(ONE-X) <= U) .AND. (U <= (ONE+X)) ) THEN       ! 
!
        EPSI=THREE*PI*COEF * (ONE - (U-X)**2 )**2 / (32.0E0_WP*X)   ! eq. (6) ref. 1
!
      ELSE IF( (ONE+X) <= U ) THEN                                  !
!
        EPSI=ZERO                                                   !
!
      END IF                                                        !
!
      END SUBROUTINE RPA1_EPS_D_TR_3D  
!
!=======================================================================
!
      SUBROUTINE RPA2_EPS_D_TR_3D(X,Z,RS,T,EPSR,EPSI)
!
!  This subroutine computes the transverse temperature-dependent 
!    RPA dielectric function EPS(q,omega,T) for 3D systems.
!
!  References: (1) H. Reinholz et al, Contrib. Plasma Phys. 43, 3-10 (2003)
!
!  Notation: hbar omega_q = hbar^2 q^2 / 2m
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)       
!       * Z        : dimensionless factor   --> Y = omega / omega_q 
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)
!
!  Intermediate parameters:
!
!       * U        : dimensionless factor   --> U = omega / q v_F       
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 16 Jun 2020
!
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,TWO
      USE CONSTANTS_P1,     ONLY : H_BAR,M_E,K_B
      USE FERMI_SI,         ONLY : KF_SI,VF_SI  
      USE EXT_FUNCTIONS,    ONLY : DAWSON
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X,Z,U,RS,T
      REAL (WP)             ::  EPSR,EPSI
      REAL (WP)             ::  Q_SI,O_SI,XX
!
      U=X*Z                                                         ! omega / (q * v_F)
!
      Q_SI=TWO*X*KF_SI                                              ! q     in SI
      O_SI=U*Q_SI*VF_SI                                             ! omega in SI
!
      XX=O_SI*DSQRT(M_E/(TWO*K_B*T))/Q_SI                           !
!
      EPSR=ONE+ENE_P_SI*ENE_P_SI*DAWSON(XX)/(H_BAR*H_BAR*O_SI*O_SI) ! ref. (1) eq. (10) 
      EPSI=ZERO                                                     !
!
      END SUBROUTINE RPA2_EPS_D_TR_3D  
!
!=======================================================================
!
      SUBROUTINE LVLA_EPS_D_TR_3D(X,Z,RS,T,EPSR,EPSI)
!
!  This subroutine computes the transverse linearized Vlasov dynamical
!     dielectric function in 3D
!
!  References: (1) S. Ichimaru, "Statistical Plasma Physics - Vol1", 
!                               CRC Press (2004)
!
!  Notation: hbar omega_q = hbar q^2 / 2m
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)       
!       * Z        : dimensionless factor   --> Y = omega / omega_q 
!       * T        : temperature (SI)
!
!  Intermediate parameters:
!
!       * U        : dimensionless factor   --> U = omega / q v_F       
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 16 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,HALF
      USE CONSTANTS_P1,     ONLY : H_BAR,M_E,K_B
      USE FERMI_SI,         ONLY : KF_SI,VF_SI  
      USE EXT_FUNCTIONS,    ONLY : W
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP)             :: X,Z,RS,T,U,Y,Y2,V
      REAL (WP)             :: EPSR,EPSI
      REAL (WP)             :: ZZ,Q_SI,OM
      REAL (WP)             :: FR
!
      Y=X+X                                                         ! Y = q / k_F 
      Y2=Y*Y                                                        !
      U=X*Z                                                         ! omega / (q * v_F)
      V=Z*Y2                                                        ! omega / omega_{k_F}
      Q_SI=TWO*X*KF_SI                                              ! q
      OM=V*HALF*H_BAR*KF_SI*KF_SI/M_E                               ! omega
!
      ZZ=U*VF_SI/DSQRT(K_B*T/M_E)                                   ! argument of PDF W(zz)
      FR=(ENE_P_SI/(H_BAR*OM))**2                                   ! (omega_p/omega)^2
!
      EPSR=ONE - FR * (ONE - DREAL(W(ZZ)))                          ! ref. (2) eq. (4.76)
      EPSI=FR * (ONE - DIMAG(W(ZZ)))                                !
!
      END SUBROUTINE LVLA_EPS_D_TR_3D  
!
!=======================================================================
!
      SUBROUTINE MER1_EPS_D_TR_3D(X,Z,RS,TAU,EPSR,EPSI)
!
!  This subroutine computes the transverse Mermin dynamical
!     dielectric function in 3D
!
!  References: (1) P.-O. Chapuis et al, Phys. Rev. B 77, 035441 (2008)
!
!  Note: for TAU --> infinity, we should recover the RPA values
!
!  Notation: hbar omega_q = hbar q^2 / 2m
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)       
!       * Z        : dimensionless factor   --> Y = omega / omega_q 
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * TAU      : relaxation time (used for damping)  in SI
!
!  Intermediate parameters:
!
!       * U        : dimensionless factor   --> U = omega / q v_F       
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 16 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,FOUR,EIGHT,  &
                                   HALF
      USE COMPLEX_NUMBERS,  ONLY : IC
      USE CONSTANTS_P1,     ONLY : H_BAR
      USE FERMI_SI,         ONLY : KF_SI,VF_SI
      USE MULTILAYER,       ONLY : EPS_1
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP)             :: X,Z,U,RS,TAU
      REAL (WP)             :: EPSR,EPSI
      REAL (WP)             :: Q_SI,O_SI
!
      REAL*8 EPS_INF
!
      COMPLEX (WP)          :: UU,FT_U,FT_0,FL_U,FL_0
      COMPLEX (WP)          :: ZPU,ZMU
      COMPLEX (WP)          :: COEF,EPS
!
      U=X*Z                                                         ! omega / (q * v_F)
      Q_SI=TWO*X*KF_SI                                              ! q     in SI
      O_SI=U*Q_SI*VF_SI                                             ! omega in SI
!
      COEF=ENE_P_SI*ENE_P_SI/(H_BAR*H_BAR*(O_SI+IC/TAU))            !
!
      UU=(O_SI+IC/TAU)/(Q_SI*VF_SI)                                 !
      ZPU=X+UU                                                      !
      ZMU=X-UU                                                      !
!
      FL_0=HALF + (ONE-X*X)*DLOG(DABS((X+ONE)/(X-ONE)))/(FOUR*X)    ! ref (1) eq. (13)
      FL_U=HALF + (ONE-ZMU*ZMU)*CDLOG((ZMU+ONE)/(ZMU-ONE)) /     &  !
                   (EIGHT*X) +                                   &  !
                  (ONE-ZPU*ZPU)*CDLOG((ZPU+ONE)/(ZPU-ONE)) /     &  ! 
                   (EIGHT*X)                                        ! ref (1) eq. (11)
!
      FT_0=0.375E0_WP*(X*X+ONE) - 0.1875E0_WP*(ONE-X*X)*(ONE-X*X)* &! ref (1) eq. (14)
                               DLOG(DABS((X+ONE)/(X-ONE)))/X        !
      FT_U=0.375E0_WP*(X*X+THREE*UU*UU+ONE) -                    &  !
           0.09375E0_WP*(ONE-ZMU*ZMU)*(ONE-ZMU*ZMU)*             &  ! ref (1) eq. (12)
                      CDLOG((ZMU+ONE)/(ZMU-ONE))/X -             &  ! 
           0.09375E0_WP*(ONE-ZPU*ZPU)*(ONE-ZPU*ZMU)*             &  ! 
                      CDLOG((ZPU+ONE)/(ZPU-ONE))/X                  ! 
!
      EPS=EPS_1 - COEF*( O_SI*(FT_U-THREE*X*X*FL_U) +            &  !  
                             IC*(FT_0-THREE*X*X*FL_0)/TAU        &  ! ref (1) eq. (11) 
                         )                                          ! 
!
      EPSR=DREAL(EPS)                                               !
      EPSI=DIMAG(EPS)                                               !
!
      END SUBROUTINE MER1_EPS_D_TR_3D  
!
!=======================================================================
!
      SUBROUTINE BLTZ_EPS_D_TR_3D(X,Z,RS,TAU,EPSR,EPSI)
!
!  This subroutine computes the transverse Boltzmann dynamical
!     dielectric function in 3D
!
!  References: (1) R. Esquivel and V. B. Stetovoy, Phys. Rev. A 69, 062102 (2004)
!
!  Notation: hbar omega_q = hbar q^2 / 2m
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)       
!       * Z        : dimensionless factor   --> Z = omega / omega_q 
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * TAU      : relaxation time (used for damping)  in SI
!
!  Intermediate parameters:
!
!       * U        : dimensionless factor   --> U = omega / q v_F       
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 17 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,HALF
      USE COMPLEX_NUMBERS,  ONLY : IC
      USE CONSTANTS_P1,     ONLY : H_BAR
      USE FERMI_SI,         ONLY : KF_SI,VF_SI
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X,Z,U,RS,TAU
      REAL (WP)             ::  EPSR,EPSI
      REAL (WP)             ::  Q_SI,O_SI
!
      COMPLEX (WP)          ::  UU,U3,COEF
      COMPLEX (WP)          ::  LLOG,FT
!
      U=X*Z                                                         ! omega / (q * v_F)
!
      Q_SI=TWO*X*KF_SI                                              ! q     in SI
      O_SI=U*Q_SI*VF_SI                                             ! omega in SI
!
      UU=Q_SI*VF_SI/(O_SI + IC/TAU)                                 ! ref. (1) eq. (16)
      U3=UU*UU*UU                                                   !
      COEF=ENE_P_SI*ENE_P_SI/(H_BAR*H_BAR) *                      & !
               ONE/(O_SI*O_SI + IC*O_SI/TAU)                        !
      LLOG=CDLOG((ONE+UU)/(ONE-UU))                                 !
!
      FT=THREE*HALF/U3 * ( UU - HALF*(ONE-UU*UU)*LLOG )             ! ref. (1) eq. (14)
!
      EPSR=ONE - DREAL(COEF*FT)                                     !
      EPSI=DIMAG(COEF*FT)                                           !
!
      END SUBROUTINE BLTZ_EPS_D_TR_3D  
!
!=======================================================================
!
!    1) 2D case
!
!=======================================================================
!
      SUBROUTINE DFUNCT_DYNAMIC_2D(X,Z,D_FUNCT,EPSR,EPSI)
!
!  This subroutine computes the transverse dynamic 
!    dielectric functions in 2D
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * Z        : dimensionless factor   --> Z = omega / omega_q 
!       * D_FUNCT  : type of transverse dielectric function (2D)
!                      D_FUNCT = 'TRPA' random phase approximation
!
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Jun 2020
!
!
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  D_FUNCT
!
      REAL (WP)             ::  X,Z,RS
      REAL (WP)             ::  EPSR,EPSI
!
      IF(D_FUNCT == 'RPA1') THEN                                    !
        CALL RPA1_EPS_D_TR_2D(X,Z,RS,EPSR,EPSI)                     !
      END IF                                                        !
!
      END SUBROUTINE DFUNCT_DYNAMIC_2D
!
!=======================================================================
!
      SUBROUTINE RPA1_EPS_D_TR_2D(X,Z,RS,EPSR,EPSI)
!
!  This subroutine computes the transverse 
!    RPA dielectric function EPS(q,omega,T) for 2D systems.
!
!  References: (1) R. Nifosi, S. Conti and M. P. Tosi, 
!                     Phys. Rev. B 58, 12758 (1998)
!
!  Notation: hbar omega_q = hbar^2 q^2 / 2m
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)       
!       * Z        : dimensionless factor   --> Z = omega / omega_q 
!       * RS       : Wigner-Seitz radius (in units of a_0)
!
!  Intermediate parameters:
!
!       * U        : dimensionless factor   --> U = omega / q v_F       
!
!  Output parameters:
!
!       * EPSR     : real part of the dielectric function
!       * EPSI     : imaginary part of the dielectric function
!
!  Note:
!
!  The dielectric function is obtained from the current-current susceptibility by
!
!     eps =  1 - ( omega_p/omega )^2 * [ 1 + m/n * chi ]
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 12 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,TWO,SIX,THIRD
      USE CONSTANTS_P1,     ONLY : H_BAR
      USE FERMI_SI,         ONLY : KF_SI,VF_SI
      USE PLASMON_ENE_SI
!
      IMPLICIT NONE
!
      REAL (WP)             ::  X,Z,U,RS
      REAL (WP)             ::  EPSR,EPSI
      REAL (WP)             ::  XP,XM,EP,EM,BP,BM
      REAL (WP)             ::  Q_SI,O_SI,COEF
!
      U=X*Z                                                         ! omega / (q * v_F)
!
      XP=X+U                                                        !
      XM=X-U                                                        !
!
      Q_SI=TWO*X*KF_SI                                              ! q     in SI
      O_SI=U*Q_SI*VF_SI                                             ! omega in SI
!
      COEF=ENE_P_SI*ENE_P_SI/(H_BAR*H_BAR*O_SI*O_SI)                ! 
!
      IF(XP.GE.ONE) THEN                                            !
        EP=SIGN(XP,(XP*XP-ONE)**1.5E0_WP)                           ! eq. (A7) ref. 1
        BP=ZERO                                                     !
      ELSE                                                          !
        EP=ZERO                                                     !
        BP=(ONE-XP*XP)**1.5E0_WP                                    ! eq. (A4) ref. 1
      ENDIF                                                         ! 
!                                                    
      IF(XM.GE.ONE) THEN                                            ! 
        EM=SIGN(XM,(XM*XM-ONE)**1.5E0_WP)                           ! eq. (A7) ref. 1
        BM=ZERO                                                     !
      ELSE                                                          !
        EM=ZERO                                                     !
        BM=(ONE-XM*XM)**1.5E0_WP                                    ! eq. (A4) ref. 1
      ENDIF                                                         !
!
!  Real part
!
      EPSR=ONE-COEF*THIRD* (                                    &   !
                             TWO*X*X*X + SIX*U*U*X - EP - EM    &   ! eq. (A7) ref. 1
                           ) / X                                    !
!
!  Imaginary part
!
      EPSI=-COEF*THIRD*(BP-BM)/X                                    ! eq. (A4) ref. 1
!
      END SUBROUTINE RPA1_EPS_D_TR_2D  
!
END MODULE DFUNCT_STAN_DYNAMIC
