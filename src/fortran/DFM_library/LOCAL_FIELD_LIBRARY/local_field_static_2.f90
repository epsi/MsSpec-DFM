!
!=======================================================================
!
MODULE LOCAL_FIELD_STATIC_2
!
!  This modules provides subroutines/functions to compute
!            static local-field factors G(q) 
!
!  These G(q) DEPEND of the static structure factor S(q)
!
!
      USE ACCURACY_REAL
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE LFIELD_STATIC_2(X,RS,T,GQ_TYPE,GQ)
!
!  This subroutine computes static local-field factors G(q) 
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * GQ_TYPE  : local-field correction type (3D)
!
!
!  Output parameters:
!
!       * GQ       : static local field correction
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 19 Jun 2020
!
!
      USE MATERIAL_PROP,    ONLY : DMN
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  GQ_TYPE
!
      REAL (WP)             ::  X,RS,T
      REAL (WP)             ::  GQ
!
      IF(DMN == '3D') THEN                                          !
        CALL LOCAL_FIELD_STATIC_3D_2(X,RS,T,GQ_TYPE,GQ)             !
      ELSE IF(DMN == '2D') THEN                                     !
        CONTINUE                                                    !  not implemented
      ELSE IF(DMN == '1D') THEN                                     !
        CONTINUE                                                    !  not implemented
      END IF                                                        !
!
      END SUBROUTINE LFIELD_STATIC_2
!
!------ 1) 3D case --------------------------------------------
!
!=======================================================================
!
      SUBROUTINE LOCAL_FIELD_STATIC_3D_2(X,RS,T,GQ_TYPE,GQ)
!
!  This subroutine computes static local-field factors G(q) 
!    depending on the structure factor S(q) for 3D systems.
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * GQ_TYPE  : local-field correction type (3D)
!                       GQ_TYPE = 'IKPA' Iwamoto-Krotscheck-Pines
!  temperature-dep. --> GQ_TYPE = 'HNCA' hypernetted chain
!
!
!  Output parameters:
!
!       * GQ       : static local field correction
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  2 Dec 2020
!
!
      USE SF_VALUES,           ONLY : SQ_TYPE
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  GQ_TYPE
!
      REAL (WP)             ::  X,RS,T
      REAL (WP)             ::  GQ
!
      IF(GQ_TYPE == 'IKPA') THEN                                    !
        GQ=IKPA_LFC(X,RS,T,SQ_TYPE)                                 !
      ELSE IF(GQ_TYPE == 'HNCA') THEN                               !
        GQ=HNCA_LFC(X,RS,T,SQ_TYPE)                                 !             
      END IF                                                        !
!
      END SUBROUTINE LOCAL_FIELD_STATIC_3D_2  
!
!=======================================================================
!
      FUNCTION HNCA_LFC(X,RS,T,SQ_TYPE)
!
!  This function computes the hypernetted chain
!      local-field correction
!
!  References: (1) A. Isihara, "Electron Liquids", 2nd edition,
!                     Springer Series in Solid-STate Sciences 96,
!                     (Springer, 1998) p. 33
! 
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * SQ_TYPE  : structure factor approximation (3D)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 16 Sep 2020
!
!
!
      USE REAL_NUMBERS,             ONLY : ONE
      USE FERMI_SI,                 ONLY : KF_SI
      USE SCREENING_VEC,            ONLY : DEBYE_VECTOR
      USE STRUCTURE_FACTOR_STATIC
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  SQ_TYPE
!
!
      REAL (WP)             ::  X,RS,T,Y
      REAL (WP)             ::  HNCA_LFC
      REAL (WP)             ::  Q_SI,Q_D,R
      REAL (WP)             ::  SQ
!
      Y    = X + X                                                  ! Y = q / k_F
      Q_SI = Y * KF_SI                                              ! q   in SI
!
!  Computing the Debye vector
!
      CALL DEBYE_VECTOR('3D',T,RS,Q_D)                              ! q_D in SI
!
      R = Q_SI / Q_D                                                !
!
!  Computing the structure factor
!
      CALL STFACT_STATIC(X,RS,T,SQ_TYPE,SQ)                         !
!
      HNCA_LFC = ONE + (ONE - ONE / SQ) * R * R                     ! ref. (1) eq. (2.2.33)
!
      END FUNCTION HNCA_LFC  
!
!=======================================================================
!
      FUNCTION IKPA_LFC(X,RS,T,SQ_TYPE)
!
!  This function computes the Iwamoto-Krotscheck-Pines 
!      local-field correction
!
!  References: (1) N. Iwamoto, E. Krotscheck and D. Pines,
!                     Phys. Rev. B 29, 3936-3951 (1984)
! 
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!       * SQ_TYPE  : structure factor approximation (3D)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 16 Sep 2020
!
!
      USE REAL_NUMBERS,             ONLY : ONE,THREE,FOUR
      USE PI_ETC,                   ONLY : PI
      USE UTILITIES_1,              ONLY : ALFA
      USE STRUCTURE_FACTOR_STATIC
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  SQ_TYPE
!
      REAL (WP), INTENT(IN) ::  X,RS,T
      REAL (WP)             ::  IKPA_LFC
      REAL (WP)             ::  Y,Y4,COEF
      REAL (WP)             ::  SF,SQ
      REAL (WP)             ::  SQ2,SF2
      REAL (WP)             ::  ALPHA
!
      Y  = X + X                                                    ! Y = q / k_F
      Y4 = Y * Y * Y * Y                                            ! Y^4
!
      ALPHA = ALFA('3D')                                            !
      COEF  = THREE * PI / (FOUR * FOUR * ALPHA * RS)               !
!
      SF = HFA_SF(X)                                                !
      CALL STFACT_STATIC(X,RS,T,SQ_TYPE,SQ)                         !
!
      SQ2 = SQ * SQ                                                 !
      SF2 = SF * SF                                                 !
!
      IKPA_LFC = ONE - COEF * ( ONE / SQ2 - ONE / SF2 ) * Y4        !
!
      END FUNCTION IKPA_LFC  
!
END MODULE LOCAL_FIELD_STATIC_2
