!
!=======================================================================
!
MODULE ASYMPT
!
!  This module defines the asymptotic quantities 
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  G0,GI,GR0
!
END MODULE ASYMPT
!
!=======================================================================
!
MODULE CALC_ASYMPT
!
      USE ACCURACY_REAL
!
!  This modules computes the asymptotic values:
!
!                        * gamma_0
!                        * gamma_inf
!                        * g(0)
!
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_ASYMPT_VALUES
!

      USE ASYMPT
!
      USE MATERIAL_PROP,   ONLY : DMN,RS
      USE EXT_FIELDS,      ONLY : T
!
      USE GAMMA_ASYMPT
      USE PC_VALUES,       ONLY : GR0_MODE
      USE GR_0
!
      IMPLICIT NONE
!
      IF(DMN == '3D') THEN                                          !
        G0  = GAMMA_0_3D(RS,T)                                      !
        GI  = GAMMA_I_3D(RS,T)                                      !
        GR0 = GR_0_3D(RS,GR0_MODE)                                  !
      ELSE IF(DMN == '2D') THEN                                     !
        G0  = GAMMA_0_2D(RS,T)                                      !
        GI  = GAMMA_I_2D(RS,T)                                      !
        GR0 = GR_0_2D(RS,GR0_MODE)                                  !
      ELSE IF(DMN == '1D') THEN                                     !
        CONTINUE                                                    ! not implemented yet
      END IF                                                        !
!
      END SUBROUTINE CALC_ASYMPT_VALUES
!
END MODULE CALC_ASYMPT
