!
!=======================================================================
!
MODULE SQ_I
!
      USE ACCURACY_REAL
!
!  It contains the following functions/subroutines:
!
!             * FUNCTION SQ_I_3D(X,GR0_MODE)
!
CONTAINS
!
!
!=======================================================================
!
      FUNCTION SQ_I_3D(X,RS,GR0_MODE)
!
!  This function computes the asymptotic behaviour of the static 
!    structure factor at infinity:
!
!    lim (q --> infinity) S(q)
!
!  References: (1) N. Iwamoto, E. Krotscheck and D. Pines, 
!                    Phys. Rev. B 28, 3936-3951 (1984)
!
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * GR0_MODE : g(0) (3D)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  5 Aug 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,SIX
      USE CONSTANTS_P1,     ONLY : BOHR
      USE FERMI_SI,         ONLY : KF_SI
      USE PI_ETC,           ONLY : PI
      USE GR_0,             ONLY : GR_0_3D
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   :: GR0_MODE
!
      REAL (WP), INTENT(IN) :: X,RS
      REAL (WP)             :: SQ_I_3D
      REAL (WP)             :: COEF
!
      COEF = ONE / (SIX * PI * BOHR * KF_SI)                        !
!
      SQ_I_3D = ONE - COEF * GR_0_3D(RS,GR0_MODE) / (X * X * X * X) !
!
      RETURN
!
      END   
!
END MODULE SQ_I
 
