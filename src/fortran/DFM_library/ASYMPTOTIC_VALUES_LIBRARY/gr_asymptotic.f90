!
!=======================================================================
!
MODULE GR_0
!
      USE ACCURACY_REAL
!
!  It contains the following functions/subroutines:
!
!             * FUNCTION GR_0_2D(RS,GR0_MODE)
!
!             * FUNCTION GR_0_3D(RS,GR0_MODE)
!
CONTAINS
!
!=======================================================================
!
      FUNCTION GR_0_2D(RS,GR0_MODE)
!
!  This function computes the value of the pair correlation function 
!     g(r) at r = 0 for 2D systems
!
!  References: (1) J. Moreno and D. C. Marinescu, 
!                     J. Phys.: Condens. Matter 15, 6321-6329 (2003)
!              (2) M. L. Glasser, J. Phys. C: Solid State Phys. 10, 
!                     L121-L123 (1977)
!              (3) S. Nagano, K. S. Singwi and S. Ohnishi, 
!                     Phys. Rev. B 29, 1209-1213 (1984)
!                  S. Nagano, K. S. Singwi and S. Ohnishi,
!                     Phys. Rev. B 31, 3166 (1985)
!              (4) L. Calmels and A. Gold, Phys. Rev. B 57, 
!                     1436-1443 (1998)
!              (5) Z. Qian, Phys. Rev. B 73, 035106 (2006)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * GR0_MODE : g(0) (2D)
!                       GR0_MODE  = 'MOMA' --> Moreno-Marinescu 
!                       GR0_MODE  = 'HAFO' --> Hartree-Fock 
!                       GR0_MODE  = 'NSOA' --> Nagano-Singwi-Ohnishi
!                       GR0_MODE  = 'CAGO' --> Calmels-Gold
!                       GR0_MODE  = 'QIAN' --> Qian
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 24 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,HALF
      USE FERMI_AU,         ONLY : KF_AU
      USE BESSEL,           ONLY : BESSI0
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  GR0_MODE
!
      REAL (WP), INTENT(IN) :: RS
      REAL (WP)             :: GR_0_2D
!
      REAL (WP)             :: L2_1,L2_2,L2_3
      REAL (WP)             :: NUM,DEN
!
      REAL (WP)             :: SQRT
!
      IF(GR0_MODE == 'MOMA') THEN                                   !
        GR_0_2D = HALF / (ONE + 0.6032E0_WP * RS +                & !
                          0.07263E0_WP * RS * RS)**2                ! ref. (1) eq. (1)
      ELSE IF(GR0_MODE == 'HAFO') THEN                              !
        GR_0_2D = 0.75E0_WP                                         ! ref. (2)
      ELSE IF(GR0_MODE == 'NSOA') THEN                              !
        GR_0_2D = HALF / BESSI0(TWO / SQRT(KF_AU))                  !
      ELSE IF(GR0_MODE == 'CAGO') THEN                              !
        GR_0_2D = HALF / (ONE + TWO / KF_AU + 1.5E0_WP /          & !
                         (KF_AU * KF_AU))                           ! ref. (4) eq. (18)
      ELSE IF(GR0_MODE == 'QIAN') THEN                              !
        L2_1    =  RS / SQRT(TWO)                                   ! lambda_2
        L2_2    =  L2_1 * L2_1                                      !
        L2_3    =  L2_2 * L2_1                                      !
        NUM     =  15.0E0 * ( 64.0E0_WP + 25.0E0_WP * L2_1 +      & !
                              THREE * L2_2 )                        !
        DEN     =  960.0E0_WP + 1335.0E0_WP * L2_1 +              & !
                   509.0E0_WP * L2_2 + 64.0E0_WP * L2_3             !
        GR_0_2D = NUM * NUM / (DEN * DEN)                           ! ref. (5) eq. (10)
      END IF                                                        !
!
      END FUNCTION GR_0_2D  
!
!=======================================================================
!
      FUNCTION GR_0_3D(RS,GR0_MODE)
!
!  This function computes the value of the pair correlation function 
!     g(r) at r = 0 for 3D systems
!
!  References: (1) B. Davoudi, M. Polini, G. F. Giuliani and M. P. Tosi,
!                     Phys. Rev. B 64, 153101 (2001)
!              (2) G. E. Simion and G. F. Giuliani, Phys. Rev. B 77,
!                     035131 (2008)
!              (3) A. Holas, P. K. Aravind and K. S. Singwi, 
!                     Phys. Rev. B 20, 4912-4934 (1979)
!              (4) S. Ichimaru, Rev. Mod. Phys. 54, 1017-1059 (1982)
!              (5) L. Calmels and A. Gold, Phys. Rev. B 57, 
!                     1436-1443 (1998)
!              (6) V. D. Gorobchenko, V. N. Kohn and E. G. Maksimov, 
!                    in "Modern Problems in Condensed Matter" Vol. 24, 
!                    L. V. Keldysh, D. A. Kirzhnitz and A. A. Maradudin ed. 
!                    pp. 87-219 (North-Holland, 1989)
!              (7) Z. Qian, Phys. Rev. B 73, 035106 (2006)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * GR0_MODE : g(0) (3D)
!                       GR0_MODE  = 'DPGT' --> Davoudi-Polini-Giuliani-Tosi
!                       GR0_MODE  = 'OVE1' --> Overhauser 1
!                       GR0_MODE  = 'OVE2' --> Overhauser 2
!                       GR0_MODE  = 'HASA' --> Holas-Aravind-Singwi (small r_s)
!                       GR0_MODE  = 'ICHI' --> Ichimaru
!                       GR0_MODE  = 'CAGO' --> Calmels-Gold
!                       GR0_MODE  = 'KIMB' --> Kimball
!                       GR0_MODE  = 'QIAN' --> Qian
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  5 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,FOUR,SIX,EIGHT, &
                                   NINE,HALF,FOURTH,FIFTH,EIGHTH
      USE FERMI_AU,         ONLY : KF_AU
      USE PI_ETC,           ONLY : PI,PI2,PI_INV
      USE UTILITIES_1,      ONLY : ALFA
      USE BESSEL,           ONLY : BESSI1
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  GR0_MODE
!
      REAL (WP), INTENT(IN) :: RS
      REAL (WP)             :: GR_0_3D
!
      REAL (WP)             :: Z
      REAL (WP)             :: ALPHA
      REAL (WP)             :: C,D,ARP
      REAL (WP)             :: L3_1,L3_2,L3_3
      REAL (WP)             :: NUM,DEN
!
      REAL (WP)             :: LOG,SQRT
!
      ALPHA = ALFA('3D')                                            !
!
      IF(GR0_MODE == 'DPGT') THEN                                   !
        GR_0_3D = HALF / ( ONE + 1.372E0_WP * RS +                & !
                           0.0830E0_WP * RS * RS )                  ! ref. (1) eq. (9)
      ELSE IF(GR0_MODE == 'OVE1') THEN                              !
        GR_0_3D = 32.0E0_WP / (EIGHT + THREE * RS)**2               ! ref. (2) eq. (35)
      ELSE IF(GR0_MODE == 'HASA') THEN                              !
        GR_0_3D = HALF * FIFTH * ALPHA * PI_INV *                 & !
                     (PI2 + SIX * LOG(TWO) - THREE) * RS -        & ! ref. (3) eq. (7.13)
                     (1.5E0_WP * ALPHA * PI_INV)**2   *           & !
                     (THREE - FOURTH * PI2) * RS * RS * LOG(RS)     !
      ELSE IF(GR0_MODE == 'ICHI') THEN                              !
        Z       =FOUR * SQRT(ALPHA * RS / PI)                       ! ref. (4) eq. (3.67)
        GR_0_3D = EIGHTH * (Z / BESSI1(Z))**2                       !
      ELSE IF(GR0_MODE == 'CAGO') THEN                              !
        GR_0_3D = HALF * TWO * PI_INV * KF_AU +                  &  ! ref. (5) eq. (11)
                         14.0E0_WP / (THREE * (PI * KF_AU)**2)      !
      ELSE IF(GR0_MODE == 'OVE2') THEN                              !
        GR_0_3D = HALF / ( ONE + 0.75E0_WP * RS +                 & !
                           0.141E0_WP * RS * RS )                   ! ref. (5) 
      ELSE IF(GR0_MODE == 'KIMB') THEN                              !
        C       = FIFTH * (PI2 + SIX * LOG(TWO) - THREE)            ! ref. (6) eq. (2.55)
        D       = NINE * (12.0E0_WP - PI2) / 16.0E0_WP              ! ref. (6) eq. (2.55)
        ARP     = ALPHA * PI_INV * RS                               !
        GR_0_3D = HALF - C * ARP - D * ARP * ARP * LOG(RS)          ! ref. (6) eq. (2.54)
      ELSE IF(GR0_MODE == 'QIAN') THEN                              !
        L3_1 = TWO * ALPHA * RS * PI_INV                            !
        L3_2 = L3_1 * L3_1                                          !
        L3_3 = L3_2 * L3_1                                          !
        NUM  = 45.0E0_WP * ( 45.0E0_WP + 24.0E0_WP * L3_1 +       & !
                             FOUR * L3_2 )                          !
        DEN  = 2025.0E0_WP + 3105.0E0_WP * L3_1  +                & !
               1512.0E0_WP * L3_2 + 256.0E0_WP * L3_3               !
        GR_0_3D = NUM * NUM / (DEN * DEN)                           ! ref. (7) eq. (9)
      END IF                                                        !
!
      END FUNCTION GR_0_3D  
!
END MODULE GR_0
 
