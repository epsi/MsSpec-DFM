!
!=======================================================================
!
MODULE GAMMA_ASYMPT
!
      USE ACCURACY_REAL
!
!  It contains the following functions/subroutines:
!
!             * FUNCTION GAMMA_0_3D(RS,T)
!
!             * FUNCTION GAMMA_I_3D(RS,T)
!
!             * FUNCTION GAMMA_0_2D(RS,T)
!
!             * FUNCTION GAMMA_I_2D(RS,T)
!
!             * FUNCTION G0_INF_2D(X,RS,T)
!
!
CONTAINS
!
!=======================================================================
!
      FUNCTION GAMMA_0_3D(RS,T)
!
!  This function computes the coefficient gamma0 so that:
!
!          lim (q --> 0) G(q) = gamma_0 (q / k_F)^2
!
!
!  References: (1) S. Ichimaru, Rev. Mod. Phys. 54, 1017-1059 (1982)
!              (3) K. Utsumi and S. Ichimaru, Phys. Rev. B 22, 1522-1533 (1980)
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)     
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  2 Dec 2020
!
!
      USE LF_VALUES,            ONLY : GQ_TYPE,G0_TYPE
      USE SF_VALUES,            ONLY : SQ_TYPE
      USE ENERGIES,             ONLY : EC_TYPE
!
      USE REAL_NUMBERS,         ONLY : TWO,FOUR,HALF,FOURTH
      USE PI_ETC,               ONLY : PI
      USE FERMI_SI,             ONLY : KF_SI
      USE UTILITIES_1,          ONLY : ALFA
      USE CORRELATION_ENERGIES, ONLY : DERIVE_EC_3D
      USE SCREENING_VEC,        ONLY : THOMAS_FERMI_VECTOR
      USE SPECIFIC_INT_2
!
      IMPLICIT NONE
!
      INTEGER               ::  IN_MODE,NMAX
!
      REAL (WP), INTENT(IN) ::  RS,T
      REAL (WP)             ::  GAMMA_0_3D
!
      REAL (WP)             ::  ALPHA
      REAL (WP)             ::  RS2,RS3
      REAL (WP)             ::  D_EC_1,D_EC_2
      REAL (WP)             ::  KS,X_TF
      REAL (WP)             ::  X_MAX,IN
!
      IF(G0_TYPE == 'EC') THEN                                      !
!
        ALPHA = ALFA('3D')                                          !
        RS2   = RS  * RS                                            !
        RS3   = RS2 * RS                                            !
!
!  Computing the correlation energy derivatives
!
        CALL DERIVE_EC_3D(EC_TYPE,1,5,RS,T,D_EC_1,D_EC_2)           !
!
        GAMMA_0_3D = FOURTH - (PI * ALPHA / 24.0E0_WP) *          & ! ref. (1) eq. (3.30a)
                              (RS3 * D_EC_2 - TWO * RS2 * D_EC_1)   !
!
      ELSE IF(G0_TYPE == 'SQ') THEN                                 !
!
        IN_MODE = 2                                                 !
        NMAX    = 1000                                              ! number of integration points
        X_MAX   = 50.0E0_WP                                         !
!
!  Computing Thomas-Fermi screening vector
!
        CALL THOMAS_FERMI_VECTOR('3D',KS)                           !
        X_TF =  KS / KF_SI                                          ! q_{TF} / k_F 
!
!  Computing the integral
!
        CALL INT_SQM1(NMAX,X_MAX,IN_MODE,RS,T,X_TF,0,SQ_TYPE,     & !
                      GQ_TYPE,IN)
!
        GAMMA_0_3D = - HALF * IN                                    ! ref. (3) eq. (5.5)
!
      END IF                                                        !
!
      END FUNCTION GAMMA_0_3D  
!
!=======================================================================
!
      FUNCTION GAMMA_I_3D(RS,T)
!
!  This function computes the coefficient gamma_i so that:
!
!          lim (q --> 0) I(q) = gamma_i (q / k_F)^2         --> 3D
!
!
!  References: (1) S. Ichimaru, Rev. Mod. Phys. 54, 1017-1059 (1982)
!              (2) N. Iwamoto, Phys. Rev. A 30, 3289-3304 (1984)
!              (3) K. Utsumi and S. Ichimaru, Phys. Rev. B 22, 1522-1533 (1980)
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)     
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  2 Dec 2020
!
!
      USE LF_VALUES,            ONLY : GQ_TYPE,GI_TYPE
      USE SF_VALUES,            ONLY : SQ_TYPE
      USE ENERGIES,             ONLY : EC_TYPE
!
      USE REAL_NUMBERS,         ONLY : TWO,FOUR,TEN,FIFTH
      USE PI_ETC,               ONLY : PI
      USE FERMI_SI,             ONLY : KF_SI
      USE UTILITIES_1,          ONLY : ALFA
      USE CORRELATION_ENERGIES, ONLY : DERIVE_EC_3D,EC_3D
      USE SCREENING_VEC,        ONLY : THOMAS_FERMI_VECTOR
      USE SPECIFIC_INT_2
!
      IMPLICIT NONE
!
      INTEGER               ::  IN_MODE,NMAX
!
      REAL (WP), INTENT(IN) ::  RS,T
      REAL (WP)             ::  GAMMA_I_3D
!
      REAL (WP)             ::  ALPHA
      REAL (WP)             ::  RS2 
      REAL (WP)             ::  EC,D_EC_1,D_EC_2
      REAL (WP)             ::  KS,X_TF
      REAL (WP)             ::  X_MAX,IN
!
      IF(GI_TYPE == 'EC') THEN                                      !
!
        ALPHA = ALFA('3D')                                          !
        RS2   = RS  * RS                                            !
!
!  Computing the correlation energy derivatives
!
        EC = EC_3D(EC_TYPE,1,RS,T)                                  !
        CALL DERIVE_EC_3D(EC_TYPE,1,5,RS,T,D_EC_1,D_EC_2)           !
!
        GAMMA_I_3D = 0.15E0_WP - (PI * ALPHA / TEN) *             & ! ref. (1) eq. (3.30b)
                                 (RS2 * D_EC_1 + TWO* RS * EC)      ! 
!
      ELSE IF(GI_TYPE == 'SQ') THEN                                 !
!
        IN_MODE = 1                                                 !
        NMAX    = 1000                                              ! number of integration points
        X_MAX   = 50.0E0_WP                                         !
!
!  Computing Thomas-Fermi screening vector
!
        CALL THOMAS_FERMI_VECTOR('3D',KS)                           !
        X_TF =  KS / KF_SI                                          ! q_{TF} / k_F
!
!  Computing the integral
!
        CALL INT_SQM1(NMAX,X_MAX,IN_MODE,RS,T,X_TF,0,SQ_TYPE,     & !
                      GQ_TYPE,IN)
!
        GAMMA_I_3D = - FIFTH * IN                                   ! ref. (3) eq. (5.6)
!
      END IF                                                        !
!
      END FUNCTION GAMMA_I_3D  
!
!=======================================================================
!
      FUNCTION GAMMA_0_2D(RS,T)
!
!  This function computes the coefficient gamma0 so that:
!
!          lim (q --> 0) G(q) = gamma_i (q / k_F)           --> 2D
!
!
!  References: (2) N. Iwamoto, Phys. Rev. A 30, 3289-3304 (1984)
!              (3) K. Utsumi and S. Ichimaru, Phys. Rev. B 22, 1522-1533 (1980)
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)     
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  3 Dec 2020
!
!
      USE LF_VALUES,            ONLY : GQ_TYPE,G0_TYPE
      USE SF_VALUES,            ONLY : SQ_TYPE
      USE ENERGIES,             ONLY : EC_TYPE
!
      USE REAL_NUMBERS,         ONLY : ONE,HALF,EIGHTH
      USE PI_ETC,               ONLY : PI_INV
      USE FERMI_SI,             ONLY : KF_SI
      USE UTILITIES_1,          ONLY : ALFA
      USE CORRELATION_ENERGIES, ONLY : DERIVE_EC_2D
      USE ENERGIES,             ONLY : EC_TYPE
      USE SCREENING_VEC,        ONLY : THOMAS_FERMI_VECTOR
      USE SPECIFIC_INT_2
!
      IMPLICIT NONE
!
      INTEGER               ::  IN_MODE,NMAX
!
      REAL (WP), INTENT(IN) ::  RS,T
      REAL (WP)             ::  GAMMA_0_2D
!
      REAL (WP)             ::  ALPHA,RS2,RS3
      REAL (WP)             ::  D_EC_1,D_EC_2
      REAL (WP)             ::  KS,X_TF
      REAL (WP)             ::  X_MAX,IN
!
      IF(G0_TYPE == 'EC') THEN                                      !
!
        ALPHA = ALFA('2D')                                          !
        RS2   = RS  * RS                                            !
        RS3   = RS2 * RS                                            !
!
!  Computing the correlation energy derivatives
!
        CALL DERIVE_EC_2D(EC_TYPE,1,RS,T,D_EC_1,D_EC_2)             !
!
        GAMMA_0_2D = PI_INV + EIGHTH * ALPHA * (                  & ! 
                                  RS2 * D_EC_1 - RS3 * D_EC_2     & ! ref. (1) eq. (3.6c)  
                                               )                    !
!
      ELSE IF(G0_TYPE == 'SQ') THEN                                 !
!
        IN_MODE = 2                                                 !
        NMAX    = 1000                                              ! number of integration points
        X_MAX   = 50.0E0_WP                                         !
!
!  Computing Thomas-Fermi screening vector
!
        CALL THOMAS_FERMI_VECTOR('2D',KS)                           !
        X_TF =  KS / KF_SI                                          ! q_{TF} / k_F 
!
!  Computing the integral
!
        CALL INT_SQM1(NMAX,X_MAX,IN_MODE,RS,T,X_TF,0,SQ_TYPE,     & !
                      GQ_TYPE,IN)
!
        GAMMA_0_2D = - HALF * IN                                    ! ref. (3) eq. (5.5)
!
      END IF                                                        !
!
      END FUNCTION GAMMA_0_2D  
!
!=======================================================================
!
      FUNCTION GAMMA_I_2D(RS,T)
!
!  This function computes the coefficient gamma0 so that:
!{\bf 33}
!          lim (q --> 0) I(q) = gamma_i (q / k_F)           --> 2D
!
!
!  References: (2) N. Iwamoto, Phys. Rev. A 30, 3289-3304 (1984)
!              (3) K. Utsumi and S. Ichimaru, Phys. Rev. B 22, 1522-1533 (1980)
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)     
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  3 Dec 2020
!
!
      USE LF_VALUES,            ONLY : GQ_TYPE,GI_TYPE
      USE SF_VALUES,            ONLY : SQ_TYPE
      USE ENERGIES,             ONLY : EC_TYPE
!
      USE REAL_NUMBERS,         ONLY : TWO,FIVE,SIX,FIFTH
      USE PI_ETC,               ONLY : PI_INV
      USE FERMI_SI,             ONLY : KF_SI
      USE UTILITIES_1,          ONLY : ALFA
      USE CORRELATION_ENERGIES, ONLY : DERIVE_EC_2D,EC_2D
      USE ENERGIES,             ONLY : EC_TYPE
      USE SCREENING_VEC,        ONLY : THOMAS_FERMI_VECTOR
      USE SPECIFIC_INT_2
!
      IMPLICIT NONE
!
      INTEGER               ::  IN_MODE,NMAX
!
      REAL (WP), INTENT(IN) ::  RS,T
      REAL (WP)             ::  GAMMA_I_2D
!
      REAL (WP)             ::  ALPHA,RS2
      REAL (WP)             ::  EC,D_EC_1,D_EC_2
      REAL (WP)             ::  KS,X_TF
      REAL (WP)             ::  X_MAX,IN
!
      IF(GI_TYPE == 'EC') THEN                                      !
!
        ALPHA = ALFA('3D')                                          !
        RS2   = RS * RS                                             !
!
!  Computing the correlation energy derivatives
!
        EC = EC_2D(EC_TYPE,RS,T)                                    !
        CALL DERIVE_EC_2D(EC_TYPE,1,RS,T,D_EC_1,D_EC_2)             !
!
        GAMMA_I_2D = FIVE * PI_INV / SIX -                        & !
                     (FIVE * ALPHA / 16.0E0_WP) *                 & !
                     (RS2 * D_EC_1 + TWO * RS * EC)                 ! ref. (2) eq. (D9c)
!
      ELSE IF(GI_TYPE == 'SQ') THEN                                 !
!
        IN_MODE = 1                                                 !
        NMAX    = 1000                                              ! number of integration points
        X_MAX   = 50.0E0_WP                                         !
!
!  Computing Thomas-Fermi screening vector
!
        CALL THOMAS_FERMI_VECTOR('3D',KS)                           !
        X_TF =  KS / KF_SI                                          ! q_{TF} / k_F
!
!  Computing the integral
!
        CALL INT_SQM1(NMAX,X_MAX,IN_MODE,RS,T,X_TF,0,SQ_TYPE,     & !
                      GQ_TYPE,IN)
!
        GAMMA_I_2D = - FIFTH * IN                                   ! ref. (3) eq. (5.6)
!
      END IF                                                        !
!
      END FUNCTION GAMMA_I_2D  
!
!=======================================================================
!
      FUNCTION G0_INF_2D(X,RS,T)
!
!  This function computes G(0,infinity), the value of the dynamic 
!    local-field correction for q --> 0 and omega = infinity, for 2D systems
!
!  References: (1) B. Tanatar, Phys. Lett. A 158, 153-157 (1991)
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature (SI)     
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  2 Dec 2020
!
!
      USE REAL_NUMBERS,          ONLY : ONE,SEVEN,EIGHT,HALF,THIRD
      USE SQUARE_ROOTS,          ONLY : SQR2
      USE PI_ETC,                ONLY : PI_INV
      USE CORRELATION_ENERGIES,  ONLY : DERIVE_EC_2D,EC_2D
      USE ENERGIES,              ONLY : EC_TYPE
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN) ::  X,RS,T
      REAL (WP)             ::  G0_INF_2D
!
      REAL (WP)             ::  Y
      REAL (WP)             ::  A
      REAL (WP)             ::  EC,D_EC_1,D_EC_2
!
      Y = X + X                                                     ! Y = q / k_F
! 
      A = ONE / SQR2                                                !
!
!  Computing the correlation energy derivatives
!
      EC = EC_2D(EC_TYPE,RS,T)                                      !
      CALL DERIVE_EC_2D(EC_TYPE,1,RS,T,D_EC_1,D_EC_2)               !
!
      G0_INF_2D = Y *(0.20E0_WP * HALF * THIRD * PI_INV +         & !
                   SEVEN * A * RS * EC / EIGHT          +         & ! ref. (1) eq. (7)
                   19.0E0_WP * A * RS * D_EC_1 / 16.0E0_WP)         !
!
      END FUNCTION G0_INF_2D  
!
END MODULE GAMMA_ASYMPT
