!
!=======================================================================
!
MODULE CHANGE_FILENAMES
!
!  This module changes all the output filenames by 
!   incorporating into them a string characteristic of 
!   the calculation performed
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE LOGFILE_NAMES(N_IF,LOGFILE)
!
!  This subroutine constructs the name of the logfiles when 
!   several input data file are read in
!
!
!  Input parameters:
!
!       * N_IF     : number of input data file
!
!
!  Uutput parameters:
!
!       * LOGFILE  : array containing the names of the log files
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  8 Sep 2020
!
!
      IMPLICIT NONE 
!
      INTEGER                 ::  N_IF,JF
!
      CHARACTER (LEN = 100)   ::  LOGFILE(999)
      CHARACTER (LEN =   7)   ::  BASENAME
      CHARACTER (LEN =   4)   ::  EXTENSION
      CHARACTER (LEN =    1)  ::  UNDSC
!
      BASENAME  = 'epsilon'                                         !
      EXTENSION = '.lis'                                            !
      UNDSC     = '_'                                               !
!
      IF(N_IF == 1) THEN                                            !
        LOGFILE(1) =  BASENAME//EXTENSION                           !
      ELSE
        DO JF = 1,N_IF                                              !
          LOGFILE(JF) = BASENAME//UNDSC//NUM2STRING(JF)//EXTENSION  !
        END DO                                                      !
      END IF
!
      END SUBROUTINE LOGFILE_NAMES
!
!=======================================================================
!
      FUNCTION NUM2STRING(NUM)
!
!  This function converts an integer into the corresponding 
!    character string, including the zeros (e. g. 3 --> 003)
!
!  This version: limited to 999 
!
!
!  Input parameter:
!
!       * NUM     :  number to be transformed into a string 
!
!
!
!   Author :  D. Sébilleau
!
!                                            Last modified : 8 Sep 2020
!
!
      IMPLICIT NONE
!
      INTEGER, PARAMETER      ::  N_LE = 3                          ! max. number of digits
      INTEGER, PARAMETER      ::  N_IF = 999                        ! max. number of input files
!
      CHARACTER (LEN = N_LE)  ::  NUM2STRING
!
      INTEGER                 ::  NUM
      INTEGER                 ::  JF
      INTEGER                 ::  I_D1,I_D2,I_D3
      INTEGER                 ::  NNUM
!
!  Initialisation of the digits to 0
!
      I_D1 = 48
      I_D2 = 48
      I_D3 = 48
!
      DO JF = 1,N_IF                                                ! start loop on files
!
        I_D1 = I_D1 + 1                                             ! incrementation of 1st digit
        IF(I_D1 == 58) THEN
          I_D1 = 48
          I_D2 = I_D2 + 1                                           ! incrementation of 2nd digit
          IF(I_D2 == 58) THEN
            I_D2 = 48
            I_D3 = I_D3 + 1                                         ! incrementation of 3rd digit
          END IF
        END IF
!
        NNUM = (I_D3 - 48) * 100 + (I_D2 - 48) * 10 + (I_D1 - 48)   ! number generated
!
        IF(NUM == NNUM) GO TO 10
! 
      END DO
! 
  10  NUM2STRING = CHAR(I_D3)//CHAR(I_D2)//CHAR(I_D1)
!
      END FUNCTION NUM2STRING
!
!=======================================================================
!
      SUBROUTINE NEW_FILENAMES(N_IF,JF,FLIST)
!
!  This subroutine changes all the output filenames by 
!   incorporating into them a string characteristic of 
!   the calculation performed
!
!
!  Input parameters:
!
!       * N_IF     : number of input data file
!       * J  F     : current input data file
!
!
!  Input/output parameters:
!
!       * FLIST    : array containing the names of the output files
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Sep 2020
!
!
      USE DIMENSION_CODE,    ONLY : NOFFN
      USE OUT_VALUES_10
      USE OUTFILES     
!
      IMPLICIT NONE 
!
      INTEGER, INTENT(IN)                  ::  N_IF
      INTEGER, INTENT(IN)                  ::  JF
!
      INTEGER                              ::  FILE
!
      CHARACTER (LEN = 100), INTENT(INOUT) ::  FLIST(NOFFN)
!
      CHARACTER (LEN = 100)                ::  CLIST(NOFFN)
      CHARACTER (LEN = 100)                ::  INFILE
      CHARACTER (LEN = 100)                ::  OUTFILE
      CHARACTER (LEN = 100)                ::  STRING
      CHARACTER (LEN =    1)               ::  UNDSC
!
      UNDSC     = '_'                                               !
!
      IF(N_IF == 1) THEN                                            !
!
!  Only the type of calculation can be appended
!
        IF(I_FN == 1) THEN                                          !
!
          CALL CALC_TYPE(CLIST)                                     !
          CALL OUT_FILES(FLIST)                                     !
!
          DO FILE = 7,NOFFN                                         !
!
             INFILE = FLIST(FILE)                                   !
             STRING = CLIST(FILE)                                   !
!
             CALL CHG_FILENAME(INFILE,STRING,OUTFILE)               !
!
             FLIST(FILE) = OUTFILE                                  !
!
          END DO                                                    !
!
        ELSE                                                        !
!
          CALL OUT_FILES(FLIST)                                     !
!
        END IF                                                      !
!
      ELSE                                                          !
!
        IF(I_FN == 1) THEN                                          !
!
!  Type of calculation appended + input data file number appended
!
          CALL CALC_TYPE(CLIST)                                     !
          CALL OUT_FILES(FLIST)                                     !
!
          DO FILE = 7,NOFFN                                         !
!
             INFILE = FLIST(FILE)                                   !
!
!  Checking if input data file number must ne appended
!
             IF(INDEX_FILE(FILE) == 0) THEN                         !
               STRING = CLIST(FILE)//UNDSC//NUM2STRING(JF)          !
             ELSE                                                   !
               STRING = CLIST(FILE)                                 !
             END IF                                                 !
!
             CALL CHG_FILENAME(INFILE,STRING,OUTFILE)               !
!
             FLIST(FILE) = OUTFILE                                  !
!
          END DO                                                    !
!
        ELSE
!
!  Only input data file number appended
!
          CALL OUT_FILES(FLIST)                                     !
!
          DO FILE = 7,NOFFN                                         !
!
             INFILE = FLIST(FILE)                                   !
!
!  Checking if input data file number must ne appended
!
             IF(INDEX_FILE(FILE) == 0) THEN                         !
               STRING = NUM2STRING(JF)                              !
               CALL CHG_FILENAME(INFILE,STRING,OUTFILE)             !
               FLIST(FILE) = OUTFILE                                !
             END IF                                                 !
!
          END DO                                                    !
!
        END IF                                                      !
!
      END IF                                                        !
!
      END SUBROUTINE NEW_FILENAMES
!
!=======================================================================
!
      SUBROUTINE CHG_FILENAME(INFILE,STRING,OUTFILE)
!
!  This subroutine incorporates a given string 
!    into a filename
!
!
!  Input parameters:
!
!       * INFILE   : name of input file
!       * STRING   : string to be appended
!
!
!  Input/output parameters:
!
!       * OUTFILE  : name of output file
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  7 Sep 2020
!
!
      IMPLICIT NONE
!
      INTEGER, PARAMETER                 ::  N_LENGTH = 100
!
      CHARACTER (LEN = 100), INTENT(IN)  ::  INFILE
      CHARACTER (LEN = 100), INTENT(OUT) ::  OUTFILE
!
      CHARACTER (LEN = 100)              ::  STRING
!
      INTEGER                            ::  J_CHAR
      INTEGER                            ::  N_DOT,N_CHAR
!
!  Finding the real size of the file name 
!         and the position of the dot
!
      N_DOT = 1                                                     !
      DO J_CHAR = 1,N_LENGTH                                        !
        IF(INFILE(J_CHAR:J_CHAR).EQ.'.') GO TO 10                   !
        N_DOT = N_DOT + 1                                           !
      END DO                                                        !
!
  10  CONTINUE                                                      !
!
      N_CHAR = 0                                                    !
      DO J_CHAR = 1,N_LENGTH                                        !
        IF(INFILE(J_CHAR:J_CHAR).EQ.' ') GO TO 20                   !
        N_CHAR = N_CHAR + 1                                         !
      END DO                                                        !
!
  20  CONTINUE                                                      !
! 
!  Incorporation of the string
!
      OUTFILE = INFILE(1:N_DOT-1)//'_'//TRIM(STRING)//            & !
                INFILE(N_DOT:N_CHAR)                                !
!
      END SUBROUTINE CHG_FILENAME
!
END MODULE CHANGE_FILENAMES
