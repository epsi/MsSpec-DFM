!
!=======================================================================
!
MODULE OUTFILES 
!
!  This module contains functions/subroutines for working with 
!    the output files
!
    CHARACTER (LEN = 80) ::  OUTDIR
CONTAINS
!
!
!=======================================================================
!
      FUNCTION FN(NAFILE)      
!
!  This function returns the Fortran unit corresponding to file NAFILE
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  5 Jun 2020
!
!
      USE DIMENSION_CODE,    ONLY : NOFFN
!
      IMPLICIT NONE
!
      CHARACTER (LEN =  100)  ::  NAFILE
      CHARACTER (LEN =  100)  ::  FLIST(NOFFN)
!
      INTEGER                 ::  FN,J
!
      CALL OUT_FILES(FLIST)                                         !
!
      DO J=7,NOFFN                                                  !
        IF(FLIST(J) == NAFILE) THEN                                 !
          FN=J                                                      !
          RETURN                                                    !
        END IF                                                      !
      END DO                                                        !
!
      END FUNCTION FN  
!
!=======================================================================
!
      SUBROUTINE OUT_FILES(FLIST)
!
!  This subroutine stores all the output files and their Fortran 
!    unit number in the FLIST array
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 26 Jan 2021
!
!
      USE DIMENSION_CODE,    ONLY : NOFFN
      USE FILENAMES
!
      IMPLICIT NONE
!
      CHARACTER (LEN =  100)             ::  FLIST(NOFFN)
!
!  Filenames:
!
      DFFILE=TRIM(OUTDIR)//'/diel_func.dat'                        ! dielectric function file
      PZFILE=TRIM(OUTDIR)//'/pola_func.dat'                        ! polarization function
      SUFILE=TRIM(OUTDIR)//'/susc_func.dat'                        ! susceptibility function
      CDFILE=TRIM(OUTDIR)//'/cond_func.dat'                        ! electrical conductivity
!
      PDFILE=TRIM(OUTDIR)//'/plas_disp.dat'                        ! plasmon dispersion file
      EHFILE=TRIM(OUTDIR)//'/elec_hole.dat'                        ! electron-hole dispersion file
      E2FILE=TRIM(OUTDIR)//'/elec_hol2.dat'                        ! two electron-hole dispersion
      CKFILE=TRIM(OUTDIR)//'/int_pot_k.dat'                        ! interaction potential (k-space)
      CRFILE=TRIM(OUTDIR)//'/int_pot_r.dat'                        ! interaction potential (real space)
      PKFILE=TRIM(OUTDIR)//'/plas_kine.dat'                        ! plasmon kinetic energy file
!
      LFFILE=TRIM(OUTDIR)//'/loca_fiel.dat'                        ! local-field correction file G(q,om)
      IQFILE=TRIM(OUTDIR)//'/ginf_fiel.dat'                        ! G(q,inf) file
      SFFILE=TRIM(OUTDIR)//'/stru_fact.dat'                        ! structure factor file S(q,om)
      PCFILE=TRIM(OUTDIR)//'/pair_corr.dat'                        ! pair correlation function file
      P2FILE=TRIM(OUTDIR)//'/pair_dist.dat'                        ! pair distribution function file
      VXFILE=TRIM(OUTDIR)//'/vertex_fu.dat'                        ! vertex function Gamma(q,om)
      DCFILE=TRIM(OUTDIR)//'/plas_damp.dat'                        ! plasmon damping coefficient Im[eps]/q^2 
      MDFILE=TRIM(OUTDIR)//'/mome_dist.dat'                        ! momentum distribution
      LDFILE=TRIM(OUTDIR)//'/landau_pa.dat'                        ! Landau parameters
      DPFILE=TRIM(OUTDIR)//'/damp_file.dat'                        ! damping file
      LTFILE=TRIM(OUTDIR)//'/life_time.dat'                        ! plasmon lifetime file
      BRFILE=TRIM(OUTDIR)//'/broadenin.dat'                        ! plasmon broadening
      PEFILE=TRIM(OUTDIR)//'/plas_ener.dat'                        ! plasmon energy
      QCFILE=TRIM(OUTDIR)//'/qc_bounds.dat'                        ! plasmon q-bounds
      RLFILE=TRIM(OUTDIR)//'/rela_time.dat'                        ! relaxation time
      KSFILE=TRIM(OUTDIR)//'/screen_wv.dat'                        ! screening wave vector
      OQFILE=TRIM(OUTDIR)//'/omega_qvf.dat'                        ! omega = q * v_F file
      MEFILE=TRIM(OUTDIR)//'/moments_e.dat'                        ! moments of epsilon
      MSFILE=TRIM(OUTDIR)//'/moments_s.dat'                        ! moments of S(q,omega)
      MLFILE=TRIM(OUTDIR)//'/moments_l.dat'                        ! moments of loss function
      MCFILE=TRIM(OUTDIR)//'/moments_c.dat'                        ! moments of conductivity
      DEFILE=TRIM(OUTDIR)//'/deri_epsi.dat'                        ! derivative of Re[ dielectric function ]
      ZEFILE=TRIM(OUTDIR)//'/ree0_file.dat'                        ! Re[ dielectric function ] = 0
      SRFILE=TRIM(OUTDIR)//'/sum_rules.dat'                        ! sum rules for epsilon
      CWFILE=TRIM(OUTDIR)//'/confin_wf.dat'                        ! confinement wave function
      CFFILE=TRIM(OUTDIR)//'/confin_pt.dat'                        ! confinement potential
      EMFILE=TRIM(OUTDIR)//'/effe_mass.dat'                        ! effective mass
      MFFILE=TRIM(OUTDIR)//'/mean_path.dat'                        ! mean free path
      SPFILE=TRIM(OUTDIR)//'/spec_func.dat'                        ! spectral function
      SEFILE=TRIM(OUTDIR)//'/self_ener.dat'                        ! self-energy
      SBFILE=TRIM(OUTDIR)//'/subb_ener.dat'                        ! subband energies
      ESFILE=TRIM(OUTDIR)//'/elia_func.dat'                        ! Eliashberg function
      GRFILE=TRIM(OUTDIR)//'/grune_par.dat'                        ! Grüneisen parameter
      FDFILE=TRIM(OUTDIR)//'/fermi_dir.dat'                        ! Fermi-Dirac distribution
      BEFILE=TRIM(OUTDIR)//'/bose_eins.dat'                        ! Bose-Einstein distribution
      MXFILE=TRIM(OUTDIR)//'/maxwell_d.dat'                        ! Maxwell distribution
      SCFILE=TRIM(OUTDIR)//'/scale_par.dat'                        ! scale parameters
      DSFILE=TRIM(OUTDIR)//'/dens_stat.dat'                        ! density of states
      NVFILE=TRIM(OUTDIR)//'/neva_four.dat'                        ! Nevanlinaa/memory function
      MTFILE=TRIM(OUTDIR)//'/memo_time.dat'                        ! time domain memory function
!
      GPFILE=TRIM(OUTDIR)//'/gran_part.dat'                        ! grand partition function
      PRFILE=TRIM(OUTDIR)//'/epressure.dat'                        ! electronic pressure
      COFILE=TRIM(OUTDIR)//'/comp_file.dat'                        ! compressibility 
      CPFILE=TRIM(OUTDIR)//'/chem_pote.dat'                        ! chemical potential
      BMFILE=TRIM(OUTDIR)//'/bulk_modu.dat'                        ! bulk modulus
      SHFILE=TRIM(OUTDIR)//'/shear_mod.dat'                        ! shear modulus
      S0FILE=TRIM(OUTDIR)//'/zero_soun.dat'                        ! zero sound velocity
      S1FILE=TRIM(OUTDIR)//'/firs_soun.dat'                        ! first sound velocity
      DTFILE=TRIM(OUTDIR)//'/Debye_tmp.dat'                        ! Debye temperature
      PSFILE=TRIM(OUTDIR)//'/para_susc.dat'                        ! Pauli paramagnetic susceptibility
      IEFILE=TRIM(OUTDIR)//'/inter_ene.dat'                        ! internal energy
      EIFILE=TRIM(OUTDIR)//'/exces_ene.dat'                        ! excess internal energy
      FHFILE=TRIM(OUTDIR)//'/helm_free.dat'                        ! Helmholtz free energy
      EYFILE=TRIM(OUTDIR)//'/entropy_f.dat'                        ! entropy
!
      EFFILE=TRIM(OUTDIR)//'/fermi_ene.dat'                        ! Fermi energy
      KFFILE=TRIM(OUTDIR)//'/fermi_vec.dat'                        ! Fermi momentum
      VFFILE=TRIM(OUTDIR)//'/fermi_vel.dat'                        ! Fermi velocity
      TEFILE=TRIM(OUTDIR)//'/fermi_tmp.dat'                        ! Fermi temperature
      DLFILE=TRIM(OUTDIR)//'/fermi_dos.dat'                        ! Fermi density of states
!
      TWFILE=TRIM(OUTDIR)//'/thermal_w.dat'                        ! thermal De Broglie wavelength
      VTFILE=TRIM(OUTDIR)//'/thermal_v.dat'                        ! thermal velocity
      TCFILE=TRIM(OUTDIR)//'/thermal_c.dat'                        ! thermal conductivity
!
      EGFILE=TRIM(OUTDIR)//'/ground_st.dat'                        ! ground state energy
      EXFILE=TRIM(OUTDIR)//'/ex_energy.dat'                        ! exchange energy
      XCFILE=TRIM(OUTDIR)//'/xc_energy.dat'                        ! exchange correlation energy
      ECFILE=TRIM(OUTDIR)//'/corr_ener.dat'                        ! correlation energy
      HFFILE=TRIM(OUTDIR)//'/hf_energy.dat'                        ! Hartree-Fock energy
      EKFILE=TRIM(OUTDIR)//'/kine_ener.dat'                        ! kinetic energy
      EPFILE=TRIM(OUTDIR)//'/pote_ener.dat'                        ! potential energy
!
      VIFILE=TRIM(OUTDIR)//'/visc_coef.dat'                        ! shear viscosity
      DIFILE=TRIM(OUTDIR)//'/diff_coef.dat'                        ! diffusion coefficient
!      
      FPFILE=TRIM(OUTDIR)//'/fluct_pot.dat'                        ! fluctuation potential file
      ELFILE=TRIM(OUTDIR)//'/ener_loss.dat'                        ! energy loss function
      POFILE=TRIM(OUTDIR)//'/stop_powe.dat'                        ! stopping power
      RFFILE=TRIM(OUTDIR)//'/refrac_in.dat'                        ! refractive index
      VCFILE=TRIM(OUTDIR)//'/dyna_coul.dat'                        ! dynamic screened Coulomb potential V(q,omega)
!
! Corresponding fortran units
!
      FLIST(7)=DFFILE                                               !
      FLIST(8)=PZFILE                                               !
      FLIST(9)=SUFILE                                               !
      FLIST(10)=CDFILE                                              !
!
      FLIST(11)=PDFILE                                              !
      FLIST(12)=EHFILE                                              !
      FLIST(13)=E2FILE                                              !
      FLIST(14)=CKFILE                                              !
      FLIST(15)=CRFILE                                              !
      FLIST(16)=PKFILE                                              !
!
      FLIST(17)=LFFILE                                              !
      FLIST(18)=IQFILE                                              !
      FLIST(19)=SFFILE                                              !
      FLIST(20)=PCFILE                                              !
      FLIST(21)=P2FILE                                              !
      FLIST(22)=VXFILE                                              !
      FLIST(23)=DCFILE                                              !
      FLIST(24)=MDFILE                                              !
      FLIST(25)=LDFILE                                              !
      FLIST(26)=DPFILE                                              !
      FLIST(27)=LTFILE                                              !
      FLIST(28)=BRFILE                                              !
      FLIST(29)=PEFILE                                              !
      FLIST(30)=QCFILE                                              !
      FLIST(31)=RLFILE                                              !
      FLIST(32)=KSFILE                                              !
      FLIST(33)=OQFILE                                              !
      FLIST(34)=MEFILE                                              !
      FLIST(35)=MSFILE                                              !
      FLIST(36)=MLFILE                                              !
      FLIST(37)=MCFILE                                              !
      FLIST(38)=DEFILE                                              !
      FLIST(39)=ZEFILE                                              !
      FLIST(40)=SRFILE                                              !
      FLIST(41)=CWFILE                                              !
      FLIST(42)=CFFILE                                              !
      FLIST(43)=EMFILE                                              !
      FLIST(44)=MFFILE                                              !
      FLIST(45)=SPFILE                                              !
      FLIST(46)=SEFILE                                              !
      FLIST(47)=SBFILE                                              !
      FLIST(48)=ESFILE                                              !
      FLIST(49)=GRFILE                                              !
      FLIST(50)=FDFILE                                              !
      FLIST(51)=BEFILE                                              !
      FLIST(52)=MXFILE                                              !
      FLIST(53)=SCFILE                                              !
      FLIST(54)=DSFILE                                              !
      FLIST(55)=NVFILE                                              !
      FLIST(56)=MTFILE                                              !
!
      FLIST(57)=GPFILE                                              !
      FLIST(58)=PRFILE                                              !
      FLIST(59)=COFILE                                              !
      FLIST(60)=CPFILE                                              !
      FLIST(61)=BMFILE                                              !
      FLIST(62)=SHFILE                                              !
      FLIST(63)=S0FILE                                              !
      FLIST(64)=S1FILE                                              !
      FLIST(65)=DTFILE                                              !
      FLIST(66)=PSFILE                                              !
      FLIST(67)=IEFILE                                              !
      FLIST(68)=EIFILE                                              !
      FLIST(69)=FHFILE                                              !
      FLIST(70)=EYFILE                                              !
!
      FLIST(71)=EFFILE                                              !
      FLIST(72)=KFFILE                                              !
      FLIST(73)=VFFILE                                              !
      FLIST(74)=TEFILE                                              !
      FLIST(75)=DLFILE                                              !
!
      FLIST(76)=TWFILE                                              !
      FLIST(77)=VTFILE                                              !
      FLIST(78)=TCFILE                                              !
!
      FLIST(79)=EGFILE                                              !
      FLIST(80)=EXFILE                                              !
      FLIST(81)=XCFILE                                              !
      FLIST(82)=ECFILE                                              !
      FLIST(83)=HFFILE                                              !
      FLIST(84)=EKFILE                                              !
      FLIST(85)=EPFILE                                              !
!
      FLIST(86)=VIFILE                                              !
      FLIST(87)=DIFILE                                              !
!
      FLIST(88)=FPFILE                                              !
      FLIST(89)=ELFILE                                              !
      FLIST(90)=POFILE                                              !
      FLIST(91)=RFFILE                                              !
      FLIST(92)=VCFILE                                              !
!
      END SUBROUTINE OUT_FILES  
!
!=======================================================================
!
      SUBROUTINE CALC_TYPE(CLIST)
!
!  This subroutine stores the calculation approximations 
!    into the CLIST array
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 26 Jan 2021
!
!
      USE DIMENSION_CODE,    ONLY : NOFFN
!
      USE MATERIAL_PROP
      USE EXT_FIELDS
      USE CONFIN_VAL
      USE MULTILAYER
      USE SCREENING_TYPE
      USE PLASMA
      USE DF_VALUES
      USE PLASMON_DISPERSION
      USE LF_VALUES
      USE DAMPING_VALUES
      USE EL_ELE_INTER
      USE EL_PHO_INTER
      USE EL_IMP_INTER
      USE CLASSICAL_FLUID_VALUES
      USE SF_VALUES
      USE PC_VALUES
      USE PD_VALUES
      USE ENERGIES
      USE THERMO_PROPERTIES
      USE ION_BEAM
!
      IMPLICIT NONE
!
      CHARACTER (LEN =  100)  ::  CLIST(NOFFN)
      CHARACTER (LEN =  100)  ::  NONE
      CHARACTER (LEN =  100)  ::  LFTYPE
      CHARACTER (LEN =  100)  ::  SFTYPE
!
      IF(GSTDY == ' STATIC') THEN                                   !
        LFTYPE = GQ_TYPE                                            !
      ELSE                                                          !
        LFTYPE = GQO_TYPE                                           !
      END IF                                                        !
      IF(SSTDY == ' STATIC') THEN                                   !
        SFTYPE = SQ_TYPE                                            !
      ELSE                                                          !
        SFTYPE = SQO_TYPE                                           !
      END IF                                                        !
!
      NONE = ''                                                     !
!
      CLIST(7)  = D_FUNC                                            !
      CLIST(8)  = D_FUNC                                            !
      CLIST(9)  = D_FUNC                                            !
      CLIST(10) = D_FUNC                                            !
!
      CLIST(11) = PL_DISP                                           !
      CLIST(12) = NONE                                              !
      CLIST(13) = NONE                                              !
      CLIST(14) = SC_TYPE                                           !
      CLIST(15) = NONE                                              !
      CLIST(16) = NONE                                              !
!
      CLIST(17) = LFTYPE                                            !
      CLIST(18) = IQ_TYPE                                           !
      CLIST(19) = SFTYPE                                            !
      CLIST(20) = GR_TYPE                                           !
      CLIST(21) = RH_TYPE                                           !
      CLIST(22) = D_FUNC                                            !
      CLIST(23) = D_FUNC                                            !
      CLIST(24) = NONE                                              !
      CLIST(25) = LANDAU                                            !
      CLIST(26) = DAMPING                                           !
      CLIST(27) = LT_TYPE                                           !
      CLIST(28) = NONE                                              !
      CLIST(29) = D_FUNC                                            !
      CLIST(30) = D_FUNC                                            !
      CLIST(31) = RT_TYPE                                           !
      CLIST(32) = SC_TYPE                                           !
      CLIST(33) = NONE                                              !
      CLIST(34) = D_FUNC                                            !
      CLIST(35) = SFTYPE                                            !
      CLIST(36) = D_FUNC                                            !
      CLIST(37) = D_FUNC                                            !
      CLIST(38) = D_FUNC                                            !
      CLIST(39) = D_FUNC                                            !
      CLIST(40) = D_FUNC                                            !
      CLIST(41) = CONFIN                                            !
      CLIST(42) = CONFIN                                            !
      CLIST(43) = NONE                                              !
      CLIST(44) = NONE                                              !
      CLIST(45) = D_FUNC                                            !
      CLIST(46) = D_FUNC                                            !
      CLIST(47) = CONFIN                                            !
      CLIST(48) = NONE                                              !
      CLIST(49) = NONE                                              !
      CLIST(50) = NONE                                              !
      CLIST(51) = NONE                                              !
      CLIST(52) = NONE                                              !
      CLIST(53) = NONE                                              !
      CLIST(54) = NONE                                              !
      CLIST(55) = NEV_TYPE                                          !
      CLIST(56) = MEM_TYPE                                          !
!
      CLIST(57) = GP_TYPE                                           !
      CLIST(58) = TH_PROP                                           !
      CLIST(59) = TH_PROP                                           !
      CLIST(60) = TH_PROP                                           !
      CLIST(61) = TH_PROP                                           !
      CLIST(62) = TH_PROP                                           !
      CLIST(63) = TH_PROP                                           !
      CLIST(64) = TH_PROP                                           !
      CLIST(65) = TH_PROP                                           !
      CLIST(66) = TH_PROP                                           !
      CLIST(67) = TH_PROP                                           !
      CLIST(68) = TH_PROP                                           !
      CLIST(60) = TH_PROP                                           !
      CLIST(70) = TH_PROP                                           !
!
      CLIST(71) = NONE                                              !
      CLIST(72) = NONE                                              !
      CLIST(73) = NONE                                              !
      CLIST(74) = NONE                                              !
      CLIST(75) = NONE                                              !
!
      CLIST(76) = NONE                                              !
      CLIST(77) = NONE                                              !
      CLIST(78) = NONE                                              !
!
      CLIST(79) = NONE                                              !
      CLIST(80) = NONE                                              !
      CLIST(81) = EXC_TYPE                                          !
      CLIST(82) = EC_TYPE                                           !
      CLIST(83) = NONE                                              !
      CLIST(84) = NONE                                              !
      CLIST(85) = NONE                                              !
!
      CLIST(86) = VI_TYPE                                           !
      CLIST(87) = DC_TYPE                                           !
!
      CLIST(88) = D_FUNC                                            !
      CLIST(89) = D_FUNC                                            !
      CLIST(90) = D_FUNC                                            !
      CLIST(91) = D_FUNC                                            !
      CLIST(92) = D_FUNC                                            !
!
      END SUBROUTINE CALC_TYPE
!
!=======================================================================
!
      FUNCTION INDEX_FILE(I_UNIT)
!
!  This function associates each Fortran unit to an index (0/1) 
!   which determines whether the file has to indexed by the 
!   input data file number (0) or not (1), when looping 
!   over these input data files. In practice:
!
!       * Files depending on q, omega, r have to be indexed
!       * files depending on r_s, T must not be indexed
!
!   For instance, if we want to compute the correlation energy 
!    as a function of r_s, we will loop on the input data files 
!    with different r_s, but all results have to be written 
!    into the same output file 'OUTDIR/corr_ener.dat'
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 26 Jan 2021
!
!
      USE DIMENSION_CODE,    ONLY : NOFFN
!
      IMPLICIT NONE
!
      INTEGER, INTENT(IN)     ::  I_UNIT
      INTEGER                 ::  INDEX_FILE
      INTEGER                 ::  ID(NOFFN)
!
      DATA ID( 1) / 0 /       ! \
      DATA ID( 2) / 0 /       !  \
      DATA ID( 3) / 0 /       !   \>>  Fortran units not used
      DATA ID( 4) / 0 /       !   />>  for output files
      DATA ID( 5) / 0 /       !  /
      DATA ID( 6) / 0 /       ! / 
      DATA ID( 7) / 0 /       ! dielectric function file
      DATA ID( 8) / 0 /       ! polarization function 
      DATA ID( 9) / 0 /       ! susceptibility function
      DATA ID(10) / 0 /       ! electrical conductivity
!
      DATA ID(11) / 0 /       ! plasmon dispersion file 
      DATA ID(12) / 0 /       ! electron-hole dispersion file 
      DATA ID(13) / 0 /       ! two electron-hole dispersion 
      DATA ID(14) / 0 /       ! screened Coulomb (k-space) 
      DATA ID(15) / 0 /       ! screened Coulomb (real space) 
      DATA ID(16) / 0 /       ! plasmon kinetic energy 
!
      DATA ID(17) / 0 /       ! local-field correction file G(q,om)
      DATA ID(18) / 0 /       ! G(q,inf) file 
      DATA ID(19) / 0 /       ! structure factor file S(q,om) 
      DATA ID(20) / 0 /       ! pair correlation function file  
      DATA ID(21) / 0 /       ! pair distribution function file  
      DATA ID(22) / 0 /       ! vertex function Gamma(q,om) 
      DATA ID(23) / 0 /       ! plasmon damping coefficient Im[eps]/q^2 
      DATA ID(24) / 0 /       ! momentum distribution 
      DATA ID(25) / 0 /       ! Landau parameters 
      DATA ID(26) / 0 /       ! damping file 
      DATA ID(27) / 0 /       ! plasmon lifetime file 
      DATA ID(28) / 0 /       ! plasmon broadening 
      DATA ID(29) / 0 /       ! plasmon energy 
      DATA ID(30) / 0 /       ! plasmon q-bounds 
      DATA ID(31) / 0 /       ! relaxation time 
      DATA ID(32) / 0 /       ! screening wave vector 
      DATA ID(33) / 0 /       ! Debye wave vector 
      DATA ID(34) / 0 /       ! moments of epsilon 
      DATA ID(35) / 0 /       ! moments of S(q,omega) 
      DATA ID(36) / 0 /       ! moments of loss function 
      DATA ID(37) / 0 /       ! moments of conductivity 
      DATA ID(38) / 0 /       ! derivative of Re[ dielectric function ] 
      DATA ID(39) / 0 /       ! Re[ dielectric function ] = 0 
      DATA ID(40) / 0 /       ! sum rules for epsilon 
      DATA ID(41) / 0 /       ! confinement wave function 
      DATA ID(42) / 0 /       ! confinement potential 
      DATA ID(43) / 0 /       ! effective mass 
      DATA ID(44) / 0 /       ! mean free path 
      DATA ID(45) / 0 /       ! spectral function 
      DATA ID(46) / 0 /       ! self-energy 
      DATA ID(47) / 1 /       ! subband energies
      DATA ID(48) / 0 /       ! Eliashberg function 
      DATA ID(49) / 0 /       ! Grüneisen parameter
      DATA ID(50) / 0 /       ! Fermi-Dirac distribution
      DATA ID(51) / 0 /       ! Bose-Einstein distribution  
      DATA ID(52) / 0 /       ! Maxwell distribution
      DATA ID(53) / 1 /       ! scale parameters  
      DATA ID(54) / 0 /       ! density of states  
      DATA ID(55) / 0 /       ! Nevanlinaa function 
      DATA ID(56) / 0 /       ! memory function 
!
      DATA ID(57) / 1 /       ! grand partition function  
      DATA ID(58) / 1 /       ! electronic pressure  
      DATA ID(59) / 1 /       ! compressibility  
      DATA ID(60) / 1 /       ! chemical potential  
      DATA ID(61) / 1 /       ! bulk modulus  
      DATA ID(62) / 1 /       ! shear modulus  
      DATA ID(63) / 1 /       ! zero sound velocity  
      DATA ID(64) / 1 /       ! first sound velocity  
      DATA ID(65) / 1 /       ! Debye temperature 
      DATA ID(66) / 1 /       ! Pauli paramagnetic susceptibility  
      DATA ID(67) / 1 /       ! internal energy  
      DATA ID(68) / 1 /       ! excess internal energy  
      DATA ID(69) / 1 /       ! Helmholtz free energy  
      DATA ID(70) / 1 /       ! entropy  
!
      DATA ID(71) / 1 /       ! Fermi energy  
      DATA ID(72) / 1 /       ! Fermi momentum  
      DATA ID(73) / 1 /       ! Fermi velocity 
      DATA ID(74) / 1 /       ! Fermi temperature  
      DATA ID(75) / 1 /       ! Fermi density of states  
!
      DATA ID(76) / 1 /       ! thermal De Broglie wavelength 
      DATA ID(77) / 1 /       ! thermal velocity 
      DATA ID(78) / 1 /       ! thermal conductivity 
!
      DATA ID(79) / 1 /       ! ground state energy 
      DATA ID(80) / 1 /       ! exchange energy 
      DATA ID(81) / 1 /       ! exchange correlation energy 
      DATA ID(82) / 1 /       ! correlation energy 
      DATA ID(83) / 1 /       ! Hartree-Fock energy 
      DATA ID(84) / 1 /       ! kinetic energy 
      DATA ID(85) / 1 /       ! potential energy 
!
      DATA ID(86) / 1 /       ! shear viscosity 
      DATA ID(87) / 1 /       ! diffusion coefficient 
!
      DATA ID(88) / 0 /       ! fluctuation potential file 
      DATA ID(89) / 0 /       ! energy loss function 
      DATA ID(90) / 0 /       ! stopping power 
      DATA ID(91) / 0 /       ! refractive index
      DATA ID(92) / 0 /       ! dynamic screened Coulomb potential V(q,omega) 
!
      INDEX_FILE = ID(I_UNIT)                                       !
!
      END FUNCTION INDEX_FILE
!
END MODULE OUTFILES
