!
!=======================================================================
!
MODULE DISP_COEF_EH
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  AE(0:6)
!
END MODULE DISP_COEF_EH
 !
!=======================================================================
!
MODULE DISP_COEF_REAL
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)              ::  AR(0:6)
!
END MODULE DISP_COEF_REAL
!
!=======================================================================
!
MODULE DISP_COEF_COMP
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      COMPLEX (WP)          ::  AC(0:6)
!
END MODULE DISP_COEF_COMP

