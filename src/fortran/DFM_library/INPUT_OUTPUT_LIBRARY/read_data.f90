!
!=======================================================================
!
MODULE INPUT_DATA
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE READ_DATA
!
!  This subroutine reads the input data file of the epsilon.f90 code,
!    and either stores them into modules or pass them as arguments
!
!
!
!   Author :  D. Sébilleau
!
!                                            Last modified :  6 Aug 2021
!
!  Modules storing the data
!
      USE MATERIAL_PROP
      USE EXT_FIELDS
      USE Q_GRID
      USE E_GRID
      USE R_GRID
      USE CONFIN_VAL
      USE MULTILAYER
      USE UNITS
      USE SCREENING_TYPE
      USE PLASMA
      USE CALCTYPE
      USE DF_VALUES
      USE PLASMON_DISPERSION
      USE LF_VALUES
      USE DAMPING_VALUES
      USE EL_ELE_INTER
      USE EL_PHO_INTER
      USE EL_IMP_INTER
      USE CLASSICAL_FLUID_VALUES
      USE SF_VALUES
      USE PC_VALUES
      USE PD_VALUES
      USE SPF_VALUES
      USE ENERGIES
      USE SPIN_POLARIZATION
      USE THERMO_PROPERTIES
      USE ELECTRON_MEAN_FREE_PATH
      USE MOMENTS
      USE ION_BEAM
!
      USE OUT_VALUES_1
      USE OUT_VALUES_2
      USE OUT_VALUES_3
      USE OUT_VALUES_4
      USE OUT_VALUES_5
      USE OUT_VALUES_6
      USE OUT_VALUES_7
      USE OUT_VALUES_8
      USE OUT_VALUES_9
      USE OUT_VALUES_10
      USE OUT_VALUES_P
!
      USE OUT_CALC
!
      IMPLICIT NONE
!
      INTEGER               ::  N_HEAD,N_SEP,N_INT
      INTEGER               ::  I
!
      CHARACTER (LEN = 8)   ::  DUMMY
!
!  Separation blocks to skip when reading
!
      N_HEAD=8                                                      !
      N_SEP=3                                                       !
      N_INT=1                                                       !
!
!  Reading the input data file
!
      DO I=1,N_HEAD                                                 !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,10) Q_MIN,Q_MAX,N_Q                                    !
      READ(5,11) E_MIN,E_MAX,N_E                                    !
      READ(5,12) R_MIN,R_MAX,N_R                                    !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,13) RS,MSOM,MAT_TYP,EPS_B                              !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,14) T,E,H,FLD                                          !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,15) DMN                                                !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,16) R0,L,OM0,CONFIN                                    !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,17) DL,D1,N_DEP,N_INV                                  !
      READ(5,18) H_TYPE,EPS_1,EPS_2                                 !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,19) UNIT,UNIK                                          !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,20) SC_TYPE                                            !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,21) PL_TYPE,ZION,ZION2                                 !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,22)  CAL_TYPE                                          !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,23) ESTDY,EPS_T,D_FUNC,I_T                             !
      READ(5,24) NEV_TYPE,MEM_TYPE,ALPHA,BETA                       !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,25) PL_DISP                                            !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,26) GSTDY,GQ_TYPE,IQ_TYPE                              !
      READ(5,27) LANDAU,GQO_TYPE,G0_TYPE,GI_TYPE                    !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,28) DAMPING,LT_TYPE,RT_TYPE                            !
      READ(5,29) DR_TYPE,DC_TYPE,VI_TYPE                            !
      READ(5,30) EE_TYPE,EP_TYPE,EI_TYPE                            !
      READ(5,31) IP_TYPE,PD_TYPE,QD_TYPE,ZETA                       !
      READ(5,32) D_VALUE_1,POWER_1,EK                               !
      READ(5,33) D_VALUE_2,POWER_2,PCT                              !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,34) INT_POT,S,EPS,DELTA                                !
      READ(5,35) RC,ALF,M,N                                         !
      READ(5,36) A1,A2,A3,A4                                        !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,37) EP_C,DEBYE_T                                       !
      READ(5,38) NA,MA,RA                                           !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,39) NI,EI_C                                            !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,40) CF_TYPE,PF_TYPE,SL_TYPE                            !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,41) SSTDY,SQ_TYPE,SQO_TYPE                             !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,42) GR_TYPE,GR0_MODE                                   !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,43) RH_TYPE                                            !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,44) SPF_TYPE                                           !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,45) EC_TYPE,FXC_TYPE,EXC_TYPE                          !
      READ(5,46) EX_TYPE,EK_TYPE
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,47) IMODE,XI
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,48) TH_PROP,GP_TYPE                                    !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,49) EK_INI,EK_FIN                                      !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,50) N_M,M_TYPE                                         !
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,51) Z_BEAM,EK_BEAM                                     !
!
!  Reading the print switches I_XX
!
      DO I=1,N_SEP                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,201) I_DF,I_PZ,I_SU,I_CD                               !
!
      DO I=1,N_INT                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,202) I_PD,I_EH,I_E2,I_CK                               !
      READ(5,204) I_CR,I_PK                                         !
!
      DO I=1,N_INT                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,205) I_LF,I_IQ,I_SF,I_PC                               !
      READ(5,206) I_P2,I_VX,I_DC,I_MD                               !
      READ(5,207) I_LD,I_DP,I_LT,I_BR                               !
      READ(5,208) I_PE,I_QC,I_RL,I_KS                               !
      READ(5,209) I_OQ,I_ME,I_MS,I_ML                               !
      READ(5,210) I_MC,I_DE,I_ZE,I_SR                               !
      READ(5,211) I_CW,I_CF,I_EM,I_MF                               !
      READ(5,212) I_SP,I_SE,I_SB,I_ES                               !
      READ(5,213) I_GR,I_FD,I_BE,I_MX                               !
      READ(5,214) I_SC,I_DS,I_NV,I_MT                               !
!
      DO I=1,N_INT                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,215) I_GP,I_PR,I_CO,I_CP                               !
      READ(5,216) I_BM,I_SH,I_S0,I_S1                               !
      READ(5,217) I_DT,I_PS,I_IE,I_EI                               !
      READ(5,218) I_FH,I_EY                                         !
!
      DO I=1,N_INT                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,219) I_EF,I_KF,I_VF,I_TE                               !
      READ(5,220) I_DL                                              !
!
      DO I=1,N_INT                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,221) I_TW,I_VT,I_TC                                    !
!
      DO I=1,N_INT                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,222) I_EG,I_EX,I_XC,I_EC                               !
      READ(5,223) I_HF,I_EK,I_EP                                    !
!
      DO I=1,N_INT                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,224) I_VI,I_DI                                         !
!
      DO I=1,N_INT                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,225) I_FP,I_EL,I_PO,I_RF                               !
      READ(5,226) I_VC                                              !
!
      DO I=1,N_INT                                                  !
        READ(5,5) DUMMY                                             !
      END DO                                                        !
!
      READ(5,227) I_FN,I_WR,I_TI                                    !
!
!  Switches to call specific calculators
!
      I_C1 = I_DF + I_PZ + I_SU + I_CD                              !
      I_C2 = I_PD + I_EH + I_E2 + I_CK + I_CR + I_PK                !
      I_C3 = I_LF + I_IQ + I_SF + I_PC + I_P2 + I_VX + I_DC +     & !
             I_MD + I_LD + I_DP + I_LT + I_BR + I_PE + I_QC +     & !
             I_RL + I_KS + I_OQ + I_ME + I_MS + I_ML + I_MC +     & !
             I_DE + I_ZE + I_SR + I_CW + I_CF + I_EM + I_MF +     & !
             I_SP + I_SE + I_SB + I_ES + I_GR + I_FD + I_BE +     & !
             I_MX + I_SC + I_DS + I_NV + I_MT                       !
      I_C4 = I_GP + I_PR + I_CO + I_CP + I_BM + I_SH + I_S0 +     & !
             I_S1 + I_DT + I_PS + I_IE + I_EI + I_FH + I_EY         !
      I_C5 = I_EF + I_KF + I_VF + I_TE + I_DL                       !
      I_C6 = I_TW + I_VT + I_TC                                     !
      I_C7 = I_EG + I_EX + I_XC + I_EC + I_HF + I_EK + I_EP         !
      I_C8 = I_VI + I_DI                                            !
      I_C9 = I_EL + I_PO + I_RF + I_VC                              !
      I_PP = I_FP + I_PD                                            !
!
!  Steps in q/k_F , (h_bar omega)/E_F and k_F*r
!
        IF(N_Q == 1) THEN                                           !
          Q_STEP = 1                                                !
        ELSE                                                        ! step in Q
          Q_STEP = (Q_MAX - Q_MIN) / (FLOAT(N_Q-1))                 !
        END IF                                                      !
!
        IF(N_E == 1) THEN                                           !
          E_STEP = 1                                                !
        ELSE                                                        ! step in E
          E_STEP = (E_MAX - E_MIN) / (FLOAT(N_E-1))                 !
        END IF                                                      !
!
        IF(N_R == 1) THEN                                           !
          R_STEP = 1                                                !
        ELSE                                                        ! step in R
          R_STEP = (R_MAX - R_MIN)  /(FLOAT(N_R-1))                 !
        END IF                                                      !
!
!  Writing into the log file
!
!
      WRITE(6,401)                                                  !
      WRITE(6,402)                                                  !
      WRITE(6,403)                                                  !
      WRITE(6,404)                                                  !
      WRITE(6,405)                                                  !
      WRITE(6,406)                                                  !
      WRITE(6,407)                                                  !
!
      WRITE(6,601)                                                  !
      WRITE(6,702)                                                  !
      WRITE(6,601)                                                  !
      WRITE(6,801)                                                  !
      WRITE(6,602)                                                  !
!
      WRITE(6,110) Q_MIN,Q_MAX,N_Q                                  !
      WRITE(6,111) E_MIN,E_MAX,N_E                                  !
      WRITE(6,112) R_MIN,R_MAX,N_R                                  !
!
      WRITE(6,602)                                                  !
      WRITE(6,802)                                                  !
      WRITE(6,602)                                                  !
!
      WRITE(6,113) RS,MSOM,MAT_TYP,EPS_B                            !
!
      WRITE(6,602)                                                  !
      WRITE(6,803)                                                  !
      WRITE(6,602)                                                  !
!
      WRITE(6,114) T,E,H,FLD                                        !
!
      WRITE(6,602)                                                  !
      WRITE(6,804)                                                  !
!
      WRITE(6,115) DMN                                              !
!
      WRITE(6,602)                                                  !
      WRITE(6,805)                                                  !
      WRITE(6,602)                                                  !
!
      WRITE(6,116) R0,L,OM0,CONFIN                                  !
!
      WRITE(6,602)                                                  !
      WRITE(6,806)                                                  !
      WRITE(6,602)                                                  !
!
      WRITE(6,117) DL,D1,N_DEP,N_INV                                !
      WRITE(6,118) H_TYPE,EPS_1,EPS_2                               !
!
      WRITE(6,602)                                                  !
      WRITE(6,807)                                                  !
      WRITE(6,602)                                                  !
!
      WRITE(6,119) UNIT,UNIK                                        !
!
      WRITE(6,602)                                                  !
      WRITE(6,808)                                                  !
      WRITE(6,602)                                                  !
!
      WRITE(6,120) SC_TYPE                                          !
!
      WRITE(6,602)                                                  !
      WRITE(6,809)                                                  !
      WRITE(6,602)                                                  !
!
      WRITE(6,121) PL_TYPE,ZION,ZION2                               !
!
      WRITE(6,602)                                                  !
      WRITE(6,810)                                                  !
      WRITE(6,602)                                                  !
!
      WRITE(6,122) CAL_TYPE
!
      WRITE(6,601)                                                  !
      WRITE(6,703)                                                  !
      WRITE(6,601)                                                  !
!
      WRITE(6,123) ESTDY,EPS_T,D_FUNC,I_T                           !
!
      WRITE(6,124) NEV_TYPE,MEM_TYPE,ALPHA,BETA                     !
!
      WRITE(6,602)                                                  !
      WRITE(6,811)                                                  !
      WRITE(6,602)                                                  !
!
      WRITE(6,125) PL_DISP                                          !
!
      WRITE(6,602)                                                  !
      WRITE(6,812)                                                  !
      WRITE(6,602)                                                  !
!
      WRITE(6,126) GSTDY,GQ_TYPE,IQ_TYPE                            !
      WRITE(6,127) LANDAU,GQO_TYPE,G0_TYPE,GI_TYPE                  !
!
      WRITE(6,602)                                                  !
      WRITE(6,813)                                                  !
      WRITE(6,602)                                                  !
!
      WRITE(6,128) DAMPING,LT_TYPE,RT_TYPE                          !
      WRITE(6,129) DR_TYPE,DC_TYPE,VI_TYPE                          !
      WRITE(6,130) EE_TYPE,EP_TYPE,EI_TYPE                          !
      WRITE(6,131) IP_TYPE,PD_TYPE,QD_TYPE,ZETA                     !
      WRITE(6,132) D_VALUE_1,POWER_1,EK                             !
      WRITE(6,133) D_VALUE_2,POWER_2,PCT                            !
!
      WRITE(6,602)                                                  !
      WRITE(6,814)                                                  !
      WRITE(6,602)                                                  !
!
      WRITE(6,134) INT_POT,S,EPS,DELTA                              !
      WRITE(6,135) RC,ALF,M,N                                       !
      WRITE(6,136) A1,A2,A3,A4                                      !
!
      WRITE(6,602)                                                  !
      WRITE(6,815)                                                  !
      WRITE(6,602)                                                  !
!
      WRITE(6,137) EP_C,DEBYE_T                                     !
      WRITE(6,138) NA,MA,RA                                         !
!
      WRITE(6,602)                                                  !
      WRITE(6,816)                                                  !
      WRITE(6,602)                                                  !
!
      WRITE(6,139) NI,EI_C                                          !
!
      WRITE(6,602)                                                  !
      WRITE(6,817)                                                  !
      WRITE(6,602)                                                  !
!
      WRITE(6,140) CF_TYPE,PF_TYPE,SL_TYPE                          !
!
      WRITE(6,601)                                                  !
      WRITE(6,704)                                                  !
      WRITE(6,601)                                                  !
!
      WRITE(6,141) SSTDY,SQ_TYPE,SQO_TYPE                           !
!
      WRITE(6,601)                                                  !
      WRITE(6,705)                                                  !
      WRITE(6,601)                                                  !
!
      WRITE(6,142) GR_TYPE,GR0_MODE                                 !
!
      WRITE(6,601)                                                  !
      WRITE(6,706)                                                  !
      WRITE(6,601)                                                  !
!
      WRITE(6,143) RH_TYPE                                          !
!
      WRITE(6,601)                                                  !
      WRITE(6,707)                                                  !
      WRITE(6,601)                                                  !
!
      WRITE(6,144) SPF_TYPE                                         !
!
      WRITE(6,601)                                                  !
      WRITE(6,708)                                                  !
      WRITE(6,601)                                                  !
!
!
      WRITE(6,145) EC_TYPE,FXC_TYPE,EXC_TYPE                        !
      WRITE(6,146) EX_TYPE,EK_TYPE                                  !
!
      WRITE(6,601)                                                  !
      WRITE(6,709)                                                  !
      WRITE(6,601)                                                  !
!
      WRITE(6,147) IMODE,XI
!
      WRITE(6,601)                                                  !
      WRITE(6,710)                                                  !
      WRITE(6,601)                                                  !
!
      WRITE(6,148) TH_PROP,GP_TYPE                                  !
!
      WRITE(6,601)                                                  !
      WRITE(6,711)                                                  !
      WRITE(6,601)                                                  !
!
      WRITE(6,149) EK_INI,EK_FIN                                    !
!
      WRITE(6,601)                                                  !
      WRITE(6,712)                                                  !
      WRITE(6,601)                                                  !
!
      WRITE(6,150) N_M,M_TYPE                                       !
!
      WRITE(6,601)                                                  !
      WRITE(6,713)                                                  !
      WRITE(6,601)                                                  !
!
      WRITE(6,151) Z_BEAM,EK_BEAM                                   !
!
!  Writing the print switches
!
      WRITE(6,601)                                                  !
      WRITE(6,714)                                                  !
      WRITE(6,601)                                                  !
!
      WRITE(6,301) I_DF,I_PZ,I_SU,I_CD                              !
!
      WRITE(6,602)                                                  !
!
      WRITE(6,302) I_PD,I_EH,I_E2,I_CK                              !
      WRITE(6,304) I_CR,I_PK                                        !
!
      WRITE(6,602)                                                  !
!
      WRITE(6,305) I_LF,I_IQ,I_SF,I_PC                              !
      WRITE(6,306) I_P2,I_VX,I_DC,I_MD                              !
      WRITE(6,307) I_LD,I_DP,I_LT,I_BR                              !
      WRITE(6,308) I_PE,I_QC,I_RL,I_KS                              !
      WRITE(6,309) I_OQ,I_ME,I_MS,I_ML                              !
      WRITE(6,310) I_MC,I_DE,I_ZE,I_SR                              !
      WRITE(6,311) I_CW,I_CF,I_EM,I_MF                              !
      WRITE(6,312) I_SP,I_SE,I_SB,I_ES                              !
      WRITE(6,313) I_GR,I_FD,I_BE,I_MX                              !
      WRITE(6,314) I_SC,I_DS,I_NV,I_MT                              !
!
      WRITE(6,602)                                                  !
!
      WRITE(6,315) I_GP,I_PR,I_CO,I_CP                              !
      WRITE(6,316) I_BM,I_SH,I_S0,I_S1                              !
      WRITE(6,317) I_DT,I_PS,I_IE,I_EI                              !
      WRITE(6,318) I_FH,I_EY                                        !
!
      WRITE(6,602)                                                  !
!
      WRITE(6,319) I_EF,I_KF,I_VF,I_TE                              !
      WRITE(6,320) I_DL                                             !
!
!
      WRITE(6,321) I_TW,I_VT,I_TC                                   !
!
      WRITE(6,602)                                                  !
!
      WRITE(6,322) I_EG,I_EX,I_XC,I_EC                              !
      WRITE(6,323) I_HF,I_EK,I_EP                                   !
!
      WRITE(6,602)                                                  !
!
      WRITE(6,324) I_VI,I_DI                                        !
!
      WRITE(6,602)                                                  !
!
      WRITE(6,325) I_FP,I_EL,I_PO,I_RF                              !
      WRITE(6,326) I_VC                                             !
!
      WRITE(6,602)                                                  !
!
      WRITE(6,327) I_FN,I_WR,I_TI                                   !
!
      WRITE(6,601)                                                  !
!
      WRITE(6,500)                                                  !
      WRITE(6,500)                                                  !
      WRITE(6,407)                                                  !
!
!  Formats: Reading standard input data
!
   5  FORMAT(A8)
!
  10  FORMAT(6X,F7.3,3X,F7.3,2X,I5)
  11  FORMAT(6X,F7.3,3X,F7.3,2X,I5)
  12  FORMAT(6X,F7.3,3X,F7.3,2X,I5)
  13  FORMAT(6X,F7.3,3X,F7.3,2X,A5,4X,F9.3)
  14  FORMAT(3X,F10.3,3X,F7.3,3X,F7.3,5X,A2)
  15  FORMAT(8X,A2)
  16  FORMAT(6X,F7.3,3X,F7.3,3X,F6.2,1X,A7)
  17  FORMAT(6X,F7.3,3X,F7.3,3X,F7.3,3X,F7.3)
  18  FORMAT(6X,A4,4X,F9.3,1X,F9.3)
  19  FORMAT(7X,A3,8X,A2)
  20  FORMAT(8X,A2)
  21  FORMAT(7X,A3,6X,F7.3,3X,F7.3)
  22  FORMAT(3X,A7)
  23  FORMAT(3X,A7,6X,A4,6X,A4,9X,I1)
  24  FORMAT(6X,A4,6X,A4,8X,F5.3,5X,F5.3)
  25  FORMAT(3X,A7)
  26  FORMAT(3X,A7,6X,A4,7X,A3)
  27  FORMAT(6X,A4,6X,A4,8X,A2,8X,A2)
  28  FORMAT(6X,A4,6X,A4,7X,A3)
  29  FORMAT(6X,A4,6X,A4,6X,A4)
  30  FORMAT(6X,A4,6X,A4,6X,A4)
  31  FORMAT(6X,A4,6X,A4,6X,A4,4X,F9.3)
  32  FORMAT(6X,F7.3,2X,A5,4X,F9.3)
  33  FORMAT(6X,F7.3,2X,A5,8X,F4.2)
  34  FORMAT(5X,A5,6X,F7.3,2X,F8.3,3X,F7.3)
  35  FORMAT(6X,F7.3,3X,F7.3,5X,I2,8X,I2)
  36  FORMAT(6X,F7.3,3X,F7.3,3X,F7.3,3X,F7.3)
  37  FORMAT(5X,F8.3,2X,F8.3)
  38  FORMAT(5X,F8.3,2X,F8.3,2X,F8.3)
  39  FORMAT(5X,F8.3,2X,F8.3)
  40  FORMAT(7X,A3,7X,A3,7X,A3)
  41  FORMAT(3X,A7,7X,A3,7X,A3)
  42  FORMAT(7X,A3,6X,A4)
  43  FORMAT(7X,A3)
  44  FORMAT(6X,A4)
  45  FORMAT(4X,A6,8X,A2,8X,A2)
  46  FORMAT(7X,A3,7X,A3)
  47  FORMAT(9X,I1,6X,F7.3)
  48  FORMAT(6X,A4,7X,A3)
  49  FORMAT(5X,F9.3,1X,F9.3)
  50  FORMAT(8X,I2,7X,A3)
  51  FORMAT(6X,F6.2,1X,F9.2)
!
!  Formats: Writing standard input data
!
 110  FORMAT(6X,F7.3,3X,F7.3,2X,I5,19X,'Q_MIN,Q_MAX,N_Q')
 111  FORMAT(6X,F7.3,3X,F7.3,2X,I5,19X,'E_MIN,E_MAX,N_E')
 112  FORMAT(6X,F7.3,3X,F7.3,2X,I5,19X,'R_MIN,R_MAX,N_R')
 113  FORMAT(6X,F7.3,3X,F7.3,2X,A5,4X,F9.3,6X,'RS,MSOM,MAT_TYP,EPS_B')
 114  FORMAT(3X,F10.3,3X,F7.3,3X,F7.3,5X,A2,9X,'T,E,H,FLD')
 115  FORMAT(8X,A2,39X,'DIM')
 116  FORMAT(6X,F7.3,3X,F7.3,3X,F6.2,1X,A7,9X,'R0,L,OM0,CONFIN')
 117  FORMAT(6X,F7.3,3X,F7.3,3X,F7.3,3X,F7.3,6X,'DL,D1,N_DEP,N_INV')
 118  FORMAT(6X,A4,4X,F9.3,1X,F9.3,16X,'H_TYPE,EPS_1,EPS_2')
 119  FORMAT(7X,A3,8X,A2,29X,'UNIT,UNIK')
 120  FORMAT(8X,A2,39X,'SC_TYPE')
 121  FORMAT(7X,A3,6X,F7.3,3X,F7.3,16X,'PL_TYPE,ZION,ZION2')
 122  FORMAT(3X,A7,39X,'CAL_TYPE')
 123  FORMAT(3X,A7,6X,A4,6X,A4,9X,I1,9X,'ESTDY,EPS_T,D_FUNC,', &
             'I_T')
 124  FORMAT(6X,A4,6X,A4,8X,F5.3,5X,F5.3,6X,'NEV_TYPE,MEM_TYPE,ALPHA,BETA')
 125  FORMAT(3X,A7,39X,'PL_DISP')
 126  FORMAT(3X,A7,6X,A4,7X,A3,19X,'GSTDY,GQ_TYPE,IQ_TYPE')
 127  FORMAT(6X,A4,6X,A4,8X,A2,8X,A2,9X,'LANDAU,GQO_TYPE,G0_TYPE,GI_TYPE')
 128  FORMAT(6X,A4,6X,A4,7X,A3,19X,'DAMPING,LT_TYPE,RT_TYPE')
 129  FORMAT(6X,A4,6X,A4,6X,A4,19X,'DR_TYPE,DC_TYPE,VI_TYPE')
 130  FORMAT(6X,A4,6X,A4,6X,A4,19X,'EE_TYPE,EP_TYPE,EI_TYPE')
 131  FORMAT(6X,A4,6X,A4,6X,A4,4X,F9.3,6X,'IP_TYPE,PD_TYPE,QD_TYPE,ZETA')
 132  FORMAT(6X,F7.3,2X,A5,4X,F9.3,16X,'D_VALUE_1,POWER_1,EK')
 133  FORMAT(6X,F7.3,2X,A5,8X,F4.2,17X,'D_VALUE_2,POWER_2,PCT')
 134  FORMAT(5X,A5,6X,F7.3,2X,F8.3,3X,F7.3,6X,'INT_POT,S,EPS,DELTA')
 135  FORMAT(6X,F7.3,3X,F7.3,5X,I2,8X,I2,9X,'RC,ALF,M,N')
 136  FORMAT(6X,F7.3,3X,F7.3,3X,F7.3,3X,F7.3,6X,'A1,A2,A3,A4')
 137  FORMAT(5X,F8.3,2X,F8.3,26X,'EP_C,DEBYE_T')
 138  FORMAT(5X,F8.3,2X,F8.3,2X,F8.3,16X,'NA,MA,RA')
 139  FORMAT(5X,F8.3,2X,F8.3,26X,'NI,EI_C')
 140  FORMAT(7X,A3,7X,A3,7X,A3,19X,'CF_TYPE,PF_TYPE,SL_TYPE')
 141  FORMAT(3X,A7,7X,A3,7X,A3,19X,'SSTDY,SQ_TYPE,SQO_TYPE')
 142  FORMAT(7X,A3,6X,A4,29X,'GR_TYPE,GR0_MODE')
 143  FORMAT(7X,A3,39X,'RH_TYPE')
 144  FORMAT(6X,A4,39X,'SPF_TYPE')
 145  FORMAT(4X,A6,8X,A2,8X,A2,19X,'EC_TYPE,FXC_TYPE,EXC_TYPE')
 146  FORMAT(7X,A3,7X,A3,29X,'EX_TYPE,EK_TYPE')
 147  FORMAT(9X,I1,6X,F7.3,26X,'IMODE,XI')
 148  FORMAT(6X,A4,7X,A3,29X,'TH_PROP,GP_TYPE')
 149  FORMAT(4X,F9.3,1X,F9.3,26X,'EK_INI,EK_FIN')
 150  FORMAT(8X,I2,7X,A3,29X,'N_M,M_TYPE')
 151  FORMAT(6X,F6.2,1X,F9.2,27X,'Z_BEAM,EK_BEAM')
!
!  Formats: Reading print switches
!
  201 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
!---------------------------------------------------
  202 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
  204 FORMAT( 9X,I1,9X,I1)
!---------------------------------------------------
  205 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
  206 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
  207 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
  208 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
  209 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
  210 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
  211 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
  212 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
  213 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
  214 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
!---------------------------------------------------
  215 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
  216 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
  217 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
  218 FORMAT( 9X,I1,9X,I1)
!---------------------------------------------------
  219 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
  220 FORMAT( 9X,I1)
!---------------------------------------------------
  221 FORMAT( 9X,I1,9X,I1,9X,I1)
!---------------------------------------------------
  222 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
  223 FORMAT( 9X,I1,9X,I1,9X,I1)
!---------------------------------------------------
  224 FORMAT( 9X,I1,9X,I1)
!---------------------------------------------------
  225 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1)
  226 FORMAT( 9X,I1)
!---------------------------------------------------
  227 FORMAT( 9X,I1,9X,I1,8X,I2)
!---------------------------------------------------
!
!  Formats: Writing print switches
!
  301 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_DF,I_PZ,I_SU,I_CD')
!---------------------------------------------------
  302 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_PD,I_EH,I_E2,I_CK')
  304 FORMAT( 9X,I1,9X,I1,29X,'I_CR,I_PK')
!---------------------------------------------------
  305 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_LF,I_IQ,I_SF,I_PC')
  306 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_P2,I_VX,I_DC,I_MD')
  307 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_LD,I_DP,I_LT,I_BR')
  308 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_PE,I_QC,I_RL,I_KS')
  309 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_OQ,I_ME,I_MS,I_ML')
  310 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_MC,I_DE,I_ZE,I_SR')
  311 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_CW,I_CF,I_EM,I_MF')
  312 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_SP,I_SE,I_SB,I_ES')
  313 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_GR,I_FD,I_BE,I_MX')
  314 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_SC,I_DS,I_NV,I_MT')
!---------------------------------------------------
  315 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_GP,I_PR,I_CO,I_CP')
  316 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_BM,I_SH,I_S0,I_S1')
  317 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_DT,I_PS,I_IE,I_EI')
  318 FORMAT( 9X,I1,9X,I1,29X,'I_FH,I_EY')
!---------------------------------------------------
  319 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_EF,I_KF,I_VF,I_TE')
  320 FORMAT( 9X,I1,39X,'I_DL')
!---------------------------------------------------
  321 FORMAT( 9X,I1,9X,I1,9X,I1,19X,'I_TW,I_VT,I_TC')
!---------------------------------------------------
  322 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_EG,I_EX,I_XC,I_EC')
  323 FORMAT( 9X,I1,9X,I1,9X,I1,19X,'I_HF,I_EK,I_EP')
!---------------------------------------------------
  324 FORMAT( 9X,I1,9X,I1,29X,'I_VI,I_DI')
!---------------------------------------------------
  325 FORMAT( 9X,I1,9X,I1,9X,I1,9X,I1,9X,'I_FP,I_EL,I_PO,I_RF')
  326 FORMAT( 9X,I1,39X,'I_VC')
!---------------------------------------------------
  327 FORMAT( 9X,I1,9X,I1,8X,I2,19X,'I_FN,I_WR,I_TI')
!
  401  FORMAT('**********************************************************************************')
  402  FORMAT('*********************                                        *********************')
  403  FORMAT('*********************              Fermi Liquid              *********************')
  404  FORMAT('*********************           Dielectric Function          *********************')
  405  FORMAT('*********************             Input Datafile             *********************')
  406  FORMAT('*********************                                        *********************')
  407  FORMAT('**********************************************************************************',//)
!
  500  FORMAT('          ')
!
  601  FORMAT('  =======+=========+=========+=========+=========+============================')
  602  FORMAT('  -------+---------+---------+---------+---------+----------------------------')
!
  702  FORMAT('                          GENERAL PARAMETERS :                                ')
  703  FORMAT('                          DIELECTRIC FUNCTION :                               ')
  704  FORMAT('                          STRUCTURE FACTOR :                                  ')
  705  FORMAT('                          PAIR CORRELATION FUNCTION :                         ')
  706  FORMAT('                          PAIR DISTRIBUTION FUNCTION :                        ')
  707  FORMAT('                          SPECTRAL FUNCTION :                                 ')
  708  FORMAT('                          ENERGY CALCULATIONS :                               ')
  709  FORMAT('                          SPIN POLARIZATION :                                 ')
  710  FORMAT('                          THERMODYNAMIC PROPERTIES :                          ')
  711  FORMAT('                          ELECTRON MEAN FREE PATH :                           ')
  712  FORMAT('                          CALCULATION OF MOMENTS:                             ')
  713  FORMAT('                          INCOMING ION BEAM :                                 ')
  714  FORMAT('                          OUTPUT CALCULATIONS/PRINTING :                      ')
!
  801  FORMAT('                          (q,omega,r)  :                                      ')
  802  FORMAT('                          Material''s properties :                            ')
  803  FORMAT('                          External fields :                                   ')
  804  FORMAT('                          System''s dimension :                               ')
  805  FORMAT('                          Confinement :                                       ')
  806  FORMAT('                          Multilayer structure :                              ')
  807  FORMAT('                          Units :                                             ')
  808  FORMAT('                          Screening :                                         ')
  809  FORMAT('                          Plasma type :                                       ')
  810  FORMAT('                          Calculation type :                                  ')
  811  FORMAT('                          Analytical plasmon dispersion :                     ')
  812  FORMAT('                          Local-field corrections                             ')
  813  FORMAT('                          Damping :                                           ')
  814  FORMAT('                          Electron-electron interaction :                     ')
  815  FORMAT('                          Electron-phonon interaction :                       ')
  816  FORMAT('                          Electron-impurity interaction :                     ')
  817  FORMAT('                          Classical fluid parameters :                        ')
!
      END SUBROUTINE READ_DATA
!
END MODULE INPUT_DATA
