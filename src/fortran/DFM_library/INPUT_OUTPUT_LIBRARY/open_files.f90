!
!=======================================================================
!
MODULE OPEN_OUTFILES 
!
!  This module contains the subroutine that opens the output files
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE OPEN_OUTPUT_FILES(N_IF,JF)
!
!  This subroutine open the output files for printing. These files 
!   are open:
!
!     1) at the first input data file iteration (JF = 1)
!     2) for the other iteration: if file is indexed 
!          on the input data files (INDEX_FILE(IO_DF) = 0)
!
!
!  Input parameter:
!
!       * N_IF     :  number of input data files
!       * JF       :  index of current input data file
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 26 Jan 2021
!
!
!
      USE DIMENSION_CODE,    ONLY : NOFFN
!
      USE OUT_VALUES_1
      USE OUT_VALUES_2
      USE OUT_VALUES_3
      USE OUT_VALUES_4
      USE OUT_VALUES_5
      USE OUT_VALUES_6
      USE OUT_VALUES_7
      USE OUT_VALUES_8
      USE OUT_VALUES_9
      USE OUT_VALUES_10
      USE OUT_VALUES_P
!
      USE FILENAMES
      USE OUTFILES
      USE CHANGE_FILENAMES
!
      USE PRINT_FILES
!
      IMPLICIT NONE 
!
      INTEGER                          ::  N_IF                   
      INTEGER                          ::  JF                   
      INTEGER                          ::  J                   
!
      CHARACTER (LEN = 100)            ::  FLIST(NOFFN)
!
!  Changing the output filenames if required
!
      CALL NEW_FILENAMES(N_IF,JF,FLIST)                             !
!
!  Files for calculator 1
!
      IF(I_DF == 1) THEN                                            !
        IO_DF=FN(DFFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_DF) == 0) THEN                !
          OPEN(UNIT=IO_DF,FILE=FLIST(IO_DF),STATUS='unknown')       ! dielectric function file
        END IF                                                      !
      END IF                                                        !
      IF(I_PZ == 1) THEN                                            !
        IO_PZ=FN(PZFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_PZ) == 0) THEN                !
          OPEN(UNIT=IO_PZ,FILE=FLIST(IO_PZ),STATUS='unknown')       ! polarization function
        END IF                                                      !
      END IF                                                        !
      IF(I_SU == 1) THEN                                            !
        IO_SU=FN(SUFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_SU) == 0) THEN                !
          OPEN(UNIT=IO_SU,FILE=FLIST(IO_SU),STATUS='unknown')       ! susceptibility function
        END IF                                                      !
      END IF                                                        !
      IF(I_CD == 1) THEN                                            !
        IO_CD=FN(CDFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_CD) == 0) THEN                !
          OPEN(UNIT=IO_CD,FILE=FLIST(IO_CD),STATUS='unknown')       ! electrical conductivity
        END IF                                                      !
      END IF                                                        !
!
!  Files for calculator 2
!
      IF(I_PD == 1) THEN                                            !
        IO_PD=FN(PDFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_PD) == 0) THEN                !
          OPEN(UNIT=IO_PD,FILE=FLIST(IO_PD),STATUS='unknown')       ! plasmon dispersion file
        END IF                                                      !
      END IF                                                        !
      IF(I_EH == 1) THEN                                            !
        IO_EH=FN(EHFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_EH) == 0) THEN                !
          OPEN(UNIT=IO_EH,FILE=FLIST(IO_EH),STATUS='unknown')       ! electron-hole dispersion file
        END IF                                                      !
      END IF                                                        !
      IF(I_E2 == 1) THEN                                            !
        IO_E2=FN(E2FILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_E2) == 0) THEN                !
          OPEN(UNIT=IO_E2,FILE=FLIST(IO_E2),STATUS='unknown')       ! two electron-hole dispersion
        END IF                                                      !
      END IF                                                        !
      IF(I_CF == 1) THEN                                            !
        IO_CK=FN(CKFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_CK) == 0) THEN                !
          OPEN(UNIT=IO_CK,FILE=FLIST(IO_CK),STATUS='unknown')       ! screened Coulomb (k-space)
        END IF                                                      !
      END IF                                                        !
      IF(I_CR == 1) THEN                                            !
        IO_CR=FN(CRFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_CR) == 0) THEN                !
          OPEN(UNIT=IO_CR,FILE=FLIST(IO_CR),STATUS='unknown')       ! screened Coulomb (real space)
        END IF                                                      !
      END IF                                                        !
      IF(I_PK == 1) THEN                                            !
        IO_PK=FN(PKFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_PK) == 0) THEN                !
          OPEN(UNIT=IO_PK,FILE=FLIST(IO_PK),STATUS='unknown')       ! plasmon kinetic energy
        END IF                                                      !
      END IF                                                        !
!
!  Files for calculator 3
!
      IF(I_LF == 1) THEN                                            !
        IO_LF=FN(LFFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_LF) == 0) THEN                !
          OPEN(UNIT=IO_LF,FILE=FLIST(IO_LF),STATUS='unknown')       ! local-field correction file G(q,om)
        END IF                                                      !
      END IF                                                        !
      IF(I_IQ == 1) THEN                                            !
        IO_IQ=FN(IQFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_IQ) == 0) THEN                !
          OPEN(UNIT=IO_IQ,FILE=FLIST(IO_IQ),STATUS='unknown')       ! G(q,inf) file
        END IF                                                      !
      END IF                                                        !
      IF(I_SF == 1) THEN                                            !
        IO_SF=FN(SFFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_SF) == 0) THEN                !
          OPEN(UNIT=IO_SF,FILE=FLIST(IO_SF),STATUS='unknown')       ! structure factor file S(q,om)
        END IF                                                      !
      END IF                                                        !
      IF(I_PC == 1) THEN                                            !
        IO_PC=FN(PCFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_PC) == 0) THEN                !
          OPEN(UNIT=IO_PC,FILE=FLIST(IO_PC),STATUS='unknown')       ! pair correlation function file
        END IF                                                      !
      END IF                                                        !
      IF(I_P2 == 1) THEN                                            !
        IO_P2=FN(P2FILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_P2) == 0) THEN                !
          OPEN(UNIT=IO_P2,FILE=FLIST(IO_P2),STATUS='unknown')       ! pair distribution function file
        END IF                                                      !
      END IF                                                        !
      IF(I_VX == 1) THEN                                            !
        IO_VX=FN(VXFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_VX) == 0) THEN                !
          OPEN(UNIT=IO_VX,FILE=FLIST(IO_VX),STATUS='unknown')       ! vertex function Gamma(q,om)
        END IF                                                      !
      END IF                                                        !
      IF(I_DC == 1) THEN                                            !
        IO_DC=FN(DCFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_DC) == 0) THEN                !
          OPEN(UNIT=IO_DC,FILE=FLIST(IO_DC),STATUS='unknown')       ! plasmon damping coefficient Im[eps]/q^2 
        END IF                                                      !
      END IF                                                        !
      IF(I_MD == 1) THEN                                            !
        IO_MD=FN(MDFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_MD) == 0) THEN                !
          OPEN(UNIT=IO_MD,FILE=FLIST(IO_MD),STATUS='unknown')       ! momentum distribution
        END IF                                                      !
      END IF                                                        !
      IF(I_LD == 1) THEN                                            !
        IO_LD=FN(LDFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_LD) == 0) THEN                !
          OPEN(UNIT=IO_LD,FILE=FLIST(IO_LD),STATUS='unknown')       ! Landau parameters
        END IF                                                      !
      END IF                                                        !
      IF(I_DP == 1) THEN                                            !
        IO_DP=FN(DPFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_DP) == 0) THEN                !
          OPEN(UNIT=IO_DP,FILE=FLIST(IO_DP),STATUS='unknown')       ! damping file
        END IF                                                      !
      END IF                                                        !
      IF(I_LT == 1) THEN                                            !
        IO_LT=FN(LTFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_LT) == 0) THEN                !
          OPEN(UNIT=IO_LT,FILE=FLIST(IO_LT),STATUS='unknown')       ! plasmon lifetime file
        END IF                                                      !
      END IF                                                        !
      IF(I_BR == 1) THEN                                            !
        IO_BR=FN(BRFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_BR) == 0) THEN                !
          OPEN(UNIT=IO_BR,FILE=FLIST(IO_BR),STATUS='unknown')       ! plasmon broadening
        END IF                                                      !
      END IF                                                        !
      IF(I_PE == 1) THEN                                            !
        IO_PE=FN(PEFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_PE) == 0) THEN                !
          OPEN(UNIT=IO_PE,FILE=FLIST(IO_PE),STATUS='unknown')       ! plasmon energy
        END IF                                                      !
      END IF                                                        !
      IF(I_QC == 1) THEN                                            !
        IO_QC=FN(QCFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_QC) == 0) THEN                !
          OPEN(UNIT=IO_QC,FILE=FLIST(IO_QC),STATUS='unknown')       ! plasmon q-bounds
        END IF                                                      !
      END IF                                                        !
      IF(I_RL == 1) THEN                                            !
        IO_RL=FN(RLFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_RL) == 0) THEN                !
          OPEN(UNIT=IO_RL,FILE=FLIST(IO_RL),STATUS='unknown')       ! relaxation time
        END IF                                                      !
      END IF                                                        !
      IF(I_KS == 1) THEN                                            !
        IO_KS=FN(KSFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_KS) == 0) THEN                !
          OPEN(UNIT=IO_KS,FILE=FLIST(IO_KS),STATUS='unknown')       ! screening wave vector
        END IF                                                      !
      END IF                                                        !
      IF(I_OQ == 1) THEN                                            !
        IO_OQ=FN(OQFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_OQ) == 0) THEN                !
          OPEN(UNIT=IO_OQ,FILE=FLIST(IO_OQ),STATUS='unknown')       ! omega = q * v_F
        END IF                                                      !
      END IF                                                        !
      IF(I_ME == 1) THEN                                            !
        IO_ME=FN(MEFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_ME) == 0) THEN                !
          OPEN(UNIT=IO_ME,FILE=FLIST(IO_ME),STATUS='unknown')       ! moments of epsilon
        END IF                                                      !
      END IF                                                        !
      IF(I_MS == 1) THEN                                            !
        IO_MS=FN(MSFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_MS) == 0) THEN                !
          OPEN(UNIT=IO_MS,FILE=FLIST(IO_MS),STATUS='unknown')       ! moments of S(q,omega)
        END IF                                                      !
      END IF                                                        !
      IF(I_ML == 1) THEN                                            !
        IO_ML=FN(MLFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_ML) == 0) THEN                !
          OPEN(UNIT=IO_ML,FILE=FLIST(IO_ML),STATUS='unknown')       ! moments of loss function
        END IF                                                      !
      END IF                                                        !
      IF(I_MC == 1) THEN                                            !
        IO_MC=FN(MCFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_MC) == 0) THEN                !
          OPEN(UNIT=IO_MC,FILE=FLIST(IO_MC),STATUS='unknown')       ! moments of conductivity
        END IF                                                      !
      END IF                                                        !
      IF(I_DE == 1) THEN                                            !
        IO_DE=FN(DEFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_DE) == 0) THEN                !
          OPEN(UNIT=IO_DE,FILE=FLIST(IO_DE),STATUS='unknown')       ! derivative of Re[ dielectric function ]
        END IF                                                      !
      END IF                                                        !
      IF(I_ZE == 1) THEN                                            !
        IO_ZE=FN(ZEFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_ZE) == 0) THEN                !
          OPEN(UNIT=IO_ZE,FILE=FLIST(IO_ZE),STATUS='unknown')       ! Re[ dielectric function ] = 0
        END IF                                                      !
      END IF                                                        !
      IF(I_SR == 1) THEN                                            !
        IO_SR=FN(SRFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_SR) == 0) THEN                !
          OPEN(UNIT=IO_SR,FILE=FLIST(IO_SR),STATUS='unknown')       ! sum rules for epsilon
        END IF                                                      !
      END IF                                                        !
      IF(I_CW == 1) THEN                                            !
        IO_CW=FN(CWFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_CW) == 0) THEN                !
          OPEN(UNIT=IO_CW,FILE=FLIST(IO_CW),STATUS='unknown')       ! confinement wave function
        END IF                                                      !
      END IF                                                        !
      IF(I_CF == 1) THEN                                            !
        IO_CF=FN(CFFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_CF) == 0) THEN                !
          OPEN(UNIT=IO_CF,FILE=FLIST(IO_CF),STATUS='unknown')       ! confinement potential
        END IF                                                      !
      END IF                                                        !
      IF(I_EM == 1) THEN                                            !
        IO_EM=FN(EMFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_EM) == 0) THEN                !
          OPEN(UNIT=IO_EM,FILE=FLIST(IO_EM),STATUS='unknown')       ! effective mass
        END IF                                                      !
      END IF                                                        !
      IF(I_MF == 1) THEN                                            !
        IO_MF=FN(MFFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_MF) == 0) THEN                !
          OPEN(UNIT=IO_MF,FILE=FLIST(IO_MF),STATUS='unknown')       ! mean free path
        END IF                                                      !
      END IF                                                        !
      IF(I_SP == 1) THEN                                            !
        IO_SP=FN(SPFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_SP) == 0) THEN                !
          OPEN(UNIT=IO_SP,FILE=FLIST(IO_SP),STATUS='unknown')       ! spectral function
        END IF                                                      !
      END IF                                                        !
      IF(I_SE == 1) THEN                                            !
        IO_SE=FN(SEFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_SE) == 0) THEN                !
          OPEN(UNIT=IO_SE,FILE=FLIST(IO_SE),STATUS='unknown')       ! self-energy
        END IF                                                      !
      END IF                                                        !
      IF(I_SB == 1) THEN                                            !
        IO_SB=FN(SBFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_SB) == 0) THEN                !
          OPEN(UNIT=IO_SB,FILE=FLIST(IO_SB),STATUS='unknown')       ! subband energies
        END IF                                                      !
      END IF                                                        !
      IF(I_ES == 1) THEN                                            !
        IO_ES=FN(ESFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_ES) == 0) THEN                !
          OPEN(UNIT=IO_ES,FILE=FLIST(IO_ES),STATUS='unknown')       ! Eliashberg function
        END IF                                                      !
      END IF                                                        !
      IF(I_GR == 1) THEN                                            !
        IO_GR=FN(GRFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_GR) == 0) THEN                !
          OPEN(UNIT=IO_GR,FILE=FLIST(IO_GR),STATUS='unknown')       ! Grüneisen parameter
        END IF                                                      !
      END IF                                                        !
      IF(I_FD == 1) THEN                                            !
        IO_FD=FN(FDFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_FD) == 0) THEN                !
          OPEN(UNIT=IO_FD,FILE=FLIST(IO_FD),STATUS='unknown')       ! Fermi-Dirac distribution
        END IF                                                      !
      END IF                                                        !
      IF(I_BE == 1) THEN                                            !
        IO_BE=FN(BEFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_BE) == 0) THEN                !
          OPEN(UNIT=IO_BE,FILE=FLIST(IO_BE),STATUS='unknown')       ! Bose-Einstein distribution
        END IF                                                      !
      END IF                                                        !
      IF(I_MX == 1) THEN                                            !
        IO_MX=FN(MXFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_MX) == 0) THEN                !
          OPEN(UNIT=IO_MX,FILE=FLIST(IO_MX),STATUS='unknown')       ! Maxwell distribution
        END IF                                                      !
      END IF                                                        !
      IF(I_SC == 1) THEN                                            !
        IO_SC=FN(SCFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_SC) == 0) THEN                !
          OPEN(UNIT=IO_SC,FILE=FLIST(IO_SC),STATUS='unknown')       ! scale parameters
        END IF                                                      !
      END IF                                                        !
      IF(I_DS == 1) THEN                                            !
        IO_DS=FN(DSFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_DS) == 0) THEN                !
          OPEN(UNIT=IO_DS,FILE=FLIST(IO_DS),STATUS='unknown')       ! density of states
        END IF                                                      !
      END IF                                                        !
      IF(I_NV == 1) THEN                                            !
        IO_NV=FN(NVFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_NV) == 0) THEN                !
          OPEN(UNIT=IO_NV,FILE=FLIST(IO_NV),STATUS='unknown')       ! Nevanlinaa function
        END IF                                                      !
      END IF                                                        !
      IF(I_MT == 1) THEN                                            !
        IO_MT=FN(MTFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_MT) == 0) THEN                !
          OPEN(UNIT=IO_MT,FILE=FLIST(IO_MT),STATUS='unknown')       ! time domain memory function
        END IF                                                      !
      END IF                                                        !
!
!  Files for calculator 4
!
      IF(I_GP == 1) THEN                                            !
        IO_GP=FN(GPFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_GP) == 0) THEN                !
          OPEN(UNIT=IO_GP,FILE=FLIST(IO_GP),STATUS='unknown')       ! grand partition function
        END IF                                                      !
      END IF                                                        !
      IF(I_PR == 1) THEN                                            !
        IO_PR=FN(PRFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_PR) == 0) THEN                !
          OPEN(UNIT=IO_PR,FILE=FLIST(IO_PR),STATUS='unknown')       ! electronic pressure
        END IF                                                      !
      END IF                                                        !
      IF(I_CO == 1) THEN                                            !
        IO_CO=FN(COFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_CO) == 0) THEN                !
          OPEN(UNIT=IO_CO,FILE=FLIST(IO_CO),STATUS='unknown')       ! compressibility
        END IF                                                      !
      END IF                                                        !
      IF(I_CP == 1) THEN                                            !
        IO_CP=FN(CPFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_CP) == 0) THEN                !
          OPEN(UNIT=IO_CP,FILE=FLIST(IO_CP),STATUS='unknown')       ! chemical potential
        END IF                                                      !
      END IF                                                        !
      IF(I_BM == 1) THEN                                            !
        IO_BM=FN(BMFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_BM) == 0) THEN                !
          OPEN(UNIT=IO_BM,FILE=FLIST(IO_BM),STATUS='unknown')       ! bulk modulus
        END IF                                                      !
      END IF                                                        !
      IF(I_SH == 1) THEN                                            !
        IO_SH=FN(SHFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_SH) == 0) THEN                !
          OPEN(UNIT=IO_SH,FILE=FLIST(IO_SH),STATUS='unknown')       ! shear modulus
        END IF                                                      !
      END IF                                                        !
      IF(I_S0 == 1) THEN                                            !
        IO_S0=FN(S0FILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_S0) == 0) THEN                !
          OPEN(UNIT=IO_S0,FILE=FLIST(IO_S0),STATUS='unknown')       ! zero sound velocity
        END IF                                                      !
      END IF                                                        !
      IF(I_S1 == 1) THEN                                            !
        IO_S1=FN(S1FILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_S1) == 0) THEN                !
          OPEN(UNIT=IO_S1,FILE=FLIST(IO_S1),STATUS='unknown')       ! first sound velocity
        END IF                                                      !
      END IF                                                        !
      IF(I_DT == 1) THEN                                            !
        IO_DT=FN(DTFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_DT) == 0) THEN                !
          OPEN(UNIT=IO_DT,FILE=FLIST(IO_DT),STATUS='unknown')       ! Debye temperature
        END IF                                                      !
      END IF                                                        !
      IF(I_PS == 1) THEN                                            !
        IO_PS=FN(PSFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_PS) == 0) THEN                !
          OPEN(UNIT=IO_PS,FILE=FLIST(IO_PS),STATUS='unknown')       ! Pauli paramagnetic susceptibility
        END IF                                                      !
      END IF                                                        !
      IF(I_IE == 1) THEN                                            !
        IO_IE=FN(IEFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_IE) == 0) THEN                !
          OPEN(UNIT=IO_IE,FILE=FLIST(IO_IE),STATUS='unknown')       ! internal energy
        END IF                                                      !
      END IF                                                        !
      IF(I_EI == 1) THEN                                            !
        IO_EI=FN(EIFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_EI) == 0) THEN                !
          OPEN(UNIT=IO_EI,FILE=FLIST(IO_EI),STATUS='unknown')       ! excess internal energy
        END IF                                                      !
      END IF                                                        !
      IF(I_FH == 1) THEN                                            !
        IO_FH=FN(FHFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_FH) == 0) THEN                !
          OPEN(UNIT=IO_FH,FILE=FLIST(IO_FH),STATUS='unknown')       ! Helmholtz free energy
        END IF                                                      !
      END IF                                                        !
      IF(I_EY == 1) THEN                                            !
        IO_EY=FN(EYFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_EY) == 0) THEN                !
          OPEN(UNIT=IO_EY,FILE=FLIST(IO_EY),STATUS='unknown')       ! entropy
        END IF                                                      !
      END IF                                                        !
!
!  Files for calculator 5
!
      IF(I_EF == 1) THEN                                            !
        IO_EF=FN(EFFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_EF) == 0) THEN                !
          OPEN(UNIT=IO_EF,FILE=FLIST(IO_EF),STATUS='unknown')       ! Fermi energy
        END IF                                                      !
      END IF                                                        !
      IF(I_KF == 1) THEN                                            !
        IO_KF=FN(KFFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_KF) == 0) THEN                !
          OPEN(UNIT=IO_KF,FILE=FLIST(IO_KF),STATUS='unknown')       ! Fermi momentum
        END IF                                                      !
      END IF                                                        !
      IF(I_VF == 1) THEN                                            !
        IO_VF=FN(VFFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_VF) == 0) THEN                !
          OPEN(UNIT=IO_VF,FILE=FLIST(IO_VF),STATUS='unknown')       ! Fermi velocity
        END IF                                                      !
      END IF                                                        !
      IF(I_TE == 1) THEN                                            !
        IO_TE=FN(TEFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_TE) == 0) THEN                !
          OPEN(UNIT=IO_TE,FILE=FLIST(IO_TE),STATUS='unknown')       ! Fermi temperature
        END IF                                                      !
      END IF                                                        !
      IF(I_DL == 1) THEN                                            !
        IO_DL=FN(DLFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_DL) == 0) THEN                !
          OPEN(UNIT=IO_DL,FILE=FLIST(IO_DL),STATUS='unknown')       ! Fermi density of states
        END IF                                                      !
      END IF                                                        !
!
!  Files for calculator 6
!
      IF(I_TW == 1) THEN                                            !
        IO_TW=FN(TWFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_TW) == 0) THEN                !
          OPEN(UNIT=IO_TW,FILE=FLIST(IO_TW),STATUS='unknown')       ! thermal De Broglie wavelength
        END IF                                                      !
      END IF                                                        !
      IF(I_VT == 1) THEN                                            !
        IO_VT=FN(VTFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_VT) == 0) THEN                !
          OPEN(UNIT=IO_VT,FILE=FLIST(IO_VT),STATUS='unknown')       ! thermal velocity
        END IF                                                      !
      END IF                                                        !
      IF(I_TC == 1) THEN                                            !
        IO_TC=FN(TCFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_TC) == 0) THEN                !
          OPEN(UNIT=IO_TC,FILE=FLIST(IO_TC),STATUS='unknown')       ! thermal conductivity
        END IF                                                      !
      END IF                                                        !
!
!  Files for calculator 7
!
      IF(I_EG == 1) THEN                                            !
        IO_EG=FN(EGFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_EG) == 0) THEN                !
          OPEN(UNIT=IO_EG,FILE=FLIST(IO_EG),STATUS='unknown')       ! ground state energy
        END IF                                                      !
      END IF                                                        !
      IF(I_EX == 1) THEN                                            !
        IO_EX=FN(EXFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_EX) == 0) THEN                !
          OPEN(UNIT=IO_EX,FILE=FLIST(IO_EX),STATUS='unknown')       ! exchange energy
        END IF                                                      !
      END IF                                                        !
      IF(I_XC == 1) THEN                                            !
        IO_XC=FN(XCFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_XC) == 0) THEN                !
          OPEN(UNIT=IO_XC,FILE=FLIST(IO_XC),STATUS='unknown')       ! exchange correlation energy
        END IF                                                      !
      END IF                                                        !
      IF(I_EC == 1) THEN                                            !
        IO_EC=FN(ECFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_EC) == 0) THEN                !
          OPEN(UNIT=IO_EC,FILE=FLIST(IO_EC),STATUS='unknown')       ! correlation energy
        END IF                                                      !
      END IF                                                        !
      IF(I_HF == 1) THEN                                            !
        IO_HF=FN(HFFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_HF) == 0) THEN                !
          OPEN(UNIT=IO_HF,FILE=FLIST(IO_HF),STATUS='unknown')       ! Hartree-Fock energy
        END IF                                                      !
      END IF                                                        !
      IF(I_EK == 1) THEN                                            !
        IO_EK=FN(EKFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_EK) == 0) THEN                !
          OPEN(UNIT=IO_EK,FILE=FLIST(IO_EK),STATUS='unknown')       ! kinetic energy
        END IF                                                      !
      END IF                                                        !
      IF(I_EP == 1) THEN                                            !
        IO_EP=FN(EPFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_EP) == 0) THEN                !
          OPEN(UNIT=IO_EP,FILE=FLIST(IO_EP),STATUS='unknown')       ! potential energy
        END IF                                                      !
      END IF                                                        !
!
!  Files for calculator 8
!
      IF(I_VI == 1) THEN                                            !
        IO_VI=FN(VIFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_VI) == 0) THEN                !
          OPEN(UNIT=IO_VI,FILE=FLIST(IO_VI),STATUS='unknown')       ! shear viscosity
        END IF                                                      !
      END IF                                                        !
      IF(I_DI == 1) THEN                                            !
        IO_DI=FN(DIFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_DI) == 0) THEN                !
          OPEN(UNIT=IO_DI,FILE=FLIST(IO_DI),STATUS='unknown')       ! diffusion coefficient
        END IF                                                      !
      END IF                                                        !
!
!  Files for calculator 9
!
      IF(I_FP == 1) THEN                                            !
        IO_FP=FN(FPFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_FP) == 0) THEN                !
          OPEN(UNIT=IO_FP,FILE=FLIST(IO_FP),STATUS='unknown')       ! fluctuation potential file
        END IF                                                      !
      END IF                                                        !
      IF(I_EL == 1) THEN                                            !
        IO_EL=FN(ELFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_EL) == 0) THEN                !
          OPEN(UNIT=IO_EL,FILE=FLIST(IO_EL),STATUS='unknown')       ! energy loss function
        END IF                                                      !
      END IF                                                        !
      IF(I_PO == 1) THEN                                            !
        IO_PO=FN(POFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_PO) == 0) THEN                !
          OPEN(UNIT=IO_PO,FILE=FLIST(IO_PO),STATUS='unknown')       ! stopping power
        END IF                                                      !
      END IF                                                        !
      IF(I_RF == 1) THEN                                            !
        IO_RF=FN(RFFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_RF) == 0) THEN                !
          OPEN(UNIT=IO_RF,FILE=FLIST(IO_RF),STATUS='unknown')       ! refractive index
        END IF                                                      !
      END IF                                                        !
      IF(I_VC == 1) THEN                                            !
        IO_VC=FN(VCFILE)                                            !
        IF(JF == 1 .OR. INDEX_FILE(IO_VC) == 0) THEN                !
          OPEN(UNIT=IO_VC,FILE=FLIST(IO_VC),STATUS='unknown')       ! dynamic screened Coulomb potential V(q,omega)
        END IF                                                      !
      END IF                                                        !
!
      END SUBROUTINE OPEN_OUTPUT_FILES
!
END MODULE OPEN_OUTFILES
