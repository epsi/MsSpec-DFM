!
!=======================================================================
!
MODULE MATERIAL_PROP
!
!  This module contains input values for the material's properties
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS,MSOM,EPS_B
!
      CHARACTER (LEN = 5)   ::  MAT_TYP
      CHARACTER (LEN = 2)   ::  DMN
!
END MODULE MATERIAL_PROP
!
!=======================================================================
!
MODULE EXT_FIELDS
!
!  This module contains input values for the external fields
!         (temperature, electric, magnetic)
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  T,E,H
!
      CHARACTER (LEN = 2)   ::  FLD
!
END MODULE EXT_FIELDS
!
!=======================================================================
!
MODULE Q_GRID
!
!  This module contains input values for q-grid
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      INTEGER               ::  N_Q
!
      REAL (WP)             ::  Q_MIN,Q_MAX,Q_STEP
!
      END MODULE Q_GRID
!
!=======================================================================
!
MODULE E_GRID
!
!  This module contains input values for energy grid
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      INTEGER               ::  N_E
!
      REAL (WP)             ::  E_MIN,E_MAX,E_STEP
!
      END MODULE E_GRID
!
!=======================================================================
!
MODULE R_GRID
!
!  This module contains input values for radial grid
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      INTEGER               ::  N_R
!
      REAL (WP)             ::  R_MIN,R_MAX,R_STEP
!
      END MODULE R_GRID
!
!=======================================================================
!
MODULE CONFIN_VAL
!
!  This module contains the input values for the confinement
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  R0,L,OM0
!
      CHARACTER (LEN = 7)   ::  CONFIN
!
END MODULE CONFIN_VAL
!
!=======================================================================
!
MODULE MULTILAYER
!
!  This module contains the input values for multilayer systems
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  DL,D1
      REAL (WP)             ::  EPS_1,EPS_2
      REAL (WP)             ::  N_DEP,N_INV
!
      CHARACTER (LEN = 4)   ::  H_TYPE
!
END MODULE MULTILAYER
!
!=======================================================================
!
MODULE UNITS
!
!  This module contains the input values for the units used
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  UNIT
      CHARACTER (LEN = 2)   ::  UNIK
!
END MODULE UNITS
!
!=======================================================================
!
MODULE SCREENING_TYPE
!
!  This module contains the input values for the screening type
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 2)   ::  SC_TYPE
!
END MODULE SCREENING_TYPE
!
!=======================================================================
!
MODULE PLASMA
!
!  This module contains the input values for the plasma type
!
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  PL_TYPE
!
      REAL (WP)             ::  ZION,ZION2
!
END MODULE PLASMA
!
!=======================================================================
!
MODULE CALCTYPE
!
!  This module contains the input values for the calculation type
!
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 7)   ::  CAL_TYPE
!
END MODULE CALCTYPE
!
!=======================================================================
!
MODULE DF_VALUES
!
!  This module contains the input values for the dielectric
!    function calculation
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 7)   ::  ESTDY
      CHARACTER (LEN = 4)   ::  EPS_T,D_FUNC,NEV_TYPE,MEM_TYPE
!
      REAL (WP)             ::  ALPHA,BETA
!
      INTEGER               ::  I_T
!
END MODULE DF_VALUES
!
!=======================================================================
!
MODULE PLASMON_DISPERSION
!
!  This module contains the input values for the analytical
!    plasmon dispersion calculation
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 7)   ::  PL_DISP
!
END MODULE PLASMON_DISPERSION
!
!=======================================================================
!
MODULE LF_VALUES
!
!  This module contains the input values for the local field
!    corrections calculation
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 7)   ::  GSTDY
      CHARACTER (LEN = 4)   ::  GQ_TYPE,GQO_TYPE,LANDAU
      CHARACTER (LEN = 3)   ::  IQ_TYPE
      CHARACTER (LEN = 2)   ::  G0_TYPE,GI_TYPE
!
END MODULE LF_VALUES
!
!=======================================================================
!
MODULE DAMPING_VALUES
!
!  This module contains the input values for the plasmon damping calculation
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 5)   ::  POWER_1,POWER_2
      CHARACTER (LEN = 4)   ::  DAMPING,LT_TYPE,DR_TYPE,DC_TYPE,VI_TYPE
      CHARACTER (LEN = 4)   ::  EE_TYPE,EP_TYPE,EI_TYPE,IP_TYPE,PD_TYPE
      CHARACTER (LEN = 4)   ::  QD_TYPE
      CHARACTER (LEN = 3)   ::  RT_TYPE
!
      REAL (WP)             ::  ZETA,D_VALUE_1,D_VALUE_2,EK,PCT
!
END MODULE DAMPING_VALUES
!
!=======================================================================
!
MODULE EL_ELE_INTER
!
!  This module contains the input values for the electron-electron
!    interaction calculation
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 5)   ::  INT_POT
!
      INTEGER               ::  M,N
!
      REAL (WP)             ::  S,EPS,DELTA
      REAL (WP)             ::  RC,ALF
      REAL (WP)             ::  A1,A2,A3,A4
!
END MODULE EL_ELE_INTER
!
!=======================================================================
!
MODULE EL_PHO_INTER
!
!  This module contains the input values for the electron-phonon
!    interaction calculation
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  EP_C,DEBYE_T
      REAL (WP)             ::  NA,MA,RA
!
END MODULE EL_PHO_INTER
!
!=======================================================================
!
MODULE EL_IMP_INTER
!
!  This module contains the input values for the electron-impurity
!    interaction calculation
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  NI,EI_C
!
END MODULE EL_IMP_INTER
!
!=======================================================================
!
MODULE CLASSICAL_FLUID_VALUES
!
!  This module contains the input values for the classical
!    fluid parameters
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  CF_TYPE,PF_TYPE,SL_TYPE
!
END MODULE CLASSICAL_FLUID_VALUES
!
!=======================================================================
!
MODULE SF_VALUES
!
!  This module contains the input values for the structure
!    factor calculation
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 7)   ::  SSTDY
      CHARACTER (LEN = 3)   ::  SQ_TYPE,SQO_TYPE
!
END MODULE SF_VALUES
!
!=======================================================================
!
MODULE PC_VALUES
!
!  This module contains the input values for the pair correlation
!    function calculation
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  GR0_MODE
      CHARACTER (LEN = 3)   ::  GR_TYPE
!
END MODULE PC_VALUES
!
!=======================================================================
!
MODULE PD_VALUES
!
!  This module contains the input values for the pair distribution
!    function calculation
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  RH_TYPE
!
END MODULE PD_VALUES
!
!=======================================================================
!
MODULE SPF_VALUES
!
!  This module contains the input values for the spectral
!    function calculation
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  SPF_TYPE
!
END MODULE SPF_VALUES
!
!=======================================================================
!
MODULE ENERGIES
!
!  This module contains the input values for the calculation
!    of the different energies
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 6)   ::  EC_TYPE
      CHARACTER (LEN = 3)   ::  EX_TYPE,EK_TYPE
      CHARACTER (LEN = 2)   ::  FXC_TYPE,EXC_TYPE
!
END MODULE ENERGIES
!
!=======================================================================
!
MODULE SPIN_POLARIZATION
!
!  This module contains the input values for the spin polarization
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      INTEGER               ::  IMODE
!
      REAL (WP)             ::  XI
!
END MODULE SPIN_POLARIZATION
!
!=======================================================================
!
MODULE THERMO_PROPERTIES
!
!  This module contains the input values for the thermodynamic
!    properties calculation
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 4)   ::  TH_PROP
      CHARACTER (LEN = 3)   ::  GP_TYPE
!
END MODULE THERMO_PROPERTIES
!
!=======================================================================
!
MODULE ELECTRON_MEAN_FREE_PATH
!
!  This module contains the input values for the inelastic
!    electron mean free path calculation
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  EK_INI,EK_FIN
!
END MODULE ELECTRON_MEAN_FREE_PATH
!
!=======================================================================
!
MODULE MOMENTS
!
!  This module contains the input values for the calculation
!    of the moments
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)   ::  M_TYPE
!
      INTEGER               ::  N_M
!
END MODULE MOMENTS
!
!=======================================================================
!
MODULE ION_BEAM
!
!  This module contains the input values for the incoming ion beam
!
      USE ACCURACY_REAL
!
      IMPLICIT NONE
!
      REAL (WP)             ::  Z_BEAM,EK_BEAM
!
!
END MODULE ION_BEAM
!
!=======================================================================
!
MODULE OUT_VALUES_1
!
!  This module contains input values for print switches
!
      IMPLICIT NONE
!
      INTEGER               ::  I_DF,I_PZ,I_SU,I_CD
!
END MODULE OUT_VALUES_1
!
!=======================================================================
!
MODULE OUT_VALUES_2
!
!  This module contains input values for print switches
!
      IMPLICIT NONE
!
      INTEGER               ::  I_PD,I_EH,I_E2,I_CK
      INTEGER               ::  I_CR,I_PK
!
END MODULE OUT_VALUES_2
!
!=======================================================================
!
MODULE OUT_VALUES_3
!
!  This module contains input values for print switches
!
      IMPLICIT NONE
!
      INTEGER               ::  I_LF,I_IQ,I_SF,I_PC
      INTEGER               ::  I_P2
      INTEGER               ::  I_VX,I_DC,I_MD,I_LD
      INTEGER               ::  I_DP,I_LT,I_BR,I_PE
      INTEGER               ::  I_QC,I_RL,I_KS,I_OQ
      INTEGER               ::  I_ME,I_MS,I_ML,I_MC
      INTEGER               ::  I_DE,I_ZE,I_SR,I_CW
      INTEGER               ::  I_CF,I_EM,I_MF,I_SP
      INTEGER               ::  I_SE,I_SB,I_ES,I_GR
      INTEGER               ::  I_FD,I_BE,I_MX
      INTEGER               ::  I_SC,I_DS,I_NV,I_MT
!
END MODULE OUT_VALUES_3
!
!=======================================================================
!
MODULE OUT_VALUES_4
!
!  This module contains input values for print switches
!
      IMPLICIT NONE
!
      INTEGER               ::  I_GP,I_PR,I_CO,I_CP
      INTEGER               ::  I_BM,I_SH,I_S0,I_S1
      INTEGER               ::  I_DT,I_PS,I_IE,I_EI
      INTEGER               ::  I_FH,I_EY
!
END MODULE OUT_VALUES_4
!
!=======================================================================
!
MODULE OUT_VALUES_5
!
!  This module contains input values for print switches
!
      IMPLICIT NONE
!
      INTEGER               ::  I_EF,I_KF,I_VF,I_TE,I_DL
!
END MODULE OUT_VALUES_5
!
!=======================================================================
!
MODULE OUT_VALUES_6
!
!  This module contains input values for print switches
!
      IMPLICIT NONE
!
      INTEGER               ::  I_TW,I_VT,I_TC
!
END MODULE OUT_VALUES_6
!
!=======================================================================
!
MODULE OUT_VALUES_7
!
!  This module contains input values for print switches
!
      IMPLICIT NONE
!
      INTEGER               ::  I_EG,I_EX,I_XC,I_EC
      INTEGER               ::  I_HF,I_EK,I_EP
!
END MODULE OUT_VALUES_7
!
!=======================================================================
!
MODULE OUT_VALUES_8
!
!  This module contains input values for print switches
!
      IMPLICIT NONE
!
      INTEGER               ::  I_VI,I_DI
!
END MODULE OUT_VALUES_8
!
!=======================================================================
!
MODULE OUT_VALUES_9
!
!  This module contains input values for print switches
!
      IMPLICIT NONE
!
      INTEGER               ::  I_EL,I_PO,I_RF
      INTEGER               ::  I_VC
!
END MODULE OUT_VALUES_9
!
!=======================================================================
!
MODULE OUT_VALUES_10
!
!  This module contains input values for print switches
!
      IMPLICIT NONE
!
      INTEGER               ::  I_FN,I_WR,I_TI
!
END MODULE OUT_VALUES_10
!
!=======================================================================
!
MODULE OUT_VALUES_P
!
!  This module contains input values for print switches
!
      IMPLICIT NONE
!
      INTEGER               ::  I_FP
!
END MODULE OUT_VALUES_P
!
!=======================================================================
!
MODULE OUT_CALC
!
!  This module contains input values for the calculators
!
      IMPLICIT NONE
!
      INTEGER               ::  I_C1,I_C2,I_C3,I_C4,I_C5
      INTEGER               ::  I_C6,I_C7,I_C8,I_C9,I_PP
!
END MODULE OUT_CALC
!
!=======================================================================
!
