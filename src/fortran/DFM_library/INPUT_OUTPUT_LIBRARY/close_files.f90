!
!=======================================================================
!
MODULE CLOSE_OUTFILES 
!
!  This module contains the subroutine that opens the output files
!
CONTAINS
!
!
!=======================================================================
!
      SUBROUTINE CLOSE_OUTPUT_FILES(IND)
!
!  This subroutine open the output files for printing
!
!
!  Input parameter:
!
!       * IND     :  integer specifying which files have to be closed 
!                       IND = 0  files indexed with input data file
!                       IND = 1  files not indexed with input data file
!
!  (see FUNCTION INDEX_FILE(I_UNIT) in outfiles.f90 for more details)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 26 Jan 2021
!
!
      USE OUT_VALUES_1
      USE OUT_VALUES_2
      USE OUT_VALUES_3
      USE OUT_VALUES_4
      USE OUT_VALUES_5
      USE OUT_VALUES_6
      USE OUT_VALUES_7
      USE OUT_VALUES_8
      USE OUT_VALUES_9
      USE OUT_VALUES_P
!
      USE OUTFILES
!
      USE PRINT_FILES
!
      IMPLICIT NONE
!
      INTEGER                 ::  IND
!
      IF(I_DF == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_DF)) CLOSE(IO_DF)                   ! dielectric function file
      END IF                                                        !
      IF(I_PZ == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_PZ)) CLOSE(IO_PZ)                   ! polarization function
      END IF                                                        !
      IF(I_SU == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_SU)) CLOSE(IO_SU)                   ! susceptibility function
      END IF                                                        !
      IF(I_CD == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_CD)) CLOSE(IO_CD)                   ! electrical conductivity
      END IF                                                        !
!
      IF(I_PD == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_PD)) CLOSE(IO_PD)                   ! plasmon dispersion file
      END IF                                                        !
      IF(I_EH == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_EH)) CLOSE(IO_EH)                   ! electron-hole dispersion file
      END IF                                                        !
      IF(I_E2 == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_E2)) CLOSE(IO_E2)                   ! two electron-hole dispersion
      END IF                                                        !
      IF(I_CF == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_CK)) CLOSE(IO_CK)                   ! screened Coulomb (k-space)
      END IF                                                        !
      IF(I_CR == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_CR)) CLOSE(IO_CR)                   ! screened Coulomb (real space)
      END IF                                                        !
      IF(I_PK == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_PK)) CLOSE(IO_PK)                   ! plasmon kinetic energy
      END IF                                                        !
!
      IF(I_LF == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_LF)) CLOSE(IO_LF)                   ! local-field correction file G(q,om)
      END IF                                                        !
      IF(I_IQ == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_IQ)) CLOSE(IO_IQ)                   ! G(q,inf) file
      END IF                                                        !
      IF(I_SF == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_SF)) CLOSE(IO_SF)                   ! structure factor file S(q,om)
      END IF                                                        !
      IF(I_PC == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_PC)) CLOSE(IO_PC)                   ! pair correlation function file
      END IF                                                        !
      IF(I_P2 == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_P2)) CLOSE(IO_P2)                   ! pair distribution function file
      END IF                                                        !
      IF(I_VX == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_VX)) CLOSE(IO_VX)                   ! vertex function Gamma(q,om)
      END IF                                                        !
      IF(I_DC == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_DC)) CLOSE(IO_DC)                   ! plasmon damping coefficient Im[eps]/q^2 
      END IF                                                        !
      IF(I_MD == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_MD)) CLOSE(IO_MD)                   ! momentum distribution
      END IF                                                        !
      IF(I_LD == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_LD)) CLOSE(IO_LD)                   ! Landau parameters
      END IF                                                        !
      IF(I_DP == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_DP)) CLOSE(IO_DP)                   ! damping file
      END IF                                                        !
      IF(I_LT == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_LT)) CLOSE(IO_LT)                   ! plasmon lifetime file
      END IF                                                        !
      IF(I_BR == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_BR)) CLOSE(IO_BR)                   ! plasmon broadening
      END IF                                                        !
      IF(I_PE == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_PE)) CLOSE(IO_PE)                   ! plasmon energy
      END IF                                                        !
      IF(I_QC == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_QC)) CLOSE(IO_QC)                   ! plasmon q-bounds
      END IF                                                        !
      IF(I_RL == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_RL)) CLOSE(IO_RL)                   ! relaxation time
      END IF                                                        !
      IF(I_KS == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_KS)) CLOSE(IO_KS)                   ! screening wave vector
      END IF                                                        !
      IF(I_OQ == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_OQ)) CLOSE(IO_OQ)                   ! omega = q * v_F
      END IF                                                        !
      IF(I_ME == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_ME)) CLOSE(IO_ME)                   ! moments of epsilon
      END IF                                                        !
      IF(I_MS == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_MS)) CLOSE(IO_MS)                   ! moments of S(q,omega)
      END IF                                                        !
      IF(I_ML == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_ML)) CLOSE(IO_ML)                   ! moments of loss function
      END IF                                                        !
      IF(I_MC == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_MC)) CLOSE(IO_MC)                   ! moments of conductivity
      END IF                                                        !
      IF(I_DE == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_DE)) CLOSE(IO_DE)                   ! derivative of Re[ dielectric function ]
      END IF                                                        !
      IF(I_ZE == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_ZE)) CLOSE(IO_ZE)                   ! Re[ dielectric function ] = 0
      END IF                                                        !
      IF(I_SR == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_SR)) CLOSE(IO_SR)                   ! sum rules for epsilon
      END IF                                                        !
      IF(I_CW == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_CW)) CLOSE(IO_CW)                   ! confinement wave function
      END IF                                                        !
      IF(I_CF == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_CF)) CLOSE(IO_CF)                   ! confinement potential
      END IF                                                        !
      IF(I_EM == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_EM)) CLOSE(IO_EM)                   ! effective mass
      END IF                                                        !
      IF(I_MF == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_MF)) CLOSE(IO_MF)                   ! mean free path
      END IF                                                        !
      IF(I_SP == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_SP)) CLOSE(IO_SP)                   ! spectral function
      END IF                                                        !
      IF(I_SE == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_SE)) CLOSE(IO_SE)                   ! self-energy
      END IF                                                        !
      IF(I_SB == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_SB)) CLOSE(IO_SB)                   ! subband energies
      END IF                                                        !
      IF(I_ES == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_ES)) CLOSE(IO_ES)                   ! Eliashberg function
      END IF                                                        !
      IF(I_GR == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_GR)) CLOSE(IO_GR)                   ! Grüneisen parameter
      END IF                                                        !
      IF(I_FD == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_FD)) CLOSE(IO_FD)                   ! Fermi-Dirac distribution
      END IF                                                        !
      IF(I_BE == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_BE)) CLOSE(IO_BE)                   ! Bose-Einstein distribution
      END IF                                                        !
      IF(I_MX == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_MX)) CLOSE(IO_MX)                   ! Maxwell distribution
      END IF                                                        !
      IF(I_SC == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_SC)) CLOSE(IO_SC)                   ! scale parameters
      END IF                                                        !
      IF(I_DS == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_DS)) CLOSE(IO_DS)                   ! density of states
      END IF                                                        !
      IF(I_NV == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_NV)) CLOSE(IO_NV)                   ! Nevanlinaa function
      END IF                                                        !
      IF(I_MT == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_MT)) CLOSE(IO_MT)                   ! time domain memory function
      END IF                                                        !
!
      IF(I_GP == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_GP)) CLOSE(IO_GP)                   ! grand partition function
      END IF                                                        !
      IF(I_PR == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_PR)) CLOSE(IO_PR)                   ! electronic pressure
      END IF                                                        !
      IF(I_CO == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_CO)) CLOSE(IO_CO)                   ! compressibility
      END IF                                                        !
      IF(I_CP == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_CP)) CLOSE(IO_CP)                   ! chemical potential
      END IF                                                        !
      IF(I_BM == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_BM)) CLOSE(IO_BM)                   ! bulk modulus
      END IF                                                        !
      IF(I_SH == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_SH)) CLOSE(IO_SH)                   ! shear modulus
      END IF                                                        !
      IF(I_S0 == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_S0)) CLOSE(IO_S0)                   ! zero sound velocity
      END IF                                                        !
      IF(I_S1 == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_S1)) CLOSE(IO_S1)                   ! first sound velocity
      END IF                                                        !
      IF(I_DT == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_DT)) CLOSE(IO_DT)                   ! Debye temperature
      END IF                                                        !
      IF(I_PS == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_PS)) CLOSE(IO_PS)                   ! Pauli paramagnetic susceptibility
      END IF                                                        !
      IF(I_IE == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_IE)) CLOSE(IO_IE)                   ! internal energy
      END IF                                                        !
      IF(I_EI == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_EI)) CLOSE(IO_EI)                   ! excess internal energy
      END IF                                                        !
      IF(I_FH == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_FH)) CLOSE(IO_FH)                   ! Helmholtz free energy
      END IF                                                        !
      IF(I_EY == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_EY)) CLOSE(IO_EY)                   ! entropy
      END IF                                                        !
!
      IF(I_EF == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_EF)) CLOSE(IO_EF)                   ! Fermi energy
      END IF                                                        !
      IF(I_KF == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_KF)) CLOSE(IO_KF)                   ! Fermi momentum
      END IF                                                        !
      IF(I_VF == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_VF)) CLOSE(IO_VF)                   ! Fermi velocity
      END IF                                                        !
      IF(I_TE == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_TE)) CLOSE(IO_TE)                   ! Fermi temperature
      END IF                                                        !
      IF(I_DL == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_DL)) CLOSE(IO_DL)                   ! Fermi density of states
      END IF                                                        !
!
      IF(I_TW == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_TW)) CLOSE(IO_TW)                   ! thermal De Broglie wavelength
      END IF                                                        !
      IF(I_VT == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_VT)) CLOSE(IO_VT)                   ! thermal velocity
      END IF                                                        !
      IF(I_TC == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_TC)) CLOSE(IO_TC)                   ! thermal conductivity
      END IF                                                        !
!
      IF(I_EG == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_EG)) CLOSE(IO_EG)                   ! ground state energy
      END IF                                                        !
      IF(I_EX == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_EX)) CLOSE(IO_EX)                   ! exchange energy
      END IF                                                        !
      IF(I_XC == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_XC)) CLOSE(IO_XC)                   ! exchange correlation energy
      END IF                                                        !
      IF(I_EC == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_EC)) CLOSE(IO_EC)                   ! correlation energy
      END IF                                                        !
      IF(I_HF == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_HF)) CLOSE(IO_HF)                   ! Hartree-Fock energy
      END IF                                                        !
      IF(I_EK == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_EK)) CLOSE(IO_EK)                   ! kinetic energy
      END IF                                                        !
      IF(I_EP == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_EP)) CLOSE(IO_EP)                   ! potential energy
      END IF                                                        !
!
      IF(I_VI == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_VI)) CLOSE(IO_VI)                   ! shear viscosity
      END IF                                                        !
      IF(I_DI == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_DI)) CLOSE(IO_DI)                   ! diffusion coefficient
      END IF                                                        !
!
      IF(I_FP == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_FP)) CLOSE(IO_FP)                   ! fluctuation potential file
      END IF                                                        !
      IF(I_EL == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_EL)) CLOSE(IO_EL)                   ! energy loss function
      END IF                                                        !
      IF(I_PO == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_PO)) CLOSE(IO_PO)                   ! stopping power
      END IF                                                        !
      IF(I_RF == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_RF)) CLOSE(IO_RF)                   ! refractive index
      END IF                                                        !
      IF(I_VC == 1) THEN                                            !
        IF(IND == INDEX_FILE(IO_VC)) CLOSE(IO_VC)                   ! dynamic screened Coulomb potential V(q,omega)
      END IF                                                        !
!
      END SUBROUTINE CLOSE_OUTPUT_FILES
!
END MODULE CLOSE_OUTFILES
 
