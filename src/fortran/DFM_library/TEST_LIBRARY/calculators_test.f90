!
!=======================================================================
!
MODULE CALCULATORS_TEST
!
      USE ACCURACY_REAL
!
!  This module contains the test subroutines  
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_TEST(IX,X)
!
!  This subroutine tests different subroutines
!
!  Input parameters:
!
!       * X        : dimensionless factor   --> X = q / (2 * k_F)  
!
!
!  Intermediate parameters:
!
!       * RS       : Wigner-Seitz radius (in units of a_0)
!       * T        : temperature in SI
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 15 Sep 2020
!
!
      USE DIMENSION_CODE,   ONLY : NSIZE
      USE REAL_NUMBERS,     ONLY : ONE
      USE FERMI_SI,         ONLY : EF_SI
      USE MATERIAL_PROP,    ONLY : RS
      USE EXT_FIELDS,       ONLY : T
!
      USE E_GRID
!
      USE LF_VALUES,        ONLY : GQ_TYPE
      USE SF_VALUES,        ONLY : SQ_TYPE
!
      USE RELAXATION_TIME_STATIC
!
      IMPLICIT NONE
!
      INTEGER               ::  IX,IE
!
      REAL (WP), INTENT(IN) ::  X
      REAL (WP)             ::  E,EK
      REAL (WP)             ::  TAU_1,TAU_2,TAU_3,TAU_4,TAU_5
!
      IF(IX == 1) THEN
        TAU_1 = BACA_RT_3D(RS,T)                                    !
        TAU_2 = FSTB_RT_3D(RS)                                      !
        TAU_3 = UTIC_RT_3D(X,RS,T,SQ_TYPE,GQ_TYPE)                  !
!        PRINT *,'BACA_RT_3D = ',TAU_1                               !
!        PRINT *,'FSTB_RT_3D = ',TAU_2                               !
!        PRINT *,'UTIC_RT_3D = ',TAU_3                               !
      END IF                                                        !
!
      DO IE = 1, N_E                                                ! energy loop
!
        E  = E_MIN + FLOAT(IE - 1) * E_STEP                         ! E = hbar omega / E_F
        EK = E * EF_SI
!
        TAU_4 = QIVI_RT_3D(EK,X,T)                                  !
        TAU_5 = QIV2_RT_3D(EK,X,T)                                  !
!
        IF(TAU_4 < ONE) THEN                                        !
!          PRINT *,'QIVI_RT_3D = ',TAU_4                             !
!          PRINT *,'QIV2_RT_3D = ',TAU_5                             !
        END IF                                                      !
!
!
      END DO                                                        !
!
      END SUBROUTINE CALC_TEST
!
END MODULE CALCULATORS_TEST
