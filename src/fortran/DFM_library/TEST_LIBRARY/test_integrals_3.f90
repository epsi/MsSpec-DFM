!
!=======================================================================
!
MODULE TEST_INTEGRALS_3
!
      USE ACCURACY_REAL
!
!  This module contains the test subroutine for MODULE SPECIFIC_INT_3
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_TEST_INT_3
!
!  This subroutine allows to test the integrals contained in 
!                      MODULE SPECIFIC_INT_3
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  3 Dec 2020
!
!
      USE MATERIAL_PROP,        ONLY : RS,DMN
      USE EXT_FIELDS,           ONLY : T
      USE PC_VALUES,            ONLY : GR_TYPE
      USE PD_VALUES,            ONLY : RH_TYPE 
!
      USE SPECIFIC_INT_3
!
      IMPLICIT NONE
!
      INTEGER               ::  LOGF
      INTEGER               ::  IN_MODE,NMAX,L
!
      REAL (WP)             ::  X_MAX,A
      REAL (WP)             ::  IN
!
      LOGF = 6                                                      ! log file
!
      NMAX    = 1000                                                ! number of integration points
      X_MAX   = 4.0E0_WP                                            ! upper intergration bound
      L       = 2                                                   ! power of x
      A       = 1.0E0_WP                                            !
!
      WRITE(LOGF,10)                                                !
      WRITE(LOGF,20)                                                !
      WRITE(LOGF,30)                                                !
      WRITE(LOGF,20)                                                !
!
      WRITE(LOGF,40) NMAX,X_MAX                                     !
      WRITE(LOGF,20)                                                !
!
!  Computing the integral
!
      DO IN_MODE = 1, 6                                             !
!
        CALL INT_GRM1(NMAX,X_MAX,IN_MODE,RS,T,A,L,GR_TYPE,        & !
                      RH_TYPE,IN)                                   !
!
        IF(IN_MODE == 1) THEN                                       !
          WRITE(LOGF,50) IN_MODE,IN                                 !
        ELSE IF(IN_MODE == 2 .OR. IN_MODE == 3) THEN                !
          WRITE(LOGF,60) IN_MODE,L,IN                               !
        ELSE                                                        !
          WRITE(LOGF,70) IN_MODE,A,IN                               !
        END IF                                                      !
!
      END DO                                                        !
!
      WRITE(LOGF,20)                                                !
      WRITE(LOGF,80)                                                !
!
!  Formats:
!
  10  FORMAT(6X,'_________________________________________________________')
  20  FORMAT(5X,'|                                                         |')
  30  FORMAT(5X,'|',3X,'Test of the integrals contained in SPECIFIC_INT_3     |')
  40  FORMAT(5X,'|',5X,'Integr. points: ',I4,'  Up. bound: ',F8.3,11X,'|')
  50  FORMAT(5X,'|',5X,'IN_MODE = ',I1,20X,'INT = ',F8.3,7X,'|')
  60  FORMAT(5X,'|',5X,'IN_MODE = ',I1,'  L = ',I4,10X,'INT = ',F8.3,7X,'|')
  70  FORMAT(5X,'|',5X,'IN_MODE = ',I1,'  A = ',F8.3,6X,'INT = ',F8.3,7X,'|')
  80  FORMAT(5X,'|_________________________________________________________|',/)
!
      END SUBROUTINE CALC_TEST_INT_3
!
END MODULE TEST_INTEGRALS_3
