!
!=======================================================================
!
MODULE TEST_INTEGRALS_8
!
      USE ACCURACY_REAL
!
!  This module contains the test subroutine for MODULE SPECIFIC_INT_8
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_TEST_INT_8
!
!  This subroutine allows to test the integrals contained in 
!                      MODULE SPECIFIC_INT_38
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  3 Dec 2020
!
!
      USE Q_GRID
!
      USE REAL_NUMBERS,     ONLY : HALF
!
      USE SPECIFIC_INT_8
!
      IMPLICIT NONE
!
      INTEGER                ::  LOGF                  ! log file index
      INTEGER                ::  IQ                    ! loop index
      
!
      REAL (WP)              ::  Q,X
      REAL (WP)              ::  A,B
      REAL (WP)              ::  INTG
!
      LOGF = 6                                                      ! log file
!
      A = 1.0E0_WP                                                  !
      B = 1.0E0_WP                                                  !
!
      OPEN(UNIT = 2, FILE = 'test_int8.dat', STATUS = 'unknown')    !
!
      WRITE(LOGF,10)                                                !
      WRITE(LOGF,20)                                                !
      WRITE(LOGF,30)                                                !
      WRITE(LOGF,20)                                                !
!
!  Computing the q values
!
      DO IQ = 1, N_Q                                                !
!
        Q = Q_MIN + FLOAT(IQ - 1) * Q_STEP                          ! Q = q/k_F
!
        X = HALF * Q                                                ! X = q/(2k_f)
!
        CALL INT_ARB(X,A,B,INTG)                                    !
!
        WRITE(2,*) X,INTG                                           !
!
      END DO                                                        !
!
      CLOSE(2)
!
      WRITE(LOGF,40)                                                !
      WRITE(LOGF,20)                                                !
      WRITE(LOGF,80)                                                !
!
!  Format:
!
  10  FORMAT(6X,'_________________________________________________________')
  20  FORMAT(5X,'|                                                         |')
  30  FORMAT(5X,'|',3X,'Test of the integrals contained in SPECIFIC_INT_8     |')
  40  FORMAT(5X,'|',7X,'Result written into file "test_int8.dat"',10X,'|')
  80  FORMAT(5X,'|_________________________________________________________|',/)
!
      END SUBROUTINE CALC_TEST_INT_8
!
END MODULE TEST_INTEGRALS_8
 
