!
!=======================================================================
!
MODULE TEST_INT_HUBBARD
!
      USE ACCURACY_REAL
!
!  This module contains the test subroutine for the Hubbard double integral
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_TEST_HUBBARD
!
!  This subroutine tests the Hubbard double integral occuring 
!    in the calculation of the correlation energy
!
!  Reference : J. Hubbard, Proc. Roy. Soc. A 243, 336-352 (1958)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 15 Dec 2020
!
!
!
      USE DIMENSION_CODE,   ONLY : NZ_MAX
      USE MATERIAL_PROP,    ONLY : RS
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,TWO,HALF,FOURTH
      USE PI_ETC,           ONLY : PI,PI_INV
      USE UTILITIES_1,      ONLY : ALFA
      USE INTEGRATION,      ONLY : INTEGR_L
!
      IMPLICIT NONE
!
      INTEGER               ::  IX,IY
      INTEGER               ::  LOGF
!
      REAL (WP)             ::  XI,HX,HY
      REAL (WP)             ::  X,X3,Y
      REAL (WP)             ::  A(NZ_MAX,NZ_MAX)
      REAL (WP)             ::  SIGMA(NZ_MAX,NZ_MAX)
      REAL (WP)             ::  F1(NZ_MAX),F2(NZ_MAX)
      REAL (WP)             ::  INT_1,INT_2
      REAL (WP)             ::  NUM1,NUM2,DEN1,DEN2,Z1,Z2
!
      REAL (WP), PARAMETER  ::  MX = 5.0E0_WP                       ! upper integration
      REAL (WP), PARAMETER  ::  MY = 5.0E0_WP                       !  bounds in x and y
!
      REAL (WP), PARAMETER  ::  SM = 1.0E-8_WP                      ! starting grid value
!
      REAL (WP)             ::  FLOAT,LOG,ABS
!
      LOGF = 6                                                      ! log file
!
      XI = TWO * ALFA('3D') * PI_INV * RS                           ! ref. 1 eq. (28)
!
      HX = MX / FLOAT(NZ_MAX - 1)                                   ! x-step
      HY = MY / FLOAT(NZ_MAX - 1)                                   ! y-step
!
      WRITE(LOGF,10)                                                !
      WRITE(LOGF,20)                                                !
      WRITE(LOGF,30)                                                !
      WRITE(LOGF,20)                                                !
!
      WRITE(LOGF,40) NZ_MAX,MX                                      !
      WRITE(LOGF,40) NZ_MAX,MY                                      !
      WRITE(LOGF,20)                                                !
!
!  Construction the functions A and Sigma
!
      DO IX = 1, NZ_MAX                                             !
!
        X  = SM + FLOAT(IX - 1) * HX                                !
        X3 = X * X * X                                              !
!
        DO IY = 1, NZ_MAX                                           !
!
          Y = SM + FLOAT(IY - 1) * HY                               !
!
!  Calculation of Sigma(x,y)                                        ! ref. 1 eq. (26)
!
          IF(Y > X * (X + TWO)) THEN                                !
            SIGMA(IX,IY) = ZERO                                     !
          ELSE IF(X > TWO .AND. Y < X * (X - TWO)) THEN             !
            SIGMA(IX,IY) = ZERO                                     !
          ELSE IF( X > TWO .AND. X * (X - TWO) < Y .AND.          & !
                                 Y < X * (X + TWO)                & !
                                 .OR.                             & !
                  X < TWO .AND. X * (TWO - X) < Y .AND.           & !
                                Y < X * (X + TWO)                 & !
                 ) THEN                                             !
            SIGMA(IX,IY) = - PI * XI * HALF * ( ONE - FOURTH *    & !
                                                (Y / X - X)**2    & !
                                              ) / X3                !
          ELSE IF(X < TWO .AND. ZERO < Y .AND.                    & !
                  Y < X * (TWO - X)) THEN                           !
            SIGMA(IX,IY) = - PI * XI * Y * HALF / X3                !
          END IF                                                    !
!
!  Calculation of A(x,y)                                            ! ref. 1 eq. (27)
!
          NUM1 = Y - X * (X + TWO)                                  ! 
          NUM2 = Y + X * (X + TWO)                                  ! 
          DEN1 = Y - X * (X - TWO)                                  ! 
          DEN2 = Y + X * (X - TWO)                                  ! 
!
          Z1 = (Y / X - X)**2                                       ! 
          Z2 = (Y / X + X)**2                                       ! 
!
          A(IX,IY) = - XI * ( X +                                 & !
                              HALF * (ONE - FOURTH * Z1) *        & !
                              LOG(ABS(NUM1 / DEN1)) +             & !
                              HALF * (ONE - FOURTH * Z2) *        & !
                              LOG(ABS(NUM2 / DEN2))               & !
                            ) / X3                                  !
!
!  y-integrand
!
          F2(IY) = ATAN( SIGMA(IX,IY) / (ONE - A(IX,IY)) ) -      & !
                   SIGMA(IX,IY)                                     !
!
        END DO                                                      !
!
!  Computing the integral over  y
!
        CALL INTEGR_L(F2,HY,NZ_MAX,NZ_MAX,INT_2,1)                  !
!
!  x-integrand
!
        F1(IX) = X * X * INT_2                                      !
!
        
      END DO                                                        !
!
!  Computing the integral over  x
!
      CALL INTEGR_L(F1,HX,NZ_MAX,NZ_MAX,INT_1,1)                    !
!
      WRITE(LOGF,60) INT_1                                          !
!
      WRITE(LOGF,80)                                                !
!
!  Formats:
!
  10  FORMAT(6X,'_________________________________________________________')
  20  FORMAT(5X,'|                                                         |')
  30  FORMAT(5X,'|',3X,'Test of the integrals contained in HUbbard eps_c      |')
  40  FORMAT(5X,'|',5X,'Integr. points: ',I4,'  Up. bound x: ',F8.3,9X,'|')
  50  FORMAT(5X,'|',5X,'Integr. points: ',I4,'  Up. bound y: ',F8.3,9X,'|')
  60  FORMAT(5X,'|',36X,'INT = ',F8.3,7X,'|')
   80  FORMAT(5X,'|_________________________________________________________|',/)
!
      END SUBROUTINE CALC_TEST_HUBBARD
!
END MODULE TEST_INT_HUBBARD
