!
!=======================================================================
!
MODULE TEST_INTEGRALS_2
!
      USE ACCURACY_REAL
!
!  This module contains the test subroutine for MODULE SPECIFIC_INT_2
!
CONTAINS
!
!=======================================================================
!
      SUBROUTINE CALC_TEST_INT_2
!
!  This subroutine allows to test the integrals contained in 
!                      MODULE SPECIFIC_INT_2
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  3 Dec 2020
!
!
      USE MATERIAL_PROP,        ONLY : RS,DMN
      USE EXT_FIELDS,           ONLY : T
      USE LF_VALUES,            ONLY : GQ_TYPE 
      USE SF_VALUES,            ONLY : SQ_TYPE
!
      USE FERMI_SI,             ONLY : KF_SI
!
      USE SCREENING_VEC,        ONLY : THOMAS_FERMI_VECTOR
      USE SPECIFIC_INT_2
!
      IMPLICIT NONE
!
      INTEGER               ::  LOGF
      INTEGER               ::  IN_MODE,NMAX
      INTEGER               ::  L
!
      REAL (WP)             ::  KS
      REAL (WP)             ::  X_MAX,AA,A,X_TF
      REAL (WP)             ::  IN
!
      LOGF = 6                                                      ! log file
!
      NMAX    = 1000                                                ! number of integration points
      X_MAX   = 4.0E0_WP                                            ! upper intergration bound
      L       = 2                                                   ! power of x
      AA      = 1.0E0_WP                                            !
!
!  Computing Thomas-Fermi screening vector
!
      CALL THOMAS_FERMI_VECTOR(DMN,KS)                              !
      X_TF =  KS / KF_SI                                            ! q_{TF} / k_F 
!
      WRITE(LOGF,10)                                                !
      WRITE(LOGF,20)                                                !
      WRITE(LOGF,30)                                                !
      WRITE(LOGF,20)                                                !
!
      WRITE(LOGF,40) NMAX,X_MAX                                     !
      WRITE(LOGF,20)                                                !
!
!  Computing the integral
!
      DO IN_MODE = 1, 7                                             !
!
        IF(IN_MODE == 3) THEN                                       !
          A = X_TF                                                  !
        ELSE                                                        !
          A = AA                                                    !
        END IF                                                      !
!
        CALL INT_SQM1(NMAX,X_MAX,IN_MODE,RS,T,X_TF,L,SQ_TYPE,     & !
                           GQ_TYPE,IN)                              !
!
        IF(IN_MODE == 1) THEN                                       !
          WRITE(LOGF,50) IN_MODE,IN                                 !
        ELSE IF(IN_MODE == 5) THEN                                  !
          WRITE(LOGF,60) IN_MODE,L,IN                               !
        ELSE                                                        !
          WRITE(LOGF,70) IN_MODE,A,IN                               !
        END IF                                                      !
!
      END DO                                                        !
!
      WRITE(LOGF,20)                                                !
      WRITE(LOGF,80)                                                !
!
!  Formats:
!
  10  FORMAT(6X,'_________________________________________________________')
  20  FORMAT(5X,'|                                                         |')
  30  FORMAT(5X,'|',3X,'Test of the integrals contained in SPECIFIC_INT_2     |')
  40  FORMAT(5X,'|',5X,'Integr. points: ',I4,'  Up. bound: ',F8.3,11X,'|')
  50  FORMAT(5X,'|',5X,'IN_MODE = ',I1,20X,'INT = ',F8.3,7X,'|')
  60  FORMAT(5X,'|',5X,'IN_MODE = ',I1,'  L = ',I4,10X,'INT = ',F8.3,7X,'|')
  70  FORMAT(5X,'|',5X,'IN_MODE = ',I1,'  A = ',F8.3,6X,'INT = ',F8.3,7X,'|')
  80  FORMAT(5X,'|_________________________________________________________|',/)
!
      END SUBROUTINE CALC_TEST_INT_2
!
END MODULE TEST_INTEGRALS_2
