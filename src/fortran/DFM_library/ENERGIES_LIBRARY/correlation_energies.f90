!
!=======================================================================
!
MODULE CORRELATION_ENERGIES
!
      USE ACCURACY_REAL
!
CONTAINS
!
!------ 1) 3D case --------------------------------------------
!
!=======================================================================
!
      FUNCTION EC_3D(EC_TYPE,IMODE,RS,T)
!
!  This subroutine computes the 3D correlation energy EC
!    at a given value RS
!
!
!  Input parameters:
!
!       * EC_TYPE  : type of correlation energy functional
!                       EC_TYPE = 'GEBR_W'   --> Gell-Mann and Brueckner
!                       EC_TYPE = 'CAMA_W'   --> Carr and Maradudin
!                       EC_TYPE = 'EHTY_W'   --> Endo-Horiuchi-Takada-Yasuhara
!                       EC_TYPE = 'HELU_W'   --> Hedin and Lundqvist
!                       EC_TYPE = 'VBHE_W'   --> von Barth and Hedin
!                       EC_TYPE = 'PEZU_W'   --> Perdew and Zunger
!                       EC_TYPE = 'WIGN_S'   --> Wigner
!                       EC_TYPE = 'NOPI_S'   --> Nozières and Pines
!                       EC_TYPE = 'LIRO_S'   --> Lindgren and Rosen
!                       EC_TYPE = 'PEZU_S'   --> Perdew and Zunger
!                       EC_TYPE = 'REHI_S'   --> Rebei and Hitchon
!                       EC_TYPE = 'GGSB_G'   --> Gori-Giorgi-Sacchetti-Bachelet
!                       EC_TYPE = 'PRKO_G'   --> Proynov and Kong
!                       EC_TYPE = 'VWNU_G'   --> Vosko, Wilk and Nusair
!                       EC_TYPE = 'PEWA_G'   --> Perdew and Wang
!                       EC_TYPE = 'HUBB_G'   --> Hubbard
!                       EC_TYPE = 'CHAC_G'   --> Chachiyo
!                       EC_TYPE = 'ISKO_T'   --> Isihara and Kojima
!       * IMODE    : choice of parameters
!                       IMODE = 1 : no spin polarization 
!                       IMODE = 2 : fully spin-polarized
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * T        : temperature (SI)     
!
!  Output parameters:
!
!       * EC_3D    : value at RS
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Sep 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 6)   ::  EC_TYPE
!
      REAL (WP)             ::  EC_3D,RS,T
      REAL (WP)             ::  XI
!
      INTEGER               ::  IMODE
!
      XI = 0.0E0_WP                                                 ! temporary value
!
      IF(EC_TYPE == 'GEBR_W') THEN                                  !
        EC_3D = EC_GB_W(RS)                                         !
      ELSE IF(EC_TYPE == 'CAMA_W') THEN                             !
        EC_3D = EC_CM_W(RS)                                         !
      ELSE IF(EC_TYPE == 'EHTY_W') THEN                             !
        EC_3D = EC_EH_W(RS)                                         !
      ELSE IF(EC_TYPE == 'HELU_W') THEN                             !
        EC_3D = EC_HL_W(RS)                                         !
      ELSE IF(EC_TYPE == 'VBHE_W') THEN                             !                            
        EC_3D = EC_BH_W(RS,IMODE)                                   !
      ELSE IF(EC_TYPE == 'PEZU_W') THEN                             !
        EC_3D = EC_PZ_W(RS,IMODE)                                   !
      ELSE IF(EC_TYPE == 'WIGN_S') THEN                             !
        EC_3D = EC_W_S(RS)                                          !
      ELSE IF(EC_TYPE == 'NOPI_S') THEN                             !
        EC_3D = EC_NP_S(RS)                                         !
      ELSE IF(EC_TYPE == 'LIRO_S') THEN                             !
        EC_3D = EC_LR_S(RS)                                         !
      ELSE IF(EC_TYPE == 'PEZU_S') THEN                             !
        EC_3D = EC_PZ_S(RS,IMODE)                                   !
      ELSE IF(EC_TYPE == 'REHI_S') THEN                             !
        EC_3D = EC_RH_S(RS)                                         !
      ELSE IF(EC_TYPE == 'GGSB_G') THEN                             !
        EC_3D = EC_GG_G(RS)                                         !
      ELSE IF(EC_TYPE == 'PRKO_G') THEN                             !
        EC_3D = EC_PK_G(RS,IMODE,XI)                                !
      ELSE IF(EC_TYPE == 'VWNU_G') THEN                             !
        EC_3D = EC_VWN_G(RS,IMODE)                                  !
      ELSE IF(EC_TYPE == 'PEWA_G') THEN                             !
        EC_3D = EC_PW_G(RS,IMODE)                                   !
      ELSE IF(EC_TYPE == 'HUBB_G') THEN                             !
        EC_3D = EC_HU_G(RS)                                         !
      ELSE IF(EC_TYPE == 'CHAC_G') THEN                             !
        EC_3D = EC_CH_G(RS,IMODE)                                   !
      ELSE IF(EC_TYPE == 'ISKO_T') THEN                             !
        EC_3D = EC_IK_T(RS,T)                                       !
      END IF                                                        !
!
      END FUNCTION EC_3D
!
!=======================================================================
!
      SUBROUTINE DERIVE_EC_3D(EC_TYPE,IMODE,IDERIV,RS,T,  & 
                              D_EC_1,D_EC_2)
!
!  This subroutine computes the first and second derivative 
!    of the correlation energy E_c with repect to r_s
!    at a given value RS
!
!
!  Input parameters:
!
!       * EC_TYPE  : type of correlation energy functional
!                       EC_TYPE = 'GEBR_W'   --> Gell-Mann and Brueckner
!                       EC_TYPE = 'CAMA_W'   --> Carr and Maradudin
!                       EC_TYPE = 'EHTY_W'   --> Endo-Horiuchi-Takada-Yasuhara
!                       EC_TYPE = 'HELU_W'   --> Hedin and Lundqvist
!                       EC_TYPE = 'VBHE_W'   --> von Barth and Hedin
!                       EC_TYPE = 'PEZU_W'   --> Perdew and Zunger
!                       EC_TYPE = 'WIGN_S'   --> Wigner
!                       EC_TYPE = 'NOPI_S'   --> Nozières and Pines
!                       EC_TYPE = 'LIRO_S'   --> Lindgren and Rosen
!                       EC_TYPE = 'PEZU_S'   --> Perdew and Zunger
!                       EC_TYPE = 'REHI_S'   --> Rebei and Hitchon
!                       EC_TYPE = 'GGSB_G'   --> Gori-Giorgi-Sacchetti-Bachelet
!                       EC_TYPE = 'PRKO_G'   --> Proynov and Kong
!                       EC_TYPE = 'VWNU_G'   --> Vosko, Wilk and Nusair
!                       EC_TYPE = 'PEWA_G'   --> Perdew and Wang
!                       EC_TYPE = 'HUBB_G'   --> Hubbard
!                       EC_TYPE = 'CHAC_G'   --> Chachiyo
!                       EC_TYPE = 'ISKO_T'   --> Isihara and Kojima
!       * IMODE    : choice of parameters
!                       IMODE = 1 : no spin polarization 
!                       IMODE = 2 : fully spin-polarized
!       * IDERIV   : type of n_point formula used for derivation (n = IDERIV)
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * T        : temperature (SI)     
!
!  Output parameters:
!
!       * D_EC_1    : first derivative at RS
!       * D_EC_2    : second derivative at RS
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
!
      USE DIMENSION_CODE,   ONLY : ND_MAX
      USE DERIVATION  
      USE INTERPOLATION,    ONLY : INTERP_NR,SPLINE,SPLINT
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 6)   ::  EC_TYPE
!
      INTEGER               ::  IMODE,IDERIV,I,LOGF
!
      REAL (WP)             ::  RS,RI,T
      REAL (WP)             ::  D_EC_1,D_EC_2
!
      REAL (WP)             ::  R(ND_MAX),EC(ND_MAX)
      REAL (WP)             ::  D_EC(ND_MAX),DD_EC(ND_MAX)
      REAL (WP)             ::  R_MIN,R_MAX,STEP
      REAL (WP)             ::  XI
!
      REAL (WP)             ::  FLOAT
!
      XI = 0.0E0_WP                                                 ! temporary value
!
      R_MIN =  0.01E0_WP                                            !
      R_MAX = 50.01E0_WP                                            !
      STEP  = (R_MAX - R_MIN) / FLOAT(ND_MAX-1)                     !
!
      LOGF=6                                                        !
!
!  Storing the correlation energy EC as a function of RS
!
      DO I=1,ND_MAX                                                 !
!
        R(I)=R_MIN+FLOAT(I-1)*STEP                                  !
        RI=R(I)                                                     !
!
        IF(EC_TYPE == 'GEBR_W') THEN                                !
          EC(I) = EC_GB_W(RI)                                       !
        ELSE IF(EC_TYPE == 'CAMA_W') THEN                           !
          EC(I) = EC_CM_W(RI)                                       !
        ELSE IF(EC_TYPE == 'EHTY_W') THEN                           !
          EC(I) = EC_EH_W(RI)                                       !
        ELSE IF(EC_TYPE == 'HELU_W') THEN                           !
          EC(I) = EC_HL_W(RI)                                       !
        ELSE IF(EC_TYPE == 'VBHE_W') THEN                           !                            
          EC(I) = EC_BH_W(RS,IMODE)                                 !
        ELSE IF(EC_TYPE == 'PEZU_W') THEN                           !
          EC(I) = EC_PZ_W(RI,IMODE)                                 !
        ELSE IF(EC_TYPE == 'WIGN_S') THEN                           !
          EC(I) = EC_W_S(RI)                                        !
        ELSE IF(EC_TYPE == 'NOPI_S') THEN                           !
          EC(I) = EC_NP_S(RI)                                       !
        ELSE IF(EC_TYPE == 'LIRO_S') THEN                           !
          EC(I) = EC_LR_S(RI)                                       !
        ELSE IF(EC_TYPE == 'PEZU_S') THEN                           !
          EC(I) = EC_PZ_S(RI,IMODE)                                 !
        ELSE IF(EC_TYPE == 'REHI_S') THEN                           !
          EC(I) = EC_RH_S(RI)                                       !
        ELSE IF(EC_TYPE == 'GGSB_G') THEN                           !
          EC(I) = EC_RH_S(RI)                                       !
        ELSE IF(EC_TYPE == 'PRKO_G') THEN                           !
          EC(I) = EC_PK_G(RI,IMODE,XI)                              !
        ELSE IF(EC_TYPE == 'VWNU_G') THEN                           !
          EC(I) = EC_VWN_G(RI,IMODE)                                !
        ELSE IF(EC_TYPE == 'PEWA_G') THEN                           !
          EC(I) = EC_PW_G(RI,IMODE)                                 !
        ELSE IF(EC_TYPE == 'HUBB_G') THEN                           !
          EC(I) = EC_HU_G(RS)                                       !
        ELSE IF(EC_TYPE == 'CHAC_G') THEN                           !
          EC(I) = EC_CH_G(RS,IMODE)                                 !
        ELSE IF(EC_TYPE == 'ISKO_T') THEN                           !
          EC(I) = EC_IK_T(RI,T)                                     !
        END IF                                                      !
!
      END DO                                                        !
!
!  Computing the first and second derivatives 
!    with a IDERIV-point formula
!
      CALL DERIV_1(EC,ND_MAX,IDERIV,STEP,D_EC)                      !
      CALL DERIV_1(D_EC,ND_MAX,IDERIV,STEP,DD_EC)                   !
!
!  Interpolation of derivatives at RS
!
      CALL INTERP_NR(LOGF,R,D_EC,ND_MAX,RS,D_EC_1)                  !
      CALL INTERP_NR(LOGF,R,DD_EC,ND_MAX,RS,D_EC_2)                 !
!
      END SUBROUTINE DERIVE_EC_3D
!
!=======================================================================
!
!             Correlation energy functionals (in Ryd)
!
!  Different regimes:  * weak coupling          : r_s << 1
!                      * metallic state         : 2 <= r_s  <= 6
!                      * Wigner crystallization : r_s >= 100
!
!
!=======================================================================
!
!
!  (1) Weak coupling regime: _W
!
!
!=======================================================================
!
      FUNCTION EC_GB_W(RS)
!
!  Gell-Mann and Brueckner correlation energy
!
!
!  Reference: M. Gell-Mann and K. A. Brueckner, Phys. Rev. 106, 
!                364-368 (1957)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!
!  Output parameters:
!
!       * EC_GB_W  : correlation energy (in Ry)
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE
      USE PI_ETC,           ONLY : PI2
      USE ZETA_RIEMANN
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  EC_GB_W
      REAL (WP)             ::  A,B
!
      REAL (WP)             ::  LOG
!
      A = TWO * (ONE - LOG(TWO)) / PI2                              !
      B = TWO * LOG(TWO) / THREE - THREE * ZETA(3) / PI2            !
!
      EC_GB_W = A * LOG(RS) - B                                     ! ref. 1 eq. (28)
!
      END FUNCTION EC_GB_W
!
!=======================================================================
!
      FUNCTION EC_CM_W(RS)
!
!  Carr and Maradudin correlation energy
!
!
!  Reference: W. J. Carr and A. A. Maradudin, Phys. Rev. 133, 
!                A371-A374 (1964)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!
!  Output parameters:
!
!       * EC_GB_W  : correlation energy (in Ry)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE
      USE PI_ETC,           ONLY : PI2
      USE ZETA_RIEMANN
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  EC_CM_W
      REAL (WP)             ::  A,B,C,D,E
!
      REAL (WP)             ::  LOG
!
      A = TWO * (ONE - LOG(TWO)) / PI2                              !
      B = TWO * LOG(TWO) / THREE - THREE * ZETA(3) / PI2            !
      C =   0.018E0_WP                                              !
      D =   0.036E0_WP                                              !
      E =   0.0013E0_WP                                             !
!
      EC_CM_W = A * LOG(RS) - B + RS * (C * LOG(RS) + E - D)        ! ref. 1 eq. (27)
!
      END FUNCTION EC_CM_W
!
!=======================================================================
!
      FUNCTION EC_EH_W(RS)
!
!  Endo-Horiuchi-Takada-Yasuhara correlation energy
!
!
!  Reference: T. Endo, M. Horiuchi, Y. Takada and H. Yasuhara,
!                Phys. Rev. B 59, 7367-7372 (1999)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!
!  Output parameters:
!
!       * EC_EH_W  : correlation energy (in Ry)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO
      USE PI_ETC,           ONLY : PI2
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  EC_EH_W
      REAL (WP)             ::  AA,BB,CC,DD
!
      REAL (WP), PARAMETER  ::  A = 1.13E0_WP
      REAL (WP), PARAMETER  ::  B = 0.202E0_WP
!
      REAL (WP)             ::  LOG
!
      AA = TWO * (ONE - LOG(TWO)) / PI2                             !
      BB =   0.0184E0_WP                                            !
      CC = - 0.0938E0_WP                                            !
      DD =   0.020E0_WP                                             !
!
      EC_EH_W = ( AA + (BB * RS)/(ONE + B * RS) ) *               & !
                 LOG( RS / (ONE + A * RS) )                   +   & ! ref. 1 eq. (10)
                 CC / ( ONE + (ONE / CC) * (DD - AA * A) * RS )     !
!
      END FUNCTION EC_EH_W
!
!=======================================================================
!
      FUNCTION EC_HL_W(RS)
!
!  Hedin and Lundqvist correlation energy
!
!
!  Reference: L. Hedin and B. I. Lundqvist, 
!                J. Phys. C : Solid St. Phys. 4, 2065-2084 (1971)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!
!  Output parameters:
!
!       * EC_HL_W  : correlation energy (in Ry)
!
!
!
!   Author :  D. Sébilleau
!
!
!                                           Last modified : 14 Dec 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,FOUR,HALF,THIRD
      USE PI_ETC,           ONLY : PI,PI_INV
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  EC_HL_W
      REAL (WP)             ::  A,C,X,X2,X3,X_INV
!
      REAL (WP)             ::  LOG
!
      A = 21.0E0_WP                                                 !
      C =  0.045E0_WP                                               !
!
      X     = RS / A                                                ! ref. 1 eq. (3.5)
      X2    = X * X                                                 !
      X3    = X2 * X                                                !
      X_INV = ONE / X                                               !
!
      EC_HL_W = - C * ( (ONE + X3) * LOG(ONE + X_INV) +           & ! ref. 1 eq. (3.8)
                          HALF * X - X2 - THIRD                   & !
                        )                                           !
!
      END FUNCTION EC_HL_W
!
!=======================================================================
!
      FUNCTION EC_BH_W(RS,IMODE)
!
!  von Barth and Hedin correlation energy
!
!
!  Reference: U. von Barth and L. Hedin, J. Phys. C: Solid State Phys. 5,
!                1629-1642 (1972)
!
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * IMODE    : Choice of parameters
!                       IMODE = 1 : paramagnetic  state
!                       IMODE = 2 : ferromagnetic state
!
!
!  Output parameters:
!
!       * EC_BH_W  : correlation energy (in Ry)
!
!
!
!   Author :  D. Sébilleau
!
!
!                                           Last modified : 11 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,FOUR,HALF,THIRD
      USE CUBE_ROOTS,       ONLY : CUB2
!
      IMPLICIT NONE
!
      INTEGER               ::  IMODE
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  EC_BH_W
      REAL (WP)             ::  ECP,ECF,NUC
      REAL (WP)             ::  X,XP,XF
      REAL (WP)             ::  FP,FF,FX
!
      REAL (WP), PARAMETER  ::  CP    =  0.0504E0_WP
      REAL (WP), PARAMETER  ::  CF    =  0.0254E0_WP
      REAL (WP), PARAMETER  ::  RP    = 30.0E0_WP
      REAL (WP), PARAMETER  ::  RF    = 75.0E0_WP
      REAL (WP), PARAMETER  ::  A     = ONE / CUB2
      REAL (WP), PARAMETER  ::  GAMMA = FOUR * THIRD * A / (ONE - A)
!
      REAL (WP)             ::  LOG
!
      IF(IMODE == 1) THEN                                           !
        X = HALF                                                    !
      ELSE IF(IMODE == 2) THEN                                      !
        X = ZERO                                                    !
      END IF                                                        !
!
      FX = ONE / (ONE - A) * ( X**(FOUR*THIRD) +                  & !
                              (ONE - X)**(FOUR*THIRD) - A         & ! ref. 1 eq. (5.3) 
                             )                                      !
!
      XP = RS / RP                                                  !
      XF = RS / RF                                                  !
!
      FP = (ONE + XP * XP * XP) * LOG(ONE + ONE / XP) +           & !
           HALF * XP - XP * XP - THIRD                              ! ref. 1 eq. (5.11)
      FF = (ONE + XF * XF * XF) * LOG(ONE + ONE / XF) +           & !
           HALF * XF - XF * XF - THIRD                              !
!
      ECP = - CP * FP                                               ! ref. 1 eq. (5.10)
      ECF = - CF * FF                                               !
!
      NUC = GAMMA * (ECF - ECP)                                     ! ref. 1 eq. (5.6)
!
      EC_BH_W = ECP + NUC * FX / GAMMA                              ! ref. 1 eq. (5.5) 
!
      END FUNCTION EC_BH_W
!
!=======================================================================
!
      FUNCTION EC_PZ_W(RS,IMODE)
!
!  Perdew and Zunger correlation energy
!
!
!  Reference: J. P. Perdew and A. Zunger,  Phys. Rev. B 23, 
!                5048- 5079 (1981)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * IMODE    : Choice of parameters
!                       IMODE = 1 : no spin polarization 
!                       IMODE = 2 : fully spin-polarized
!
!
!  Output parameters:
!
!       * EC_PZ_W  : correlation energy (in Ry)
!
!
!  Note : final result multiplied by two as the energy values 
!         are given in Hartree
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
      USE REAL_NUMBERS,     ONLY : TWO,THIRD
      USE CONSTANTS_P1,     ONLY : BOHR
!
      IMPLICIT NONE
!
      INTEGER               ::  IMODE
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  EC_PZ_W
      REAL (WP)             ::  A,B,C,D
      REAL (WP)             ::  BB,CC,DD
!
      REAL (WP)             ::  LOG
!
      IF(IMODE == 1) THEN                                           !
        A =   0.0311E0_WP                                           !
        B = - 0.0480E0_WP                                           ! 
        C =   0.0020E0_WP                                           ! ref. 1 table XII
        D = - 0.0116E0_WP                                           !
      ELSE IF(IMODE == 2) THEN                                      !
        A =   0.01555E0_WP                                          !
        B = - 0.0269E0_WP                                           !
        C =   0.0007E0_WP                                           ! ref. 1 table XII
        D = - 0.0048E0_WP                                           !
      END IF                                                        !
!
      BB = B - THIRD * A                                            !
      CC = TWO * THIRD * C                                          !
      DD = THIRD * (TWO * D - C)                                    !
!
      EC_PZ_W = TWO * (A * LOG(RS) + BB + CC * RS * LOG(RS) +     & ! ref. 1 eq. (C5)
                       DD * RS)                                     !
!
      END FUNCTION EC_PZ_W
!
!=======================================================================
!
!  (2) Strong coupling regime (r_s small): _S
!
!=======================================================================
!
      FUNCTION EC_W_S(RS)
!
!  Wigner correlation energy
!
!
!  Reference: E. P. Wigner, Phys. Rev. 46, 1002-1011 (1934)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!
!  Output parameters:
!
!       * EC_W_S   : correlation energy (in Ry)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Sep 2020
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  EC_W_S
      REAL (WP)             ::  A,B
!
      A = - 0.88E0_WP                                               !
      B =   7.8E0_WP                                                !
!
      EC_W_S = A / (RS + B)                                         !
!
      END FUNCTION EC_W_S
!
!=======================================================================
!
      FUNCTION EC_NP_S(RS)
!
!  Nozières and Pines correlation energy
!
!
!  Reference: G. D. Mahan, "Many-Particle Physics", 2nd edition, 
!                Plenum Press 1990
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  EC_NP_S
      REAL (WP)             ::  A,B
!
      REAL (WP)             ::  LOG
!
      A = - 0.115E0_WP                                              !
      B =   0.031E0_WP                                              !
!
      EC_NP_S = A + B * LOG(RS)                                     ! ref. 1 p. 409
!
      END FUNCTION EC_NP_S
!
!=======================================================================
!
      FUNCTION EC_LR_S(RS)
!
!  Lindgren and Rosen correlation energy
!
!
!  Reference: G. D. Mahan, "Many-Particle Physics", 2nd edition, 
!                Plenum Press 1990
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,THREE,FOUR
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  EC_LR_S
      REAL (WP)             ::  A,B,C,D
!
      REAL (WP)             ::  SQRT
!
      A =   ONE                                                     !
      B =   THREE                                                   !
      C =   FOUR                                                    !
      D = - 0.08E0_WP                                               !
!
      EC_LR_S = - ONE / (A * RS + B  + C * SQRT(RS) + D / SQRT(RS)) ! ref. 1 p. 410
!
      END FUNCTION EC_LR_S
!
!=======================================================================
!
      FUNCTION EC_PZ_S(RS,IMODE)
!
!  Perdew and Zunger correlation energy
!
!
!  Reference: J. P. Perdew and A. Zunger,  Phys. Rev. B 23, 
!                5048- 5079 (1981)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * IMODE    : Choice of parameters
!                       IMODE = 1 : no spin polarization 
!                       IMODE = 2 : fully spin-polarized
!                       IMODE = 3 : Emfietzoglou et al values
!
!
!  Output parameters:
!
!       * EC_PZ_S  : correlation energy (in Ry)
!
!
!  Note : final result multiplied by two as the energy values 
!         are given in Hartree
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO
!
      IMPLICIT NONE
!
      INTEGER               ::  IMODE
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  EC_PZ_S
      REAL (WP)             ::  A,B,C,D
!
      REAL (WP)             ::  SQRT
!
      IF(IMODE == 1) THEN                                           !
        A = - 0.1423E0_WP                                           !
        B =   ONE                                                   !
        C =   1.0529E0_WP                                           !
        D =   0.3334E0_WP                                           !
      ELSE IF(IMODE == 2) THEN                                      !
        A = - 0.0843E0_WP                                           !
        B =   ONE                                                   !
        C =   1.3981E0_WP                                           !
        D =   0.2611E0_WP                                           !
      ELSE IF(IMODE == 3) THEN                                      !
        A = - 0.103756E0_WP                                         !
        B =   ONE                                                   !
        C =   0.56371E0_WP                                          !
        D =   0.27358E0_WP                                          !
      END IF                                                        !
!
      EC_PZ_S = TWO * ( A / (B + C * SQRT(RS) + D * RS) )           !
!
      END FUNCTION EC_PZ_S
!
!=======================================================================
!
      FUNCTION EC_RH_S(RS)
!
!  Rebei and Hitchon correlation energy
!
!
!  Reference:  A. Rebei and W. N. G. Hitchon, Phys. Lett. A 196, 
!                           295-299 (1994)
!
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!
!  The formula is:
!
!                
!                      / 1
!                3    |      2       (        4 g(x)     )
!    Ec  =  - ------  |     x  * Log ( 1 + ------------- )  dx
!              4 pi   |              (      alpha * r_s  )
!                    / 0
!                
!
!        with :
!                                    1/3
!                          (   4    )
!               * alpha =  ( ------ )
!                          (  9 pi  )
!
!                                    2
!                               1 - x        (  1 + x  )
!               * g(x)  =  1 + ------- * Log ( ------- )
!                                 x          (  1 - x  )
!
!
!  Note :  * for x = 1, we have (1 - x) * Log(1 - x) = 0 so that
!
!                      g(1) = 1
!
!          * for x = 0, we have g(0) = 3
!
!
!  Warning : we have added a factor a_0 in the final result as eq. (16) 
!            does not give the results of table 1 ...
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 10 Sep 2020
!
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,THREE,FOUR,HALF
      USE PI_ETC,           ONLY : PI
      USE UTILITIES_1,      ONLY : ALFA
      USE INTEGRATION,      ONLY : INTEGR_L
      USE ENE_CHANGE,       ONLY : BOHR2A
!
      IMPLICIT NONE
!
      INTEGER               ::  J
!
      INTEGER, PARAMETER    ::  N_GRID = 100
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  EC_RH_S
      REAL (WP)             ::  ALPHA,COEF
      REAL (WP)             ::  X,H,A
      REAL (WP)             ::  F(N_GRID),G(N_GRID)
!
      REAL (WP)             ::  FLOAT,LOG
!
      ALPHA = ALFA('3D')                                            !
      COEF  = - THREE / (FOUR * PI)                                 !
      H     =   ONE / FLOAT(N_GRID - 1)                             !  step
!
!  Defining the fonction to integrate over a grid
!
      DO J = 1,N_GRID                                               !
!
        X = FLOAT(J-1) * H                                          !
!
        IF(J == 1) THEN                                             !
          G(J) = THREE                                              !
        ELSE IF(J > 1 .AND. J < N_GRID) THEN                        !
          G(J) = ONE +    (ONE -  X * X) *                        & !
                          LOG((ONE + X) / (ONE - X)) / X            !
        ELSE IF(J == N_GRID) THEN                                   !
          G(J) = ONE                                                !
        END IF                                                      !
!
        F(J) = X * X * LOG(ONE + (FOUR * G(J)) / (ALPHA * RS))      !
!
      END DO                                                        !
!
      CALL INTEGR_L(F,H,N_GRID,N_GRID,A,1)                          !
!
      EC_RH_S = COEF * A  * BOHR2A                                  !
!
      END FUNCTION EC_RH_S
!
!=======================================================================
!
!  (3) General coupling regime: _G
!
!=======================================================================
!
      FUNCTION EC_GG_G(RS)
!
!   Gori-Giorgi, Sacchetti and Bachelet correlation energy
!
!  Reference : P. Gori-Giorgi, F. Sacchetti and G. B. Bachelet,
!                 Phys. Rev. B 61, 7353-7363 (2000)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!
!  Note: the extra factor 2 in the definition comes from the fact 
!        that the energies here are expressed in Hartree
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 20 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,FIVE,EIGHT,HALF
      USE PI_ETC,           ONLY : PI2
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)  ::  RS
      REAL (WP)              ::  EC_GG_G
      REAL (WP)              ::  A1,A2
      REAL (WP)              ::  B1,B2,B3,B4,B5,B6
      REAL (WP)              ::  XS,RS2
      REAL (WP)              ::  NUM,DEN
!
      REAL (WP)              ::  LOG,SQRT,EXP
!
      REAL (WP), PARAMETER   ::  A = (ONE - LOG(TWO)) / PI2
      REAL (WP), PARAMETER   ::  B = - 0.0469205E0_WP
      REAL (WP), PARAMETER   ::  C =   0.0092292E0_WP
      REAL (WP), PARAMETER   ::  D = - 0.01E0_WP
!
      XS  = SQRT(RS)                                                !
      RS2 = RS * RS                                                 !
!
      A1 = C / A                                                    !
      A2 = FIVE                                                     !
!
      B1 = HALF * EXP(HALF * B / A) / A                             !
      B2 = TWO * A * B1 * B1                                        !
      B3 = HALF * B1 * ( EIGHT * B1 * B1 * A * A * A * A -        & !
                         C * B + D * A ) / (A * A)                  !
      B4 = 45.0E0_WP                                                !
      B5 = 32.0E0_WP                                                !
      B6 = 12.7E0_WP                                                !
!
      NUM = - TWO * A * (ONE + A1 * RS + A2 * RS2)                  !
      DEN =   TWO * A * ( B1 * XS + B2 * RS  + B3 *  RS * XS +    & !
                                    B4 * RS2 + B5 * RS2 * XS +    & !
                                    B6 * RS2 * RS )                 !
!
      EC_GG_G = TWO * NUM * LOG(ONE + ONE / DEN)                    ! ref. (1) eq. (B1)
!
      END FUNCTION EC_GG_G
!
!=======================================================================
!
      FUNCTION EC_PK_G(RS,IMODE,XI)
!
!  Proynov and Kong correlation energy 
!
!
!  Reference: (1) E. Proynov and J. Kong,  Phys. Rev. A 79, 014103 (2009)
!             (2) E. Proynov and J. Kong,  Phys. Rev. A 95, 059904(E) (2017)
!             (3) E. Proynov, J. Mol. Struc.: THEOCHEM 139–145 (2006)
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * IMODE    : Choice of parameters
!                       IMODE = 1 : no spin polarization 
!                       IMODE = 2 : fully spin-polarized
!       * XI       : spin polarization 
!
!
!  Warning : Only non-polarized case implemented at present, 
!            i.e. XI = 0 so that NP = NM = N0/2
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 14 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,FOUR,HALF,THIRD
      USE PI_ETC,           ONLY : PI,PI2
!
      IMPLICIT NONE
!
      INTEGER                ::  IMODE
!
      REAL (WP), INTENT(IN)  ::  RS,XI
      REAL (WP)              ::  EC_PK_G
      REAL (WP)              ::  XS
      REAL (WP)              ::  N0,NP,NM
      REAL (WP)              ::  ALPHA_N,ALPHA_XI,ALPHA_EFF
      REAL (WP)              ::  FR,FS,P1,P2,P3,P4,SRX
      REAL (WP)              ::  KF,KS
      REAL (WP)              ::  D1,D2,D3,D4,D5,D6,D7,D8
      REAL (WP)              ::  Q1,Q2,Q3
      REAL (WP)              ::  S1
      REAL (WP)              ::  ECP,ECM
!
      REAL (WP), PARAMETER   ::  AX = (THREE * PI2)**THIRD    ! ref. 2 (ii)
!
      REAL (WP)              ::  A(18),C(29),ETA(10)
      REAL (WP)              ::  AA(0:5),BB(0:4),CC(0:4),DD(0:3)
!
      REAL (WP)              ::  EXP,SQRT,ATAN,LOG
!
      DATA A    /   0.184630439485191E0_WP,    &   ! a_1
                    5.939656549519008E0_WP,    &   ! a_2
                    2.369580128666418E0_WP,    &   ! a_3
                    0.051188865525959E0_WP,    &   ! a_4
                    0.095768925320043E0_WP,    &   ! a_5
                    0.028359261614488E0_WP,    &   ! a_6
                    0.022627416997970E0_WP,    &   ! a_7
                    0.005317361552717E0_WP,    &   ! a_8
                    0.191537850640085E0_WP,    &   ! a_9      ref. 1 table III
                    0.147313777119493E0_WP,    &   ! a_10
                    0.152825093835090E0_WP,    &   ! a_11
                    1.015083075438391E0_WP,    &   ! a_12
                    0.076412546917545E0_WP,    &   ! a_13
                    0.898537460263473E0_WP,    &   ! a_14
                    0.017956673497508E0_WP,    &   ! a_15
                    0.034618207403477E0_WP,    &   ! a_16
                    0.035913346995016E0_WP,    &   ! a_17
                    0.222017353476156E0_WP/        ! a_17
!
      DATA C    / 132.479090287794E0_WP   ,    &   ! c_1
                   32.4014708516771E0_WP  ,    &   ! c_2
                   22.5664453162504E0_WP  ,    &   ! c_3
                   11.2832226581252E0_WP  ,    &   ! c_4
                    0.40106052394096E0_WP ,    &   ! c_5
                    0.32000000000000E0_WP ,    &   ! c_6
                    0.07519884823893E0_WP ,    &   ! c_7
                  116.935042647481E0_WP   ,    &   ! c_8
                   29.6240023046901E0_WP  ,    &   ! c_9
                    0.48225718199447E0_WP ,    &   ! c_10
                    0.24690398117910E0_WP ,    &   ! c_11
                    0.50000000000000E0_WP ,    &   ! c_12
                    0.41070969677819E0_WP ,    &   ! c_13
                    0.10532352447677E0_WP ,    &   ! c_14
                   14.5650971711660E0_WP  ,    &   ! c_15      ref. 1 table III
                    0.78125000000000E0_WP ,    &   ! c_16
                    0.62334731312724E0_WP ,    &   ! c_17
                    0.14648437500000E0_WP ,    &   ! c_18
                  111.8115481057978E0_WP  ,    &   ! c_19
                    0.160041105570901E0_WP,    &   ! c_20
                    0.781250000000000E0_WP,    &   ! c_21
                    0.320866950607957E0_WP,    &   ! c_22
                   13.28444950729984E0_WP ,    &   ! c_23
                    0.268418671319107E0_WP,    &   ! c_24
                    0.471060597934992E0_WP,    &   ! c_25
                    0.250000000000000E0_WP,    &   ! c_26
                    0.252882919616990E0_WP,    &   ! c_27
                    0.072048583112715E0_WP,    &   ! c_28
                   42.64905448910311E0_WP /        ! c_29 
!
      DATA ETA  /   0.538074483500437E0_WP,    &   ! eta_1
                   -2.226094990985190E0_WP,    &   ! eta_2
                    0.837303782322808E0_WP,    &   ! eta_3
                    2.619709858963178E0_WP,    &   ! eta_4
                    1.036657594643520E0_WP,    &   ! eta_5    in text of appendix
                    0.41081146652128E0_WP ,    &   ! eta_6
                    0.599343256903515E0_WP,    &   ! eta_7
                    1.70939476802168E0_WP ,    &   ! eta_8
                    0.077123208419481E0_WP,    &   ! eta_9
                    0.46958449007619E0_WP /        ! eta_10
!
      DATA AA   /-113.693369789727190E0_WP  ,  &   ! a_0
                   24.00502151278711440E0_WP,  &   ! a_1
                   49.34131295839670750E0_WP,  &   ! a_2      in text of appendix
                  -23.8242372168379302E0_WP ,  &   ! a_3        for eq. (24)-(25)
                    0.944080741695104794E0_WP, &   ! a_4
                    0.000293039144178338E0_WP/     ! a_5
!
      DATA BB   /-109.74263493216910E0_WP   ,  &   ! b_0
                   16.2663129444242415E0_WP ,  &   ! b_1      in text of appendix
                   54.4034331373908366E0_WP ,  &   ! b_2        for eq. (24)-(25)
                  -25.154009904187990E0_WP  ,  &   ! b_3
                    1.0000000000000000E0_WP /      ! b_4
!
      DATA CC   /  -0.32481568604919886E0_WP , &   ! c_0
                    1.180131465463191050E0_WP, &   ! c_1      in text of appendix
                   -1.42693041498421640E0_WP , &   ! c_2        for eq. (24)-(25)
                    0.580344063812247980E0_WP, &   ! c_3
                   -0.01099122367291440E0_WP /     ! c_4
!
      DATA DD   /  -0.57786103193239430E0_WP , &   ! d_0
                    2.09708505883490736E0_WP , &   ! d_1      in text of appendix
                   -2.52188183586948180E0_WP , &   ! d_2        for eq. (24)-(25)
                    1.00000000000000000E0_WP /     ! D_3
!
      XS = RS**THIRD                                                ! 
!
      S1 = 1.28E0_WP                                                !
!
!  Spin-polarized densities
!
      N0 = FOUR * PI * THIRD * RS * RS * RS                         !  N0 in a. u.
      NP = HALF * N0 * (ONE + XI)                                   ! ref. 1 eq. (16)
      NM = HALF * N0 * (ONE - XI)                                   ! 
!
!  Screened Fermi wave vector calculation (eq. (17))
!
      P1 = AA(0) + AA(1) * RS + AA(2) * RS * RS +                 & !
           AA(3) * RS * RS * RS + AA(4) * RS * RS * RS * RS +     & ! ref. 1 eq. (24)
           AA(5) * RS * RS * RS * RS * RS                           !
      P2 = BB(0) + BB(1) * RS + BB(2) * RS * RS +                 & ! ref. 1 eq. (24)
           BB(3) * RS * RS * RS + BB(4) * RS * RS * RS * RS         !
      P3 = CC(0) + CC(1) * XI + CC(2) * XI * XI +                 & ! ref. 1 eq. (25)
           CC(3) * XI * XI * XI + CC(4) * XI * XI * XI * XI         !
      P4 = DD(0) + DD(1) * XI + DD(2) * XI * XI +                 & ! ref. 1 eq. (25)
           DD(3) * XI * XI * XI                                     !
!
      FR = P1 / P2                                                  ! ref. 1 eq. (24)
      FS = P3 / P4                                                  ! ref. 1 eq. (25)
!
      SRX = S1 * FR * FS                                            ! ref. 2 eq. (23)
!
      ALPHA_XI = TWO / ( (ONE + XI)**SRX + (ONE - XI)**SRX )        ! ref. 1 eq. (22)
!
      ALPHA_N  = ETA(6) + ETA(7) * EXP(- ETA(8) * XS) * XS * XS + & !
                 ETA(9) * EXP(- ETA(10) * XS) * XS                  ! ref. 1 eq. (21)
!
      ALPHA_EFF = ALPHA_N * ALPHA_XI                                ! ref. 1 eq. (20)
!
      KF = AX * (N0 * (ONE + XI))**THIRD                            ! ref. 1 eq. (15) 
!
      KS = ALPHA_EFF * KF                                           ! ref. 1 eq. (17)
!
!  Calculation of the Dn polynomials
!
      D1 =  A(6) * KS * KS +  A(7) * KS +  A(8)                     !
      D2 =  A(1) * KS * KS + A(10) * KS + A(16)                     !
      D3 =  A(5) * KS * KS + A(13) * KS + A(15)                     !
      D4 =  A(9) * KS * KS + A(11) * KS + A(17)                     !
      D5 =  C(5) * KS * KS +  C(6) * KS +  C(7)                     !
      D6 = C(12) * KS * KS + C(13) * KS + C(14)                     !
      D7 = C(16) * KS * KS + C(17) * KS + C(18)                     !
      D8 = SQRT( C(26) * KS * KS + C(27) * KS + C(28) )             !
!
!  Computation of the Qn functions
!
      Q1 = ( - ATAN( A(2) * KS + A(3) ) * D2 / KS            -    & !
             D3 * LOG(D1) / KS + D4 * LOG(KS) / KS           -    & !  ref. 1 eq. (8)
             A(4) * KS + A(12) + A(14) / KS + A(18) / (KS * KS)   & !
           ) / D1                                                   !
!
      Q2 = - C(1) / KS - C(2) / (KS * KS) - C(3) * LOG(KS) / KS + & !
           C(4) * LOG(D5) / KS                  +                 & !
           C(8) * ATAN( A(2) * KS + A(3) ) / KS +                 & !  ref. 1 eq. (9)
           C(9) * LOG( KS + C(10) ) / KS        -                 & !
           C(11) * LOG(D6) / KS                                     !
!
      Q3 = C(19) * ATAN( C(20) / ( C(21) * KS + C(22) ) ) / KS  - & !
           C(23) * ATAN( (C(24) + C(25) * KS) / D8 ) / KS       - & !  ref. 1 eq. (10)
           C(15) * LOG(D7) / KS - C(29) * D8 / (KS * KS)            ! 
!
      ECP = HALF * NP * NP * (Q1 + Q2 + Q3) / N0                    !  ref. 1 eq. (14)
      ECM = HALF * NM * NM * (Q1 + Q2 + Q3) / N0                    !
!
!  Correlation energy (Ry)
!
      EC_PK_G = TWO * (ECP + ECM)                                   !  ref. 1 eq. (13)
!
      END FUNCTION EC_PK_G
!
!=======================================================================
!
!
      FUNCTION EC_VWN_G(RS,IMODE)
!
!  Vosko, Wilk and Nusair correlation energy 
!
!
!  Reference: S. H. Vosko, L. Wilk and M. Nusair, 
!                Can J . Phys. 58, 1200-1211 (1980)
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * IMODE    : Choice of parameters
!                       IMODE = 1 : no spin polarization 
!                       IMODE = 2 : fully spin-polarized
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : TWO,FOUR
!
      IMPLICIT NONE
!
      INTEGER               ::  IMODE
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  EC_VWN_G
      REAL (WP)             ::  X,X0,XX,XX0,Q
      REAL (WP)             ::  Q2XB
      REAL (WP)             ::  A,B,C
!
      REAL (WP)             ::  SQRT,LOG,ATAN
!
      IF(IMODE == 1) THEN                                           !
        X0 = - 0.409286E0_WP                                        !      
        A  =   0.0621814E0_WP / 1.32139E0_WP                     !
!        A  =   0.0621814E0_WP                    !
        B  =  13.0720E0_WP                                          !
        C  =  42.7198E0_WP                                          !
      ELSE IF(IMODE == 2) THEN                                      !
        X0 = - 0.743294E0_WP                                        !
        A  =   0.0310907E0_WP                                       !
        B  =  20.1231E0_WP                                          !
        C  = 101.578E0_WP                                           !
      END IF                                                        !
!
      X = SQRT(RS)                                                  !
!
      XX  = X  * X  + B * X  + C                                    !
      XX0 = X0 * X0 + B * X0 + C                                    !
      Q   = SQRT(FOUR * C - B * B)                                  !
!
      Q2XB = Q / (TWO * X + B )                                     !
!
      EC_VWN_G = A * ( LOG(RS / XX) + TWO * B * ATAN(Q2XB) / Q -  & !
                       B * X0 * ( LOG((X - X0)**2 / XX)        +  & !
                                  TWO * (B + TWO * X0) *          & ! ref. 1 eq. (4.4)
                                  ATAN(Q2XB) / Q                  & !
                                ) / XX0                           & !
                     )                                              !
!
      END FUNCTION EC_VWN_G
!
!=======================================================================
!
      FUNCTION EC_PW_G(RS,IMODE)
!
!  Perdew and Wang correlation energy 
!
!
!  Reference: J. P. Perdew and Y. Wang, Phys. Rev. B 45, 
!                13244-13249 (1992)
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * IMODE    : Choice of parameters
!                       IMODE = 1 : no spin polarization 
!                       IMODE = 2 : fully spin-polarized
!
!
!  Note: the extra factor 2 in the definition comes from the fact 
!        that the energies here are expressed in Hartree
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,FOUR
!
      IMPLICIT NONE
!
      INTEGER               ::  IMODE
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  EC_PW_G
      REAL (WP)             ::  A,A1,B1,B2,B3,B4
      REAL (WP)             ::  U,Z
!
      REAL (WP)             ::  SQRT,LOG
!
      IF(IMODE == 1) THEN                                           !
        A  = 0.0310907E0_WP                                         !
        A1 = 0.21370E0_WP                                           !
        B1 = 7.5957E0_WP                                            !
        B2 = 3.5876E0_WP                                            ! ref. 1 table I
        B3 = 1.6382E0_WP                                            !
        B4 = 0.49294E0_WP                                           !
      ELSE IF(IMODE == 2) THEN                                      !
        A  =  0.015545E0_WP                                         !
        A1 =  0.20548E0_WP                                          !
        B1 = 14.1189E0_WP                                           !
        B2 =  6.1977E0_WP                                           !
        B3 =  3.3662E0_WP                                           !
        B4 =  0.62517E0_WP                                          !
      END IF                                                        !
!
      U = SQRT(RS)                                                  !
      Z = TWO * A * (B1 * U + B2 * RS + B3 * RS * U + B4 * RS * RS) !
!
      EC_PW_G = - FOUR * A * (ONE + A1 * RS) * LOG(ONE + ONE / Z)   ! ref. 1 eq. (10)
!
      END FUNCTION EC_PW_G
!
!=======================================================================
!
      FUNCTION EC_HU_G(RS)
!
!  Hubbard correlation energy
!
!  Reference : J. Hubbard, Proc. Roy. Soc. A 243, 336-352 (1958)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Sep 2020
!
!
!
      USE DIMENSION_CODE,   ONLY : NZ_MAX
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,TWO,THREE,HALF,FOURTH
      USE PI_ETC,           ONLY : PI,PI_INV
      USE FERMI_AU,         ONLY : EF_AU
      USE UTILITIES_1,      ONLY : ALFA
      USE INTEGRATION,      ONLY : INTEGR_L
!
      IMPLICIT NONE
!
      INTEGER               ::  IX,IY
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  EC_HU_G
      REAL (WP)             ::  XI,HX,HY
      REAL (WP)             ::  X,X3,Y
      REAL (WP)             ::  A(NZ_MAX,NZ_MAX)
      REAL (WP)             ::  SIGMA(NZ_MAX,NZ_MAX)
      REAL (WP)             ::  F1(NZ_MAX),F2(NZ_MAX)
      REAL (WP)             ::  INT_1,INT_2
      REAL (WP)             ::  NUM1,NUM2,DEN1,DEN2,Z1,Z2
!
      REAL (WP), PARAMETER  ::  MX = 5.0E0_WP                       ! upper integration
      REAL (WP), PARAMETER  ::  MY = 5.0E0_WP                       !  bounds in x and y
!
      REAL (WP), PARAMETER  ::  SM = 1.0E-8_WP                      ! starting grid value
!
      REAL (WP)             ::  FLOAT,LOG,ABS
!
      XI = TWO * ALFA('3D') * PI_INV * RS                           ! ref. 1 eq. (28)
!
      HX = MX / FLOAT(NZ_MAX - 1)                                   ! x-step
      HY = MY / FLOAT(NZ_MAX - 1)                                   ! y-step
!
!  Construction the functions A and Sigma
!
      DO IX = 1, NZ_MAX                                             !
!
        X  = SM + FLOAT(IX - 1) * HX                                !
        X3 = X * X * X                                              !
!
        DO IY = 1, NZ_MAX                                           !
!
          Y = SM + FLOAT(IY - 1) * HY                               !
!
!  Calculation of Sigma(x,y)                                        ! ref. 1 eq. (26)
!
          IF(Y > X * (X + TWO)) THEN                                !
            SIGMA(IX,IY) = ZERO                                     !
          ELSE IF(X > TWO .AND. Y < X * (X - TWO)) THEN             !
            SIGMA(IX,IY) = ZERO                                     !
          ELSE IF( X > TWO .AND. X * (X - TWO) < Y .AND.          & !
                                 Y < X * (X + TWO)                & !
                                 .OR.                             & !
                  X < TWO .AND. X * (TWO - X) < Y .AND.           & !
                                Y < X * (X + TWO)                 & !
                 ) THEN                                             !
            SIGMA(IX,IY) = - PI * XI * HALF * ( ONE - FOURTH *    & !
                                                (Y / X - X)**2    & !
                                              ) / X3                !
          ELSE IF(X < TWO .AND. ZERO < Y .AND.                    & !
                  Y < X * (TWO - X)) THEN                           !
            SIGMA(IX,IY) = - PI * XI * Y * HALF / X3                !
          END IF                                                    !
!
!  Calculation of A(x,y)                                            ! ref. 1 eq. (27)
!
          NUM1 = Y - X * (X + TWO)                                  ! 
          NUM2 = Y + X * (X + TWO)                                  ! 
          DEN1 = Y - X * (X - TWO)                                  ! 
          DEN2 = Y + X * (X - TWO)                                  ! 
!
          Z1 = (Y / X - X)**2                                       ! 
          Z2 = (Y / X + X)**2                                       ! 
!
          A(IX,IY) = - XI * ( X +                                 & !
                              HALF * (ONE - FOURTH * Z1) *        & !
                              LOG(ABS(NUM1 / DEN1)) +             & !
                              HALF * (ONE - FOURTH * Z2) *        & !
                              LOG(ABS(NUM2 / DEN2))               & !
                            ) / X3                                  !
!
!  y-integrand
!
          F2(IY) = ATAN( SIGMA(IX,IY) / (ONE - A(IX,IY)) ) -      & !
                   SIGMA(IX,IY)                                     !
!
        END DO                                                      !
!
!  Computing the integral over  y
!
        CALL INTEGR_L(F2,HY,NZ_MAX,NZ_MAX,INT_2,1)                  !
!
!  x-integrand
!
        F1(IX) = X * X * INT_2                                      !
!
        
      END DO                                                        !
!
!  Computing the integral over  x
!
      CALL INTEGR_L(F1,HX,NZ_MAX,NZ_MAX,INT_1,1)                    !
!
      EC_HU_G = - THREE * FOURTH * PI_INV * EF_AU * INT_1           !
!
      END FUNCTION EC_HU_G
!
!=======================================================================
!
      FUNCTION EC_CH_G(RS,IMODE)
!
!  Chachiyo correlation energy
!
!  Reference : T. Chachiyo, J. Chem. Phys. 145, 021101 (2016)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * IMODE    : Choice of parameters
!                       IMODE = 1 : no spin polarization 
!                       IMODE = 2 : fully spin-polarized
!
!  Note: the extra factor 2 in the definition comes from the fact 
!        that the energies here are expressed in Hartree
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,FOUR 
      USE PI_ETC,           ONLY : PI2
!
      IMPLICIT NONE
!
      INTEGER               ::  IMODE
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  EC_CH_G
      REAL (WP)             ::  A,B
!
      REAL (WP)             ::  LOG
!
      IF(IMODE == 1) THEN                                           !
        A = (LOG(TWO) - ONE) / (TWO * PI2)                          !
        B = 20.4562557E0_WP                                         !
      ELSE IF(IMODE == 2) THEN                                      !
        A = (LOG(TWO) - ONE) / (FOUR * PI2)                         !
        B = 27.4203609E0_WP                                         !
      END IF                                                        !
!
      EC_CH_G = TWO * A * LOG( ONE + B / RS + B / (RS * RS) )       ! ref. 1 eq. (1)
!
      END FUNCTION EC_CH_G
!
!=======================================================================
!
      FUNCTION EC_IK_T(RS,T)
!
!  Temperature-dependent correlation energy for 3D systems
!
!  References:  A. Isihara and D. Y. Kojima, Z. Physik B 21, 
!                  33-45 (1975)
!
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * T        : temperature (SI)     
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 11 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE
      USE CONSTANTS_P1,     ONLY : K_B
      USE FERMI_SI,         ONLY : EF_SI
      USE PI_ETC,           ONLY : PI2
      USE ENE_CHANGE,       ONLY : EV,RYD
      USE UTILITIES_1,      ONLY : ALFA
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS,T
      REAL (WP)             ::  EC_IK_T
      REAL (WP)             ::  EC_0,EC_T
      REAL (WP)             ::  A0,B0,AT,BT,CT,DT
      REAL (WP)             ::  ETA_0,P02,ALPHA,BETA
      REAL (WP)             ::  KF2
!
      REAL (WP)             ::  LOG
!      
      ALPHA = ALFA('3D')                                            !
      T = 0.0001E0_WP
!
      BETA  = ONE / (K_B * T)                                       ! in SI
      P02   = EF_SI                                                 ! in SI
      ETA_0 = BETA * P02                                            ! dimensionless
!
!
      A0 = - 0.08140E0_WP                                           !
      B0 =   0.06218E0_WP                                           ! ref. (1) eq. (3.7)
!
      AT =   1.42728E0_WP                                           !
      BT = - 0.15198E0_WP                                           ! ref. (1) eq. (3.7)
      CT = - 0.61594E0_WP                                           !
      DT = - 0.30396E0_WP                                           !
!
      EC_0 = A0 + B0 * LOG(RS)                                      !
      EC_T = PI2 * ( AT + BT * LOG(RS) + CT * LOG(ETA_0) +        & !
                     DT * LOG(ETA_0) * LOG(ETA_0)                 & ! ref. (1) eq. (3.7) 
                   ) / (12.0E0_WP * ETA_0**2)                       !
!
      EC_IK_T = EC_0 - EC_T                                         ! ref. (1) eq. (3.7)
!
      END FUNCTION EC_IK_T
!
!------ 2) 2D case --------------------------------------------
!
!
!=======================================================================
!
      FUNCTION EC_2D(EC_TYPE,RS,T)
!
!  This subroutine computes the 2D correlation energy EC
!    at a given value RS
!
!
!  Input parameters:
!
!       * EC_TYPE  : type of correlation energy functional
!                       EC_TYPE = 'TACE_G'   --> Tanatar-Ceperley
!                       EC_TYPE = 'CPPA_G'   --> Seidl-Perdew_Levy
!                       EC_TYPE = 'AMGB_G'   --> Attaccalite-Moroni-Gori-Giorgi-Bachelet
!                       EC_TYPE = 'SEID_G'   --> Seidl
!                       EC_TYPE = 'LOOS_W'   --> Loos
!                       EC_TYPE = 'WIGN_S'   --> Wigner
!                       EC_TYPE = 'ISTO_T'   --> Isihara-Toyoda
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * T        : temperature (SI)     
!
!  Output parameters:
!
!       * EC_2D    : value at RS
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 6)   ::  EC_TYPE
!
      REAL (WP)             ::  EC_2D,RS,T
!
      IF(EC_TYPE == 'TACE_G') THEN                                  !
        EC_2D = EC_TC_G(RS)                                         !
      ELSE IF(EC_TYPE == 'CPPA_G') THEN                             !
        EC_2D = EC_SP_G(RS)                                         !
      ELSE IF(EC_TYPE == 'AMGB_G') THEN                             !
        EC_2D = EC_MG_G(RS)                                         !
      ELSE IF(EC_TYPE == 'SEID_G') THEN                             !
        EC_2D = EC_SE_G(RS)                                         !
      ELSE IF(EC_TYPE == 'LOOS_W') THEN                             !
        EC_2D = EC_L2_W(RS)                                         !
      ELSE IF(EC_TYPE == 'WIGN_S') THEN                             !
        EC_2D = EC_W2_S(RS)                                         !
      ELSE IF(EC_TYPE == 'ISTO_T') THEN                             !
        EC_2D = EC_IT_T(RS,T)                                       !
      END IF                                                        !
!
      END FUNCTION EC_2D
!
!=======================================================================
!
      SUBROUTINE DERIVE_EC_2D(EC_TYPE,IDERIV,RS,T,D_EC_1,D_EC_2)
!
!  This subroutine computes the first and second derivative 
!    of the correlation energy E_c with repect to r_s
!    at a given value RS
!
!
!  Input parameters:
!
!       * EC_TYPE  : type of correlation energy functional
!                       EC_TYPE = 'TACE_G'   --> Tanatar-Ceperley
!                       EC_TYPE = 'CPPA_G'   --> Seidl-Perdew_Levy
!                       EC_TYPE = 'AMGB_G'   --> Attaccalite-Moroni-Gori-Giorgi-Bachelet
!                       EC_TYPE = 'SEID_G'   --> Seidl
!                       EC_TYPE = 'LOOS_W'   --> Loos
!                       EC_TYPE = 'WIGN_S'   --> Wigner
!                       EC_TYPE = 'ISTO_T'   --> Isihara-Toyoda
!       * IDERIV   : type of n_point formula used for derivation (n = IDERIV)
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * T        : temperature (SI)     
!
!  Output parameters:
!
!       * D_EC_1    : first derivative at RS
!       * D_EC_2    : second derivative at RS
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
!
      USE DIMENSION_CODE,   ONLY : ND_MAX
      USE DERIVATION  
      USE INTERPOLATION,    ONLY : INTERP_NR,SPLINE,SPLINT
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 6)   ::  EC_TYPE
!
      INTEGER               ::  IDERIV,I,LOGF
!
      REAL (WP)             ::  RS,RI,T
      REAL (WP)             ::  D_EC_1,D_EC_2
!
      REAL (WP)             ::  R(ND_MAX),EC(ND_MAX)
      REAL (WP)             ::  D_EC(ND_MAX),DD_EC(ND_MAX)
      REAL (WP)             ::  R_MIN,R_MAX,STEP
!
      REAL (WP)             ::  FLOAT
!
      R_MIN = 0.01E0_WP                                             !
      R_MAX = 50.01E0_WP                                            !
      STEP  = (R_MAX - R_MIN) / FLOAT(ND_MAX - 1)                   !
!
      LOGF  = 6                                                     !
!
!  Storing the correlation energy EC as a function of RS
!
      DO I = 1,ND_MAX                                               !
!
        R(I) = R_MIN + FLOAT(I - 1) * STEP                          !
        RI   = R(I)                                                 !
!
        IF(EC_TYPE == 'TACE_G') THEN                                !
          EC(I) = EC_TC_G(RI)                                       !
        ELSE IF(EC_TYPE == 'CPPA_G') THEN                           !
          EC(I) = EC_SP_G(RI)                                       !
        ELSE IF(EC_TYPE == 'AMGB_G') THEN                           !
          EC(I) = EC_MG_G(RI)                                       !
        ELSE IF(EC_TYPE == 'SEID_G') THEN                           !
          EC(I) = EC_SE_G(RI)                                       !
        ELSE IF(EC_TYPE == 'LOOS_W') THEN                           !
          EC(I) = EC_L2_W(RI)                                       !
        ELSE IF(EC_TYPE == 'WIGN_S') THEN                           !
          EC(I) = EC_W2_S(RI)                                       !
        ELSE IF(EC_TYPE == 'ISTO_T') THEN                           !
          EC(I) = EC_IT_T(RI,T)                                     !
        END IF                                                      !
!
      END DO                                                        !
!
!  Computing the first and second derivatives 
!    with a IDERIV-point formula
!
      CALL DERIV_1(EC,ND_MAX,IDERIV,STEP,D_EC)                      !
      CALL DERIV_1(D_EC,ND_MAX,IDERIV,STEP,DD_EC)                   !
!
!  Interpolation of derivatives at RS
!
      CALL INTERP_NR(LOGF,R,D_EC,ND_MAX,RS,D_EC_1)                  !
      CALL INTERP_NR(LOGF,R,DD_EC,ND_MAX,RS,D_EC_2)                 !
!
      END SUBROUTINE DERIVE_EC_2D
!
!=======================================================================
!
!             Exchange/Correlation energy functionals (in Ryd)
!
!  Different regimes:  * weak coupling          : r_s << 1
!                      * metallic state         : 2 <= r_s  <= 6
!                      * Wigner crystallization : r_s >= 100
!
!
!  (1) General coupling regime: _G
!
!=======================================================================
!
      FUNCTION EC_TC_G(RS)
!
!  Tanatar-Ceperley correlation energy for 2D systems
!
!  References: (1) B. Tanatar and D. M. Ceperley, Phys. Rev. B 39, 
!                     5005-5016 (1989)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS,X
      REAL (WP)             ::  A0,A1,A2,A3
      REAL (WP)             ::  NUM,DEN
      REAL (WP)             ::  EC_TC_G
!
      REAL (WP)             ::  SQRT
!
      A0 =- 0.3568E0_WP                                             !
      A1 =  1.1300E0_WP                                             ! ref. (1) table IV
      A2 =  0.9052E0_WP                                             !
      A3 =  0.4165E0_WP                                             !
!
      X = SQRT(RS)                                                  !
!
      NUM = ONE + A1 * X                                            !
      DEN = ONE + A1 * X + A2 * X * X + A3 * X * X * X              !
!
      EC_TC_G = A0 * NUM / DEN                                      ! ref. (1) eq. (14)
!
      END FUNCTION EC_TC_G
!
!=======================================================================
!
      FUNCTION EC_SP_G(RS)
!
!  Seidl-Perdew-Levy correlation energy for 2D systems
!
!  References: (1) L. A. Constantin, J. P. Perdew and J. M. Pitarke, 
!                  Phys. Rev. Lett. 101, 269902 (2008)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  A0,A1,A2
      REAL (WP)             ::  EC_SP_G
!
      REAL (WP)             ::  SQRT
!
      A0 = 0.5058E0_WP                                              !
      A1 = 1.3311E0_WP                                              !
      A2 = 1.5026E0_WP                                              !
!
      EC_SP_G = A0 * (A1 / (RS * RS) * (SQRT(ONE + A2 * RS) - ONE) - & !
                ONE / (RS * RS))                                       ! ref. (1) eq. (9)
!
      END FUNCTION EC_SP_G
!
!=======================================================================
!
      FUNCTION EC_MG_G(RS)
!
!  Attaccalite-Moroni-Gori-Giorgi-Bachelet correlation energy for 2D systems
!
!  References: (1) C. Attaccalite, S. Moroni, P. Gori-Giorgi and 
!                     G. B. Bachelet, Phys. Rev. Lett. 88, 256601 (2002)
!              (2) Erratum: Phys. Rev. Lett. 91, 109902 (2003)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS,R1,R2,R3,X3
      REAL (WP)             ::  A0,B0,C0,D0,E0,F0,G0,H0
      REAL (WP)             ::  EC_MG_G
!
      REAL (WP)             ::  SQRT,LOG
!
      X3 = SQRT(RS) * RS                                            !
      R1 = RS                                                       !
      R2 = RS * RS                                                  !
      R3 = R2 * RS                                                  !
!
      A0 = - 0.1925E0_WP                                            !
      B0 =   0.0863136E0_WP                                         !
      C0 =   0.0572384E0_WP                                         ! ref. (1)
      E0 =   1.0022E0_WP                                            !   and
      F0 = - 0.02069E0_WP                                           ! ref. (2)
      G0 =   0.33997E0_WP                                           ! table II
      H0 =   1.747E-02_WP                                           !
!
      D0 = - A0 * H0                                                !
!
      EC_MG_G = A0 + (B0 * R1 + C0 * R2 + D0 * R3) *              & ! ref. (1) eq. (3)
                      LOG(ONE + ONE / ( E0 * R1 + F0 * X3 +       & !
                                        G0 * R2 + H0 * R3 ))        ! and eq. (4)
!
      END FUNCTION EC_MG_G
!
!=======================================================================
!
      FUNCTION EC_SE_G(RS)
!
!  Seidl correlation energy for 2D systems, based on the 
!    interaction-strength interpolation (ISI)
!
!  References: (1) M. Seidl, Phys. Rev. B 70, 073101 (2004)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,FOUR,EIGHT,THIRD
      USE PI_ETC,           ONLY : PI_INV
      USE DIRICHLET
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  B0,CX,AI,X,Y,Z
      REAL (WP)             ::  EC_SE_G
!
      REAL (WP)             ::  SQRT,LOG
!
      B0 =   LOG(TWO) + DB(2) - EIGHT * DB(4) * PI_INV * PI_INV     !
      CX = - FOUR * SQRT(TWO) * THIRD * PI_INV                      !
      AI = - 1.1061E0_WP                                            !
!
      X = - B0 / (CX - AI)**2 * ONE / RS                            !
      Y =   FOUR * B0 * B0 / (CX - AI)**4  *  RS                    ! ref. (1) eq. (20)
      Z = - B0 / (CX - AI)**3  - ONE                                !
!
      EC_SE_G = AI / RS + TWO * X / Y * (                          &!
                                          SQRT(ONE + Y) - ONE -    &! ref. (1) eq. (19)
                                          Z * LOG(( SQRT(ONE + Y)  &!
                                          + Z) / ( ONE + Z))       &!
                                        )                           !
!
      END FUNCTION EC_SE_G
!
!  (2) Weak coupling regime: _W
!
!=======================================================================
!
      FUNCTION EC_L2_W(RS)
!
!  Weak coupling correlation energy for 2D systems
!
!  References: (1) P.-F. Loos, J. Chem. Phys. 138, 064108 (2013)

!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,TWO,EIGHT,TEN,THIRD
      USE PI_ETC,           ONLY : PI_INV
      USE DIRICHLET
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  LAMBDA_0,LAMBDA_1,E_0
      REAL (WP)             ::  EC_L2_W
!
      REAL (WP)             ::  SQRT,LOG
!
      LAMBDA_0 =   ZERO                                             !
      LAMBDA_1 = - SQRT(TWO) * (TEN * THIRD * PI_INV - ONE)         ! ref. (1) table I
      E_0      = LOG(TWO) + DB(2) - EIGHT * DB(4) * PI_INV * PI_INV !
!
      EC_L2_W = LAMBDA_0 * LOG(RS) +  E_0 + LAMBDA_1 * RS*  LOG(RS) ! ref. (1) eq. (1)
!
      END FUNCTION EC_L2_W
!
!  (3) Weak coupling regime (r_s small): _S
!
!
!=======================================================================
!
      FUNCTION EC_W2_S(RS)
!
!  Wigner correlation energy
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!  References: (1) P.-F. Loos and P. M. W. Gill, WIREs Comput. Mol. Sci. 6,
!                        410-419 (2016)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 30 Aug 2019
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS,EC_W2_S
      REAL (WP)             ::  ETA_0,ETA_1
      REAL (WP)             ::  X1
!
      REAL (WP)             ::  SQRT
!
      X1 = SQRT(RS)                                                 ! 
!
      ETA_0 = - 1.106103E0_WP                                       ! ref. (1) table 2 
      ETA_1 =   0.795E0_WP                                          ! 
! 
      EC_W2_S = ETA_0 / RS + ETA_1 / (RS * X1)                      ! ref. (1) eq. (48) 
!
      END FUNCTION EC_W2_S
!
!=======================================================================
!
      FUNCTION EC_IT_T(RS,T)
!
!  Temperature-dependent correlation energy for 2D systems
!
!  References:  A. Isihara and T. Toyoda, Phys. Rev. B 21,
!                  3358-3365 (1980)

!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * T        : temperature (SI)     
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : TWO,FOUR
      USE CONSTANTS_P1,     ONLY : K_B
      USE PI_ETC,           ONLY : PI,PI_INV
      USE ENE_CHANGE,       ONLY : EV,RYD
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS,T
      REAL (WP)             ::  EC_IT_T
      REAL (WP)             ::  EC_0,EC_T
      REAL (WP)             ::  A0,B0,C0,AT,BT,CT
      REAL (WP)             ::  ETA_0,P02,BETA
!
      REAL (WP)             ::  LOG
!
      BETA  = EV / (K_B * T * RYD)                                  ! in 1/Ryd
      P02   = TWO / (RS * RS)                                       ! 2 pi n
      ETA_0 = BETA * P02                                            !
!
      A0 = - 0.3946E0_WP                                            !
      B0 =   0.865E0_WP                                             ! ref. (1) eq. (8.6)
      C0 = - 0.173E0_WP                                             !
!
      AT = - 0.1824E0_WP                                            !
      BT = - 0.02968E0_WP                                           ! ref. (1) eq. (8.6)
      CT =   PI_INV / 24.0E0_WP                                     !
!
      EC_0  = A0 + B0 * RS + C0 * RS * LOG(RS)                      !
      EC_T  = FOUR * PI * ( AT + BT * LOG(ETA_0) +                & !
                            CT * LOG(ETA_0) * LOG(ETA_0)          & ! ref. (1) eq. (8.6) 
                          ) / ETA_0**2                              !
!
      EC_IT_T = EC_0 + EC_T                                         ! ref. (1) eq. (8.5)
!
      END FUNCTION EC_IT_T
!
!------ 3) 1D case --------------------------------------------
!
!
!=======================================================================
!
      FUNCTION EC_1D(EC_TYPE,RS,T)
!
!  This subroutine computes the 1D correlation energy EC
!    at a given value RS
!
!
!  Input parameters:
!
!       * EC_TYPE  : type of correlation energy functional
!                       EC_TYPE = 'LOOS_W'   --> Loos
!                       EC_TYPE = 'WIGN_S'   --> Wigner
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * T        : temperature (SI)     
!
!  Output parameters:
!
!       * EC        : value at RS
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
      IMPLICIT NONE
!
      CHARACTER*6 EC_TYPE
!
      REAL (WP)             ::  EC_1D,RS,T
!
      IF(EC_TYPE == 'LOOS_W') THEN                                  !
        EC_1D = EC_L1_W(RS)                                         !
      ELSE IF(EC_TYPE == 'WIGN_S') THEN                             !
        EC_1D = EC_W1_S(RS)                                         !
      END IF                                                        !
!
      END FUNCTION EC_1D
!
!=======================================================================
!
      SUBROUTINE DERIVE_EC_1D(EC_TYPE,IDERIV,RS,T,D_EC_1,D_EC_2)
!
!  This subroutine computes the first and second derivative 
!    of the correlation energy E_c with repect to r_s
!    at a given value RS
!
!
!  Input parameters:
!
!       * EC_TYPE  : type of correlation energy functional
!                       EC_TYPE = 'LOOS_W'   --> Loos
!                       EC_TYPE = 'WIGN_S'   --> Wigner
!       * IDERIV   : type of n_point formula used for derivation (n = IDERIV)
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * T        : temperature (SI)     
!
!  Output parameters:
!
!       * D_EC_1    : first derivative at RS
!       * D_EC_2    : second derivative at RS
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
!
      USE DIMENSION_CODE,   ONLY : ND_MAX
      USE DERIVATION  
      USE INTERPOLATION,    ONLY : INTERP_NR,SPLINE,SPLINT
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 6)   ::  EC_TYPE
!
      INTEGER               ::  IDERIV,I,LOGF
!
      REAL (WP)             ::  RS,RI,T
      REAL (WP)             ::  D_EC_1,D_EC_2
!
      REAL (WP)             ::  R(ND_MAX),EC(ND_MAX)
      REAL (WP)             ::  D_EC(ND_MAX),DD_EC(ND_MAX)
      REAL (WP)             ::  R_MIN,R_MAX,STEP
!
      REAL (WP)             ::  FLOAT
!
      R_MIN = 0.01E0_WP                                             !   
      R_MAX = 50.01E0_WP                                            !
      STEP  = (R_MAX - R_MIN) / FLOAT(ND_MAX - 1)                   !
!
      LOGF=6                                                        !
!
!  Storing the correlation energy EC as a function of RS
!
      DO I = 1,ND_MAX                                               !
!
        R(I) = R_MIN + FLOAT(I - 1) * STEP                          !
        RI   = R(I)                                                 !
!
      IF(EC_TYPE == 'LOOS_W') THEN                                  !
        EC(I) = EC_L1_W(RS)                                         !
      ELSE IF(EC_TYPE == 'WIGN_S') THEN                             !
        EC(I) = EC_W1_S(RS)                                         !
      END IF                                                        !
!
      END DO                                                        !
!
!  Computing the first and second derivatives 
!    with a IDERIV-point formula
!
      CALL DERIV_1(EC,ND_MAX,IDERIV,STEP,D_EC)                      !
      CALL DERIV_1(D_EC,ND_MAX,IDERIV,STEP,DD_EC)                   !
!
!  Interpolation of derivatives at RS
!
      CALL INTERP_NR(LOGF,R,D_EC,ND_MAX,RS,D_EC_1)                  !
      CALL INTERP_NR(LOGF,R,DD_EC,ND_MAX,RS,D_EC_2)                 !
!
      END SUBROUTINE DERIVE_EC_1D
!
!=======================================================================
!
!             Exchange/Correlation energy functionals (in Ryd)
!
!  Different regimes:  * weak coupling          : r_s << 1
!                      * metallic state         : 2 <= r_s  <= 6
!                      * Wigner crystallization : r_s >= 100
!
!
!
!  (1) Weak coupling regime: _W
!
!=======================================================================
!
      FUNCTION EC_L1_W(RS)
!
!  Weak coupling correlation energy for 1D systems
!
!  References: (1) P.-F. Loos, J. Chem. Phys. 138, 064108 (2013)
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : ZERO,THIRD
      USE PI_ETC,           ONLY : PI2
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS
      REAL (WP)             ::  LAMBDA_0,LAMBDA_1,E_0,E_1
      REAL (WP)             ::  EC_L1_W
!
      REAL (WP)             ::  LOG
!
      LAMBDA_0 =   ZERO                                             !
      LAMBDA_1 =   ZERO                                             ! ref. (1) table I
      E_0      = - PI2 / 360.0E0_WP                                 !
      E_1      =   0.00845E0_WP                                     !
!
      EC_L1_W = LAMBDA_0 * LOG(RS) +                              & !
                E_0 + LAMBDA_1 * RS * LOG(RS)  +  E_1 * RS          ! ref. (1) eq. (1)
!
      END FUNCTION EC_L1_W
!
!  (2) Weak coupling regime (r_s small): _S
!
!
!=======================================================================
!
      FUNCTION EC_W1_S(RS)
!
!  Wigner correlation energy
!
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!
!  References: (1) P.-F. Loos and P. M. W. Gill, WIREs Comput. Mol. Sci. 6,
!                        410-419 (2016)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified :  9 Jun 2020
!
!
      USE REAL_NUMBERS,     ONLY : TWO,HALF
      USE EULER_CONST,      ONLY : EUMAS
!
      IMPLICIT NONE
!
      REAL (WP)             ::  RS,EC_W1_S
      REAL (WP)             ::  ETA_0,ETA_1
      REAL (WP)             ::  X1
!
      REAL (WP)             ::  SQRT,LOG
!
      X1 = SQRT(RS)                                                 ! 
!
      ETA_0 = HALF * (EUMAS - LOG(TWO))                             ! ref. (1) table 2
      ETA_1 = 0.359933E0_WP                                         ! 
!
      EC_W1_S = ETA_0 / RS + ETA_1 / (RS * X1)                      !  ref. (1) eq. (48)
!
      END FUNCTION EC_W1_S
!
END MODULE CORRELATION_ENERGIES
