!
!=======================================================================
!
MODULE EXCHANGE_ENERGIES
!
      USE ACCURACY_REAL
!
CONTAINS
!
!------ 1) 3D case --------------------------------------------
!
!=======================================================================
!
      FUNCTION EX_3D(EX_TYPE,IMODE,RS,T,XI)
!
!  This subroutine computes the 3D exchange energy EX
!    at a given value RS
!
!
!  Input parameters:
!
!       * EX_TYPE  : type of kinetic energy functional
!                       EX_TYPE = 'HEG'   --> homogeneous electron gas
!       * IMODE    : choice of parameters
!                       IMODE = 1 : no spin polarization 
!                       IMODE = 2 : fully spin-polarized
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * T        : temperature (SI)     
!       * XI       : spin polarization : (n+ - n-) / n
!
!  Output parameters:
!
!       * EX_3D    : value at RS
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 15 Sep 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)     ::  EX_TYPE
!
      REAL (WP), INTENT(IN)   ::  RS,T,XI
      REAL (WP)               ::  EX_3D
!
      INTEGER                 ::  IMODE
!
      IF(EX_TYPE == 'HEG') THEN                                     !
        EX_3D = EX_HEG_3D(RS,XI)                                    !
      END IF                                                        !
!
      END FUNCTION EX_3D
!
!=======================================================================
!
      FUNCTION EX_HEG_3D(RS,XI)  
!
!  This function computes the exchange energy in the 3D homegeneous 
!    electron gas model
!
!  Reference:  (1) U. von Barth and L. Hedin, 
!                     J. Phys. C : Solid State Phys. 5, 1629-1642 (1972)
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * XI       : spin polarization : (n+ - n-) / n
!
!  Output parameters:
!
!       * EX_HEG   : value at RS (in Ry)
!
!
!  Note: in reference (1), equations are expressend in terms of 
!        x = n+ / n. The relation with XI is 
!
!                       XI = 2x - 1
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 15 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,FOUR,HALF,THIRD
      USE PI_ETC,           ONLY : PI
      USE UTILITIES_1,      ONLY : ALFA
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)   ::  RS,XI
      REAL (WP)               ::  EX_HEG_3D
      REAL (WP)               ::  X,A,E,FX,G
      REAL (WP)               ::  ALPHA,COEF,EX_P,MUX_P
!
      X  = HALF * (XI - ONE)                                        !
      A  = HALF**THIRD                                              !
      E  = FOUR * THIRD                                             !
      FX = ( X**E + (ONE - X)**E - A ) / (ONE - A)                  ! ref. 1 eq. (5.3)
      G  = E * A / (ONE - A)                                        !
!
      ALPHA = ALFA('3D')                                            !
      COEF  = THREE / (TWO * PI * ALPHA)                            ! 
      EX_P  = - COEF / RS                                           ! ref. 1 eq. (5.7) 
      MUX_P = E * EX_P                                              ! ref. 1 eq. (5.8) 
!
      EX_HEG_3D = EX_P + MUX_P * FX / G                             ! ref. 1 eq. (5.4)  
!
      END FUNCTION EX_HEG_3D
!
END MODULE EXCHANGE_ENERGIES
