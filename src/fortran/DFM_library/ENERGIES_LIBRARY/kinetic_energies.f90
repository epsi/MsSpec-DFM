!
!=======================================================================
!
MODULE KINETIC_ENERGIES
!
      USE ACCURACY_REAL
!
CONTAINS
!
!------ 1) 3D case --------------------------------------------
!
!=======================================================================
!
      FUNCTION EK_3D(EK_TYPE,RS,T,XI)
!
!  This subroutine computes the 3D kinetic energy EK
!    at a given value RS
!
!
!  Input parameters:
!
!       * EK_TYPE  : type of exchange energy functional
!                       EK_TYPE = 'HEG'   --> homogeneous electron gas
!       * IMODE    : choice of parameters
!                       IMODE = 1 : no spin polarization 
!                       IMODE = 2 : fully spin-polarized
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * T        : temperature (SI)     
!       * XI       : spin polarization : (n+ - n-) / n
!
!  Output parameters:
!
!       * EK_3D    : value at RS
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 15 Sep 2020
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 3)     ::  EK_TYPE
!
      REAL (WP), INTENT(IN)   ::  RS,T,XI
      REAL (WP)               ::  EK_3D
!
      INTEGER                 ::  IMODE
!
      IF(EK_TYPE == 'HEG') THEN                                     !
        EK_3D = EK_HEG_3D(RS,XI)                                    !
      END IF                                                        !
!
      END FUNCTION EK_3D
!
!=======================================================================
!
      FUNCTION EK_HEG_3D(RS,XI)  
!
!  This function computes the kinetic energy in the 3D homegeneous 
!    electron gas model
!
!  Reference:  (1) A. Sarkar, S. Haldar, D. Roy and D. Sen,
!                     Acta Phys. Polonica A 106, 497-514 (2004)
!              (2) H. T. Tran and J. P. Perdew, Am. J. Phys. 71,
!                     1048-1061 (2003)
!
!  Input parameters:
!
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * XI       : spin polarization : (n+ - n-) / n
!
!  Output parameters:
!
!       * EX_HEG   : value at RS (in Ry)
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 15 Sep 2020
!
!
      USE REAL_NUMBERS,     ONLY : ONE,TWO,THREE,FIVE,THIRD
      USE UTILITIES_1,      ONLY : ALFA
!
      IMPLICIT NONE
!
      REAL (WP), INTENT(IN)   ::  RS,XI
      REAL (WP)               ::  EK_HEG_3D
      REAL (WP)               ::  ALPHA,COEF
      REAL (WP)               ::  A,E,FK
      REAL (WP)               ::  EK_P,EK_F
!
      A  = TWO**(TWO * THIRD)                                       !
      E  = FIVE * THIRD                                             !
      FK = ( (ONE + XI)**E + (ONE - XI)**E - TWO ) /              & !
           (TWO * (A - ONE) )                                       !
!
      ALPHA = ALFA('3D')                                            !
      COEF  = THREE / (FIVE * ALPHA * ALPHA)                        ! ref. 2 eq. (4)
      EK_P  = COEF / (RS * RS)                                      !
      EK_F  = A * EK_P                                              !
!
      EK_HEG_3D = EK_P + (EK_F - EK_P) * FK                         ! ref. 1 eq. (11)
!
      END FUNCTION EK_HEG_3D
!
END MODULE KINETIC_ENERGIES
 
