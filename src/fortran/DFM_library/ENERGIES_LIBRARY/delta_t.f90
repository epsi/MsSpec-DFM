!
!=======================================================================
!
MODULE DELTA_KIN
!
      USE ACCURACY_REAL
!
CONTAINS
!
!------ 1) 3D case --------------------------------------------
!
!=======================================================================
!
      SUBROUTINE DELTA_KIN_3D(EC_TYPE,IMODE,IDERIV,RS,T,I_DE,DT,DT2) 
!
!  This subroutine computes delta_t and delta_t2 values for 3D systems
!
!
!  References: (1) J. Toulouse, Phys. Rev. B 72, 035117 (2005)
!
!  They are defined, in terms of the kinetic energy,  as 
!
!                        < t > - < t >  
!                                     0
!          delta_t   =  ----------------
! 
!                            < t >
!                                 0
!
!
!                           2       2
!                        < t > - < t >  
!                                     0
!          delta_t2   =  ----------------
!                               2
!                            < t >
!                                 0
!
!
!  Input parameters:
!
!       * EC_TYPE  : type of correlation energy functional
!       * IMODE    : choice of parameters
!                       IMODE = 1 : no spin polarization 
!                       IMODE = 2 : fully spin-polarized
!       * IDERIV   : type of n_point formula used for derivation (n = IDERIV)
!       * RS       : Wigner-Seitz radius of electron (in units of a_0)
!       * T        : temperature (SI)     
!       * I_DE     : type of parametrization of delta_t2
!                       I_DE = 1 :  RPA
!                       I_DE = 2 :  GW
!                       I_DE = 3 :  Gori-Giorgi and Ziesche
!
!
!  Output parameters:
!
!       * DT       : delta_t
!       * DT2      : delta_t2
!
!
!
!   Author :  D. Sébilleau
!
!                                           Last modified : 30 Oct 2020
!
!
!
      USE REAL_NUMBERS,     ONLY : TWO,FIVE,THIRD
      USE FERMI_AU,         ONLY : EF_AU
!
      USE CORRELATION_ENERGIES
!
      IMPLICIT NONE
!
      CHARACTER (LEN = 6)    ::  EC_TYPE
!
      INTEGER, INTENT(IN)    ::  IMODE,IDERIV,I_DE
!
      REAL (WP), INTENT(IN)  ::  RS,T
      REAL (WP), INTENT(OUT) ::  DT,DT2
!
      REAL (WP)              ::  EC,D_EC_1,D_EC_2
      REAL (WP)              ::  EF
      REAL (WP)              ::  D1(3:6),D2(3:6),D3(3:6)
      REAL (WP)              ::  U,U3,U4,U5,U6
!
      REAL (WP)              ::  SQRT
!
      DATA D1 /  0.093623E0_WP,  0.194288E0_WP,  0.051445E0_WP,  0.005449E0_WP /  ! RPA
      DATA D2 /  0.126362E0_WP,  0.001428E0_WP,  0.014278E0_WP, -0.004522E0_WP /  ! GW
      DATA D3 /  0.271191E0_WP, -0.009998E0_WP, -0.036383E0_WP,  0.006706E0_WP /  ! GZ
!
      U  = SQRT(RS)                                                 !
      U3 = U * U * U                                                !
      U4 = U3 * U                                                   !
      U5 = U4 * U                                                   !
      U6 = U5 * U                                                   !
!
!  Correlation energy and its derivatives
!
      EC = EC_3D(EC_TYPE,1,RS,T)                                    !
      CALL DERIVE_EC_3D(EC_TYPE,1,5,RS,T,D_EC_1,D_EC_2)             ! 
!
!  E_F in Rydberg
!
      EF = TWO * EF_AU                                              ! EF_AU in Hartree
!
      DT = - FIVE * THIRD * (EC + RS * D_EC_1) / EF                 !
!
!  Parametrization of delta_t2
!
      IF(I_DE == 1) THEN                                            !
        DT2 = D1(3) * U3 + D1(4) * U4 + D1(5) * U5 + D1(6) * U6     !
      ELSE IF(I_DE == 2) THEN                                       !
        DT2 = D2(3) * U3 + D2(4) * U4 + D2(5) * U5 + D2(6) * U6     ! ref. (1) eq. (B1)
      ELSE IF(I_DE == 3) THEN                                       !
        DT2 = D3(3) * U3 + D3(4) * U4 + D3(5) * U5 + D3(6) * U6     !
      END IF                                                        !
!
      END SUBROUTINE DELTA_KIN_3D
!
END MODULE DELTA_KIN
