!
!=======================================================================
!
SUBROUTINE EPSILON(MYINFILE, MYOUTDIR)
!
!  This program computes model dielectric functions for many type
!    of materials. These dielectric functions are essentially based
!    on the Fermi liquid theory.
!
!  Several other physical quantities, based on the
!    dielectric function can also be computed, namely:
!
!                 * the loss function
!                 * the EELS cross-section
!                 * the plasmon fluctuation potential
!                 * the stopping power
!                 * the optical properties
!
!
!  Lead developer: Didier Sébilleau
!
!  Co-developers : Aditi Mandal, Sylvain Tricot
!
!
!
!  Main notations :
!
!       * X : dimensionless factor   --> X = q / (2 * k_F)
!
!       * Y : dimensionless factor   --> Y = q / k_F
!
!       * Z : dimensionless factor   --> Z = omega / omega_q     = V / (4 * X * X)
!
!       * U : dimensionless factor   --> U = omega / (q * v_F)   = X * Z   = V / (4 * X)
!
!       * V : dimensionless factor   --> V = omega / omega_{k_F} = Z * Y^2 = 4 U * X
!
!
!
!
!                                           Last modified :  6 Aug 2021
!
!
      USE ACCURACY_REAL
      USE DIMENSION_CODE,   ONLY : NSIZE
!
      USE SF_VALUES,        ONLY : SQO_TYPE
!
      USE REAL_NUMBERS,     ONLY : ZERO,ONE,HALF,SMALL
      USE MATERIAL_PROP,    ONLY : RS,DMN
      USE EXT_FIELDS,       ONLY : H
!
      USE Q_GRID
!
      USE FERMI_VALUES
      USE FERMI_VALUES_M
      USE PLASMON_ENE
      USE PLASMA_SCALE
      USE CALC_ASYMPT
!
      USE PRINT_CALC_TYPE
      USE PRINT_HEADERS
      USE PRINT_FERMI
      USE PRINT_PLASMONS
      USE PRINT_ASYMPTOTIC
      USE PRINT_SCALE_PARAM
      USE PRINT_MAT_LENGTHS
      USE PRINT_THERMAL
      USE PRINT_THERMODYNAMICS
      USE PRINT_ENERGIES_EL
!
      USE DAMPING_COEF
      USE PLASMON_DISPERSION
!
      USE CALL_CALC_1
      USE CALL_CALC_2
      USE CALL_CALC_3
      USE CALL_CALC_5
      USE CALL_CALC_7
      USE CALL_CALC_9
      USE CALL_CALC_P
!
      USE TEST_INTEGRALS_2
      USE TEST_INTEGRALS_3
      USE TEST_INTEGRALS_8
      USE TEST_INT_HUBBARD
!
      USE RE_EPS_0_TREATMENT
!
      USE INPUT_DATA
!
      USE OUT_VALUES_10
!
      USE OUT_CALC
!
      USE CHANGE_FILENAMES
!
      USE OUTFILES
      USE OPEN_OUTFILES
      USE CLOSE_OUTFILES
!
      IMPLICIT NONE
!
      INTEGER               ::  N_IF,JF
      INTEGER               ::  IQ,IE
!
      REAL (WP)             ::  Q,X
      REAL (WP)             ::  EPSR(NSIZE),EPSI(NSIZE),EN(NSIZE)
!
      CHARACTER (LEN = 100) ::  INPDATA(999)
      CHARACTER (LEN = 100) ::  LOGFILE(999)
!
      CHARACTER (LEN = 100) ::  MYINFILE
      CHARACTER (LEN =  80) ::  MYOUTDIR
!
      OUTDIR = MYOUTDIR
      CALL SYSTEM('mkdir -p '//TRIM(OUTDIR))
!
!  Loop on the input data files
!
!      N_IF = 11
      N_IF = 1
!      READ(*,15) N_IF                                                !
!      DO JF = 1,N_IF                                                 !
!        READ(*,25) INPDATA(JF)                                       !
!      END DO                                                         !
      INPDATA(1) = TRIM(MYINFILE)
!      INPDATA(1) = 'Data/epsilon.dat'
!      INPDATA(1) = 'Data/epsilon_00.dat'
!      INPDATA(2) = 'Data/epsilon_01.dat'
!      INPDATA(3) = 'Data/epsilon_02.dat'
!      INPDATA(4) = 'Data/epsilon_03.dat'
!      INPDATA(5) = 'Data/epsilon_04.dat'
!      INPDATA(6) = 'Data/epsilon_05.dat'
!      INPDATA(7) = 'Data/epsilon_06.dat'
!      INPDATA(8) = 'Data/epsilon_07.dat'
!      INPDATA(9) = 'Data/epsilon_08.dat'
!      INPDATA(10) = 'Data/epsilon_09.dat'
!      INPDATA(11) = 'Data/epsilon_10.dat'
!
!
!  Name of the corresponding log files
!
      CALL LOGFILE_NAMES(N_IF,LOGFILE)                              !
!
      DO JF = 1,N_IF                                                ! start loop on files
!
!  Initialization of the arrays
!
        DO IE=1,NSIZE                                               !
          EN(IE)   = ZERO                                           !
          EPSR(IE) = ZERO                                           !
          EPSI(IE) = ZERO                                           !
        END DO                                                      !
!
!  Opening input/log data files
!
        OPEN(UNIT=5,FILE=TRIM(INPDATA(JF)),STATUS='OLD')            !
!        OPEN(UNIT=6,FILE=TRIM(LOGFILE(JF)),STATUS='UNKNOWN')        !
!
!  Printing the headers
!
        CALL PRINT_ASCII                                            !
!
!  Reading the input data file
!
        CALL READ_DATA                                              !
!
        IF(SQO_TYPE == 'UTI') THEN                                  !
          OPEN(UNIT = 1, FILE = TRIM(OUTDIR)//'/utic_para.dat',   & !
                         STATUS = 'unknown')                        !
        END IF                                                      !
!
!  Opening result files
!
        CALL OPEN_OUTPUT_FILES(N_IF,JF)                             !
!
!  Post-processing:
!
        IF(PL_DISP == '  EXACT') THEN                               !
          I_PP = I_FP + I_PD                                        !
        ELSE                                                        !
          I_PP = I_FP                                               !
        END IF                                                      !
!
!  Printing the information on the calculations to be performed
!
!        CALL PRINT_CALC_INFO                                        !
!
!  Computation of the Fermi values and storage
!
        WRITE(6,10)                                                 !
!
        IF(H < SMALL) THEN                                          !
          CALL CALC_FERMI(DMN,RS)                                   !
          CALL PRINT_FERMI_SI                                       !
        ELSE                                                        !
          CALL CALC_FERMI_M(DMN,RS)                                 !
          CALL PRINT_FERMI_SI_M                                     !
        END IF                                                      !
!
!  Test of the integrals
!
        IF(I_TI /= 0) THEN                                          !
          IF(I_TI ==  2) CALL CALC_TEST_INT_2                       !
          IF(I_TI ==  3) CALL CALC_TEST_INT_3                       !
          IF(I_TI ==  8) CALL CALC_TEST_INT_8                       !
          IF(I_TI == 10) CALL CALC_TEST_HUBBARD                     !
        END IF                                                      !
!
!  Computation of the plasmon properties and storage
!
        CALL CALC_PLASMON_ENE                                       !
        CALL CALC_PLASMA_SCALE                                      !
!
!  Computation of the asymptotic values and storage
!
!        CALL CALC_ASYMPT_VALUES                                     !
!
!  Selective printing of physical properties (log file)
!
        IF(I_WR == 1) THEN                                          !
!
!  Printing the plasma properties
!
          WRITE(6,10)                                               !
          CALL PRINT_PLASMA                                         !
!
!  Printing the asymptotic values
!
          WRITE(6,10)                                               !
          CALL PRINT_ASYMPT_VALUES                                  !
!
!  Printing the scale parameters
!
          WRITE(6,10)                                               !
          CALL PRINT_SCALE_PARAMETERS                               !
!
!  Printing the material's characteristic lengths
!
          WRITE(6,10)                                               !
          CALL PRINT_CHAR_LENGTHS                                   !
!
!  Printing the thermal properties
!
          WRITE(6,10)                                               !
          CALL PRINT_THERMAL_PROP                                   !
!
!  Printing the thermodynamics properties
!
          WRITE(6,10)                                               !
          CALL PRINT_THERMODYNAMICS_PROP                            !
!
!  Printing the energies at q = 0
!
          WRITE(6,10)                                               !
          CALL PRINT_ENERGIES(ZERO,0,ZERO,ONE)                      !
!
        END IF                                                      !
!
!  Calling calculator 5 (Fermi properties)
!
        IF(I_C5 > 0) CALL USE_CALC_5                                !
!
!  Starting the loop on q
!    (the loop on energy is inside the calculators)
!
!
!..........  Loop on plasmon momentum Q  ..........
!
        DO IQ = 1,N_Q                                               !
!
          Q = Q_MIN + FLOAT(IQ - 1) * Q_STEP                        ! Q = q/k_F
!
          X = HALF * Q                                              ! X = q/(2k_f)
!
!  Computing and printing the damping (if any)
!
          CALL CALC_DAMPING(IQ,X)
!
!  Calling calculator 1 (eps, pi, chi, sigma)
!
          IF(I_C1 > 0) CALL USE_CALC_1(X,EN,EPSR,EPSI)              !
!
!  Calling calculator 2
!
          IF(I_C2 > 0) CALL USE_CALC_2(IQ,X)                        !
!
!  Calling calculator 3
!
          IF(I_C3 > 0) CALL USE_CALC_3(IQ,X)                        !
!
!  Calling calculator 9
!
          IF(I_C9 > 0) CALL USE_CALC_9(X)                           !
!
!  Calling test calculator
!
!          CALL CALC_TEST(IQ,X)
!
!..........  End of loop on plasmon momentum Q  ..........
!
        END DO                                                      !
!
!  Calling calculator 7 (Energies)
!
        IF(I_C7 > 0) CALL USE_CALC_7                                !
!
!  Post-processing whenever requested
!
        IF(I_PP > 0) CALL USE_CALC_P                                !
!
        IF(I_ZE == 1) THEN                                          !
          CALL REORDER_EPS0_PRINT                                   !
        END IF                                                      !
!
!  Closing input/log data files
!
        CLOSE(5)                                                    !
!        CLOSE(6)                                                    !
!
        IF(SQO_TYPE == 'UTI') THEN                                  !
          CLOSE(1)                                                  !
        END IF                                                      !

!
!  Closing the indexed result files
!
        CALL CLOSE_OUTPUT_FILES(0)                                  !
!
!  End of input data files loop
!
      END DO                                                        !
!
!  Closing the other result files
!
      CALL CLOSE_OUTPUT_FILES(1)                                    !
!
!  Formats:
!
  10  FORMAT('     ')
!  15  FORMAT(I3)
!  25  FORMAT(A50)
!
END SUBROUTINE
